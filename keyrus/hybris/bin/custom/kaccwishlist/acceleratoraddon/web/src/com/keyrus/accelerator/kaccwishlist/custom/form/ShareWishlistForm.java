package com.keyrus.accelerator.kaccwishlist.custom.form;

import de.hybris.platform.validation.annotations.NotEmpty;

import org.hibernate.validator.constraints.Email;


/**
 * @author Fahmi.Allani
 * 
 */
public class ShareWishlistForm
{
	private String receiverName;
	
	@NotEmpty
	@Email(message = "{wishListShareForm.email.invalid}")
	private String receiverEmail;

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getReceiverEmail() {
		return receiverEmail;
	}

	public void setReceiverEmail(String receiverEmail) {
		this.receiverEmail = receiverEmail;
	}
}