package com.keyrus.accelerator.kaccwishlist.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.keyrus.accelerator.wishlist.data.WishlistData;
import com.keyrus.accelerator.wishlist.data.WishlistEntryData;

import de.hybris.platform.commercefacades.product.data.ImageData;


public class WishListFormatterUtils
{
	private WishListFormatterUtils()
	{
		//empty
	}

	public static String getFormattedWhishList(final WishlistData wishData)
	{
		final List<WishlistEntryData> wishlistEntries = wishData.getEntries();

		final List<WishlistEntryData> allEntries = new ArrayList<>();


		for (final WishlistEntryData entryData : wishlistEntries)
		{
			allEntries.add(entryData);
		}

		final StringBuilder printableList = new StringBuilder();
		printableList.append(
				"<table width='90%' cellspacing='10px' style='margin:10px;padding:10px;table-layout:auto;border:1px solid #000000;'>");
		// Wish list name
		printableList.append("<tr>");
		printableList.append("<td align='center' valign='middle' colspan=2><strong>");
		printableList.append(wishData.getName());
		printableList.append("</strong></td>");
		printableList.append("</tr><tr/>");
		printableList.append(displayWishlistEntryData(wishlistEntries));
		printableList.append("</table>");

		return printableList.toString();
	}
	
	public static String displayWishlistEntryData (final List<WishlistEntryData> wishlistEntries){
		final StringBuilder display = new StringBuilder();
		if (CollectionUtils.isNotEmpty(wishlistEntries))
		{
			for (WishlistEntryData wishlistEntrie :wishlistEntries)
			{
				display.append("<tr>");
				display.append("<td valign='top'><p><font color='#666666' size='2' face='Arial, Helvetica, sans-serif'><a href='${ctx.secureBaseUrl}/p/");
				display.append(wishlistEntrie.getProduct().getCode());
				display.append("'><font color='#666666'><b>");
				display.append(wishlistEntrie.getProduct().getName());
				display.append("</b></font></p></a>");
				display.append("</td>");
				display.append("<td valign='top'><p><font color='#666666' size='2' face='Arial, Helvetica, sans-serif'><b>");
				display.append(wishlistEntrie.getProduct().getPrice().getFormattedValue());
				display.append("</b></font></p>");
				display.append("</td>");
				display.append("</tr>");				
			}
			
		}
		return display.toString();
		
	}
	
}
