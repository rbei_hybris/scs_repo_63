<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<input type="hidden" id="idproductCode" value="${productCode}" />
<input type="hidden" id="idwishlistName" value="${wishlistName}" />
<input type="hidden" id="idurl" value="${pageContext.request.contextPath}" />

<div id="init-pwd-popin" class="">
    <h2><spring:theme code="wishlist.remove.popup.title" /></h2>
    
    <p>
        <i class="ico ico-warning"></i>
        <spring:theme code="wishlist.popup.text" />
    </p>
    <div class="action">
		<button type="button"   onclick="performRemoveWishlistEntry()"><spring:theme code="wishlist.remove.popup.submit" /></button>
    	<button type="button"   onclick="performCancelRemovingWishlistEntry()"><spring:theme code="wishlist.remove.popup.cancelbutton" /></button>
    </div>
</div>
