<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="addonProduct" tagdir="/WEB-INF/tags/addons/kaccwishlist/responsive/product" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="entry" required="true" type="com.keyrus.accelerator.wishlist.data.WishlistEntryData" %>

	<c:set var="stockLevelStatus" value="${entry.product.stock.stockLevelStatus.code}" />
	<c:set var="isForceInStock" value="${entry.product.stock.stockLevelStatus.code eq 'inStock' and empty product.stock.stockLevel}"/>
	<c:choose> 
	  <c:when test="${isForceInStock}">
	    <c:set var="maxQty" value="FORCE_IN_STOCK"/>
	  </c:when>
	  <c:otherwise>
	    <c:set var="maxQty" value="${entry.product.stock.stockLevel}"/>
	  </c:otherwise>
	</c:choose>
	<c:set var="qtyMinus" value="1" />

	<!--product item-->
	<div class="product wishItem">
	
		
		
		<!-- Product name + message-->
		<div class=" wishItem--infos">
			<!--wishItem--infos-part-1-->
			<div class="wishItem--infos-part-1">
				<!--details-->
				<div class="wishItem--infos-details">
					<div class="check">
						<addonProduct:productAddToCartButton product="${entry.product}" />
					</div>
					<div class="check">
						<a href="${pageContext.request.contextPath}/p/${entry.product.code}" class="name">${entry.product.name}</a>
	<div class = "wishlist">	
		<c:if test="${entry.product.stock.stockLevel gt 0}">
			<c:set var="productStockLevel"> <spring:theme code="product.variants.in.stock"/>
			</c:set>
		</c:if>
		<c:if test="${entry.product.stock.stockLevelStatus.code eq 'lowStock'}">
			<c:set var="productStockLevel">
				<spring:theme code="product.variants.in.stock" />
			</c:set>
		</c:if>
		<c:if test="${not empty productStockLevel}">
			<div class="stock-wrapper availability clearfix">
				<p class="availability--stock">${productStockLevel}</p>
			</div>
		</c:if>
		<c:if test="${entry.product.stock.stockLevelStatus.code eq 'outOfStock'}">
			<div class="stock-wrapper availability clearfix">
				<p class="out--stock"><spring:theme code="product.variants.out.of.stock"/></p>
			</div>
		</c:if>
	</div>		
		
<%-- 						<span class="${entry.product.code}-wl-entry infos-wl-entry" ${display}> --%>
<%-- 							<spring:theme code="wishlist.addToCart.message.unavailable" /> --%>
<!-- 						</span> -->
					</div>

					
					
				</div>

								<!-- Configuration selector -->
				<div class="configuration">
				<c:if test="${not empty entry.product.classifications}">
					<c:forEach items="${entry.product.classifications}" var="classification">
						<c:if test="${fn:toLowerCase(classification.name) eq 'configuration'}">
							<c:if test="${not empty classification.features}">
								<div class="add-to-cart--configuration-select">
									<c:forEach items="${classification.features}" var="feature">
		                       	<select id="productConfOption${entry.product.code}">
		                           <c:forEach items="${feature.featureValues}" var="featureValue" varStatus="status">
		                           	<option value="${fn:escapeXml(featureValue.value)}" ${featureValue.value == entry.confProduct ? 'selected="selected"' : ''}>${fn:escapeXml(featureValue.value)}</option>
		                           </c:forEach>
		                       	</select>								
									</c:forEach>
								</div>
							</c:if>
						</c:if>
					</c:forEach>
				</c:if>
				</div>

				<!-- Price data -->
				<div class="wishItem--price price">
					<div class="price--line">
			  			<product:productPromotionSection product="${entry.product}" /> 
						
						<c:if test="${entry.product.discountPrice ne null}">
							<c:set var="realPriceValue"  value="${entry.product.discountPrice.value}"/>
							<span class="percentDiscount">${entry.product.percentageDiscount }<spring:theme code="product.percentage.off"/></span>
							<span class="firstPrice"><format:fromPrice priceData="${entry.product.price}" strike="true"/></span>
							<span class="discountPrice"><format:price priceData="${entry.product.discountPrice}"/></span>
							
						</c:if>
						<c:if test="${entry.product.discountPrice eq null}">
							<c:set var="realPriceValue"  value="${entry.product.price.value}"/>
							<format:price priceData="${entry.product.price}"/>
						</c:if>
						
						<input type="hidden" id="productUnitPrice_${entry.product.code}" value="${realPriceValue}"/>
						<input type="hidden" id="productCurrency_${entry.product.code}" value="${entry.product.price.currencyIso}"/>
					</div>
				</div>
			</div>

			<!--wishItem--infos-part-2-->
			<div class="wishItem--infos-part-2">
				<!--wishItem--qty-selection-row-->
				<div class="wishItem--qty-selection-row">
				<!-- Quantity selector -->
				<div class="quantity">
					<c:set var="quantity" value="${entry.desired}" />
					<div class="cart-qty">
						<c:url value="/cart/add" var="addToCartUrl"/>
						<form:form id="addToCartForm${entry.product.code}" action="${addToCartUrl}" method="post" class="add_to_cart_form">
							<c:set var="isForceInStock" value="${entry.product.stock.stockLevelStatus.code eq 'inStock' and empty entry.product.stock.stockLevel}"/>
							<c:choose>
								<c:when test="${isForceInStock}">
							   	<c:set var="maxQty" value="FORCE_IN_STOCK"/>
								</c:when>
								<c:otherwise>
								  <c:set var="maxQty" value="${entry.product.stock.stockLevel}"/>
								</c:otherwise>
							</c:choose>
							<div class="qty row js-qty-selector wishItem--qty ${entry.product.code}">
								<div class="wishItem--qty-selector">
									<div class="qty-selector">
										<button class="qty-selector--btn qty-selector--minus minus js-qty-selector-minus" type="button" data-productCode="${entry.product.code}">
											<span class="icon icon--minus-carnation"></span>
										</button>
									
										<input type="text" maxlength="3" class="qty-selector--input orm-control js-qty-selector-input" size="1" value="${quantity}"
												data-max="${maxQty}" data-min="1"  data-product="${entry.product.code}"
												id="pdpAddtoCartInput" name="pdpAddtoCartInput"  />
									
										<button class="qty-selector--btn qty-selector--plus plus js-qty-selector-plus" type="button" data-productCode="${entry.product.code}">
											<span class="icon icon--plus-carnation"></span>
										</button>
									</div>
								</div>
								<div class="clearfix"></div>
								
							</div>
				         <input type="hidden" name="productCodePost" value="${entry.product.code}"/>
						</form:form>	
					</div>
				</div>

				
				<!-- Subtotal --> 
				<div class="price wishItem--total price--total">
							<span id="productSubtotalPrice_${entry.product.code}" class="js-subtotal-price" data-product-code="${entry.product.code}">
								<fmt:formatNumber type="number" minFractionDigits="2"  maxFractionDigits="2" value="${realPriceValue * quantity}" />
							</span>
							&nbsp;
							<span class="currency">${entry.product.price.currencyIso}</span>
				</div>	
				
				<!-- Delete -->	
				<div class="remove wishItem--delete delete remove-item remove-mini-cart-entry-button">
					<button class="js-wishlist-entry-delete" data-product-code="${entry.product.code}" data-wishlist-name="${selectedWishList}">
						<span class="icon icon--garbage js-remove-entry-button" id="removeEntry_0">&nbsp;</span>
					</button>
				</div>
			</div>
			<!--Message-->
<%-- 			<c:if test="${!isForceInStock and maxQty > 0}"> --%>
<%-- 				<div class="wishItem--max-quantity-message"><spring:theme code="product.variants.quantity.limit" arguments="${maxQty}" /></div> --%>
<%-- 			</c:if> --%>
			<!--end Message-->
			<!--end wishItem--qty-selection-row-->
					
			</div>
			<!--end part 2-->
		</div>
		
	</div>
	<!--end product item-->
