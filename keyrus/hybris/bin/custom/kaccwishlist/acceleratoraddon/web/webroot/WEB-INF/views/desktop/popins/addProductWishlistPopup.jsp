<%@ page trimDirectiveWhitespaces="true" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<input type="hidden" id="productCode" value="${productCode}" />
<c:choose>
	<c:when test="${isConnected}">
		<div id="idResult"></div>
		<div class="wishListPopin">		
			<h2><spring:theme code="kacc.wishlist.add.wishlist.choice"/></h2>			
			<c:choose>
				<c:when test="${not empty wishlists}">
					<select id="idSelectOption" onchange="performSelectWishlistOption()" >
						 <option selected="selected" disabled="disabled" style="display:none;">
						 	<spring:theme code="kacc.wishlist.default.option.message" />
						 </option>
						<c:forEach items="${wishlists }" var="wishlist">
							<option class="classOption" value="${wishlist.name}">${wishlist.name}</option>
						</c:forEach>
					</select>
				</c:when>
				<c:otherwise>
					<p><spring:theme code="kacc.wishlist.empty.wishlist.message" /></p>
				</c:otherwise>
			</c:choose>	
			
			<c:if test="${fn:length(wishlists) lt 10}">
				<h2> <spring:theme code="kacc.wishlist.add.wishlist.message" /></h2>
				<div  class="inputStyle">				 
					<input id="idNewWish" name="idNewWish" type="text" class="required" placeholder="Nom de la liste" />					 
					<input id="idSubmitNewWish"  type="submit"  onclick="performSubmitNewWishlist()" value="<spring:theme code="kacc.wishlist.submit.wishlist.message" />" /> 
					<div class="errorValidation">
						<spring:theme code="kacc.wishlist.empty.wishlist.mandatory"/>
					</div>	
					<div class="errorSize">
						<spring:theme code="kacc.wishlist.empty.wishlist.sizeError"/>
					</div>
				</div>
			</c:if>
		</div>
	</c:when>
	<c:otherwise>				
		<div class="wishListPopin">
			<h2><spring:theme code="kacc.wishlist.login.wishlist.message" /></h2>
			<p><spring:theme code="kacc.wishlist.login.wishlist.message.partOne" />&nbsp;<a id="idLoginhref" href="${pageContext.request.contextPath}/login" onclick="performWishlistCookieValidation()" ><spring:theme code="kacc.wishlist.login.wishlist.link" /></a>&nbsp;<spring:theme code="kacc.wishlist.login.wishlist.message.partTwo" /></p> 
		</div>	
	</c:otherwise>
</c:choose>
