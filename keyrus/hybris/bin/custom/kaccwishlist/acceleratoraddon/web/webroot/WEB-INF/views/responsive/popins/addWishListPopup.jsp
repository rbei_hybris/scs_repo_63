<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<div id="wishListPopin" class="wishListPopin">
	<h1><spring:theme code="kacc.wishlist.account.name"/></h1>
	
	<div class="text">	
		<label for="idWishlistName"><spring:theme code="kacc.wishlist.account.name"/></label>
		<input type="text" placeholder="<spring:theme code="kacc.wishlist.account.name"/>" 
			class="required" name="idWishlistName" id="idWishlistName">	

		<div class="alert alert-danger errorValidationEmpty" style="display:none;">
			<p><spring:theme code="kacc.wishlist.empty.wishlist.mandatory"/></p>
		</div>
		<div class="alert alert-danger errorValidationSpace" style="display:none;">
			<p><spring:theme code="kacc.wishlist.account.add.newWishlist.error.space"/></p>
		</div>
		<div class="alert alert-danger errorValidationUnique" style="display:none;">
			<p><spring:theme code="kacc.wishlist.account.add.newWishlist.errorUnique"/></p>
		</div>
		<div class="alert alert-danger errorSizeMessage" style="display:none;">
			<p><spring:theme code="kacc.wishlist.account.add.newWishlist.maxSizeTen"/></p>
		</div>
		<div class="alert alert-danger errorMaxCharacter" style="display:none;">
			<p><spring:theme code="kacc.wishlist.empty.wishlist.sizeError"/></p>
		</div>
	</div>
	
	<div class="actions">
		<button type="button" class="js-wishlist-new-cancel button button--secondary">
			<spring:theme code="wishlist.remove.popup.cancelbutton"/>  
		</button>
		<button type="button" id="idWishlistSaveButton" class="js-wishlist-create button button--secondary" disabled="disabled">
			<spring:theme code="kacc.wishlist.account.submit.create" />
		</button>
	</div>
</div>
