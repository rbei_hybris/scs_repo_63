module.exports = function(grunt) {
	grunt.registerTask('buildJsHintTargets', 'task to generate the themes folder paths', function() {
		var path = require('path');
		var fs = require('fs');
		var pkg = grunt.config('pkg');
		var store = grunt.config('store');
		var ttSelected = store.theme.themeType.selected;
		var ttListed = store.theme.themeType.listed;
		var key = '',
			obj = {};
		store.src.js.jshint.targets = [];

		function msgColor(msg, severity) {
			var color = '';
			var wrapper = '';
			switch(severity) {
				case 1:
					color = 'yellow';
					wrapper = '\n' +
						'\t\t!!!!!!!!!!!!!!!!! ' [color].bold + msg[color].bold + ' !!!!!!!!!!!!!!!!!\n' [color].bold;
					break;
				case 2:
					color = 'red';
					wrapper = '\n' +
						'\t\t!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n' [color].bold +
						'\t\t ' [color].bold + msg[color].bold + ' \n' [color].bold +
						'\t\t!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n' [color].bold;
					break;
				default:
					color = 'green';
					wrapper = '\n' +
						'\t\t' [color].bold + msg[color].bold + '\n' [color].bold;
			}
			return wrapper;
		}

		if(ttSelected.indexOf('all') === -1) {

			//= Check if the json file with all the js configuration exists
			if(!fs.existsSync('config.js.' + ttSelected + '.json')) {
				grunt.log.writeln(msgColor('Info : File "config.js.' + ttSelected + '.json" doesn\'t exists !', 1));
				grunt.fail.warn(msgColor('Advice : create a file named "config.js.' + ttSelected + '.json" and see the CONFLUENCE documentation to get the file structure', 2));
			} else {
				//= Creation of the JS file list to compile
				key = ttSelected;
				obj[key] = grunt.file.readJSON('config.js.' + ttSelected + '.json');

				//= Get every theme bundle defined in the JS configuration file
				for(var themeBundle in obj[key]) {
					var bundle = obj[key][themeBundle].bundle;

					//= JShint targets building
					for(var i in bundle) {
						bundle[i] = store.baseSrc + bundle[i];
						//= Check if the file in the bundle already exists in the array
						if(store.src.js.jshint.targets.indexOf(bundle[i]) === -1) {
							store.src.js.jshint.targets.push(bundle[i]);
						}
					}

					//= JShint ignore targets building
					for(var j in obj[key][themeBundle].jshint.ignore) {
						//= Check if the file to ignore in the bundle already exists in the jshint ignore array
						if(store.src.js.jshint.ignore.indexOf(obj[key][themeBundle].jshint.ignore[j]) === -1) {
							// store.src.js.jshint.ignore.push(obj[key][themeBundle].jshint.ignore[j]);
							store.src.js.jshint.ignore.push(obj[key][themeBundle].jshint.ignore[j]);
						}
					}

				}
			}

		} else {
			for(var oneTheme in ttListed) {
				//= Check if the json file with all the js configuration exists
				if(!fs.existsSync('config.js.' + ttListed[oneTheme] + '.json')) {
					grunt.log.writeln(msgColor('Info : File "config.js.' + ttListed[oneTheme] + '.json" doesn\'t exists !', 1));
					grunt.fail.warn(msgColor('Advice : create a file named "config.js.' + ttListed[oneTheme] + '.json" and see the CONFLUENCE documentation to get the file structure', 2));
				} else {

					//= Creation of the JS file list to compile
					key = ttListed[oneTheme];
					obj[key] = grunt.file.readJSON('config.js.' + ttListed[oneTheme] + '.json');
					//= Get every theme bundle defined in the JS configuration file
					for(var thisThemeBundle in obj[key]) {
						var thisBundle = obj[key][thisThemeBundle].bundle;
						// grunt.log.writeln('>>>>>>>>>>> ' + JSON.stringify(obj[key][thisThemeBundle].jshint.ignore, null, 4));
						for(var k in thisBundle) {
							thisBundle[k] = thisBundle[k];

							//= Check if the file in the bundle already exists in the array
							if(store.src.js.jshint.targets.indexOf(thisBundle[k]) === -1) {
								store.src.js.jshint.targets.push(thisBundle[k]);
							}
						}

						for(var l in obj[key][thisThemeBundle].jshint.ignore) {
							//= Check if the file to ignore in the bundle already exists in the jshint ignore array
							if(store.src.js.jshint.ignore.indexOf(obj[key][thisThemeBundle].jshint.ignore[l]) === -1) {
								store.src.js.jshint.ignore.push(obj[key][thisThemeBundle].jshint.ignore[l]);
							}
						}
					}
				}
			}
		}

		for(var jsHintToIgnore in store.src.js.jshint.ignore) {
			store.src.js.jshint.targets.push('!' + store.baseSrc + store.src.js.jshint.ignore[jsHintToIgnore]);
		}

		grunt.config.set('store', store);
		grunt.config.set('jshint.default.src', store.src.js.jshint.targets);

		//= For debugging
		// if(grunt.option('debug')) {
		// 	console.log('==============');
		// 	console.log('store : \n\t\t');
		// 	console.log(JSON.stringify(grunt.config.get('jshint'), null, 4));
		// 	console.log('==============');
		// 	grunt.fail.warn('stop');
		// }

	});
};
