module.exports = function(grunt) {
	grunt.registerTask('buildLessTargets', 'task to generate the themes folder paths', function() {
		var pkg = grunt.config('pkg');
		var store = grunt.config('store');

		store.src.less.targets = [];
		store.src.less.lesshint.targets = [];
		// grunt.log.writeln(JSON.stringify(grunt.config('store.theme.typeChoices')));

		if(store.theme.themeType.selected.indexOf('all') === -1) {
			store.src.less.lesshint.targets.push( store.baseSrc + store.theme.themeType.selected + '/**/*.less');
			store.src.less.targets.push( store.theme.themeType.selected + '/**/' + store.src.less.mainFile);
		}else{
			for (var oneTheme in store.theme.themeType.listed) {
				store.src.less.lesshint.targets.push( store.baseSrc + store.theme.themeType.listed[oneTheme] + '/**/*.less');
				store.src.less.targets.push( store.theme.themeType.listed[oneTheme] + '/**/' + store.src.less.mainFile);
			}
		}

		//= Adding the files to ignore in the less task
		if(store.src.less.ignore.length > 0){
			for (var i in store.src.less.ignore) {
				store.src.less.targets.push( '!' + store.baseSrc + store.src.less.ignore[i] );
			}
		}
		
		//= Adding the files to ignore in the leshint task
		if(store.src.less.lesshint.ignore.length > 0){
			for (var j in store.src.less.lesshint.ignore) {
				store.src.less.lesshint.targets.push( '!' + store.baseSrc + store.src.less.lesshint.ignore[j] );
			}
		}


		grunt.config.set('store.src.less.targets',store.src.less.targets);
		grunt.config.set('store.src.less.lesshint.targets',store.src.less.lesshint.targets);
		// store.src.less.lesshint.targets.push.apply(store.src.less.lesshint.targets, store.src.less.lesshint.ignore);

		//= For debugging
		if (grunt.option('debug')) {
			console.log('==============');
			console.log('store : \n\t\t');
			console.log(JSON.stringify(grunt.config.get('less')));
			console.log('==============');
			grunt.fail.warn('stop');
		}

	});
};
