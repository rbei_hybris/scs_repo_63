module.exports = {
  options: {
    files: false,
    folder: true,
    depth: 1
  },
  files: {
    src: ['**', '!addons'],
    dest: '<%= pkg.grunt.generated.path %>info.themes.json',
    cwd: 'web/webroot/WEB-INF/_ui-src/'
  }
};
