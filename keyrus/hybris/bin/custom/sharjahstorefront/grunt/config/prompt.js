/*global require, module,  __dirname */
module.exports = {
  themeType: {
    options: {
      questions: [{
        config: 'store.theme.themeType.selected', // arbitrary name or config for any other grunt task
        type: 'list', // list, checkbox, confirm, input, password
        message: 'Choose the theme type :', // Question to ask the user, function needs to return a string,
        default: '<%= store.theme.themeType.default %>', // default value if nothing is entered
        choices: '<%= store.theme.themeType.choices %>'
        // validate: function(value){}, // return true if valid, error message if invalid. works only with type:input 
        // filter: function(value){}, // modify the answer
        // when: function(answers){} // only ask this question when this function returns true
      }]
    }
  },
};
