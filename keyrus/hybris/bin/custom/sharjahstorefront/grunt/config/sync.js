/*global require, module,  __dirname */
module.exports = {
    syncimages: {
        files: [{
            expand: true,
            cwd: '<%= store.baseSrc %>',
            src: ['**/themes/**/images/**/*.{gif,ico,jpg,png,svg}', '**/shared/**/*.{gif,ico,jpg,png,svg}', '**/common/**/*.{gif,ico,jpg,png,svg}'],
            dest: '<%= store.baseDist %>',
            rename: function(dest, src) {
                var nsrc = src;
                if (src.indexOf('common') === -1) {
                    nsrc = nsrc.replace(new RegExp("/themes/(.*)"), "/theme-$1/");
                } else {
                    nsrc = nsrc.replace(new RegExp("/themes/(.*)"), "/$1/");
                }
                return dest + nsrc;
            }
        }]
    },
    syncfonts: {
        files: [{
            expand: true,
            cwd: '<%= store.baseSrc %>',
            src: ['<%= store.src.fonts.fontsFolder %>**/*'],
            dest: '<%= store.baseDist %>',
            rename: function(dest, src) {
                var nsrc = src;
                if (src.indexOf('common') === -1) {
                    nsrc = nsrc.replace(new RegExp("/themes/(.*)"), "/theme-$1/");
                } else {
                    nsrc = nsrc.replace(new RegExp("/themes/(.*)"), "/$1/");
                }
                return dest + nsrc;
            }
        }]
    },
    synccss: {
        files: [{
            expand: true,
            cwd: '<%= store.baseSrc %>',
            src: ['**/shared/**/*.css', '**/common/**/*.css'],
            dest: '<%= store.baseDist %>',
            rename: function(dest, src) {
                var nsrc = src;
                if (src.indexOf('common') === -1) {
                    nsrc = nsrc.replace(new RegExp("/themes/(.*)"), "/theme-$1/");
                } else {
                    nsrc = nsrc.replace(new RegExp("/themes/(.*)"), "/$1/");
                }
                return dest + nsrc;
            }
        }]
    },
    syncjs: {
        files: [{
            expand: true,
            cwd: '<%= store.baseSrc %>',
            src: ['**/shared/**/*.js'],
            dest: '<%= store.baseDist %>',
            rename: function(dest, src) {
                var nsrc = src;
                if (src.indexOf('common') === -1) {
                    nsrc = nsrc.replace(new RegExp("/themes/(.*)"), "/theme-$1/");
                } else {
                    nsrc = nsrc.replace(new RegExp("/themes/(.*)"), "/$1/");
                }
                return dest + nsrc;
            }
        }]
    }
};
