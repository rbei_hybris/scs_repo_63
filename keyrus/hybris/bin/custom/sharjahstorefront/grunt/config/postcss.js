/*global require, module,  __dirname, console*/
module.exports = {
	dev: {
		// src: '<%=store.baseDist%>**/css/*.css',
		files: [{
			expand: true,
			cwd: '<%=store.baseDist%>',
			src: '**/css/style.css',
			dest: '<%= store.baseDist %>',
			ext: '<%= store.dist.css.nominExt %>'
		}],
		options: {
			map: {
				inline: true,
			},
			processors: [
				require('autoprefixer')({
					browsers: 'last 2 versions'
				}), // add vendor prefixes
				// require('cssnano')() // minify the result
			]
		}
	},
	dist: {
		// src: '<%=store.baseDist%>**/css/*.css',
		files: [{
			expand: true,
			cwd: '<%=store.baseDist%>',
			src: '**/css/style.min.css',
			dest: '<%= store.baseDist %>',
			ext: '<%= store.dist.css.minExt %>'
		}],
		options: {
			map: false,
			processors: [
				require('autoprefixer')({
					browsers: 'last 2 versions'
				}), // add vendor prefixes
				// require('cssnano')() // minify the result
			]
		}
	}
};
