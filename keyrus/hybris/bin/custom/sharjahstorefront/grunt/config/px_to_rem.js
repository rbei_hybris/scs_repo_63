/*global require, module,  __dirname, console*/
module.exports = {
	dev: {
		options: {
			base: 26,
			fallback: true,
			fallback_existing_rem: false,
			ignore: ['border', 'border-top', 'border-right', 'border-bottom', 'border-left', 'max-width'],
			map: {
				inline: true
			}
		},
		files: [{
			expand: true,
			cwd: '<%= store.baseDist %>',
			src: ['**/*<%= store.dist.css.nominExt %>', '!**/*<%= store.dist.css.minExt %>', '!**/addons/**/*<%= store.dist.css.nominExt %>', '!**/addons/**/*<%= store.dist.css.minExt %>'],
			dest: '<%= store.baseDist %>'
		}],
	},
	dist: {
		options: {
			base: 26,
			fallback: false,
			fallback_existing_rem: false,
			ignore: ['border', 'border-top', 'border-right', 'border-bottom', 'border-left', 'max-width']
		},
		files: [{
			expand: true,
			cwd: '<%= store.baseDist %>',
			src: ['!**/*<%= store.dist.css.nominExt %>', '**/*<%= store.dist.css.minExt %>', '!**/addons/**/*<%= store.dist.css.nominExt %>'],
			dest: '<%= store.baseDist %>'
		}],
	}
};
