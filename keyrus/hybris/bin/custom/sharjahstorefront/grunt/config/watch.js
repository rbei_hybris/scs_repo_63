/*global require, module,  __dirname */
module.exports = {
	less: {
		files: ['<%= store.baseSrc %>**/*.less', '!<%= store.baseSrc %>**/lib/**/*.less', '!<%= store.baseSrc %>**/addons/**/*'],
		tasks: ['buildcss']
	},
	fonts: {
		files: ['<%= store.baseSrc %>**/themes/**/fonts/*', '!<%= store.baseSrc %>**/addons/**/*'],
		tasks: ['sync:syncfonts']
	},
	js: {
		files: ['<%= store.baseSrc %>**/*.js', '!<%= store.baseSrc %>**/lib/**/*.js', '!<%= store.baseSrc %>**/addons/**/*'],
		tasks: ['buildjs']
	},
	images: {
		files: ['<%= store.baseSrc %>**/themes/**/*.{png,jpg,gif}', '!<%= store.baseSrc %>**/addons/**/*'],
		tasks: ['buildimg']
	},
	options: {
		livereload: true
	}
};
