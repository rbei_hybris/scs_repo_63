module.exports = {
	default: '<%=store.src.js.jshint.targets %>',
	options: {
		jshintrc: true,
		reporter: require('jshint-stylish')
	}
};
