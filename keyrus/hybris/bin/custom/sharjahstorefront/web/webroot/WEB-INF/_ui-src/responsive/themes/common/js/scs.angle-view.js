ACC.view360 = {

	/*_autoload: [
		"open360View"
	],*/
	open360View : function() {
		console.log("Open360Vew Popin Function");
		var content360View = $(".listImagesView360").html();
		content360View = '<div class="angle-view View360InPopin">' + content360View + '</div>';
		ACC.colorbox.open("", {
					open: true,
					current:'',
					title:'',
					html: content360View,
					close: '<span class="close"></span>',
					className: 'View360Popin',
					onComplete: function() {
						console.log('360 Angle View Product Details- loaded');
						$('.View360InPopin').angle({
						    speed: 1,
							previous: '.prev-image',
							next: '.next-image',
						});
					}
				});
	}
};
