<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/responsive/action" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>

<c:set var="isForceInStock" value="${product.stock.stockLevelStatus.code eq 'inStock' and empty product.stock.stockLevel}"/>
<c:choose> 
  <c:when test="${isForceInStock}">
    <c:set var="maxQty" value="FORCE_IN_STOCK"/>
  </c:when>
  <c:otherwise>
    <c:set var="maxQty" value="${product.stock.stockLevel}"/>
  </c:otherwise>
</c:choose>

<c:set var="qtyMinus" value="1" />

<div class="add-to-cart">
	<div class="add-to-cart--first-section">
		<c:if test="${not empty product.classifications}">
			<c:forEach items="${product.classifications}" var="classification">
				<c:if test="${fn:toLowerCase(classification.name) eq 'configuration'}">
					<c:if test="${not empty classification.features}">
						<div class="stock-wrapper add-to-cart--configuration clearfix">
							<p class="add-to-cart--configuration-title"><spring:theme code="addToCart.text.configuration"/></p>
							<div class="add-to-cart--configuration-select">
								<c:forEach items="${classification.features}" var="feature">
                        	<select id="productConfOption">
	                           <c:forEach items="${feature.featureValues}" var="featureValue" varStatus="status">
	                           	<option value="${fn:escapeXml(featureValue.value)}">${fn:escapeXml(featureValue.value)}</option>
	                           </c:forEach>
                        	</select>								
								</c:forEach>
							</div>
						</div>
					</c:if>
				</c:if>
			</c:forEach>
		</c:if>
		<c:if test="${product.stock.stockLevel gt 0}">
			<c:set var="productStockLevel"> <spring:theme code="product.variants.in.stock"/>
			</c:set>
		</c:if>
		<c:if test="${product.stock.stockLevelStatus.code eq 'lowStock'}">
			<c:set var="productStockLevel">
				<spring:theme code="product.variants.in.stock" />
			</c:set>
		</c:if>
		<c:if test="${not empty productStockLevel}">
			<div class="stock-wrapper add-to-cart--availability clearfix">
				<p class="add-to-cart--availability--stock">${productStockLevel}</p>
			</div>
		</c:if>
		<c:if test="${product.stock.stockLevelStatus.code eq 'outOfStock'}">
			<div class="stock-wrapper add-to-cart--availability clearfix">
				<p class="add-to-cart--out--stock"><spring:theme code="product.variants.out.of.stock"/></p>
			</div>
		</c:if>
		
	</div>
	<div class="add-to-cart--second-section">
		<c:if test="${empty showAddToCart ? true : showAddToCart}">
			<div class="add-to-cart--qty-selector">
				<%-- QTY Selector --%>
				<c:if test="${!isForceInStock && not empty product.stock.stockLevel && product.stock.stockLevel > 0}">
					<product:productQuantitySelector product="${product}" />
				</c:if>
			</div>
		</c:if>

<%-- 		<c:if test="${!isForceInStock && not empty product.stock.stockLevel && product.stock.stockLevel > 0}"> --%>
<!-- 			<div class="stock-wrapper add-to-cart--availability clearfix"> -->
<!-- 				<p class="add-to-cart--availability--limits"> -->
<%-- 					<spring:theme code="product.variants.quantity.limit" arguments="${not empty product.stock.stockLevel ? product.stock.stockLevel : 0}"/>					 --%>
<!-- 				</p> -->
<!-- 			</div> -->
<%-- 		</c:if>  --%>
	
		 <div class="add-to-cart--actions">
        <c:if test="${multiDimensionalProduct}" >
                <c:url value="${product.url}/orderForm" var="productOrderFormUrl"/>
                <a href="${productOrderFormUrl}" class="btn btn-default btn-block btn-icon js-add-to-cart glyphicon-list-alt">
                    <spring:theme code="order.form" />
                </a>
        </c:if>
        <action:actions element="div"  parentComponent="${component}"/> 
    	</div>
	</div>
</div>
