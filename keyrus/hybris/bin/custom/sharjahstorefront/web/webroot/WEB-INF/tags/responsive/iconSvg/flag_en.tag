<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%--
 SVG EN flag icon
--%>
<?xml version="1.0" encoding="utf-8"?>
<!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="flag_en" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 960 480" style="enable-background:new 0 0 960 480;" xml:space="preserve">
	<style type="text/css">
	.st0 {
		fill: #000066;
	}
	.st1 {
		fill: #FFFFFF;
	}
	.st2 {
		fill: #CC0000;
	}

	</style>
	<rect x="160" class="st0" width="640" height="480" />
	<path class="st1" d="M160,346.3v107.3l320-160L372.7,240L160,346.3z M480,293.7l320,160V346.3L587.3,240L480,293.7z M480,186.3
	L587.3,240L800,133.7V26.3L480,186.3z M160,133.7L372.7,240L480,186.3l-320-160V133.7z" />
	<polygon class="st1" points="400,0 400,160 160,160 160,320 400,320 400,480 560,480 560,320 800,320 800,160 560,160 560,0 " />
	<polygon class="st2" points="432,0 432,192 160,192 160,288 432,288 432,480 528,480 528,288 800,288 800,192 528,192 528,0 " />
	<path class="st2" d="M320,320l-160,80v35.8L391.6,320H320z M640,320l160,80v-35.8L711.6,320H640z M160,115.8l88.5,44.2H320L160,80
	V115.8z M568.5,160H640l160-80V44.2L568.5,160z" />
</svg>
