<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="addonProduct" tagdir="/WEB-INF/tags/addons/kaccwishlist/responsive/product" %>

<c:if test="${not product.multidimensional }">
	<c:url value="/cart/add" var="addToCartUrl" />
	<c:url value="${product.url}/configuratorPage/${configuratorType}"
		var="configureProductUrl" />

	<c:set var="isForceInStock"
		value="${product.stock.stockLevelStatus.code eq 'inStock' and empty product.stock.stockLevel}" />
	<c:choose>
		<c:when test="${isForceInStock}">
			<c:set var="maxQty" value="FORCE_IN_STOCK" />
		</c:when>
		<c:otherwise>
			<c:set var="maxQty" value="${product.stock.stockLevel}" />
		</c:otherwise>
	</c:choose>
	<c:set var="qtyMinus" value="1" />


	<form:form id="addToCartForm${product.code}" action="${addToCartUrl}"
		method="post" class="add_to_cart_form">

		<c:if test="${empty showAddToCart ? true : showAddToCart}">
			<%-- QTY Selector --%>
			<product:productQuantitySelector product="${product}" />

	<%-- 			<c:if test="${!isForceInStock}">
					<div><spring:theme code="product.variants.quantity.limit" arguments="${maxQty}" /></div>
				</c:if> --%>
		</c:if>
		<c:if test="${product.stock.stockLevel gt 0}">
			<c:set var="productStockLevel">${product.stock.stockLevel}&nbsp;
				<spring:theme code="product.variants.in.stock" />
			</c:set>
		</c:if>
		<c:if test="${product.stock.stockLevelStatus.code eq 'lowStock'}">
			<c:set var="productStockLevel">
				<spring:theme code="product.variants.in.stock" />
			</c:set>
		</c:if>
		<c:if test="${isForceInStock}">
			<c:set var="productStockLevel">
				<spring:theme code="product.variants.available" />
			</c:set>
		</c:if>
		<!-- <div class="stock-wrapper clearfix">${productStockLevel}</div> -->

		<c:if test="${multiDimensionalProduct}">
			<c:url value="${product.url}/orderForm" var="productOrderFormUrl" />
			<a href="${productOrderFormUrl}"
				class="btn btn-default btn-block btn-icon js-add-to-cart glyphicon-list-alt">
				<spring:theme code="order.form" />
			</a>
		</c:if>

        <div class="add-button">
	        <button class="add-button-whishlist">
	        	<addonProduct:productAddToWishlistButton product="${product}" />
	        </button>
			<ycommerce:testId code="addToCartButton">
				<input type="hidden" name="productCodePost" value="${product.code}" />
				<input type="hidden" name="productNamePost"
					value="${fn:escapeXml(product.name)}" />
				<input type="hidden" name="productPostPrice"
					value="${product.price.value}" />

				<c:choose>
					<c:when
						test="${product.stock.stockLevelStatus.code eq 'outOfStock' }">
						<button type="submit"
							class="add-button-toCart"
							aria-disabled="true" disabled="disabled">
							<span><spring:theme code="text.addToCart" /></span>
						</button>
					</c:when>
					<c:otherwise>
						<button type="submit"
							class="add-button-toCart js-enable-btn"
							disabled="disabled">
							<span><spring:theme code="text.addToCart" /></span>
						</button>
					</c:otherwise>
				</c:choose>
			</ycommerce:testId>
		</div>
	</form:form>

	<form:form id="configureForm${product.code}"
		action="${configureProductUrl}" method="get" class="configure_form">
		<c:if test="${product.configurable}">
			<c:choose>
				<c:when
					test="${product.stock.stockLevelStatus.code eq 'outOfStock' }">
					<button id="configureProduct" type="button"
						class="btn btn-primary btn-block" disabled="disabled">
						<spring:theme code="basket.configure.product" />
					</button>
				</c:when>
				<c:otherwise>
					<button id="configureProduct" type="button"
						class="btn btn-primary btn-block js-enable-btn"
						disabled="disabled"
						onclick="location.href='${configureProductUrl}'">
						<spring:theme code="basket.configure.product" />
					</button>
				</c:otherwise>
			</c:choose>
		</c:if>
	</form:form>
</c:if>
