ACC.deliveryarea = {

	_autoload: [
		"fillCitiesList",
		"updateAreaDropDown"
	],
	citiesList:[],
	fillCitiesList:function(){
		if($("#address\\.townCity").length){
			console.log('scs.deliveryarea.js - ACC.deliveryarea.fillCitiesList() : executed');
			$.ajax({
				url: ACC.config.encodedContextPath + '/my-account/address-area-list',
				async: true,
				dataType: "json"
			}).done(function(data) {
				console.log('scs.deliveryarea.js - ACC.deliveryarea.fillCitiesList() - Ajax /my-account/address-area-list : success');
				if(data){
					ACC.deliveryarea.citiesList = data;
					var length = $('#address\\.townCity').children('option').length;
					if(length === 2 && $("#address\\.townCity option").is(":selected"))
					{
						ACC.deliveryarea.getAreaDropDownList("#address\\.state",$("#address\\.townCity option:selected").val());
					}
				}
			});	
		}
	},
	updateAreaDropDown:function (){
		console.log('scs.deliveryarea.js - ACC.deliveryarea.updateAreaDropDown() : executed');
		$(document).on("change",'#address\\.townCity',function() {
			console.log('scs.deliveryarea.js - ACC.deliveryarea.updateAreaDropDown() - Change event triggered');
			ACC.deliveryarea.getAreaDropDownList("#address\\.state",$(this).val());
		});
	},
	getAreaDropDownList:function(areaPath,cityCode)
	{
		console.log('scs.deliveryarea.js - ACC.deliveryarea.getAreaDropDownList() : executed');
		$(areaPath).empty();
		var areaList = [];
		console.log('scs.deliveryarea.js - ACC.deliveryarea.getAreaDropDownList() - fetching areas');
		$.each(ACC.deliveryarea.citiesList,function(index,item){
			if(cityCode === item.code)
			{
				areaList = item.areas;
			}
	    });
		
		var selected;
		if( $(areaPath).parent().find("#selected-value").length){
			selected = $(areaPath).parent().find("#selected-value").attr("selected-value");
		}
		
		console.log('scs.deliveryarea.js - ACC.deliveryarea.getAreaDropDownList() - updating html');
		$.each(areaList,function(index,item){
			if(selected !== null && selected === item.code){
				$(areaPath).append($("<option selected></option>").attr("value", item.code).text(item.name));
			} else {
				$(areaPath).append($("<option></option>").attr("value", item.code).text(item.name));
			}
	    });
	}
};
