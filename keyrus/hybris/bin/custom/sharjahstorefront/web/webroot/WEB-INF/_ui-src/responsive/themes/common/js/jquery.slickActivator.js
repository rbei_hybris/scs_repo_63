(function ($) {
	$.fn.slickActivator = function () {
		console.log('jquery.slickActivator.js - loaded', $(this).attr('class'));

		var jsSlickMobileDots = (typeof $(this).data('carousel-mobile-dots') !== 'undefined' && $(this).data('carousel-mobile-dots') === true ? true : false);
		var jsSlickMobileAutoplay = (typeof $(this).data('carousel-mobile-autoplay') !== 'undefined' && $(this).data('carousel-mobile-autoplay') === true ? true : false);
		var jsSlickMobileArrows = (typeof $(this).data('carousel-mobile-arrows') !== 'undefined' && $(this).data('carousel-mobile-arrows') === true ? true : false);
		var jsSlickMobileItems = (typeof $(this).data('carousel-mobile-items') !== 'undefined' ? $(this).data('carousel-mobile-items') : 1);
		var isCenterModeMobile = (typeof $(this).data('carousel-mobile-center') !== 'undefined' && $(this).data('carousel-mobile-center') === true ? true : false);

		var jsSlickDesktopDots = (typeof $(this).data('carousel-desktop-dots') !== 'undefined' && $(this).data('carousel-desktop-dots') === true ? true : false);
		var jsSlickDesktopArrows = (typeof $(this).data('carousel-desktop-arrows') !== 'undefined' && $(this).data('carousel-desktop-arrows') === true ? true : false);
		var jsSlickDesktopItems = (typeof $(this).data('carousel-desktop-items') !== 'undefined' ? $(this).data('carousel-desktop-items') : 1);
		var isCenterModeDesktop = (typeof $(this).data('carousel-desktop-center') !== 'undefined' && $(this).data('carousel-desktop-center') === true ? true : false);

		var jsWidthXs = (typeof $(this).data('carousel-width-xs') !== 'undefined' && $(this).data('carousel-width-xs') === true ? true : false);
		var jsWidthSm = (typeof $(this).data('carousel-width-sm') !== 'undefined' && $(this).data('carousel-width-sm') === true ? true : false);
		var jsWidthLg = (typeof $(this).data('carousel-width-lg') !== 'undefined' && $(this).data('carousel-width-lg') === true ? true : false);
		var jsArrowsColor = ($(this).data('carousel-arrow-color') !== 'undefined' && typeof $(this).data('carousel-arrow-color') === 'string' ? $(this).data('carousel-arrow-color') : 'black');
		var jsDir = ($(this).data('carousel-dir') !== 'undefined' && $(this).data('carousel-dir') === 'rtl' ? true : false);
		var jsSlickAutoplay = (typeof $(this).data('carousel-autoplay') !== 'undefined' && $(this).data('carousel-autoplay') === true ? true : false);
		var jsSlickSpeed = (typeof $(this).data('carousel-speed') !== 'undefined' ? Number($(this).data('carousel-speed')) : 500);
		var customPrevArrow = '<div class="a-left control-c prev slick-prev"><img src="' + ACC.config.themeResourcePath + '/images/slick/arrow-left-' + jsArrowsColor + '.png" /></div>';
		var customNextArrow = '<div class="a-right control-c next slick-next"><img src="' + ACC.config.themeResourcePath + '/images/slick/arrow-right-' + jsArrowsColor + '.png" /></div>';

		var jsSlickConfig = {
			autoplay: jsSlickAutoplay,
			// cssEase: 'linear',
			centerPadding: '0',
			rtl: jsDir,
			mobileFirst: true,
			// infinite: true,
			prevArrow: customPrevArrow,
			nextArrow: customNextArrow,
			responsive: [{
				breakpoint: 0,
				settings: {
					arrows: jsSlickMobileArrows,
					centerMode: isCenterModeMobile,
					dots: jsSlickMobileDots,
					slidesToShow: jsSlickMobileItems,
					variableWidth: jsWidthSm
				}
			},
			{
				breakpoint: 1023,
				settings: {
					arrows: jsSlickDesktopArrows,
					centerMode: isCenterModeDesktop,
					dots: jsSlickDesktopDots,
					slidesToShow: jsSlickDesktopItems,
					variableWidth: jsWidthLg
				}
			}
			],
			slidesToScroll: 1,
			speed: jsSlickSpeed
		};

		console.log(jsSlickConfig);
		if (!$(this).parents('.js-carousel-disabled').length) {
			$(this).slick(jsSlickConfig);
		} else {
			$(this).addClass('not-slicked');
		}
	};
})(jQuery);
