ACC.wishlist = {

	NEW: "newWishlist",
	NONE: "noWishlist",

	/**
	 * The currently entered quantity for the selected product to add to a wishlist.
	 */
	quantity: 1,

	_autoload: [
		"bindWishlist"
	],

	bindWishlist: function() {
		console.log('scs.wishlist.js - ACC.wishlist.bindWishlist : executed');

		ACC.wishlist.selectedByDefault();
		ACC.wishlist.updateTotalPrice();

		// Account wishlist & wishlist detail pag

		$('.js-wishlist-click').on('click', function(e) {
			e.preventDefault();
			ACC.wishlist.performChooseWishlistValue($(this));
		});

		$('.js-wishlist-new').on('click', function(e) {
			e.preventDefault();
			ACC.wishlist.openNewWishlistPopin();
		});

		$('.js-add-product-to-wishlist').on('click', function(e) {
			e.preventDefault();
			ACC.wishlist.openAddProductToWishlistPopup(this);
		});

		$('.js-wishlist-rename').on('click', function(e) {
			e.preventDefault();
			ACC.wishlist.openRenameWishlistPopin();
		});

		$('.js-wishlist-share').on('click', function(e) {
			e.preventDefault();
			ACC.wishlist.openShareWishlistPopin();
		});

		$('body').on('keyup', '#idWishlistName', function(e) {
			e.preventDefault();
			ACC.wishlist.wishlistNameValidator();
		});

		$('body').on('click', '.js-wishlist-new-cancel', function(e) {
			e.preventDefault();
			ACC.colorbox.close();
		});

		$('body').on('click', '.js-wishlist-new-send', function(e) {
			var receiverName = $('#receiverName').val();
			var codewishlist = $('#codewishlist').val();
			var receiverEmail = $('#receiverEmail').val();
			var url = ACC.config.encodedContextPath + "/my-account/wishlist/share/?wishlitCode=" + codewishlist + "&receiverName=" + receiverName + "&receiverEmail=" + receiverEmail;
			$.ajax({
				url: url,
				data: {},
				type: 'POST',
				success: function() {
					ACC.colorbox.close();
				},
				error: function(e) {
					console.log(e.message);
				}
			});
		});

		$('body').on('click', '.js-wishlist-create', function(e) {
			e.preventDefault();
			ACC.wishlist.performSaveWishlistFromAccount('INSERT');
		});

		$('body').on('click', '.js-wishlist-rename-confirm', function(e) {
			e.preventDefault();
			ACC.wishlist.performSaveWishlistFromAccount('UPDATE');
		});

		$(".js-wishlist-delete").on('click', function(e) {
			e.preventDefault();
			ACC.wishlist.openDeleteWishlistPopin($(this));
		});

		$('body').on('click', '.js-wishlist-delete-confirm', function(e) {
			e.preventDefault();
			ACC.wishlist.performDeleteWishlist();
		});

		$('body').on('click', '.js-wishlist-delete-cancel', function(e) {
			e.preventDefault();
			ACC.colorbox.close();
		});

		$('.js-wishlist-back').on('click', function(e) {
			e.preventDefault();
			$(location).attr('href', ACC.config.encodedContextPath + '/my-account/wishlist');
		});

		$('.js-wishlist-entry-delete').on('click', function(e) {
			e.preventDefault();
			ACC.wishlist.openDeleteWishlistEntryPopin($(this));
		});

		$('body').on('click', '.js-wishlist-entry-delete-confirm', function(e) {
			e.preventDefault();
			ACC.wishlist.performRemoveWishlistEntry();
		});

		$('body').on('click', '.js-wishlist-entry-delete-cancel', function(e) {
			e.preventDefault();
			ACC.colorbox.close();
		});

		$('.js-wishlist-entry-check-all').change(function() {
			$('input:checkbox').prop('checked', $(this).prop('checked'));
			ACC.wishlist.disableAddToCartButton();
		});

		$('.js-entry-checkbox').change(function() {
			ACC.wishlist.disableAddToCartButton();
		});

		$('.my-account-wishlist .js-wishlist-add-to-cart').on('click', function(e) {
			e.preventDefault();
			ACC.wishlist.performAddEntriesTocart($(this));
		});

		$('.my-account-wishlist .js-qty-selector-input').on('change', function(e) {
			ACC.wishlist.updateSubtotalPrice($(this));
		}).triggerHandler('change');


		// addProductWishlistPopup
		if ($.trim($(".promo-banniere").text()) === "") {
			$(".promo-banniere").hide();
		}

		// Select a default address in customer account
		$('.fact-liv:not(.n-livrer)').click(function() {
			if ($(this).parent('.facturation').index() === 1) {
				$('#factAddress').val($(this).attr("id").substring(5));
			}
			if ($(this).parent('.facturation').index() === 2) {
				$('#livAddress').val($(this).attr("id").substring(4));
			}
			$(this).addClass('checked');
		});

		var bodyClass = $('body').attr('class');
		if (bodyClass) {
			var nclass = bodyClass.split(" ");
			if (ACC.wishlist.getPageName(nclass)) {
				var cookieValue = ACC.wishlist.readCookie('flagwishlist');
				if (cookieValue === 'true') {
					ACC.colorbox.open("", {
						href: $("#idFav").attr('href'),
						resize: true
					});

					ACC.colorbox.resize();
					ACC.wishlist.eraseCookie('flagwishlist');
				}
			}
		}
	},

	selectedByDefault: function() {
		console.log('scs.wishlist.js - ACC.wishlist.selectedByDefault : executed');
		// force checkbox hecked by default in wishlist page
		$('.my-account-wishlist input:checkbox').prop('checked', true);
	},

	/*--- 			----			--*/
	/* --  addProductWishlistPopup -- */
	/*--- 			----			--*/

	performWishlistCookieValidation: function() {
		console.log('scs.wishlist.js - ACC.wishlist.performWishlistCookieValidation : executed');
		var cookieValue = ACC.wishlist.readCookie('flagwishlist');
		if (cookieValue === 'false') {
			ACC.wishlist.createCookie('flagwishlist', 'true', 1);
			ACC.wishlist.readCookie('flagwishlist');
		}
	},

	bindAddProductPopup: function() {
		console.log('scs.wishlist.js - ACC.wishlist.bindAddProductPopup : executed');
		$(".wishlistError").hide();
		ACC.wishlist.refreshAddProductPopup();
		ACC.colorbox.resize();
		if ($("#idSelectOption").val() === ACC.wishlist.NEW) {
			$("#newWishlistSection").removeClass("hidden");
			$("#idNewWish").focus();
			ACC.colorbox.resize();
		}

		$("#wishlistCancel").click(function() {
			ACC.colorbox.close();
		});

		$("#idSelectOption").change(function() {
			if ($(this).val() === ACC.wishlist.NEW) {
				console.debug("Selected New shopping list");
				$("#newWishlistSection").removeClass("hidden");
				$("#idNewWish").focus();
				ACC.colorbox.resize();
			}
			ACC.wishlist.refreshAddProductPopup();
		});

		$("#idNewWish").on("change, keyup", function() {
			ACC.wishlist.refreshAddProductPopup();
		});

		$("#idSubmitNewWish").click(function() {
			var selection = $("#idSelectOption").val();
			if (selection === ACC.wishlist.NEW) {
				ACC.wishlist.performSubmitNewWishlist();
			} else if (selection !== ACC.wishlist.NONE) {
				ACC.wishlist.performSelectWishlistOption();
			}
		});

		$("#idLoginhref").click(function() {
			ACC.wishlist.performWishlistCookieValidation();
		});
		SCS.main.removeColorboxTitle();
	},

	refreshAddProductPopup: function() {
		console.log('scs.wishlist.js - ACC.wishlist.refreshAddProductPopup : executed');
		var submitButton = $("#idSubmitNewWish");

		switch ($("#idSelectOption").val()) {
			case ACC.wishlist.NONE:
				submitButton.text(submitButton.data("add-label"));
				submitButton.attr("disabled", true);
				break;
			case ACC.wishlist.NEW:
				submitButton.text(submitButton.data("create-label"));
				submitButton.attr("disabled", $('#idNewWish').val().length === 0);
				break;
			default:
				submitButton.text(submitButton.data("add-label"));
				submitButton.attr("disabled", false);
		}
	},

	generateAddProductParams: function() {
		console.log('scs.wishlist.js - ACC.wishlist.generateAddProductParams : executed');
		var selectedList = $("#idSelectOption").val();
		var config = "";
		if (selectedList === ACC.wishlist.NEW) {
			selectedList = $("#idNewWish").val();
		}
		if ($("#productConfOption").length > 0) {
			config = $("#productConfOption").val();
		}
		var params = {
			productCode: $("#productCode").val(),
			quantity: ACC.wishlist.quantity,
			wishlistName: selectedList,
			listFromCart: $("#listFromCart").val(),
			configProduct: config
		};
		return $.param(params);
	},

	performSelectWishlistOption: function() {
		console.log('scs.wishlist.js - ACC.wishlist.performSelectWishlistOption : executed');
		ACC.wishlist.addProductToWishList("/wishlist/addProductWishlist/?" + ACC.wishlist.generateAddProductParams());
	},

	performSubmitNewWishlist: function() {
		console.log('scs.wishlist.js - ACC.wishlist.performSubmitNewWishlist : executed');
		if ($("#idNewWish").val().length === 0) {
			$("#idNewWish").addClass("errorInput");
			$(".errorValidation").show();
			ACC.colorbox.resize();
			return false;
		} else if ($("#idNewWish").val().length > 50) {
			$("#idNewWish").addClass("errorInput");
			$(".errorSize").show();
			ACC.colorbox.resize();
			return false;
		} else {
			ACC.wishlist.addNewWishList("/wishlist/addNewWishlist/?" + ACC.wishlist.generateAddProductParams());
			return true;
		}
	},

	/* --  Select a wishlist	--  */
	performChooseWishlistValue: function($element) {
		console.log('scs.wishlist.js - ACC.wishlist.performChooseWishlistValue : executed');
		var listName = $element.data('wishlistItemName');
		var toUrl = ACC.config.encodedContextPath + '/my-account/wishlist/detail?wishlistParam=' + listName;
		$(location).attr('href', toUrl);
	},

	/*--- New Wishlist Popin --*/
	openNewWishlistPopin: function() {
		console.log('scs.wishlist.js - ACC.wishlist.openNewWishlistPopin : executed');
		var title = $('.js-wishlist-new').attr('title');
		var url = ACC.config.encodedContextPath + '/wishlist/addwishlist';
		ACC.colorbox.open(title, {
			open: true,
			href: url,
			width: "600px",
			close: '<span class="close"></span>',
			maxWidth: "90%",
			maxHeight: "90%",
			onComplete: function() {
				$('#wishListPopin p:first').html(title);
				$('#cboxTitle').css({
					'height': 0,
					'padding': 0
				});
			}
		});
		ACC.wishlist.createCookie('flagwishlist', 'false', 1);
	},

	/*--- Rename Wishlist Popin --*/
	openRenameWishlistPopin: function() {
		console.log('scs.wishlist.js - ACC.wishlist.openRenameWishlistPopin : executed');
		var title = $('.js-wishlist-rename').attr('title');
		var listName = $('#wishlistName').val();
		var url = ACC.config.encodedContextPath + '/wishlist/renamewishlist';
		ACC.colorbox.open(title, {
			open: true,
			href: url,
			width: "600px",
			close: '<span class="close"></span>',
			maxWidth: "90%",
			maxHeight: "90%",
			onComplete: function() {
				$('#idWishlistName').val(listName);
				SCS.main.removeColorboxTitle();
			}
		});
	},

	/*--- Share Wishlist Popin --*/
	openShareWishlistPopin: function() {
		console.log('scs.wishlist.js - ACC.wishlist.openShareWishlistPopin : executed');
		var listName = $('#wishlistName').val();
		var url = ACC.config.encodedContextPath + '/my-account/wishlist/share/?wishlitCode=' + listName;
		$.ajax({
			url: url,
			type: "GET",
			success: function(data) {
				ACC.colorbox.open("", {
					open: true,
					html: data,
					close: '<span class="close"></span>',
					maxWidth: '90%',
					maxHeight: '90%',
					width: "600px",
					height: "400px"
				});
			},
			error: function(xht, textStatus, ex) {}
		});
	},

	/**
	 * Add Product To Wishlist Pop-up from PDP/PLP/SRP/Carousel.
	 */
	openAddProductToWishlistPopup: function(self) {
		console.log('scs.wishlist.js - ACC.wishlist.openAddProductToWishlistPopup : executed');

		if (document.location.href.indexOf('/p/') > -1) {
			ACC.wishlist.quantity = ACC.wishlist.searchQuantity(self, ".addtocart-component");
		} else if (document.location.href.indexOf('/search/') > -1) {
			ACC.wishlist.quantity = ACC.wishlist.searchQuantity(self, ".actions-container-for-SearchResultsGrid");
		} else {
			ACC.wishlist.quantity = ACC.wishlist.searchQuantity(self, ".carousel__item");
		}

		var url = $(self).data("addProductToWishlistUrl");
		var title = $(self).data("title");

		ACC.colorbox.open("", {
			href: url,
			title: title,
			onComplete: ACC.wishlist.bindAddProductPopup,
			width: "400px",
			height: "200px",
			maxWidth: "90%",
			maxHeight: "90%"
		});
	},

	/**
	 * Search the quantity set in the quantity selector corresponding to a specific Add to Wishlist button
	 */
	searchQuantity: function(clickedElement, code) {
		var quantity = $(clickedElement).parents(code).find(".js-qty-selector-input").val();
		if (!quantity) {
			console.warn("No quantity found, setting to 1");
			quantity = 1;
		}
		return quantity;
	},

	/*--- disable the create button if name is empty --*/
	wishlistNameValidator: function() {
		console.log('scs.wishlist.js - ACC.wishlist.wishlistNameValidator : executed');
		if ($('#idWishlistName').val().length === 0) {
			$('body').find('#idWishlistSaveButton').prop('disabled', true);
		} else {
			$('body').find('#idWishlistSaveButton').prop('disabled', false);
		}
	},

	/* -- Save wishlist (create & rename) -- */
	performSaveWishlistFromAccount: function(mode) {
		console.log('scs.wishlist.js - ACC.wishlist.performSaveWishlistFromAccount : executed');
		if ($('#idWishlistName').val().length === 0) {
			$("#idWishlistName").addClass("errorInput");
			$(".errorValidationEmpty").show();
			ACC.colorbox.resize();
			return false;
		} else if ($('#idWishlistName').val().length > 50) {
			$("#idWishlistName").addClass("errorInput");
			$(".errorMaxCharacter").show();
			ACC.colorbox.resize();
			return false;
		} else {
			ACC.wishlist.saveWishlist(mode);
			return true;
		}
	},

	saveWishlist: function(mode) {
		console.log('scs.wishlist.js - ACC.wishlist.saveWishlist : executed');
		var wishlistName = $('#idWishlistName').val();
		var url;
		if (mode === 'INSERT') {
			url = '/wishlist/addwishlist/?wishlistName=' + wishlistName;
		} else {
			var oldWishlistName = $('#wishlistName').val();
			url = '/wishlist/renamewishlist/?wishlistName=' + oldWishlistName + '&newWishlistName=' + wishlistName;
		}

		$.ajax({
			url: ACC.config.encodedContextPath + url,
			type: "POST",
			success: function(data) {
				ACC.wishlist.showSaveWishlistResult(data, mode, wishlistName);
			}
		});
	},

	showSaveWishlistResult: function(data, mode, wishlistName) {
		console.log('scs.wishlist.js - ACC.wishlist.showSaveWishlistResult : executed');
		if (data.error) {
			$("#idWishlistName").addClass("errorInput");
			$('.errorValidationEmpty').hide();
			$('.errorValidationSpace').hide();
			ACC.colorbox.resize();

			var input = $("#idWishlistName").val();
			if (data.message === 'maxSize') {
				$(".errorSizeMessage").show();
				$(".errorValidationUnique").hide();
				ACC.colorbox.resize();
			} else {
				if (input === " ") {
					$(".errorValidationUnique").hide();
					$(".errorValidationSpace").show();
				} else {
					$(".errorValidationUnique").show();
					$(".errorSizeMessage").hide();
				}
				ACC.colorbox.resize();
			}
		} else {
			var redirectUrl;
			if (mode === 'INSERT') {
				redirectUrl = ACC.config.encodedContextPath + '/my-account/wishlist';
			} else {
				redirectUrl = ACC.config.encodedContextPath + '/my-account/wishlist/detail?wishlistParam=' + wishlistName;
			}
			$(location).attr('href', redirectUrl);
			ACC.colorbox.resize();
		}
	},

	/*--- Delete Wishlist Popin --*/
	openDeleteWishlistPopin: function($element) {
		console.log('scs.wishlist.js - ACC.wishlist.openDeleteWishlistPopin : executed');
		var listName = $element.data('wishlistItemName');
		$('#wishlistName').val(listName);

		var html = $('#whishConfirmationMssg').html();
		html = html.replace('{0}', '<b>' + listName + '</b>');
		$('.js-wishlist-delete-confirm')[0].setAttribute("id", listName);
		ACC.colorbox.open("", {
			open: true,
			html: html,
			width: "600px",
			close: '<span class="close"></span>',
			maxWidth: '90%',
			maxHeight: '90%',
			onComplete: SCS.main.removeColorboxTitle
		});
	},

	/* -- Delete wishlist -- */
	performDeleteWishlist: function() {
		console.log('scs.wishlist.js - ACC.wishlist.performDeleteWishlist : executed');
		var selectedValue = $('.js-wishlist-delete-confirm')[0].id;
		$.ajax({
			url: ACC.config.encodedContextPath + '/wishlist/remove/',
			type: "POST",
			data: 'wishlistName=' + selectedValue,
			success: function(data) {
				ACC.colorbox.close();
				window.location = ACC.config.encodedContextPath + '/my-account/wishlist/';
			},
			error: function(xht, textStatus, ex) {

			}
		});
	},

	/* -- Delete wishlist Entry Popin -- */
	openDeleteWishlistEntryPopin: function($element) {
		console.log('scs.wishlist.js - ACC.wishlist.openDeleteWishlistEntryPopin : executed');
		var productCode = $element.data('productCode');
		var wishlistName = $element.data('wishlistName').replace(' ', '%20');
		var url = ACC.config.encodedContextPath + '/wishlist/askremove/?productCode=' + productCode + '&wishlistName=' + wishlistName;
		ACC.colorbox.open("", {
			open: true,
			href: url,
			width: "600px",
			close: '<span class="close"></span>',
			maxWidth: '90%',
			maxHeight: '90%',
			onComplete: SCS.main.removeColorboxTitle
		});
	},

	/* -- Delete wishlist Entry -- */
	performRemoveWishlistEntry: function() {
		console.log('scs.wishlist.js - ACC.wishlist.performRemoveWishlistEntry : executed');
		var productCode = $('#idproductCode').attr('value');
		var wishlistName = $('#idwishlistName').attr('value');
		var url = ACC.config.encodedContextPath + '/wishlist/removeEntry/?productCode=' + productCode + '&wishlistName=' + wishlistName;
		$(location).attr('href', url);
	},

	/* -- Add selected entries to cart -- */
	performAddEntriesTocart: function($element) {
		console.log('scs.wishlist.js - ACC.wishlist.performAddEntriesTocart : executed');
		var listProduct = [];

		// product checkbox
		var listProductCode = [];
		$('input.js-entry-checkbox:checkbox').each(function() {
			if (this.checked) {
				listProductCode.push("" + $(this).attr('id'));
			}
		});
		// product quantity
		$('input.js-qty-selector-input:text').each(function() {
			var productCode = $(this).data('product').toString();
			for (var key in listProductCode) {
				if (listProductCode[key] === productCode) {
					var selectConfig = "#productConfOption" + productCode;
					var configValue = "";
					if ($(selectConfig).length) {
						configValue = $(selectConfig).val();
					}
					var product = { 'productCode': productCode.toString(), 'quantity': parseFloat($(this).val()), 'configuration': configValue.toString() };
					listProduct.push(product);
				}
			}
		});

		var url = ACC.config.encodedContextPath + '/add-wish-to-cart/add';

		$.ajax({
			url: url,
			type: "POST",
			data: JSON.stringify(listProduct),
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			success: function(data) {
				data.productsNotAdded.forEach(function(entry) {
					$('.' + entry.productCode + '-wl-entry').show();
				});
				data.productsAdded.forEach(function(entry) {
					$('.' + entry.productCode + '-wl-entry').hide();
				});

				if (!$.isArray(data.productsAdded) || !data.productsAdded.length) {
					$('.add-message').removeClass('alert alert-ok');
					$('.add-message').removeClass('alert-info');
					$('.add-message').addClass('alert global-alerts alert-msg alert-danger alert-dismissable');
				} else {
					$('.add-message').removeClass('alert');
					$('.add-message').removeClass('alert-danger');
					$('.add-message').addClass('alert alert-ok global-alerts alert-msg alert-info alert-dismissable');
				}
				$('.add-message').html(data.message);
				$('input:checkbox').prop('checked', false);
				ACC.minicart.updateMiniCartDisplay();
				ACC.wishlist.disableAddToCartButton();
			}
		});
	},

	/* -- disable the ADD TO CART BUTTON -- */
	disableAddToCartButton: function($element) {
		console.log('scs.wishlist.js - ACC.wishlist.disableAddToCartButton : executed');
		var checked = 0;
		$('input.js-entry-checkbox:checkbox').each(function() {
			checked += this.checked;
		});
		if (checked === 0) {
			//$('.js-wishlist-add-to-cart').prop('disabled', true);
			$('.js-wishlist-entry-check-all').prop('checked', false);
		} else {
			//$('.js-wishlist-add-to-cart').prop('disabled', false);
		}
		ACC.wishlist.updateTotalPrice();
	},

	updateSubtotalPrice: function(input) {
		console.log('scs.wishlist.js - ACC.wishlist.updateSubtotalPrice : executed');
		var productCode = $(input).data('product');
		var qty = input.val();
		var unitPrice = $('input#productUnitPrice_' + productCode).val();
		if (unitPrice !== '') {
			var totalPrice = parseFloat(qty) * parseFloat(unitPrice);
			$('span#productSubtotalPrice_' + productCode).html(totalPrice.toFixed(2));
		}
		ACC.wishlist.updateTotalPrice();
	},

	updateTotalPrice: function() {
		console.log('scs.wishlist.js - ACC.wishlist.updateTotalPrice : executed');
		var totalprice = 0;
		var currency = $('.currency').html();

		$('.js-subtotal-price').each(function() {
			var productCode = $(this).data("productCode");
			// Checked ?
			if ($('#' + productCode).is(':checked')) {
				totalprice += parseFloat($(this).html().replace(/[^\d\.]/g, ''));
			}
		});
		$('#grandTotalPrice').html(totalprice.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') + " " + currency);
		ACC.wishlist.updateTotalQty();
	},

	updateTotalQty: function() {
		console.log('scs.wishlist.js - ACC.wishlist.updateTotalQty : executed');
		var totalqty = 0;

		$('.js-qty-selector-input').each(function() {
			var productCode = $(this).data("product");
			// Checked ?
			if ($('#' + productCode).is(':checked')) {
				var qty = parseFloat($(this).val());

				// Qty is at 1 for food
				var productType = $('input#productType_' + productCode).val();
				if (productType === 'FOOD') { qty = 1; }

				totalqty += qty;
			}
		});
		if (totalqty !== 1) {
			$('#itemsCount').html(totalqty + $('#items').html());
		} else {
			$('#itemsCount').html(totalqty + $('#item').html());
		}
	},


	getPageName: function(nclass) {
		console.log('scs.wishlist.js - ACC.wishlist.getPageName : executed');
		var name;
		for (var int = 0; int < nclass.length; int++) {
			if (nclass[int] === "page-productDetails") {
				name = nclass[int];
			}
		}
		return name;
	},

	addProductToWishList: function(url) {
		console.log('scs.wishlist.js - ACC.wishlist.addProductToWishList : executed');
		$.ajax({
			url: ACC.config.encodedContextPath + url,
			dataType: "html",
			type: "POST",
			success: function(data) {
				$('#idResult').html(data);
				ACC.colorbox.resize();
				var addResult = $('#addResult').attr('value');
				if (addResult === 'false') {
					$('.wishListPopin').hide();
				}
			},
			error: function(xht, textStatus, ex) {}
		});
	},


	addNewWishList: function(url) {
		console.log('scs.wishlist.js - ACC.wishlist.addNewWishList : executed');
		$.ajax({
			url: ACC.config.encodedContextPath + url,
			dataType: "html",
			type: "POST",
			success: function(data) {
				$('#idResult').html(data);
				ACC.colorbox.resize();
				var addResult = $('#addResult').attr('value');
				if (addResult === 'false') {
					$('.wishListPopin').hide();
				}
			}
		});
	},

	/* -- COOKIE MANAGEMENT -- */

	createCookie: function(name, value, days) {
		console.log('scs.wishlist.js - ACC.wishlist.createCookie : executed');
	},

	readCookie: function(name) {
		console.log('scs.wishlist.js - ACC.wishlist.readCookie : executed');
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) === ' ') {
				c = c.substring(1, c.length);
			}
			if (c.indexOf(nameEQ) === 0) {
				return c.substring(nameEQ.length, c.length);
			}
		}
		return null;
	},

	eraseCookie: function(name) {
		console.log('scs.wishlist.js - ACC.wishlist.eraseCookie : executed');
		ACC.wishlist.createCookie(name, '', -1);
	},


	/* -- SHARE -- */

	formShareList: function(url) {
		console.log('scs.wishlist.js - ACC.wishlist.formShareList : executed');
		$("#shareForm").validate({
			submitHandler: function(form) {
				shareWishList(form, url);
				ACC.colorbox.resize();
			}
		});
	},

	shareWishList: function(form, url) {
		console.log('scs.wishlist.js - ACC.wishlist.shareWishList : executed');
		var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;

		if ($('#email').val().length === 0) {
			$("#email").addClass("errorInputShare");
			$(".errorValidation").show();
			$(".errorSize").hide();
			ACC.colorbox.resize();
			return false;
		} else if ($('#email').val().search(emailRegEx) === -1) {
			$("#email").addClass("errorInputShare");
			$(".errorValidation").hide();
			$(".errorSize").show();
			ACC.colorbox.resize();
			return false;
		} else if ($('#message').val().length > 500) {
			$("#message").addClass("errorInputShare");
			$(".errorValidation").hide();
			$(".errorSize").hide();
			$(".errorSizeMsg").show();
			ACC.colorbox.resize();
			return false;
		} else {
			$.ajax({
				type: "POST",
				url: ACC.config.contextPath + url,
				data: $(form).serialize(),
				success: function(data) {
					$('#partage-form').html($('#div-popin-pwd-ok').html());
					ACC.colorbox.resize();
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.error(errorThrown);
				}
			});
			return true;
		}
	}

};
