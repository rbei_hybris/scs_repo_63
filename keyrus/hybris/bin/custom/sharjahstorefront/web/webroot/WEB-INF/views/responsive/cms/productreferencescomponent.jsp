<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="addonProduct" tagdir="/WEB-INF/tags/addons/kaccwishlist/responsive/product" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"  %>
<c:set var="carouselClass" value="carousel" />

<c:set var="carouselMobileDots" value="true" />
<c:set var="carouselDesktopDots" value="true" />
<c:set var="carouselMobileArrows" value="false" />
<c:set var="carouselDesktopArrows" value="true" />
<c:set var="carouselMobileItems" value="2" />
<c:set var="carouselDesktopItems" value="5" />
<c:set var="carouselWidthSm" value="true" />
<c:set var="carouselDir" value="${currentLanguage.isocode == 'ar' ? 'rtl' : 'ltr'}" />

<c:url value="/cart/add" var="addToCartUrl"/>
<c:choose>
	<c:when test="${not empty productReferences and component.maximumNumberProducts > 0}">
		<div class="carousel ${carouselClass}--product--wrapper">
			<div class="carousel--headline">${component.title}</div>
			<div class="${carouselClass} js-carousel ${carouselClass}--product" data-carousel-mobile-items="${carouselMobileItems}" data-carousel-mobile-dots="${carouselMobileDots}" data-carousel-desktop-items="${carouselDesktopItems}" data-carousel-desktop-dots="${carouselDesktopDots}" data-carousel-desktop-arrows="${carouselDesktopArrows}" data-carousel-mobile-arrows="${carouselMobileArrows}" data-carousel-width-sm="${carouselWidthSm}" data-carousel-dir="${carouselDir}">
				<c:forEach end="${component.maximumNumberProducts}" items="${productReferences}" var="product">
					<c:url value="${product.target.url}" var="productUrl"/>
					<c:url value="${product.target.url}/configuratorPage/${configuratorType}" var="configureProductUrl"/>
					<c:set var="isForceInStock" value="${product.target.stock.stockLevelStatus.code eq 'inStock' and empty product.target.stock.stockLevel}"/>
					<c:choose>
						<c:when test="${isForceInStock}">
							<c:set var="maxQty" value="FORCE_IN_STOCK"/>
						</c:when>
						<c:otherwise>
							<c:set var="maxQty" value="${product.target.stock.stockLevel}"/>
						</c:otherwise>
					</c:choose>
					<c:set var="qtyMinus" value="1" />
					<c:url value="${product.target.url}/quickView" var="productQuickViewUrl"/>
					<div class="carousel--item product-tile">
						<div class="product-tile--wrapper">
							<a class="product-tile--info" href="${productUrl}">

								<div class="product-tile--thumb">
								<c:if test="${product.target.discountPrice ne null}">
													<div class="price--sticker">-${product.target.percentageDiscount}%</div>
												</c:if>
									<product:productPrimaryReferenceImage product="${product.target}" format="product"/>
								</div>

								<div class="product-tile--name">${fn:escapeXml(product.target.name)}</div>

								<!--price-->
										<div class="product-tile--price price">
											<ycommerce:testId code="productDetails_productNamePrice_label_${product.target.code}">
												<product:productPricePanel product="${product.target}" />
											</ycommerce:testId>
										</div>
								<!--end price-->
							</a>

							<c:if test="${not product.target.multidimensional }">
								<form:form id="addToCartForm${product.target.code}" action="${addToCartUrl}" method="post" class="add_to_cart_form">
									<c:if test="${empty showAddToCart ? true : showAddToCart}">
										<div class="product-tile--qty-selector">
											<%-- QTY Selector --%>
											<product:productQuantitySelector product="${product.target}" />
										</div>
									</c:if>
									<c:if test="${product.target.stock.stockLevel gt 0}">
										<c:set var="productStockLevel">${product.target.stock.stockLevel}&nbsp;
											<spring:theme code="product.variants.in.stock"/>
										</c:set>
									</c:if>
									<c:if test="${product.target.stock.stockLevelStatus.code eq 'lowStock'}">
										<c:set var="productStockLevel">
											<spring:theme code="product.variants.in.stock" />
										</c:set>
									</c:if>
									<c:if test="${isForceInStock}">
										<c:set var="productStockLevel">
											<spring:theme code="product.variants.available"/>
										</c:set>
									</c:if>
									<%-- <div class="stock-wrapper clearfix">
										${productStockLevel}
									</div> --%>
									<c:if test="${multiDimensionalProduct}" >
										<c:url value="${product.target.url}/orderForm" var="productOrderFormUrl"/>
										<a href="${productOrderFormUrl}" class="btn btn-default btn-block btn-icon js-add-to-cart glyphicon-list-alt">
											<spring:theme code="order.form" />
										</a>
									</c:if>
									<div class="product-tile--add-button product-tile--buttons add-button">
									   <div class="product-tile--buttons-item add-button-whishlist">
											<button  href="javascript:void();" id="idFav" class="button button--tertiary addtolist addtolist-home-ar add-button-whishlist-link js-add-product-to-wishlist" data-add-product-to-wishlist-url="${pageContext.request.contextPath}/wishlist/addProductToWishlistPopup/${product.target.code}" data-title="${addProductToWishlistTitle}"	>
											<div><div class="icon icon--add-list">&nbsp;</div></div>
												<addonProduct:productAddToWishlistButton product="${product.target}" />
											</button>
										</div>
										<ycommerce:testId code="addToCartButton">
											<input type="hidden" name="productCodePost" value="${product.target.code}"/>
											<input type="hidden" name="productNamePost" value="${fn:escapeXml(product.target.name)}"/>
											<input type="hidden" name="productPostPrice" value="${product.target.price.value}"/>
											<div class="product-tile--buttons-item add-button-cart">
											<c:choose>
												<c:when test="${product.target.stock.stockLevelStatus.code eq 'outOfStock' }">
													<button type="submit" class="button button--secondary add-button-toCart add-button-toCart-home-ar " aria-disabled="true" disabled="disabled">
														<div class="icon icon--cart-white">&nbsp;</div>
														<span><spring:theme code="text.addToCart" /></span>
													</button>
												</c:when>
												<c:otherwise>
													<button type="submit" class="button button--secondary add-button-toCart add-button-toCart-home-ar  js-enable-btn" disabled="disabled">
														<div><div class="icon icon--cart-white">&nbsp;</div></div>
														<span><spring:theme code="text.addToCart" /></span>
													</button>
												</c:otherwise>
											</c:choose>
											</div>
										</ycommerce:testId>
									</div>
								</form:form>
								<form:form id="configureForm${product.target.code}" action="${configureProductUrl}" method="get" class="configure_form">
									<c:if test="${product.target.configurable}">
										<c:choose>
											<c:when test="${product.target.stock.stockLevelStatus.code eq 'outOfStock' }">
												<button id="configureProduct" type="button" class="btn btn-primary btn-block" disabled="disabled">
													<spring:theme code="basket.configure.product"/>
												</button>
											</c:when>
											<c:otherwise>
												<button id="configureProduct" type="button" class="btn btn-primary btn-block js-enable-btn" disabled="disabled" onclick="location.href='${configureProductUrl}'">
													<spring:theme code="basket.configure.product"/>
												</button>
											</c:otherwise>
										</c:choose>
									</c:if>
								</form:form>
							</c:if>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
	</c:when>
	<c:otherwise>
		<component:emptyComponent />
	</c:otherwise>
</c:choose>
