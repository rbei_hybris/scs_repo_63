<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>

<ycommerce:testId code="searchPage_price_label_${product.code}">

		<%-- if product is multidimensional with different prices, show range, else, show unique price --%>
		<c:choose>
			<c:when test="${product.multidimensional and (product.priceRange.minPrice.value ne product.priceRange.maxPrice.value)}">
				<format:price priceData="${product.priceRange.minPrice}"/> - <format:price priceData="${product.priceRange.maxPrice}"/>
			</c:when>
			<c:otherwise>
				<c:if test="${product.discountPrice ne null}">
						<format:fromPrice priceData="${product.price}" strike="true"/>
						<span class="price--discount"><format:price priceData="${product.discountPrice}"/></span>
					</c:if>
					<c:if test="${product.discountPrice eq null}">
						<format:price priceData="${product.price}"/>
					</c:if>
			</c:otherwise>
		</c:choose>

</ycommerce:testId>
