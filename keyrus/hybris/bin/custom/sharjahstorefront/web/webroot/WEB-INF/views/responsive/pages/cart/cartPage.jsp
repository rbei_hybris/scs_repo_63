<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<template:page pageTitle="${pageTitle}">

	<cms:pageSlot position="BottomHeaderSlot" var="component" element="div"	class="container-fluid">
		<cms:component component="${component}" />
	</cms:pageSlot>

	<cart:cartValidation/>
	<cart:cartPickupValidation/>

	<div class="cart--continue-shopping-button-container">
		<a class="button cart--continue-shopping-button" href="/">
			<spring:theme  code="cart.page.continue"/>
		</a>
	</div>
	
	 <cms:pageSlot position="HelpContentSlot" var="feature">
			 <cms:component component="${feature}" element="div" class="cart-top-bar"/>
	 </cms:pageSlot>

	<div class="cartContent">
	
		<cms:pageSlot position="TopContent" var="feature">
				<cms:component component="${feature}" element="div" class="yComponentWrapper"/>
		</cms:pageSlot>

		<c:if test="${not empty cartData.entries}">
			<cms:pageSlot position="CenterLeftContentSlot" var="feature">
				<cms:component component="${feature}" element="div" class="yComponentWrapper"/>
			</cms:pageSlot>
		</c:if>

		<c:if test="${not empty cartData.entries}">
			<cms:pageSlot position="CenterRightContentSlot" var="feature">
				<cms:component component="${feature}" element="div" class="yComponentWrapper"/>
			</cms:pageSlot>
			<cms:pageSlot position="BottomContentSlot" var="feature">
				<cms:component component="${feature}" element="div" class="yComponentWrapper"/>
			</cms:pageSlot>

		</c:if>

		<c:if test="${empty cartData.entries}">
			<cms:pageSlot position="EmptyCartMiddleContent" var="feature">
				<cms:component component="${feature}" element="div" class="yComponentWrapper content__empty"/>
			</cms:pageSlot>
		</c:if>
	</div>
</template:page>
