<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ attribute name="id" required="true" type="java.lang.String" %>
<%@ attribute name="name" required="true" type="java.lang.String" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="isForceInStock" required="false" type="java.lang.Boolean" %>
<%@ attribute name="quantity" required="false" type="java.lang.String" %>
<%@ attribute name="readonly" required="false" type="java.lang.String" %>
<%@ attribute name="customClass" required="false" type="java.lang.String" %>

<c:set var="minQty" value="1" />
<c:set var="defaultQty" value="${not empty quantity ? quantity : minQty}" />
<c:set var="isForceInStock" value="${product.stock.stockLevelStatus.code eq 'inStock' and empty product.stock.stockLevel}"/>

<%--Format for removing trailing zeroes --%>
<fmt:formatNumber var="defaultQtyString" type="number" pattern="#.##" value="${defaultQty}" />

<c:set var="increment" value="1" />

<c:choose>
	<c:when test="${isForceInStock}">
		<c:set var="maxQty" value="FORCE_IN_STOCK" />
	</c:when>
	<c:otherwise>
		<c:set var="maxQty" value="${product.stock.stockLevel}" />
	</c:otherwise>
</c:choose>

<input type="number" maxlength="3" class="js-qty-selector-input ${customClass}" size="3" value="${defaultQtyString}"
		data-max="${maxQty}"
		data-min="${minQty}"
		data-val="${defaultQtyString}
		data-delta="${increment}"
		id="${id}" name="${name}" data-product="${product.code}" ${readonly} />
