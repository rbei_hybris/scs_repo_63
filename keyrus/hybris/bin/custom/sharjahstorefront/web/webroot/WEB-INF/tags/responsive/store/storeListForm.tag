<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="searchPageData" required="true" type="de.hybris.platform.commerceservices.search.pagedata.SearchPageData" %>
<%@ attribute name="locationQuery" required="false" type="java.lang.String" %>
<%@ attribute name="geoPoint" required="false" type="de.hybris.platform.commerceservices.store.data.GeoPoint" %>
<%@ attribute name="numberPagesShown" required="true" type="java.lang.Integer" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="store" tagdir="/WEB-INF/tags/responsive/store" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/responsive/action" %>

<c:url value="/store-finder" var="storeFinderFormAction" />

<div class="store__finder js-store-finder" data-url="${storeFinderFormAction}">
    <div class="row">
        <ycommerce:testId code="storeFinder">
            <!--List store-finder-->
            <div class="col-lg-12">
                <!--Store finder panel-->
                <div class="store__finder--panel">

                    <!--navigation-->
                    <div class="store__finder--navigation">
                        <a href="#" class="btnControlNav closeNavStoreFinder js-closeNavStoreFinder">
                            <i class="glyphicon glyphicon-menu-left enIcon"></i>
                            <i class="glyphicon glyphicon-menu-right arIcon"></i>
                        </a>
                        <a href="#" class="btnControlNav openNavStoreFinder js-openNavStoreFinder">
                            <i class="glyphicon glyphicon-menu-right enIcon"></i>
                            <i class="glyphicon glyphicon-menu-left arIcon"></i>
                        </a>
                        <!--pagination-->
                        <div class="store__finder--pagination" >
                            <div class="pull-right" style="display: none;">
                                <button class="btn btn-default js-store-finder-pager-prev" type="button">
                                    <i class="glyphicon glyphicon-circle-arrow-left"></i>
                                </button>
                                <button class="btn btn-default js-store-finder-pager-next" type="button">
                                    <i class="glyphicon glyphicon-circle-arrow-right"></i>
                                </button>
                            </div>
                            <span class="js-store-finder-pager-item-from"></span>-
                            <span class="js-store-finder-pager-item-to"></span>
                            <spring:theme code="storeFinder.pagination.from"></spring:theme>
                            <span class="js-store-finder-pager-item-all"></span>
                            <spring:theme code="storeFinder.pagination.stores"></spring:theme>
                        </div>
                        <!--end pagination-->
                        <div class="clearfix"></div>
                        <ul class="store__finder--navigation-list listSoreFinder js-store-finder-navigation-list">
                            <li class="loading"><span class="glyphicon glyphicon-repeat"></span></li>
                        </ul>
                        <div class="clearfix"></div>
                        <!--pagination-->
                            <div class="store__finder--pagination">

                                <span class="js-store-finder-pager-item-from"></span>-
                                <span class="js-store-finder-pager-item-to"></span>
                                <spring:theme code="storeFinder.pagination.from" text="from"></spring:theme>
                                <span class="js-store-finder-pager-item-all"></span>
                                <spring:theme code="storeFinder.pagination.stores"></spring:theme>
                                <div class="clearfix"></div>
                                <div class="pull-right">
                                    <button class="btn js-store-finder-pager-prev" type="button" >
                                        <span class="glyphicon ${currentLanguage.isocode == 'ar' ? ' glyphicon-chevron-right' : ' glyphicon-chevron-left'}"></span>
                                    </button>
                                    <button class="btn js-store-finder-pager-next" type="button" >
                                        <span class="glyphicon ${currentLanguage.isocode == 'ar' ? ' glyphicon-chevron-left' : ' glyphicon-chevron-right'}"></span>
                                    </button>
                                </div>
                            </div>
                        <!--end pagination-->
                    </div>
                    <!--End navigation-->

                    <!--Store finder details-->
                    <div class="store__finder--details js-store-finder-details">
                        <!--div>
                            <button class="btn btn-default pull-right store__finder--details-back js-store-finder-details-back">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                                <spring:theme code="pickup.in.store.back.to.results" text="Back"></spring:theme>
                            </button>
                        </div-->
                        <!--headerMap-->
                        <div class="headerMap">
                            <div class="store__finder--details-image js-store-image"></div>
                            <div class="store__finder--details-info">
                                <div class="info__name js-store-displayName"></div>
                                <div class="info__address">
                                    <div class="js-store-line1"></div>
                                    <div class="js-store-line2"></div>
                                </div>
                            </div>
                        </div>
                        <!--end headerMap-->
								 <!--Store finder details-->
                        <div class="store__finder--details-openings">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pull-left">
                                <dl class="dl-horizontal storeOpenings js-store-openings"></dl>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pull-right">
                                <div class="js-store-phone"></div>
                                <div class="openings__title"><spring:theme code="storeDetails.table.features" /></div>
                                <ul class="js-store-features"></ul>
                            </div>
                        </div>
                        <!--Map-->
                        <div class="store__finder--map js-store-finder-map"></div>
                        <!--end Map-->


                        <!--End Store finder details-->
                    </div>
                    <!--End Store finder details-->
                </div>
                <!--End Store finder panel-->
            </div>
            <!--End List store-finder-->
        </ycommerce:testId>
    </div>
</div>
