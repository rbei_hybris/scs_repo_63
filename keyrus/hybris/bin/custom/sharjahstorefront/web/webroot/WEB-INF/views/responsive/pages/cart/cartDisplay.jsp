<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true" />
<spring:theme code="text.addToCart" var="addToCartText"/>
<spring:theme code="text.popupCartTitle" var="popupCartTitleText"/>
<c:url value="/cart" var="cartUrl"/>
<c:url value="/cart/checkout" var="checkoutUrl"/>
<c:url value="/cart/updateMiniCart/SUBTOTAL" var="updateUrl"/>

<c:if test="${not empty cartData.entries}">
	<c:url value="/cart/checkout" var="checkoutUrl" scope="session"/>
	<c:url value="/quote/create" var="createQuoteUrl" scope="session"/>
	<c:url value="${continueUrl}" var="continueShoppingUrl" scope="session"/>
	<c:set var="showTax" value="false"/>

	<c:choose>
		<c:when test="${continueCheckout eq true}">
			<c:set var="checkoutButtonStyle" value="btn-primary js-continue-checkout-button" />
		</c:when>
		<c:otherwise>
			<c:set var="checkoutButtonStyle" value="btn-default btn--stop-shopping" />
		</c:otherwise>
	</c:choose>
	<div class="cart--header">
		<div class="title">${popupCartTitleText}</div>
		<div class="flex flex--dir-row flex--align-center">
			<div class="nbitems js-number-items-popin">
			<c:choose>
				<c:when test="${cartData.totalUnitCount > 1}">
					<spring:theme code="basket.page.totals.total.items.nospan" arguments="${cartData.totalUnitCount}"/>
				</c:when>
				<c:otherwise>
					<spring:theme code="basket.page.totals.total.items.one.nospan" arguments="${cartData.totalUnitCount}"/>
				</c:otherwise>
			</c:choose>
			</div>
		</div>
	</div>
	<cart:cartItems cartData="${cartData}"/>

</c:if>
<cart:ajaxCartTopTotalSection/>
