<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:set var="carouselClass" value="carousel" />

<c:set var="carouselMobileDots" value="true" />
<c:set var="carouselDesktopDots" value="true" />
<c:set var="carouselMobileArrows" value="false" />
<c:set var="carouselDesktopArrows" value="false" />
<c:set var="carouselMobileItems" value="1" />
<c:set var="carouselDesktopItems" value="5" />
<c:set var="carouselWidthSm" value="true" />
<c:set var="carouselDesktopCenter" value="true" />
<c:set var="carouselDir" value="${currentLanguage.isocode == 'ar' ? 'rtl' : 'ltr'}" />

<template:page pageTitle="${pageTitle}">
	<cms:pageSlot position="BottomHeaderSlot" var="component" element="div"	class="container-fluid">
		<cms:component component="${component}" />
	</cms:pageSlot>
<cms:pageSlot position="Section1" var="feature">
	<cms:component component="${feature}" />
</cms:pageSlot>

<div class="carousel ${carouselClass}--category--wrapper category--carousel1">
	<div class="${carouselClass}--headline"><spring:theme code="homepage.slider.addCategory.title"/></div>
	<div class="${carouselClass} js-carousel ${carouselClass}--category" data-carousel-mobile-items="${carouselMobileItems}" data-carousel-mobile-dots="${carouselMobileDots}" data-carousel-desktop-items="${carouselDesktopItems}" data-carousel-desktop-dots="${carouselDesktopDots}" data-carousel-desktop-arrows="${carouselDesktopArrows}" data-carousel-mobile-arrows="${carouselMobileArrows}" data-carousel-width-sm="${carouselWidthSm}" data-carousel-dir="${carouselDir}">
		<cms:pageSlot position="Section2A" var="feature" element="" class="">
				<cms:component component="${feature}" element="div" class="yComponentWrapper carousel-slide1"/>
		</cms:pageSlot>

	</div>
</div>

<cms:pageSlot position="Section3" var="feature" element="div">
	<cms:component component="${feature}" element="div" class="yComponentWrapper best-selling visible-xs visible-sm"/>
	<cms:component component="${feature}" element="div" class="yComponentWrapper best-selling visible-md visible-lg js-carousel-disabled"/>
</cms:pageSlot>


<c:set var="carouselMobileDots" value="true" />
<c:set var="carouselDesktopDots" value="true" />
<c:set var="carouselMobileArrows" value="false" />
<c:set var="carouselDesktopArrows" value="false" />
<c:set var="carouselMobileItems" value="1" />
<c:set var="carouselDesktopItems" value="5" />
<c:set var="carouselWidthSm" value="true" />
<c:set var="carouselDesktopCenter" value="true" />

<div class="carousel ${carouselClass}--brand--wrapper brand--carousel2">
	<div class="${carouselClass}--headline"><spring:theme code="homepage.slider.brand.title"/></div>
	<div class="${carouselClass} js-carousel ${carouselClass}--brand" data-carousel-mobile-items="${carouselMobileItems}" data-carousel-mobile-dots="${carouselMobileDots}" data-carousel-desktop-items="${carouselDesktopItems}" data-carousel-desktop-dots="${carouselDesktopDots}" data-carousel-desktop-arrows="${carouselDesktopArrows}" data-carousel-mobile-arrows="${carouselMobileArrows}" data-carousel-width-sm="${carouselWidthSm}">
		<cms:pageSlot position="Section4" var="feature" element="" class="">
			<cms:component component="${feature}" element="div" class="yComponentWrapper carousel-slide2"/>
		</cms:pageSlot>
	</div>
</div>

</template:page>
