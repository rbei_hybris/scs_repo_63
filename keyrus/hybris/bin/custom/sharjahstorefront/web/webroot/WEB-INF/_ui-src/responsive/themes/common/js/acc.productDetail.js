ACC.productDetail = {

	_autoload: [
		'initPageEvents',
		'bindVariantOptions'
	],


	checkQtySelector: function(self, mode) {
		console.log('acc.productDetail.js - ACC.productDetail.checkQtySelector() : executed');
		ACC.productDetail.mainCheckQtySelector(self, mode, '.js-qty-selector');
	},

	mainCheckQtySelector: function(self, mode, jsSelectorName) {
		console.log('acc.productDetail.js - ACC.productDetail.mainCheckQtySelector() : executed');
		var qtySelector = $(self).parents('.js-qty-selector');
		var input = qtySelector.find('.js-qty-selector-input');
		var inputVal = parseInt(input.val());
		var max = input.data('max');
		var minusBtn = qtySelector.find(jsSelectorName + '-minus');
		var plusBtn = qtySelector.find(jsSelectorName + '-plus');
		// console.log(self);
		// console.log(mode);
		// console.log(jsSelectorName);
		// console.log(qtySelector);
		// console.log(input);
		// console.log(inputVal);
		// console.log(max);
		// console.log(minusBtn);
		// console.log(plusBtn);
		qtySelector.find('.qty-selector-btn').removeAttr('disabled');

		if (mode === 'minus') {
			if (inputVal !== 1) {
				ACC.productDetail.updateQtyValue(self, inputVal - 1);
				if (inputVal - 1 === 1) {
					minusBtn.attr('disabled', 'disabled');
				}

			} else {
				minusBtn.attr('disabled', 'disabled');
			}
		} else if (mode === 'reset') {
			ACC.productDetail.updateQtyValue(self, 1);

		} else if (mode === 'plus') {
			if (max === 'FORCE_IN_STOCK') {
				ACC.productDetail.updateQtyValue(self, inputVal + 1);
			} else if (inputVal < max) {
				ACC.productDetail.updateQtyValue(self, inputVal + 1);
				if (inputVal + 1 === max) {
					plusBtn.attr('disabled', 'disabled');
				}
			} else {
				plusBtn.attr('disabled', 'disabled');
			}
		} else if (mode === 'input') {
			if (inputVal === 1) {
				minusBtn.attr('disabled', 'disabled');
			} else if (max === 'FORCE_IN_STOCK' && inputVal > 0) {
				ACC.productDetail.updateQtyValue(self, inputVal);
			} else if (inputVal === max) {
				plusBtn.attr('disabled', 'disabled');
			} else if (inputVal < 1) {
				ACC.productDetail.updateQtyValue(self, 1);
				minusBtn.attr('disabled', 'disabled');
			} else if (inputVal > max) {
				ACC.productDetail.updateQtyValue(self, max);
				plusBtn.attr('disabled', 'disabled');
			}
		} else if (mode === 'focusout') {
			if (isNaN(inputVal)) {
				ACC.productDetail.updateQtyValue(self, 1);
				minusBtn.attr('disabled', 'disabled');
			} else if (inputVal >= max) {
				plusBtn.attr('disabled', 'disabled');
			}
		}

	},

	updateQtyValue: function(self, value) {
		console.log('acc.productDetail.js - ACC.productDetail.updateQtyValue() : executed');
		var input = $(self).parents('.js-qty-selector').find('.js-qty-selector-input');
		var pdpForm = $(self).parents('.add-to-cart').find('#addToCartForm');
		var plpForm = $(self).parents('.bottom').find('#addToCartForm' + input.data('product'));
		var form = pdpForm.attr('id') === undefined ? plpForm : pdpForm;
		var addtocartQty = form.find('.js-qty-selector-input');
		var addtocartQtyHidden = $(self).parents('form').find('#qty');
		input.val(value).triggerHandler('change');
		addtocartQty.val(value);
		addtocartQtyHidden.val(value);
	},

	initPageEvents: function() {
		console.log('acc.productDetail.js - ACC.productDetail.initPageEvents() : executed');
		$(document).on('click', '.js-qty-selector .js-qty-selector-minus', function(event) {
			ACC.productDetail.checkQtySelector(this, 'minus');
			if ($(this).closest('.update_cart_form').length) {
				ACC.cartitem.handleUpdateQuantity(this, event);
			}
		});

		$(document).on('click', '.js-qty-selector .js-qty-selector-plus', function(event) {
			ACC.productDetail.checkQtySelector(this, 'plus');
			if ($(this).closest('.update_cart_form').length) {
				ACC.cartitem.handleUpdateQuantity(this, event);
			}
		});

		$(document).on('keydown', '.js-qty-selector .js-qty-selector-input', function(e) {

			if (($(this).val() !== ' ' && ((e.which >= 48 && e.which <= 57) || (e.which >= 96 && e.which <= 105))) || e.which === 8 || e.which === 46 || e.which === 37 || e.which === 39 || e.which === 9) {} else if (e.which === 38) {
				ACC.cartitem.changeQuantityForm($(this).closest('form'));
				ACC.productDetail.checkQtySelector(this, 'plus');
			} else if (e.which === 40) {
				ACC.cartitem.changeQuantityForm($(this).closest('form'));
				ACC.productDetail.checkQtySelector(this, 'minus');
			} else {
				e.preventDefault();
			}
		});

		$(document).on('keyup', '.js-qty-selector .js-qty-selector-input', function(e) {
			ACC.productDetail.checkQtySelector(this, 'input');
			ACC.cartitem.changeQuantityForm($(this).closest('form'));
			ACC.productDetail.updateQtyValue(this, $(this).val());

		});

		$(document).on('focusout', '.js-qty-selector .js-qty-selector-input', function(e) {
			ACC.productDetail.checkQtySelector(this, 'focusout');
			ACC.cartitem.changeQuantityForm($(this).closest('form'));
			ACC.productDetail.updateQtyValue(this, $(this).val());
		});

		$('#Size').change(function() {
			ACC.productDetail.changeOnVariantOptionSelection($('#Size option:selected'));
		});

		$('#variant').change(function() {
			ACC.productDetail.changeOnVariantOptionSelection($('#variant option:selected'));
		});

		$('.selectPriority').change(function() {
			window.location.href = $(this[this.selectedIndex]).val();
		});

		$(document).on('click', '.js-add-to-cart-action', function(event) {
			console.log('add to cart form with conf submit');
			var productConfOption = $('#productConfOption').val();
			$('input[name="productConfOption "]').val(productConfOption);
			$('#addToCartForm').submit();
		});

	},

	changeOnVariantOptionSelection: function(optionSelected) {
		console.log('acc.productDetail.js - ACC.productDetail.changeOnVariantOptionSelection() : executed');
		window.location.href = optionSelected.attr('value');
	},

	bindVariantOptions: function() {
		console.log('acc.productDetail.js - ACC.productDetail.bindVariantOptions() : executed');
		ACC.productDetail.bindCurrentStyle();
		ACC.productDetail.bindCurrentSize();
		ACC.productDetail.bindCurrentType();
	},

	bindCurrentStyle: function() {
		console.log('acc.productDetail.js - ACC.productDetail.bindCurrentStyle() : executed');
		var currentStyle = $('#currentStyleValue').data('styleValue');
		var styleSpan = $('.styleName');
		if (currentStyle !== null) {
			styleSpan.text(': ' + currentStyle);
		}
	},

	bindCurrentSize: function() {
		console.log('acc.productDetail.js - ACC.productDetail.bindCurrentSize() : executed');
		var currentSize = $('#currentSizeValue').data('sizeValue');
		var sizeSpan = $('.sizeName');
		if (currentSize !== null) {
			sizeSpan.text(': ' + currentSize);
		}
	},

	bindCurrentType: function() {
		console.log('acc.productDetail.js - ACC.productDetail.bindCurrentType() : executed');
		var currentSize = $('#currentTypeValue').data('typeValue');
		var sizeSpan = $('.typeName');
		if (currentSize !== null) {
			sizeSpan.text(': ' + currentSize);
		}
	}
};
