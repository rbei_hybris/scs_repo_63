<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/responsive/common/header"  %>

<c:url value="/_s/language" var="setLanguageActionUrl" />
<div style="display:none;">
	<div class="mobile-lang-switcher">
		<header:languageSelector languages="${languages}" currentLanguage="${currentLanguage}" />
	</div>
</div>
<div id="topNavigation" class="navigation-menu">
	<c:if test="${component.visible}">
		<nav class="navigation navigation-menu--bottom navigation--bottom js_navigation--bottom js-enquire-offcanvas-navigation" role="navigation">
			<ul class="sticky-nav-top js-sticky-user-group hidden-md hidden-lg">
				<%--Dynamically generated by 'acc.navigation.js"--%>
			</ul>
			<div class="navigation__overflow util--wrapper-max">
				<ul data-trigger="#signedInUserOptionsToggle" class="nav__links nav__links--products nav__links--mobile js-userAccount-Links js-nav-collapse-body offcanvasGroup1 collapse in hidden-md hidden-lg">
					<%--Dynamically generated by 'acc.navigation.js"--%>
				</ul>
				<ul class="nav__links nav__links--products js-offcanvas-links">
					<c:forEach items="${component.navigationNode.children}" var="childLevel1">
						<c:set value="${childLevel1.getClass().name eq 'com.sharjah.core.model.SharjahCMSNavigationNodeModel'}" var="isSharjahNode" />
						<li class="auto nav__links--primary <c:if test="${not empty childLevel1.children}">nav__links--primary-has__sub js-enquire-has-sub</c:if>">
							<c:forEach items="${childLevel1.entries}" var="childlink1">
								<cms:component component="${childlink1.item}" evaluateRestriction="true" element="span" class="nav__link js_nav__link" />
							</c:forEach>
							<%-- Calculate how many sub columns are needed -- Start --%>
							<c:set var="totalSubNavigationColumns" value="${0}"/>
							<c:set var="hasSubChild" value="false"/>
							<c:forEach items="${childLevel1.children}" var="childLevel2">
								<c:if test="${not empty childLevel2.children}">
									<c:set var="hasSubChild" value="true"/>
									<c:set var="subSectionColumns" value="${fn:length(childLevel2.children)/component.wrapAfter}"/>
									<c:if test="${subSectionColumns > 1 && fn:length(childLevel2.children)%component.wrapAfter > 0}">
										<c:set var="subSectionColumns" value="${subSectionColumns + 1}"/>
									</c:if>
									<c:choose>
										<c:when test="${subSectionColumns > 1}">
											<c:set var="totalSubNavigationColumns" value="${totalSubNavigationColumns + subSectionColumns}" />
										</c:when>

										<c:when test="${subSectionColumns < 1}">
											<c:set var="totalSubNavigationColumns" value="${totalSubNavigationColumns + 1}" />
										</c:when>
									</c:choose>
								</c:if>
							</c:forEach>
							<%-- Calculate how many sub columns are needed -- End --%>
							<%-- Decide which class to use -- Start --%>
							<c:choose>
								<c:when test="${!hasSubChild || (totalSubNavigationColumns > 0 && totalSubNavigationColumns <= 1)}">
									<c:set value="col-md-3 col-lg-2" var="subNavigationClass"/>
									<c:set value="col-md-12" var="subNavigationItemClass"/>
								</c:when>

								<c:when test="${totalSubNavigationColumns == 2}">
									<c:set value="col-md-6 col-lg-4" var="subNavigationClass"/>
									<c:set value="col-md-6" var="subNavigationItemClass"/>
								</c:when>

								<c:when test="${totalSubNavigationColumns == 3}">
									<c:set value="col-md-9 col-lg-6" var="subNavigationClass"/>
									<c:set value="col-md-4" var="subNavigationItemClass"/>
								</c:when>

								<c:when test="${totalSubNavigationColumns == 4}">
									<c:set value="col-md-12 col-lg-8" var="subNavigationClass"/>
									<c:set value="col-md-3" var="subNavigationItemClass"/>
								</c:when>

								<c:when test="${totalSubNavigationColumns == 5}">
									<c:set value="col-md-12" var="subNavigationClass"/>
									<%--custom grid class required because 1/5th columns aren't supported by bootstrap--%>
									<c:set value="column-20-percent" var="subNavigationItemClass"/>
								</c:when>

								<c:when test="${totalSubNavigationColumns > 5}">
									<c:set value="col-md-12" var="subNavigationClass"/>
									<c:set value="col-md-2" var="subNavigationItemClass"/>
								</c:when>
							</c:choose>
							<%-- Decide which class to use -- End --%>
							<c:if test="${not empty childLevel1.children}">
							  <%--<span class="glyphicon glyphicon-plus hidden-md hidden-lg nav__link--drill__down js_nav__link--drill__down"></span>--%>
								<c:set var="context" value="${pageContext.request.contextPath}"/>
								<c:choose>
									<c:when test="${context eq '/ar'}">
										<span class="hidden-md hidden-lg nav__link--drill__down js_nav__link--drill__down">
											<span class="icon icon--arrow-thin-carnation icon--dir-right"></span>
										</span>
									</c:when>
									<c:otherwise>
										<span class="hidden-md hidden-lg nav__link--drill__down js_nav__link--drill__down">
											<span class="icon icon--arrow-thin-carnation icon--dir-right"></span>
										</span>
									</c:otherwise>
								</c:choose>
								<div class="sub__navigation js_sub__navigation ${subNavigationClass}">
									<a class="sm-back js-enquire-sub-close hidden-md hidden-lg" href="#"><span class="icon icon--arrow-thin"></span><span class="sm-back--content">Back</span></a>
									<div class="row">
										<c:choose>
											<c:when test="${hasSubChild}">
												<c:forEach items="${childLevel1.children}" var="childLevel2" varStatus="st">
													<div class="sub-navigation-section ${subNavigationItemClass}">
												 <div id = "child2Desktop" class = "displayChildDesktop">
													<c:choose>
														<c:when test="${not empty childLevel2.links}">
															<ul class="sub-navigation-list">
																<cms:component component="${childLevel2.links[0]}" evaluateRestriction="true"  element="li" class="nav__link--secondary--child2" />
															</ul>
														</c:when>
														<c:otherwise>
																<div class="title" data-target=".sub-navigation-list">${childLevel2.title}</div>
														</c:otherwise>
													</c:choose>
												</div>		
												
												<div id = "child2Mobile"  class="displayChildMobile js-dropdown-subcat title" data-target=".sub-navigation-list">${childLevel2.title}</div>
														
												<ul class="sub-navigation-list <c:if test="${not empty childLevel2.title}">has-title</c:if>">
															<c:forEach items="${childLevel2.children}" var="childLevel3" step="${component.wrapAfter}" varStatus="i">
																<%-- wrap if more than 'component.wrapAfter' rows in one sub column --%>
																<c:if test="${i.index>=component.wrapAfter}">
																	<c:if test="${!i.first}">
																			</ul>
																		</div>
																	</c:if>
																	<div class="sub-navigation-section ${subNavigationItemClass}">
																		<ul class="sub-navigation-list <c:if test="${not empty childLevel2.title}">has-title</c:if>">
																</c:if>
																<c:forEach items="${childLevel2.children}" var="childLevel3" begin="${i.index}" end="${i.index + component.wrapAfter - 1}">
																	<c:forEach items="${childLevel3.entries}" var="childlink3" >
																		<cms:component component="${childlink3.item}" evaluateRestriction="true" element="li" class="nav__link--secondary" />
																	</c:forEach>
																</c:forEach>
															</c:forEach>
														</ul>

													</div>
												</c:forEach>
											</c:when>
											<c:otherwise>
												<div class="sub-navigation-section ${subNavigationItemClass}">
													<ul class="sub-navigation-list <c:if test="${not empty childLevel2.title}">has-title</c:if>">
														<c:forEach items="${childLevel1.children}" var="childLevel2">
															<c:forEach items="${childLevel2.entries}" var="childlink2">
																<cms:component component="${childlink2.item}" evaluateRestriction="true" element="li" class="nav__link--secondary" />
															</c:forEach>
														</c:forEach>
													</ul>
												</div>
											</c:otherwise>
										</c:choose>
									</div>

									<div class="row navigation-banner">
											<c:if test="${not empty childLevel1.banner1}">
												<cms:component component="${childLevel1.banner1}" evaluateRestriction="true"/>
											</c:if>
											<c:if test="${not empty childLevel1.banner2}">
												<cms:component component="${childLevel1.banner2}" evaluateRestriction="true"/>
											</c:if>
									</div>

								</div>
							</c:if>
						</li>
					</c:forEach>
				</ul>
			</div>
		</nav>
	</c:if>
</div>
