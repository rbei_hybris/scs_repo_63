<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/responsive/action" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="addonProduct" tagdir="/WEB-INF/tags/addons/kaccwishlist/responsive/product" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<spring:theme code="text.addToCart" var="addToCartText"/>
<c:url value="/cart/add" var="addToCartUrl"/>
<c:url value="${product.url}" var="productUrl"/>
<c:set value="${not empty product.potentialPromotions}" var="hasPromotion"/>


<div class="product-tile">
	<div class="product-tile--wrapper">
		<a href="${productUrl}" class="product-tile--link">
			<div class="product-tile--infos">
				<div class="product-tile--thumb">
					<c:if test="${product.discountPrice ne null}">
						<div class="price--sticker">-${product.percentageDiscount}%</div>
					</c:if>
					<product:productPrimaryImage product="${product}" format="product"/>
				</div>
				<div class="product-tile--name">${fn:escapeXml(product.name)}</div>
			</div>
		</a>
		<div class="product-tile--price price">
			<ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">
				<product:productPricePanel product="${product}" />
			</ycommerce:testId>
		</div>
		<c:if test="${not product.multidimensional }">
			<form:form id="addToCartForm${product.code}" action="${addToCartUrl}" method="post" class="add_to_cart_form">
				<c:if test="${empty showAddToCart ? true : showAddToCart}">
					<div class="product-tile--qty-selector">
						<%-- QTY Selector --%>
						<product:productQuantitySelector product="${product}" />
					</div>
				</c:if>
				<c:if test="${product.stock.stockLevel gt 0}">
					<c:set var="productStockLevel">${product.stock.stockLevel}&nbsp;
						<spring:theme code="product.variants.in.stock"/>
					</c:set>
				</c:if>
				<c:if test="${product.stock.stockLevelStatus.code eq 'lowStock'}">
					<c:set var="productStockLevel">
						<spring:theme code="product.variants.in.stock" />
					</c:set>
				</c:if>
				<c:if test="${isForceInStock}">
					<c:set var="productStockLevel">
						<spring:theme code="product.variants.available"/>
					</c:set>
				</c:if>
				<!-- 	<div class="stock-wrapper clearfix">
					${productStockLevel}
				</div> -->
				<c:if test="${multiDimensionalProduct}" >
					<c:url value="${product.url}/orderForm" var="productOrderFormUrl"/>
					<a href="${productOrderFormUrl}" class="button button--secondary js-add-to-cart glyphicon-list-alt">
						<div><div class="icon icon--cart-white">&nbsp;</div></div>
						<div class="button--label"><spring:theme code="order.form" /></div>
					</a>
				</c:if>
				<div class="product-tile--buttons add-button">

					<div class="product-tile--buttons-item add-button-whishlist">
						<c:choose>
							<c:when test="${currentLanguage.isocode eq 'en'}">
								<button id="idFav" class="button button--tertiary addtolist addtolist-en add-button-whishlist-link js-add-product-to-wishlist"	data-add-product-to-wishlist-url="${pageContext.request.contextPath}/wishlist/addProductToWishlistPopup/${product.code}" data-title="${addProductToWishlistTitle}">
									<div><div class="icon icon--add-list">&nbsp;</div></div>
									<spring:theme code="text.whishlist.add" />
								</button>
							</c:when>
								<c:otherwise>
									<button id="idFav" class="button button--tertiary addtolist addtolist-ar add-button-whishlist-link js-add-product-to-wishlist"	data-add-product-to-wishlist-url="${pageContext.request.contextPath}/wishlist/addProductToWishlistPopup/${product.code}" data-title="${addProductToWishlistTitle}">
									<div><div class="icon icon--add-list">&nbsp;</div></div>
									<spring:theme code="text.whishlist.add" />
								</button>
							</c:otherwise>
						</c:choose>
					</div>

					<ycommerce:testId code="addToCartButton">
						<input type="hidden" name="productCodePost" value="${product.code}"/>
						<input type="hidden" name="productNamePost" value="${fn:escapeXml(product.name)}"/>
						<input type="hidden" name="productPostPrice" value="${product.price.value}"/>
						<div class="product-tile--buttons-item add-button-cart">
							<c:choose>
								<c:when test="${currentLanguage.isocode eq 'en'}">
									<c:choose>
										<c:when test="${product.stock.stockLevelStatus.code eq 'outOfStock' }">
											<button class="button button--secondary add-button-toCart" aria-disabled="true" disabled="disabled">
												<div><div class="icon icon--cart-white">&nbsp;</div></div>
												<span><spring:theme code="text.addToCart" /></span>
											</button>
										</c:when>
										<c:otherwise>
											<button class="button button--secondary add-button-toCart js-enable-btn" disabled="disabled">
												<div><div class="icon icon--cart-white">&nbsp;</div></div>
												<div class="button--label"><spring:theme code="text.addToCart" /></div>
											</button>
										</c:otherwise>
									</c:choose>
								</c:when>
								<c:otherwise>
								<c:choose>
										<c:when test="${product.stock.stockLevelStatus.code eq 'outOfStock' }">
											<button class="button button--secondary add-button-toCart add-button-toCart-ar" aria-disabled="true" disabled="disabled">
												<div><div class="icon icon--cart-white">&nbsp;</div></div>
												<span><spring:theme code="text.addToCart" /></span>
											</button>
										</c:when>
										<c:otherwise>
											<button class="button button--secondary add-button-toCart add-button-toCart-ar js-enable-btn" disabled="disabled">
												<div><div class="icon icon--cart-white">&nbsp;</div></div>
												<div class="button--label"><spring:theme code="text.addToCart" /></div>
											</button>
										</c:otherwise>
									</c:choose>
								</c:otherwise>
							</c:choose>
						</div>
					</ycommerce:testId>

				</div>
			</form:form>
			<form:form id="configureForm${product.code}" action="${configureProductUrl}" method="get" class="configure_form">
				<c:if test="${product.configurable}">
					<c:choose>
						<c:when test="${product.stock.stockLevelStatus.code eq 'outOfStock' }">
							<button id="configureProduct" type="button" class="btn btn-primary btn-block" disabled="disabled">
								<spring:theme code="basket.configure.product"/>
							</button>
						</c:when>
						<c:otherwise>
							<button id="configureProduct" type="button" class="btn btn-primary btn-block js-enable-btn" disabled="disabled" onclick="location.href='${configureProductUrl}'">
									<spring:theme code="basket.configure.product"/>
							</button>
						</c:otherwise>
					</c:choose>
				</c:if>
			</form:form>
		</c:if>
	</div>
</div>
