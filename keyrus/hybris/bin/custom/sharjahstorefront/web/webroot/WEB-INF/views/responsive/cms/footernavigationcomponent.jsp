<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="footer" tagdir="/WEB-INF/tags/responsive/common/footer"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:if test="${component.visible}">
	<div class="footer--container util--wrapper-max">
		<div class="skiptocontent">
			<a href="#skip-to-content" data-role="none" class="icon icon--arrow-thin-carnation icon--pos-right"><spring:theme code="homepage.skipToContent"/></a>
		</div>
	    <div class="footer--container--top">
            <div class="footer--container--top--content">
              	<c:forEach items="${component.navigationNode.children}" var="childLevel1">
              		<c:choose>
              			<c:when test="${childLevel1.uid ne 'FollowUsNavNode' and childLevel1.uid ne 'BottomFooterNavNode'}">
	               	<c:forEach items="${childLevel1.children}" step="${component.wrapAfter}" varStatus="i">
						   <div class="footer--container--top--content--item">
	                	   	<c:if test="${component.wrapAfter > i.index}">
                                   <div class="footer--container--top--content--item-title">${childLevel1.title}</div>
                           </c:if>
                               <ul class="footer--container--top--content--item-link">
	                           	<c:forEach items="${childLevel1.children}" var="childLevel2" begin="${i.index}" end="${i.index + component.wrapAfter - 1}">
	                              	<c:forEach items="${childLevel2.entries}" var="childlink" >
		                              	<cms:component component="${childlink.item}" evaluateRestriction="true" element="li" class="footer__link"/>
	                                 </c:forEach>
	                              </c:forEach>
                        		</ul>
                			</div>
					    	</c:forEach>
              			</c:when>
              			<c:when test="${childLevel1.uid eq 'BottomFooterNavNode'}">
              				<c:set var="bottomComp" value="${childLevel1}" />
              			</c:when>
              			<c:otherwise>
              				<c:set var="socialComp" value="${childLevel1}" />
              			</c:otherwise>
              		</c:choose>
              	</c:forEach>

				<c:if test="${not empty socialComp}">
	           		<div class="footer--container--top--content--item">
           				<div class="footer--container--top--content--item-title">
           					${socialComp.title}
           				</div>
           				<c:set var="socialClass" value="socialClass"/>
           				<ul class="footer--container--top--content--item-socialMedia">
           					<c:forEach items="${socialComp.children}" var="childLevel2">
			                  	<c:forEach items="${childLevel2.entries}" var="childlink" >
			                  		<c:choose>
			                  			<c:when test="${childlink.item.uid eq 'FacebookLink'}">

			                  				<c:set var="socialClass" value="fb"/>
			                  			</c:when>
			                  			<c:when test="${childlink.item.uid eq 'TwitterLink'}">

			                  				<c:set var="socialClass" value="tw"/>
			                  			</c:when>
			                  			<c:when test="${childlink.item.uid eq 'InstagramLink'}">

			                  				<c:set var="socialClass" value="ig"/>
			                  			</c:when>
			                  			<c:when test="${childlink.item.uid eq 'SnapchatLink'}">

											<c:set var="socialClass" value="sc"/>
			                  			</c:when>
			                  		</c:choose>
				                  	<cms:component component="${childlink.item}" evaluateRestriction="true" element="li" class="${socialClass}"/>
			                     </c:forEach>
           					</c:forEach>
           				</ul>
	           		</div>
           		</c:if>
       		</div>
		</div>
		<div class="footer--container--bottom">
		    <div class="footer--container--bottom--breadcrumb">
				<ul class="footer--container--bottom--breadcrumb-list">
		      	<c:forEach items="${bottomComp.children}" var="childLevel2">
				   	<c:forEach items="${childLevel2.entries}" var="childlink" >
				   		<cms:component component="${childlink.item}" evaluateRestriction="true" element="li"/>
				   	</c:forEach>
				   </c:forEach>
				</ul>
			 </div>
		     <div class="footer--container--bottom--copyright">${notice}</div>
		</div>
	</div>
</c:if>
