<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="order" required="true"
	type="de.hybris.platform.commercefacades.order.data.OrderData"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<div class="order-detail-overview">
	<div class="order-detail-overview--first-row">
		<div class="order-detail-overview--order-number">
			<ycommerce:testId code="orderDetail_overviewOrderID_label">
				<span class="order-detail-overview--label">
					<spring:theme code="text.account.orderHistory.orderNumber" />
				</span>
				<span class="order-detail-overview--value">
						${fn:escapeXml(orderData.code)}
				</span>
			</ycommerce:testId>
		</div>
		<div class="order-detail-overview--date-placed">
			<ycommerce:testId code="orderDetail_overviewStatusDate_label">
				<span class="order-detail-overview--label">
					<spring:theme code="text.account.orderHistory.datePlaced" />
				</span>
				<span class="order-detail-overview--value">
					<fmt:formatDate value="${order.created}" pattern="dd/MM/yyyy HH:mm" dateStyle="medium" timeStyle="short" type="both" />
				</span>
			</ycommerce:testId>
		</div>
		<div class="order-detail-overview--total">
			<ycommerce:testId code="orderDetail_overviewOrderTotal_label">
				<span class="order-detail-overview--label">
					<spring:theme code="text.account.order.total" />
				</span>
				<span class="order-detail-overview--value">
					<format:price priceData="${order.totalPrice}" />
				</span>
			</ycommerce:testId>
		</div>
	</div>
	<div class="order-detail-overview--second-row">
		<c:if test="${not empty orderData.statusDisplay}">
			<div class="order-detail-overview--status">
				<ycommerce:testId code="orderDetail_overviewOrderStatus_label">
					<span class="order-detail-overview--label">
						<spring:theme code="text.account.orderHistory.orderStatus" />
					</span>
					<span class="order-detail-overview--value">
						<spring:theme code="text.account.order.status.display.${orderData.statusDisplay}" />
					</span>
				</ycommerce:testId>
			</div>
		</c:if>
		<button class="js-reOrder button button--secondary order-detail-overview--button" id="reOrderButton">
			<spring:theme code="text.account.detail.reorder" />
		</button>
		<button class="js-newShopList js-wishlist-add-button button button--tertiary order-detail-overview--button">
			<spring:theme code="text.account.detail.newShopList" />
		</button>
		<c:if test="${((empty showCancel) and (orderData.statusDisplay ne 'completed') and (orderData.statusDisplay ne 'cancelled') and (orderData.statusDisplay ne 'cancelling'))}">
			<button class="js-cancelOrderDetail button button--tertiary order-detail-overview--button" id="cancelOrderButton">
				<spring:theme code="text.account.detail.cancel" />
			</button>
		</c:if>
	</div>
	<div id="idAddOrderToWishlist" style="display:none;">
	<div id="idResult"></div>
	<div class="wishListPopin">
		<div class="row">
			<spring:theme code="kacc.wishlist.add.wishlist.choice"/>
		</div>
		<div class="row">
			<select id="idSelectOption">
				<option value="newWishlist" selected="selected">
					<spring:theme code="kacc.wishlist.new.option.message" />
				</option>
				<c:forEach items="${wishlists}" var="wishlist" varStatus="status">
					<option class="classOption" value="${wishlist.name}" ${status.first ? 'selected="selected"' : ''}>
						${wishlist.name}
					</option>
				</c:forEach>
			</select>
			<input id="orderCode" name="orderCode" type="hidden" value="${orderCode}"/>
		</div>
		<div id="newWishlistSection" class="row">
			<label for="newWishlistName">
				<spring:theme code="kacc.wishlist.add.wishlist.message" />
			</label>
			<input id="newWishlistName" name="newWishlistName" type="text" class="required show"
					placeholder="<spring:theme code="kacc.wishlist.add.wishlist.name.placeholder" />" />
		</div>
		<div class="row">
			<div class="col-sm-5 col-lg-2">
				<button id="wishlistCancel" class="btn btn-primary btn-block">
					<spring:theme code="kacc.wishlist.add.wishlist.cancel" />
				</button>
			</div>
			<div class="col-sm-7 col-lg-10">
				<spring:theme code="kacc.wishlist.add.wishlist.add" var="wishlistAddButton" />
				<spring:theme code="kacc.wishlist.add.wishlist.create" var="wishlistCreateButton" />
				<button id="idSubmitNewWishlist" class="btn btn-primary btn-block"
						data-add-label="${wishlistAddButton}" data-create-label="${wishlistCreateButton}">
					${wishlistAddButton}
				</button>
			</div>
		</div>
		<p class="alert alert-danger errorValidation errorValidation" style="display:none;"><spring:theme code="kacc.wishlist.empty.wishlist.mandatory"/></p>
		<p class="alert alert-danger errorSize errorSize" style="display:none;"><spring:theme code="kacc.wishlist.empty.wishlist.sizeError"/></p>
	</div>
</div>
</div>
