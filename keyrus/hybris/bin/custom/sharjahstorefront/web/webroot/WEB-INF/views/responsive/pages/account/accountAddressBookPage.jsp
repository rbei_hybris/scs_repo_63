<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<c:set var="noBorder" value=""/>
<c:if test="${not empty addressData}">
    <c:set var="noBorder" value="no-border"/>
</c:if>

<div class="account-section-header ${noBorder}">
	<span class="glyphicon account-section-header-orderhistory ${currentLanguage.isocode == 'ar' ? ' glyphicon-chevron-right' : ' glyphicon-chevron-left'}"></span>
   <span><spring:theme code="text.account.addressBook"/></span>
</div>
<ycommerce:testId code="addressBook_addNewAddress_button">
<div class="account-section-header-add">
    <a href="add-address" class="button button--secondary addAddress">
        <spring:theme code="text.account.addressBook.addAddress"/>
    </a>
</div>
</ycommerce:testId>

<div class="account-addressbook account-list">
    <c:if test="${empty addressData}">
		<div class="account-section-content content-empty">
			<spring:theme code="text.account.addressBook.noSavedAddresses" />
		</div>
    </c:if>

    <c:if test="${not empty addressData}">
	    <div class="account-cards card-select cardsAddressBook">
			<div class="">
				<c:forEach items="${addressData}" var="address">
				<!--item cardAddress-->
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 card cardAddress">
						<!--cardDetailsAddress-->
						<div class="cardDetailsAddress viewHover">
							<ul class="pull-left">
								<li>
									<h3>
										${fn:escapeXml(address.name)}
									</h3>
								</li>
								<li class="cardAdressLine1">${fn:escapeXml(address.firstName)}&nbsp;${fn:escapeXml(address.lastName)}</li>
								<li class="cardAdressLine1">${fn:escapeXml(address.line1)}</li>
								<c:if test="${not empty fn:escapeXml(address.line2)}">
								<li class="cardAdressLine2">${fn:escapeXml(address.line2)}</li>
								</c:if>
								<c:if test="${not empty fn:escapeXml(address.building)}">
								<li class="cardAdressLine2">${fn:escapeXml(address.building)}</li>
								</c:if>
								<c:if test="${not empty fn:escapeXml(address.apartment)}">
								<li class="cardAdressLine2">${fn:escapeXml(address.apartment)}</li>
								</c:if>
								<c:if test="${not empty fn:escapeXml(address.landmark)}">
								<li class="cardAdressLine2">${fn:escapeXml(address.landmark)}</li>
								</c:if>
								<li>${fn:escapeXml(address.town)}&nbsp;${fn:escapeXml(address.region.name)}</li>
								<li class="cardAdressCountryPc"> ${fn:escapeXml(address.country.name)}&nbsp;${fn:escapeXml(address.postalCode)}</li>
								<li class="cardAdressLine2">${fn:escapeXml(address.phone)}</li>
								<c:if test="${address.defaultAddress}">
									<li class="cardDefault">(<spring:theme code="text.default"/>)</li>
								</c:if>
							</ul>

							<!--listActions-->
			                     <ul class="listActions">
									<ycommerce:testId code="addressBook_editAddress_button">
									<li>
										<a class="action-links linkActionAddress" href="edit-address/${address.id}" title="<spring:theme code="text.address.edit" />">

											<span class="glyphicon glyphicon-pencil"></span>
										</a>
									</li>
									</ycommerce:testId>
									<ycommerce:testId code="addressBook_removeAddress_button">
									<li>
										<a href="#"  class="action-links removeAddressFromBookButton linkActionAddress" title="<spring:theme code="text.address.delete" />" data-address-id="${address.id}" data-popup-title="">
											<span class="glyphicon glyphicon-remove"></span>
										</a>
									</li>
									</ycommerce:testId>
									<c:if test="${not address.defaultAddress}">
									<ycommerce:testId code="addressBook_isDefault_button">
									<li>
										<a class="account-set-default-address linkActionAddress" href="set-default-address/${address.id}" title="<spring:theme code="text.address.set.default" />">
											<span class="glyphicon glyphicon-star"></span>
											<!--spring:theme code="text.setDefault"/-->
										</a>
									</li>
									</ycommerce:testId>
								</c:if>
								</ul>
							    <!--end listActions-->
						</div>
						<!--end cardDetailsAddress-->

					</div>
					<!--end item cardAddress-->
				</c:forEach>
			</div>

			<c:forEach items="${addressData}" var="address">
		        <div class="display-none">
		       	 	<div id="popup_confirm_address_removal_${address.id}" class="account-address-removal-popup">
		        		<div class="addressItem">
		        			<spring:theme code="text.address.remove.following" />

		       				<div class="address">
						        <strong>
						        ${fn:escapeXml(address.firstName)}&nbsp;
						        ${fn:escapeXml(address.lastName)}
						        </strong>
						        <br>
						        ${fn:escapeXml(address.line1)}&nbsp;
						        ${fn:escapeXml(address.line2)}
						        <br>
						        ${fn:escapeXml(address.town)}&nbsp;
						        <c:if test="${not empty address.region.name }">
						            ${fn:escapeXml(address.region.name)}&nbsp;
						        </c:if>
						        <br>
						        ${fn:escapeXml(address.country.name)}&nbsp;
						        ${fn:escapeXml(address.postalCode)}
						        <br/>


						        ${fn:escapeXml(address.phone)}
		       				</div>

					        <div class="modal-actions">
                                <div class="row">
                                  <div class="actions">
                                    <ycommerce:testId code="addressRemove_delete_button">
                                            <a class="button button--secondary" data-address-id="${address.id}" href="remove-address/${address.id}">
                                                <spring:theme code="text.address.delete" />
                                            </a>
                                    </ycommerce:testId>
                                        <a class="button button--secondary closeColorBox" data-address-id="${address.id}">
                                            <spring:theme code="text.button.cancel"/>
                                        </a>
                                    </div>
					       	    </div>
					       	</div>
		        		</div>
		        	</div>
		        </div>
		    </c:forEach>
	    </div>
    </c:if>
</div>
<div class="account-section-header-add account-section-header-add-bottom">
    <a href="/" class="button button--secondary returnAddressPage">
        <spring:theme code="menu.button.return.home"/>
    </a>
</div>
<div class="clearfix"></div>
<br>
