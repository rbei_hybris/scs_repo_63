$(document).ready(function() {
	console.log('scs.checkout.js : executed');
	if($('#codePaymentModeValue').val() === 'cod'){
		$('#paymentCashText').removeClass("hidden");
	}
	
	$(document).on('click', '.js-payment-mode', function(e) {
		var currenPaymentMode = $(this).val();
		var COD = 'cod';
		var CREDIT_CARD = 'creditcard';
		$('#paymentCashText').toggleClass('hidden', COD !== currenPaymentMode);
		$('#saveCardBlock').toggle(CREDIT_CARD === currenPaymentMode);
		
		$.get(ACC.config.encodedContextPath + '/checkout/multi/payment-method/' + currenPaymentMode);
		
		$('.payment-type span').html($("label[for='"+$(this).attr('id')+"']").text());
	});
	
	$(document).on('click', '.js-cancelOrderDetail', function(e) {
		console.log('scs.checkout.js : start executed');
		console.log('CancelOrder : start executed');
    	var code =  document.getElementById('orderCode').value;

        $.ajax({
            url: ACC.config.encodedContextPath + '/my-account/cancelOrder?orderCode=' + code,
            type: "POST",
            data: {},
            success: function(data) {
            	window.location = ACC.config.encodedContextPath + '/my-account/orders';
            }
          });
		  console.log('CancelOrder : end executed');
	});
	
	$(document).on('click', '.js-reOrder', function(e) {
		console.log('scs.checkout.js : start executed');
		console.log('ReOrder : start executed');
    	var code =  document.getElementById('orderCode').value;

        $.ajax({
            url: ACC.config.encodedContextPath + '/my-account/reorder?orderCode=' + code,
            type: "POST",
            data: {},
            success: function(data) {
            	window.location = ACC.config.encodedContextPath + '/cart';
            }
          });
		  console.log('ReOrder : end executed');
	});

});
