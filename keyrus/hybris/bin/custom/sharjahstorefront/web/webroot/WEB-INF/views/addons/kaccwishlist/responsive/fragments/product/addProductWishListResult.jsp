<%@ page trimDirectiveWhitespaces="true"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>



<input type="hidden" id="addResult" value="${result.error}" />

<div id="idResultwishlist">
	<c:choose>
		<c:when test="${result.error}">
			<div class="alert alert-danger" role="alert">${result.message}</div>
		</c:when>
		<c:otherwise>
			<script type="text/javascript">
				setTimeout(parent.$.colorbox.close, 3000);	
			</script>
			<div class="itemAdded">
				<c:set var="url" value="" />
				
				<c:forEach items="${product.images }" var="img">
					<c:if test="${img.format == 'thumbnail'}">
						<c:set var="url" value="${img.url }" />
					</c:if>
				</c:forEach>
				 
			
				<c:if test="${not empty url }">
					<img alt="" src="${url}">
				</c:if>
				<br>
				<span>${product.name}</span> <br>
				<span>
					  <spring:theme code="kacc.wishlist.add.wishlist.confirm"/>&nbsp;
					 <a href="${pageContext.request.contextPath}/my-account/wishlist/?wishlistParam=${wishlistName}">${wishlistName}</a>
				 </span>
			</div>
		</c:otherwise>
	</c:choose>

</div>

