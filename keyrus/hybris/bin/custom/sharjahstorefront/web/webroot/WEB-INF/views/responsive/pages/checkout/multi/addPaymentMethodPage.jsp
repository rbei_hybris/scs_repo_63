<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="multiCheckout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>


<spring:url value="/checkout/multi/termsAndConditions" var="getTermsAndConditionsUrl"/>
<c:url value="${currentStepUrl}" var="choosePaymentMethodUrl" />
<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">
	<cms:pageSlot position="BottomHeaderSlot" var="component" element="div"	class="container-fluid">
		<cms:component component="${component}" />
	</cms:pageSlot>
    <div class="row checkout">
	<div class="account-section-header">
    <a href="javascript:history.back();">
    	<div class="account-section-header ${noBorder}">
					<span class="glyphicon account-section-header-orderhistory ${currentLanguage.isocode == 'ar' ? 'glyphicon-chevron-right' : ' glyphicon-chevron-left'}"></span>
	<span> <spring:theme code="checkout.multi.secure.checkout" /></span>
</div>
   </a>
	</div>
        <div class="col-md-5">
            <multiCheckout:checkoutSteps checkoutSteps="${checkoutSteps}" progressBarId="${progressBarId}">
                <jsp:body>
                    <ycommerce:testId code="checkoutStepThree">
                        <div class="checkout-paymentmethod">
                            <div class="checkout-indent">
                                <c:if test="${not empty paymentFormUrl}">
                                    <ycommerce:testId code="sharjahPaymentDetailsForm">
                                        <form:form id="silentOrderPostForm" name="silentOrderPostForm" commandName="sharjahPaymentDetailsForm" action="${request.contextPath}${paymentFormUrl}" method="POST" class="form">
                                            <div class="headline">
												<spring:theme code="checkout.multi.payment.method"/>
											</div>
											<div class="form-group">
												<c:if test="${not empty paymentInfos}">
													<button type="button" class="btn button btn-block js-saved-payments button--secondary "><spring:theme code="checkout.multi.paymentMethod.addPaymentDetails.useSavedCard"/></button>
												</c:if>
											</div>

											<c:forEach var="paymentMode" items="${paymentModes}">

												<div class="form-group">
													<input type="radio" class="js-payment-mode" id="paymentMode-${paymentMode.code}" name="paymentId"
														value="${paymentMode.code}" ${paymentMode.code eq codePaymentMode ? 'checked="checked"' : ''}/>
													<label for="paymentMode-${paymentMode.code}"> ${paymentMode.name}</label>
												</div>
											</c:forEach>
											
											<input type="hidden" value="${codePaymentMode}"   id="codePaymentModeValue"/>
											
											
											<spring:theme code="please.select" var="selectElt"/>
											<div id="paymentCashText" class="hidden">
												<formElement:formSelectBox idKey="moneyChange" labelKey="moneyChange" path="moneyChange" mandatory="false" skipBlank="false"
													skipBlankMessageKey="${selectElt}" items="${availableCashChange}" selectCSSClass="form-control" />
											</div>

											<hr/>
											<div class="headline">
												<spring:theme code="checkout.multi.paymentMethod.addPaymentDetails.billingAddress"/>
											</div>

											<c:if test="${cartData.deliveryItemsQuantity > 0}">
												<div id="useDeliveryAddressData"
													data-firstname="${deliveryAddress.firstName}"
													data-lastname="${deliveryAddress.lastName}"
													data-countryisocode="${deliveryAddress.country.isocode}"
													data-town="${deliveryAddress.town}"
													data-state="${deliveryAddress.deliveryArea.code}"
													data-line1="${deliveryAddress.line1}"
													data-building="${deliveryAddress.building}"
													data-apartment="${deliveryAddress.apartment}"
													data-landmark="${deliveryAddress.landmark}"
													data-address-id="${deliveryAddress.id}"
												></div>
												<formElement:formCheckbox
													path="useDeliveryAddress"
													idKey="useDeliveryAddress"
													labelKey="checkout.multi.sop.useMyDeliveryAddress"
													tabindex="11"/>
											</c:if>

											<address:paymentAddressFormSelector supportedCountries="${countries}" regions="${regions}" tabindex="12"/>

											<div class="checkbox--wrapper">
												<div id="saveCardBlock" class="checkbox">
													<input id="saveCard" name="saveCard" type="checkbox" value="true">
													<label for="saveCard" class="checkbox--text"><spring:theme code="checkout.summary.paymentMethod.saveCard" /></label>
												</div>

												<div class="checkbox">
													<input id="termsCheck" name="termsCheck" type="checkbox" value="true">
													<label for="termsCheck" class="checkbox--text">
														<spring:theme code="checkout.summary.placeOrder.readTermsAndConditions" arguments="${getTermsAndConditionsUrl}" text="Terms and Conditions"/>
													</label>
												</div>
											</div>

											<button type="button" class="button button--secondary submit_silentOrderPostForm checkout-next">
												<spring:theme code="checkout.multi.paymentMethod.continue" text="Next"/>
											</button>
										</form:form>
                                    </ycommerce:testId>
                                </c:if>
                            </div>
                        </div>

                        <c:if test="${not empty paymentInfos}">
                            <div class="checkout-saved-payments">
                                <div id="savedpaymentstitle">
                                    <div class="headline">
                                        <span class="headline-text"><spring:theme code="checkout.multi.paymentMethod.addPaymentDetails.useSavedCard"/></span>
                                    </div>
                                </div>
                                <div id="savedpaymentsbody">
                                    <c:forEach items="${paymentInfos}" var="paymentInfo" varStatus="status">
                                        <form action="${request.contextPath}/checkout/multi/payment-method/choose" method="GET">
                                            <input type="hidden" name="selectedPaymentMethodId" value="${paymentInfo.id}"/>
                                            <div class=""><strong>${fn:escapeXml(paymentInfo.billingAddress.firstName)}&nbsp; ${fn:escapeXml(paymentInfo.billingAddress.lastName)}</strong></div>
                                            <div class="">${fn:escapeXml(paymentInfo.cardType)}</div>
                                            <div class="">${fn:escapeXml(paymentInfo.accountHolderName)}</div>
                                            <div class="">${fn:escapeXml(paymentInfo.cardNumber)}</div>
														  <div class=""><spring:theme code="checkout.multi.paymentMethod.paymentDetails.expires" arguments="${fn:escapeXml(paymentInfo.expiryMonth)},${fn:escapeXml(paymentInfo.expiryYear)}"/></div>
														  <div class="">${fn:escapeXml(paymentInfo.billingAddress.line1)}</div>
														  <div class="">${fn:escapeXml(paymentInfo.billingAddress.apartment)}&nbsp; ${fn:escapeXml(paymentInfo.billingAddress.region.building)}</div>
                                            <c:if test="${not empty paymentInfo.billingAddress.country.isocode}">
                                            	<div class="">
                                            		<c:if test="${not empty paymentInfo.billingAddress.deliveryArea}">${fn:escapeXml(paymentInfo.billingAddress.deliveryArea.code)}&nbsp; </c:if>
                                            		${fn:escapeXml(paymentInfo.billingAddress.town)}&nbsp; ${fn:escapeXml(paymentInfo.billingAddress.country.isocode)}
                                            	</div>
                                            </c:if>

											<div class="checkbox">
												<input id="termsCheck${status.count}" name="termsCheck" type="checkbox" value="true">
												<label for="termsCheck${status.count}">
													<spring:theme code="checkout.summary.placeOrder.readTermsAndConditions" arguments="${getTermsAndConditionsUrl}" text="Terms and Conditions"/>
												</label>
											</div>

											<button type="submit" class="button button--secondary btn-block" tabindex="${(status.count * 2) - 1}">
												<spring:theme code="checkout.multi.paymentMethod.addPaymentDetails.useThesePaymentDetails"/>
											</button>
                                        </form>
                                    </c:forEach>
                                </div>
                            </div>
                        </c:if>
                    </ycommerce:testId>
               </jsp:body>
            </multiCheckout:checkoutSteps>
		</div>
        <div class="col-md-1">
        </div>
        <div class="col-md-6 checkout-summary">
            <multiCheckout:checkoutOrderDetails cartData="${cartData}" showDeliveryAddress="true" showPaymentInfo="false" showTaxEstimate="false" showTax="true" />
        </div>

		<div class="col-sm-12 col-lg-12">
			<cms:pageSlot position="SideContent" var="feature" element="div" class="checkout-help">
				<cms:component component="${feature}"/>
			</cms:pageSlot>
		</div>
	</div>

</template:page>
