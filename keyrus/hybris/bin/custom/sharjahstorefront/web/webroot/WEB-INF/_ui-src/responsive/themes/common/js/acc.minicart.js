ACC.minicart = {

    _autoload: [
        'bindMiniCart'
    ],
    id: '#cart',
    class: 'cart',
    classActive: 'active',
    classLoader: 'cart--loader',
    classTriggerToggle: 'js-mini-cart-link',
    classWrapperContent: 'cart--content',
    classWrapperItemList: 'cart--item-list',
    opened: false, // minicart get html state
    active: false, // minicart display state
    bindMiniCart: function() {
        console.log('acc.minicart.js - ACC.minicart.bindMiniCart() : executed');

        // Open/close the mini cart
        $(document).on('click', '.' + ACC.minicart.classTriggerToggle, function(e) {
            console.log('acc.minicart.js - ACC.minicart.bindMiniCart() - click on : .' + ACC.minicart.classTriggerToggle);
            e.preventDefault();
            if (ACC.device.type === 'desktop') {
                ACC.minicart.toggleMiniCart();
            } else {
                window.location = $(this).data('mini-cart-url-checkout');
            }
        });

        // Update an item quantity by -
        $(document).on('click', '.js-qty-selector .js-cart-qty-selector-minus', function(e) {
            console.log('acc.minicart.js - ACC.minicart.bindMiniCart() - click on : ' + ACC.minicart.id + ' .js-qty-selector .js-cart-qty-selector-minus');
            e.preventDefault();
            var inputQty = $(this).parents('.js-qty-selector').find('.js-qty-selector-input');
            var lastQtyVal = inputQty.val();
            ACC.productDetail.mainCheckQtySelector(this, 'minus', '.js-cart-qty-selector');
            var newQtyVal = inputQty.val();

            ACC.minicart.updateEntry(this, lastQtyVal, newQtyVal);
        });

        // Update an item quantity by +
        $(document).on('click', '.js-cart-qty-selector-plus', function(e) {
            console.log('acc.minicart.js - ACC.minicart.bindMiniCart() - click on : ' + ACC.minicart.id + ' .js-cart-qty-selector-plus');
            e.preventDefault();
            var inputQty = $(this).parents('.js-qty-selector').find('.js-qty-selector-input');
            var lastQtyVal = inputQty.val();

            console.log('inputQty  : ');
            console.log(inputQty);
            console.log('lastQtyVal ' + lastQtyVal);

            ACC.productDetail.mainCheckQtySelector(this, 'plus', '.js-cart-qty-selector');
            var newQtyVal = inputQty.val();
            console.log('newQtyVal ' + newQtyVal);

            ACC.minicart.updateEntry(this, lastQtyVal, newQtyVal);
        });

        // Remove an item
        $(document).on('click', '.remove-mini-cart-entry-button', function(e) {
            console.log('acc.minicart.js - ACC.minicart.bindMiniCart() - click on : .remove-mini-cart-entry-button');
            var productCode = $(this).data('productcode');
            var productQuantity = $(this).data('quantity');
            var productName = $(this).data('productname');
            var productCategories = $(this).data('productcategories');
            var productBrand = $(this).data('productbrand');
            var productPrice = $(this).data('productprice');

            e.preventDefault();
            console.log('REMOVE');
            ACC.minicart.updateEntry(this, 0, 0);
            ACC.track.trackRemoveFromCart(productCode, productQuantity, productName, productCategories, productBrand, productPrice);
        });

        // Close the miniCart
        $(document).on('click', '.js-mini-cart-close-button', function(e) {
            console.log('acc.minicart.js - ACC.minicart.bindMiniCart() - click on : .js-mini-cart-close-button');
            e.preventDefault();
            ACC.colorbox.close();
            ACC.minicart.hideCart();
        });
        $(document).on('click', '.js-resize', function(e) {
            if (ACC.minicart.active) {
                ACC.minicart.resizeRuler();
            }
        });
        $(document).on('click', function(e) {
            console.log('acc.minicart.js - ACC.minicart.bindMiniCart() - click on : document');
            console.log(e.target.id);
            console.log($(e.target).parents('.cart'));
            if ((e.target.id !== 'cart' && !$(e.target).parents('#cart').length)) {
                ACC.minicart.hideCart();
            }
        });

        window.addEventListener('resize', function() {
            if (ACC.minicart.opened) {
                ACC.minicart.hideCart();
            }
            ACC.minicart.resizeRuler();
        });
    },
    // Get html content of minicart
    getContent: function() {
        console.log('acc.minicart.js - ACC.minicart.getContent() : executed');
        var url = $('.' + ACC.minicart.classTriggerToggle).data('miniCartUrl');
        var $target = $(ACC.minicart.id + ' .' + ACC.minicart.classWrapperContent);
        $.ajax({
            url: url,
            cache: false,
            type: 'GET',
            success: function(jsonData) {
                console.log('acc.minicart.js - ACC.minicart.getContent() - ajax call : success');
                $('.' + ACC.minicart.classLoader).fadeOut('slow', function() {
                    $(this).removeClass(ACC.minicart.classActive);
                    $target.html(jsonData);
                    $target.show();
                    ACC.minicart.showCart();
                    ACC.minicart.opened = true;
                    ACC.minicart.active = true;
                    $('.' + ACC.minicart.classWrapperContent).fadeIn('fast');
                });
            }
        });
    },
    hideCart: function() {
        console.log('acc.minicart.js - ACC.minicart.hideCart() : executed');
        if (ACC.minicart.active) {
            $('.' + ACC.minicart.classWrapperContent).fadeOut('fast', function() {
                $(ACC.minicart.id).removeClass(ACC.minicart.classActive); // remove class to display the minicart
                ACC.minicart.active = false; // change minicart state
                $('#underHeader').removeClass('underHeader_overlay');
            });
        }
    },
    // Init minicart content
    initMiniCart: function(alertMessage) {
        console.log('acc.minicart.js - ACC.minicart.initMiniCart() : executed');
        if (ACC.minicart.opened === true) {
            $('.' + ACC.minicart.classWrapperContent).fadeOut('fast', function() {
                $('.' + ACC.minicart.classLoader).fadeIn('fast');
                ACC.minicart.getContent();
            });
        } else {
            ACC.minicart.getContent();
        }
    },
    resize: function(resize, position) {
        console.log('acc.minicart.js - ACC.minicart.resize() : executed');
        var $target = $(ACC.minicart.id);
        var $targetListItem = $(ACC.minicart.id + ' .' + ACC.minicart.classWrapperItemList);
        var mustResize = (typeof resize !== 'undefined' && resize === true ? true : false);
        var mustPositionning = (typeof position !== 'undefined' && position === true ? true : false);

        // $target.css({
        // 	'height': window.innerHeight - $('.header').outerHeight() + $('#cookie-bar').outerHeight() + 'px',
        // 	'top': $('.header').outerHeight() + $('#cookie-bar').outerHeight() + 'px'
        // }); // resizing the minicart wrapper depends on resolutions
        // $targetListItem.css({
        // 	'height': $target.outerHeight() - $('.cart--header').outerHeight() - $('.cart--footer').outerHeight() + 'px',
        // });
        $target.css({
            'opacity': '0',
            'visibility': 'visible',
            'height': '',
            'top': ''
        });
        $targetListItem.css({
            'opacity': '0',
            'visibility': 'visible',
            'height': ''
        });
        if (mustResize) {
            console.log('acc.minicart.js - ACC.minicart.resize() : mustResize');
            $target.animate({
                'opacity': '1',
                'height': window.innerHeight - $('.header').outerHeight() - $('#cookie-bar').outerHeight() + 'px'
            }, 0, function() {
                console.log('acc.minicart.js - ACC.minicart.resize() : ' + $target.outerHeight() - $('.cart--header').outerHeight() - $('.cart--footer').outerHeight() + 'px');
                $targetListItem.css({
                    'opacity': '1',
                    'height': $target.outerHeight() - $('.cart--header').outerHeight() - $('.cart--footer').outerHeight() + 'px'
                });
            });
        }
        if (mustPositionning) {
            $target.css({
                'opacity': '1',
                'top': $('.header').outerHeight() + $('#cookie-bar').outerHeight() + 'px'
            });
            $targetListItem.css({
                'opacity': '1'
            });
        }
    },
    resizeRuler: function() {
        console.log('acc.minicart.js - ACC.minicart.resizeRuler() : executed');
        if (ACC.device.type === 'desktop') {
            console.log('acc.minicart.js - ACC.minicart.resizeRuler() - desktop');
            ACC.minicart.resize(true, true); // resize and positionning of the minicart wrapper
        }
        if (ACC.device.type === 'mobile') {
            console.log('acc.minicart.js - ACC.minicart.resizeRuler() - mobile');
            ACC.minicart.resize(false, true); // positionning of the minicart wrapper without resizing
        }
    },
    showCart: function() {
        console.log('acc.minicart.js - ACC.minicart.showCart() : executed');
        var $target = $(ACC.minicart.id);
        ACC.minicart.resizeRuler();
        $target.addClass(ACC.minicart.classActive); // add class to display the minicart
        $('#underHeader').addClass('underHeader_overlay');
    },
    // hide or open then update minicart, depends on display state
    toggleMiniCart: function(alertMessage) {
        console.log('acc.minicart.js - ACC.minicart.toggleMiniCart() : executed');
        if (ACC.minicart.active) {
            ACC.minicart.hideCart();
        } else {
            if (ACC.minicart.opened) {
                ACC.minicart.updateContent(alertMessage);
            } else {
                ACC.minicart.initMiniCart(alertMessage);
            }
        }
    },
    toggleContentScroll: function() {},
    // Replace html content of minicart
    updateContent: function() {
        var url = $('.' + ACC.minicart.classTriggerToggle).data('miniCartUrl');
        var $target = $(ACC.minicart.id + ' .' + ACC.minicart.classWrapperContent);
        $target.fadeOut('fast', function() {
            ACC.minicart.showCart();
            $('.' + ACC.minicart.classLoader).fadeIn('fast', function() {
                ACC.minicart.getContent();
            });
        });
    },
    // update or remove one item
    updateEntry: function(self, lastQtyVal, newQtyVal) {
        console.log('acc.minicart.js - ACC.minicart.updateEntry() : executed');
        var url = $(self).data('miniCartUrl');
        var productCode = $(self).data('productcode');
        var entryNumber = $(self).data('entrynumber');
        var urlAndParams = url + '?productCode=' + productCode + '&entryNumber=' + entryNumber + '&quantity=' + newQtyVal;

        // var firstQuantityInput = $('input[name=quantity_'+entryNumber+']');
        var firstQuantityInput = $('#cart div.qty[data-productCode=' + productCode + ']');
        var unit = $(firstQuantityInput).data('unit');
        var hasWarranty = $(self).data('hasWarranty');

        var ajax;

        if (ajax) {
            ajax.abort();
        }

        ajax = $.ajax({
            url: urlAndParams,
            cache: false,
            type: 'GET'
        }).done(function(jsonData) {
            console.log('//==== SUCCESS updateEntry ===//');
            console.log(jsonData);
            console.log('//============================//');
            // update miniCart menu
            ACC.minicart.updateMiniCartDisplayResult(jsonData);
            // $(self).parents('.minicartItem').find('')
            // update minicart popup
            ACC.minicart.updatePriceForEntry(entryNumber, parseInt(newQtyVal), jsonData);
            $('.js-number-items-popin').children('span').html(jsonData.miniCartCount);
            $('.js-total-price-popin').html(jsonData.miniCartPrice);
            // reload the popin for a REMOVE, an ALERT or a WARRANTY
            if (newQtyVal === 0 || jsonData.alertMessage || hasWarranty) {
                ACC.minicart.initMiniCart(jsonData.alertMessage);
            } else {
                $('.alert-message').html('');
                // firstQuantityInput.val(newQtyVal + unit);
                if (unit !== undefined) {
                    firstQuantityInput.text(newQtyVal + unit);
                } else {
                    firstQuantityInput.text(newQtyVal);
                }
            }
        }).always(function() {
            ajax = undefined;
        });
    },

    // Get the new minicart total values
    updateMiniCartDisplay: function() {
        console.log('acc.minicart.js - ACC.minicart.updateMiniCartDisplay() : executed');
        var url = $('.' + ACC.minicart.classTriggerToggle).data('miniCartRefreshUrl');
        $.ajax({
            url: url,
            cache: false,
            dataType: 'json',
            type: 'GET',
            success: function(jsonData) {
                console.log('//==== COMPLETE updateMiniCartDisplay ===//');
                console.log(jsonData);
                console.log('//============================//');
                ACC.minicart.updateMiniCartDisplayResult(jsonData);
            },
            error: function(msg) {
                console.log('//==== ERROR updateMiniCartDisplay ===//');
                console.log(msg);
                console.log('//============================//');
            }
        });
    },

    // Update every value in minicart and its icon displayed
    updateMiniCartDisplayResult: function(jsonData) {
        console.log('acc.minicart.js - ACC.minicart.updateMiniCartDisplayResult() : executed');
        ACC.minicart.updateMinicartCounter(jsonData.miniCartCount);
        ACC.minicart.updateMinicartTotal(jsonData.totalPrice, jsonData.totalDiscounts, jsonData.subTotal);
        $(document).find(' .js-mini-cart-price .price--value').html(jsonData.totalPrice);
    },

    // Update display of minicart item counter
    updateMinicartCounter: function(nbItems) {
        console.log('acc.minicart.js - ACC.minicart.updateMinicartCounter() : executed');
        $(document).find('.js-mini-cart-qty').html(nbItems);
    },

    // Update display of minicart total amount
    updateMinicartTotal: function(amount, savings, subTotal) {
        console.log('acc.minicart.js - ACC.minicart.updateMinicartTotal() : executed');
        $(document).find('.js-mini-cart-price span[class^="price--total"]').html(amount);
        $(document).find('.cart--totals-savings-amount .price--base').html(savings);
        $(document).find('.cart--totals-subTotal-amount .price--base').html(subTotal);
    },

    // Update the price for an entry
    updatePriceForEntry: function(entryNumber, newQtyVal, jsonData) {
        var priceDiv = $('.entry' + entryNumber);
        var price = parseFloat(priceDiv.attr('data-productprice'));
        var totalPrice = (newQtyVal * price).toFixed(2);
        priceDiv.find('.price--total').html((newQtyVal * price).toFixed(2));
        ACC.minicart.updateMessageReached(totalPrice, jsonData);
    },

    // Update message reached 
    updateMessageReached: function(totalPrice, jsonData) {
        var thresold = document.getElementById('threshold').value;
        var totalCart = jsonData.totalPrice;

        if (totalCart >= thresold) {
            $('#cart--warning').hide();
            $('.mini-cart-checkout-button').removeClass("disabled");
            $('#cart--warning--hiden').addClass("hidden");


        } else {
            var remainingPrice = thresold - totalCart;
            if (document.getElementById('cart--warning--hiden') !== null && totalCart < thresold) {
                $('#cart--warning--hiden').removeClass("hidden");
                $(document).find('.cart--warning .cart--warning-value').html(remainingPrice.toFixed(2));
                $('.mini-cart-checkout-button').addClass("disabled");
            } else {
                $(document).find('.cart--warning .cart--warning-value').html(remainingPrice.toFixed(2));
                $('.mini-cart-checkout-button').addClass("disabled");
                $('#cart--warning').show();
            }

        }

    },

    log: function(functionName) {
        if (typeof functionName === 'undefined') {
            functionName = 'unknwon';
        }
        console.log('========== start minicart log : ' + functionName + ' ==========');
        console.log('ACC.minicart.active', ACC.minicart.active);
        console.log('ACC.minicart.opened', ACC.minicart.opened);
        console.log('========== finish minicart log : ' + functionName + ' ==========');
    }
};
