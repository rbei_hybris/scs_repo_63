<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="product" required="true"
	type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ attribute name="galleryImages" required="true" type="java.util.List"%>
<%@ attribute name="galleryImages3D" required="false" type="java.util.List"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>

<c:set var="carouselClass" value="carousel" />

<c:set var="carouselMobileDots" value="true" />
<c:set var="carouselDesktopDots" value="false" />
<c:set var="carouselMobileArrows" value="false" />
<c:set var="carouselDesktopArrows" value="false" />
<c:set var="carouselMobileItems" value="1" />
<c:set var="carouselDesktopItems" value="1" />

<div class="image-gallery">

	<c:if test="${product.discountPrice ne null}">
		<c:choose>
			<c:when test="${currentLanguage.isocode eq 'ar'}">
				<div class="sticker">
					<span class="sticker--price">${product.percentageDiscount}%-</span>
				</div>
			</c:when>
			<c:otherwise>
				<div class="sticker">
					<span class="sticker--price">-${product.percentageDiscount}%</span>
				</div>
			</c:otherwise>
		</c:choose>
	</c:if>
	<c:if test="${product.stickerUrl ne null}">
		<div class="sticker">
			<img src="${product.stickerUrl}" />
		</div>
	</c:if>
	<c:choose>
		<c:when test="${galleryImages == null || galleryImages.size() == 0}">
			<div class="${carouselClass} js-carousel ${carouselClass}--pdp image-gallery__image"  data-carousel-mobile-items="${carouselMobileItems}" data-carousel-mobile-dots="${carouselMobileDots}" data-carousel-desktop-items="${carouselDesktopItems}" data-carousel-desktop-dots="${carouselDesktopDots}" data-carousel-desktop-arrows="${carouselDesktopArrows}" data-carousel-mobile-arrows="${carouselMobileArrows}">
				<div class="item">
						<spring:theme code="img.missingProductImage.responsive.product"
							text="/" var="imagePath" />
						<c:choose>
							<c:when test="${originalContextPath ne null}">
								<c:url value="${imagePath}" var="imageUrl"
									context="${originalContextPath}" />
							</c:when>
							<c:otherwise>
								<c:url value="${imagePath}" var="imageUrl" />
							</c:otherwise>
						</c:choose>
						<img class="lazyOwl" src="${imageUrl}" />
				</div>
			</div>
			<div class="empty--360--product">
				<product:productGalleryThumbnail galleryImages="${galleryImages}" galleryImages3D="${galleryImages3D}" />
			</div>
		</c:when>
		<c:otherwise>

			<div class="${carouselClass} js-carousel ${carouselClass}--pdp image-gallery__image"  data-carousel-mobile-items="${carouselMobileItems}" data-carousel-mobile-dots="${carouselMobileDots}" data-carousel-desktop-items="${carouselDesktopItems}" data-carousel-desktop-dots="${carouselDesktopDots}" data-carousel-desktop-arrows="${carouselDesktopArrows}" data-carousel-mobile-arrows="${carouselMobileArrows}">
				<c:forEach items="${galleryImages}" var="container"
					varStatus="loop">
					<div class="item zoom">
						<img  id="imageProductZoom_${loop.index}" src="${container.zoom.url}" alt="no image available" />
					</div>
				</c:forEach>
			</div>
			<product:productGalleryThumbnail galleryImages="${galleryImages}" galleryImages3D="${galleryImages3D}" />
		</c:otherwise>
	</c:choose>
</div>
