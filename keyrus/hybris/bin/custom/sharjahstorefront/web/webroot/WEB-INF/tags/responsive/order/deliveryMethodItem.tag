<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<spring:htmlEscape defaultHtmlEscape="true"/>
<br class="visible-xs">
<div class="delivery-method">
    <div class="delivery-method--title">
        <spring:theme code="text.shippingMethod"/>
    </div>
	<div>
		${order.deliveryMode.name}
		<br>
		${order.deliveryMode.description}
	</div>
</div>
