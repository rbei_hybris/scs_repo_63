var SLOTS = {
	classSlot: 'deliverySlot',
	classSlotSelected: 'selected',
	classSlotAvailable: 'available'
};

SLOTS.selected = {
	"TRUE": true,
	"FALSE": false
};

SLOTS.refreshSlots = function() {
	$.each($("#divDeliverySlotsTable .deliverySlot"), function() {
		if ($(this).data("selected") === SLOTS.selected.TRUE) {
			$(this).addClass(SLOTS.classSlotSelected);
		} else {
			$(this).removeClass(SLOTS.classSlotSelected);
		}
	});
};

SLOTS.capitalize = function(text) {
	return text.charAt(0).toUpperCase() + text.slice(1);
};

deliveryPerferedSlot = function() {
    var valueSelected = document.getElementById('deliveryPerferedSlot').value;
    var slot = this;
	var fields = valueSelected.split(',');
	var pos = fields[0];
	var date = fields[1];
	var codeSlot = fields[2];
	var day = fields[3];
	// Save the preferred delivery slot
	$.ajax({
		url: "save-preferred-slot",
		data: {
			pos: pos,
			date: date,
			slotCode: codeSlot
		},
		type: "post",
		dataType: "json",
		success: function() {
			console.log('scs.delivery-address.js - Save the preferred delivery slot : SUCCESS');
			$("#divFavoriteSlot").show();
			var dayName = SLOTS.capitalize(day);
			$("#divFavoriteSlot .lnkShowSlots").text(" " + dayName);
			$("#divShowSlots").hide();
			$("#divSelectSlot").hide();
		},
		error: function() {
			console.error("scs.delivery-address.js - Error saving the preferred delivery slot");
			$("#divSelectSlot").hide();
		}
	});

};

$(function() {
	SLOTS.refreshSlots();

	$(".deliverySlot").css("cursor", "pointer");

	$(".lnkShowSlots").click(function() {
		$("#divSelectSlot").toggle();
	});

	$("#divDeliverySlotsTable .deliverySlot").click(function() {
		if ($(this).data("status") === "unavailable") {
			return;
		}

		var $this = $(this);
		// Save the selected delivery slot
		$.ajax({
			url: "save-selected-slot",
			data: {
				pos: $(this).data("pos"),
				date: $(this).data("date"),
				slotCode: $(this).data("slot-code")
			},
			type: "post",
			dataType: "json",
			success: function() {
				console.log('scs.delivery-address.js - Save the selected delivery slot : SUCCESS');
				// Mark the previously selected slot as available, in case it was the last place left
				$("#divDeliverySlotsTable .deliverySlot[data-selected='true']").data("status", "available");

				// Deselect all slots first
				$("#divDeliverySlots .deliverySlot").data("selected", SLOTS.selected.FALSE);

				$this.data("selected", SLOTS.selected.TRUE);
				SLOTS.refreshSlots();
			},
			error: function() {
				console.error("scs.delivery-address.js - Error saving the selected delivery slot");
			}
		});
	});

	$('#divDeliverySlotsTable [data-status="' + SLOTS.classSlotAvailable + '"]:not(".selected")').hover(function() {
		SLOTS.saveHtml = $(this).html();
		$(this).html(ACC.config.deliverySlot.statusOk);
	}, function() {
		$(this).html(SLOTS.saveHtml);
	});

	$('#divSelectSlot .' + SLOTS.classSlot).hover(function() {
		SLOTS.saveHtml = $(this).html();
		$(this).html(ACC.config.deliverySlot.statusOk);
	}, function() {
		$(this).html(SLOTS.saveHtml);
	});
});
