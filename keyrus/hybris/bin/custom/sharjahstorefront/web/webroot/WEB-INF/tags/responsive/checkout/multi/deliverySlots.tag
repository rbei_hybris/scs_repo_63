<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>


<c:if test="${not empty deliverySlots and not empty maxDeliverySlots}">
	<div id="divDeliverySlots">
		<h3><spring:theme code="deliverySlots.title" /></h3>

		<div id="divDeliverySlotsTable" style="overflow: auto;">
			<multi-checkout:deliverySlotTable slotRange="${deliverySlots.size()}" />
		</div>

		<c:set value="${not empty preferredSlot}" var="hasPreferredSlot" />
		<div id="divShowSlots" style="display: ${hasPreferredSlot ? 'none' : 'block'};">
			<spring:theme code="deliverySlots.link.select.prefix" /><br>
<!-- 			<a class="lnkShowSlots" href="javascript:void(0);"> -->
<%-- 				<spring:theme code="deliverySlots.link.select.click" /> --%>
<!-- 			</a> -->
			<spring:theme code="deliverySlots.link.select.suffix" />
		</div>

		<div id="divFavoriteSlot" style="display: ${hasPreferredSlot ? 'block' : 'none'};">
			<spring:theme code="deliverySlots.link.select.favorite" />
			<a class="lnkShowSlots" href="javascript:void(0);">
				<c:if test="${hasPreferredSlot}">
					<span id="preferredSlotLabel">${preferredSlot.dayOfWeek}
					&nbsp;${preferredSlot.beginTime}-${preferredSlot.endTime}</span>
				</c:if>
			</a>
		</div>

		<div id="divSelectSlot">
			<multi-checkout:deliverySlotList slotRange="${preferredSlotRange}" />
		</div>
	</div>
</c:if>
