<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="entry" required="true" type="de.hybris.platform.commercefacades.order.data.OrderEntryData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:if test="${entry.product.discountPrice ne null}">
	<format:fromPrice priceData="${entry.product.price}" strike="true"/>
	<format:price priceData="${entry.product.discountPrice}"/>
	<p style="color:red">${entry.product.percentageDiscount }<spring:theme code="product.percentage.off"/></p>
</c:if>
<c:if test="${entry.product.discountPrice eq null}">
	<format:price priceData="${entry.basePrice}" />
</c:if>