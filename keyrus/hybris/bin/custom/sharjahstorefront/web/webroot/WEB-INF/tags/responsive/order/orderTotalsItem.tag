<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>

<%@ attribute name="containerCSS" required="false" type="java.lang.String" %>

<c:choose>
	<c:when test="${continueCheckout eq true}">
		<c:set var="checkoutButtonStyle" value="button--full" />
	</c:when>
	<c:otherwise>
		<c:set var="checkoutButtonStyle" value="disabled" />
	</c:otherwise>
</c:choose>

<div class="cart--totals js-cart--totals">
	<div class="cart--totals-title"><spring:theme code="basket.page.totals.total" /></div>
	<div class="cart--totals-line cart--totals-subtotal">
		<div class="cart--totals-left cart--totals-total-text">
			<spring:theme code="basket.page.totals.subtotal.original"/>
		</div>
		<div class="cart--totals-right">
			<span class="price--currency">${order.totalPrice.currencyIso}&nbsp;</span>
			<span class="price--base">${order.subTotal.value}</span>
		</div>
	</div>
	<div class="cart--totals-line cart--totals-qty">
		<div class="cart--totals-left cart--totals--qty-text js-number-items-popin"><spring:theme code="basket.page.totals.total.items" arguments="${order.totalUnitCount}"/></div>
		<div class="cart--totals-right">&nbsp;</div>
	</div>
	<div class="cart--totals-saving_shipping">
		<c:choose>
			<c:when test="${not empty order.deliveryCost}">
				<div class="cart--totals-line cart--totals-shipping">
					<div class="cart--totals-left cart--totals-shipping-text"><spring:theme code="basket.page.shipping"/></div>
					<div class="cart--totals-right cart--totals-shipping-amount"><format:priceDom priceData="${order.deliveryCost}" displayFreeForZero="TRUE"/></div>
				</div>
			</c:when>
			<c:when test="${empty order.deliveryCost and empty cartThresholdConfigurationData}">
				<div class="cart--totals-line cart--totals-shipping">
					<div class="cart--totals-left cart--totals-shipping-text"><spring:theme code="basket.page.shipping"/></div>
					<div class="cart--totals-right cart--totals-shipping-amount"><spring:theme code="text.free"/></div>
				</div>
			</c:when>
		</c:choose>
	</div>
	<c:if test="${order.totalDiscounts.value > 0}">
			<div class="cart--totals-line cart--totals-savings">
				<div class="cart--totals-left cart--totals-savings-text"><spring:theme code="basket.page.totals.savings"/></div>
				<div class="cart--totals-right cart--totals-savings-amount">
					<ycommerce:testId code="Order_Totals_Savings"><format:priceDom priceData="${order.totalDiscounts}"/></ycommerce:testId>
				</div>
			</div>
		</c:if>
	<div class="cart--totals-line cart--totals-total">
		<div class="cart--totals-left cart--totals-total-text"><spring:theme code="basket.page.total"/></div>
		<div class="cart--totals-right cart--totals-total-amount js-mini-cart-price">
			<ycommerce:testId code="cart_totalPrice_label">
				<c:choose>
					<c:when test="${showTax}">
						<format:priceTotalDom priceData="${order.totalPriceWithTax}"/>
					</c:when>
					<c:otherwise>
						<format:priceTotalDom priceData="${order.totalPrice}"/>
					</c:otherwise>
				</c:choose>
			</ycommerce:testId>
		</div>
	</div>
<%-- 	<c:if test="${not order.net}"> --%>
<!-- 		<div class="cart--totals-taxes"> -->
<%-- 			<ycommerce:testId code="orderTotal_includesTax_label"> --%>
<%-- 				<spring:theme code="text.account.order.total.includesTax" argumentSeparator=";" --%>
<%-- 							  arguments="${order.totalTax.formattedValue}"/> --%>
<%-- 			</ycommerce:testId> --%>
<!-- 		</div> -->
<%-- 	</c:if> --%>
</div>
