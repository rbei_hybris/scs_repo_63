<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="facetData" required="true" type="de.hybris.platform.commerceservices.search.facetdata.FacetData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>


<c:if test="${not empty facetData.values}">
<ycommerce:testId code="facetNav_title_${facetData.name}">
	<div class="product-facet--container js-facet">
		<div class="product-facet--name js-facet-name">
			<span class="facet__arrow">
				<span><spring:theme code="search.nav.facetTitle" arguments="${facetData.name}"/></span>
			</span>
		</div>


		<div class="product-facet--values js-facet-values js-facet-form">

			<c:if test="${not empty facetData.topValues}">
				<ul class="product-facet--values-list js-facet-list js-facet-top-values">
					<c:forEach items="${facetData.topValues}" var="facetValue">
					<li>
						<c:if test="${facetData.multiSelect}">
							<ycommerce:testId code="facetNav_selectForm">
							<form action="#" method="get">
								<input type="hidden" name="q" value="${facetValue.query.query.value}"/>
								<input type="hidden" name="text" value="${searchPageData.freeTextSearch}"/>
								<label>
									<input type="checkbox" class="facet-checkbox facet__list__checkbox js-facet-checkbox" value=""  ${facetValue.selected ? 'checked="checked"' : ''}>
									<span class="product-facet-alias">${facetValue.name}&nbsp;</span>
									<ycommerce:testId code="facetNav_count">
										<span class="product-facet--count"><spring:theme code="search.nav.facetValueCount" arguments="${facetValue.count}"/></span>
									</ycommerce:testId>
								</label>
							</form>
							</ycommerce:testId>
						</c:if>
						<c:if test="${not facetData.multiSelect}">
							<c:url value="${facetValue.query.url}" var="facetValueQueryUrl"/>
							<a href="${facetValueQueryUrl}">
								<label>
									<span>${facetValue.name}</span>
									<ycommerce:testId code="facetNav_count">
										<span class="product-facet--count"><spring:theme code="search.nav.facetValueCount" arguments="${facetValue.count}"/></span>
									</ycommerce:testId>
								</label>
							</a>
						</c:if>
					</li>
					</c:forEach>
				</ul>
			</c:if>
			<ul class="product-facet--values-list js-facet-list<c:if test="${not empty facetData.topValues}"> facet__list--hidden js-facet-list-hidden</c:if>">
				<c:forEach items="${facetData.values}" var="facetValue">
					<li>
						<c:if test="${facetData.multiSelect}">
							<ycommerce:testId code="facetNav_selectForm">
							<form action="#" method="get">
								<input type="hidden" name="q" value="${facetValue.query.query.value}"/>
								<input type="hidden" name="text" value="${searchPageData.freeTextSearch}"/>
								<label>
									<input type="checkbox" class="facet-checkbox facet__list__checkbox js-facet-checkbox" value=""  ${facetValue.selected ? 'checked="checked"' : ''}>
									<span class="product-facet-alias">${facetValue.name}&nbsp;</span>
									<ycommerce:testId code="facetNav_count">
									<span class="product-facet--count"><spring:theme code="search.nav.facetValueCount" arguments="${facetValue.count}"/></span>
									</ycommerce:testId>
								</label>
							</form>
							</ycommerce:testId>
						</c:if>
						<c:if test="${not facetData.multiSelect}">
							<c:url value="${facetValue.query.url}" var="facetValueQueryUrl"/>
							<a href="${facetValueQueryUrl}">
								<label>
									<span>${facetValue.name}</span>
									<ycommerce:testId code="facetNav_count">
										<span class="product-facet--count"><spring:theme code="search.nav.facetValueCount" arguments="${facetValue.count}"/></span>
									</ycommerce:testId>
								</label>
							</a>
						</c:if>
					</li>
				</c:forEach>
			</ul>

			<c:if test="${not empty facetData.topValues}">
				<label>
					<span class="product-facet--values-more js-more-facet-values">
						<a href="#" class="js-more-facet-values-link red-link" ><spring:theme code="search.nav.facetShowMore_${facetData.code}" /></a>
					</span>
				</label>
				<label>
					<span class="product-facet--values-less js-less-facet-values">
						<a href="#" class="js-less-facet-values-link red-link"><spring:theme code="search.nav.facetShowLess_${facetData.code}" /></a>
					</span>
				</label>
			</c:if>
		</div>
	</div>
</ycommerce:testId>
</c:if>
