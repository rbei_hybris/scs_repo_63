<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags"%>

<c:if test="${(not empty accConfMsgs) || (not empty accInfoMsgs) || (not empty accErrorMsgs)}">
	<div class="global-alerts">

		<%-- Information (confirmation) messages --%>
		<c:if test="${not empty accConfMsgs}">
			<c:forEach items="${accConfMsgs}" var="msg">
				<div class="alert-msg alert-info alert-dismissable">
					<div class="icon icon--close close" aria-hidden="true" data-dismiss="alert">&nbsp;</div>
					<spring:theme code="${msg.code}" arguments="${msg.attributes}"/>
				</div>
			</c:forEach>
		</c:if>

		<%-- Warning messages --%>
		<c:if test="${not empty accInfoMsgs}">
			<c:forEach items="${accInfoMsgs}" var="msg">
				<div class="alert-msg alert-warning alert-dismissable">
					<div class="icon icon--close close" aria-hidden="true" data-dismiss="alert">&nbsp;</div>
					<spring:theme code="${msg.code}" arguments="${msg.attributes}"/>
				</div>
			</c:forEach>
		</c:if>

		<%-- Error messages (includes spring validation messages)--%>
		<c:if test="${not empty accErrorMsgs}">
			<c:forEach items="${accErrorMsgs}" var="msg">
				<div class="alert-msg alert-danger alert-dismissable">
					<div class="icon icon--close close" aria-hidden="true" data-dismiss="alert">&nbsp;</div>
					<spring:theme code="${msg.code}" arguments="${msg.attributes}"/>
				</div>
			</c:forEach>
		</c:if>

	</div>
</c:if>
