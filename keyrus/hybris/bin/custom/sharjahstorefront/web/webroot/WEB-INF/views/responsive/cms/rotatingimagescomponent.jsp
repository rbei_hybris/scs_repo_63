<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<c:set var="carouselClass" value="carousel" />

<c:set var="carouselMobileDots" value="true" />
<c:set var="carouselDesktopDots" value="true" />
<c:set var="carouselMobileArrows" value="false" />
<c:set var="carouselDesktopArrows" value="true" />
<c:set var="carouselMobileItems" value="1" />
<c:set var="carouselDesktopItems" value="1" />
<c:set var="carouselWidthSm" value="false" />
<c:set var="carouselArrowsColor" value="white" />
<c:set var="carouselDir" value="${currentLanguage.isocode == 'ar' ? 'rtl' : 'ltr'}" />
<c:set var="carouselAutoplay" value="true" />
<c:set var="carouselSpeed" value="4000" />

<div class="slider_component simple-banner" >
	<div id="" class="${carouselClass} js-carousel ${carouselClass}--simple-banner" data-carousel-mobile-items="${carouselMobileItems}" data-carousel-mobile-dots="${carouselMobileDots}" data-carousel-desktop-items="${carouselDesktopItems}" data-carousel-desktop-dots="${carouselDesktopDots}" data-carousel-desktop-arrows="${carouselDesktopArrows}" data-carousel-mobile-arrows="${carouselMobileArrows}" data-carousel-width-sm="${carouselWidthSm}" data-carousel-arrow-color="${carouselArrowsColor}" data-carousel-dir="${carouselDir}" data-carousel-autoplay="${carouselAutoplay}" data-carousel-speed="${carouselSpeed}">
		<!-- <ul class="slick-track slick-carousel"> -->
			<c:forEach items="${banners}" var="banner" varStatus="status">
				<c:if test="${ycommerce:evaluateRestrictions(banner)}">
					<c:url value="${banner.urlLink}" var="encodedUrl" />
					<div>
						<a tabindex="-1" href="${encodedUrl}"<c:if test="${banner.external}"> target="_blank"</c:if>>

						<picture>
						<source srcset="${banner.mediaDesktop.url}" media="(min-width: 1600px)">
							<source srcset="${banner.mediaDesktop.url}" media="(min-width: 1024px)">
							<source srcset="${banner.mediaMobile.url}" media="(min-width: 768px)">
							<img src="${banner.mediaMobile.url}" alt="${not empty banner.headline ? banner.headline : banner.media.altText}" title="${not empty banner.headline ? banner.headline : banner.media.altText}"/>
						</picture>
						</a>
					</div>
				</c:if>
			</c:forEach>
		<!-- </ul> -->
	</div>
</div>
