<%@ attribute name="supportedCountries" required="true" type="java.util.List"%>
<%@ attribute name="regions" required="true" type="java.util.List"%>
<%@ attribute name="country" required="false" type="java.lang.String"%>
<%@ attribute name="cancelUrl" required="false" type="java.lang.String"%>
<%@ attribute name="addressBook" required="false" type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<c:if test="${not empty deliveryAddresses}">
	<div class="checkout-shipping--choices">
		<div class="headline"><spring:theme code="checkout.summary.shippingAddress"></spring:theme></div>
		<div type="button" class="button button--secondary button--align-center js-address-book">
			<c:if test="${empty sharjahAddressForm.firstName}">
				<spring:theme
					code="checkout.checkout.multi.deliveryAddress.viewAddressBook"/>
			</c:if>
			<c:if test="${not empty sharjahAddressForm.firstName}">
				<spring:theme
					code="checkout.checkout.multi.deliveryAddress.viewAddressBook.different" />
			</c:if>
		</div>
	</div>
</c:if>

<c:if test="${empty addressFormEnabled or addressFormEnabled}">
	<form:form method="post" commandName="sharjahAddressForm" modelAttribute="sharjahAddressForm" class="form">
		<form:hidden path="addressId" class="add_edit_delivery_address_id" status="${not empty suggestedAddresses ? 'hasSuggestedAddresses' : ''}" />
		<input type="hidden" name="bill_state" id="address.billstate" />

		<div id="countrySelector" data-address-code="${addressData.id}"
			data-country-iso-code="${addressData.country.isocode}"
			class="form-group">
		</div>
		<div id="i18nAddressForm" class="i18nAddressForm">
				<address:addressFormElements regions="${regions}"
					country="AE" />
		</div>
		<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
			<div class="checkbox">
				<c:choose>
					<c:when test="${showSaveToAddressBook}">
						<formElement:formCheckbox idKey="saveAddressInMyAddressBook"
							labelKey="checkout.summary.deliveryAddress.saveAddressInMyAddressBook"
							path="saveInAddressBook" inputCSS="add-address-left-input"
							labelCSS="add-address-left-label" mandatory="true" />
					</c:when>
					<c:when test="${not addressBookEmpty && not isDefaultAddress}">
						<ycommerce:testId code="editAddress_defaultAddress_box">
							<formElement:formCheckbox idKey="defaultAddress"
								labelKey="address.default" path="defaultAddress"
								inputCSS="add-address-left-input"
								labelCSS="add-address-left-label" mandatory="true" />
						</ycommerce:testId>
					</c:when>
				</c:choose>
			</div>
		</sec:authorize>
			<c:choose>
				<c:when test="${edit eq true && not addressBook}">
					<ycommerce:testId code="multicheckout_saveAddress_button">
						<div id="addressform_button_panel" class="form-actions">
							<button
								class="positive right change_address_button show_processing_message button button--secondary"
								type="submit">
								<spring:theme code="checkout.multi.saveAddress"
									text="Save address" />
							</button>
						</div>
					</ycommerce:testId>
				</c:when>
				<c:when test="${addressBook eq true}">
					<div id="addressform_button_panel" class="form-actions">
						<div class="accountActions">
							<div class="row sharjahAddressForm-form--bottom-button-row">
								<div class="col-sm-6 col-sm-push-6 accountButtons">
									<ycommerce:testId code="editAddress_saveAddress_button">
										<button class="btn-block change_address_button show_processing_message button button--secondary"
												type="submit">
											<spring:theme code="text.button.save"
														  text="Save" />
										</button>
									</ycommerce:testId>
								</div>
								<div class="col-sm-6 col-sm-pull-6 accountButtons">
									<ycommerce:testId code="editAddress_cancelAddress_button">
										<c:url value="${cancelUrl}" var="cancel"/>
										<a class="button button--secondary" href="${cancel}">
											<spring:theme code="text.button.cancel"
														  text="Cancel" />
										</a>
									</ycommerce:testId>
								</div>
							</div>
						</div>
					</div>
				</c:when>
			</c:choose>
	</form:form>
</c:if>
