<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<spring:theme code="please.select" var="selectElt"/>

<div class="account-section-header ${noBorder}">
				<span class="glyphicon account-section-header-orderhistory ${currentLanguage.isocode == 'ar' ? 'glyphicon-chevron-right' : ' glyphicon-chevron-left'}"></span>
	<span> <spring:theme code="text.account.profile.updatePersonalDetails" /></span>
</div>

<div class="row">
    <div>
        <div class="account-section-content">
            <div class="account-section-form">
                <form:form action="update-profile" class="personal-details-form" method="post" commandName="sharjahUpdateProfileForm">
					<div class="row personal-details-form--shareholder-row">
						<div class="col-md-2">
							<div class="form-group">
								<div class="form-label">
									<spring:theme code="profile.shareholderNumberPrefix"/>
								</div>
							</div>
							<div class="form-inline">
								<c:choose>
									<c:when test="${currentLanguage.isocode eq 'en'}">
										<formElement:formInputBox idKey="profile.shareholderNumberPrefix" placeholder="X"  path="shareholderNumberPrefix" size="1" maxlength="1" inputCSS="text" colCSS=" col-xs-4"/>
										<formElement:formInputBox idKey="profile.shareholderNumber" placeholder="XXXXX"  path="shareholderNumber" size="5" maxlength="5" inputCSS="text" colCSS=" col-xs-7"/>
									</c:when>
									<c:otherwise>
										<formElement:formInputBox idKey="profile.shareholderNumber" placeholder="XXXXX"  path="shareholderNumber" size="5" maxlength="5" inputCSS="text" colCSS=" col-xs-7"/>
										<formElement:formInputBox idKey="profile.shareholderNumberPrefix" placeholder="X"  path="shareholderNumberPrefix" size="1" maxlength="1" inputCSS="text" colCSS=" col-xs-4"/>
									</c:otherwise>
								</c:choose>
								<div class="form-label">
									<a href="<c:url value="/becomeamember"/>"><spring:theme code="register.membershipPrefixNumber.link.label" /></a>
								</div>
							</div>
						</div>
						<div class="col-md-10">
							<div class="form-group">
								<div class="form-label">
									<spring:theme code="register.cards.description"/>
								</div>
							</div>
							<img class="personal-details-form--card-picture card-picture-mobile" src="${themeResourcePath}/images/card-01.jpg">
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<formElement:formInputBox idKey="profile.firstName" labelKey="profile.firstName" path="firstName" inputCSS="text" mandatory="true"/>
						</div>
						<div class="col-md-6">
							<formElement:formInputBox idKey="profile.lastName" labelKey="profile.lastName" path="lastName" inputCSS="text" mandatory="true"/>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<formElement:formInputBox idKey="profile.email" labelKey="profile.email" path="email" inputCSS="text" mandatory="true" disabled="true" />
						</div>
						<div class="col-md-6">
							<label class="form-label">
								<spring:theme code="profile.mobileprefixNumber"/>
							</label>
							<div>
								<div class="row">
								<c:choose>
									<c:when test="${currentLanguage.isocode eq 'en'}">
									<div class="col-md-2 col-xs-3 form-col">
										<formElement:formInputBox idKey="profile.mobileprefixNumber" path="mobileprefixNumber" inputCSS="personal-details-form--mobile-input mobile-input" mandatory="true" value="+971" disabled="true"/>
									</div>
									<div class="col-md-2 col-xs-3 form-col">
	                           <formElement:formInputBox idKey="profile.mobileSecondPrefixNumber"  path="mobileSecondPrefixNumber" inputCSS="mobile-input"  mandatory="true" value="(0)" disabled="true" />
	                        </div>
									<div class="col-md-8 col-xs-6">
										<formElement:formInputBox idKey="profile.phoneNumber" path="phoneNumber" inputCSS="text personal-details-form--mobile-phone-part-2" mandatory="true" placeholder="XXXXXXXXX"/>
									</div>
									</c:when>
									<c:otherwise>
									<div class="col-md-8 col-xs-6">
										<formElement:formInputBox idKey="profile.phoneNumber" path="phoneNumber" inputCSS="text personal-details-form--mobile-phone-part-2" mandatory="true" placeholder="XXXXXXXXX"/>
									</div>
									<div class="col-md-2 col-xs-3 form-col">
	                           <formElement:formInputBox idKey="profile.mobileSecondPrefixNumber"  path="mobileSecondPrefixNumber" inputCSS="mobile-input"  mandatory="true" value="(0)" disabled="true" />
	                        </div>
									<div class="col-md-2 col-xs-3 form-col">
										<formElement:formInputBox idKey="profile.mobileprefixNumber" path="mobileprefixNumber" inputCSS="personal-details-form--mobile-input mobile-input" mandatory="true" value="971+" disabled="true"/>
									</div>
									</c:otherwise>
								</c:choose>
								</div>
							</div>
						</div>
					</div>
					<div class="row row--separation">
						<div class="col-md-6">
							<div class="double-inline-radio-boxes">
								<formElement:formRadioBox labelKey="profile.languageCode" items="${languageData}" idKey="profile.languageCode" path="languageCode"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="double-inline-radio-boxes">
								<formElement:formRadioBox labelKey="profile.genderCode" items="${genderData}" idKey="profile.genderCode" path="genderCode"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<label class="form-label"><spring:theme code="profile.birthday"/></label>
							<div>
								<div class="row">
									<div class="col-xs-4 col-md-2">
										<formElement:formInputBox idKey="profile.dayOfBirth" placeholder="XX" path="dayOfBirth" size="2" maxlength="2" inputCSS="text" />
									</div>
									<div class="col-xs-4 col-md-2">
										<formElement:formInputBox idKey="profile.monthOfBirth" placeholder="XX" path="monthOfBirth" size="2" maxlength="2" inputCSS="text" />
									</div>
									<div class="col-xs-4 col-md-2">
										<formElement:formInputBox idKey="profile.yearOfBirth" placeholder="XXXX" path="yearOfBirth" size="4" maxlength="4" inputCSS="text" />
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<formElement:formInputBox labelKey="profile.landLine"  idKey="profile.landLine" path="landLine"/>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<formElement:formSelectBox skipBlankMessageKey="${selectElt}" selectCSSClass="form-control" labelKey="profile.maritalStatus" items="${maritalStatusData}" idKey="profile.maritalStatus" path="maritalStatus"/>
						</div>
						<div class="col-md-6">
							<formElement:formSelectBox skipBlankMessageKey="${selectElt}" selectCSSClass="form-control" labelKey="profile.numberOfDependants" items="${dependantsData}" idKey="profile.numberOfDependants" path="numberOfDependants"/>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<formElement:formSelectBox skipBlankMessageKey="${selectElt}"  selectCSSClass="form-control" labelKey="profile.nationality" items="${nationalityData}" idKey="profile.nationality" path="nationality"/>
						</div>
						<div class="col-md-6">
							<formElement:formSelectBox skipBlankMessageKey="${selectElt}" selectCSSClass="form-control" labelKey="profile.houseHoldSize" items="${householdSizeData}" idKey="profile.houseHoldSize" path="houseHoldSize"/>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<formElement:formSelectBox skipBlankMessageKey="${selectElt}" selectCSSClass="form-control" idKey="profile.howDidYouFindUs" items="${hdfuData}" labelKey="profile.howDidYouFindUs" path="howDidYouFindUs" />
						</div>
					</div>

                    <div class="row personal-details-form--bottom-button-row">
                        <div class="col-md-6 col-md-push-6">
                            <div class="accountActions">
                                <ycommerce:testId code="personalDetails_savePersonalDetails_button">
                                    <button type="submit" class="button button--full button--secondary">
                                        <spring:theme code="text.account.profile.saveUpdates" text="Save Updates"/>
                                    </button>
                                </ycommerce:testId>
                            </div>
                        </div>
                        <div class="col-md-6 col-md-pull-6">
                            <div class="accountActions">
                                <ycommerce:testId code="personalDetails_cancelPersonalDetails_button">
                                    <button type="button" class="button button--full button--secondary">
                                        <spring:theme code="text.account.profile.cancel" text="Cancel"/>
                                    </button>
                                </ycommerce:testId>
                            </div>
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</div>
