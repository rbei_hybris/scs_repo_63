<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="account" tagdir="/WEB-INF/tags/addons/kaccwishlist/responsive/account" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>

<body>
	<div class="account-section my-account-wishlist">
	
		<div class="add-message"></div>

	<div class="page-cartPage cart--continue-shopping-button-container">
		<a class="button cart--continue-shopping-button" href="/${currentLanguage.isocode}/cart">
			<spring:theme code="wishlist.view.cart"/>
		</a>
	</div>
	
		<br>
	
		<div>

			<div>

				<div class="account-section-header ${noBorder}">
					<span class="glyphicon account-section-header-orderhistory ${currentLanguage.isocode == 'ar' ? 'glyphicon-chevron-right' : ' glyphicon-chevron-left'}"></span>
				   <span>${selectedWishList}</span>
					<span class="linksWishlist">
						<a href="#" class="js-wishlist-rename" title="<spring:theme code="kacc.wishlist.account.renameWishlist"/>"><spring:theme code="wishlist.rename.popup.submit"/></a>
						<a href="#" class="js-wishlist-delete" data-wishlist-item-name="${selectedWishList}"><spring:theme code="wishlist.remove.popup.submit"/></a>
						<a href="#" onclick="window.print()" class="a2a_s__default" >
                                  <span><spring:theme code="product.share.print"/></span>  
						</a>
						<div class="tools">
							<c:set var="selectedWishList" value="${selectedWishList}"/>
							<c:set var="selectedWishList2" value="${fn:replace(selectedWishList,' ','%20')}"/>
							<c:set var="urlResources"  value="${contextPath}/_ui/addons/kaccwishlist/${fn:toLowerCase(uiExperienceLevel)}/common"/>
							<div class="a2a_kit a2a_kit_size_32 a2a_default_style">
								<a href="#" class="a2a_s__default js-wishlist-share linkToPopin">
                                     <span class="a2a_label-print"><spring:theme code="product.share.email"/></span>  
								</a>
							</div>
							<script async src="https://static.addtoany.com/menu/page.js"></script>
						</div>
						<div>
						</div class="tools">
					</span>
				</div>
		
				<input type="hidden" id="wishlistName" value="${selectedWishList}" />

			</div>

			

		</div>		

	
<!-- 		<p class="alert alert-info"> -->
<%-- 			<strong> <spring:theme code="kacc.wishlist.account.shortText"/></strong><br>  --%>
<%-- 			<span> <spring:theme code="kacc.wishlist.account.longText"/> </span> --%>
<!-- 		</p> -->
		
		<div class="wishlistShare">
		
			<c:choose>
				<c:when test="${not empty wishlistEntries}">
			
					<!-- ADD SELECTED ITEMS TO CART (TOP) -->
					<div class="row row-left">
						<div class="checkbox selectAll">
							<input type="checkbox" id="selectAllTop" class="js-wishlist-entry-check-all" />
							<label for="selectAllTop"><spring:theme code="kacc.wishlist.account.entries.selectAll"/></label>
						</div>

						<button class="js-wishlist-add-to-cart wishListAddToCart button button--secondary" data-wishlist-name="${selectedWishList}">
							<spring:theme code="kacc.wishlist.account.addToCart"/>  
						</button>
					</div>
					
					<!-- WISHLIST ENTRIES -->
					<div class="list-content list-content-myWishlist row">
						<c:if test="${not empty allEntries}">
							<!--h2><spring:theme code="text.account.wishlistDetails"/></h2-->
							<table class="cart-list">
							<c:forEach var="entry" items="${allEntries}" varStatus="status">
								<account:wishlistItem entry="${entry}"/>
							</c:forEach>
							</table>
						</c:if>
					</div>

					<div class="row">
						<!-- totals  -->
						<div class="wishListTotals">
							<h2><spring:theme code="basket.page.totals.total"/></h2>
							<table class="cart-totals">
								<tbody>
									<tr class="grand-total">
										<td class="titleTotal"><spring:theme code="basket.page.total"/></td>
										<td>
											<span id="itemsCount"></span>
											<div id="items" style="display:none;">&nbsp;<spring:theme code="basket.items"/></strong></div>
											<div id="item" style="display:none;">&nbsp;<spring:theme code="basket.items.one"/></strong></div>
										</td>
										<td class="price">
											<span id="grandTotalPrice"></span>
										</td>
									</tr>
								</tbody>
							</table>

						</div>
						<div class="clearfix"></div>
			
						<!-- ADD SELECTED ITEMS TO CART (BOTTOM) -->
						<div class="row row-left">
							<div class="checkbox selectAll">
								<input type="checkbox" id="selectAllBottom" class="js-wishlist-entry-check-all" />
								<label for="selectAllBottom"><spring:theme code="kacc.wishlist.account.entries.selectAll"/></label>
							</div>
							<button class="js-wishlist-add-to-cart wishListAddToCart button button--secondary" data-wishlist-name="${selectedWishList}">
								<spring:theme code="kacc.wishlist.account.addToCart"/>  
							</button>
						</div>
					</div>
				</c:when>
				<c:otherwise>
					<br>
					<div class="global-alerts">
						<p class="alert-msg alert-info"><spring:theme code="kacc.wishlist.account.empty.entries.message"/></p>
					</div>
					  <div class="account-section-header-add ">
                    <a href="/" class="button button--secondary ">
                        <spring:theme code="add.items.list"/>
                    </a>
                </div>
					
				</c:otherwise>
			</c:choose>
		</div>
		
		<!-- DELETE WISHLIST POPIN -->
      <div id="whishConfirmationMssg" class="displayPopin"> 
			<p><spring:theme code="kacc.wishlist.account.deleteWishlist"/></p>
			<div class="actions">
				<button class="js-wishlist-delete-cancel button button--secondary">
					<spring:theme code="wishlist.remove.popup.cancelbutton"/>
				</button>
				<button class="confirmDelete js-wishlist-delete-confirm button button--secondary" id="${selectedWishList}" >
					<spring:theme code="wishlist.remove.popup.submit"/>
				</button>
			</div>	
		</div>
		
	</div>

</body>
</html>
