<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<div class="cart--footer-bottom">
	<spring:url value="/my-account/orders" var="orderHistoryUrl" />

	<ycommerce:testId code="orderDetails_backToOrderHistory_button">
		<div class="orderBackBtn cart--totals--back-to-order-history-button">
			<button type="button" class="button button--tertiary"
							data-back-to-orders="${orderHistoryUrl}">
			<spring:theme code="text.account.orderDetails.backToOrderHistory" />
			</button>
		</div>
	</ycommerce:testId>

	<div class="cart--submit">
		<a href="${checkoutUrl}" class="button ${checkoutButtonStyle} button--secondary mini-cart-checkout-button">
			<spring:theme code="text.account.detail.reorder" />
		</a>
	</div>
</div>

