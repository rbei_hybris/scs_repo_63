<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="order" required="true"
	type="de.hybris.platform.commercefacades.order.data.OrderData"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<div class="order-detail-overview">
	<div class="row">
		<div class="col-sm-3">
			<div class="item-group">
				<ycommerce:testId code="orderDetail_overviewOrderID_label">
					<span class="item-label"><spring:theme
							code="text.account.orderHistory.orderNumber" /></span>
					<span class="item-value">${fn:escapeXml(orderData.code)}</span>
				</ycommerce:testId>
			</div>
		</div>
		<c:if test="${not empty orderData.statusDisplay}">
			<div class="col-sm-3">
				<div class="item-group">
					<ycommerce:testId code="orderDetail_overviewOrderStatus_label">
						<span class="item-label"><spring:theme
								code="text.account.orderHistory.orderStatus" /></span>
						<span class="item-value"><spring:theme
								code="text.account.order.status.display.${orderData.statusDisplay}" /></span>
					</ycommerce:testId>
				</div>
			</div>
		</c:if>
		<div class="col-sm-3">
			<div class="item-group">
				<ycommerce:testId code="orderDetail_overviewStatusDate_label">
					<span class="item-label"><spring:theme
							code="text.account.orderHistory.datePlaced" /></span>
					<span class="item-value"><fmt:formatDate
							value="${order.created}" dateStyle="medium" timeStyle="short"
							type="both" /></span>
				</ycommerce:testId>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="item-group">
				<ycommerce:testId code="orderDetail_overviewOrderTotal_label">
					<span class="item-label"><spring:theme
							code="text.account.order.total" /></span>
					<span class="item-value"><format:price
							priceData="${order.totalPrice}" /></span>
				</ycommerce:testId>
			</div>
		</div>
	</div>
	<div id="idAddOrderToWishlist" style="display:none;">
	<div id="idResult"></div>
	<div class="wishListPopin">
		<div class="row">
			<spring:theme code="kacc.wishlist.add.wishlist.choice"/>
		</div>
		<div class="row">
			<select id="idSelectOption">
				<option value="newWishlist" selected="selected">
					<spring:theme code="kacc.wishlist.new.option.message" />
				</option>
				<c:forEach items="${wishlists}" var="wishlist" varStatus="status">
					<option class="classOption" value="${wishlist.name}" ${status.first ? 'selected="selected"' : ''}>
						${wishlist.name}
					</option>
				</c:forEach>
			</select>
			<input id="orderCode" name="orderCode" type="hidden" value="${orderCode}"/>
		</div>
		<div id="newWishlistSection" class="row">
			<label for="newWishlistName">
				<spring:theme code="kacc.wishlist.add.wishlist.message" />
			</label>
			<input id="newWishlistName" name="newWishlistName" type="text" class="required show"
					placeholder="<spring:theme code="kacc.wishlist.add.wishlist.name.placeholder" />" />
		</div>
		<div class="row wishListPopin--button">
			<div class="col-sm-6 col-lg-2">
				<button id="wishlistCancel" class="button--secondary">
					<spring:theme code="kacc.wishlist.add.wishlist.cancel" />
				</button>
			</div>
			<div class="col-sm-6 col-lg-10">
				<spring:theme code="kacc.wishlist.add.wishlist.add" var="wishlistAddButton" />
				<spring:theme code="kacc.wishlist.add.wishlist.create" var="wishlistCreateButton" />
				<button id="idSubmitNewWishlist" class="button--secondary"
						data-add-label="${wishlistAddButton}" data-create-label="${wishlistCreateButton}">
					${wishlistAddButton}
				</button>
			</div>
		</div>
		<p class="alert alert-danger errorValidation errorValidation" style="display:none;"><spring:theme code="kacc.wishlist.empty.wishlist.mandatory"/></p>
		<p class="alert alert-danger errorSize errorSize" style="display:none;"><spring:theme code="kacc.wishlist.empty.wishlist.sizeError"/></p>
	</div>
</div>
</div>
