<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="errorNoResults" required="true" type="java.lang.String"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>

<c:url value="/store-finder" var="storeFinderFormAction" />

<div class="row">
	<div class="col-lg-8">

	<div class="account-section-header">
    <div class="row">
        <div class="container-lg col-md-6">
				<span class="glyphicon account-section-header-orderhistory ${currentLanguage.isocode == 'ar' ? 'glyphicon-chevron-right' : ' glyphicon-chevron-left'}"></span>
				<span><spring:theme code="storeFinder.find.a.store"/></span>
        </div>
    </div>
	</div>

		<div class="store__finder--search">
			<div class="row">
			<form:form action="${storeFinderFormAction}" method="get" commandName="storeFinderForm">
				<div class="col-sm-4">
						<ycommerce:testId code="storeFinder_search_box">
							<div class="input-group">
								<formElement:formInputBox idKey="storelocator-query" labelKey="storelocator.query" path="q" labelCSS="sr-only" inputCSS="form-control searchInputText js-store-finder-search-input" mandatory="true"  placeholder="pickup.search.message" />
								<span class="input-group-btn">
									<button class="btn searchLoop" type="submit" data-search-empty="<spring:theme code="storelocator.error.no.results.subtitle" />">
										<span class="glyphicon glyphicon-search"></span>
									</button>
								</span>
							</div>
						</ycommerce:testId>
				</div>
				<div class="col-sm-4">
					<ycommerce:testId code="storeFinder_nearMe_button">
<!-- 						<button id="findStoresNearMe" class="btn btn-primary btn-block searchBtn" type="button" disabled> -->
<%-- 							<spring:theme code="storeFinder.findStoresNearMe"/> --%>
<!-- 						</button> -->
						<button class="btn button button--secondary btn-block searchBtn " type="submit" data-search-empty="<spring:theme code="storelocator.error.no.results.subtitle" />">
										<spring:theme code="storeFinder.findStoresNearMe"/>
						</button>
					</ycommerce:testId>
				</div>
				</form:form>
			</div>
		</div>
		<div id="noPosFound" class="hide bg-danger"> <spring:theme code="storeFinder.find.no.result"/> </div>

	</div>
</div>
