<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="hideHeaderLinks" required="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/responsive/common/header"  %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"  %>
<%@ taglib prefix="iconSvg" tagdir="/WEB-INF/tags/responsive/iconSvg"  %>
<sec:authorize var="loggedIn" access="isFullyAuthenticated()" />
 

<cms:pageSlot position="TopHeaderSlot" var="component" element="div" class="container">
	<cms:component component="${component}" />
</cms:pageSlot>

<header class="header js-mainHeader">
	<c:if test="${not empty user.gender}">
		<input type="hidden" value="${user.gender.code}" id="myAccountGenderCode"/>
	</c:if>
	
	<c:choose>
			<c:when test="${not empty user.gender && user.gender.code eq 'FEMALE'}">
				   <c:set  var="iconuser" value="icon--woman" />
			</c:when>
			<c:otherwise>
					<c:set  var="iconuser" value="icon--user" />
			</c:otherwise>
	</c:choose>	
	
	<nav class="navigation js-navigation--middle util--wrapper-max">
		<div class="navigation--wrapper">
			<div class="navigation--mobile">
				<div class="navigation--mobile-navbar">
					<div class="navbar">
						<div class="navbar--item navbar--burger">
							<button class="mobile__nav__row--btn btn mobile__nav__row--btn-menu js-toggle-sm-navigation" type="button">
								<span class="icon icon--burger-white"></span>
							</button>
						</div>
						<div div class="navbar--item navbar--content">
							<div class="navbar--content-item navbar--logo">
								<a href="/"><img title="${siteName}" alt="${siteName}" src="${commonResourcePath}/images/logo.png" class="img--full"></a>
							</div>
							<div class=" navbar--icon">
								<c:choose>
									<c:when test="${loggedIn}">
<%-- 								<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')"> --%>
									<c:set var="maxNumberChars" value="25" />
									<c:if test="${fn:length(user.firstName) gt maxNumberChars}">
										<c:set target="${user}" property="firstName" value="${fn:substring(user.firstName, 0, maxNumberChars)}..." />
									</c:if>
									<div class="navbar--icon-item navigation-account-item logged_in js-logged_in liOffcanvas">
										<ycommerce:testId code="header_signOut">
											<a class="logout-label-link" href="<c:url value='/logout'/>">
												<spring:theme code="header.link.logout" />
											</a>
										</ycommerce:testId>
										<a class="icon" href="#"><div class="navbar--log-in--icon icon ${iconuser} myAcctUserIcon visible-xs visible-sm"></div></a>
									</div>
<%-- 								</sec:authorize> --%>
									</c:when>
									<c:otherwise>
<%-- 								<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')" >  --%>
									<div class="navbar--icon-item liOffcanvas">
										<ycommerce:testId code="header_Login_link">
											<a class="signIn" href="<c:url value='/login'/>">
												<div class="navbar--sign-in--icon icon icon--user myAcctUserIcon"></div>
												<span class="navbar--sign-in--link"><spring:theme code="navigation.link.signIn" /> /
												<spring:theme code="navigation.link.register" /></span>
											</a>
										</ycommerce:testId>
									</div>
<%-- 								</sec:authorize> --%>
							</c:otherwise>
						</c:choose>

								<div class="navbar--icon-item liOffcanvas">
									<cms:pageSlot position="MiniCart" var="cart">
										<cms:component component="${cart}"/>
									</cms:pageSlot>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="navigation--mobile-site-search">
					<ycommerce:testId code="header_search_activation_button">
						<div class="site-search">
							<cms:pageSlot position="SearchBox" var="component">
								<cms:component component="${component}" element="div"/>
							</cms:pageSlot>
						</div>
					</ycommerce:testId>
				</div>
			</div>

			<div class="navigation--left">
				<div class="navigation--left--logo js-site-logo">
					<a href="/"><img title="${siteName}" alt="${siteName}" src="${commonResourcePath}/images/logo.png"></a>
				</div>
				<div class="navigation--left--shop hidden-xs hidden-sm">
					<nav:topNavigation />
				</div>
			</div>

			<div class="navigation--middle">
				<div class="navigation-info">
					<ul class="navigation-info--list">
						<li class="navigation-info--item navigation-info--item-about">
							<a href="<c:url value="/aboutus"/>">
								<div class="icon">
									<iconSvg:info />
								</div>
								<div class="text"><spring:theme code="navigation.link.aboutUs"/></div>
							</a>
						</li>
						<li class="navigation-info--item navigation-info--item-storelocator">
								<ycommerce:testId code="header_StoreFinder_link">
									<a href="<c:url value="/store-finder"/>">
										<div class="icon">
											<iconSvg:location />
										</div>
										<div class="text"><spring:theme code="navigation.link.storeFinder"/></div>
									</a>
								</ycommerce:testId>
						</li>
					</ul>
					<div class="navigation-info--language">
						<header:languageSelector languages="${languages}" currentLanguage="${currentLanguage}" />
					</div>
				</div>
				<div class="navigation--middle--site-search">
					<div class="site-search">
						<cms:pageSlot position="SearchBox" var="component">
							<cms:component component="${component}" element="div"/>
						</cms:pageSlot>
					</div>
				</div>
			</div>
			<div class="navigation--right">
				<div class="navigation--right-item nav__right">
					<div class="navigation-account">
						<ul class="nav__links nav__links--account">
								<c:if test="${uiExperienceOverride}">
									<li class="navigation--account-item backToMobileLink"><c:url
											value="/_s/ui-experience?level=" var="backToMobileStoreUrl" />
										<a href="${backToMobileStoreUrl}"> <spring:theme
												code="text.backToMobileStore" />
										</a>
									</li>
								</c:if>
								
								<c:choose>
									<c:when test="${loggedIn}">
<%-- 								<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">55522 --%>
									<c:set var="maxNumberChars" value="25" />
									<c:if test="${fn:length(user.firstName) gt maxNumberChars}">
										<c:set target="${user}" property="firstName" value="${fn:substring(user.firstName, 0, maxNumberChars)}..." />
									</c:if>
									<li class="item logged_in js-logged_in">
										<%--<span class="glyphicon glyphicon-user myAcctUserIcon visible-xs visible-sm"></span>--%>
										<ycommerce:testId code="header_LoggedUser">
										
										<c:choose>
												<c:when test="${currentLanguage.isocode eq 'en'}">
														<c:if test="${not empty user.firstName}"><div class="user-name user-firstname"><span class="icon ${iconuser} hidden-xs hidden-sm ">&nbsp;</span>${user.firstName}</div></c:if><span class="hidden-xs hidden-sm">&nbsp;</span>
														<c:if test="${not empty user.lastName}"><div class="user-name user-lastname">${user.lastName}</div></c:if>
												</c:when>
												<c:otherwise>
														<c:if test="${not empty user.lastName}"><div class="user-name user-lastname">${user.lastName}</div></c:if><span class="hidden-xs hidden-sm">&nbsp;</span>
														<c:if test="${not empty user.firstName}"><div class="user-name user-firstname"><span class="icon ${iconuser} hidden-xs hidden-sm ">&nbsp;</span>${user.firstName}</div></c:if>
												</c:otherwise>
										</c:choose>	
										</ycommerce:testId>
									</li>
<%-- 								</sec:authorize> --%>
								</c:when>
								<c:otherwise>
<%-- 								<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')" > --%>
										<ycommerce:testId code="header_Login_link">
											<li class="item item-login liOffcanvas">
												<a class="signIn" href="<c:url value='/login'/>">
													<div class="icon icon--user myAcctUserIcon"></div>
													<spring:theme code="navigation.link.signIn" />
												</a>
											</li>
											<li class="item item-register liOffcanvas">
												<a class="signUp" href="<c:url value='/login'/>">
													<div class="icon icon--user-new myAcctUserIcon"></div>
													<spring:theme code="navigation.link.register" />
												</a>
											</li>
										</ycommerce:testId>
									</li>
<%-- 								</sec:authorize> --%>
						</c:otherwise>
						</c:choose>
								<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
									<li class="item item--no-icon">
										<cms:pageSlot position="HeaderLinks" var="link">
											<cms:component component="${link}" element="div" class="" />
										</cms:pageSlot>
										<div class="navigation-account--nav js-secondaryNavAccount collapse" id="accNavComponentDesktopOne" >
											<ul class="nav__links"></ul>
										</div>
									</li>
								</sec:authorize>
						</ul>
					</div>
				</div>
				<div class="hidden-xs hidden-sm js-secondaryNavCompany collapse" id="accNavComponentDesktopTwo">
					<ul class="nav__links js-nav__links"></ul>
				</div>
				<div class="navigation--right-item nav__right navigation--right-item--margin">
					<div class="navigation-shopping">
						<ul class="navigation-shopping--item navigation-shopping--whishlist">
							<li class="item item-wishlist">
							<c:choose>
									<c:when test="${loggedIn}">
<%-- 								<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')"> --%>
									<a class="wishlist" href="<c:url value='/my-account/wishlist'/>">
										<div class="icon icon--list myAcctUserIcon"></div>
										<spring:theme code="header.my.shopping.list" />
									</a>
<%-- 								</sec:authorize> --%>
								</c:when>
								<c:otherwise>
<%-- 								<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')"> --%>
									<a class="signIn" href="<c:url value='/login'/>">
										<div class="icon icon--list myAcctUserIcon"></div>
										<spring:theme code="header.my.shopping.list" />
									</a>
<%-- 								</sec:authorize> --%>
							</c:otherwise>
						</c:choose>
							</li>
							<li class="item item--miniCart">
								<cms:pageSlot position="MiniCart" var="cart" element="div" class="componentContainer">
									<cms:component component="${cart}" element="div"/>
								</cms:pageSlot>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<a id="skiptonavigation"></a>
	</nav>
	<section id="cart" class="cart">
		<div id="cart--loader" class="cart--loader"></div>
		<div class="cart--content"></div>
	</section>
	<div class="hidden-md hidden-lg">
		<nav:topNavigation />
	</div>
</header>

