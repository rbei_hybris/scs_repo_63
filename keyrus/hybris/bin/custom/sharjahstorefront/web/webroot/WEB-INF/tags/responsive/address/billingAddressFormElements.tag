<%@ attribute name="regions" required="true" type="java.util.List"%>
<%@ attribute name="country" required="false" type="java.lang.String"%>
<%@ attribute name="tabindex" required="false" type="java.lang.Integer"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
 <spring:theme code="please.select" var="selectElt"/>
 
	<formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="billTo_firstName" inputCSS="text" mandatory="true"/>
	<formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="billTo_lastName" inputCSS="text" mandatory="true"/>
	
	<spring:theme code="address.country.default" var="defaultCountry"/>
	<formElement:formInputBox idKey="defaultCountry" labelKey="address.country" path="billTo_street2" inputCSS="form-control" mandatory="true" disabled="true" value="${defaultCountry}"/>
	
	<formElement:formSelectBox idKey="address.townCity" labelKey="address.townCity" path="billTo_city"  mandatory="true" 
		skipBlankMessageKey="${selectElt}" items="${citiesList}" selectedValue="${sharjahAddressForm.townCity}" selectCSSClass="form-control" />
		
	<formElement:formSelectBox idKey="address.state" labelKey="address.state" path="billTo_state"  mandatory="true" 
		skipBlankMessageKey="${selectElt}" items="${areasList}" selectedValue="${sharjahAddressForm.state}" selectCSSClass="form-control"  />
		
	<formElement:formInputBox idKey="address.phone" labelKey="address.phone" path="billTo_phoneNumber" inputCSS="form-control" mandatory="true"/>
	<formElement:formInputBox idKey="address.street" labelKey="address.street" path="billTo_street1" inputCSS="form-control" mandatory="true"/>
	<formElement:formInputBox idKey="address.building" labelKey="address.building" path="billTo_building" inputCSS="form-control" mandatory="true"/>
	<formElement:formInputBox idKey="address.apartment" labelKey="address.apartment" path="billTo_apartment" inputCSS="form-control" mandatory="true"/>
	<formElement:formInputBox idKey="address.landmark" labelKey="address.landmark" path="billTo_landmark" inputCSS="form-control" mandatory="false"/>
	