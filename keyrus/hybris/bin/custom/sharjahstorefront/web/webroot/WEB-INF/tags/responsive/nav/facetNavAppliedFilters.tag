<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="pageData" required="true" type="de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>


<c:if test="${not empty pageData.breadcrumbs}">
<div class="product-facet--container product-facet--applied js-facet">
	<div class="product-facet--name js-facet-name">
		<span class="facet__arrow">
			<span>
				<spring:theme code="search.nav.applied.facets"/>
			</span>
		</span>
	</div>
	<div class="product-facet--values js-facet-values">
		<ul class="product-facet--values-list js-facet-list">
			<c:forEach items="${pageData.breadcrumbs}" var="breadcrumb">
				<li>
					<c:url value="${breadcrumb.removeQuery.url}" var="removeQueryUrl"/>
					<a href="${removeQueryUrl}">
						<span class="icon icon--close-carnation">&nbsp;</span>&nbsp;
						<span>${breadcrumb.facetValueName}</span>
					</a>
				</li>
			</c:forEach>
		</ul>
	</div>
</div>
</c:if>
