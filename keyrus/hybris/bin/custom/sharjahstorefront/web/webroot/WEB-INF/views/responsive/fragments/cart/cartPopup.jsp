<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart"%>

<spring:theme code="text.addToCart" var="addToCartText"/>
<spring:theme code="text.popupCartTitle" var="popupCartTitleText"/>
<c:url value="/cart" var="cartUrl"/>
<c:url value="/cart" var="checkoutUrl"/>
<c:url value="/cart/updateMiniCart/SUBTOTAL" var="updateUrl"/>
<div class="cart--header">
	<div class="title">${popupCartTitleText}</div>
	<div class="flex flex--dir-row flex--space-between flex--align-center">
		<div class="nbitems js-number-items-popin"><span>${cartData.totalUnitCount}</span>&nbsp;<spring:theme code="basket.items"/></div>
		<div class="showall"><a href="${checkoutUrl}" class="js-minicart-showAll"><spring:theme code="review.show.all"/></a></div>
	</div>
</div>
<div class="cart--body">
	<c:choose>
		<c:when test="${numberShowing > 0 }">
				<div class="alert-message"></div>
				<c:if test="${not empty cartData.entries}">
					<div class="cart--item-list">
						<c:forEach items="${cartData.entries}" var="entry">
							<cart:miniCartItem entry="${entry}" updateUrl="${updateUrl}" />
						</c:forEach>
					</div>
				</c:if>
			</div>
			<div class="cart--footer">
				<cart:minicartTotals cartData="${cartData}" showTax="false"/>
					<c:set var="currencyLeftUntilTresholdReached" value="${threshold - (cartData.totalPrice.value)}" />
					<div class="cart--footer-bottom">
		             <c:choose>
		             	<c:when test="${currencyLeftUntilTresholdReached gt 0}">
					       <div id="cart--warning" class="cart--warning"><spring:theme code="mini.cart.page.threshold" arguments="${threshold} , AED, ${currencyLeftUntilTresholdReached}"/></div>
			            </c:when>
			         <c:otherwise>
			               <div id="cart--warning--hiden" class="cart--warning hidden" ><spring:theme code="mini.cart.page.threshold" arguments="${threshold} , AED, ${currencyLeftUntilTresholdReached}"/></div>
			         </c:otherwise>
		            </c:choose>
						 <c:choose>
		             	<c:when test="${currencyLeftUntilTresholdReached gt 0}">
								<div class="cart--submit">
									<a href="${checkoutUrl}" class="button button--full button--secondary mini-cart-checkout-button disabled ">
										<spring:theme code="checkout.checkout" />
									</a>									
								</div>
							</c:when>
							 <c:otherwise>
							 	<div class="cart--submit cart--submit--disable">
									<a href="${checkoutUrl}" class="button button--full button--secondary mini-cart-checkout-button">
										<spring:theme code="checkout.checkout" />
									</a>
								</div>
			         	</c:otherwise>
		            </c:choose>
					</div>
			
			</div>
		</c:when>

		<c:otherwise>
				<%-- ///////////// EMPTY CART ///////////// --%>
				<div class="alert-message"></div>
				<div class="cart--empty"><spring:theme code="popup.cart.empty" /></div>
			</div>
			<div class="cart--footer">
				<div class="cart--footer-bottom">
					<c:set var="currencyLeftUntilTresholdReached" value="${threshold - (cartData.totalPrice.value + cartData.totalDiscounts.value)}" />
					<div class="cart--warning"><spring:theme code="mini.cart.page.threshold" arguments="${threshold} , AED, ${currencyLeftUntilTresholdReached}"/></div>
					<div class="cart--submit cart--submit--disable">
						<a class="button button--full button--secondary mini-cart-checkout-button disabled" onclick="javascript:void();">
							<spring:theme code="checkout.checkout" />
						</a>
					</div>
				</div>
			</div>
		</c:otherwise>
	</c:choose>
	<input type="hidden" id="threshold" value="${threshold}" />
</div>
