<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="entry" required="true" type="de.hybris.platform.commercefacades.order.data.OrderEntryData" %>
<%@ attribute name="updateUrl" required="true" type="java.lang.String" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>


<li class="product mini-cart-service-item">

	<div class="media"></div>

	<div class="content">
		<div class="top">
			<%-- NAME --%>
			<c:set value="${entry.product.yearOfWarranty == 1 ? 'product.warranty.year' : 'product.warranty.years'}" var="yearsLabel" />
			<p class="name"> ${entry.product.name} - <spring:theme code="${yearsLabel}" arguments="${entry.product.yearOfWarranty}" /></p>

			<p><format:price priceData="${entry.basePrice.finalPrice}"/></p>

			<%-- UNIT PRICE AND QUANTITY --%>
			<div class="qty">
				${entry.quantity}
			</div>
		</div>

		<div class="bottom">
			<c:if test="${entry.updateable}">
				<button class="delete remove-item remove-mini-cart-entry-button" id="removeEntry_${entry.entryNumber}"
					data-mini-cart-url="${updateUrl}" data-productCode="${entry.product.code}" data-entryNumber="${entry.entryNumber}">
				</button>
			</c:if>
		</div>
	</div>

</li>
