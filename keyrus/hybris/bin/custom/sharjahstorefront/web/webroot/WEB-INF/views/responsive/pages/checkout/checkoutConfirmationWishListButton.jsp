<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<div class="row" style="margin-top: 10px;">
    <div class="pull-right col-xs-12 col-sm-6 col-md-5 col-lg-4">
        <div class="continue__shopping">
            <button class="button button--secondary button--orderconfirmation--global btn-block btn--continue-shopping js-wishlist-add-button">
                <spring:theme code="checkout.orderConfirmation.addToWishList" />
            </button>
        </div>
    </div>
</div>

<div id="idAddOrderToWishlist" style="display:none;">
	<div id="idResult"></div>
	<div class="wishListPopin">
		<div class="row">
			<spring:theme code="kacc.wishlist.add.wishlist.choice"/>
		</div>
		<div class="row">
			<select id="idSelectOption">
				<option value="newWishlist" selected="selected">
					<spring:theme code="kacc.wishlist.new.option.message" />
				</option>
				<c:forEach items="${wishlists}" var="wishlist" varStatus="status">
					<option class="classOption" value="${wishlist.name}" ${status.first ? 'selected="selected"' : ''}>
						${wishlist.name}
					</option>
				</c:forEach>
			</select>
			<input id="orderCode" name="orderCode" type="hidden" value="${orderCode}"/>
		</div>
		<div id="newWishlistSection" class="row">
			<label for="newWishlistName">
				<spring:theme code="kacc.wishlist.add.wishlist.message" />
			</label>
			<input id="newWishlistName" name="newWishlistName" type="text" class="required show"
					placeholder="<spring:theme code="kacc.wishlist.add.wishlist.name.placeholder" />" />
		</div>
		<div class="row">
			<div class="col-sm-5 col-lg-2">
				<button id="wishlistCancel" class="btn btn-primary btn-block">
					<spring:theme code="kacc.wishlist.add.wishlist.cancel" />
				</button>
			</div>
			<div class="col-sm-7 col-lg-10">
				<spring:theme code="kacc.wishlist.add.wishlist.add" var="wishlistAddButton" />
				<spring:theme code="kacc.wishlist.add.wishlist.create" var="wishlistCreateButton" />
				<button id="idSubmitNewWishlist" class="btn btn-primary btn-block"
						data-add-label="${wishlistAddButton}" data-create-label="${wishlistCreateButton}">
					${wishlistAddButton}
				</button>
			</div>
		</div>
		<p class="alert alert-danger errorValidation errorValidation" style="display:none;"><spring:theme code="kacc.wishlist.empty.wishlist.mandatory"/></p>
		<p class="alert alert-danger errorSize errorSize" style="display:none;"><spring:theme code="kacc.wishlist.empty.wishlist.sizeError"/></p>
	</div>
</div>
