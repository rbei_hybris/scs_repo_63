<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<c:url value="/cart/miniCart/${totalDisplay}" var="miniCartRefreshUrl"/>
<c:url value="/cart/rollover/${component.uid}" var="rolloverPopupUrl"/>
<c:url value="/cart" var="cartUrl"/>
<c:url value="/cart/checkout" var="checkoutUrl"/>
<div class="mini-cart">
	<a id="cart_list" href="${cartUrl}" class="mini-cart--link js-mini-cart-link"
		data-mini-cart-url="${rolloverPopupUrl}"
		data-mini-cart-refresh-url="${miniCartRefreshUrl}"
		data-mini-cart-name="<spring:theme code="text.cart"/>"
		data-mini-cart-empty-name="<spring:theme code="popup.cart.empty"/>"
		data-mini-cart-items-text="<spring:theme code="basket.items"/>"
		data-mini-cart-url-checkout="${cartUrl}"
		>
		<!-- <div class="mini-cart-icon">
			<span class="glyphicon glyphicon-shopping-cart "></span>
		</div> -->
		<ycommerce:testId code="miniCart_items_label">

			<div class="mini-cart--item mini-cart--price price js-mini-cart-price">
				<c:if test="${totalDisplay == 'TOTAL'}">
					<format:formatPriceDom priceData="${totalPrice}" />
				</c:if>

				<c:if test="${totalDisplay == 'SUBTOTAL'}">
					<format:formatPriceDom priceData="${subTotal}" />
				</c:if>

				<c:if test="${totalDisplay == 'TOTAL_WITHOUT_DELIVERY'}">
					<format:formatPriceDom priceData="${totalNoDelivery}" />
				</c:if>
			</div>
			<div class="mini-cart--item mini-cart--item-minicart js-mini-cart-count">
				<div class="icon icon--cart"></div>
				<span class="count js-mini-cart-qty">${totalItems gt 0 ? totalItems : "0"}</span>
			</div>
		</ycommerce:testId>
	</a>
</div>
