<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="addonProduct" tagdir="/WEB-INF/tags/addons/kaccwishlist/responsive/product" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"  %>

<c:set var="carouselClass" value="carousel" />

<c:set var="carouselMobileDots" value="true" />
<c:set var="carouselDesktopDots" value="true" />
<c:set var="carouselMobileArrows" value="false" />
<c:set var="carouselDesktopArrows" value="true" />
<c:set var="carouselMobileItems" value="2" />
<c:set var="carouselDesktopItems" value="6" />
<c:set var="carouselWidthSm" value="true" />
<c:set var="carouselDir" value="${currentLanguage.isocode == 'ar' ? 'rtl' : 'ltr'}" />

<c:choose>
	<c:when test="${not empty productData}">
		<c:url value="/cart/add" var="addToCartUrl"/>
		<div class="carousel ${carouselClass}--product--wrapper">
			<p class="${carouselClass}--headline">${title}</p>
			<div class="${carouselClass} js-carousel ${carouselClass}--product" data-carousel-mobile-items="${carouselMobileItems}" data-carousel-mobile-dots="${carouselMobileDots}" data-carousel-desktop-items="${carouselDesktopItems}" data-carousel-desktop-dots="${carouselDesktopDots}" data-carousel-desktop-arrows="${carouselDesktopArrows}" data-carousel-mobile-arrows="${carouselMobileArrows}" data-carousel-width-sm="${carouselWidthSm}" data-carousel-dir="${carouselDir}" >
				<c:choose>
					<c:when test="${component.popup}">
						<div class="${carouselClass}--carousel js-owl-carousel js-owl-lazy-reference js-owl-carousel-reference">
							<div id="quickViewTitle" class="quickView-header display-none">
								<div class="headline">
									<span class="headline-text"><spring:theme code="popup.quick.view.select"/></span>
								</div>
							</div>
							<c:forEach items="${productData}" var="product">
								<c:url value="${product.url}" var="productUrl"/>
								<c:url value="${product.url}/configuratorPage/${configuratorType}" var="configureProductUrl"/>
								<c:set var="isForceInStock" value="${product.stock.stockLevelStatus.code eq 'inStock' and empty product.stock.stockLevel}"/>
								<c:choose>
									<c:when test="${isForceInStock}">
										<c:set var="maxQty" value="FORCE_IN_STOCK"/>
									</c:when>
									<c:otherwise>
										<c:set var="maxQty" value="${product.stock.stockLevel}"/>
									</c:otherwise>
								</c:choose>
								<c:set var="qtyMinus" value="1" />
								<c:url value="${product.url}/quickView" var="productQuickViewUrl"/>
								<div class="${carouselClass}--item">
									<a href="${productQuickViewUrl}" class="js-reference-item">
										<div class="${carouselClass}--item--thumb">
											<product:productPrimaryReferenceImage product="${product}" format="product"/>
										</div>
										<div class="${carouselClass}--item--name">
											<ycommerce:testId code="product_productName">
												<c:choose>
													<c:when test="${fn:length(product.name) > 30}">
														<c:out value="${fn:substring(product.name, 0, 30)}..."/>
													</c:when>
													<c:otherwise>
														<c:out value="${product.name}" />
													</c:otherwise>
												</c:choose>
											</ycommerce:testId>
										</div>
										<div class="${carouselClass}--item--price"><format:fromPrice priceData="${product.price}"/></div>
									</a>
									<c:if test="${not product.multidimensional }">
										<form:form id="addToCartForm${product.code}" action="${addToCartUrl}" method="post" class="add_to_cart_form">
											<c:if test="${empty showAddToCart ? true : showAddToCart}">
												<div class="${carouselClass}--item--qty-selector">
													<%-- QTY Selector --%>
													<product:productQuantitySelector product="${product}" />
												</div>
											</c:if>
											<c:if test="${product.stock.stockLevel gt 0}">
												<c:set var="productStockLevel">${product.stock.stockLevel}&nbsp;
													<spring:theme code="product.variants.in.stock"/>
												</c:set>
											</c:if>
											<c:if test="${product.stock.stockLevelStatus.code eq 'lowStock'}">
												<c:set var="productStockLevel">
													<spring:theme code="product.variants.in.stock" />
												</c:set>
											</c:if>
											<c:if test="${isForceInStock}">
												<c:set var="productStockLevel">
													<spring:theme code="product.variants.available"/>
												</c:set>
											</c:if>
											<!-- <div class="stock-wrapper clearfix">
												${productStockLevel}
											</div> -->
											<c:if test="${multiDimensionalProduct}" >
												<c:url value="${product.url}/orderForm" var="productOrderFormUrl"/>
												<a href="${productOrderFormUrl}" class="btn btn-default btn-block btn-icon js-add-to-cart glyphicon-list-alt">
														<spring:theme code="order.form" />
												</a>
											</c:if>
											<ycommerce:testId code="addToCartButton">
												<input type="hidden" name="productCodePost" value="${product.code}"/>
												<input type="hidden" name="productNamePost" value="${fn:escapeXml(product.name)}"/>
												<input type="hidden" name="productPostPrice" value="${product.price.value}"/>
												<c:choose>
													<c:when test="${product.stock.stockLevelStatus.code eq 'outOfStock' }">
														<button type="submit" class="btn btn-primary btn-block glyphicon glyphicon-shopping-cart" aria-disabled="true" disabled="disabled">
														</button>
													</c:when>
													<c:otherwise>
														<button type="submit" class="btn btn-primary btn-block glyphicon glyphicon-shopping-cart js-enable-btn" disabled="disabled">
														</button>
													</c:otherwise>
												</c:choose>
											</ycommerce:testId>
										</form:form>
										<form:form id="configureForm${product.code}" action="${configureProductUrl}" method="get" class="configure_form">
											<c:if test="${product.configurable}">
												<c:choose>
													<c:when test="${product.stock.stockLevelStatus.code eq 'outOfStock' }">
														<button id="configureProduct" type="button" class="btn btn-primary btn-block" disabled="disabled">
																<spring:theme code="basket.configure.product"/>
														</button>
													</c:when>
													<c:otherwise>
														<button id="configureProduct" type="button" class="btn btn-primary btn-block js-enable-btn" disabled="disabled" onclick="location.href='${configureProductUrl}'">
																<spring:theme code="basket.configure.product"/>
														</button>
													</c:otherwise>
												</c:choose>
											</c:if>
										</form:form>
										<addonProduct:productAddToWishlistButton product="${product}" />
									</c:if>
								</div>
							</c:forEach>
						</div>
					</c:when>
					<c:otherwise>
						<!-- <div class="carousel__component--carousel"> -->
						<c:forEach items="${productData}" var="product">
							<c:url value="${product.url}" var="productUrl"/>
							<c:url value="${product.url}/configuratorPage/${configuratorType}" var="configureProductUrl"/>
							<c:set var="isForceInStock" value="${product.stock.stockLevelStatus.code eq 'inStock' and empty product.stock.stockLevel}"/>
							<c:choose>
								<c:when test="${isForceInStock}">
									<c:set var="maxQty" value="FORCE_IN_STOCK"/>
								</c:when>
								<c:otherwise>
									<c:set var="maxQty" value="${product.stock.stockLevel}"/>
								</c:otherwise>
							</c:choose>
							<c:set var="qtyMinus" value="1" />
							<c:url value="${product.url}/quickView" var="productQuickViewUrl"/>
							<div class="${carouselClass}--item product-tile">
								<div class="product-tile--wrapper">
									<a href="${productUrl}" class="product-tile--link">
										<div class="product-tile--infos">
											<div class="product-tile--thumb">
												<c:if test="${product.discountPrice ne null}">
													<div class="price--sticker">-${product.percentageDiscount}%</div>
												</c:if>
												<product:productPrimaryImage product="${product}" format="product"/>
											</div>
											<div class="product-tile--name">${fn:escapeXml(product.name)}</div>
										</div>
									</a>
									<div class="product-tile--command">
										<div class="product-tile--price price">
											<ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">
												<product:productPricePanel product="${product}" />
											</ycommerce:testId>
										</div>
										<c:if test="${not product.multidimensional }">
											<form:form id="addToCartForm${product.code}" action="${addToCartUrl}" method="post" class="add_to_cart_form">
												<c:if test="${empty showAddToCart ? true : showAddToCart}">
													<div class="product-tile--qty-selector">
														<%-- QTY Selector --%>
														<product:productQuantitySelector product="${product}" />
													</div>
												</c:if>
												<c:if test="${product.stock.stockLevel gt 0}">
													<c:set var="productStockLevel">${product.stock.stockLevel}&nbsp;
														<spring:theme code="product.variants.in.stock"/>
													</c:set>
												</c:if>
												<c:if test="${product.stock.stockLevelStatus.code eq 'lowStock'}">
													<c:set var="productStockLevel">
														<spring:theme code="product.variants.in.stock" />
													</c:set>
												</c:if>
												<c:if test="${isForceInStock}">
													<c:set var="productStockLevel">
														<spring:theme code="product.variants.available"/>
													</c:set>
												</c:if>
												<!-- 	<div class="stock-wrapper clearfix">
													${productStockLevel}
												</div> -->
												<c:if test="${multiDimensionalProduct}" >
													<c:url value="${product.url}/orderForm" var="productOrderFormUrl"/>
													<a href="${productOrderFormUrl}" class="button button--secondary js-add-to-cart glyphicon-list-alt">
														<div><div class="icon icon--cart-white">&nbsp;</div></div>
														<div class="button--label"><spring:theme code="order.form" /></div>
													</a>
												</c:if>
												<div class="product-tile--buttons add-button">
													<c:choose>
							    					<c:when test="${currentLanguage.isocode eq 'en'}">
															<div class="product-tile--buttons-item add-button-whishlist">
																<button href="javascript:void();" id="idFav" class="button button--tertiary addtolist addtolist-en add-button-whishlist-link js-add-product-to-wishlist"	data-add-product-to-wishlist-url="${pageContext.request.contextPath}/wishlist/addProductToWishlistPopup/${product.code}" data-title="${addProductToWishlistTitle}">
																	<div><div class="icon icon--add-list">&nbsp;</div></div>
																	<spring:theme code="text.whishlist.add" />
																</button>
															</div>
														</c:when>
														<c:otherwise>
															<div class="product-tile--buttons-item add-button-whishlist">
																<button href="javascript:void();" id="idFav" class="button button--tertiary addtolist addtolist-home-ar add-button-whishlist-link js-add-product-to-wishlist"	data-add-product-to-wishlist-url="${pageContext.request.contextPath}/wishlist/addProductToWishlistPopup/${product.code}" data-title="${addProductToWishlistTitle}">
																	<div><div class="icon icon--add-list">&nbsp;</div></div>
																	<spring:theme code="text.whishlist.add" />
																</button>
															</div>
														</c:otherwise>
						     						</c:choose>
													<ycommerce:testId code="addToCartButton">
														<input type="hidden" name="productCodePost" value="${product.code}"/>
														<input type="hidden" name="productNamePost" value="${fn:escapeXml(product.name)}"/>
														<input type="hidden" name="productPostPrice" value="${product.price.value}"/>
														<div class="product-tile--buttons-item add-button-cart">
														<c:choose>
							    							<c:when test="${currentLanguage.isocode eq 'en'}">
																<c:choose>
																	<c:when test="${product.stock.stockLevelStatus.code eq 'outOfStock' }">
																		<button class="button button--secondary add-button-toCart" aria-disabled="true" disabled="disabled">
																			<div><div class="icon icon--cart-white">&nbsp;</div></div>
																			<span><spring:theme code="text.addToCart" /></span>
																		</button>
																	</c:when>
																	<c:otherwise>
																		<button class="button button--secondary add-button-toCart js-enable-btn" disabled="disabled">
																			<div><div class="icon icon--cart-white">&nbsp;</div></div>
																			<div class="button--label"><spring:theme code="text.addToCart" /></div>
																		</button>
																	</c:otherwise>
																</c:choose>
															</c:when>
															<c:otherwise>
																<c:choose>
																	<c:when test="${product.stock.stockLevelStatus.code eq 'outOfStock' }">
																		<button class="button button--secondary add-button-toCart add-button-toCart-home-ar" aria-disabled="true" disabled="disabled">
																			<div><div class="icon icon--cart-white">&nbsp;</div></div>
																			<span><spring:theme code="text.addToCart" /></span>
																		</button>
																	</c:when>
																	<c:otherwise>
																		<button class="button button--secondary add-button-toCart add-button-toCart-home-ar js-enable-btn" disabled="disabled">
																			<div><div class="icon icon--cart-white">&nbsp;</div></div>
																			<div class="button--label"><spring:theme code="text.addToCart" /></div>
																		</button>
																	</c:otherwise>
																</c:choose>
															</c:otherwise>
														</c:choose>
														</div>
													</ycommerce:testId>
												</div>
											</form:form>
											<form:form id="configureForm${product.code}" action="${configureProductUrl}" method="get" class="configure_form">
												<c:if test="${product.configurable}">
													<c:choose>
														<c:when test="${product.stock.stockLevelStatus.code eq 'outOfStock' }">
															<button id="configureProduct" type="button" class="btn btn-primary btn-block" disabled="disabled">
																<spring:theme code="basket.configure.product"/>
															</button>
														</c:when>
														<c:otherwise>
															<button id="configureProduct" type="button" class="btn btn-primary btn-block js-enable-btn" disabled="disabled" onclick="location.href='${configureProductUrl}'">
																	<spring:theme code="basket.configure.product"/>
															</button>
														</c:otherwise>
													</c:choose>
												</c:if>
											</form:form>
										</c:if>
									</div>
								</div>
							</div>
						</c:forEach>
						<!-- 	</div> -->
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</c:when>
	<c:otherwise>
		<component:emptyComponent/>
	</c:otherwise>
</c:choose>
