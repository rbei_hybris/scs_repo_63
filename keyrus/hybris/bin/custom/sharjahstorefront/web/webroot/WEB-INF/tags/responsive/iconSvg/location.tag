<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%--
 SVG location icon
--%>
<?xml version="1.0" encoding="iso-8859-1"?>
<svg id="location" data-name="location" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48" width="45px" height="45px">
	<title>location</title>
	<path class="cls-1" d="M24,0A16,16,0,0,0,8,16c0,8.49,14.55,30.61,15.17,31.55a1,1,0,0,0,1.67,0C25.45,46.61,40,24.49,40,16A16,16,0,0,0,24,0Zm0,45.16C20.87,40.26,10,22.79,10,16a14,14,0,0,1,28,0C38,22.79,27.13,40.26,24,45.16Z" />
	<path class="cls-1" d="M24,9a7,7,0,1,0,7,7A7,7,0,0,0,24,9Zm0,12a5,5,0,1,1,5-5A5,5,0,0,1,24,21Z" />
</svg>
