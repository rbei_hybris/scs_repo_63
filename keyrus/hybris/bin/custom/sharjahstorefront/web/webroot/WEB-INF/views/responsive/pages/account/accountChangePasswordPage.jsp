<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>

<div class="account-section-header">
	<div class="row">
	<span class="glyphicon account-section-header-orderhistory ${currentLanguage.isocode == 'ar' ? 'glyphicon-chevron-right' : ' glyphicon-chevron-left'}"></span>
	<span><spring:theme code="text.account.profile.updatePasswordForm"/></span>
	</div>
</div>
<div class="row">
	<div class = "content-account">
		<div class="account-section-content">
			<div class="account-section-form">
				<form:form action="${action}" method="post" commandName="updatePasswordForm" class="update-password-form">

					<formElement:formPasswordBox idKey="currentPassword"
												 labelKey="profile.currentPassword" path="currentPassword" inputCSS="form-control"
												 mandatory="true" />
												 
				<spring:theme code="resetPwd.placeholder.pwd" var="resetPwdPlaceholder"/>
												 
					<formElement:formPasswordBox idKey="newPassword" placeholder="${resetPwdPlaceholder}"
												 labelKey="profile.newPassword" path="newPassword" inputCSS="form-control"
												 mandatory="true" />
					<formElement:formPasswordBox idKey="checkNewPassword"
												 labelKey="profile.checkNewPassword" path="checkNewPassword" inputCSS="form-control"
												 mandatory="true" />


					<div class="row updatePasswordForm-form--bottom-button-row">
						<div class="col-sm-6 col-sm-push-6">
							<div class="accountActions">
								<button type="submit" class="button button--full button--secondary update-password-form__update-button">
									<spring:theme code="updatePwd.submit" text="Update Password" />
								</button>
							</div>
						</div>
						<div class="col-sm-6 col-sm-pull-6">
							<div class="accountActions">
								<button type="button" class="button button--full button--secondary update-password-form__cancel-button">
									<spring:theme code="text.button.cancel" text="Cancel" />
								</button>
							</div>
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</div>
