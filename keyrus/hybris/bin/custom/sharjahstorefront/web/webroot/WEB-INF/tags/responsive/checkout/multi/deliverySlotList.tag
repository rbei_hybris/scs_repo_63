<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="slotRange" required="true" type="java.lang.Integer" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<select id="deliveryPerferedSlot" onchange="deliveryPerferedSlot()" class="form-control">
	<option selected="selected" class="deliverySlot">
		<spring:theme code='deliverySlots.link.select.click'/>
	</option>
	<c:forEach items="${deliverySlots}" var="deliverySlot">
		<c:set value="${deliverySlot.value}" var="slotDataList" />
		<fmt:formatDate pattern="yyyy-MM-dd" value="${deliverySlot.key}"
			var="slotDate" />
		<c:forEach var="i" begin="0" end="${maxDeliverySlots - 1}">
			<c:if test="${not empty slotDataList[i].dayOfWeek}" >
				<option id="deliverySlotSeleted" class="deliverySlot" 
					value="${slotDataList[i].pos},${slotDate},${slotDataList[i].deliverySlotCode},${slotDataList[i].dayOfWeek} ${slotDataList[i].beginTime}-${slotDataList[i].endTime}">
					${slotDataList[i].dayOfWeek}&nbsp; ${slotDate} -
					${slotDataList[i].beginTime}-${slotDataList[i].endTime}</option>
			</c:if>
		</c:forEach>
	</c:forEach>
</select>

