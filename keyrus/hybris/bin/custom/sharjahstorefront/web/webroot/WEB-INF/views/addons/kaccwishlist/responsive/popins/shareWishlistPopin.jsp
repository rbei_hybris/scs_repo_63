<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>

<h2><spring:theme code="kacc.wishlist.account.shareWishlist.title" arguments="${wishlistCode}"/></h2>
<form:form commandName="shareForm" method="post">

	   <div class="text">
	       <form:label path="receiverName"><spring:theme code="kacc.wishlist.account.shareWishlist.name"/></form:label>
	       <form:input path="receiverName" />
	   </div>
	   <div class="text">
	       <form:label path="receiverEmail"><spring:theme code="kacc.wishlist.account.shareWishlist.email"/></form:label>
	       <form:input path="receiverEmail" />
	       <input  value="${wishlistCode}" id="codewishlist" class="codewishlist" type="hidden"/>
	   </div>

	   <div class="actions">
	       
		<button type="button" class="js-wishlist-new-cancel button button--tertiary">
			<spring:theme code="kacc.wishlist.account.shareWishlist.cancel"/>  
		</button>
		
		<button type="button" class="js-wishlist-new-send button button--secondary">
			<spring:theme code="kacc.wishlist.account.shareWishlist.send"/>  
		</button>
		
	   </div>

</form:form>
