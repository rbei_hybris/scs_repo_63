<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData" %>
<%@ attribute name="orderEntry" required="true" type="de.hybris.platform.commercefacades.order.data.OrderEntryData" %>
<%@ attribute name="consignmentEntry" required="false"
              type="de.hybris.platform.commercefacades.order.data.ConsignmentEntryData" %>
<%@ attribute name="itemIndex" required="true" type="java.lang.Integer" %>
<%@ attribute name="targetUrl" required="false" type="java.lang.String" %>
<%@ attribute name="showStock" required="false" type="java.lang.Boolean" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="grid" tagdir="/WEB-INF/tags/responsive/grid" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>

<c:set var="varShowStock" value="${(empty showStock) ? true : showStock}" />

<c:url value="${orderEntry.product.url}" var="productUrl"/>
<c:set var="entryStock" value="${orderEntry.product.stock.stockLevelStatus.code}"/>

<div class="cartItem">
	<%-- IMAGE --%>
	<div class="cartItem--image">
		<a href="${productUrl}">
			<product:productPrimaryImage product="${orderEntry.product}" format="thumbnail"/>
		</a>
	</div>
	<div class="cartItem--infos">
		<div class="cartItem--infos-part-1">
			<div class="cartItem--subinfos">
				<%-- NAME --%>
				<div class="cartItem--name">
					<a href="${orderEntry.product.purchasable ? productUrl : ''}"><span class="item__name">${fn:escapeXml(orderEntry.product.name)}</span></a>
				</div>
				<%-- STOCK --%>
				<div class="cartItem--stock visible-md visible-lg">
					<c:if test="${varShowStock}">
						<c:set var="entryStock" value="${orderEntry.product.stock.stockLevelStatus.code}"/>
						<c:choose>
							<c:when test="${not empty entryStock and entryStock ne 'outOfStock' or orderEntry.product.multidimensional}">
								<div class="cartItem--stock-ok icon icon--check-circle-jungle-green">
									<spring:theme code="product.variants.in.stock"/>
								</div>
							</c:when>
							<c:otherwise>
								<div class="cartItem--stock-ko icon icon--close">
									<spring:theme code="product.variants.out.of.stock"/>
								</div>
							</c:otherwise>
						</c:choose>
					</c:if>
				</div>
			</div>
			<div class="item__configuration cartItem--config product-config visible-md visible-lg">
				<c:if test="${not empty orderEntry.classifProductCartData}">
					<c:forEach items="${orderEntry.classifProductCartData}" var="classification">
						<c:if test="${fn:toLowerCase(classification.name) eq 'configuration'}">
							<c:if test="${not empty classification.features}">
								<c:url value="/cart/updateConfig" var="cartUpdateConfigFormAction" />
								<form:form id="updateCartConfigForm${orderEntry.entryNumber}" action="${cartUpdateConfigFormAction}" method="post" class="update_cart_form product-config--form" modelAttribute="updateConfigForm">
									<p class="product-config--title"><spring:theme code="addToCart.text.configuration"/></p>
									<div class="product-config--select-wrapper">
										<c:forEach items="${classification.features}" var="feature">
											<form:select id="productConfOption" onchange="submit()" path="configuration" class="product-config--select js-activeMe">
												<c:forEach items="${feature.featureValues}" var="featureValue" varStatus="status">
													<option value="${fn:escapeXml(featureValue.value)}" ${featureValue.value == orderEntry.entryConf ? 'selected="selected"' : ''}>${fn:escapeXml(featureValue.value)}</option>
												</c:forEach>
											</form:select>
										</c:forEach>
									</div>
									<input type="hidden" name="entryNumber" value="${orderEntry.entryNumber}"/>
									<input type="hidden" name="productCode" value="${orderEntry.product.code}"/>
								</form:form>
							</c:if>
						</c:if>
					</c:forEach>
				</c:if>
			</div>
			<%-- UNIT PRICE AND QANTITY --%>
			<div class="cartItem--price price">
				<c:if test="${orderEntry.product.discountPrice ne null}">
					<div class="price--sticker">${orderEntry.product.percentageDiscount}<spring:theme code="product.percentage.off"/></div>
				</c:if>
				<div class="price--line">
					<ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">
						<product:productPricePanel product="${orderEntry.product}" noStrikeCurrency="true"/>
					</ycommerce:testId>
				</div>
			</div>
			<%-- STOCK --%>
			<div class="cartItem--stock visible-xs visible-sm">
				<div class="cartItem--stock-ok icon icon--check-circle-jungle-green">
					<spring:theme code="cart.product.stock.ok"/>
				</div>
			</div>
		</div>
		<div class="cartItem--infos-part-2">
			<%-- Configuration selector --%>
			<c:if test="${orderEntry.product.configurable}">
				<div class="cartItem--config">
					<c:url value="/cart/${orderEntry.entryNumber}/configuration/${orderEntry.configurationInfos[0].configuratorType}" var="entryConfigUrl"/>
					<div class="item__configurations">
						<c:forEach var="config" items="${orderEntry.configurationInfos}">
							<c:set var="style" value=""/>
							<c:if test="${config.status eq errorStatus}">
								<c:set var="style" value="color:red"/>
							</c:if>
							<div class="item__configuration--entry" style="${style}">
								<div class="row">
									<div class="item__configuration--name col-sm-4">
											${config.configurationLabel}
										<c:if test="${not empty config.configurationLabel}">:</c:if>
									</div>
									<div class="item__configuration--value col-sm-8">
											${config.configurationValue}
									</div>
								</div>
							</div>
						</c:forEach>
					</div>
					<c:if test="${not empty orderEntry.configurationInfos}">
						<div class="item__configurations--edit">
							<a class="btn" href="${entryConfigUrl}"><spring:theme code="basket.page.change.configuration"/></a>
						</div>
					</c:if>
				</div>
			</c:if>

			<div class="cartItem--qty-selection-row">
				<div class="cartItem--qty">
					<div class="cartItem--qty-selector">
						<c:forEach items="${orderEntry.product.baseOptions}" var="option">
							<c:if test="${not empty option.selected and option.selected.url eq orderEntry.product.url}">
								<c:forEach items="${option.selected.variantOptionQualifiers}" var="selectedOption">
									<div>
										<ycommerce:testId code="orderDetail_variantOption_label">
											<span>${fn:escapeXml(selectedOption.name)}:</span>
											<span>${fn:escapeXml(selectedOption.value)}</span>
										</ycommerce:testId>
									</div>
									<c:set var="entryStock" value="${option.selected.stock.stockLevelStatus.code}"/>
								</c:forEach>
							</c:if>
						</c:forEach>

						<ycommerce:testId code="orderDetails_productQuantity_label">
							<label class="visible-xs visible-sm cartItem--qty-label"><spring:theme code="text.account.order.qty"/>:
							<span class="qtyValue">
										${fn:escapeXml(orderEntry.quantity)}
							</span></label>
							<span class="visible-md visible-lg qtyValue">
										${fn:escapeXml(orderEntry.quantity)}
							</span>
						</ycommerce:testId>
					</div>
				</div>

				<div class="cartItem--total price--total">
					<format:priceTotalDom priceData="${orderEntry.totalPrice}"/>
				</div>
			</div>
		</div>
	</div>
</div>

<li>
	<c:if test="${empty targetUrl}">
		<spring:url value="/my-account/order/{/orderCode}/getReadOnlyProductVariantMatrix" var="targetUrl">
			<spring:param name="orderCode" value="${order.code}"/>
		</spring:url>
	</c:if>
	<grid:gridWrapper entry="${orderEntry}" index="${itemIndex}" styleClass="display-none add-to-cart-order-form-wrap" targetUrl="${targetUrl}"/>
</li>

