<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>

<%@ attribute name="languages" required="true"
	type="java.util.Collection"%>
<%@ attribute name="currentLanguage" required="true"
	type="de.hybris.platform.commercefacades.storesession.data.LanguageData"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="iconSvg" tagdir="/WEB-INF/tags/responsive/iconSvg" %>

<c:set var="langSize" value="${fn:length(languages)}"/>
<c:if test="${langSize > 1}">
	<c:url value="/_s/language" var="setLanguageActionUrl" />
	<%--<form:form id="lang-form" class="lang-style" action="${setLanguageActionUrl}" method="post">
		<select name="code" class="">
			<c:forEach items="${languages}" var="lang" varStatus="index">
				<c:choose>
					<c:when test="${lang.isocode == currentLanguage.isocode}">
						<c:if test="${lang.nativeName == 'English'}">
							<option class="flag-${lang.isocode} english switcher-off">
								&nbsp;
							</option>
						</c:if>
						<c:if test="${lang.nativeName != 'English'}">
							<option class="flag-${lang.isocode} arabic switcher-off">
								&nbsp;
							</option>
						</c:if>
					</c:when>
					<c:otherwise>
						<c:if test="${lang.nativeName == 'English'}">
							<option class="flag-${lang.isocode} english switcher-on js-lang-click" value="${lang.nativeName}">
								&nbsp;
							</option>
						</c:if>
						<c:if test="${lang.nativeName != 'English'}">
							<option class="flag-${lang.isocode} arabic switcher-on js-lang-click" value="${lang.nativeName}">
								&nbsp;
							</option>
						</c:if>
					<!-- 	<input type="hidden" name="code" value="${lang.isocode}"/> -->
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</select>
	</form:form>--%>

	<form:form id="lang-form" class="lang-style" action="${setLanguageActionUrl}" method="post">
	<div class="language-switcher">
		<div class="item active"><img src="${sharedResourcePath}/images/flags/flag_${currentLanguage.isocode}.jpg" alt="${lang.nativeName}" /></div>
		<div class="items-wrapper hide">
			<c:forEach items="${languages}" var="lang" varStatus="index">
				<c:if test="${lang.isocode != currentLanguage.isocode}">
					<div class="item js-lang-switch" data-lang-to-switch="${lang.isocode}"><img src="${sharedResourcePath}/images/flags/flag_${lang.isocode}.jpg" alt="${lang.nativeName}"/></div>
				</c:if>
			</c:forEach>
		</div>
		<input id="langcode" type="hidden" name="code" value="${lang.isocode}"/>
	</div>
	</form:form>
</c:if>
