<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ attribute name="pageTitle" required="false" rtexprvalue="true" %>
<%@ attribute name="pageCss" required="false" fragment="true" %>
<%@ attribute name="pageScripts" required="false" fragment="true" %>
<%@ attribute name="hideHeaderLinks" required="false" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="iconSvg" tagdir="/WEB-INF/tags/responsive/iconSvg" %>

<%@ taglib prefix="header" tagdir="/WEB-INF/tags/responsive/common/header" %>
<%@ taglib prefix="footer" tagdir="/WEB-INF/tags/responsive/common/footer" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>


<template:master pageTitle="${pageTitle}">

	<jsp:attribute name="pageCss">
		<jsp:invoke fragment="pageCss" />
	</jsp:attribute>

	<jsp:attribute name="pageScripts">
		<jsp:invoke fragment="pageScripts" />
	</jsp:attribute>

	<jsp:body>

<!-- 		<div class="branding-mobile hidden hidden-md hidden-lg">
			<div class="js-mobile-logo">
				<%--populated by JS acc.navigation--%>
			</div>
		</div> -->

		<main data-currency-iso-code="${currentCurrency.isocode}">
			<header:header hideHeaderLinks="${hideHeaderLinks}" />
			<div id="underHeader">
				<a id="skip-to-content"></a>

				<div id="content" class="content util--wrapper-max">
					<common:globalMessages />
					<cart:cartRestoration />
					<div id="wrapper-content" class="wrapper-content">
						<jsp:doBody />
					</div>
				</div>

	            <%-- 			<spring:theme code="text.skipToNavigation" var="skipToNavigation" />
				<a href="#skiptonavigation" class="skiptonavigation" data-role="none">${skipToNavigation}</a> --%>

				<footer:footer />
			</div>
		</main>

	</jsp:body>

</template:master>
