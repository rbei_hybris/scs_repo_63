var oDoc = document;

ACC.navigation = {
	_autoload: [
		'bindShowNavDesktop',
		'bindDrodownSubcat',
		'offcanvasNavigation',
		'myAccountNavigation',
		'orderToolsNavigation'
	],
	bindShowNavDesktop: function() {
		$(document).on('click', '.shopCategory', function(e) {
			$('#topNavigation').show();
			if ($('.js-secondaryNavAccount').hasClass('in')) {
				$('.js-myAccount-toggle').click();
			}
		});
		$('#topNavigation').on('mouseleave', function() {
			$(this).hide();
		});
	},
	bindDrodownSubcat: function() {
		console.log('ACC.navigation.js - ACC.navigation.bindDrodownSubcat() : executed');
		$('.js-dropdown-subcat').on('click', function(e) {
			e.preventDefault();
			console.log("ACC.navigation.js - ACC.navigation.bindDrodownSubcat() - Click on $('.js-dropdown-subcat')");
			var $this = $(this);
			if (($this).hasClass('open')) {
				$this.removeClass('open');
				$($this.data('target')).hide().removeClass('active');
			} else {
				//Remove open class on other element with same class
				$('.js-dropdown-subcat').removeClass('open');
				//Add open class on target
				$this.addClass('open');
				//Remove active class from all similar items
				$($this.data('target')).removeClass('active');
				//Add active class on the targeted item target
				$this.parent().find($this.data('target')).addClass('active');
				//Hide all other similar items opened
				$($this.data('target')).not('.active').each(function() {
					$(this).hide();
				}).promise().done(function() {
					//Then open the first targeted one
					$this.parent().find($this.data('target')).show();
				});
			}
		});
	},
	offcanvasNavigation: function() {
		console.log('ACC.navigation.js - ACC.navigation.offcanvasNavigation() : executed');
		enquire.register('screen and (max-width:' + screenSmMax + ')', {
			match: function() {
				$('body').on('click', '.js-enquire-offcanvas-navigation .js-enquire-has-sub .js_nav__link--drill__down', function(e) {
					console.log('ACC.navigation.js - ACC.navigation.offcanvasNavigation() - mobile mode - click on : .js-enquire-offcanvas-navigation .js-enquire-has-sub .js_nav__link--drill__down', $(this));
					e.preventDefault();
					$('.js-userAccount-Links').hide();
					$('.js-enquire-offcanvas-navigation ul.js-offcanvas-links').addClass('active');
					$('.js-enquire-offcanvas-navigation .js-enquire-has-sub').removeClass('active');
					$(this).parent('.js-enquire-has-sub').addClass('active');
					$(this).parent().find('.sm-back .sm-back--content').html($(this).parent().find('.nav__link a').html()); // Replace the back link word by the category name
				});
				$('body').on('click', '.js-enquire-offcanvas-navigation .js-enquire-sub-close', function(e) {
					console.log('ACC.navigation.js - ACC.navigation.offcanvasNavigation() - mobile mode - click on : .js-enquire-offcanvas-navigation .js-enquire-sub-close');
					e.preventDefault();
					$('.js-userAccount-Links').show();
					$('.js-enquire-offcanvas-navigation ul.js-offcanvas-links').removeClass('active');
					$('.js-enquire-offcanvas-navigation .js-enquire-has-sub').removeClass('active');
					$('.js-dropdown-subcat .active').removeClass('active').css('display', 'none');
				});
			},
			unmatch: function() {
				$('.js-userAccount-Links').show();
				$('.js-enquire-offcanvas-navigation ul.js-offcanvas-links').removeClass('active');
				$('.js-enquire-offcanvas-navigation .js-enquire-has-sub').removeClass('active');
				$('body').off('click', '.js-enquire-offcanvas-navigation .js-enquire-has-sub > a');
				$('body').off('click', '.js-enquire-offcanvas-navigation .js-enquire-sub-close');
			}
		});
	},
	mobileNavHeight: function() {
		console.log('ACC.navigation.js - ACC.navigation.mobileNavHeight() : executed');
		if (ACC.device.type === 'mobile') {
			var loginNav = $('.hidden-md.hidden-lg').find('.sticky-nav-top');
			var loginNavHeight = loginNav.height();
			var cookieBarHeight = $('#cookie-bar').height();
			var mobileNavHeight = $(window).height() - loginNavHeight - cookieBarHeight;
			$('.navigation__overflow').css({ height: mobileNavHeight + 'px' });
		}
	},
	myAccountNavigation: function() {
		//copy the site logo
		$('.js-mobile-logo').html($('.js-site-logo a').clone());
		//Add the order form img in the navigation
		$('.nav-form').html($('<span class="icon icon--list-alt"></span>'));
		var aAcctData = [];
		var sSignBtn = '';
		//my account items
		var oMyAccountData = $('.accNavComponent');
		//the my Account hook for the desktop
		var oMMainNavDesktop = $('.js-secondaryNavAccount > ul');
		//offcanvas menu for tablet/mobile
		var oMainNav = $('.navigation--bottom > ul.nav__links.nav__links--products');
		if (oMyAccountData) {
			var aLinks = oMyAccountData.find('a');
			for (var i = 0; i < aLinks.length; i++) {
				aAcctData.push({ link: aLinks[i].href, text: aLinks[i].title, className: aLinks[i].className });
				console.log('text : ', aLinks[i].href);
				console.log('link : ', aLinks[i].title);
				console.log('classList : ', aLinks[i].classList);
				console.log('--------------');
			}
		}
		var langSwitcher = $('.mobile-lang-switcher')[0].outerHTML;


		//create Welcome User + expand/collapse and close button
		//This is for mobile navigation. Adding html and classes.
		var oUserInfo = $('.nav__right ul li.logged_in');


		var navClose = '';
		navClose += '<div class="close-nav">';
		navClose += '<button type="button" class="js-toggle-sm-navigation btn"><span class="icon icon--close-white"></span></button>';
		navClose += '</div>';
		//create Sign In/Sign Out Button
		if ($('.liOffcanvas a') && $('.liOffcanvas a').length > 0) {
			sSignBtn += '<li class=\"auto liUserSign\" ><div class="userGroup myAcctUserIcon"><a class=\"userSign\" href=\"' + $('.liOffcanvas a')[0].href + '\"><span class=\"icon icon--user-white\"></span>' + $('.liOffcanvas a')[0].innerHTML + '</a></div>';
			//Check to see if user is not logged in
			if (oUserInfo.length === 0) {
				sSignBtn += langSwitcher;
			}
			sSignBtn += '</li>';
		}
		//Check to see if user is logged in
		if (oUserInfo && oUserInfo.length === 1) {
			// Action on mobile navigation when user is logged
			var sUserBtn = $('.navigation-menu--bottom .sticky-nav-top').html();
			sUserBtn += '<li class=\"auto \">';
			sUserBtn += '<div class=\"userGroup\">';
			sUserBtn += '<span class=\"icon icon--user-white myAcctUserIcon\"></span>';
			sUserBtn += '<div class=\"userName\">' + oUserInfo[0].innerHTML + '</div>';
			if (aAcctData.length > 0) {
				sUserBtn += '<a class=\"collapsed js-nav-collapse\" id="signedInUserOptionsToggle" data-toggle=\"collapse\"  data-target=\".offcanvasGroup1\">';
				sUserBtn += '<span class=\"icon icon--minus-white myAcctExp\"></span>';
				sUserBtn += '</a>';
			}
			sUserBtn += '</div>';
			sUserBtn += langSwitcher;
			sUserBtn += navClose;
			$('.js-sticky-user-group').html(sUserBtn);
			$('.js-userAccount-Links').append(sSignBtn);
			$('.js-userAccount-Links').append($('<li class="auto"><div class="myAccountLinksContainer js-myAccountLinksContainer"></div></li>'));
			//FOR DESKTOP
			var myAccountHook = $('<a class=\"myAccountLinksHeader collapsed js-myAccount-toggle\" data-toggle=\"collapse\" data-parent=".nav__right" href=\"#accNavComponentDesktopOne\">' + oMyAccountData.data("title") + '</a>');
			myAccountHook.insertBefore(oMyAccountData);
			//FOR MOBILE
			//create a My Account Top link for desktop - in case more components come then more parameters need to be passed from the backend
			myAccountHook = [];
			myAccountHook.push('<div class="sub-nav">');
			myAccountHook.push('<a id="signedInUserAccountToggle" class=\"myAccountLinksHeader collapsed js-myAccount-toggle\" data-toggle=\"collapse\" data-target=".offcanvasGroup2">');
			myAccountHook.push(oMyAccountData.data("title"));
			myAccountHook.push('<span class="icon icon--plus myAcctExp"></span>');
			myAccountHook.push('</a>');
			myAccountHook.push('</div>');
			$('.js-myAccountLinksContainer').append(myAccountHook.join(''));
			//add UL element for nested collapsing list
			$('.js-myAccountLinksContainer').append($('<ul data-trigger="#signedInUserAccountToggle" class="offcanvasGroup2 offcanvasNoBorder collapse js-nav-collapse-body subNavList js-myAccount-root sub-nav"></ul>'));
			//offcanvas items
			//TODO Follow up here to see the output of the account data in the offcanvas menu
			for (var j = aAcctData.length - 1; j >= 0; j--) {
				var oLink = oDoc.createElement("a");
				oLink.title = aAcctData[j].text;
				oLink.href = aAcctData[j].link;
				oLink.innerHTML = aAcctData[j].text;

				var oListItem = oDoc.createElement("li");
				oListItem.appendChild(oLink);
				oListItem = $(oListItem);
				oListItem.addClass("auto");
				$('.js-myAccount-root').append(oListItem);
			}
		} else {
			// Action on mobile navigation when user is not logged
			var navButtons = (sSignBtn.substring(0, sSignBtn.length - 5) + navClose) + '</li>';
			$('.js-sticky-user-group').html(navButtons);
		}
		//desktop
		for (var k = aAcctData.length - 1; k >= 0; k--) {
			var oLink2 = oDoc.createElement("a");
			oLink2.title = aAcctData[k].text;
			oLink2.href = aAcctData[k].link;
			oLink2.className = aAcctData[k].className;
			oLink2.innerHTML = aAcctData[k].text;

			var oListItem2 = oDoc.createElement("li");
			oListItem2.appendChild(oLink2);
			oListItem2 = $(oListItem2);
			oListItem2.addClass("auto");
			oMMainNavDesktop.get(0).appendChild(oListItem2.get(0));
		}
		//hide and show content areas for desktop
		$('.js-secondaryNavAccount').on('shown.bs.collapse', function() {
			if ($('.js-secondaryNavCompany').hasClass('in')) {
				$('.js-myCompany-toggle').click();
			}
		});
		$('.js-secondaryNavCompany').on('shown.bs.collapse', function() {
			if ($('.js-secondaryNavAccount').hasClass('in')) {
				$('.js-myAccount-toggle').click();
			}
		});
		//change icons for up and down
		$('.js-nav-collapse-body').on('hidden.bs.collapse', function(e) {
			var target = $(e.target);
			var targetSpan = target.attr('data-trigger') + ' > span';
			if (target.hasClass('in')) {
				$(targetSpan).removeClass('icon--plus').addClass('icon--minus');
			} else {
				$(targetSpan).removeClass('icon--minus').addClass('icon--plus');
			}
		});
		$('.js-nav-collapse-body').on('show.bs.collapse', function(e) {
			var target = $(e.target);
			var targetSpan = target.attr('data-trigger') + ' > span';
			if (target.hasClass('in')) {
				$(targetSpan).removeClass('icon--minus').addClass('icon--plus');

			} else {
				$(targetSpan).removeClass('icon--plus').addClass('icon--minus');
			}
		});
		//$('.offcanvasGroup1').collapse();
	},
	orderToolsNavigation: function() {
		$('.js-nav-order-tools').on('click', function(e) {
			$(this).toggleClass('js-nav-order-tools--active');
		});
	}
};
