<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ attribute name="paymentInfo" required="true" type="de.hybris.platform.commercefacades.order.data.CCPaymentInfoData" %>
<%@ attribute name="showPaymentInfo" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${not empty paymentInfo && showPaymentInfo}">
	<ul class="checkout-order-summary-list">
		<li class="checkout-order-summary-list-heading">
			<div class="title"><spring:theme code="checkout.multi.payment" text="Payment:"></spring:theme></div>
			<div class="address">
				<div class="">${fn:escapeXml(paymentInfo.accountHolderName)},&nbsp;${fn:escapeXml(paymentInfo.cardTypeData.name)}</div>
				<div class="">${fn:escapeXml(paymentInfo.cardNumber)},&nbsp;${paymentInfo.expiryMonth}/${paymentInfo.expiryYear}</div>
				<c:if test="${not empty paymentInfo.billingAddress}">
					<div class="">${fn:escapeXml(paymentInfo.billingAddress.line1)},&nbsp;${fn:escapeXml(paymentInfo.billingAddress.apartment)},&nbsp;${fn:escapeXml(paymentInfo.billingAddress.building)}</div>
					<c:if test="${not empty paymentInfo.billingAddress.town}">
						<div class="">
							<c:if test="${not empty paymentInfo.billingAddress.deliveryArea}">${fn:escapeXml(paymentInfo.billingAddress.deliveryArea.code)},&nbsp;</c:if>${fn:escapeXml(paymentInfo.billingAddress.town)},&nbsp;${fn:escapeXml(paymentInfo.billingAddress.country.name)}
						</div>
					</c:if>
				</c:if>
			</div>
		</li>
	</ul>
</c:if>

