<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="addonProduct" tagdir="/WEB-INF/tags/responsive/product" %>

<div class="row">
	<div class="col-md-12 col-lg-6">
		<addonProduct:productAddToWishlistButton product="${product}" />
	</div>
</div>
