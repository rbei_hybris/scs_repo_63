<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="actionNameKey" required="true"
	type="java.lang.String"%>
<%@ attribute name="action" required="true" type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<div class="register-section">
	<div class="register-section--title">
		<span class="icon icon--user-new">&nbsp;</span>
		<span class="title"><spring:theme code="register.new.customer" /></span>
	</div>
	
	<div class="register-section--description"><spring:theme code="register.description" /></div>
	<form:form method="post" commandName="sharjahRegisterForm" action="${action}" class="register-section--form form">
		<div class="register-section--form-group register-section--membershipnumber input-membershipnumber">
			<div class="input-membershipnumber--number">
				<div class="register-section--form-label form-label"><spring:theme code="register.membershipPrefixNumber" /></div>
				<div class="input-membershipnumber--number-wrapper">
				<c:choose>
					<c:when test="${currentLanguage.isocode eq 'en'}">
					<div class="input-membershipnumber--prefix">
						<formElement:formInputBox idKey="register.membershipPrefixNumber" labelKey="register.membershipPrefixNumber.prefix" path="membershipPrefixNumber" inputCSS="form-control" placeholder="X"/>
					</div>
					<div class="input-membershipnumber--suffix">
						<formElement:formInputBox idKey="register.membershipNumber" labelKey="register.membershipPrefixNumber" path="membershipNumber" inputCSS="form-control" placeholder="XXXXX"/>
					</div>
					</c:when>
				<c:otherwise>
					<div class="input-membershipnumber--suffix">
						<formElement:formInputBox idKey="register.membershipNumber" labelKey="register.membershipPrefixNumber" path="membershipNumber" inputCSS="form-control" placeholder="XXXXX"/>
					</div>
					<div class="input-membershipnumber--prefix">
						<formElement:formInputBox idKey="register.membershipPrefixNumber" labelKey="register.membershipPrefixNumber.prefix" path="membershipPrefixNumber" inputCSS="form-control" placeholder="X"/>
					</div>
				</c:otherwise>
			</c:choose>
				</div>
				<div class="input-membershipnumber--link">
					<a href="<c:url value="/becomeamember"/>"><spring:theme code="register.membershipPrefixNumber.link.label" /></a>
				</div>
			</div>
			<div class="input-membershipnumber--cards">
				<label class="input-membershipnumber--cards--description"><spring:theme code="register.cards.description" /></label>
				<div><img src="${themeResourcePath}/images/card-01.jpg"></div>
			</div>
		</div>
		
		<formElement:formInputBox idKey="register.firstName" labelKey="register.firstName" path="firstName" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="register.lastName" labelKey="register.lastName" path="lastName" inputCSS="form-control" mandatory="true" />
		<div class="double-inline-radio-boxes"> 
				<formElement:formRadioBox labelKey="profile.genderCode" items="${genderData}" idKey="profile.genderCode" path="genderCode"/>
		</div> 
		<c:choose>
			<c:when test="${hasErrors}">
				<formElement:formInputBox idKey="register.email" labelKey="register.email" path="email" inputCSS="form-control" mandatory="true" disabled="true"/>
			</c:when>
			<c:otherwise>
				<formElement:formInputBox idKey="register.email" labelKey="register.email" path="email" inputCSS="form-control" mandatory="true" />
			</c:otherwise>
		</c:choose>
		<formElement:formPasswordBox idKey="password" labelKey="register.pwd" path="pwd" inputCSS="form-control password-strength" mandatory="true" />
		<formElement:formPasswordBox idKey="register.checkPwd" labelKey="register.checkPwd" path="checkPwd" inputCSS="form-control" mandatory="true" />
		<div class="input-mobilephone">
			<div class="input-mobilephone--number">
				<div class="register-section--form-label form-label"><spring:theme code="register.mobilePrefixNumber" /></div>
			<c:choose>
				<c:when test="${currentLanguage.isocode eq 'en'}">
				<div class="input-membershipnumber--number-wrapper">
					<div class="input-mobilephone--prefix">
						<formElement:formInputBox idKey="register.mobileprefixNumber" labelKey="register.mobilePrefixNumber" path="mobilePrefixNumber" inputCSS="form-control" mandatory="true"  value="+971" disabled="true"/>
					</div>
					<div class="input-mobilephone--prefix0">
						<formElement:formInputBox idKey="register.mobileSecondPrefixNumber" labelKey="register.mobilePrefixNumber" path="mobileSecondPrefixNumber" inputCSS="form-control" mandatory="true"  value="(0)" disabled="true"/>
					</div>
					<div class="input-mobilephone--suffix">
						<formElement:formInputBox idKey="register.mobileNumber" labelKey="register.mobilePrefixNumber" path="mobileNumber" inputCSS="form-control" mandatory="true" placeholder="XXXXXXXXX"/>
					</div>
				</div>
				</c:when>
				<c:otherwise>
				<div class="input-membershipnumber--number-wrapper">
						<div class="input-mobilephone--suffix">
						<formElement:formInputBox idKey="register.mobileNumber" labelKey="register.mobilePrefixNumber" path="mobileNumber" inputCSS="form-control" mandatory="true" placeholder="XXXXXXXXX"/>
					</div>
					<div class="input-mobilephone--prefix0">
						<formElement:formInputBox idKey="register.mobileSecondPrefixNumber" labelKey="register.mobilePrefixNumber" path="mobileSecondPrefixNumber" inputCSS="form-control" mandatory="true"  value="(0)" disabled="true"/>
					</div>
					<div class="input-mobilephone--prefix">
						<formElement:formInputBox idKey="register.mobileprefixNumber" labelKey="register.mobilePrefixNumber" path="mobilePrefixNumber" inputCSS="form-control" mandatory="true"  value="971+" disabled="true"/>
					</div>
				</div>
				</c:otherwise>
		</c:choose>
			</div>
		</div>
		<div class="input-selectLang">
			<div class="register-section--form-label form-label"><spring:theme code="register.from.defaultLanguage.select" /></div>
			<div class="input-selectLang--option-wrapper">
				<div class="input-selectLang--option">
					<form:radiobutton id="defaultEnglishLanguage" name="defaultEnglishLanguage" path="defaultLanguage" value="en" />
					<label for="defaultEnglishLanguage"><spring:theme code="register.englishDefaultLanguage" /></label>
				</div>
				<div class="input-selectLang--option">
					<form:radiobutton id="defaultArabicLanguage" name="defaultArabicLanguage" path="defaultLanguage" value="ar" checked="checked" />
					<label for="defaultArabicLanguage"><spring:theme code="register.arabicDefaultLanguage" /></label>
				</div>
			</div>
		</div>
		<input type="hidden" id="recaptchaChallangeAnswered" value="${requestScope.recaptchaChallangeAnswered}" />
		<div class="form_field-elements control-group js-recaptcha-captchaaddon"></div>
		<div class="form-actions clearfix">
			<ycommerce:testId code="register_Register_button">
				<button type="submit" class="button button--secondary">
					<spring:theme code='register.submit' />
				</button>
			</ycommerce:testId>
		</div>
	</form:form>
</div>
