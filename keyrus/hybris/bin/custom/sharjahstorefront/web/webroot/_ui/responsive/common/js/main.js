function getShowMode(t) {
  return "All" === t ? "show=All" : "show=Page";
}

function updateQueryStringParameter(t, e) {
  return -1 !== t.indexOf("?q=") ? -1 !== t.indexOf("page=") ? (t = t.replace("page=", "")) + "&" + getShowMode(e) : "Page" === e && -1 !== t.indexOf("show=All") ? t.replace("show=All", "show=Page") : "All" === e && -1 !== t.indexOf("show=Page") ? t.replace("show=Page", "show=All") : t.endsWith("#") ? (t = t.substr(0, t.length - 1)) + "&" + getShowMode(e) + "#" : t + "?" + getShowMode(e) : "Page" === e && -1 !== t.indexOf("show=All") ? t.replace("show=All", "show=Page") : "All" === e && -1 !== t.indexOf("show=Page") ? t.replace("show=Page", "show=All") : -1 !== t.indexOf("?text") ? t + "&show=" + e : t + "?show=" + e;
}

function _autoload() {
  $.each(ACC, function(t, e) {
    $.isArray(e._autoload) && $.each(e._autoload, function(e, i) {
      $.isArray(i) ? i[1] ? ACC[t][i[0]]() : i[2] && ACC[t][i[2]]() : ACC[t][i]();
    });
  });
}

var screenXs = "480px", screenSm = "640px", screenMd = "1024px", screenLg = "1400px", screenXsMin = "480px", screenSmMin = "640px", screenMdMin = "1024px", screenLgMin = "1400px", screenXsMax = "639px", screenSmMax = "1023px", screenMdMax = "1399px", mediator = {
  trackers: {},
  publish: function(t) {
    if (!mediator.trackers[t]) return !1;
    for (var e = Array.prototype.slice.call(arguments, 1), i = 0, n = mediator.trackers[t].length; i < n; i++) {
      var o = mediator.trackers[t][i];
      o.callback.apply(o.context, e);
    }
    return this;
  },
  publishAll: function() {
    if (0 === Object.keys(mediator.trackers).length) return !1;
    for (var t in mediator.trackers) {
      var e = [ t ].concat(Array.prototype.slice.call(arguments));
      mediator.publish.apply(this, e);
    }
    return this;
  },
  subscribe: function(t, e) {
    return mediator.trackers[t] || (mediator.trackers[t] = []), mediator.trackers[t].push({
      context: this,
      callback: e
    }), this;
  }
};

if (function(t, e) {
  "object" == typeof module && "object" == typeof module.exports ? module.exports = t.document ? e(t, !0) : function(t) {
    if (!t.document) throw new Error("jQuery requires a window with a document");
    return e(t);
  } : e(t);
}("undefined" != typeof window ? window : this, function(t, e) {
  function i(t) {
    var e = t.length, i = J.type(t);
    return "function" !== i && !J.isWindow(t) && (!(1 !== t.nodeType || !e) || ("array" === i || 0 === e || "number" == typeof e && e > 0 && e - 1 in t));
  }
  function n(t, e, i) {
    if (J.isFunction(e)) return J.grep(t, function(t, n) {
      return !!e.call(t, n, t) !== i;
    });
    if (e.nodeType) return J.grep(t, function(t) {
      return t === e !== i;
    });
    if ("string" == typeof e) {
      if (at.test(e)) return J.filter(e, t, i);
      e = J.filter(e, t);
    }
    return J.grep(t, function(t) {
      return U.call(e, t) >= 0 !== i;
    });
  }
  function o(t, e) {
    for (;(t = t[e]) && 1 !== t.nodeType; ) ;
    return t;
  }
  function s(t) {
    var e = ht[t] = {};
    return J.each(t.match(ut) || [], function(t, i) {
      e[i] = !0;
    }), e;
  }
  function a() {
    G.removeEventListener("DOMContentLoaded", a, !1), t.removeEventListener("load", a, !1), 
    J.ready();
  }
  function r() {
    Object.defineProperty(this.cache = {}, 0, {
      get: function() {
        return {};
      }
    }), this.expando = J.expando + Math.random();
  }
  function l(t, e, i) {
    var n;
    if (void 0 === i && 1 === t.nodeType) if (n = "data-" + e.replace(bt, "-$1").toLowerCase(), 
    "string" == typeof (i = t.getAttribute(n))) {
      try {
        i = "true" === i || "false" !== i && ("null" === i ? null : +i + "" === i ? +i : vt.test(i) ? J.parseJSON(i) : i);
      } catch (t) {}
      gt.set(t, e, i);
    } else i = void 0;
    return i;
  }
  function c() {
    return !0;
  }
  function d() {
    return !1;
  }
  function u() {
    try {
      return G.activeElement;
    } catch (t) {}
  }
  function h(t, e) {
    return J.nodeName(t, "table") && J.nodeName(11 !== e.nodeType ? e : e.firstChild, "tr") ? t.getElementsByTagName("tbody")[0] || t.appendChild(t.ownerDocument.createElement("tbody")) : t;
  }
  function p(t) {
    return t.type = (null !== t.getAttribute("type")) + "/" + t.type, t;
  }
  function f(t) {
    var e = Nt.exec(t.type);
    return e ? t.type = e[1] : t.removeAttribute("type"), t;
  }
  function m(t, e) {
    for (var i = 0, n = t.length; n > i; i++) mt.set(t[i], "globalEval", !e || mt.get(e[i], "globalEval"));
  }
  function g(t, e) {
    var i, n, o, s, a, r, l, c;
    if (1 === e.nodeType) {
      if (mt.hasData(t) && (s = mt.access(t), a = mt.set(e, s), c = s.events)) {
        delete a.handle, a.events = {};
        for (o in c) for (i = 0, n = c[o].length; n > i; i++) J.event.add(e, o, c[o][i]);
      }
      gt.hasData(t) && (r = gt.access(t), l = J.extend({}, r), gt.set(e, l));
    }
  }
  function v(t, e) {
    var i = t.getElementsByTagName ? t.getElementsByTagName(e || "*") : t.querySelectorAll ? t.querySelectorAll(e || "*") : [];
    return void 0 === e || e && J.nodeName(t, e) ? J.merge([ t ], i) : i;
  }
  function b(t, e) {
    var i = e.nodeName.toLowerCase();
    "input" === i && xt.test(t.type) ? e.checked = t.checked : ("input" === i || "textarea" === i) && (e.defaultValue = t.defaultValue);
  }
  function C(e, i) {
    var n, o = J(i.createElement(e)).appendTo(i.body), s = t.getDefaultComputedStyle && (n = t.getDefaultComputedStyle(o[0])) ? n.display : J.css(o[0], "display");
    return o.detach(), s;
  }
  function y(t) {
    var e = G, i = qt[t];
    return i || ("none" !== (i = C(t, e)) && i || (Ft = (Ft || J("<iframe frameborder='0' width='0' height='0'/>")).appendTo(e.documentElement), 
    (e = Ft[0].contentDocument).write(), e.close(), i = C(t, e), Ft.detach()), qt[t] = i), 
    i;
  }
  function w(t, e, i) {
    var n, o, s, a, r = t.style;
    return (i = i || Wt(t)) && (a = i.getPropertyValue(e) || i[e]), i && ("" !== a || J.contains(t.ownerDocument, t) || (a = J.style(t, e)), 
    Lt.test(a) && Ht.test(e) && (n = r.width, o = r.minWidth, s = r.maxWidth, r.minWidth = r.maxWidth = r.width = a, 
    a = i.width, r.width = n, r.minWidth = o, r.maxWidth = s)), void 0 !== a ? a + "" : a;
  }
  function x(t, e) {
    return {
      get: function() {
        return t() ? void delete this.get : (this.get = e).apply(this, arguments);
      }
    };
  }
  function k(t, e) {
    if (e in t) return e;
    for (var i = e[0].toUpperCase() + e.slice(1), n = e, o = Qt.length; o--; ) if ((e = Qt[o] + i) in t) return e;
    return n;
  }
  function _(t, e, i) {
    var n = zt.exec(e);
    return n ? Math.max(0, n[1] - (i || 0)) + (n[2] || "px") : e;
  }
  function $(t, e, i, n, o) {
    for (var s = i === (n ? "border" : "content") ? 4 : "width" === e ? 1 : 0, a = 0; 4 > s; s += 2) "margin" === i && (a += J.css(t, i + yt[s], !0, o)), 
    n ? ("content" === i && (a -= J.css(t, "padding" + yt[s], !0, o)), "margin" !== i && (a -= J.css(t, "border" + yt[s] + "Width", !0, o))) : (a += J.css(t, "padding" + yt[s], !0, o), 
    "padding" !== i && (a += J.css(t, "border" + yt[s] + "Width", !0, o)));
    return a;
  }
  function A(t, e, i) {
    var n = !0, o = "width" === e ? t.offsetWidth : t.offsetHeight, s = Wt(t), a = "border-box" === J.css(t, "boxSizing", !1, s);
    if (0 >= o || null == o) {
      if ((0 > (o = w(t, e, s)) || null == o) && (o = t.style[e]), Lt.test(o)) return o;
      n = a && (Y.boxSizingReliable() || o === t.style[e]), o = parseFloat(o) || 0;
    }
    return o + $(t, e, i || (a ? "border" : "content"), n, s) + "px";
  }
  function S(t, e) {
    for (var i, n, o, s = [], a = 0, r = t.length; r > a; a++) (n = t[a]).style && (s[a] = mt.get(n, "olddisplay"), 
    i = n.style.display, e ? (s[a] || "none" !== i || (n.style.display = ""), "" === n.style.display && wt(n) && (s[a] = mt.access(n, "olddisplay", y(n.nodeName)))) : (o = wt(n), 
    "none" === i && o || mt.set(n, "olddisplay", o ? i : J.css(n, "display"))));
    for (a = 0; r > a; a++) (n = t[a]).style && (e && "none" !== n.style.display && "" !== n.style.display || (n.style.display = e ? s[a] || "" : "none"));
    return t;
  }
  function T(t, e, i, n, o) {
    return new T.prototype.init(t, e, i, n, o);
  }
  function D() {
    return setTimeout(function() {
      Xt = void 0;
    }), Xt = J.now();
  }
  function I(t, e) {
    var i, n = 0, o = {
      height: t
    };
    for (e = e ? 1 : 0; 4 > n; n += 2 - e) i = yt[n], o["margin" + i] = o["padding" + i] = t;
    return e && (o.opacity = o.width = t), o;
  }
  function j(t, e, i) {
    for (var n, o = (te[e] || []).concat(te["*"]), s = 0, a = o.length; a > s; s++) if (n = o[s].call(i, e, t)) return n;
  }
  function P(t, e) {
    var i, n, o, s, a;
    for (i in t) if (n = J.camelCase(i), o = e[n], s = t[i], J.isArray(s) && (o = s[1], 
    s = t[i] = s[0]), i !== n && (t[n] = s, delete t[i]), (a = J.cssHooks[n]) && "expand" in a) {
      s = a.expand(s), delete t[n];
      for (i in s) i in t || (t[i] = s[i], e[i] = o);
    } else e[n] = o;
  }
  function E(t, e, i) {
    var n, o, s = 0, a = Zt.length, r = J.Deferred().always(function() {
      delete l.elem;
    }), l = function() {
      if (o) return !1;
      for (var e = Xt || D(), i = Math.max(0, c.startTime + c.duration - e), n = 1 - (i / c.duration || 0), s = 0, a = c.tweens.length; a > s; s++) c.tweens[s].run(n);
      return r.notifyWith(t, [ c, n, i ]), 1 > n && a ? i : (r.resolveWith(t, [ c ]), 
      !1);
    }, c = r.promise({
      elem: t,
      props: J.extend({}, e),
      opts: J.extend(!0, {
        specialEasing: {}
      }, i),
      originalProperties: e,
      originalOptions: i,
      startTime: Xt || D(),
      duration: i.duration,
      tweens: [],
      createTween: function(e, i) {
        var n = J.Tween(t, c.opts, e, i, c.opts.specialEasing[e] || c.opts.easing);
        return c.tweens.push(n), n;
      },
      stop: function(e) {
        var i = 0, n = e ? c.tweens.length : 0;
        if (o) return this;
        for (o = !0; n > i; i++) c.tweens[i].run(1);
        return e ? r.resolveWith(t, [ c, e ]) : r.rejectWith(t, [ c, e ]), this;
      }
    }), d = c.props;
    for (P(d, c.opts.specialEasing); a > s; s++) if (n = Zt[s].call(c, t, d, c.opts)) return n;
    return J.map(d, j, c), J.isFunction(c.opts.start) && c.opts.start.call(t, c), J.fx.timer(J.extend(l, {
      elem: t,
      anim: c,
      queue: c.opts.queue
    })), c.progress(c.opts.progress).done(c.opts.done, c.opts.complete).fail(c.opts.fail).always(c.opts.always);
  }
  function N(t) {
    return function(e, i) {
      "string" != typeof e && (i = e, e = "*");
      var n, o = 0, s = e.toLowerCase().match(ut) || [];
      if (J.isFunction(i)) for (;n = s[o++]; ) "+" === n[0] ? (n = n.slice(1) || "*", 
      (t[n] = t[n] || []).unshift(i)) : (t[n] = t[n] || []).push(i);
    };
  }
  function O(t, e, i, n) {
    function o(r) {
      var l;
      return s[r] = !0, J.each(t[r] || [], function(t, r) {
        var c = r(e, i, n);
        return "string" != typeof c || a || s[c] ? a ? !(l = c) : void 0 : (e.dataTypes.unshift(c), 
        o(c), !1);
      }), l;
    }
    var s = {}, a = t === be;
    return o(e.dataTypes[0]) || !s["*"] && o("*");
  }
  function M(t, e) {
    var i, n, o = J.ajaxSettings.flatOptions || {};
    for (i in e) void 0 !== e[i] && ((o[i] ? t : n || (n = {}))[i] = e[i]);
    return n && J.extend(!0, t, n), t;
  }
  function F(t, e, i) {
    for (var n, o, s, a, r = t.contents, l = t.dataTypes; "*" === l[0]; ) l.shift(), 
    void 0 === n && (n = t.mimeType || e.getResponseHeader("Content-Type"));
    if (n) for (o in r) if (r[o] && r[o].test(n)) {
      l.unshift(o);
      break;
    }
    if (l[0] in i) s = l[0]; else {
      for (o in i) {
        if (!l[0] || t.converters[o + " " + l[0]]) {
          s = o;
          break;
        }
        a || (a = o);
      }
      s = s || a;
    }
    return s ? (s !== l[0] && l.unshift(s), i[s]) : void 0;
  }
  function q(t, e, i, n) {
    var o, s, a, r, l, c = {}, d = t.dataTypes.slice();
    if (d[1]) for (a in t.converters) c[a.toLowerCase()] = t.converters[a];
    for (s = d.shift(); s; ) if (t.responseFields[s] && (i[t.responseFields[s]] = e), 
    !l && n && t.dataFilter && (e = t.dataFilter(e, t.dataType)), l = s, s = d.shift()) if ("*" === s) s = l; else if ("*" !== l && l !== s) {
      if (!(a = c[l + " " + s] || c["* " + s])) for (o in c) if ((r = o.split(" "))[1] === s && (a = c[l + " " + r[0]] || c["* " + r[0]])) {
        !0 === a ? a = c[o] : !0 !== c[o] && (s = r[0], d.unshift(r[1]));
        break;
      }
      if (!0 !== a) if (a && t.throws) e = a(e); else try {
        e = a(e);
      } catch (t) {
        return {
          state: "parsererror",
          error: a ? t : "No conversion from " + l + " to " + s
        };
      }
    }
    return {
      state: "success",
      data: e
    };
  }
  function H(t, e, i, n) {
    var o;
    if (J.isArray(e)) J.each(e, function(e, o) {
      i || we.test(t) ? n(t, o) : H(t + "[" + ("object" == typeof o ? e : "") + "]", o, i, n);
    }); else if (i || "object" !== J.type(e)) n(t, e); else for (o in e) H(t + "[" + o + "]", e[o], i, n);
  }
  function L(t) {
    return J.isWindow(t) ? t : 9 === t.nodeType && t.defaultView;
  }
  var W = [], R = W.slice, z = W.concat, B = W.push, U = W.indexOf, V = {}, Q = V.toString, X = V.hasOwnProperty, Y = {}, G = t.document, K = "2.1.1", J = function(t, e) {
    return new J.fn.init(t, e);
  }, Z = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, tt = /^-ms-/, et = /-([\da-z])/gi, it = function(t, e) {
    return e.toUpperCase();
  };
  J.fn = J.prototype = {
    jquery: K,
    constructor: J,
    selector: "",
    length: 0,
    toArray: function() {
      return R.call(this);
    },
    get: function(t) {
      return null != t ? 0 > t ? this[t + this.length] : this[t] : R.call(this);
    },
    pushStack: function(t) {
      var e = J.merge(this.constructor(), t);
      return e.prevObject = this, e.context = this.context, e;
    },
    each: function(t, e) {
      return J.each(this, t, e);
    },
    map: function(t) {
      return this.pushStack(J.map(this, function(e, i) {
        return t.call(e, i, e);
      }));
    },
    slice: function() {
      return this.pushStack(R.apply(this, arguments));
    },
    first: function() {
      return this.eq(0);
    },
    last: function() {
      return this.eq(-1);
    },
    eq: function(t) {
      var e = this.length, i = +t + (0 > t ? e : 0);
      return this.pushStack(i >= 0 && e > i ? [ this[i] ] : []);
    },
    end: function() {
      return this.prevObject || this.constructor(null);
    },
    push: B,
    sort: W.sort,
    splice: W.splice
  }, J.extend = J.fn.extend = function() {
    var t, e, i, n, o, s, a = arguments[0] || {}, r = 1, l = arguments.length, c = !1;
    for ("boolean" == typeof a && (c = a, a = arguments[r] || {}, r++), "object" == typeof a || J.isFunction(a) || (a = {}), 
    r === l && (a = this, r--); l > r; r++) if (null != (t = arguments[r])) for (e in t) i = a[e], 
    n = t[e], a !== n && (c && n && (J.isPlainObject(n) || (o = J.isArray(n))) ? (o ? (o = !1, 
    s = i && J.isArray(i) ? i : []) : s = i && J.isPlainObject(i) ? i : {}, a[e] = J.extend(c, s, n)) : void 0 !== n && (a[e] = n));
    return a;
  }, J.extend({
    expando: "jQuery" + (K + Math.random()).replace(/\D/g, ""),
    isReady: !0,
    error: function(t) {
      throw new Error(t);
    },
    noop: function() {},
    isFunction: function(t) {
      return "function" === J.type(t);
    },
    isArray: Array.isArray,
    isWindow: function(t) {
      return null != t && t === t.window;
    },
    isNumeric: function(t) {
      return !J.isArray(t) && t - parseFloat(t) >= 0;
    },
    isPlainObject: function(t) {
      return "object" === J.type(t) && !t.nodeType && !J.isWindow(t) && !(t.constructor && !X.call(t.constructor.prototype, "isPrototypeOf"));
    },
    isEmptyObject: function(t) {
      var e;
      for (e in t) return !1;
      return !0;
    },
    type: function(t) {
      return null == t ? t + "" : "object" == typeof t || "function" == typeof t ? V[Q.call(t)] || "object" : typeof t;
    },
    globalEval: function(t) {
      var e, i = eval;
      (t = J.trim(t)) && (1 === t.indexOf("use strict") ? (e = G.createElement("script"), 
      e.text = t, G.head.appendChild(e).parentNode.removeChild(e)) : i(t));
    },
    camelCase: function(t) {
      return t.replace(tt, "ms-").replace(et, it);
    },
    nodeName: function(t, e) {
      return t.nodeName && t.nodeName.toLowerCase() === e.toLowerCase();
    },
    each: function(t, e, n) {
      var o = 0, s = t.length, a = i(t);
      if (n) {
        if (a) for (;s > o && !1 !== e.apply(t[o], n); o++) ; else for (o in t) if (!1 === e.apply(t[o], n)) break;
      } else if (a) for (;s > o && !1 !== e.call(t[o], o, t[o]); o++) ; else for (o in t) if (!1 === e.call(t[o], o, t[o])) break;
      return t;
    },
    trim: function(t) {
      return null == t ? "" : (t + "").replace(Z, "");
    },
    makeArray: function(t, e) {
      var n = e || [];
      return null != t && (i(Object(t)) ? J.merge(n, "string" == typeof t ? [ t ] : t) : B.call(n, t)), 
      n;
    },
    inArray: function(t, e, i) {
      return null == e ? -1 : U.call(e, t, i);
    },
    merge: function(t, e) {
      for (var i = +e.length, n = 0, o = t.length; i > n; n++) t[o++] = e[n];
      return t.length = o, t;
    },
    grep: function(t, e, i) {
      for (var n = [], o = 0, s = t.length, a = !i; s > o; o++) !e(t[o], o) !== a && n.push(t[o]);
      return n;
    },
    map: function(t, e, n) {
      var o, s = 0, a = t.length, r = [];
      if (i(t)) for (;a > s; s++) null != (o = e(t[s], s, n)) && r.push(o); else for (s in t) null != (o = e(t[s], s, n)) && r.push(o);
      return z.apply([], r);
    },
    guid: 1,
    proxy: function(t, e) {
      var i, n, o;
      return "string" == typeof e && (i = t[e], e = t, t = i), J.isFunction(t) ? (n = R.call(arguments, 2), 
      o = function() {
        return t.apply(e || this, n.concat(R.call(arguments)));
      }, o.guid = t.guid = t.guid || J.guid++, o) : void 0;
    },
    now: Date.now,
    support: Y
  }), J.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(t, e) {
    V["[object " + e + "]"] = e.toLowerCase();
  });
  var nt = function(t) {
    function e(t, e, i, n) {
      var o, s, a, r, c, u, h, p, f, m;
      if ((e ? e.ownerDocument || e : q) !== I && D(e), e = e || I, i = i || [], !t || "string" != typeof t) return i;
      if (1 !== (r = e.nodeType) && 9 !== r) return [];
      if (P && !n) {
        if (o = gt.exec(t)) if (a = o[1]) {
          if (9 === r) {
            if (!(s = e.getElementById(a)) || !s.parentNode) return i;
            if (s.id === a) return i.push(s), i;
          } else if (e.ownerDocument && (s = e.ownerDocument.getElementById(a)) && M(e, s) && s.id === a) return i.push(s), 
          i;
        } else {
          if (o[2]) return K.apply(i, e.getElementsByTagName(t)), i;
          if ((a = o[3]) && C.getElementsByClassName && e.getElementsByClassName) return K.apply(i, e.getElementsByClassName(a)), 
          i;
        }
        if (C.qsa && (!E || !E.test(t))) {
          if (p = h = F, f = e, m = 9 === r && t, 1 === r && "object" !== e.nodeName.toLowerCase()) {
            for (u = k(t), (h = e.getAttribute("id")) ? p = h.replace(bt, "\\$&") : e.setAttribute("id", p), 
            p = "[id='" + p + "'] ", c = u.length; c--; ) u[c] = p + d(u[c]);
            f = vt.test(t) && l(e.parentNode) || e, m = u.join(",");
          }
          if (m) try {
            return K.apply(i, f.querySelectorAll(m)), i;
          } catch (t) {} finally {
            h || e.removeAttribute("id");
          }
        }
      }
      return $(t.replace(at, "$1"), e, i, n);
    }
    function i() {
      function t(i, n) {
        return e.push(i + " ") > y.cacheLength && delete t[e.shift()], t[i + " "] = n;
      }
      var e = [];
      return t;
    }
    function n(t) {
      return t[F] = !0, t;
    }
    function o(t) {
      var e = I.createElement("div");
      try {
        return !!t(e);
      } catch (t) {
        return !1;
      } finally {
        e.parentNode && e.parentNode.removeChild(e), e = null;
      }
    }
    function s(t, e) {
      for (var i = t.split("|"), n = t.length; n--; ) y.attrHandle[i[n]] = e;
    }
    function a(t, e) {
      var i = e && t, n = i && 1 === t.nodeType && 1 === e.nodeType && (~e.sourceIndex || V) - (~t.sourceIndex || V);
      if (n) return n;
      if (i) for (;i = i.nextSibling; ) if (i === e) return -1;
      return t ? 1 : -1;
    }
    function r(t) {
      return n(function(e) {
        return e = +e, n(function(i, n) {
          for (var o, s = t([], i.length, e), a = s.length; a--; ) i[o = s[a]] && (i[o] = !(n[o] = i[o]));
        });
      });
    }
    function l(t) {
      return t && typeof t.getElementsByTagName !== U && t;
    }
    function c() {}
    function d(t) {
      for (var e = 0, i = t.length, n = ""; i > e; e++) n += t[e].value;
      return n;
    }
    function u(t, e, i) {
      var n = e.dir, o = i && "parentNode" === n, s = L++;
      return e.first ? function(e, i, s) {
        for (;e = e[n]; ) if (1 === e.nodeType || o) return t(e, i, s);
      } : function(e, i, a) {
        var r, l, c = [ H, s ];
        if (a) {
          for (;e = e[n]; ) if ((1 === e.nodeType || o) && t(e, i, a)) return !0;
        } else for (;e = e[n]; ) if (1 === e.nodeType || o) {
          if (l = e[F] || (e[F] = {}), (r = l[n]) && r[0] === H && r[1] === s) return c[2] = r[2];
          if (l[n] = c, c[2] = t(e, i, a)) return !0;
        }
      };
    }
    function h(t) {
      return t.length > 1 ? function(e, i, n) {
        for (var o = t.length; o--; ) if (!t[o](e, i, n)) return !1;
        return !0;
      } : t[0];
    }
    function p(t, i, n) {
      for (var o = 0, s = i.length; s > o; o++) e(t, i[o], n);
      return n;
    }
    function f(t, e, i, n, o) {
      for (var s, a = [], r = 0, l = t.length, c = null != e; l > r; r++) (s = t[r]) && (!i || i(s, n, o)) && (a.push(s), 
      c && e.push(r));
      return a;
    }
    function m(t, e, i, o, s, a) {
      return o && !o[F] && (o = m(o)), s && !s[F] && (s = m(s, a)), n(function(n, a, r, l) {
        var c, d, u, h = [], m = [], g = a.length, v = n || p(e || "*", r.nodeType ? [ r ] : r, []), b = !t || !n && e ? v : f(v, h, t, r, l), C = i ? s || (n ? t : g || o) ? [] : a : b;
        if (i && i(b, C, r, l), o) for (c = f(C, m), o(c, [], r, l), d = c.length; d--; ) (u = c[d]) && (C[m[d]] = !(b[m[d]] = u));
        if (n) {
          if (s || t) {
            if (s) {
              for (c = [], d = C.length; d--; ) (u = C[d]) && c.push(b[d] = u);
              s(null, C = [], c, l);
            }
            for (d = C.length; d--; ) (u = C[d]) && (c = s ? Z.call(n, u) : h[d]) > -1 && (n[c] = !(a[c] = u));
          }
        } else C = f(C === a ? C.splice(g, C.length) : C), s ? s(null, a, C, l) : K.apply(a, C);
      });
    }
    function g(t) {
      for (var e, i, n, o = t.length, s = y.relative[t[0].type], a = s || y.relative[" "], r = s ? 1 : 0, l = u(function(t) {
        return t === e;
      }, a, !0), c = u(function(t) {
        return Z.call(e, t) > -1;
      }, a, !0), p = [ function(t, i, n) {
        return !s && (n || i !== A) || ((e = i).nodeType ? l(t, i, n) : c(t, i, n));
      } ]; o > r; r++) if (i = y.relative[t[r].type]) p = [ u(h(p), i) ]; else {
        if ((i = y.filter[t[r].type].apply(null, t[r].matches))[F]) {
          for (n = ++r; o > n && !y.relative[t[n].type]; n++) ;
          return m(r > 1 && h(p), r > 1 && d(t.slice(0, r - 1).concat({
            value: " " === t[r - 2].type ? "*" : ""
          })).replace(at, "$1"), i, n > r && g(t.slice(r, n)), o > n && g(t = t.slice(n)), o > n && d(t));
        }
        p.push(i);
      }
      return h(p);
    }
    function v(t, i) {
      var o = i.length > 0, s = t.length > 0, a = function(n, a, r, l, c) {
        var d, u, h, p = 0, m = "0", g = n && [], v = [], b = A, C = n || s && y.find.TAG("*", c), w = H += null == b ? 1 : Math.random() || .1, x = C.length;
        for (c && (A = a !== I && a); m !== x && null != (d = C[m]); m++) {
          if (s && d) {
            for (u = 0; h = t[u++]; ) if (h(d, a, r)) {
              l.push(d);
              break;
            }
            c && (H = w);
          }
          o && ((d = !h && d) && p--, n && g.push(d));
        }
        if (p += m, o && m !== p) {
          for (u = 0; h = i[u++]; ) h(g, v, a, r);
          if (n) {
            if (p > 0) for (;m--; ) g[m] || v[m] || (v[m] = Y.call(l));
            v = f(v);
          }
          K.apply(l, v), c && !n && v.length > 0 && p + i.length > 1 && e.uniqueSort(l);
        }
        return c && (H = w, A = b), g;
      };
      return o ? n(a) : a;
    }
    var b, C, y, w, x, k, _, $, A, S, T, D, I, j, P, E, N, O, M, F = "sizzle" + -new Date(), q = t.document, H = 0, L = 0, W = i(), R = i(), z = i(), B = function(t, e) {
      return t === e && (T = !0), 0;
    }, U = "undefined", V = 1 << 31, Q = {}.hasOwnProperty, X = [], Y = X.pop, G = X.push, K = X.push, J = X.slice, Z = X.indexOf || function(t) {
      for (var e = 0, i = this.length; i > e; e++) if (this[e] === t) return e;
      return -1;
    }, tt = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped", et = "[\\x20\\t\\r\\n\\f]", it = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+", nt = it.replace("w", "w#"), ot = "\\[" + et + "*(" + it + ")(?:" + et + "*([*^$|!~]?=)" + et + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + nt + "))|)" + et + "*\\]", st = ":(" + it + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + ot + ")*)|.*)\\)|)", at = new RegExp("^" + et + "+|((?:^|[^\\\\])(?:\\\\.)*)" + et + "+$", "g"), rt = new RegExp("^" + et + "*," + et + "*"), lt = new RegExp("^" + et + "*([>+~]|" + et + ")" + et + "*"), ct = new RegExp("=" + et + "*([^\\]'\"]*?)" + et + "*\\]", "g"), dt = new RegExp(st), ut = new RegExp("^" + nt + "$"), ht = {
      ID: new RegExp("^#(" + it + ")"),
      CLASS: new RegExp("^\\.(" + it + ")"),
      TAG: new RegExp("^(" + it.replace("w", "w*") + ")"),
      ATTR: new RegExp("^" + ot),
      PSEUDO: new RegExp("^" + st),
      CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + et + "*(even|odd|(([+-]|)(\\d*)n|)" + et + "*(?:([+-]|)" + et + "*(\\d+)|))" + et + "*\\)|)", "i"),
      bool: new RegExp("^(?:" + tt + ")$", "i"),
      needsContext: new RegExp("^" + et + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + et + "*((?:-\\d)?\\d*)" + et + "*\\)|)(?=[^-]|$)", "i")
    }, pt = /^(?:input|select|textarea|button)$/i, ft = /^h\d$/i, mt = /^[^{]+\{\s*\[native \w/, gt = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/, vt = /[+~]/, bt = /'|\\/g, Ct = new RegExp("\\\\([\\da-f]{1,6}" + et + "?|(" + et + ")|.)", "ig"), yt = function(t, e, i) {
      var n = "0x" + e - 65536;
      return n !== n || i ? e : 0 > n ? String.fromCharCode(n + 65536) : String.fromCharCode(n >> 10 | 55296, 1023 & n | 56320);
    };
    try {
      K.apply(X = J.call(q.childNodes), q.childNodes), X[q.childNodes.length].nodeType;
    } catch (t) {
      K = {
        apply: X.length ? function(t, e) {
          G.apply(t, J.call(e));
        } : function(t, e) {
          for (var i = t.length, n = 0; t[i++] = e[n++]; ) ;
          t.length = i - 1;
        }
      };
    }
    C = e.support = {}, x = e.isXML = function(t) {
      var e = t && (t.ownerDocument || t).documentElement;
      return !!e && "HTML" !== e.nodeName;
    }, D = e.setDocument = function(t) {
      var e, i = t ? t.ownerDocument || t : q, n = i.defaultView;
      return i !== I && 9 === i.nodeType && i.documentElement ? (I = i, j = i.documentElement, 
      P = !x(i), n && n !== n.top && (n.addEventListener ? n.addEventListener("unload", function() {
        D();
      }, !1) : n.attachEvent && n.attachEvent("onunload", function() {
        D();
      })), C.attributes = o(function(t) {
        return t.className = "i", !t.getAttribute("className");
      }), C.getElementsByTagName = o(function(t) {
        return t.appendChild(i.createComment("")), !t.getElementsByTagName("*").length;
      }), C.getElementsByClassName = mt.test(i.getElementsByClassName) && o(function(t) {
        return t.innerHTML = "<div class='a'></div><div class='a i'></div>", t.firstChild.className = "i", 
        2 === t.getElementsByClassName("i").length;
      }), C.getById = o(function(t) {
        return j.appendChild(t).id = F, !i.getElementsByName || !i.getElementsByName(F).length;
      }), C.getById ? (y.find.ID = function(t, e) {
        if (typeof e.getElementById !== U && P) {
          var i = e.getElementById(t);
          return i && i.parentNode ? [ i ] : [];
        }
      }, y.filter.ID = function(t) {
        var e = t.replace(Ct, yt);
        return function(t) {
          return t.getAttribute("id") === e;
        };
      }) : (delete y.find.ID, y.filter.ID = function(t) {
        var e = t.replace(Ct, yt);
        return function(t) {
          var i = typeof t.getAttributeNode !== U && t.getAttributeNode("id");
          return i && i.value === e;
        };
      }), y.find.TAG = C.getElementsByTagName ? function(t, e) {
        return typeof e.getElementsByTagName !== U ? e.getElementsByTagName(t) : void 0;
      } : function(t, e) {
        var i, n = [], o = 0, s = e.getElementsByTagName(t);
        if ("*" === t) {
          for (;i = s[o++]; ) 1 === i.nodeType && n.push(i);
          return n;
        }
        return s;
      }, y.find.CLASS = C.getElementsByClassName && function(t, e) {
        return typeof e.getElementsByClassName !== U && P ? e.getElementsByClassName(t) : void 0;
      }, N = [], E = [], (C.qsa = mt.test(i.querySelectorAll)) && (o(function(t) {
        t.innerHTML = "<select msallowclip=''><option selected=''></option></select>", t.querySelectorAll("[msallowclip^='']").length && E.push("[*^$]=" + et + "*(?:''|\"\")"), 
        t.querySelectorAll("[selected]").length || E.push("\\[" + et + "*(?:value|" + tt + ")"), 
        t.querySelectorAll(":checked").length || E.push(":checked");
      }), o(function(t) {
        var e = i.createElement("input");
        e.setAttribute("type", "hidden"), t.appendChild(e).setAttribute("name", "D"), t.querySelectorAll("[name=d]").length && E.push("name" + et + "*[*^$|!~]?="), 
        t.querySelectorAll(":enabled").length || E.push(":enabled", ":disabled"), t.querySelectorAll("*,:x"), 
        E.push(",.*:");
      })), (C.matchesSelector = mt.test(O = j.matches || j.webkitMatchesSelector || j.mozMatchesSelector || j.oMatchesSelector || j.msMatchesSelector)) && o(function(t) {
        C.disconnectedMatch = O.call(t, "div"), O.call(t, "[s!='']:x"), N.push("!=", st);
      }), E = E.length && new RegExp(E.join("|")), N = N.length && new RegExp(N.join("|")), 
      e = mt.test(j.compareDocumentPosition), M = e || mt.test(j.contains) ? function(t, e) {
        var i = 9 === t.nodeType ? t.documentElement : t, n = e && e.parentNode;
        return t === n || !(!n || 1 !== n.nodeType || !(i.contains ? i.contains(n) : t.compareDocumentPosition && 16 & t.compareDocumentPosition(n)));
      } : function(t, e) {
        if (e) for (;e = e.parentNode; ) if (e === t) return !0;
        return !1;
      }, B = e ? function(t, e) {
        if (t === e) return T = !0, 0;
        var n = !t.compareDocumentPosition - !e.compareDocumentPosition;
        return n || (1 & (n = (t.ownerDocument || t) === (e.ownerDocument || e) ? t.compareDocumentPosition(e) : 1) || !C.sortDetached && e.compareDocumentPosition(t) === n ? t === i || t.ownerDocument === q && M(q, t) ? -1 : e === i || e.ownerDocument === q && M(q, e) ? 1 : S ? Z.call(S, t) - Z.call(S, e) : 0 : 4 & n ? -1 : 1);
      } : function(t, e) {
        if (t === e) return T = !0, 0;
        var n, o = 0, s = t.parentNode, r = e.parentNode, l = [ t ], c = [ e ];
        if (!s || !r) return t === i ? -1 : e === i ? 1 : s ? -1 : r ? 1 : S ? Z.call(S, t) - Z.call(S, e) : 0;
        if (s === r) return a(t, e);
        for (n = t; n = n.parentNode; ) l.unshift(n);
        for (n = e; n = n.parentNode; ) c.unshift(n);
        for (;l[o] === c[o]; ) o++;
        return o ? a(l[o], c[o]) : l[o] === q ? -1 : c[o] === q ? 1 : 0;
      }, i) : I;
    }, e.matches = function(t, i) {
      return e(t, null, null, i);
    }, e.matchesSelector = function(t, i) {
      if ((t.ownerDocument || t) !== I && D(t), i = i.replace(ct, "='$1']"), !(!C.matchesSelector || !P || N && N.test(i) || E && E.test(i))) try {
        var n = O.call(t, i);
        if (n || C.disconnectedMatch || t.document && 11 !== t.document.nodeType) return n;
      } catch (t) {}
      return e(i, I, null, [ t ]).length > 0;
    }, e.contains = function(t, e) {
      return (t.ownerDocument || t) !== I && D(t), M(t, e);
    }, e.attr = function(t, e) {
      (t.ownerDocument || t) !== I && D(t);
      var i = y.attrHandle[e.toLowerCase()], n = i && Q.call(y.attrHandle, e.toLowerCase()) ? i(t, e, !P) : void 0;
      return void 0 !== n ? n : C.attributes || !P ? t.getAttribute(e) : (n = t.getAttributeNode(e)) && n.specified ? n.value : null;
    }, e.error = function(t) {
      throw new Error("Syntax error, unrecognized expression: " + t);
    }, e.uniqueSort = function(t) {
      var e, i = [], n = 0, o = 0;
      if (T = !C.detectDuplicates, S = !C.sortStable && t.slice(0), t.sort(B), T) {
        for (;e = t[o++]; ) e === t[o] && (n = i.push(o));
        for (;n--; ) t.splice(i[n], 1);
      }
      return S = null, t;
    }, w = e.getText = function(t) {
      var e, i = "", n = 0, o = t.nodeType;
      if (o) {
        if (1 === o || 9 === o || 11 === o) {
          if ("string" == typeof t.textContent) return t.textContent;
          for (t = t.firstChild; t; t = t.nextSibling) i += w(t);
        } else if (3 === o || 4 === o) return t.nodeValue;
      } else for (;e = t[n++]; ) i += w(e);
      return i;
    }, (y = e.selectors = {
      cacheLength: 50,
      createPseudo: n,
      match: ht,
      attrHandle: {},
      find: {},
      relative: {
        ">": {
          dir: "parentNode",
          first: !0
        },
        " ": {
          dir: "parentNode"
        },
        "+": {
          dir: "previousSibling",
          first: !0
        },
        "~": {
          dir: "previousSibling"
        }
      },
      preFilter: {
        ATTR: function(t) {
          return t[1] = t[1].replace(Ct, yt), t[3] = (t[3] || t[4] || t[5] || "").replace(Ct, yt), 
          "~=" === t[2] && (t[3] = " " + t[3] + " "), t.slice(0, 4);
        },
        CHILD: function(t) {
          return t[1] = t[1].toLowerCase(), "nth" === t[1].slice(0, 3) ? (t[3] || e.error(t[0]), 
          t[4] = +(t[4] ? t[5] + (t[6] || 1) : 2 * ("even" === t[3] || "odd" === t[3])), t[5] = +(t[7] + t[8] || "odd" === t[3])) : t[3] && e.error(t[0]), 
          t;
        },
        PSEUDO: function(t) {
          var e, i = !t[6] && t[2];
          return ht.CHILD.test(t[0]) ? null : (t[3] ? t[2] = t[4] || t[5] || "" : i && dt.test(i) && (e = k(i, !0)) && (e = i.indexOf(")", i.length - e) - i.length) && (t[0] = t[0].slice(0, e), 
          t[2] = i.slice(0, e)), t.slice(0, 3));
        }
      },
      filter: {
        TAG: function(t) {
          var e = t.replace(Ct, yt).toLowerCase();
          return "*" === t ? function() {
            return !0;
          } : function(t) {
            return t.nodeName && t.nodeName.toLowerCase() === e;
          };
        },
        CLASS: function(t) {
          var e = W[t + " "];
          return e || (e = new RegExp("(^|" + et + ")" + t + "(" + et + "|$)")) && W(t, function(t) {
            return e.test("string" == typeof t.className && t.className || typeof t.getAttribute !== U && t.getAttribute("class") || "");
          });
        },
        ATTR: function(t, i, n) {
          return function(o) {
            var s = e.attr(o, t);
            return null == s ? "!=" === i : !i || (s += "", "=" === i ? s === n : "!=" === i ? s !== n : "^=" === i ? n && 0 === s.indexOf(n) : "*=" === i ? n && s.indexOf(n) > -1 : "$=" === i ? n && s.slice(-n.length) === n : "~=" === i ? (" " + s + " ").indexOf(n) > -1 : "|=" === i && (s === n || s.slice(0, n.length + 1) === n + "-"));
          };
        },
        CHILD: function(t, e, i, n, o) {
          var s = "nth" !== t.slice(0, 3), a = "last" !== t.slice(-4), r = "of-type" === e;
          return 1 === n && 0 === o ? function(t) {
            return !!t.parentNode;
          } : function(e, i, l) {
            var c, d, u, h, p, f, m = s !== a ? "nextSibling" : "previousSibling", g = e.parentNode, v = r && e.nodeName.toLowerCase(), b = !l && !r;
            if (g) {
              if (s) {
                for (;m; ) {
                  for (u = e; u = u[m]; ) if (r ? u.nodeName.toLowerCase() === v : 1 === u.nodeType) return !1;
                  f = m = "only" === t && !f && "nextSibling";
                }
                return !0;
              }
              if (f = [ a ? g.firstChild : g.lastChild ], a && b) {
                for (p = (c = (d = g[F] || (g[F] = {}))[t] || [])[0] === H && c[1], h = c[0] === H && c[2], 
                u = p && g.childNodes[p]; u = ++p && u && u[m] || (h = p = 0) || f.pop(); ) if (1 === u.nodeType && ++h && u === e) {
                  d[t] = [ H, p, h ];
                  break;
                }
              } else if (b && (c = (e[F] || (e[F] = {}))[t]) && c[0] === H) h = c[1]; else for (;(u = ++p && u && u[m] || (h = p = 0) || f.pop()) && ((r ? u.nodeName.toLowerCase() !== v : 1 !== u.nodeType) || !++h || (b && ((u[F] || (u[F] = {}))[t] = [ H, h ]), 
              u !== e)); ) ;
              return (h -= o) === n || h % n == 0 && h / n >= 0;
            }
          };
        },
        PSEUDO: function(t, i) {
          var o, s = y.pseudos[t] || y.setFilters[t.toLowerCase()] || e.error("unsupported pseudo: " + t);
          return s[F] ? s(i) : s.length > 1 ? (o = [ t, t, "", i ], y.setFilters.hasOwnProperty(t.toLowerCase()) ? n(function(t, e) {
            for (var n, o = s(t, i), a = o.length; a--; ) n = Z.call(t, o[a]), t[n] = !(e[n] = o[a]);
          }) : function(t) {
            return s(t, 0, o);
          }) : s;
        }
      },
      pseudos: {
        not: n(function(t) {
          var e = [], i = [], o = _(t.replace(at, "$1"));
          return o[F] ? n(function(t, e, i, n) {
            for (var s, a = o(t, null, n, []), r = t.length; r--; ) (s = a[r]) && (t[r] = !(e[r] = s));
          }) : function(t, n, s) {
            return e[0] = t, o(e, null, s, i), !i.pop();
          };
        }),
        has: n(function(t) {
          return function(i) {
            return e(t, i).length > 0;
          };
        }),
        contains: n(function(t) {
          return function(e) {
            return (e.textContent || e.innerText || w(e)).indexOf(t) > -1;
          };
        }),
        lang: n(function(t) {
          return ut.test(t || "") || e.error("unsupported lang: " + t), t = t.replace(Ct, yt).toLowerCase(), 
          function(e) {
            var i;
            do {
              if (i = P ? e.lang : e.getAttribute("xml:lang") || e.getAttribute("lang")) return (i = i.toLowerCase()) === t || 0 === i.indexOf(t + "-");
            } while ((e = e.parentNode) && 1 === e.nodeType);
            return !1;
          };
        }),
        target: function(e) {
          var i = t.location && t.location.hash;
          return i && i.slice(1) === e.id;
        },
        root: function(t) {
          return t === j;
        },
        focus: function(t) {
          return t === I.activeElement && (!I.hasFocus || I.hasFocus()) && !!(t.type || t.href || ~t.tabIndex);
        },
        enabled: function(t) {
          return !1 === t.disabled;
        },
        disabled: function(t) {
          return !0 === t.disabled;
        },
        checked: function(t) {
          var e = t.nodeName.toLowerCase();
          return "input" === e && !!t.checked || "option" === e && !!t.selected;
        },
        selected: function(t) {
          return t.parentNode && t.parentNode.selectedIndex, !0 === t.selected;
        },
        empty: function(t) {
          for (t = t.firstChild; t; t = t.nextSibling) if (t.nodeType < 6) return !1;
          return !0;
        },
        parent: function(t) {
          return !y.pseudos.empty(t);
        },
        header: function(t) {
          return ft.test(t.nodeName);
        },
        input: function(t) {
          return pt.test(t.nodeName);
        },
        button: function(t) {
          var e = t.nodeName.toLowerCase();
          return "input" === e && "button" === t.type || "button" === e;
        },
        text: function(t) {
          var e;
          return "input" === t.nodeName.toLowerCase() && "text" === t.type && (null == (e = t.getAttribute("type")) || "text" === e.toLowerCase());
        },
        first: r(function() {
          return [ 0 ];
        }),
        last: r(function(t, e) {
          return [ e - 1 ];
        }),
        eq: r(function(t, e, i) {
          return [ 0 > i ? i + e : i ];
        }),
        even: r(function(t, e) {
          for (var i = 0; e > i; i += 2) t.push(i);
          return t;
        }),
        odd: r(function(t, e) {
          for (var i = 1; e > i; i += 2) t.push(i);
          return t;
        }),
        lt: r(function(t, e, i) {
          for (var n = 0 > i ? i + e : i; --n >= 0; ) t.push(n);
          return t;
        }),
        gt: r(function(t, e, i) {
          for (var n = 0 > i ? i + e : i; ++n < e; ) t.push(n);
          return t;
        })
      }
    }).pseudos.nth = y.pseudos.eq;
    for (b in {
      radio: !0,
      checkbox: !0,
      file: !0,
      password: !0,
      image: !0
    }) y.pseudos[b] = function(t) {
      return function(e) {
        return "input" === e.nodeName.toLowerCase() && e.type === t;
      };
    }(b);
    for (b in {
      submit: !0,
      reset: !0
    }) y.pseudos[b] = function(t) {
      return function(e) {
        var i = e.nodeName.toLowerCase();
        return ("input" === i || "button" === i) && e.type === t;
      };
    }(b);
    return c.prototype = y.filters = y.pseudos, y.setFilters = new c(), k = e.tokenize = function(t, i) {
      var n, o, s, a, r, l, c, d = R[t + " "];
      if (d) return i ? 0 : d.slice(0);
      for (r = t, l = [], c = y.preFilter; r; ) {
        (!n || (o = rt.exec(r))) && (o && (r = r.slice(o[0].length) || r), l.push(s = [])), 
        n = !1, (o = lt.exec(r)) && (n = o.shift(), s.push({
          value: n,
          type: o[0].replace(at, " ")
        }), r = r.slice(n.length));
        for (a in y.filter) !(o = ht[a].exec(r)) || c[a] && !(o = c[a](o)) || (n = o.shift(), 
        s.push({
          value: n,
          type: a,
          matches: o
        }), r = r.slice(n.length));
        if (!n) break;
      }
      return i ? r.length : r ? e.error(t) : R(t, l).slice(0);
    }, _ = e.compile = function(t, e) {
      var i, n = [], o = [], s = z[t + " "];
      if (!s) {
        for (e || (e = k(t)), i = e.length; i--; ) (s = g(e[i]))[F] ? n.push(s) : o.push(s);
        (s = z(t, v(o, n))).selector = t;
      }
      return s;
    }, $ = e.select = function(t, e, i, n) {
      var o, s, a, r, c, u = "function" == typeof t && t, h = !n && k(t = u.selector || t);
      if (i = i || [], 1 === h.length) {
        if ((s = h[0] = h[0].slice(0)).length > 2 && "ID" === (a = s[0]).type && C.getById && 9 === e.nodeType && P && y.relative[s[1].type]) {
          if (!(e = (y.find.ID(a.matches[0].replace(Ct, yt), e) || [])[0])) return i;
          u && (e = e.parentNode), t = t.slice(s.shift().value.length);
        }
        for (o = ht.needsContext.test(t) ? 0 : s.length; o-- && (a = s[o], !y.relative[r = a.type]); ) if ((c = y.find[r]) && (n = c(a.matches[0].replace(Ct, yt), vt.test(s[0].type) && l(e.parentNode) || e))) {
          if (s.splice(o, 1), !(t = n.length && d(s))) return K.apply(i, n), i;
          break;
        }
      }
      return (u || _(t, h))(n, e, !P, i, vt.test(t) && l(e.parentNode) || e), i;
    }, C.sortStable = F.split("").sort(B).join("") === F, C.detectDuplicates = !!T, 
    D(), C.sortDetached = o(function(t) {
      return 1 & t.compareDocumentPosition(I.createElement("div"));
    }), o(function(t) {
      return t.innerHTML = "<a href='#'></a>", "#" === t.firstChild.getAttribute("href");
    }) || s("type|href|height|width", function(t, e, i) {
      return i ? void 0 : t.getAttribute(e, "type" === e.toLowerCase() ? 1 : 2);
    }), C.attributes && o(function(t) {
      return t.innerHTML = "<input/>", t.firstChild.setAttribute("value", ""), "" === t.firstChild.getAttribute("value");
    }) || s("value", function(t, e, i) {
      return i || "input" !== t.nodeName.toLowerCase() ? void 0 : t.defaultValue;
    }), o(function(t) {
      return null == t.getAttribute("disabled");
    }) || s(tt, function(t, e, i) {
      var n;
      return i ? void 0 : !0 === t[e] ? e.toLowerCase() : (n = t.getAttributeNode(e)) && n.specified ? n.value : null;
    }), e;
  }(t);
  J.find = nt, J.expr = nt.selectors, J.expr[":"] = J.expr.pseudos, J.unique = nt.uniqueSort, 
  J.text = nt.getText, J.isXMLDoc = nt.isXML, J.contains = nt.contains;
  var ot = J.expr.match.needsContext, st = /^<(\w+)\s*\/?>(?:<\/\1>|)$/, at = /^.[^:#\[\.,]*$/;
  J.filter = function(t, e, i) {
    var n = e[0];
    return i && (t = ":not(" + t + ")"), 1 === e.length && 1 === n.nodeType ? J.find.matchesSelector(n, t) ? [ n ] : [] : J.find.matches(t, J.grep(e, function(t) {
      return 1 === t.nodeType;
    }));
  }, J.fn.extend({
    find: function(t) {
      var e, i = this.length, n = [], o = this;
      if ("string" != typeof t) return this.pushStack(J(t).filter(function() {
        for (e = 0; i > e; e++) if (J.contains(o[e], this)) return !0;
      }));
      for (e = 0; i > e; e++) J.find(t, o[e], n);
      return n = this.pushStack(i > 1 ? J.unique(n) : n), n.selector = this.selector ? this.selector + " " + t : t, 
      n;
    },
    filter: function(t) {
      return this.pushStack(n(this, t || [], !1));
    },
    not: function(t) {
      return this.pushStack(n(this, t || [], !0));
    },
    is: function(t) {
      return !!n(this, "string" == typeof t && ot.test(t) ? J(t) : t || [], !1).length;
    }
  });
  var rt, lt = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/;
  (J.fn.init = function(t, e) {
    var i, n;
    if (!t) return this;
    if ("string" == typeof t) {
      if (!(i = "<" === t[0] && ">" === t[t.length - 1] && t.length >= 3 ? [ null, t, null ] : lt.exec(t)) || !i[1] && e) return !e || e.jquery ? (e || rt).find(t) : this.constructor(e).find(t);
      if (i[1]) {
        if (e = e instanceof J ? e[0] : e, J.merge(this, J.parseHTML(i[1], e && e.nodeType ? e.ownerDocument || e : G, !0)), 
        st.test(i[1]) && J.isPlainObject(e)) for (i in e) J.isFunction(this[i]) ? this[i](e[i]) : this.attr(i, e[i]);
        return this;
      }
      return (n = G.getElementById(i[2])) && n.parentNode && (this.length = 1, this[0] = n), 
      this.context = G, this.selector = t, this;
    }
    return t.nodeType ? (this.context = this[0] = t, this.length = 1, this) : J.isFunction(t) ? void 0 !== rt.ready ? rt.ready(t) : t(J) : (void 0 !== t.selector && (this.selector = t.selector, 
    this.context = t.context), J.makeArray(t, this));
  }).prototype = J.fn, rt = J(G);
  var ct = /^(?:parents|prev(?:Until|All))/, dt = {
    children: !0,
    contents: !0,
    next: !0,
    prev: !0
  };
  J.extend({
    dir: function(t, e, i) {
      for (var n = [], o = void 0 !== i; (t = t[e]) && 9 !== t.nodeType; ) if (1 === t.nodeType) {
        if (o && J(t).is(i)) break;
        n.push(t);
      }
      return n;
    },
    sibling: function(t, e) {
      for (var i = []; t; t = t.nextSibling) 1 === t.nodeType && t !== e && i.push(t);
      return i;
    }
  }), J.fn.extend({
    has: function(t) {
      var e = J(t, this), i = e.length;
      return this.filter(function() {
        for (var t = 0; i > t; t++) if (J.contains(this, e[t])) return !0;
      });
    },
    closest: function(t, e) {
      for (var i, n = 0, o = this.length, s = [], a = ot.test(t) || "string" != typeof t ? J(t, e || this.context) : 0; o > n; n++) for (i = this[n]; i && i !== e; i = i.parentNode) if (i.nodeType < 11 && (a ? a.index(i) > -1 : 1 === i.nodeType && J.find.matchesSelector(i, t))) {
        s.push(i);
        break;
      }
      return this.pushStack(s.length > 1 ? J.unique(s) : s);
    },
    index: function(t) {
      return t ? "string" == typeof t ? U.call(J(t), this[0]) : U.call(this, t.jquery ? t[0] : t) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1;
    },
    add: function(t, e) {
      return this.pushStack(J.unique(J.merge(this.get(), J(t, e))));
    },
    addBack: function(t) {
      return this.add(null == t ? this.prevObject : this.prevObject.filter(t));
    }
  }), J.each({
    parent: function(t) {
      var e = t.parentNode;
      return e && 11 !== e.nodeType ? e : null;
    },
    parents: function(t) {
      return J.dir(t, "parentNode");
    },
    parentsUntil: function(t, e, i) {
      return J.dir(t, "parentNode", i);
    },
    next: function(t) {
      return o(t, "nextSibling");
    },
    prev: function(t) {
      return o(t, "previousSibling");
    },
    nextAll: function(t) {
      return J.dir(t, "nextSibling");
    },
    prevAll: function(t) {
      return J.dir(t, "previousSibling");
    },
    nextUntil: function(t, e, i) {
      return J.dir(t, "nextSibling", i);
    },
    prevUntil: function(t, e, i) {
      return J.dir(t, "previousSibling", i);
    },
    siblings: function(t) {
      return J.sibling((t.parentNode || {}).firstChild, t);
    },
    children: function(t) {
      return J.sibling(t.firstChild);
    },
    contents: function(t) {
      return t.contentDocument || J.merge([], t.childNodes);
    }
  }, function(t, e) {
    J.fn[t] = function(i, n) {
      var o = J.map(this, e, i);
      return "Until" !== t.slice(-5) && (n = i), n && "string" == typeof n && (o = J.filter(n, o)), 
      this.length > 1 && (dt[t] || J.unique(o), ct.test(t) && o.reverse()), this.pushStack(o);
    };
  });
  var ut = /\S+/g, ht = {};
  J.Callbacks = function(t) {
    var e, i, n, o, a, r, l = [], c = !(t = "string" == typeof t ? ht[t] || s(t) : J.extend({}, t)).once && [], d = function(s) {
      for (e = t.memory && s, i = !0, r = o || 0, o = 0, a = l.length, n = !0; l && a > r; r++) if (!1 === l[r].apply(s[0], s[1]) && t.stopOnFalse) {
        e = !1;
        break;
      }
      n = !1, l && (c ? c.length && d(c.shift()) : e ? l = [] : u.disable());
    }, u = {
      add: function() {
        if (l) {
          var i = l.length;
          !function e(i) {
            J.each(i, function(i, n) {
              var o = J.type(n);
              "function" === o ? t.unique && u.has(n) || l.push(n) : n && n.length && "string" !== o && e(n);
            });
          }(arguments), n ? a = l.length : e && (o = i, d(e));
        }
        return this;
      },
      remove: function() {
        return l && J.each(arguments, function(t, e) {
          for (var i; (i = J.inArray(e, l, i)) > -1; ) l.splice(i, 1), n && (a >= i && a--, 
          r >= i && r--);
        }), this;
      },
      has: function(t) {
        return t ? J.inArray(t, l) > -1 : !(!l || !l.length);
      },
      empty: function() {
        return l = [], a = 0, this;
      },
      disable: function() {
        return l = c = e = void 0, this;
      },
      disabled: function() {
        return !l;
      },
      lock: function() {
        return c = void 0, e || u.disable(), this;
      },
      locked: function() {
        return !c;
      },
      fireWith: function(t, e) {
        return !l || i && !c || (e = e || [], e = [ t, e.slice ? e.slice() : e ], n ? c.push(e) : d(e)), 
        this;
      },
      fire: function() {
        return u.fireWith(this, arguments), this;
      },
      fired: function() {
        return !!i;
      }
    };
    return u;
  }, J.extend({
    Deferred: function(t) {
      var e = [ [ "resolve", "done", J.Callbacks("once memory"), "resolved" ], [ "reject", "fail", J.Callbacks("once memory"), "rejected" ], [ "notify", "progress", J.Callbacks("memory") ] ], i = "pending", n = {
        state: function() {
          return i;
        },
        always: function() {
          return o.done(arguments).fail(arguments), this;
        },
        then: function() {
          var t = arguments;
          return J.Deferred(function(i) {
            J.each(e, function(e, s) {
              var a = J.isFunction(t[e]) && t[e];
              o[s[1]](function() {
                var t = a && a.apply(this, arguments);
                t && J.isFunction(t.promise) ? t.promise().done(i.resolve).fail(i.reject).progress(i.notify) : i[s[0] + "With"](this === n ? i.promise() : this, a ? [ t ] : arguments);
              });
            }), t = null;
          }).promise();
        },
        promise: function(t) {
          return null != t ? J.extend(t, n) : n;
        }
      }, o = {};
      return n.pipe = n.then, J.each(e, function(t, s) {
        var a = s[2], r = s[3];
        n[s[1]] = a.add, r && a.add(function() {
          i = r;
        }, e[1 ^ t][2].disable, e[2][2].lock), o[s[0]] = function() {
          return o[s[0] + "With"](this === o ? n : this, arguments), this;
        }, o[s[0] + "With"] = a.fireWith;
      }), n.promise(o), t && t.call(o, o), o;
    },
    when: function(t) {
      var e, i, n, o = 0, s = R.call(arguments), a = s.length, r = 1 !== a || t && J.isFunction(t.promise) ? a : 0, l = 1 === r ? t : J.Deferred(), c = function(t, i, n) {
        return function(o) {
          i[t] = this, n[t] = arguments.length > 1 ? R.call(arguments) : o, n === e ? l.notifyWith(i, n) : --r || l.resolveWith(i, n);
        };
      };
      if (a > 1) for (e = new Array(a), i = new Array(a), n = new Array(a); a > o; o++) s[o] && J.isFunction(s[o].promise) ? s[o].promise().done(c(o, n, s)).fail(l.reject).progress(c(o, i, e)) : --r;
      return r || l.resolveWith(n, s), l.promise();
    }
  });
  var pt;
  J.fn.ready = function(t) {
    return J.ready.promise().done(t), this;
  }, J.extend({
    isReady: !1,
    readyWait: 1,
    holdReady: function(t) {
      t ? J.readyWait++ : J.ready(!0);
    },
    ready: function(t) {
      (!0 === t ? --J.readyWait : J.isReady) || (J.isReady = !0, !0 !== t && --J.readyWait > 0 || (pt.resolveWith(G, [ J ]), 
      J.fn.triggerHandler && (J(G).triggerHandler("ready"), J(G).off("ready"))));
    }
  }), J.ready.promise = function(e) {
    return pt || (pt = J.Deferred(), "complete" === G.readyState ? setTimeout(J.ready) : (G.addEventListener("DOMContentLoaded", a, !1), 
    t.addEventListener("load", a, !1))), pt.promise(e);
  }, J.ready.promise();
  var ft = J.access = function(t, e, i, n, o, s, a) {
    var r = 0, l = t.length, c = null == i;
    if ("object" === J.type(i)) {
      o = !0;
      for (r in i) J.access(t, e, r, i[r], !0, s, a);
    } else if (void 0 !== n && (o = !0, J.isFunction(n) || (a = !0), c && (a ? (e.call(t, n), 
    e = null) : (c = e, e = function(t, e, i) {
      return c.call(J(t), i);
    })), e)) for (;l > r; r++) e(t[r], i, a ? n : n.call(t[r], r, e(t[r], i)));
    return o ? t : c ? e.call(t) : l ? e(t[0], i) : s;
  };
  J.acceptData = function(t) {
    return 1 === t.nodeType || 9 === t.nodeType || !+t.nodeType;
  }, r.uid = 1, r.accepts = J.acceptData, r.prototype = {
    key: function(t) {
      if (!r.accepts(t)) return 0;
      var e = {}, i = t[this.expando];
      if (!i) {
        i = r.uid++;
        try {
          e[this.expando] = {
            value: i
          }, Object.defineProperties(t, e);
        } catch (n) {
          e[this.expando] = i, J.extend(t, e);
        }
      }
      return this.cache[i] || (this.cache[i] = {}), i;
    },
    set: function(t, e, i) {
      var n, o = this.key(t), s = this.cache[o];
      if ("string" == typeof e) s[e] = i; else if (J.isEmptyObject(s)) J.extend(this.cache[o], e); else for (n in e) s[n] = e[n];
      return s;
    },
    get: function(t, e) {
      var i = this.cache[this.key(t)];
      return void 0 === e ? i : i[e];
    },
    access: function(t, e, i) {
      var n;
      return void 0 === e || e && "string" == typeof e && void 0 === i ? void 0 !== (n = this.get(t, e)) ? n : this.get(t, J.camelCase(e)) : (this.set(t, e, i), 
      void 0 !== i ? i : e);
    },
    remove: function(t, e) {
      var i, n, o, s = this.key(t), a = this.cache[s];
      if (void 0 === e) this.cache[s] = {}; else {
        J.isArray(e) ? n = e.concat(e.map(J.camelCase)) : (o = J.camelCase(e), e in a ? n = [ e, o ] : (n = o, 
        n = n in a ? [ n ] : n.match(ut) || [])), i = n.length;
        for (;i--; ) delete a[n[i]];
      }
    },
    hasData: function(t) {
      return !J.isEmptyObject(this.cache[t[this.expando]] || {});
    },
    discard: function(t) {
      t[this.expando] && delete this.cache[t[this.expando]];
    }
  };
  var mt = new r(), gt = new r(), vt = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/, bt = /([A-Z])/g;
  J.extend({
    hasData: function(t) {
      return gt.hasData(t) || mt.hasData(t);
    },
    data: function(t, e, i) {
      return gt.access(t, e, i);
    },
    removeData: function(t, e) {
      gt.remove(t, e);
    },
    _data: function(t, e, i) {
      return mt.access(t, e, i);
    },
    _removeData: function(t, e) {
      mt.remove(t, e);
    }
  }), J.fn.extend({
    data: function(t, e) {
      var i, n, o, s = this[0], a = s && s.attributes;
      if (void 0 === t) {
        if (this.length && (o = gt.get(s), 1 === s.nodeType && !mt.get(s, "hasDataAttrs"))) {
          for (i = a.length; i--; ) a[i] && 0 === (n = a[i].name).indexOf("data-") && (n = J.camelCase(n.slice(5)), 
          l(s, n, o[n]));
          mt.set(s, "hasDataAttrs", !0);
        }
        return o;
      }
      return "object" == typeof t ? this.each(function() {
        gt.set(this, t);
      }) : ft(this, function(e) {
        var i, n = J.camelCase(t);
        if (s && void 0 === e) {
          if (void 0 !== (i = gt.get(s, t))) return i;
          if (void 0 !== (i = gt.get(s, n))) return i;
          if (void 0 !== (i = l(s, n, void 0))) return i;
        } else this.each(function() {
          var i = gt.get(this, n);
          gt.set(this, n, e), -1 !== t.indexOf("-") && void 0 !== i && gt.set(this, t, e);
        });
      }, null, e, arguments.length > 1, null, !0);
    },
    removeData: function(t) {
      return this.each(function() {
        gt.remove(this, t);
      });
    }
  }), J.extend({
    queue: function(t, e, i) {
      var n;
      return t ? (e = (e || "fx") + "queue", n = mt.get(t, e), i && (!n || J.isArray(i) ? n = mt.access(t, e, J.makeArray(i)) : n.push(i)), 
      n || []) : void 0;
    },
    dequeue: function(t, e) {
      e = e || "fx";
      var i = J.queue(t, e), n = i.length, o = i.shift(), s = J._queueHooks(t, e);
      "inprogress" === o && (o = i.shift(), n--), o && ("fx" === e && i.unshift("inprogress"), 
      delete s.stop, o.call(t, function() {
        J.dequeue(t, e);
      }, s)), !n && s && s.empty.fire();
    },
    _queueHooks: function(t, e) {
      var i = e + "queueHooks";
      return mt.get(t, i) || mt.access(t, i, {
        empty: J.Callbacks("once memory").add(function() {
          mt.remove(t, [ e + "queue", i ]);
        })
      });
    }
  }), J.fn.extend({
    queue: function(t, e) {
      var i = 2;
      return "string" != typeof t && (e = t, t = "fx", i--), arguments.length < i ? J.queue(this[0], t) : void 0 === e ? this : this.each(function() {
        var i = J.queue(this, t, e);
        J._queueHooks(this, t), "fx" === t && "inprogress" !== i[0] && J.dequeue(this, t);
      });
    },
    dequeue: function(t) {
      return this.each(function() {
        J.dequeue(this, t);
      });
    },
    clearQueue: function(t) {
      return this.queue(t || "fx", []);
    },
    promise: function(t, e) {
      var i, n = 1, o = J.Deferred(), s = this, a = this.length, r = function() {
        --n || o.resolveWith(s, [ s ]);
      };
      for ("string" != typeof t && (e = t, t = void 0), t = t || "fx"; a--; ) (i = mt.get(s[a], t + "queueHooks")) && i.empty && (n++, 
      i.empty.add(r));
      return r(), o.promise(e);
    }
  });
  var Ct = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source, yt = [ "Top", "Right", "Bottom", "Left" ], wt = function(t, e) {
    return t = e || t, "none" === J.css(t, "display") || !J.contains(t.ownerDocument, t);
  }, xt = /^(?:checkbox|radio)$/i;
  !function() {
    var t = G.createDocumentFragment().appendChild(G.createElement("div")), e = G.createElement("input");
    e.setAttribute("type", "radio"), e.setAttribute("checked", "checked"), e.setAttribute("name", "t"), 
    t.appendChild(e), Y.checkClone = t.cloneNode(!0).cloneNode(!0).lastChild.checked, 
    t.innerHTML = "<textarea>x</textarea>", Y.noCloneChecked = !!t.cloneNode(!0).lastChild.defaultValue;
  }();
  var kt = "undefined";
  Y.focusinBubbles = "onfocusin" in t;
  var _t = /^key/, $t = /^(?:mouse|pointer|contextmenu)|click/, At = /^(?:focusinfocus|focusoutblur)$/, St = /^([^.]*)(?:\.(.+)|)$/;
  J.event = {
    global: {},
    add: function(t, e, i, n, o) {
      var s, a, r, l, c, d, u, h, p, f, m, g = mt.get(t);
      if (g) for (i.handler && (s = i, i = s.handler, o = s.selector), i.guid || (i.guid = J.guid++), 
      (l = g.events) || (l = g.events = {}), (a = g.handle) || (a = g.handle = function(e) {
        return typeof J !== kt && J.event.triggered !== e.type ? J.event.dispatch.apply(t, arguments) : void 0;
      }), c = (e = (e || "").match(ut) || [ "" ]).length; c--; ) r = St.exec(e[c]) || [], 
      p = m = r[1], f = (r[2] || "").split(".").sort(), p && (u = J.event.special[p] || {}, 
      p = (o ? u.delegateType : u.bindType) || p, u = J.event.special[p] || {}, d = J.extend({
        type: p,
        origType: m,
        data: n,
        handler: i,
        guid: i.guid,
        selector: o,
        needsContext: o && J.expr.match.needsContext.test(o),
        namespace: f.join(".")
      }, s), (h = l[p]) || (h = l[p] = [], h.delegateCount = 0, u.setup && !1 !== u.setup.call(t, n, f, a) || t.addEventListener && t.addEventListener(p, a, !1)), 
      u.add && (u.add.call(t, d), d.handler.guid || (d.handler.guid = i.guid)), o ? h.splice(h.delegateCount++, 0, d) : h.push(d), 
      J.event.global[p] = !0);
    },
    remove: function(t, e, i, n, o) {
      var s, a, r, l, c, d, u, h, p, f, m, g = mt.hasData(t) && mt.get(t);
      if (g && (l = g.events)) {
        for (c = (e = (e || "").match(ut) || [ "" ]).length; c--; ) if (r = St.exec(e[c]) || [], 
        p = m = r[1], f = (r[2] || "").split(".").sort(), p) {
          for (u = J.event.special[p] || {}, h = l[p = (n ? u.delegateType : u.bindType) || p] || [], 
          r = r[2] && new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)"), a = s = h.length; s--; ) d = h[s], 
          !o && m !== d.origType || i && i.guid !== d.guid || r && !r.test(d.namespace) || n && n !== d.selector && ("**" !== n || !d.selector) || (h.splice(s, 1), 
          d.selector && h.delegateCount--, u.remove && u.remove.call(t, d));
          a && !h.length && (u.teardown && !1 !== u.teardown.call(t, f, g.handle) || J.removeEvent(t, p, g.handle), 
          delete l[p]);
        } else for (p in l) J.event.remove(t, p + e[c], i, n, !0);
        J.isEmptyObject(l) && (delete g.handle, mt.remove(t, "events"));
      }
    },
    trigger: function(e, i, n, o) {
      var s, a, r, l, c, d, u, h = [ n || G ], p = X.call(e, "type") ? e.type : e, f = X.call(e, "namespace") ? e.namespace.split(".") : [];
      if (a = r = n = n || G, 3 !== n.nodeType && 8 !== n.nodeType && !At.test(p + J.event.triggered) && (p.indexOf(".") >= 0 && (f = p.split("."), 
      p = f.shift(), f.sort()), c = p.indexOf(":") < 0 && "on" + p, e = e[J.expando] ? e : new J.Event(p, "object" == typeof e && e), 
      e.isTrigger = o ? 2 : 3, e.namespace = f.join("."), e.namespace_re = e.namespace ? new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, 
      e.result = void 0, e.target || (e.target = n), i = null == i ? [ e ] : J.makeArray(i, [ e ]), 
      u = J.event.special[p] || {}, o || !u.trigger || !1 !== u.trigger.apply(n, i))) {
        if (!o && !u.noBubble && !J.isWindow(n)) {
          for (l = u.delegateType || p, At.test(l + p) || (a = a.parentNode); a; a = a.parentNode) h.push(a), 
          r = a;
          r === (n.ownerDocument || G) && h.push(r.defaultView || r.parentWindow || t);
        }
        for (s = 0; (a = h[s++]) && !e.isPropagationStopped(); ) e.type = s > 1 ? l : u.bindType || p, 
        (d = (mt.get(a, "events") || {})[e.type] && mt.get(a, "handle")) && d.apply(a, i), 
        (d = c && a[c]) && d.apply && J.acceptData(a) && (e.result = d.apply(a, i), !1 === e.result && e.preventDefault());
        return e.type = p, o || e.isDefaultPrevented() || u._default && !1 !== u._default.apply(h.pop(), i) || !J.acceptData(n) || c && J.isFunction(n[p]) && !J.isWindow(n) && ((r = n[c]) && (n[c] = null), 
        J.event.triggered = p, n[p](), J.event.triggered = void 0, r && (n[c] = r)), e.result;
      }
    },
    dispatch: function(t) {
      t = J.event.fix(t);
      var e, i, n, o, s, a = [], r = R.call(arguments), l = (mt.get(this, "events") || {})[t.type] || [], c = J.event.special[t.type] || {};
      if (r[0] = t, t.delegateTarget = this, !c.preDispatch || !1 !== c.preDispatch.call(this, t)) {
        for (a = J.event.handlers.call(this, t, l), e = 0; (o = a[e++]) && !t.isPropagationStopped(); ) for (t.currentTarget = o.elem, 
        i = 0; (s = o.handlers[i++]) && !t.isImmediatePropagationStopped(); ) (!t.namespace_re || t.namespace_re.test(s.namespace)) && (t.handleObj = s, 
        t.data = s.data, void 0 !== (n = ((J.event.special[s.origType] || {}).handle || s.handler).apply(o.elem, r)) && !1 === (t.result = n) && (t.preventDefault(), 
        t.stopPropagation()));
        return c.postDispatch && c.postDispatch.call(this, t), t.result;
      }
    },
    handlers: function(t, e) {
      var i, n, o, s, a = [], r = e.delegateCount, l = t.target;
      if (r && l.nodeType && (!t.button || "click" !== t.type)) for (;l !== this; l = l.parentNode || this) if (!0 !== l.disabled || "click" !== t.type) {
        for (n = [], i = 0; r > i; i++) s = e[i], o = s.selector + " ", void 0 === n[o] && (n[o] = s.needsContext ? J(o, this).index(l) >= 0 : J.find(o, this, null, [ l ]).length), 
        n[o] && n.push(s);
        n.length && a.push({
          elem: l,
          handlers: n
        });
      }
      return r < e.length && a.push({
        elem: this,
        handlers: e.slice(r)
      }), a;
    },
    props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
    fixHooks: {},
    keyHooks: {
      props: "char charCode key keyCode".split(" "),
      filter: function(t, e) {
        return null == t.which && (t.which = null != e.charCode ? e.charCode : e.keyCode), 
        t;
      }
    },
    mouseHooks: {
      props: "button buttons clientX clientY offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
      filter: function(t, e) {
        var i, n, o, s = e.button;
        return null == t.pageX && null != e.clientX && (i = t.target.ownerDocument || G, 
        n = i.documentElement, o = i.body, t.pageX = e.clientX + (n && n.scrollLeft || o && o.scrollLeft || 0) - (n && n.clientLeft || o && o.clientLeft || 0), 
        t.pageY = e.clientY + (n && n.scrollTop || o && o.scrollTop || 0) - (n && n.clientTop || o && o.clientTop || 0)), 
        t.which || void 0 === s || (t.which = 1 & s ? 1 : 2 & s ? 3 : 4 & s ? 2 : 0), t;
      }
    },
    fix: function(t) {
      if (t[J.expando]) return t;
      var e, i, n, o = t.type, s = t, a = this.fixHooks[o];
      for (a || (this.fixHooks[o] = a = $t.test(o) ? this.mouseHooks : _t.test(o) ? this.keyHooks : {}), 
      n = a.props ? this.props.concat(a.props) : this.props, t = new J.Event(s), e = n.length; e--; ) i = n[e], 
      t[i] = s[i];
      return t.target || (t.target = G), 3 === t.target.nodeType && (t.target = t.target.parentNode), 
      a.filter ? a.filter(t, s) : t;
    },
    special: {
      load: {
        noBubble: !0
      },
      focus: {
        trigger: function() {
          return this !== u() && this.focus ? (this.focus(), !1) : void 0;
        },
        delegateType: "focusin"
      },
      blur: {
        trigger: function() {
          return this === u() && this.blur ? (this.blur(), !1) : void 0;
        },
        delegateType: "focusout"
      },
      click: {
        trigger: function() {
          return "checkbox" === this.type && this.click && J.nodeName(this, "input") ? (this.click(), 
          !1) : void 0;
        },
        _default: function(t) {
          return J.nodeName(t.target, "a");
        }
      },
      beforeunload: {
        postDispatch: function(t) {
          void 0 !== t.result && t.originalEvent && (t.originalEvent.returnValue = t.result);
        }
      }
    },
    simulate: function(t, e, i, n) {
      var o = J.extend(new J.Event(), i, {
        type: t,
        isSimulated: !0,
        originalEvent: {}
      });
      n ? J.event.trigger(o, null, e) : J.event.dispatch.call(e, o), o.isDefaultPrevented() && i.preventDefault();
    }
  }, J.removeEvent = function(t, e, i) {
    t.removeEventListener && t.removeEventListener(e, i, !1);
  }, J.Event = function(t, e) {
    return this instanceof J.Event ? (t && t.type ? (this.originalEvent = t, this.type = t.type, 
    this.isDefaultPrevented = t.defaultPrevented || void 0 === t.defaultPrevented && !1 === t.returnValue ? c : d) : this.type = t, 
    e && J.extend(this, e), this.timeStamp = t && t.timeStamp || J.now(), void (this[J.expando] = !0)) : new J.Event(t, e);
  }, J.Event.prototype = {
    isDefaultPrevented: d,
    isPropagationStopped: d,
    isImmediatePropagationStopped: d,
    preventDefault: function() {
      var t = this.originalEvent;
      this.isDefaultPrevented = c, t && t.preventDefault && t.preventDefault();
    },
    stopPropagation: function() {
      var t = this.originalEvent;
      this.isPropagationStopped = c, t && t.stopPropagation && t.stopPropagation();
    },
    stopImmediatePropagation: function() {
      var t = this.originalEvent;
      this.isImmediatePropagationStopped = c, t && t.stopImmediatePropagation && t.stopImmediatePropagation(), 
      this.stopPropagation();
    }
  }, J.each({
    mouseenter: "mouseover",
    mouseleave: "mouseout",
    pointerenter: "pointerover",
    pointerleave: "pointerout"
  }, function(t, e) {
    J.event.special[t] = {
      delegateType: e,
      bindType: e,
      handle: function(t) {
        var i, n = this, o = t.relatedTarget, s = t.handleObj;
        return (!o || o !== n && !J.contains(n, o)) && (t.type = s.origType, i = s.handler.apply(this, arguments), 
        t.type = e), i;
      }
    };
  }), Y.focusinBubbles || J.each({
    focus: "focusin",
    blur: "focusout"
  }, function(t, e) {
    var i = function(t) {
      J.event.simulate(e, t.target, J.event.fix(t), !0);
    };
    J.event.special[e] = {
      setup: function() {
        var n = this.ownerDocument || this, o = mt.access(n, e);
        o || n.addEventListener(t, i, !0), mt.access(n, e, (o || 0) + 1);
      },
      teardown: function() {
        var n = this.ownerDocument || this, o = mt.access(n, e) - 1;
        o ? mt.access(n, e, o) : (n.removeEventListener(t, i, !0), mt.remove(n, e));
      }
    };
  }), J.fn.extend({
    on: function(t, e, i, n, o) {
      var s, a;
      if ("object" == typeof t) {
        "string" != typeof e && (i = i || e, e = void 0);
        for (a in t) this.on(a, e, i, t[a], o);
        return this;
      }
      if (null == i && null == n ? (n = e, i = e = void 0) : null == n && ("string" == typeof e ? (n = i, 
      i = void 0) : (n = i, i = e, e = void 0)), !1 === n) n = d; else if (!n) return this;
      return 1 === o && (s = n, n = function(t) {
        return J().off(t), s.apply(this, arguments);
      }, n.guid = s.guid || (s.guid = J.guid++)), this.each(function() {
        J.event.add(this, t, n, i, e);
      });
    },
    one: function(t, e, i, n) {
      return this.on(t, e, i, n, 1);
    },
    off: function(t, e, i) {
      var n, o;
      if (t && t.preventDefault && t.handleObj) return n = t.handleObj, J(t.delegateTarget).off(n.namespace ? n.origType + "." + n.namespace : n.origType, n.selector, n.handler), 
      this;
      if ("object" == typeof t) {
        for (o in t) this.off(o, e, t[o]);
        return this;
      }
      return (!1 === e || "function" == typeof e) && (i = e, e = void 0), !1 === i && (i = d), 
      this.each(function() {
        J.event.remove(this, t, i, e);
      });
    },
    trigger: function(t, e) {
      return this.each(function() {
        J.event.trigger(t, e, this);
      });
    },
    triggerHandler: function(t, e) {
      var i = this[0];
      return i ? J.event.trigger(t, e, i, !0) : void 0;
    }
  });
  var Tt = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi, Dt = /<([\w:]+)/, It = /<|&#?\w+;/, jt = /<(?:script|style|link)/i, Pt = /checked\s*(?:[^=]|=\s*.checked.)/i, Et = /^$|\/(?:java|ecma)script/i, Nt = /^true\/(.*)/, Ot = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g, Mt = {
    option: [ 1, "<select multiple='multiple'>", "</select>" ],
    thead: [ 1, "<table>", "</table>" ],
    col: [ 2, "<table><colgroup>", "</colgroup></table>" ],
    tr: [ 2, "<table><tbody>", "</tbody></table>" ],
    td: [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ],
    _default: [ 0, "", "" ]
  };
  Mt.optgroup = Mt.option, Mt.tbody = Mt.tfoot = Mt.colgroup = Mt.caption = Mt.thead, 
  Mt.th = Mt.td, J.extend({
    clone: function(t, e, i) {
      var n, o, s, a, r = t.cloneNode(!0), l = J.contains(t.ownerDocument, t);
      if (!(Y.noCloneChecked || 1 !== t.nodeType && 11 !== t.nodeType || J.isXMLDoc(t))) for (a = v(r), 
      s = v(t), n = 0, o = s.length; o > n; n++) b(s[n], a[n]);
      if (e) if (i) for (s = s || v(t), a = a || v(r), n = 0, o = s.length; o > n; n++) g(s[n], a[n]); else g(t, r);
      return (a = v(r, "script")).length > 0 && m(a, !l && v(t, "script")), r;
    },
    buildFragment: function(t, e, i, n) {
      for (var o, s, a, r, l, c, d = e.createDocumentFragment(), u = [], h = 0, p = t.length; p > h; h++) if ((o = t[h]) || 0 === o) if ("object" === J.type(o)) J.merge(u, o.nodeType ? [ o ] : o); else if (It.test(o)) {
        for (s = s || d.appendChild(e.createElement("div")), a = (Dt.exec(o) || [ "", "" ])[1].toLowerCase(), 
        r = Mt[a] || Mt._default, s.innerHTML = r[1] + o.replace(Tt, "<$1></$2>") + r[2], 
        c = r[0]; c--; ) s = s.lastChild;
        J.merge(u, s.childNodes), (s = d.firstChild).textContent = "";
      } else u.push(e.createTextNode(o));
      for (d.textContent = "", h = 0; o = u[h++]; ) if ((!n || -1 === J.inArray(o, n)) && (l = J.contains(o.ownerDocument, o), 
      s = v(d.appendChild(o), "script"), l && m(s), i)) for (c = 0; o = s[c++]; ) Et.test(o.type || "") && i.push(o);
      return d;
    },
    cleanData: function(t) {
      for (var e, i, n, o, s = J.event.special, a = 0; void 0 !== (i = t[a]); a++) {
        if (J.acceptData(i) && (o = i[mt.expando]) && (e = mt.cache[o])) {
          if (e.events) for (n in e.events) s[n] ? J.event.remove(i, n) : J.removeEvent(i, n, e.handle);
          mt.cache[o] && delete mt.cache[o];
        }
        delete gt.cache[i[gt.expando]];
      }
    }
  }), J.fn.extend({
    text: function(t) {
      return ft(this, function(t) {
        return void 0 === t ? J.text(this) : this.empty().each(function() {
          (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) && (this.textContent = t);
        });
      }, null, t, arguments.length);
    },
    append: function() {
      return this.domManip(arguments, function(t) {
        1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || h(this, t).appendChild(t);
      });
    },
    prepend: function() {
      return this.domManip(arguments, function(t) {
        if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
          var e = h(this, t);
          e.insertBefore(t, e.firstChild);
        }
      });
    },
    before: function() {
      return this.domManip(arguments, function(t) {
        this.parentNode && this.parentNode.insertBefore(t, this);
      });
    },
    after: function() {
      return this.domManip(arguments, function(t) {
        this.parentNode && this.parentNode.insertBefore(t, this.nextSibling);
      });
    },
    remove: function(t, e) {
      for (var i, n = t ? J.filter(t, this) : this, o = 0; null != (i = n[o]); o++) e || 1 !== i.nodeType || J.cleanData(v(i)), 
      i.parentNode && (e && J.contains(i.ownerDocument, i) && m(v(i, "script")), i.parentNode.removeChild(i));
      return this;
    },
    empty: function() {
      for (var t, e = 0; null != (t = this[e]); e++) 1 === t.nodeType && (J.cleanData(v(t, !1)), 
      t.textContent = "");
      return this;
    },
    clone: function(t, e) {
      return t = null != t && t, e = null == e ? t : e, this.map(function() {
        return J.clone(this, t, e);
      });
    },
    html: function(t) {
      return ft(this, function(t) {
        var e = this[0] || {}, i = 0, n = this.length;
        if (void 0 === t && 1 === e.nodeType) return e.innerHTML;
        if ("string" == typeof t && !jt.test(t) && !Mt[(Dt.exec(t) || [ "", "" ])[1].toLowerCase()]) {
          t = t.replace(Tt, "<$1></$2>");
          try {
            for (;n > i; i++) 1 === (e = this[i] || {}).nodeType && (J.cleanData(v(e, !1)), 
            e.innerHTML = t);
            e = 0;
          } catch (t) {}
        }
        e && this.empty().append(t);
      }, null, t, arguments.length);
    },
    replaceWith: function() {
      var t = arguments[0];
      return this.domManip(arguments, function(e) {
        t = this.parentNode, J.cleanData(v(this)), t && t.replaceChild(e, this);
      }), t && (t.length || t.nodeType) ? this : this.remove();
    },
    detach: function(t) {
      return this.remove(t, !0);
    },
    domManip: function(t, e) {
      t = z.apply([], t);
      var i, n, o, s, a, r, l = 0, c = this.length, d = this, u = c - 1, h = t[0], m = J.isFunction(h);
      if (m || c > 1 && "string" == typeof h && !Y.checkClone && Pt.test(h)) return this.each(function(i) {
        var n = d.eq(i);
        m && (t[0] = h.call(this, i, n.html())), n.domManip(t, e);
      });
      if (c && (i = J.buildFragment(t, this[0].ownerDocument, !1, this), n = i.firstChild, 
      1 === i.childNodes.length && (i = n), n)) {
        for (s = (o = J.map(v(i, "script"), p)).length; c > l; l++) a = i, l !== u && (a = J.clone(a, !0, !0), 
        s && J.merge(o, v(a, "script"))), e.call(this[l], a, l);
        if (s) for (r = o[o.length - 1].ownerDocument, J.map(o, f), l = 0; s > l; l++) a = o[l], 
        Et.test(a.type || "") && !mt.access(a, "globalEval") && J.contains(r, a) && (a.src ? J._evalUrl && J._evalUrl(a.src) : J.globalEval(a.textContent.replace(Ot, "")));
      }
      return this;
    }
  }), J.each({
    appendTo: "append",
    prependTo: "prepend",
    insertBefore: "before",
    insertAfter: "after",
    replaceAll: "replaceWith"
  }, function(t, e) {
    J.fn[t] = function(t) {
      for (var i, n = [], o = J(t), s = o.length - 1, a = 0; s >= a; a++) i = a === s ? this : this.clone(!0), 
      J(o[a])[e](i), B.apply(n, i.get());
      return this.pushStack(n);
    };
  });
  var Ft, qt = {}, Ht = /^margin/, Lt = new RegExp("^(" + Ct + ")(?!px)[a-z%]+$", "i"), Wt = function(t) {
    return t.ownerDocument.defaultView.getComputedStyle(t, null);
  };
  !function() {
    function e() {
      a.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:block;margin-top:1%;top:1%;border:1px;padding:1px;width:4px;position:absolute", 
      a.innerHTML = "", o.appendChild(s);
      var e = t.getComputedStyle(a, null);
      i = "1%" !== e.top, n = "4px" === e.width, o.removeChild(s);
    }
    var i, n, o = G.documentElement, s = G.createElement("div"), a = G.createElement("div");
    a.style && (a.style.backgroundClip = "content-box", a.cloneNode(!0).style.backgroundClip = "", 
    Y.clearCloneStyle = "content-box" === a.style.backgroundClip, s.style.cssText = "border:0;width:0;height:0;top:0;left:-9999px;margin-top:1px;position:absolute", 
    s.appendChild(a), t.getComputedStyle && J.extend(Y, {
      pixelPosition: function() {
        return e(), i;
      },
      boxSizingReliable: function() {
        return null == n && e(), n;
      },
      reliableMarginRight: function() {
        var e, i = a.appendChild(G.createElement("div"));
        return i.style.cssText = a.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0", 
        i.style.marginRight = i.style.width = "0", a.style.width = "1px", o.appendChild(s), 
        e = !parseFloat(t.getComputedStyle(i, null).marginRight), o.removeChild(s), e;
      }
    }));
  }(), J.swap = function(t, e, i, n) {
    var o, s, a = {};
    for (s in e) a[s] = t.style[s], t.style[s] = e[s];
    o = i.apply(t, n || []);
    for (s in e) t.style[s] = a[s];
    return o;
  };
  var Rt = /^(none|table(?!-c[ea]).+)/, zt = new RegExp("^(" + Ct + ")(.*)$", "i"), Bt = new RegExp("^([+-])=(" + Ct + ")", "i"), Ut = {
    position: "absolute",
    visibility: "hidden",
    display: "block"
  }, Vt = {
    letterSpacing: "0",
    fontWeight: "400"
  }, Qt = [ "Webkit", "O", "Moz", "ms" ];
  J.extend({
    cssHooks: {
      opacity: {
        get: function(t, e) {
          if (e) {
            var i = w(t, "opacity");
            return "" === i ? "1" : i;
          }
        }
      }
    },
    cssNumber: {
      columnCount: !0,
      fillOpacity: !0,
      flexGrow: !0,
      flexShrink: !0,
      fontWeight: !0,
      lineHeight: !0,
      opacity: !0,
      order: !0,
      orphans: !0,
      widows: !0,
      zIndex: !0,
      zoom: !0
    },
    cssProps: {
      float: "cssFloat"
    },
    style: function(t, e, i, n) {
      if (t && 3 !== t.nodeType && 8 !== t.nodeType && t.style) {
        var o, s, a, r = J.camelCase(e), l = t.style;
        return e = J.cssProps[r] || (J.cssProps[r] = k(l, r)), a = J.cssHooks[e] || J.cssHooks[r], 
        void 0 === i ? a && "get" in a && void 0 !== (o = a.get(t, !1, n)) ? o : l[e] : ("string" === (s = typeof i) && (o = Bt.exec(i)) && (i = (o[1] + 1) * o[2] + parseFloat(J.css(t, e)), 
        s = "number"), void (null != i && i === i && ("number" !== s || J.cssNumber[r] || (i += "px"), 
        Y.clearCloneStyle || "" !== i || 0 !== e.indexOf("background") || (l[e] = "inherit"), 
        a && "set" in a && void 0 === (i = a.set(t, i, n)) || (l[e] = i))));
      }
    },
    css: function(t, e, i, n) {
      var o, s, a, r = J.camelCase(e);
      return e = J.cssProps[r] || (J.cssProps[r] = k(t.style, r)), (a = J.cssHooks[e] || J.cssHooks[r]) && "get" in a && (o = a.get(t, !0, i)), 
      void 0 === o && (o = w(t, e, n)), "normal" === o && e in Vt && (o = Vt[e]), "" === i || i ? (s = parseFloat(o), 
      !0 === i || J.isNumeric(s) ? s || 0 : o) : o;
    }
  }), J.each([ "height", "width" ], function(t, e) {
    J.cssHooks[e] = {
      get: function(t, i, n) {
        return i ? Rt.test(J.css(t, "display")) && 0 === t.offsetWidth ? J.swap(t, Ut, function() {
          return A(t, e, n);
        }) : A(t, e, n) : void 0;
      },
      set: function(t, i, n) {
        var o = n && Wt(t);
        return _(0, i, n ? $(t, e, n, "border-box" === J.css(t, "boxSizing", !1, o), o) : 0);
      }
    };
  }), J.cssHooks.marginRight = x(Y.reliableMarginRight, function(t, e) {
    return e ? J.swap(t, {
      display: "inline-block"
    }, w, [ t, "marginRight" ]) : void 0;
  }), J.each({
    margin: "",
    padding: "",
    border: "Width"
  }, function(t, e) {
    J.cssHooks[t + e] = {
      expand: function(i) {
        for (var n = 0, o = {}, s = "string" == typeof i ? i.split(" ") : [ i ]; 4 > n; n++) o[t + yt[n] + e] = s[n] || s[n - 2] || s[0];
        return o;
      }
    }, Ht.test(t) || (J.cssHooks[t + e].set = _);
  }), J.fn.extend({
    css: function(t, e) {
      return ft(this, function(t, e, i) {
        var n, o, s = {}, a = 0;
        if (J.isArray(e)) {
          for (n = Wt(t), o = e.length; o > a; a++) s[e[a]] = J.css(t, e[a], !1, n);
          return s;
        }
        return void 0 !== i ? J.style(t, e, i) : J.css(t, e);
      }, t, e, arguments.length > 1);
    },
    show: function() {
      return S(this, !0);
    },
    hide: function() {
      return S(this);
    },
    toggle: function(t) {
      return "boolean" == typeof t ? t ? this.show() : this.hide() : this.each(function() {
        wt(this) ? J(this).show() : J(this).hide();
      });
    }
  }), J.Tween = T, T.prototype = {
    constructor: T,
    init: function(t, e, i, n, o, s) {
      this.elem = t, this.prop = i, this.easing = o || "swing", this.options = e, this.start = this.now = this.cur(), 
      this.end = n, this.unit = s || (J.cssNumber[i] ? "" : "px");
    },
    cur: function() {
      var t = T.propHooks[this.prop];
      return t && t.get ? t.get(this) : T.propHooks._default.get(this);
    },
    run: function(t) {
      var e, i = T.propHooks[this.prop];
      return this.pos = e = this.options.duration ? J.easing[this.easing](t, this.options.duration * t, 0, 1, this.options.duration) : t, 
      this.now = (this.end - this.start) * e + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), 
      i && i.set ? i.set(this) : T.propHooks._default.set(this), this;
    }
  }, T.prototype.init.prototype = T.prototype, T.propHooks = {
    _default: {
      get: function(t) {
        var e;
        return null == t.elem[t.prop] || t.elem.style && null != t.elem.style[t.prop] ? (e = J.css(t.elem, t.prop, "")) && "auto" !== e ? e : 0 : t.elem[t.prop];
      },
      set: function(t) {
        J.fx.step[t.prop] ? J.fx.step[t.prop](t) : t.elem.style && (null != t.elem.style[J.cssProps[t.prop]] || J.cssHooks[t.prop]) ? J.style(t.elem, t.prop, t.now + t.unit) : t.elem[t.prop] = t.now;
      }
    }
  }, T.propHooks.scrollTop = T.propHooks.scrollLeft = {
    set: function(t) {
      t.elem.nodeType && t.elem.parentNode && (t.elem[t.prop] = t.now);
    }
  }, J.easing = {
    linear: function(t) {
      return t;
    },
    swing: function(t) {
      return .5 - Math.cos(t * Math.PI) / 2;
    }
  }, J.fx = T.prototype.init, J.fx.step = {};
  var Xt, Yt, Gt = /^(?:toggle|show|hide)$/, Kt = new RegExp("^(?:([+-])=|)(" + Ct + ")([a-z%]*)$", "i"), Jt = /queueHooks$/, Zt = [ function(t, e, i) {
    var n, o, s, a, r, l, c, d = this, u = {}, h = t.style, p = t.nodeType && wt(t), f = mt.get(t, "fxshow");
    i.queue || (null == (r = J._queueHooks(t, "fx")).unqueued && (r.unqueued = 0, l = r.empty.fire, 
    r.empty.fire = function() {
      r.unqueued || l();
    }), r.unqueued++, d.always(function() {
      d.always(function() {
        r.unqueued--, J.queue(t, "fx").length || r.empty.fire();
      });
    })), 1 === t.nodeType && ("height" in e || "width" in e) && (i.overflow = [ h.overflow, h.overflowX, h.overflowY ], 
    "inline" === ("none" === (c = J.css(t, "display")) ? mt.get(t, "olddisplay") || y(t.nodeName) : c) && "none" === J.css(t, "float") && (h.display = "inline-block")), 
    i.overflow && (h.overflow = "hidden", d.always(function() {
      h.overflow = i.overflow[0], h.overflowX = i.overflow[1], h.overflowY = i.overflow[2];
    }));
    for (n in e) if (o = e[n], Gt.exec(o)) {
      if (delete e[n], s = s || "toggle" === o, o === (p ? "hide" : "show")) {
        if ("show" !== o || !f || void 0 === f[n]) continue;
        p = !0;
      }
      u[n] = f && f[n] || J.style(t, n);
    } else c = void 0;
    if (J.isEmptyObject(u)) "inline" === ("none" === c ? y(t.nodeName) : c) && (h.display = c); else {
      f ? "hidden" in f && (p = f.hidden) : f = mt.access(t, "fxshow", {}), s && (f.hidden = !p), 
      p ? J(t).show() : d.done(function() {
        J(t).hide();
      }), d.done(function() {
        var e;
        mt.remove(t, "fxshow");
        for (e in u) J.style(t, e, u[e]);
      });
      for (n in u) a = j(p ? f[n] : 0, n, d), n in f || (f[n] = a.start, p && (a.end = a.start, 
      a.start = "width" === n || "height" === n ? 1 : 0));
    }
  } ], te = {
    "*": [ function(t, e) {
      var i = this.createTween(t, e), n = i.cur(), o = Kt.exec(e), s = o && o[3] || (J.cssNumber[t] ? "" : "px"), a = (J.cssNumber[t] || "px" !== s && +n) && Kt.exec(J.css(i.elem, t)), r = 1, l = 20;
      if (a && a[3] !== s) {
        s = s || a[3], o = o || [], a = +n || 1;
        do {
          r = r || ".5", a /= r, J.style(i.elem, t, a + s);
        } while (r !== (r = i.cur() / n) && 1 !== r && --l);
      }
      return o && (a = i.start = +a || +n || 0, i.unit = s, i.end = o[1] ? a + (o[1] + 1) * o[2] : +o[2]), 
      i;
    } ]
  };
  J.Animation = J.extend(E, {
    tweener: function(t, e) {
      J.isFunction(t) ? (e = t, t = [ "*" ]) : t = t.split(" ");
      for (var i, n = 0, o = t.length; o > n; n++) i = t[n], te[i] = te[i] || [], te[i].unshift(e);
    },
    prefilter: function(t, e) {
      e ? Zt.unshift(t) : Zt.push(t);
    }
  }), J.speed = function(t, e, i) {
    var n = t && "object" == typeof t ? J.extend({}, t) : {
      complete: i || !i && e || J.isFunction(t) && t,
      duration: t,
      easing: i && e || e && !J.isFunction(e) && e
    };
    return n.duration = J.fx.off ? 0 : "number" == typeof n.duration ? n.duration : n.duration in J.fx.speeds ? J.fx.speeds[n.duration] : J.fx.speeds._default, 
    (null == n.queue || !0 === n.queue) && (n.queue = "fx"), n.old = n.complete, n.complete = function() {
      J.isFunction(n.old) && n.old.call(this), n.queue && J.dequeue(this, n.queue);
    }, n;
  }, J.fn.extend({
    fadeTo: function(t, e, i, n) {
      return this.filter(wt).css("opacity", 0).show().end().animate({
        opacity: e
      }, t, i, n);
    },
    animate: function(t, e, i, n) {
      var o = J.isEmptyObject(t), s = J.speed(e, i, n), a = function() {
        var e = E(this, J.extend({}, t), s);
        (o || mt.get(this, "finish")) && e.stop(!0);
      };
      return a.finish = a, o || !1 === s.queue ? this.each(a) : this.queue(s.queue, a);
    },
    stop: function(t, e, i) {
      var n = function(t) {
        var e = t.stop;
        delete t.stop, e(i);
      };
      return "string" != typeof t && (i = e, e = t, t = void 0), e && !1 !== t && this.queue(t || "fx", []), 
      this.each(function() {
        var e = !0, o = null != t && t + "queueHooks", s = J.timers, a = mt.get(this);
        if (o) a[o] && a[o].stop && n(a[o]); else for (o in a) a[o] && a[o].stop && Jt.test(o) && n(a[o]);
        for (o = s.length; o--; ) s[o].elem !== this || null != t && s[o].queue !== t || (s[o].anim.stop(i), 
        e = !1, s.splice(o, 1));
        (e || !i) && J.dequeue(this, t);
      });
    },
    finish: function(t) {
      return !1 !== t && (t = t || "fx"), this.each(function() {
        var e, i = mt.get(this), n = i[t + "queue"], o = i[t + "queueHooks"], s = J.timers, a = n ? n.length : 0;
        for (i.finish = !0, J.queue(this, t, []), o && o.stop && o.stop.call(this, !0), 
        e = s.length; e--; ) s[e].elem === this && s[e].queue === t && (s[e].anim.stop(!0), 
        s.splice(e, 1));
        for (e = 0; a > e; e++) n[e] && n[e].finish && n[e].finish.call(this);
        delete i.finish;
      });
    }
  }), J.each([ "toggle", "show", "hide" ], function(t, e) {
    var i = J.fn[e];
    J.fn[e] = function(t, n, o) {
      return null == t || "boolean" == typeof t ? i.apply(this, arguments) : this.animate(I(e, !0), t, n, o);
    };
  }), J.each({
    slideDown: I("show"),
    slideUp: I("hide"),
    slideToggle: I("toggle"),
    fadeIn: {
      opacity: "show"
    },
    fadeOut: {
      opacity: "hide"
    },
    fadeToggle: {
      opacity: "toggle"
    }
  }, function(t, e) {
    J.fn[t] = function(t, i, n) {
      return this.animate(e, t, i, n);
    };
  }), J.timers = [], J.fx.tick = function() {
    var t, e = 0, i = J.timers;
    for (Xt = J.now(); e < i.length; e++) (t = i[e])() || i[e] !== t || i.splice(e--, 1);
    i.length || J.fx.stop(), Xt = void 0;
  }, J.fx.timer = function(t) {
    J.timers.push(t), t() ? J.fx.start() : J.timers.pop();
  }, J.fx.interval = 13, J.fx.start = function() {
    Yt || (Yt = setInterval(J.fx.tick, J.fx.interval));
  }, J.fx.stop = function() {
    clearInterval(Yt), Yt = null;
  }, J.fx.speeds = {
    slow: 600,
    fast: 200,
    _default: 400
  }, J.fn.delay = function(t, e) {
    return t = J.fx ? J.fx.speeds[t] || t : t, e = e || "fx", this.queue(e, function(e, i) {
      var n = setTimeout(e, t);
      i.stop = function() {
        clearTimeout(n);
      };
    });
  }, function() {
    var t = G.createElement("input"), e = G.createElement("select"), i = e.appendChild(G.createElement("option"));
    t.type = "checkbox", Y.checkOn = "" !== t.value, Y.optSelected = i.selected, e.disabled = !0, 
    Y.optDisabled = !i.disabled, (t = G.createElement("input")).value = "t", t.type = "radio", 
    Y.radioValue = "t" === t.value;
  }();
  var ee, ie = J.expr.attrHandle;
  J.fn.extend({
    attr: function(t, e) {
      return ft(this, J.attr, t, e, arguments.length > 1);
    },
    removeAttr: function(t) {
      return this.each(function() {
        J.removeAttr(this, t);
      });
    }
  }), J.extend({
    attr: function(t, e, i) {
      var n, o, s = t.nodeType;
      if (t && 3 !== s && 8 !== s && 2 !== s) return typeof t.getAttribute === kt ? J.prop(t, e, i) : (1 === s && J.isXMLDoc(t) || (e = e.toLowerCase(), 
      n = J.attrHooks[e] || (J.expr.match.bool.test(e) ? ee : void 0)), void 0 === i ? n && "get" in n && null !== (o = n.get(t, e)) ? o : null == (o = J.find.attr(t, e)) ? void 0 : o : null !== i ? n && "set" in n && void 0 !== (o = n.set(t, i, e)) ? o : (t.setAttribute(e, i + ""), 
      i) : void J.removeAttr(t, e));
    },
    removeAttr: function(t, e) {
      var i, n, o = 0, s = e && e.match(ut);
      if (s && 1 === t.nodeType) for (;i = s[o++]; ) n = J.propFix[i] || i, J.expr.match.bool.test(i) && (t[n] = !1), 
      t.removeAttribute(i);
    },
    attrHooks: {
      type: {
        set: function(t, e) {
          if (!Y.radioValue && "radio" === e && J.nodeName(t, "input")) {
            var i = t.value;
            return t.setAttribute("type", e), i && (t.value = i), e;
          }
        }
      }
    }
  }), ee = {
    set: function(t, e, i) {
      return !1 === e ? J.removeAttr(t, i) : t.setAttribute(i, i), i;
    }
  }, J.each(J.expr.match.bool.source.match(/\w+/g), function(t, e) {
    var i = ie[e] || J.find.attr;
    ie[e] = function(t, e, n) {
      var o, s;
      return n || (s = ie[e], ie[e] = o, o = null != i(t, e, n) ? e.toLowerCase() : null, 
      ie[e] = s), o;
    };
  });
  var ne = /^(?:input|select|textarea|button)$/i;
  J.fn.extend({
    prop: function(t, e) {
      return ft(this, J.prop, t, e, arguments.length > 1);
    },
    removeProp: function(t) {
      return this.each(function() {
        delete this[J.propFix[t] || t];
      });
    }
  }), J.extend({
    propFix: {
      for: "htmlFor",
      class: "className"
    },
    prop: function(t, e, i) {
      var n, o, s = t.nodeType;
      if (t && 3 !== s && 8 !== s && 2 !== s) return (1 !== s || !J.isXMLDoc(t)) && (e = J.propFix[e] || e, 
      o = J.propHooks[e]), void 0 !== i ? o && "set" in o && void 0 !== (n = o.set(t, i, e)) ? n : t[e] = i : o && "get" in o && null !== (n = o.get(t, e)) ? n : t[e];
    },
    propHooks: {
      tabIndex: {
        get: function(t) {
          return t.hasAttribute("tabindex") || ne.test(t.nodeName) || t.href ? t.tabIndex : -1;
        }
      }
    }
  }), Y.optSelected || (J.propHooks.selected = {
    get: function(t) {
      var e = t.parentNode;
      return e && e.parentNode && e.parentNode.selectedIndex, null;
    }
  }), J.each([ "tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable" ], function() {
    J.propFix[this.toLowerCase()] = this;
  });
  var oe = /[\t\r\n\f]/g;
  J.fn.extend({
    addClass: function(t) {
      var e, i, n, o, s, a, r = "string" == typeof t && t, l = 0, c = this.length;
      if (J.isFunction(t)) return this.each(function(e) {
        J(this).addClass(t.call(this, e, this.className));
      });
      if (r) for (e = (t || "").match(ut) || []; c > l; l++) if (i = this[l], n = 1 === i.nodeType && (i.className ? (" " + i.className + " ").replace(oe, " ") : " ")) {
        for (s = 0; o = e[s++]; ) n.indexOf(" " + o + " ") < 0 && (n += o + " ");
        a = J.trim(n), i.className !== a && (i.className = a);
      }
      return this;
    },
    removeClass: function(t) {
      var e, i, n, o, s, a, r = 0 === arguments.length || "string" == typeof t && t, l = 0, c = this.length;
      if (J.isFunction(t)) return this.each(function(e) {
        J(this).removeClass(t.call(this, e, this.className));
      });
      if (r) for (e = (t || "").match(ut) || []; c > l; l++) if (i = this[l], n = 1 === i.nodeType && (i.className ? (" " + i.className + " ").replace(oe, " ") : "")) {
        for (s = 0; o = e[s++]; ) for (;n.indexOf(" " + o + " ") >= 0; ) n = n.replace(" " + o + " ", " ");
        a = t ? J.trim(n) : "", i.className !== a && (i.className = a);
      }
      return this;
    },
    toggleClass: function(t, e) {
      var i = typeof t;
      return "boolean" == typeof e && "string" === i ? e ? this.addClass(t) : this.removeClass(t) : this.each(J.isFunction(t) ? function(i) {
        J(this).toggleClass(t.call(this, i, this.className, e), e);
      } : function() {
        if ("string" === i) for (var e, n = 0, o = J(this), s = t.match(ut) || []; e = s[n++]; ) o.hasClass(e) ? o.removeClass(e) : o.addClass(e); else (i === kt || "boolean" === i) && (this.className && mt.set(this, "__className__", this.className), 
        this.className = this.className || !1 === t ? "" : mt.get(this, "__className__") || "");
      });
    },
    hasClass: function(t) {
      for (var e = " " + t + " ", i = 0, n = this.length; n > i; i++) if (1 === this[i].nodeType && (" " + this[i].className + " ").replace(oe, " ").indexOf(e) >= 0) return !0;
      return !1;
    }
  });
  var se = /\r/g;
  J.fn.extend({
    val: function(t) {
      var e, i, n, o = this[0];
      return arguments.length ? (n = J.isFunction(t), this.each(function(i) {
        var o;
        1 === this.nodeType && (null == (o = n ? t.call(this, i, J(this).val()) : t) ? o = "" : "number" == typeof o ? o += "" : J.isArray(o) && (o = J.map(o, function(t) {
          return null == t ? "" : t + "";
        })), (e = J.valHooks[this.type] || J.valHooks[this.nodeName.toLowerCase()]) && "set" in e && void 0 !== e.set(this, o, "value") || (this.value = o));
      })) : o ? (e = J.valHooks[o.type] || J.valHooks[o.nodeName.toLowerCase()]) && "get" in e && void 0 !== (i = e.get(o, "value")) ? i : "string" == typeof (i = o.value) ? i.replace(se, "") : null == i ? "" : i : void 0;
    }
  }), J.extend({
    valHooks: {
      option: {
        get: function(t) {
          var e = J.find.attr(t, "value");
          return null != e ? e : J.trim(J.text(t));
        }
      },
      select: {
        get: function(t) {
          for (var e, i, n = t.options, o = t.selectedIndex, s = "select-one" === t.type || 0 > o, a = s ? null : [], r = s ? o + 1 : n.length, l = 0 > o ? r : s ? o : 0; r > l; l++) if (!(!(i = n[l]).selected && l !== o || (Y.optDisabled ? i.disabled : null !== i.getAttribute("disabled")) || i.parentNode.disabled && J.nodeName(i.parentNode, "optgroup"))) {
            if (e = J(i).val(), s) return e;
            a.push(e);
          }
          return a;
        },
        set: function(t, e) {
          for (var i, n, o = t.options, s = J.makeArray(e), a = o.length; a--; ) n = o[a], 
          (n.selected = J.inArray(n.value, s) >= 0) && (i = !0);
          return i || (t.selectedIndex = -1), s;
        }
      }
    }
  }), J.each([ "radio", "checkbox" ], function() {
    J.valHooks[this] = {
      set: function(t, e) {
        return J.isArray(e) ? t.checked = J.inArray(J(t).val(), e) >= 0 : void 0;
      }
    }, Y.checkOn || (J.valHooks[this].get = function(t) {
      return null === t.getAttribute("value") ? "on" : t.value;
    });
  }), J.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function(t, e) {
    J.fn[e] = function(t, i) {
      return arguments.length > 0 ? this.on(e, null, t, i) : this.trigger(e);
    };
  }), J.fn.extend({
    hover: function(t, e) {
      return this.mouseenter(t).mouseleave(e || t);
    },
    bind: function(t, e, i) {
      return this.on(t, null, e, i);
    },
    unbind: function(t, e) {
      return this.off(t, null, e);
    },
    delegate: function(t, e, i, n) {
      return this.on(e, t, i, n);
    },
    undelegate: function(t, e, i) {
      return 1 === arguments.length ? this.off(t, "**") : this.off(e, t || "**", i);
    }
  });
  var ae = J.now(), re = /\?/;
  J.parseJSON = function(t) {
    return JSON.parse(t + "");
  }, J.parseXML = function(t) {
    var e, i;
    if (!t || "string" != typeof t) return null;
    try {
      i = new DOMParser(), e = i.parseFromString(t, "text/xml");
    } catch (t) {
      e = void 0;
    }
    return (!e || e.getElementsByTagName("parsererror").length) && J.error("Invalid XML: " + t), 
    e;
  };
  var le, ce, de = /#.*$/, ue = /([?&])_=[^&]*/, he = /^(.*?):[ \t]*([^\r\n]*)$/gm, pe = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/, fe = /^(?:GET|HEAD)$/, me = /^\/\//, ge = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/, ve = {}, be = {}, Ce = "*/".concat("*");
  try {
    ce = location.href;
  } catch (t) {
    (ce = G.createElement("a")).href = "", ce = ce.href;
  }
  le = ge.exec(ce.toLowerCase()) || [], J.extend({
    active: 0,
    lastModified: {},
    etag: {},
    ajaxSettings: {
      url: ce,
      type: "GET",
      isLocal: pe.test(le[1]),
      global: !0,
      processData: !0,
      async: !0,
      contentType: "application/x-www-form-urlencoded; charset=UTF-8",
      accepts: {
        "*": Ce,
        text: "text/plain",
        html: "text/html",
        xml: "application/xml, text/xml",
        json: "application/json, text/javascript"
      },
      contents: {
        xml: /xml/,
        html: /html/,
        json: /json/
      },
      responseFields: {
        xml: "responseXML",
        text: "responseText",
        json: "responseJSON"
      },
      converters: {
        "* text": String,
        "text html": !0,
        "text json": J.parseJSON,
        "text xml": J.parseXML
      },
      flatOptions: {
        url: !0,
        context: !0
      }
    },
    ajaxSetup: function(t, e) {
      return e ? M(M(t, J.ajaxSettings), e) : M(J.ajaxSettings, t);
    },
    ajaxPrefilter: N(ve),
    ajaxTransport: N(be),
    ajax: function(t, e) {
      function i(t, e, i, a) {
        var l, d, v, b, y, x = e;
        2 !== C && (C = 2, r && clearTimeout(r), n = void 0, s = a || "", w.readyState = t > 0 ? 4 : 0, 
        l = t >= 200 && 300 > t || 304 === t, i && (b = F(u, w, i)), b = q(u, b, w, l), 
        l ? (u.ifModified && ((y = w.getResponseHeader("Last-Modified")) && (J.lastModified[o] = y), 
        (y = w.getResponseHeader("etag")) && (J.etag[o] = y)), 204 === t || "HEAD" === u.type ? x = "nocontent" : 304 === t ? x = "notmodified" : (x = b.state, 
        d = b.data, v = b.error, l = !v)) : (v = x, (t || !x) && (x = "error", 0 > t && (t = 0))), 
        w.status = t, w.statusText = (e || x) + "", l ? f.resolveWith(h, [ d, x, w ]) : f.rejectWith(h, [ w, x, v ]), 
        w.statusCode(g), g = void 0, c && p.trigger(l ? "ajaxSuccess" : "ajaxError", [ w, u, l ? d : v ]), 
        m.fireWith(h, [ w, x ]), c && (p.trigger("ajaxComplete", [ w, u ]), --J.active || J.event.trigger("ajaxStop")));
      }
      "object" == typeof t && (e = t, t = void 0), e = e || {};
      var n, o, s, a, r, l, c, d, u = J.ajaxSetup({}, e), h = u.context || u, p = u.context && (h.nodeType || h.jquery) ? J(h) : J.event, f = J.Deferred(), m = J.Callbacks("once memory"), g = u.statusCode || {}, v = {}, b = {}, C = 0, y = "canceled", w = {
        readyState: 0,
        getResponseHeader: function(t) {
          var e;
          if (2 === C) {
            if (!a) for (a = {}; e = he.exec(s); ) a[e[1].toLowerCase()] = e[2];
            e = a[t.toLowerCase()];
          }
          return null == e ? null : e;
        },
        getAllResponseHeaders: function() {
          return 2 === C ? s : null;
        },
        setRequestHeader: function(t, e) {
          var i = t.toLowerCase();
          return C || (t = b[i] = b[i] || t, v[t] = e), this;
        },
        overrideMimeType: function(t) {
          return C || (u.mimeType = t), this;
        },
        statusCode: function(t) {
          var e;
          if (t) if (2 > C) for (e in t) g[e] = [ g[e], t[e] ]; else w.always(t[w.status]);
          return this;
        },
        abort: function(t) {
          var e = t || y;
          return n && n.abort(e), i(0, e), this;
        }
      };
      if (f.promise(w).complete = m.add, w.success = w.done, w.error = w.fail, u.url = ((t || u.url || ce) + "").replace(de, "").replace(me, le[1] + "//"), 
      u.type = e.method || e.type || u.method || u.type, u.dataTypes = J.trim(u.dataType || "*").toLowerCase().match(ut) || [ "" ], 
      null == u.crossDomain && (l = ge.exec(u.url.toLowerCase()), u.crossDomain = !(!l || l[1] === le[1] && l[2] === le[2] && (l[3] || ("http:" === l[1] ? "80" : "443")) === (le[3] || ("http:" === le[1] ? "80" : "443")))), 
      u.data && u.processData && "string" != typeof u.data && (u.data = J.param(u.data, u.traditional)), 
      O(ve, u, e, w), 2 === C) return w;
      (c = u.global) && 0 == J.active++ && J.event.trigger("ajaxStart"), u.type = u.type.toUpperCase(), 
      u.hasContent = !fe.test(u.type), o = u.url, u.hasContent || (u.data && (o = u.url += (re.test(o) ? "&" : "?") + u.data, 
      delete u.data), !1 === u.cache && (u.url = ue.test(o) ? o.replace(ue, "$1_=" + ae++) : o + (re.test(o) ? "&" : "?") + "_=" + ae++)), 
      u.ifModified && (J.lastModified[o] && w.setRequestHeader("If-Modified-Since", J.lastModified[o]), 
      J.etag[o] && w.setRequestHeader("If-None-Match", J.etag[o])), (u.data && u.hasContent && !1 !== u.contentType || e.contentType) && w.setRequestHeader("Content-Type", u.contentType), 
      w.setRequestHeader("Accept", u.dataTypes[0] && u.accepts[u.dataTypes[0]] ? u.accepts[u.dataTypes[0]] + ("*" !== u.dataTypes[0] ? ", " + Ce + "; q=0.01" : "") : u.accepts["*"]);
      for (d in u.headers) w.setRequestHeader(d, u.headers[d]);
      if (u.beforeSend && (!1 === u.beforeSend.call(h, w, u) || 2 === C)) return w.abort();
      y = "abort";
      for (d in {
        success: 1,
        error: 1,
        complete: 1
      }) w[d](u[d]);
      if (n = O(be, u, e, w)) {
        w.readyState = 1, c && p.trigger("ajaxSend", [ w, u ]), u.async && u.timeout > 0 && (r = setTimeout(function() {
          w.abort("timeout");
        }, u.timeout));
        try {
          C = 1, n.send(v, i);
        } catch (t) {
          if (!(2 > C)) throw t;
          i(-1, t);
        }
      } else i(-1, "No Transport");
      return w;
    },
    getJSON: function(t, e, i) {
      return J.get(t, e, i, "json");
    },
    getScript: function(t, e) {
      return J.get(t, void 0, e, "script");
    }
  }), J.each([ "get", "post" ], function(t, e) {
    J[e] = function(t, i, n, o) {
      return J.isFunction(i) && (o = o || n, n = i, i = void 0), J.ajax({
        url: t,
        type: e,
        dataType: o,
        data: i,
        success: n
      });
    };
  }), J.each([ "ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend" ], function(t, e) {
    J.fn[e] = function(t) {
      return this.on(e, t);
    };
  }), J._evalUrl = function(t) {
    return J.ajax({
      url: t,
      type: "GET",
      dataType: "script",
      async: !1,
      global: !1,
      throws: !0
    });
  }, J.fn.extend({
    wrapAll: function(t) {
      var e;
      return J.isFunction(t) ? this.each(function(e) {
        J(this).wrapAll(t.call(this, e));
      }) : (this[0] && (e = J(t, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && e.insertBefore(this[0]), 
      e.map(function() {
        for (var t = this; t.firstElementChild; ) t = t.firstElementChild;
        return t;
      }).append(this)), this);
    },
    wrapInner: function(t) {
      return this.each(J.isFunction(t) ? function(e) {
        J(this).wrapInner(t.call(this, e));
      } : function() {
        var e = J(this), i = e.contents();
        i.length ? i.wrapAll(t) : e.append(t);
      });
    },
    wrap: function(t) {
      var e = J.isFunction(t);
      return this.each(function(i) {
        J(this).wrapAll(e ? t.call(this, i) : t);
      });
    },
    unwrap: function() {
      return this.parent().each(function() {
        J.nodeName(this, "body") || J(this).replaceWith(this.childNodes);
      }).end();
    }
  }), J.expr.filters.hidden = function(t) {
    return t.offsetWidth <= 0 && t.offsetHeight <= 0;
  }, J.expr.filters.visible = function(t) {
    return !J.expr.filters.hidden(t);
  };
  var ye = /%20/g, we = /\[\]$/, xe = /\r?\n/g, ke = /^(?:submit|button|image|reset|file)$/i, _e = /^(?:input|select|textarea|keygen)/i;
  J.param = function(t, e) {
    var i, n = [], o = function(t, e) {
      e = J.isFunction(e) ? e() : null == e ? "" : e, n[n.length] = encodeURIComponent(t) + "=" + encodeURIComponent(e);
    };
    if (void 0 === e && (e = J.ajaxSettings && J.ajaxSettings.traditional), J.isArray(t) || t.jquery && !J.isPlainObject(t)) J.each(t, function() {
      o(this.name, this.value);
    }); else for (i in t) H(i, t[i], e, o);
    return n.join("&").replace(ye, "+");
  }, J.fn.extend({
    serialize: function() {
      return J.param(this.serializeArray());
    },
    serializeArray: function() {
      return this.map(function() {
        var t = J.prop(this, "elements");
        return t ? J.makeArray(t) : this;
      }).filter(function() {
        var t = this.type;
        return this.name && !J(this).is(":disabled") && _e.test(this.nodeName) && !ke.test(t) && (this.checked || !xt.test(t));
      }).map(function(t, e) {
        var i = J(this).val();
        return null == i ? null : J.isArray(i) ? J.map(i, function(t) {
          return {
            name: e.name,
            value: t.replace(xe, "\r\n")
          };
        }) : {
          name: e.name,
          value: i.replace(xe, "\r\n")
        };
      }).get();
    }
  }), J.ajaxSettings.xhr = function() {
    try {
      return new XMLHttpRequest();
    } catch (t) {}
  };
  var $e = 0, Ae = {}, Se = {
    0: 200,
    1223: 204
  }, Te = J.ajaxSettings.xhr();
  t.ActiveXObject && J(t).on("unload", function() {
    for (var t in Ae) Ae[t]();
  }), Y.cors = !!Te && "withCredentials" in Te, Y.ajax = Te = !!Te, J.ajaxTransport(function(t) {
    var e;
    return Y.cors || Te && !t.crossDomain ? {
      send: function(i, n) {
        var o, s = t.xhr(), a = ++$e;
        if (s.open(t.type, t.url, t.async, t.username, t.password), t.xhrFields) for (o in t.xhrFields) s[o] = t.xhrFields[o];
        t.mimeType && s.overrideMimeType && s.overrideMimeType(t.mimeType), t.crossDomain || i["X-Requested-With"] || (i["X-Requested-With"] = "XMLHttpRequest");
        for (o in i) s.setRequestHeader(o, i[o]);
        e = function(t) {
          return function() {
            e && (delete Ae[a], e = s.onload = s.onerror = null, "abort" === t ? s.abort() : "error" === t ? n(s.status, s.statusText) : n(Se[s.status] || s.status, s.statusText, "string" == typeof s.responseText ? {
              text: s.responseText
            } : void 0, s.getAllResponseHeaders()));
          };
        }, s.onload = e(), s.onerror = e("error"), e = Ae[a] = e("abort");
        try {
          s.send(t.hasContent && t.data || null);
        } catch (t) {
          if (e) throw t;
        }
      },
      abort: function() {
        e && e();
      }
    } : void 0;
  }), J.ajaxSetup({
    accepts: {
      script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
    },
    contents: {
      script: /(?:java|ecma)script/
    },
    converters: {
      "text script": function(t) {
        return J.globalEval(t), t;
      }
    }
  }), J.ajaxPrefilter("script", function(t) {
    void 0 === t.cache && (t.cache = !1), t.crossDomain && (t.type = "GET");
  }), J.ajaxTransport("script", function(t) {
    if (t.crossDomain) {
      var e, i;
      return {
        send: function(n, o) {
          e = J("<script>").prop({
            async: !0,
            charset: t.scriptCharset,
            src: t.url
          }).on("load error", i = function(t) {
            e.remove(), i = null, t && o("error" === t.type ? 404 : 200, t.type);
          }), G.head.appendChild(e[0]);
        },
        abort: function() {
          i && i();
        }
      };
    }
  });
  var De = [], Ie = /(=)\?(?=&|$)|\?\?/;
  J.ajaxSetup({
    jsonp: "callback",
    jsonpCallback: function() {
      var t = De.pop() || J.expando + "_" + ae++;
      return this[t] = !0, t;
    }
  }), J.ajaxPrefilter("json jsonp", function(e, i, n) {
    var o, s, a, r = !1 !== e.jsonp && (Ie.test(e.url) ? "url" : "string" == typeof e.data && !(e.contentType || "").indexOf("application/x-www-form-urlencoded") && Ie.test(e.data) && "data");
    return r || "jsonp" === e.dataTypes[0] ? (o = e.jsonpCallback = J.isFunction(e.jsonpCallback) ? e.jsonpCallback() : e.jsonpCallback, 
    r ? e[r] = e[r].replace(Ie, "$1" + o) : !1 !== e.jsonp && (e.url += (re.test(e.url) ? "&" : "?") + e.jsonp + "=" + o), 
    e.converters["script json"] = function() {
      return a || J.error(o + " was not called"), a[0];
    }, e.dataTypes[0] = "json", s = t[o], t[o] = function() {
      a = arguments;
    }, n.always(function() {
      t[o] = s, e[o] && (e.jsonpCallback = i.jsonpCallback, De.push(o)), a && J.isFunction(s) && s(a[0]), 
      a = s = void 0;
    }), "script") : void 0;
  }), J.parseHTML = function(t, e, i) {
    if (!t || "string" != typeof t) return null;
    "boolean" == typeof e && (i = e, e = !1), e = e || G;
    var n = st.exec(t), o = !i && [];
    return n ? [ e.createElement(n[1]) ] : (n = J.buildFragment([ t ], e, o), o && o.length && J(o).remove(), 
    J.merge([], n.childNodes));
  };
  var je = J.fn.load;
  J.fn.load = function(t, e, i) {
    if ("string" != typeof t && je) return je.apply(this, arguments);
    var n, o, s, a = this, r = t.indexOf(" ");
    return r >= 0 && (n = J.trim(t.slice(r)), t = t.slice(0, r)), J.isFunction(e) ? (i = e, 
    e = void 0) : e && "object" == typeof e && (o = "POST"), a.length > 0 && J.ajax({
      url: t,
      type: o,
      dataType: "html",
      data: e
    }).done(function(t) {
      s = arguments, a.html(n ? J("<div>").append(J.parseHTML(t)).find(n) : t);
    }).complete(i && function(t, e) {
      a.each(i, s || [ t.responseText, e, t ]);
    }), this;
  }, J.expr.filters.animated = function(t) {
    return J.grep(J.timers, function(e) {
      return t === e.elem;
    }).length;
  };
  var Pe = t.document.documentElement;
  J.offset = {
    setOffset: function(t, e, i) {
      var n, o, s, a, r, l, c = J.css(t, "position"), d = J(t), u = {};
      "static" === c && (t.style.position = "relative"), r = d.offset(), s = J.css(t, "top"), 
      l = J.css(t, "left"), ("absolute" === c || "fixed" === c) && (s + l).indexOf("auto") > -1 ? (n = d.position(), 
      a = n.top, o = n.left) : (a = parseFloat(s) || 0, o = parseFloat(l) || 0), J.isFunction(e) && (e = e.call(t, i, r)), 
      null != e.top && (u.top = e.top - r.top + a), null != e.left && (u.left = e.left - r.left + o), 
      "using" in e ? e.using.call(t, u) : d.css(u);
    }
  }, J.fn.extend({
    offset: function(t) {
      if (arguments.length) return void 0 === t ? this : this.each(function(e) {
        J.offset.setOffset(this, t, e);
      });
      var e, i, n = this[0], o = {
        top: 0,
        left: 0
      }, s = n && n.ownerDocument;
      return s ? (e = s.documentElement, J.contains(e, n) ? (typeof n.getBoundingClientRect !== kt && (o = n.getBoundingClientRect()), 
      i = L(s), {
        top: o.top + i.pageYOffset - e.clientTop,
        left: o.left + i.pageXOffset - e.clientLeft
      }) : o) : void 0;
    },
    position: function() {
      if (this[0]) {
        var t, e, i = this[0], n = {
          top: 0,
          left: 0
        };
        return "fixed" === J.css(i, "position") ? e = i.getBoundingClientRect() : (t = this.offsetParent(), 
        e = this.offset(), J.nodeName(t[0], "html") || (n = t.offset()), n.top += J.css(t[0], "borderTopWidth", !0), 
        n.left += J.css(t[0], "borderLeftWidth", !0)), {
          top: e.top - n.top - J.css(i, "marginTop", !0),
          left: e.left - n.left - J.css(i, "marginLeft", !0)
        };
      }
    },
    offsetParent: function() {
      return this.map(function() {
        for (var t = this.offsetParent || Pe; t && !J.nodeName(t, "html") && "static" === J.css(t, "position"); ) t = t.offsetParent;
        return t || Pe;
      });
    }
  }), J.each({
    scrollLeft: "pageXOffset",
    scrollTop: "pageYOffset"
  }, function(e, i) {
    var n = "pageYOffset" === i;
    J.fn[e] = function(o) {
      return ft(this, function(e, o, s) {
        var a = L(e);
        return void 0 === s ? a ? a[i] : e[o] : void (a ? a.scrollTo(n ? t.pageXOffset : s, n ? s : t.pageYOffset) : e[o] = s);
      }, e, o, arguments.length, null);
    };
  }), J.each([ "top", "left" ], function(t, e) {
    J.cssHooks[e] = x(Y.pixelPosition, function(t, i) {
      return i ? (i = w(t, e), Lt.test(i) ? J(t).position()[e] + "px" : i) : void 0;
    });
  }), J.each({
    Height: "height",
    Width: "width"
  }, function(t, e) {
    J.each({
      padding: "inner" + t,
      content: e,
      "": "outer" + t
    }, function(i, n) {
      J.fn[n] = function(n, o) {
        var s = arguments.length && (i || "boolean" != typeof n), a = i || (!0 === n || !0 === o ? "margin" : "border");
        return ft(this, function(e, i, n) {
          var o;
          return J.isWindow(e) ? e.document.documentElement["client" + t] : 9 === e.nodeType ? (o = e.documentElement, 
          Math.max(e.body["scroll" + t], o["scroll" + t], e.body["offset" + t], o["offset" + t], o["client" + t])) : void 0 === n ? J.css(e, i, a) : J.style(e, i, n, a);
        }, e, s ? n : void 0, s, null);
      };
    });
  }), J.fn.size = function() {
    return this.length;
  }, J.fn.andSelf = J.fn.addBack, "function" == typeof define && define.amd && define("jquery", [], function() {
    return J;
  });
  var Ee = t.jQuery, Ne = t.$;
  return J.noConflict = function(e) {
    return t.$ === J && (t.$ = Ne), e && t.jQuery === J && (t.jQuery = Ee), J;
  }, typeof e === kt && (t.jQuery = t.$ = J), J;
}), function(t) {
  "function" == typeof define && define.amd ? define([ "jquery" ], t) : t(jQuery);
}(function(t) {
  function e(e, n) {
    var o, s, a, r = e.nodeName.toLowerCase();
    return "area" === r ? (o = e.parentNode, s = o.name, !(!e.href || !s || "map" !== o.nodeName.toLowerCase()) && (!!(a = t("img[usemap='#" + s + "']")[0]) && i(a))) : (/input|select|textarea|button|object/.test(r) ? !e.disabled : "a" === r ? e.href || n : n) && i(e);
  }
  function i(e) {
    return t.expr.filters.visible(e) && !t(e).parents().addBack().filter(function() {
      return "hidden" === t.css(this, "visibility");
    }).length;
  }
  function n(t) {
    for (var e, i; t.length && t[0] !== document; ) {
      if (("absolute" === (e = t.css("position")) || "relative" === e || "fixed" === e) && (i = parseInt(t.css("zIndex"), 10), 
      !isNaN(i) && 0 !== i)) return i;
      t = t.parent();
    }
    return 0;
  }
  function o() {
    this._curInst = null, this._keyEvent = !1, this._disabledInputs = [], this._datepickerShowing = !1, 
    this._inDialog = !1, this._mainDivId = "ui-datepicker-div", this._inlineClass = "ui-datepicker-inline", 
    this._appendClass = "ui-datepicker-append", this._triggerClass = "ui-datepicker-trigger", 
    this._dialogClass = "ui-datepicker-dialog", this._disableClass = "ui-datepicker-disabled", 
    this._unselectableClass = "ui-datepicker-unselectable", this._currentClass = "ui-datepicker-current-day", 
    this._dayOverClass = "ui-datepicker-days-cell-over", this.regional = [], this.regional[""] = {
      closeText: "Done",
      prevText: "Prev",
      nextText: "Next",
      currentText: "Today",
      monthNames: [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ],
      monthNamesShort: [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ],
      dayNames: [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" ],
      dayNamesShort: [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ],
      dayNamesMin: [ "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa" ],
      weekHeader: "Wk",
      dateFormat: "mm/dd/yy",
      firstDay: 0,
      isRTL: !1,
      showMonthAfterYear: !1,
      yearSuffix: ""
    }, this._defaults = {
      showOn: "focus",
      showAnim: "fadeIn",
      showOptions: {},
      defaultDate: null,
      appendText: "",
      buttonText: "...",
      buttonImage: "",
      buttonImageOnly: !1,
      hideIfNoPrevNext: !1,
      navigationAsDateFormat: !1,
      gotoCurrent: !1,
      changeMonth: !1,
      changeYear: !1,
      yearRange: "c-10:c+10",
      showOtherMonths: !1,
      selectOtherMonths: !1,
      showWeek: !1,
      calculateWeek: this.iso8601Week,
      shortYearCutoff: "+10",
      minDate: null,
      maxDate: null,
      duration: "fast",
      beforeShowDay: null,
      beforeShow: null,
      onSelect: null,
      onChangeMonthYear: null,
      onClose: null,
      numberOfMonths: 1,
      showCurrentAtPos: 0,
      stepMonths: 1,
      stepBigMonths: 12,
      altField: "",
      altFormat: "",
      constrainInput: !0,
      showButtonPanel: !1,
      autoSize: !1,
      disabled: !1
    }, t.extend(this._defaults, this.regional[""]), this.regional.en = t.extend(!0, {}, this.regional[""]), 
    this.regional["en-US"] = t.extend(!0, {}, this.regional.en), this.dpDiv = s(t("<div id='" + this._mainDivId + "' class='ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>"));
  }
  function s(e) {
    var i = "button, .ui-datepicker-prev, .ui-datepicker-next, .ui-datepicker-calendar td a";
    return e.delegate(i, "mouseout", function() {
      t(this).removeClass("ui-state-hover"), -1 !== this.className.indexOf("ui-datepicker-prev") && t(this).removeClass("ui-datepicker-prev-hover"), 
      -1 !== this.className.indexOf("ui-datepicker-next") && t(this).removeClass("ui-datepicker-next-hover");
    }).delegate(i, "mouseover", a);
  }
  function a() {
    t.datepicker._isDisabledDatepicker(v.inline ? v.dpDiv.parent()[0] : v.input[0]) || (t(this).parents(".ui-datepicker-calendar").find("a").removeClass("ui-state-hover"), 
    t(this).addClass("ui-state-hover"), -1 !== this.className.indexOf("ui-datepicker-prev") && t(this).addClass("ui-datepicker-prev-hover"), 
    -1 !== this.className.indexOf("ui-datepicker-next") && t(this).addClass("ui-datepicker-next-hover"));
  }
  function r(e, i) {
    t.extend(e, i);
    for (var n in i) null == i[n] && (e[n] = i[n]);
    return e;
  }
  function l(t) {
    return function() {
      var e = this.element.val();
      t.apply(this, arguments), this._refresh(), e !== this.element.val() && this._trigger("change");
    };
  }
  t.ui = t.ui || {}, t.extend(t.ui, {
    version: "1.11.2",
    keyCode: {
      BACKSPACE: 8,
      COMMA: 188,
      DELETE: 46,
      DOWN: 40,
      END: 35,
      ENTER: 13,
      ESCAPE: 27,
      HOME: 36,
      LEFT: 37,
      PAGE_DOWN: 34,
      PAGE_UP: 33,
      PERIOD: 190,
      RIGHT: 39,
      SPACE: 32,
      TAB: 9,
      UP: 38
    }
  }), t.fn.extend({
    scrollParent: function(e) {
      var i = this.css("position"), n = "absolute" === i, o = e ? /(auto|scroll|hidden)/ : /(auto|scroll)/, s = this.parents().filter(function() {
        var e = t(this);
        return (!n || "static" !== e.css("position")) && o.test(e.css("overflow") + e.css("overflow-y") + e.css("overflow-x"));
      }).eq(0);
      return "fixed" !== i && s.length ? s : t(this[0].ownerDocument || document);
    },
    uniqueId: function() {
      var t = 0;
      return function() {
        return this.each(function() {
          this.id || (this.id = "ui-id-" + ++t);
        });
      };
    }(),
    removeUniqueId: function() {
      return this.each(function() {
        /^ui-id-\d+$/.test(this.id) && t(this).removeAttr("id");
      });
    }
  }), t.extend(t.expr[":"], {
    data: t.expr.createPseudo ? t.expr.createPseudo(function(e) {
      return function(i) {
        return !!t.data(i, e);
      };
    }) : function(e, i, n) {
      return !!t.data(e, n[3]);
    },
    focusable: function(i) {
      return e(i, !isNaN(t.attr(i, "tabindex")));
    },
    tabbable: function(i) {
      var n = t.attr(i, "tabindex"), o = isNaN(n);
      return (o || n >= 0) && e(i, !o);
    }
  }), t("<a>").outerWidth(1).jquery || t.each([ "Width", "Height" ], function(e, i) {
    function n(e, i, n, s) {
      return t.each(o, function() {
        i -= parseFloat(t.css(e, "padding" + this)) || 0, n && (i -= parseFloat(t.css(e, "border" + this + "Width")) || 0), 
        s && (i -= parseFloat(t.css(e, "margin" + this)) || 0);
      }), i;
    }
    var o = "Width" === i ? [ "Left", "Right" ] : [ "Top", "Bottom" ], s = i.toLowerCase(), a = {
      innerWidth: t.fn.innerWidth,
      innerHeight: t.fn.innerHeight,
      outerWidth: t.fn.outerWidth,
      outerHeight: t.fn.outerHeight
    };
    t.fn["inner" + i] = function(e) {
      return void 0 === e ? a["inner" + i].call(this) : this.each(function() {
        t(this).css(s, n(this, e) + "px");
      });
    }, t.fn["outer" + i] = function(e, o) {
      return "number" != typeof e ? a["outer" + i].call(this, e) : this.each(function() {
        t(this).css(s, n(this, e, !0, o) + "px");
      });
    };
  }), t.fn.addBack || (t.fn.addBack = function(t) {
    return this.add(null == t ? this.prevObject : this.prevObject.filter(t));
  }), t("<a>").data("a-b", "a").removeData("a-b").data("a-b") && (t.fn.removeData = function(e) {
    return function(i) {
      return arguments.length ? e.call(this, t.camelCase(i)) : e.call(this);
    };
  }(t.fn.removeData)), t.ui.ie = !!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase()), 
  t.fn.extend({
    focus: function(e) {
      return function(i, n) {
        return "number" == typeof i ? this.each(function() {
          var e = this;
          setTimeout(function() {
            t(e).focus(), n && n.call(e);
          }, i);
        }) : e.apply(this, arguments);
      };
    }(t.fn.focus),
    disableSelection: function() {
      var t = "onselectstart" in document.createElement("div") ? "selectstart" : "mousedown";
      return function() {
        return this.bind(t + ".ui-disableSelection", function(t) {
          t.preventDefault();
        });
      };
    }(),
    enableSelection: function() {
      return this.unbind(".ui-disableSelection");
    },
    zIndex: function(e) {
      if (void 0 !== e) return this.css("zIndex", e);
      if (this.length) for (var i, n, o = t(this[0]); o.length && o[0] !== document; ) {
        if (("absolute" === (i = o.css("position")) || "relative" === i || "fixed" === i) && (n = parseInt(o.css("zIndex"), 10), 
        !isNaN(n) && 0 !== n)) return n;
        o = o.parent();
      }
      return 0;
    }
  }), t.ui.plugin = {
    add: function(e, i, n) {
      var o, s = t.ui[e].prototype;
      for (o in n) s.plugins[o] = s.plugins[o] || [], s.plugins[o].push([ i, n[o] ]);
    },
    call: function(t, e, i, n) {
      var o, s = t.plugins[e];
      if (s && (n || t.element[0].parentNode && 11 !== t.element[0].parentNode.nodeType)) for (o = 0; s.length > o; o++) t.options[s[o][0]] && s[o][1].apply(t.element, i);
    }
  };
  var c = 0, d = Array.prototype.slice;
  t.cleanData = function(e) {
    return function(i) {
      var n, o, s;
      for (s = 0; null != (o = i[s]); s++) try {
        (n = t._data(o, "events")) && n.remove && t(o).triggerHandler("remove");
      } catch (t) {}
      e(i);
    };
  }(t.cleanData), t.widget = function(e, i, n) {
    var o, s, a, r, l = {}, c = e.split(".")[0];
    return e = e.split(".")[1], o = c + "-" + e, n || (n = i, i = t.Widget), t.expr[":"][o.toLowerCase()] = function(e) {
      return !!t.data(e, o);
    }, t[c] = t[c] || {}, s = t[c][e], a = t[c][e] = function(t, e) {
      return this._createWidget ? void (arguments.length && this._createWidget(t, e)) : new a(t, e);
    }, t.extend(a, s, {
      version: n.version,
      _proto: t.extend({}, n),
      _childConstructors: []
    }), r = new i(), r.options = t.widget.extend({}, r.options), t.each(n, function(e, n) {
      return t.isFunction(n) ? void (l[e] = function() {
        var t = function() {
          return i.prototype[e].apply(this, arguments);
        }, o = function(t) {
          return i.prototype[e].apply(this, t);
        };
        return function() {
          var e, i = this._super, s = this._superApply;
          return this._super = t, this._superApply = o, e = n.apply(this, arguments), this._super = i, 
          this._superApply = s, e;
        };
      }()) : void (l[e] = n);
    }), a.prototype = t.widget.extend(r, {
      widgetEventPrefix: s ? r.widgetEventPrefix || e : e
    }, l, {
      constructor: a,
      namespace: c,
      widgetName: e,
      widgetFullName: o
    }), s ? (t.each(s._childConstructors, function(e, i) {
      var n = i.prototype;
      t.widget(n.namespace + "." + n.widgetName, a, i._proto);
    }), delete s._childConstructors) : i._childConstructors.push(a), t.widget.bridge(e, a), 
    a;
  }, t.widget.extend = function(e) {
    for (var i, n, o = d.call(arguments, 1), s = 0, a = o.length; a > s; s++) for (i in o[s]) n = o[s][i], 
    o[s].hasOwnProperty(i) && void 0 !== n && (e[i] = t.isPlainObject(n) ? t.isPlainObject(e[i]) ? t.widget.extend({}, e[i], n) : t.widget.extend({}, n) : n);
    return e;
  }, t.widget.bridge = function(e, i) {
    var n = i.prototype.widgetFullName || e;
    t.fn[e] = function(o) {
      var s = "string" == typeof o, a = d.call(arguments, 1), r = this;
      return o = !s && a.length ? t.widget.extend.apply(null, [ o ].concat(a)) : o, s ? this.each(function() {
        var i, s = t.data(this, n);
        return "instance" === o ? (r = s, !1) : s ? t.isFunction(s[o]) && "_" !== o.charAt(0) ? (i = s[o].apply(s, a)) !== s && void 0 !== i ? (r = i && i.jquery ? r.pushStack(i.get()) : i, 
        !1) : void 0 : t.error("no such method '" + o + "' for " + e + " widget instance") : t.error("cannot call methods on " + e + " prior to initialization; attempted to call method '" + o + "'");
      }) : this.each(function() {
        var e = t.data(this, n);
        e ? (e.option(o || {}), e._init && e._init()) : t.data(this, n, new i(o, this));
      }), r;
    };
  }, t.Widget = function() {}, t.Widget._childConstructors = [], t.Widget.prototype = {
    widgetName: "widget",
    widgetEventPrefix: "",
    defaultElement: "<div>",
    options: {
      disabled: !1,
      create: null
    },
    _createWidget: function(e, i) {
      i = t(i || this.defaultElement || this)[0], this.element = t(i), this.uuid = c++, 
      this.eventNamespace = "." + this.widgetName + this.uuid, this.bindings = t(), this.hoverable = t(), 
      this.focusable = t(), i !== this && (t.data(i, this.widgetFullName, this), this._on(!0, this.element, {
        remove: function(t) {
          t.target === i && this.destroy();
        }
      }), this.document = t(i.style ? i.ownerDocument : i.document || i), this.window = t(this.document[0].defaultView || this.document[0].parentWindow)), 
      this.options = t.widget.extend({}, this.options, this._getCreateOptions(), e), this._create(), 
      this._trigger("create", null, this._getCreateEventData()), this._init();
    },
    _getCreateOptions: t.noop,
    _getCreateEventData: t.noop,
    _create: t.noop,
    _init: t.noop,
    destroy: function() {
      this._destroy(), this.element.unbind(this.eventNamespace).removeData(this.widgetFullName).removeData(t.camelCase(this.widgetFullName)), 
      this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName + "-disabled ui-state-disabled"), 
      this.bindings.unbind(this.eventNamespace), this.hoverable.removeClass("ui-state-hover"), 
      this.focusable.removeClass("ui-state-focus");
    },
    _destroy: t.noop,
    widget: function() {
      return this.element;
    },
    option: function(e, i) {
      var n, o, s, a = e;
      if (0 === arguments.length) return t.widget.extend({}, this.options);
      if ("string" == typeof e) if (a = {}, n = e.split("."), e = n.shift(), n.length) {
        for (o = a[e] = t.widget.extend({}, this.options[e]), s = 0; n.length - 1 > s; s++) o[n[s]] = o[n[s]] || {}, 
        o = o[n[s]];
        if (e = n.pop(), 1 === arguments.length) return void 0 === o[e] ? null : o[e];
        o[e] = i;
      } else {
        if (1 === arguments.length) return void 0 === this.options[e] ? null : this.options[e];
        a[e] = i;
      }
      return this._setOptions(a), this;
    },
    _setOptions: function(t) {
      var e;
      for (e in t) this._setOption(e, t[e]);
      return this;
    },
    _setOption: function(t, e) {
      return this.options[t] = e, "disabled" === t && (this.widget().toggleClass(this.widgetFullName + "-disabled", !!e), 
      e && (this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus"))), 
      this;
    },
    enable: function() {
      return this._setOptions({
        disabled: !1
      });
    },
    disable: function() {
      return this._setOptions({
        disabled: !0
      });
    },
    _on: function(e, i, n) {
      var o, s = this;
      "boolean" != typeof e && (n = i, i = e, e = !1), n ? (i = o = t(i), this.bindings = this.bindings.add(i)) : (n = i, 
      i = this.element, o = this.widget()), t.each(n, function(n, a) {
        function r() {
          return e || !0 !== s.options.disabled && !t(this).hasClass("ui-state-disabled") ? ("string" == typeof a ? s[a] : a).apply(s, arguments) : void 0;
        }
        "string" != typeof a && (r.guid = a.guid = a.guid || r.guid || t.guid++);
        var l = n.match(/^([\w:-]*)\s*(.*)$/), c = l[1] + s.eventNamespace, d = l[2];
        d ? o.delegate(d, c, r) : i.bind(c, r);
      });
    },
    _off: function(e, i) {
      i = (i || "").split(" ").join(this.eventNamespace + " ") + this.eventNamespace, 
      e.unbind(i).undelegate(i), this.bindings = t(this.bindings.not(e).get()), this.focusable = t(this.focusable.not(e).get()), 
      this.hoverable = t(this.hoverable.not(e).get());
    },
    _delay: function(t, e) {
      var i = this;
      return setTimeout(function() {
        return ("string" == typeof t ? i[t] : t).apply(i, arguments);
      }, e || 0);
    },
    _hoverable: function(e) {
      this.hoverable = this.hoverable.add(e), this._on(e, {
        mouseenter: function(e) {
          t(e.currentTarget).addClass("ui-state-hover");
        },
        mouseleave: function(e) {
          t(e.currentTarget).removeClass("ui-state-hover");
        }
      });
    },
    _focusable: function(e) {
      this.focusable = this.focusable.add(e), this._on(e, {
        focusin: function(e) {
          t(e.currentTarget).addClass("ui-state-focus");
        },
        focusout: function(e) {
          t(e.currentTarget).removeClass("ui-state-focus");
        }
      });
    },
    _trigger: function(e, i, n) {
      var o, s, a = this.options[e];
      if (n = n || {}, i = t.Event(i), i.type = (e === this.widgetEventPrefix ? e : this.widgetEventPrefix + e).toLowerCase(), 
      i.target = this.element[0], s = i.originalEvent) for (o in s) o in i || (i[o] = s[o]);
      return this.element.trigger(i, n), !(t.isFunction(a) && !1 === a.apply(this.element[0], [ i ].concat(n)) || i.isDefaultPrevented());
    }
  }, t.each({
    show: "fadeIn",
    hide: "fadeOut"
  }, function(e, i) {
    t.Widget.prototype["_" + e] = function(n, o, s) {
      "string" == typeof o && (o = {
        effect: o
      });
      var a, r = o ? !0 === o || "number" == typeof o ? i : o.effect || i : e;
      "number" == typeof (o = o || {}) && (o = {
        duration: o
      }), a = !t.isEmptyObject(o), o.complete = s, o.delay && n.delay(o.delay), a && t.effects && t.effects.effect[r] ? n[e](o) : r !== e && n[r] ? n[r](o.duration, o.easing, s) : n.queue(function(i) {
        t(this)[e](), s && s.call(n[0]), i();
      });
    };
  }), t.widget;
  var u = !1;
  t(document).mouseup(function() {
    u = !1;
  }), t.widget("ui.mouse", {
    version: "1.11.2",
    options: {
      cancel: "input,textarea,button,select,option",
      distance: 1,
      delay: 0
    },
    _mouseInit: function() {
      var e = this;
      this.element.bind("mousedown." + this.widgetName, function(t) {
        return e._mouseDown(t);
      }).bind("click." + this.widgetName, function(i) {
        return !0 === t.data(i.target, e.widgetName + ".preventClickEvent") ? (t.removeData(i.target, e.widgetName + ".preventClickEvent"), 
        i.stopImmediatePropagation(), !1) : void 0;
      }), this.started = !1;
    },
    _mouseDestroy: function() {
      this.element.unbind("." + this.widgetName), this._mouseMoveDelegate && this.document.unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate);
    },
    _mouseDown: function(e) {
      if (!u) {
        this._mouseMoved = !1, this._mouseStarted && this._mouseUp(e), this._mouseDownEvent = e;
        var i = this, n = 1 === e.which, o = !("string" != typeof this.options.cancel || !e.target.nodeName) && t(e.target).closest(this.options.cancel).length;
        return !(n && !o && this._mouseCapture(e)) || (this.mouseDelayMet = !this.options.delay, 
        this.mouseDelayMet || (this._mouseDelayTimer = setTimeout(function() {
          i.mouseDelayMet = !0;
        }, this.options.delay)), this._mouseDistanceMet(e) && this._mouseDelayMet(e) && (this._mouseStarted = !1 !== this._mouseStart(e), 
        !this._mouseStarted) ? (e.preventDefault(), !0) : (!0 === t.data(e.target, this.widgetName + ".preventClickEvent") && t.removeData(e.target, this.widgetName + ".preventClickEvent"), 
        this._mouseMoveDelegate = function(t) {
          return i._mouseMove(t);
        }, this._mouseUpDelegate = function(t) {
          return i._mouseUp(t);
        }, this.document.bind("mousemove." + this.widgetName, this._mouseMoveDelegate).bind("mouseup." + this.widgetName, this._mouseUpDelegate), 
        e.preventDefault(), u = !0, !0));
      }
    },
    _mouseMove: function(e) {
      if (this._mouseMoved) {
        if (t.ui.ie && (!document.documentMode || 9 > document.documentMode) && !e.button) return this._mouseUp(e);
        if (!e.which) return this._mouseUp(e);
      }
      return (e.which || e.button) && (this._mouseMoved = !0), this._mouseStarted ? (this._mouseDrag(e), 
      e.preventDefault()) : (this._mouseDistanceMet(e) && this._mouseDelayMet(e) && (this._mouseStarted = !1 !== this._mouseStart(this._mouseDownEvent, e), 
      this._mouseStarted ? this._mouseDrag(e) : this._mouseUp(e)), !this._mouseStarted);
    },
    _mouseUp: function(e) {
      return this.document.unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate), 
      this._mouseStarted && (this._mouseStarted = !1, e.target === this._mouseDownEvent.target && t.data(e.target, this.widgetName + ".preventClickEvent", !0), 
      this._mouseStop(e)), u = !1, !1;
    },
    _mouseDistanceMet: function(t) {
      return Math.max(Math.abs(this._mouseDownEvent.pageX - t.pageX), Math.abs(this._mouseDownEvent.pageY - t.pageY)) >= this.options.distance;
    },
    _mouseDelayMet: function() {
      return this.mouseDelayMet;
    },
    _mouseStart: function() {},
    _mouseDrag: function() {},
    _mouseStop: function() {},
    _mouseCapture: function() {
      return !0;
    }
  }), function() {
    function e(t, e, i) {
      return [ parseFloat(t[0]) * (p.test(t[0]) ? e / 100 : 1), parseFloat(t[1]) * (p.test(t[1]) ? i / 100 : 1) ];
    }
    function i(e, i) {
      return parseInt(t.css(e, i), 10) || 0;
    }
    function n(e) {
      var i = e[0];
      return 9 === i.nodeType ? {
        width: e.width(),
        height: e.height(),
        offset: {
          top: 0,
          left: 0
        }
      } : t.isWindow(i) ? {
        width: e.width(),
        height: e.height(),
        offset: {
          top: e.scrollTop(),
          left: e.scrollLeft()
        }
      } : i.preventDefault ? {
        width: 0,
        height: 0,
        offset: {
          top: i.pageY,
          left: i.pageX
        }
      } : {
        width: e.outerWidth(),
        height: e.outerHeight(),
        offset: e.offset()
      };
    }
    t.ui = t.ui || {};
    var o, s, a = Math.max, r = Math.abs, l = Math.round, c = /left|center|right/, d = /top|center|bottom/, u = /[\+\-]\d+(\.[\d]+)?%?/, h = /^\w+/, p = /%$/, f = t.fn.position;
    t.position = {
      scrollbarWidth: function() {
        if (void 0 !== o) return o;
        var e, i, n = t("<div style='display:block;position:absolute;width:50px;height:50px;overflow:hidden;'><div style='height:100px;width:auto;'></div></div>"), s = n.children()[0];
        return t("body").append(n), e = s.offsetWidth, n.css("overflow", "scroll"), i = s.offsetWidth, 
        e === i && (i = n[0].clientWidth), n.remove(), o = e - i;
      },
      getScrollInfo: function(e) {
        var i = e.isWindow || e.isDocument ? "" : e.element.css("overflow-x"), n = e.isWindow || e.isDocument ? "" : e.element.css("overflow-y"), o = "scroll" === i || "auto" === i && e.width < e.element[0].scrollWidth;
        return {
          width: "scroll" === n || "auto" === n && e.height < e.element[0].scrollHeight ? t.position.scrollbarWidth() : 0,
          height: o ? t.position.scrollbarWidth() : 0
        };
      },
      getWithinInfo: function(e) {
        var i = t(e || window), n = t.isWindow(i[0]), o = !!i[0] && 9 === i[0].nodeType;
        return {
          element: i,
          isWindow: n,
          isDocument: o,
          offset: i.offset() || {
            left: 0,
            top: 0
          },
          scrollLeft: i.scrollLeft(),
          scrollTop: i.scrollTop(),
          width: n || o ? i.width() : i.outerWidth(),
          height: n || o ? i.height() : i.outerHeight()
        };
      }
    }, t.fn.position = function(o) {
      if (!o || !o.of) return f.apply(this, arguments);
      o = t.extend({}, o);
      var p, m, g, v, b, C, y = t(o.of), w = t.position.getWithinInfo(o.within), x = t.position.getScrollInfo(w), k = (o.collision || "flip").split(" "), _ = {};
      return C = n(y), y[0].preventDefault && (o.at = "left top"), m = C.width, g = C.height, 
      v = C.offset, b = t.extend({}, v), t.each([ "my", "at" ], function() {
        var t, e, i = (o[this] || "").split(" ");
        1 === i.length && (i = c.test(i[0]) ? i.concat([ "center" ]) : d.test(i[0]) ? [ "center" ].concat(i) : [ "center", "center" ]), 
        i[0] = c.test(i[0]) ? i[0] : "center", i[1] = d.test(i[1]) ? i[1] : "center", t = u.exec(i[0]), 
        e = u.exec(i[1]), _[this] = [ t ? t[0] : 0, e ? e[0] : 0 ], o[this] = [ h.exec(i[0])[0], h.exec(i[1])[0] ];
      }), 1 === k.length && (k[1] = k[0]), "right" === o.at[0] ? b.left += m : "center" === o.at[0] && (b.left += m / 2), 
      "bottom" === o.at[1] ? b.top += g : "center" === o.at[1] && (b.top += g / 2), p = e(_.at, m, g), 
      b.left += p[0], b.top += p[1], this.each(function() {
        var n, c, d = t(this), u = d.outerWidth(), h = d.outerHeight(), f = i(this, "marginLeft"), C = i(this, "marginTop"), $ = u + f + i(this, "marginRight") + x.width, A = h + C + i(this, "marginBottom") + x.height, S = t.extend({}, b), T = e(_.my, d.outerWidth(), d.outerHeight());
        "right" === o.my[0] ? S.left -= u : "center" === o.my[0] && (S.left -= u / 2), "bottom" === o.my[1] ? S.top -= h : "center" === o.my[1] && (S.top -= h / 2), 
        S.left += T[0], S.top += T[1], s || (S.left = l(S.left), S.top = l(S.top)), n = {
          marginLeft: f,
          marginTop: C
        }, t.each([ "left", "top" ], function(e, i) {
          t.ui.position[k[e]] && t.ui.position[k[e]][i](S, {
            targetWidth: m,
            targetHeight: g,
            elemWidth: u,
            elemHeight: h,
            collisionPosition: n,
            collisionWidth: $,
            collisionHeight: A,
            offset: [ p[0] + T[0], p[1] + T[1] ],
            my: o.my,
            at: o.at,
            within: w,
            elem: d
          });
        }), o.using && (c = function(t) {
          var e = v.left - S.left, i = e + m - u, n = v.top - S.top, s = n + g - h, l = {
            target: {
              element: y,
              left: v.left,
              top: v.top,
              width: m,
              height: g
            },
            element: {
              element: d,
              left: S.left,
              top: S.top,
              width: u,
              height: h
            },
            horizontal: 0 > i ? "left" : e > 0 ? "right" : "center",
            vertical: 0 > s ? "top" : n > 0 ? "bottom" : "middle"
          };
          u > m && m > r(e + i) && (l.horizontal = "center"), h > g && g > r(n + s) && (l.vertical = "middle"), 
          l.important = a(r(e), r(i)) > a(r(n), r(s)) ? "horizontal" : "vertical", o.using.call(this, t, l);
        }), d.offset(t.extend(S, {
          using: c
        }));
      });
    }, t.ui.position = {
      fit: {
        left: function(t, e) {
          var i, n = e.within, o = n.isWindow ? n.scrollLeft : n.offset.left, s = n.width, r = t.left - e.collisionPosition.marginLeft, l = o - r, c = r + e.collisionWidth - s - o;
          e.collisionWidth > s ? l > 0 && 0 >= c ? (i = t.left + l + e.collisionWidth - s - o, 
          t.left += l - i) : t.left = c > 0 && 0 >= l ? o : l > c ? o + s - e.collisionWidth : o : l > 0 ? t.left += l : c > 0 ? t.left -= c : t.left = a(t.left - r, t.left);
        },
        top: function(t, e) {
          var i, n = e.within, o = n.isWindow ? n.scrollTop : n.offset.top, s = e.within.height, r = t.top - e.collisionPosition.marginTop, l = o - r, c = r + e.collisionHeight - s - o;
          e.collisionHeight > s ? l > 0 && 0 >= c ? (i = t.top + l + e.collisionHeight - s - o, 
          t.top += l - i) : t.top = c > 0 && 0 >= l ? o : l > c ? o + s - e.collisionHeight : o : l > 0 ? t.top += l : c > 0 ? t.top -= c : t.top = a(t.top - r, t.top);
        }
      },
      flip: {
        left: function(t, e) {
          var i, n, o = e.within, s = o.offset.left + o.scrollLeft, a = o.width, l = o.isWindow ? o.scrollLeft : o.offset.left, c = t.left - e.collisionPosition.marginLeft, d = c - l, u = c + e.collisionWidth - a - l, h = "left" === e.my[0] ? -e.elemWidth : "right" === e.my[0] ? e.elemWidth : 0, p = "left" === e.at[0] ? e.targetWidth : "right" === e.at[0] ? -e.targetWidth : 0, f = -2 * e.offset[0];
          0 > d ? (0 > (i = t.left + h + p + f + e.collisionWidth - a - s) || r(d) > i) && (t.left += h + p + f) : u > 0 && ((n = t.left - e.collisionPosition.marginLeft + h + p + f - l) > 0 || u > r(n)) && (t.left += h + p + f);
        },
        top: function(t, e) {
          var i, n, o = e.within, s = o.offset.top + o.scrollTop, a = o.height, l = o.isWindow ? o.scrollTop : o.offset.top, c = t.top - e.collisionPosition.marginTop, d = c - l, u = c + e.collisionHeight - a - l, h = "top" === e.my[1] ? -e.elemHeight : "bottom" === e.my[1] ? e.elemHeight : 0, p = "top" === e.at[1] ? e.targetHeight : "bottom" === e.at[1] ? -e.targetHeight : 0, f = -2 * e.offset[1];
          0 > d ? (n = t.top + h + p + f + e.collisionHeight - a - s, t.top + h + p + f > d && (0 > n || r(d) > n) && (t.top += h + p + f)) : u > 0 && (i = t.top - e.collisionPosition.marginTop + h + p + f - l, 
          t.top + h + p + f > u && (i > 0 || u > r(i)) && (t.top += h + p + f));
        }
      },
      flipfit: {
        left: function() {
          t.ui.position.flip.left.apply(this, arguments), t.ui.position.fit.left.apply(this, arguments);
        },
        top: function() {
          t.ui.position.flip.top.apply(this, arguments), t.ui.position.fit.top.apply(this, arguments);
        }
      }
    }, function() {
      var e, i, n, o, a, r = document.getElementsByTagName("body")[0], l = document.createElement("div");
      e = document.createElement(r ? "div" : "body"), n = {
        visibility: "hidden",
        width: 0,
        height: 0,
        border: 0,
        margin: 0,
        background: "none"
      }, r && t.extend(n, {
        position: "absolute",
        left: "-1000px",
        top: "-1000px"
      });
      for (a in n) e.style[a] = n[a];
      e.appendChild(l), (i = r || document.documentElement).insertBefore(e, i.firstChild), 
      l.style.cssText = "position: absolute; left: 10.7432222px;", o = t(l).offset().left, 
      s = o > 10 && 11 > o, e.innerHTML = "", i.removeChild(e);
    }();
  }(), t.ui.position, t.widget("ui.draggable", t.ui.mouse, {
    version: "1.11.2",
    widgetEventPrefix: "drag",
    options: {
      addClasses: !0,
      appendTo: "parent",
      axis: !1,
      connectToSortable: !1,
      containment: !1,
      cursor: "auto",
      cursorAt: !1,
      grid: !1,
      handle: !1,
      helper: "original",
      iframeFix: !1,
      opacity: !1,
      refreshPositions: !1,
      revert: !1,
      revertDuration: 500,
      scope: "default",
      scroll: !0,
      scrollSensitivity: 20,
      scrollSpeed: 20,
      snap: !1,
      snapMode: "both",
      snapTolerance: 20,
      stack: !1,
      zIndex: !1,
      drag: null,
      start: null,
      stop: null
    },
    _create: function() {
      "original" === this.options.helper && this._setPositionRelative(), this.options.addClasses && this.element.addClass("ui-draggable"), 
      this.options.disabled && this.element.addClass("ui-draggable-disabled"), this._setHandleClassName(), 
      this._mouseInit();
    },
    _setOption: function(t, e) {
      this._super(t, e), "handle" === t && (this._removeHandleClassName(), this._setHandleClassName());
    },
    _destroy: function() {
      return (this.helper || this.element).is(".ui-draggable-dragging") ? void (this.destroyOnClear = !0) : (this.element.removeClass("ui-draggable ui-draggable-dragging ui-draggable-disabled"), 
      this._removeHandleClassName(), void this._mouseDestroy());
    },
    _mouseCapture: function(e) {
      var i = this.options;
      return this._blurActiveElement(e), !(this.helper || i.disabled || t(e.target).closest(".ui-resizable-handle").length > 0) && (this.handle = this._getHandle(e), 
      !!this.handle && (this._blockFrames(!0 === i.iframeFix ? "iframe" : i.iframeFix), 
      !0));
    },
    _blockFrames: function(e) {
      this.iframeBlocks = this.document.find(e).map(function() {
        var e = t(this);
        return t("<div>").css("position", "absolute").appendTo(e.parent()).outerWidth(e.outerWidth()).outerHeight(e.outerHeight()).offset(e.offset())[0];
      });
    },
    _unblockFrames: function() {
      this.iframeBlocks && (this.iframeBlocks.remove(), delete this.iframeBlocks);
    },
    _blurActiveElement: function(e) {
      var i = this.document[0];
      if (this.handleElement.is(e.target)) try {
        i.activeElement && "body" !== i.activeElement.nodeName.toLowerCase() && t(i.activeElement).blur();
      } catch (t) {}
    },
    _mouseStart: function(e) {
      var i = this.options;
      return this.helper = this._createHelper(e), this.helper.addClass("ui-draggable-dragging"), 
      this._cacheHelperProportions(), t.ui.ddmanager && (t.ui.ddmanager.current = this), 
      this._cacheMargins(), this.cssPosition = this.helper.css("position"), this.scrollParent = this.helper.scrollParent(!0), 
      this.offsetParent = this.helper.offsetParent(), this.hasFixedAncestor = this.helper.parents().filter(function() {
        return "fixed" === t(this).css("position");
      }).length > 0, this.positionAbs = this.element.offset(), this._refreshOffsets(e), 
      this.originalPosition = this.position = this._generatePosition(e, !1), this.originalPageX = e.pageX, 
      this.originalPageY = e.pageY, i.cursorAt && this._adjustOffsetFromHelper(i.cursorAt), 
      this._setContainment(), !1 === this._trigger("start", e) ? (this._clear(), !1) : (this._cacheHelperProportions(), 
      t.ui.ddmanager && !i.dropBehaviour && t.ui.ddmanager.prepareOffsets(this, e), this._normalizeRightBottom(), 
      this._mouseDrag(e, !0), t.ui.ddmanager && t.ui.ddmanager.dragStart(this, e), !0);
    },
    _refreshOffsets: function(t) {
      this.offset = {
        top: this.positionAbs.top - this.margins.top,
        left: this.positionAbs.left - this.margins.left,
        scroll: !1,
        parent: this._getParentOffset(),
        relative: this._getRelativeOffset()
      }, this.offset.click = {
        left: t.pageX - this.offset.left,
        top: t.pageY - this.offset.top
      };
    },
    _mouseDrag: function(e, i) {
      if (this.hasFixedAncestor && (this.offset.parent = this._getParentOffset()), this.position = this._generatePosition(e, !0), 
      this.positionAbs = this._convertPositionTo("absolute"), !i) {
        var n = this._uiHash();
        if (!1 === this._trigger("drag", e, n)) return this._mouseUp({}), !1;
        this.position = n.position;
      }
      return this.helper[0].style.left = this.position.left + "px", this.helper[0].style.top = this.position.top + "px", 
      t.ui.ddmanager && t.ui.ddmanager.drag(this, e), !1;
    },
    _mouseStop: function(e) {
      var i = this, n = !1;
      return t.ui.ddmanager && !this.options.dropBehaviour && (n = t.ui.ddmanager.drop(this, e)), 
      this.dropped && (n = this.dropped, this.dropped = !1), "invalid" === this.options.revert && !n || "valid" === this.options.revert && n || !0 === this.options.revert || t.isFunction(this.options.revert) && this.options.revert.call(this.element, n) ? t(this.helper).animate(this.originalPosition, parseInt(this.options.revertDuration, 10), function() {
        !1 !== i._trigger("stop", e) && i._clear();
      }) : !1 !== this._trigger("stop", e) && this._clear(), !1;
    },
    _mouseUp: function(e) {
      return this._unblockFrames(), t.ui.ddmanager && t.ui.ddmanager.dragStop(this, e), 
      this.handleElement.is(e.target) && this.element.focus(), t.ui.mouse.prototype._mouseUp.call(this, e);
    },
    cancel: function() {
      return this.helper.is(".ui-draggable-dragging") ? this._mouseUp({}) : this._clear(), 
      this;
    },
    _getHandle: function(e) {
      return !this.options.handle || !!t(e.target).closest(this.element.find(this.options.handle)).length;
    },
    _setHandleClassName: function() {
      this.handleElement = this.options.handle ? this.element.find(this.options.handle) : this.element, 
      this.handleElement.addClass("ui-draggable-handle");
    },
    _removeHandleClassName: function() {
      this.handleElement.removeClass("ui-draggable-handle");
    },
    _createHelper: function(e) {
      var i = this.options, n = t.isFunction(i.helper), o = n ? t(i.helper.apply(this.element[0], [ e ])) : "clone" === i.helper ? this.element.clone().removeAttr("id") : this.element;
      return o.parents("body").length || o.appendTo("parent" === i.appendTo ? this.element[0].parentNode : i.appendTo), 
      n && o[0] === this.element[0] && this._setPositionRelative(), o[0] === this.element[0] || /(fixed|absolute)/.test(o.css("position")) || o.css("position", "absolute"), 
      o;
    },
    _setPositionRelative: function() {
      /^(?:r|a|f)/.test(this.element.css("position")) || (this.element[0].style.position = "relative");
    },
    _adjustOffsetFromHelper: function(e) {
      "string" == typeof e && (e = e.split(" ")), t.isArray(e) && (e = {
        left: +e[0],
        top: +e[1] || 0
      }), "left" in e && (this.offset.click.left = e.left + this.margins.left), "right" in e && (this.offset.click.left = this.helperProportions.width - e.right + this.margins.left), 
      "top" in e && (this.offset.click.top = e.top + this.margins.top), "bottom" in e && (this.offset.click.top = this.helperProportions.height - e.bottom + this.margins.top);
    },
    _isRootNode: function(t) {
      return /(html|body)/i.test(t.tagName) || t === this.document[0];
    },
    _getParentOffset: function() {
      var e = this.offsetParent.offset(), i = this.document[0];
      return "absolute" === this.cssPosition && this.scrollParent[0] !== i && t.contains(this.scrollParent[0], this.offsetParent[0]) && (e.left += this.scrollParent.scrollLeft(), 
      e.top += this.scrollParent.scrollTop()), this._isRootNode(this.offsetParent[0]) && (e = {
        top: 0,
        left: 0
      }), {
        top: e.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0),
        left: e.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0)
      };
    },
    _getRelativeOffset: function() {
      if ("relative" !== this.cssPosition) return {
        top: 0,
        left: 0
      };
      var t = this.element.position(), e = this._isRootNode(this.scrollParent[0]);
      return {
        top: t.top - (parseInt(this.helper.css("top"), 10) || 0) + (e ? 0 : this.scrollParent.scrollTop()),
        left: t.left - (parseInt(this.helper.css("left"), 10) || 0) + (e ? 0 : this.scrollParent.scrollLeft())
      };
    },
    _cacheMargins: function() {
      this.margins = {
        left: parseInt(this.element.css("marginLeft"), 10) || 0,
        top: parseInt(this.element.css("marginTop"), 10) || 0,
        right: parseInt(this.element.css("marginRight"), 10) || 0,
        bottom: parseInt(this.element.css("marginBottom"), 10) || 0
      };
    },
    _cacheHelperProportions: function() {
      this.helperProportions = {
        width: this.helper.outerWidth(),
        height: this.helper.outerHeight()
      };
    },
    _setContainment: function() {
      var e, i, n, o = this.options, s = this.document[0];
      return this.relativeContainer = null, o.containment ? "window" === o.containment ? void (this.containment = [ t(window).scrollLeft() - this.offset.relative.left - this.offset.parent.left, t(window).scrollTop() - this.offset.relative.top - this.offset.parent.top, t(window).scrollLeft() + t(window).width() - this.helperProportions.width - this.margins.left, t(window).scrollTop() + (t(window).height() || s.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top ]) : "document" === o.containment ? void (this.containment = [ 0, 0, t(s).width() - this.helperProportions.width - this.margins.left, (t(s).height() || s.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top ]) : o.containment.constructor === Array ? void (this.containment = o.containment) : ("parent" === o.containment && (o.containment = this.helper[0].parentNode), 
      i = t(o.containment), void ((n = i[0]) && (e = /(scroll|auto)/.test(i.css("overflow")), 
      this.containment = [ (parseInt(i.css("borderLeftWidth"), 10) || 0) + (parseInt(i.css("paddingLeft"), 10) || 0), (parseInt(i.css("borderTopWidth"), 10) || 0) + (parseInt(i.css("paddingTop"), 10) || 0), (e ? Math.max(n.scrollWidth, n.offsetWidth) : n.offsetWidth) - (parseInt(i.css("borderRightWidth"), 10) || 0) - (parseInt(i.css("paddingRight"), 10) || 0) - this.helperProportions.width - this.margins.left - this.margins.right, (e ? Math.max(n.scrollHeight, n.offsetHeight) : n.offsetHeight) - (parseInt(i.css("borderBottomWidth"), 10) || 0) - (parseInt(i.css("paddingBottom"), 10) || 0) - this.helperProportions.height - this.margins.top - this.margins.bottom ], 
      this.relativeContainer = i))) : void (this.containment = null);
    },
    _convertPositionTo: function(t, e) {
      e || (e = this.position);
      var i = "absolute" === t ? 1 : -1, n = this._isRootNode(this.scrollParent[0]);
      return {
        top: e.top + this.offset.relative.top * i + this.offset.parent.top * i - ("fixed" === this.cssPosition ? -this.offset.scroll.top : n ? 0 : this.offset.scroll.top) * i,
        left: e.left + this.offset.relative.left * i + this.offset.parent.left * i - ("fixed" === this.cssPosition ? -this.offset.scroll.left : n ? 0 : this.offset.scroll.left) * i
      };
    },
    _generatePosition: function(t, e) {
      var i, n, o, s, a = this.options, r = this._isRootNode(this.scrollParent[0]), l = t.pageX, c = t.pageY;
      return r && this.offset.scroll || (this.offset.scroll = {
        top: this.scrollParent.scrollTop(),
        left: this.scrollParent.scrollLeft()
      }), e && (this.containment && (this.relativeContainer ? (n = this.relativeContainer.offset(), 
      i = [ this.containment[0] + n.left, this.containment[1] + n.top, this.containment[2] + n.left, this.containment[3] + n.top ]) : i = this.containment, 
      t.pageX - this.offset.click.left < i[0] && (l = i[0] + this.offset.click.left), 
      t.pageY - this.offset.click.top < i[1] && (c = i[1] + this.offset.click.top), t.pageX - this.offset.click.left > i[2] && (l = i[2] + this.offset.click.left), 
      t.pageY - this.offset.click.top > i[3] && (c = i[3] + this.offset.click.top)), a.grid && (o = a.grid[1] ? this.originalPageY + Math.round((c - this.originalPageY) / a.grid[1]) * a.grid[1] : this.originalPageY, 
      c = i ? o - this.offset.click.top >= i[1] || o - this.offset.click.top > i[3] ? o : o - this.offset.click.top >= i[1] ? o - a.grid[1] : o + a.grid[1] : o, 
      s = a.grid[0] ? this.originalPageX + Math.round((l - this.originalPageX) / a.grid[0]) * a.grid[0] : this.originalPageX, 
      l = i ? s - this.offset.click.left >= i[0] || s - this.offset.click.left > i[2] ? s : s - this.offset.click.left >= i[0] ? s - a.grid[0] : s + a.grid[0] : s), 
      "y" === a.axis && (l = this.originalPageX), "x" === a.axis && (c = this.originalPageY)), 
      {
        top: c - this.offset.click.top - this.offset.relative.top - this.offset.parent.top + ("fixed" === this.cssPosition ? -this.offset.scroll.top : r ? 0 : this.offset.scroll.top),
        left: l - this.offset.click.left - this.offset.relative.left - this.offset.parent.left + ("fixed" === this.cssPosition ? -this.offset.scroll.left : r ? 0 : this.offset.scroll.left)
      };
    },
    _clear: function() {
      this.helper.removeClass("ui-draggable-dragging"), this.helper[0] === this.element[0] || this.cancelHelperRemoval || this.helper.remove(), 
      this.helper = null, this.cancelHelperRemoval = !1, this.destroyOnClear && this.destroy();
    },
    _normalizeRightBottom: function() {
      "y" !== this.options.axis && "auto" !== this.helper.css("right") && (this.helper.width(this.helper.width()), 
      this.helper.css("right", "auto")), "x" !== this.options.axis && "auto" !== this.helper.css("bottom") && (this.helper.height(this.helper.height()), 
      this.helper.css("bottom", "auto"));
    },
    _trigger: function(e, i, n) {
      return n = n || this._uiHash(), t.ui.plugin.call(this, e, [ i, n, this ], !0), /^(drag|start|stop)/.test(e) && (this.positionAbs = this._convertPositionTo("absolute"), 
      n.offset = this.positionAbs), t.Widget.prototype._trigger.call(this, e, i, n);
    },
    plugins: {},
    _uiHash: function() {
      return {
        helper: this.helper,
        position: this.position,
        originalPosition: this.originalPosition,
        offset: this.positionAbs
      };
    }
  }), t.ui.plugin.add("draggable", "connectToSortable", {
    start: function(e, i, n) {
      var o = t.extend({}, i, {
        item: n.element
      });
      n.sortables = [], t(n.options.connectToSortable).each(function() {
        var i = t(this).sortable("instance");
        i && !i.options.disabled && (n.sortables.push(i), i.refreshPositions(), i._trigger("activate", e, o));
      });
    },
    stop: function(e, i, n) {
      var o = t.extend({}, i, {
        item: n.element
      });
      n.cancelHelperRemoval = !1, t.each(n.sortables, function() {
        var t = this;
        t.isOver ? (t.isOver = 0, n.cancelHelperRemoval = !0, t.cancelHelperRemoval = !1, 
        t._storedCSS = {
          position: t.placeholder.css("position"),
          top: t.placeholder.css("top"),
          left: t.placeholder.css("left")
        }, t._mouseStop(e), t.options.helper = t.options._helper) : (t.cancelHelperRemoval = !0, 
        t._trigger("deactivate", e, o));
      });
    },
    drag: function(e, i, n) {
      t.each(n.sortables, function() {
        var o = !1, s = this;
        s.positionAbs = n.positionAbs, s.helperProportions = n.helperProportions, s.offset.click = n.offset.click, 
        s._intersectsWith(s.containerCache) && (o = !0, t.each(n.sortables, function() {
          return this.positionAbs = n.positionAbs, this.helperProportions = n.helperProportions, 
          this.offset.click = n.offset.click, this !== s && this._intersectsWith(this.containerCache) && t.contains(s.element[0], this.element[0]) && (o = !1), 
          o;
        })), o ? (s.isOver || (s.isOver = 1, s.currentItem = i.helper.appendTo(s.element).data("ui-sortable-item", !0), 
        s.options._helper = s.options.helper, s.options.helper = function() {
          return i.helper[0];
        }, e.target = s.currentItem[0], s._mouseCapture(e, !0), s._mouseStart(e, !0, !0), 
        s.offset.click.top = n.offset.click.top, s.offset.click.left = n.offset.click.left, 
        s.offset.parent.left -= n.offset.parent.left - s.offset.parent.left, s.offset.parent.top -= n.offset.parent.top - s.offset.parent.top, 
        n._trigger("toSortable", e), n.dropped = s.element, t.each(n.sortables, function() {
          this.refreshPositions();
        }), n.currentItem = n.element, s.fromOutside = n), s.currentItem && (s._mouseDrag(e), 
        i.position = s.position)) : s.isOver && (s.isOver = 0, s.cancelHelperRemoval = !0, 
        s.options._revert = s.options.revert, s.options.revert = !1, s._trigger("out", e, s._uiHash(s)), 
        s._mouseStop(e, !0), s.options.revert = s.options._revert, s.options.helper = s.options._helper, 
        s.placeholder && s.placeholder.remove(), n._refreshOffsets(e), i.position = n._generatePosition(e, !0), 
        n._trigger("fromSortable", e), n.dropped = !1, t.each(n.sortables, function() {
          this.refreshPositions();
        }));
      });
    }
  }), t.ui.plugin.add("draggable", "cursor", {
    start: function(e, i, n) {
      var o = t("body"), s = n.options;
      o.css("cursor") && (s._cursor = o.css("cursor")), o.css("cursor", s.cursor);
    },
    stop: function(e, i, n) {
      var o = n.options;
      o._cursor && t("body").css("cursor", o._cursor);
    }
  }), t.ui.plugin.add("draggable", "opacity", {
    start: function(e, i, n) {
      var o = t(i.helper), s = n.options;
      o.css("opacity") && (s._opacity = o.css("opacity")), o.css("opacity", s.opacity);
    },
    stop: function(e, i, n) {
      var o = n.options;
      o._opacity && t(i.helper).css("opacity", o._opacity);
    }
  }), t.ui.plugin.add("draggable", "scroll", {
    start: function(t, e, i) {
      i.scrollParentNotHidden || (i.scrollParentNotHidden = i.helper.scrollParent(!1)), 
      i.scrollParentNotHidden[0] !== i.document[0] && "HTML" !== i.scrollParentNotHidden[0].tagName && (i.overflowOffset = i.scrollParentNotHidden.offset());
    },
    drag: function(e, i, n) {
      var o = n.options, s = !1, a = n.scrollParentNotHidden[0], r = n.document[0];
      a !== r && "HTML" !== a.tagName ? (o.axis && "x" === o.axis || (n.overflowOffset.top + a.offsetHeight - e.pageY < o.scrollSensitivity ? a.scrollTop = s = a.scrollTop + o.scrollSpeed : e.pageY - n.overflowOffset.top < o.scrollSensitivity && (a.scrollTop = s = a.scrollTop - o.scrollSpeed)), 
      o.axis && "y" === o.axis || (n.overflowOffset.left + a.offsetWidth - e.pageX < o.scrollSensitivity ? a.scrollLeft = s = a.scrollLeft + o.scrollSpeed : e.pageX - n.overflowOffset.left < o.scrollSensitivity && (a.scrollLeft = s = a.scrollLeft - o.scrollSpeed))) : (o.axis && "x" === o.axis || (e.pageY - t(r).scrollTop() < o.scrollSensitivity ? s = t(r).scrollTop(t(r).scrollTop() - o.scrollSpeed) : t(window).height() - (e.pageY - t(r).scrollTop()) < o.scrollSensitivity && (s = t(r).scrollTop(t(r).scrollTop() + o.scrollSpeed))), 
      o.axis && "y" === o.axis || (e.pageX - t(r).scrollLeft() < o.scrollSensitivity ? s = t(r).scrollLeft(t(r).scrollLeft() - o.scrollSpeed) : t(window).width() - (e.pageX - t(r).scrollLeft()) < o.scrollSensitivity && (s = t(r).scrollLeft(t(r).scrollLeft() + o.scrollSpeed)))), 
      !1 !== s && t.ui.ddmanager && !o.dropBehaviour && t.ui.ddmanager.prepareOffsets(n, e);
    }
  }), t.ui.plugin.add("draggable", "snap", {
    start: function(e, i, n) {
      var o = n.options;
      n.snapElements = [], t(o.snap.constructor !== String ? o.snap.items || ":data(ui-draggable)" : o.snap).each(function() {
        var e = t(this), i = e.offset();
        this !== n.element[0] && n.snapElements.push({
          item: this,
          width: e.outerWidth(),
          height: e.outerHeight(),
          top: i.top,
          left: i.left
        });
      });
    },
    drag: function(e, i, n) {
      var o, s, a, r, l, c, d, u, h, p, f = n.options, m = f.snapTolerance, g = i.offset.left, v = g + n.helperProportions.width, b = i.offset.top, C = b + n.helperProportions.height;
      for (h = n.snapElements.length - 1; h >= 0; h--) l = n.snapElements[h].left - n.margins.left, 
      c = l + n.snapElements[h].width, d = n.snapElements[h].top - n.margins.top, u = d + n.snapElements[h].height, 
      l - m > v || g > c + m || d - m > C || b > u + m || !t.contains(n.snapElements[h].item.ownerDocument, n.snapElements[h].item) ? (n.snapElements[h].snapping && n.options.snap.release && n.options.snap.release.call(n.element, e, t.extend(n._uiHash(), {
        snapItem: n.snapElements[h].item
      })), n.snapElements[h].snapping = !1) : ("inner" !== f.snapMode && (o = m >= Math.abs(d - C), 
      s = m >= Math.abs(u - b), a = m >= Math.abs(l - v), r = m >= Math.abs(c - g), o && (i.position.top = n._convertPositionTo("relative", {
        top: d - n.helperProportions.height,
        left: 0
      }).top), s && (i.position.top = n._convertPositionTo("relative", {
        top: u,
        left: 0
      }).top), a && (i.position.left = n._convertPositionTo("relative", {
        top: 0,
        left: l - n.helperProportions.width
      }).left), r && (i.position.left = n._convertPositionTo("relative", {
        top: 0,
        left: c
      }).left)), p = o || s || a || r, "outer" !== f.snapMode && (o = m >= Math.abs(d - b), 
      s = m >= Math.abs(u - C), a = m >= Math.abs(l - g), r = m >= Math.abs(c - v), o && (i.position.top = n._convertPositionTo("relative", {
        top: d,
        left: 0
      }).top), s && (i.position.top = n._convertPositionTo("relative", {
        top: u - n.helperProportions.height,
        left: 0
      }).top), a && (i.position.left = n._convertPositionTo("relative", {
        top: 0,
        left: l
      }).left), r && (i.position.left = n._convertPositionTo("relative", {
        top: 0,
        left: c - n.helperProportions.width
      }).left)), !n.snapElements[h].snapping && (o || s || a || r || p) && n.options.snap.snap && n.options.snap.snap.call(n.element, e, t.extend(n._uiHash(), {
        snapItem: n.snapElements[h].item
      })), n.snapElements[h].snapping = o || s || a || r || p);
    }
  }), t.ui.plugin.add("draggable", "stack", {
    start: function(e, i, n) {
      var o, s = n.options, a = t.makeArray(t(s.stack)).sort(function(e, i) {
        return (parseInt(t(e).css("zIndex"), 10) || 0) - (parseInt(t(i).css("zIndex"), 10) || 0);
      });
      a.length && (o = parseInt(t(a[0]).css("zIndex"), 10) || 0, t(a).each(function(e) {
        t(this).css("zIndex", o + e);
      }), this.css("zIndex", o + a.length));
    }
  }), t.ui.plugin.add("draggable", "zIndex", {
    start: function(e, i, n) {
      var o = t(i.helper), s = n.options;
      o.css("zIndex") && (s._zIndex = o.css("zIndex")), o.css("zIndex", s.zIndex);
    },
    stop: function(e, i, n) {
      var o = n.options;
      o._zIndex && t(i.helper).css("zIndex", o._zIndex);
    }
  }), t.ui.draggable, t.widget("ui.droppable", {
    version: "1.11.2",
    widgetEventPrefix: "drop",
    options: {
      accept: "*",
      activeClass: !1,
      addClasses: !0,
      greedy: !1,
      hoverClass: !1,
      scope: "default",
      tolerance: "intersect",
      activate: null,
      deactivate: null,
      drop: null,
      out: null,
      over: null
    },
    _create: function() {
      var e, i = this.options, n = i.accept;
      this.isover = !1, this.isout = !0, this.accept = t.isFunction(n) ? n : function(t) {
        return t.is(n);
      }, this.proportions = function() {
        return arguments.length ? void (e = arguments[0]) : e || (e = {
          width: this.element[0].offsetWidth,
          height: this.element[0].offsetHeight
        });
      }, this._addToManager(i.scope), i.addClasses && this.element.addClass("ui-droppable");
    },
    _addToManager: function(e) {
      t.ui.ddmanager.droppables[e] = t.ui.ddmanager.droppables[e] || [], t.ui.ddmanager.droppables[e].push(this);
    },
    _splice: function(t) {
      for (var e = 0; t.length > e; e++) t[e] === this && t.splice(e, 1);
    },
    _destroy: function() {
      var e = t.ui.ddmanager.droppables[this.options.scope];
      this._splice(e), this.element.removeClass("ui-droppable ui-droppable-disabled");
    },
    _setOption: function(e, i) {
      if ("accept" === e) this.accept = t.isFunction(i) ? i : function(t) {
        return t.is(i);
      }; else if ("scope" === e) {
        var n = t.ui.ddmanager.droppables[this.options.scope];
        this._splice(n), this._addToManager(i);
      }
      this._super(e, i);
    },
    _activate: function(e) {
      var i = t.ui.ddmanager.current;
      this.options.activeClass && this.element.addClass(this.options.activeClass), i && this._trigger("activate", e, this.ui(i));
    },
    _deactivate: function(e) {
      var i = t.ui.ddmanager.current;
      this.options.activeClass && this.element.removeClass(this.options.activeClass), 
      i && this._trigger("deactivate", e, this.ui(i));
    },
    _over: function(e) {
      var i = t.ui.ddmanager.current;
      i && (i.currentItem || i.element)[0] !== this.element[0] && this.accept.call(this.element[0], i.currentItem || i.element) && (this.options.hoverClass && this.element.addClass(this.options.hoverClass), 
      this._trigger("over", e, this.ui(i)));
    },
    _out: function(e) {
      var i = t.ui.ddmanager.current;
      i && (i.currentItem || i.element)[0] !== this.element[0] && this.accept.call(this.element[0], i.currentItem || i.element) && (this.options.hoverClass && this.element.removeClass(this.options.hoverClass), 
      this._trigger("out", e, this.ui(i)));
    },
    _drop: function(e, i) {
      var n = i || t.ui.ddmanager.current, o = !1;
      return !(!n || (n.currentItem || n.element)[0] === this.element[0]) && (this.element.find(":data(ui-droppable)").not(".ui-draggable-dragging").each(function() {
        var i = t(this).droppable("instance");
        return i.options.greedy && !i.options.disabled && i.options.scope === n.options.scope && i.accept.call(i.element[0], n.currentItem || n.element) && t.ui.intersect(n, t.extend(i, {
          offset: i.element.offset()
        }), i.options.tolerance, e) ? (o = !0, !1) : void 0;
      }), !o && (!!this.accept.call(this.element[0], n.currentItem || n.element) && (this.options.activeClass && this.element.removeClass(this.options.activeClass), 
      this.options.hoverClass && this.element.removeClass(this.options.hoverClass), this._trigger("drop", e, this.ui(n)), 
      this.element)));
    },
    ui: function(t) {
      return {
        draggable: t.currentItem || t.element,
        helper: t.helper,
        position: t.position,
        offset: t.positionAbs
      };
    }
  }), t.ui.intersect = function() {
    function t(t, e, i) {
      return t >= e && e + i > t;
    }
    return function(e, i, n, o) {
      if (!i.offset) return !1;
      var s = (e.positionAbs || e.position.absolute).left + e.margins.left, a = (e.positionAbs || e.position.absolute).top + e.margins.top, r = s + e.helperProportions.width, l = a + e.helperProportions.height, c = i.offset.left, d = i.offset.top, u = c + i.proportions().width, h = d + i.proportions().height;
      switch (n) {
       case "fit":
        return s >= c && u >= r && a >= d && h >= l;

       case "intersect":
        return s + e.helperProportions.width / 2 > c && u > r - e.helperProportions.width / 2 && a + e.helperProportions.height / 2 > d && h > l - e.helperProportions.height / 2;

       case "pointer":
        return t(o.pageY, d, i.proportions().height) && t(o.pageX, c, i.proportions().width);

       case "touch":
        return (a >= d && h >= a || l >= d && h >= l || d > a && l > h) && (s >= c && u >= s || r >= c && u >= r || c > s && r > u);

       default:
        return !1;
      }
    };
  }(), t.ui.ddmanager = {
    current: null,
    droppables: {
      default: []
    },
    prepareOffsets: function(e, i) {
      var n, o, s = t.ui.ddmanager.droppables[e.options.scope] || [], a = i ? i.type : null, r = (e.currentItem || e.element).find(":data(ui-droppable)").addBack();
      t: for (n = 0; s.length > n; n++) if (!(s[n].options.disabled || e && !s[n].accept.call(s[n].element[0], e.currentItem || e.element))) {
        for (o = 0; r.length > o; o++) if (r[o] === s[n].element[0]) {
          s[n].proportions().height = 0;
          continue t;
        }
        s[n].visible = "none" !== s[n].element.css("display"), s[n].visible && ("mousedown" === a && s[n]._activate.call(s[n], i), 
        s[n].offset = s[n].element.offset(), s[n].proportions({
          width: s[n].element[0].offsetWidth,
          height: s[n].element[0].offsetHeight
        }));
      }
    },
    drop: function(e, i) {
      var n = !1;
      return t.each((t.ui.ddmanager.droppables[e.options.scope] || []).slice(), function() {
        this.options && (!this.options.disabled && this.visible && t.ui.intersect(e, this, this.options.tolerance, i) && (n = this._drop.call(this, i) || n), 
        !this.options.disabled && this.visible && this.accept.call(this.element[0], e.currentItem || e.element) && (this.isout = !0, 
        this.isover = !1, this._deactivate.call(this, i)));
      }), n;
    },
    dragStart: function(e, i) {
      e.element.parentsUntil("body").bind("scroll.droppable", function() {
        e.options.refreshPositions || t.ui.ddmanager.prepareOffsets(e, i);
      });
    },
    drag: function(e, i) {
      e.options.refreshPositions && t.ui.ddmanager.prepareOffsets(e, i), t.each(t.ui.ddmanager.droppables[e.options.scope] || [], function() {
        if (!this.options.disabled && !this.greedyChild && this.visible) {
          var n, o, s, a = t.ui.intersect(e, this, this.options.tolerance, i), r = !a && this.isover ? "isout" : a && !this.isover ? "isover" : null;
          r && (this.options.greedy && (o = this.options.scope, (s = this.element.parents(":data(ui-droppable)").filter(function() {
            return t(this).droppable("instance").options.scope === o;
          })).length && (n = t(s[0]).droppable("instance"), n.greedyChild = "isover" === r)), 
          n && "isover" === r && (n.isover = !1, n.isout = !0, n._out.call(n, i)), this[r] = !0, 
          this["isout" === r ? "isover" : "isout"] = !1, this["isover" === r ? "_over" : "_out"].call(this, i), 
          n && "isout" === r && (n.isout = !1, n.isover = !0, n._over.call(n, i)));
        }
      });
    },
    dragStop: function(e, i) {
      e.element.parentsUntil("body").unbind("scroll.droppable"), e.options.refreshPositions || t.ui.ddmanager.prepareOffsets(e, i);
    }
  }, t.ui.droppable, t.widget("ui.resizable", t.ui.mouse, {
    version: "1.11.2",
    widgetEventPrefix: "resize",
    options: {
      alsoResize: !1,
      animate: !1,
      animateDuration: "slow",
      animateEasing: "swing",
      aspectRatio: !1,
      autoHide: !1,
      containment: !1,
      ghost: !1,
      grid: !1,
      handles: "e,s,se",
      helper: !1,
      maxHeight: null,
      maxWidth: null,
      minHeight: 10,
      minWidth: 10,
      zIndex: 90,
      resize: null,
      start: null,
      stop: null
    },
    _num: function(t) {
      return parseInt(t, 10) || 0;
    },
    _isNumber: function(t) {
      return !isNaN(parseInt(t, 10));
    },
    _hasScroll: function(e, i) {
      if ("hidden" === t(e).css("overflow")) return !1;
      var n = i && "left" === i ? "scrollLeft" : "scrollTop", o = !1;
      return e[n] > 0 || (e[n] = 1, o = e[n] > 0, e[n] = 0, o);
    },
    _create: function() {
      var e, i, n, o, s, a = this, r = this.options;
      if (this.element.addClass("ui-resizable"), t.extend(this, {
        _aspectRatio: !!r.aspectRatio,
        aspectRatio: r.aspectRatio,
        originalElement: this.element,
        _proportionallyResizeElements: [],
        _helper: r.helper || r.ghost || r.animate ? r.helper || "ui-resizable-helper" : null
      }), this.element[0].nodeName.match(/canvas|textarea|input|select|button|img/i) && (this.element.wrap(t("<div class='ui-wrapper' style='overflow: hidden;'></div>").css({
        position: this.element.css("position"),
        width: this.element.outerWidth(),
        height: this.element.outerHeight(),
        top: this.element.css("top"),
        left: this.element.css("left")
      })), this.element = this.element.parent().data("ui-resizable", this.element.resizable("instance")), 
      this.elementIsWrapper = !0, this.element.css({
        marginLeft: this.originalElement.css("marginLeft"),
        marginTop: this.originalElement.css("marginTop"),
        marginRight: this.originalElement.css("marginRight"),
        marginBottom: this.originalElement.css("marginBottom")
      }), this.originalElement.css({
        marginLeft: 0,
        marginTop: 0,
        marginRight: 0,
        marginBottom: 0
      }), this.originalResizeStyle = this.originalElement.css("resize"), this.originalElement.css("resize", "none"), 
      this._proportionallyResizeElements.push(this.originalElement.css({
        position: "static",
        zoom: 1,
        display: "block"
      })), this.originalElement.css({
        margin: this.originalElement.css("margin")
      }), this._proportionallyResize()), this.handles = r.handles || (t(".ui-resizable-handle", this.element).length ? {
        n: ".ui-resizable-n",
        e: ".ui-resizable-e",
        s: ".ui-resizable-s",
        w: ".ui-resizable-w",
        se: ".ui-resizable-se",
        sw: ".ui-resizable-sw",
        ne: ".ui-resizable-ne",
        nw: ".ui-resizable-nw"
      } : "e,s,se"), this.handles.constructor === String) for ("all" === this.handles && (this.handles = "n,e,s,w,se,sw,ne,nw"), 
      e = this.handles.split(","), this.handles = {}, i = 0; e.length > i; i++) n = t.trim(e[i]), 
      s = "ui-resizable-" + n, (o = t("<div class='ui-resizable-handle " + s + "'></div>")).css({
        zIndex: r.zIndex
      }), "se" === n && o.addClass("ui-icon ui-icon-gripsmall-diagonal-se"), this.handles[n] = ".ui-resizable-" + n, 
      this.element.append(o);
      this._renderAxis = function(e) {
        var i, n, o, s;
        e = e || this.element;
        for (i in this.handles) this.handles[i].constructor === String && (this.handles[i] = this.element.children(this.handles[i]).first().show()), 
        this.elementIsWrapper && this.originalElement[0].nodeName.match(/textarea|input|select|button/i) && (n = t(this.handles[i], this.element), 
        s = /sw|ne|nw|se|n|s/.test(i) ? n.outerHeight() : n.outerWidth(), o = [ "padding", /ne|nw|n/.test(i) ? "Top" : /se|sw|s/.test(i) ? "Bottom" : /^e$/.test(i) ? "Right" : "Left" ].join(""), 
        e.css(o, s), this._proportionallyResize()), t(this.handles[i]).length;
      }, this._renderAxis(this.element), this._handles = t(".ui-resizable-handle", this.element).disableSelection(), 
      this._handles.mouseover(function() {
        a.resizing || (this.className && (o = this.className.match(/ui-resizable-(se|sw|ne|nw|n|e|s|w)/i)), 
        a.axis = o && o[1] ? o[1] : "se");
      }), r.autoHide && (this._handles.hide(), t(this.element).addClass("ui-resizable-autohide").mouseenter(function() {
        r.disabled || (t(this).removeClass("ui-resizable-autohide"), a._handles.show());
      }).mouseleave(function() {
        r.disabled || a.resizing || (t(this).addClass("ui-resizable-autohide"), a._handles.hide());
      })), this._mouseInit();
    },
    _destroy: function() {
      this._mouseDestroy();
      var e, i = function(e) {
        t(e).removeClass("ui-resizable ui-resizable-disabled ui-resizable-resizing").removeData("resizable").removeData("ui-resizable").unbind(".resizable").find(".ui-resizable-handle").remove();
      };
      return this.elementIsWrapper && (i(this.element), e = this.element, this.originalElement.css({
        position: e.css("position"),
        width: e.outerWidth(),
        height: e.outerHeight(),
        top: e.css("top"),
        left: e.css("left")
      }).insertAfter(e), e.remove()), this.originalElement.css("resize", this.originalResizeStyle), 
      i(this.originalElement), this;
    },
    _mouseCapture: function(e) {
      var i, n, o = !1;
      for (i in this.handles) ((n = t(this.handles[i])[0]) === e.target || t.contains(n, e.target)) && (o = !0);
      return !this.options.disabled && o;
    },
    _mouseStart: function(e) {
      var i, n, o, s = this.options, a = this.element;
      return this.resizing = !0, this._renderProxy(), i = this._num(this.helper.css("left")), 
      n = this._num(this.helper.css("top")), s.containment && (i += t(s.containment).scrollLeft() || 0, 
      n += t(s.containment).scrollTop() || 0), this.offset = this.helper.offset(), this.position = {
        left: i,
        top: n
      }, this.size = this._helper ? {
        width: this.helper.width(),
        height: this.helper.height()
      } : {
        width: a.width(),
        height: a.height()
      }, this.originalSize = this._helper ? {
        width: a.outerWidth(),
        height: a.outerHeight()
      } : {
        width: a.width(),
        height: a.height()
      }, this.sizeDiff = {
        width: a.outerWidth() - a.width(),
        height: a.outerHeight() - a.height()
      }, this.originalPosition = {
        left: i,
        top: n
      }, this.originalMousePosition = {
        left: e.pageX,
        top: e.pageY
      }, this.aspectRatio = "number" == typeof s.aspectRatio ? s.aspectRatio : this.originalSize.width / this.originalSize.height || 1, 
      o = t(".ui-resizable-" + this.axis).css("cursor"), t("body").css("cursor", "auto" === o ? this.axis + "-resize" : o), 
      a.addClass("ui-resizable-resizing"), this._propagate("start", e), !0;
    },
    _mouseDrag: function(e) {
      var i, n, o = this.originalMousePosition, s = this.axis, a = e.pageX - o.left || 0, r = e.pageY - o.top || 0, l = this._change[s];
      return this._updatePrevProperties(), !!l && (i = l.apply(this, [ e, a, r ]), this._updateVirtualBoundaries(e.shiftKey), 
      (this._aspectRatio || e.shiftKey) && (i = this._updateRatio(i, e)), i = this._respectSize(i, e), 
      this._updateCache(i), this._propagate("resize", e), n = this._applyChanges(), !this._helper && this._proportionallyResizeElements.length && this._proportionallyResize(), 
      t.isEmptyObject(n) || (this._updatePrevProperties(), this._trigger("resize", e, this.ui()), 
      this._applyChanges()), !1);
    },
    _mouseStop: function(e) {
      this.resizing = !1;
      var i, n, o, s, a, r, l, c = this.options, d = this;
      return this._helper && (i = this._proportionallyResizeElements, n = i.length && /textarea/i.test(i[0].nodeName), 
      o = n && this._hasScroll(i[0], "left") ? 0 : d.sizeDiff.height, s = n ? 0 : d.sizeDiff.width, 
      a = {
        width: d.helper.width() - s,
        height: d.helper.height() - o
      }, r = parseInt(d.element.css("left"), 10) + (d.position.left - d.originalPosition.left) || null, 
      l = parseInt(d.element.css("top"), 10) + (d.position.top - d.originalPosition.top) || null, 
      c.animate || this.element.css(t.extend(a, {
        top: l,
        left: r
      })), d.helper.height(d.size.height), d.helper.width(d.size.width), this._helper && !c.animate && this._proportionallyResize()), 
      t("body").css("cursor", "auto"), this.element.removeClass("ui-resizable-resizing"), 
      this._propagate("stop", e), this._helper && this.helper.remove(), !1;
    },
    _updatePrevProperties: function() {
      this.prevPosition = {
        top: this.position.top,
        left: this.position.left
      }, this.prevSize = {
        width: this.size.width,
        height: this.size.height
      };
    },
    _applyChanges: function() {
      var t = {};
      return this.position.top !== this.prevPosition.top && (t.top = this.position.top + "px"), 
      this.position.left !== this.prevPosition.left && (t.left = this.position.left + "px"), 
      this.size.width !== this.prevSize.width && (t.width = this.size.width + "px"), this.size.height !== this.prevSize.height && (t.height = this.size.height + "px"), 
      this.helper.css(t), t;
    },
    _updateVirtualBoundaries: function(t) {
      var e, i, n, o, s, a = this.options;
      s = {
        minWidth: this._isNumber(a.minWidth) ? a.minWidth : 0,
        maxWidth: this._isNumber(a.maxWidth) ? a.maxWidth : 1 / 0,
        minHeight: this._isNumber(a.minHeight) ? a.minHeight : 0,
        maxHeight: this._isNumber(a.maxHeight) ? a.maxHeight : 1 / 0
      }, (this._aspectRatio || t) && (e = s.minHeight * this.aspectRatio, n = s.minWidth / this.aspectRatio, 
      i = s.maxHeight * this.aspectRatio, o = s.maxWidth / this.aspectRatio, e > s.minWidth && (s.minWidth = e), 
      n > s.minHeight && (s.minHeight = n), s.maxWidth > i && (s.maxWidth = i), s.maxHeight > o && (s.maxHeight = o)), 
      this._vBoundaries = s;
    },
    _updateCache: function(t) {
      this.offset = this.helper.offset(), this._isNumber(t.left) && (this.position.left = t.left), 
      this._isNumber(t.top) && (this.position.top = t.top), this._isNumber(t.height) && (this.size.height = t.height), 
      this._isNumber(t.width) && (this.size.width = t.width);
    },
    _updateRatio: function(t) {
      var e = this.position, i = this.size, n = this.axis;
      return this._isNumber(t.height) ? t.width = t.height * this.aspectRatio : this._isNumber(t.width) && (t.height = t.width / this.aspectRatio), 
      "sw" === n && (t.left = e.left + (i.width - t.width), t.top = null), "nw" === n && (t.top = e.top + (i.height - t.height), 
      t.left = e.left + (i.width - t.width)), t;
    },
    _respectSize: function(t) {
      var e = this._vBoundaries, i = this.axis, n = this._isNumber(t.width) && e.maxWidth && e.maxWidth < t.width, o = this._isNumber(t.height) && e.maxHeight && e.maxHeight < t.height, s = this._isNumber(t.width) && e.minWidth && e.minWidth > t.width, a = this._isNumber(t.height) && e.minHeight && e.minHeight > t.height, r = this.originalPosition.left + this.originalSize.width, l = this.position.top + this.size.height, c = /sw|nw|w/.test(i), d = /nw|ne|n/.test(i);
      return s && (t.width = e.minWidth), a && (t.height = e.minHeight), n && (t.width = e.maxWidth), 
      o && (t.height = e.maxHeight), s && c && (t.left = r - e.minWidth), n && c && (t.left = r - e.maxWidth), 
      a && d && (t.top = l - e.minHeight), o && d && (t.top = l - e.maxHeight), t.width || t.height || t.left || !t.top ? t.width || t.height || t.top || !t.left || (t.left = null) : t.top = null, 
      t;
    },
    _getPaddingPlusBorderDimensions: function(t) {
      for (var e = 0, i = [], n = [ t.css("borderTopWidth"), t.css("borderRightWidth"), t.css("borderBottomWidth"), t.css("borderLeftWidth") ], o = [ t.css("paddingTop"), t.css("paddingRight"), t.css("paddingBottom"), t.css("paddingLeft") ]; 4 > e; e++) i[e] = parseInt(n[e], 10) || 0, 
      i[e] += parseInt(o[e], 10) || 0;
      return {
        height: i[0] + i[2],
        width: i[1] + i[3]
      };
    },
    _proportionallyResize: function() {
      if (this._proportionallyResizeElements.length) for (var t, e = 0, i = this.helper || this.element; this._proportionallyResizeElements.length > e; e++) t = this._proportionallyResizeElements[e], 
      this.outerDimensions || (this.outerDimensions = this._getPaddingPlusBorderDimensions(t)), 
      t.css({
        height: i.height() - this.outerDimensions.height || 0,
        width: i.width() - this.outerDimensions.width || 0
      });
    },
    _renderProxy: function() {
      var e = this.element, i = this.options;
      this.elementOffset = e.offset(), this._helper ? (this.helper = this.helper || t("<div style='overflow:hidden;'></div>"), 
      this.helper.addClass(this._helper).css({
        width: this.element.outerWidth() - 1,
        height: this.element.outerHeight() - 1,
        position: "absolute",
        left: this.elementOffset.left + "px",
        top: this.elementOffset.top + "px",
        zIndex: ++i.zIndex
      }), this.helper.appendTo("body").disableSelection()) : this.helper = this.element;
    },
    _change: {
      e: function(t, e) {
        return {
          width: this.originalSize.width + e
        };
      },
      w: function(t, e) {
        var i = this.originalSize;
        return {
          left: this.originalPosition.left + e,
          width: i.width - e
        };
      },
      n: function(t, e, i) {
        var n = this.originalSize;
        return {
          top: this.originalPosition.top + i,
          height: n.height - i
        };
      },
      s: function(t, e, i) {
        return {
          height: this.originalSize.height + i
        };
      },
      se: function(e, i, n) {
        return t.extend(this._change.s.apply(this, arguments), this._change.e.apply(this, [ e, i, n ]));
      },
      sw: function(e, i, n) {
        return t.extend(this._change.s.apply(this, arguments), this._change.w.apply(this, [ e, i, n ]));
      },
      ne: function(e, i, n) {
        return t.extend(this._change.n.apply(this, arguments), this._change.e.apply(this, [ e, i, n ]));
      },
      nw: function(e, i, n) {
        return t.extend(this._change.n.apply(this, arguments), this._change.w.apply(this, [ e, i, n ]));
      }
    },
    _propagate: function(e, i) {
      t.ui.plugin.call(this, e, [ i, this.ui() ]), "resize" !== e && this._trigger(e, i, this.ui());
    },
    plugins: {},
    ui: function() {
      return {
        originalElement: this.originalElement,
        element: this.element,
        helper: this.helper,
        position: this.position,
        size: this.size,
        originalSize: this.originalSize,
        originalPosition: this.originalPosition
      };
    }
  }), t.ui.plugin.add("resizable", "animate", {
    stop: function(e) {
      var i = t(this).resizable("instance"), n = i.options, o = i._proportionallyResizeElements, s = o.length && /textarea/i.test(o[0].nodeName), a = s && i._hasScroll(o[0], "left") ? 0 : i.sizeDiff.height, r = s ? 0 : i.sizeDiff.width, l = {
        width: i.size.width - r,
        height: i.size.height - a
      }, c = parseInt(i.element.css("left"), 10) + (i.position.left - i.originalPosition.left) || null, d = parseInt(i.element.css("top"), 10) + (i.position.top - i.originalPosition.top) || null;
      i.element.animate(t.extend(l, d && c ? {
        top: d,
        left: c
      } : {}), {
        duration: n.animateDuration,
        easing: n.animateEasing,
        step: function() {
          var n = {
            width: parseInt(i.element.css("width"), 10),
            height: parseInt(i.element.css("height"), 10),
            top: parseInt(i.element.css("top"), 10),
            left: parseInt(i.element.css("left"), 10)
          };
          o && o.length && t(o[0]).css({
            width: n.width,
            height: n.height
          }), i._updateCache(n), i._propagate("resize", e);
        }
      });
    }
  }), t.ui.plugin.add("resizable", "containment", {
    start: function() {
      var e, i, n, o, s, a, r, l = t(this).resizable("instance"), c = l.options, d = l.element, u = c.containment, h = u instanceof t ? u.get(0) : /parent/.test(u) ? d.parent().get(0) : u;
      h && (l.containerElement = t(h), /document/.test(u) || u === document ? (l.containerOffset = {
        left: 0,
        top: 0
      }, l.containerPosition = {
        left: 0,
        top: 0
      }, l.parentData = {
        element: t(document),
        left: 0,
        top: 0,
        width: t(document).width(),
        height: t(document).height() || document.body.parentNode.scrollHeight
      }) : (e = t(h), i = [], t([ "Top", "Right", "Left", "Bottom" ]).each(function(t, n) {
        i[t] = l._num(e.css("padding" + n));
      }), l.containerOffset = e.offset(), l.containerPosition = e.position(), l.containerSize = {
        height: e.innerHeight() - i[3],
        width: e.innerWidth() - i[1]
      }, n = l.containerOffset, o = l.containerSize.height, s = l.containerSize.width, 
      a = l._hasScroll(h, "left") ? h.scrollWidth : s, r = l._hasScroll(h) ? h.scrollHeight : o, 
      l.parentData = {
        element: h,
        left: n.left,
        top: n.top,
        width: a,
        height: r
      }));
    },
    resize: function(e) {
      var i, n, o, s, a = t(this).resizable("instance"), r = a.options, l = a.containerOffset, c = a.position, d = a._aspectRatio || e.shiftKey, u = {
        top: 0,
        left: 0
      }, h = a.containerElement, p = !0;
      h[0] !== document && /static/.test(h.css("position")) && (u = l), c.left < (a._helper ? l.left : 0) && (a.size.width = a.size.width + (a._helper ? a.position.left - l.left : a.position.left - u.left), 
      d && (a.size.height = a.size.width / a.aspectRatio, p = !1), a.position.left = r.helper ? l.left : 0), 
      c.top < (a._helper ? l.top : 0) && (a.size.height = a.size.height + (a._helper ? a.position.top - l.top : a.position.top), 
      d && (a.size.width = a.size.height * a.aspectRatio, p = !1), a.position.top = a._helper ? l.top : 0), 
      o = a.containerElement.get(0) === a.element.parent().get(0), s = /relative|absolute/.test(a.containerElement.css("position")), 
      o && s ? (a.offset.left = a.parentData.left + a.position.left, a.offset.top = a.parentData.top + a.position.top) : (a.offset.left = a.element.offset().left, 
      a.offset.top = a.element.offset().top), i = Math.abs(a.sizeDiff.width + (a._helper ? a.offset.left - u.left : a.offset.left - l.left)), 
      n = Math.abs(a.sizeDiff.height + (a._helper ? a.offset.top - u.top : a.offset.top - l.top)), 
      i + a.size.width >= a.parentData.width && (a.size.width = a.parentData.width - i, 
      d && (a.size.height = a.size.width / a.aspectRatio, p = !1)), n + a.size.height >= a.parentData.height && (a.size.height = a.parentData.height - n, 
      d && (a.size.width = a.size.height * a.aspectRatio, p = !1)), p || (a.position.left = a.prevPosition.left, 
      a.position.top = a.prevPosition.top, a.size.width = a.prevSize.width, a.size.height = a.prevSize.height);
    },
    stop: function() {
      var e = t(this).resizable("instance"), i = e.options, n = e.containerOffset, o = e.containerPosition, s = e.containerElement, a = t(e.helper), r = a.offset(), l = a.outerWidth() - e.sizeDiff.width, c = a.outerHeight() - e.sizeDiff.height;
      e._helper && !i.animate && /relative/.test(s.css("position")) && t(this).css({
        left: r.left - o.left - n.left,
        width: l,
        height: c
      }), e._helper && !i.animate && /static/.test(s.css("position")) && t(this).css({
        left: r.left - o.left - n.left,
        width: l,
        height: c
      });
    }
  }), t.ui.plugin.add("resizable", "alsoResize", {
    start: function() {
      var e = t(this).resizable("instance").options, i = function(e) {
        t(e).each(function() {
          var e = t(this);
          e.data("ui-resizable-alsoresize", {
            width: parseInt(e.width(), 10),
            height: parseInt(e.height(), 10),
            left: parseInt(e.css("left"), 10),
            top: parseInt(e.css("top"), 10)
          });
        });
      };
      "object" != typeof e.alsoResize || e.alsoResize.parentNode ? i(e.alsoResize) : e.alsoResize.length ? (e.alsoResize = e.alsoResize[0], 
      i(e.alsoResize)) : t.each(e.alsoResize, function(t) {
        i(t);
      });
    },
    resize: function(e, i) {
      var n = t(this).resizable("instance"), o = n.options, s = n.originalSize, a = n.originalPosition, r = {
        height: n.size.height - s.height || 0,
        width: n.size.width - s.width || 0,
        top: n.position.top - a.top || 0,
        left: n.position.left - a.left || 0
      }, l = function(e, n) {
        t(e).each(function() {
          var e = t(this), o = t(this).data("ui-resizable-alsoresize"), s = {}, a = n && n.length ? n : e.parents(i.originalElement[0]).length ? [ "width", "height" ] : [ "width", "height", "top", "left" ];
          t.each(a, function(t, e) {
            var i = (o[e] || 0) + (r[e] || 0);
            i && i >= 0 && (s[e] = i || null);
          }), e.css(s);
        });
      };
      "object" != typeof o.alsoResize || o.alsoResize.nodeType ? l(o.alsoResize) : t.each(o.alsoResize, function(t, e) {
        l(t, e);
      });
    },
    stop: function() {
      t(this).removeData("resizable-alsoresize");
    }
  }), t.ui.plugin.add("resizable", "ghost", {
    start: function() {
      var e = t(this).resizable("instance"), i = e.options, n = e.size;
      e.ghost = e.originalElement.clone(), e.ghost.css({
        opacity: .25,
        display: "block",
        position: "relative",
        height: n.height,
        width: n.width,
        margin: 0,
        left: 0,
        top: 0
      }).addClass("ui-resizable-ghost").addClass("string" == typeof i.ghost ? i.ghost : ""), 
      e.ghost.appendTo(e.helper);
    },
    resize: function() {
      var e = t(this).resizable("instance");
      e.ghost && e.ghost.css({
        position: "relative",
        height: e.size.height,
        width: e.size.width
      });
    },
    stop: function() {
      var e = t(this).resizable("instance");
      e.ghost && e.helper && e.helper.get(0).removeChild(e.ghost.get(0));
    }
  }), t.ui.plugin.add("resizable", "grid", {
    resize: function() {
      var e, i = t(this).resizable("instance"), n = i.options, o = i.size, s = i.originalSize, a = i.originalPosition, r = i.axis, l = "number" == typeof n.grid ? [ n.grid, n.grid ] : n.grid, c = l[0] || 1, d = l[1] || 1, u = Math.round((o.width - s.width) / c) * c, h = Math.round((o.height - s.height) / d) * d, p = s.width + u, f = s.height + h, m = n.maxWidth && p > n.maxWidth, g = n.maxHeight && f > n.maxHeight, v = n.minWidth && n.minWidth > p, b = n.minHeight && n.minHeight > f;
      n.grid = l, v && (p += c), b && (f += d), m && (p -= c), g && (f -= d), /^(se|s|e)$/.test(r) ? (i.size.width = p, 
      i.size.height = f) : /^(ne)$/.test(r) ? (i.size.width = p, i.size.height = f, i.position.top = a.top - h) : /^(sw)$/.test(r) ? (i.size.width = p, 
      i.size.height = f, i.position.left = a.left - u) : ((0 >= f - d || 0 >= p - c) && (e = i._getPaddingPlusBorderDimensions(this)), 
      f - d > 0 ? (i.size.height = f, i.position.top = a.top - h) : (f = d - e.height, 
      i.size.height = f, i.position.top = a.top + s.height - f), p - c > 0 ? (i.size.width = p, 
      i.position.left = a.left - u) : (p = d - e.height, i.size.width = p, i.position.left = a.left + s.width - p));
    }
  }), t.ui.resizable, t.widget("ui.selectable", t.ui.mouse, {
    version: "1.11.2",
    options: {
      appendTo: "body",
      autoRefresh: !0,
      distance: 0,
      filter: "*",
      tolerance: "touch",
      selected: null,
      selecting: null,
      start: null,
      stop: null,
      unselected: null,
      unselecting: null
    },
    _create: function() {
      var e, i = this;
      this.element.addClass("ui-selectable"), this.dragged = !1, this.refresh = function() {
        (e = t(i.options.filter, i.element[0])).addClass("ui-selectee"), e.each(function() {
          var e = t(this), i = e.offset();
          t.data(this, "selectable-item", {
            element: this,
            $element: e,
            left: i.left,
            top: i.top,
            right: i.left + e.outerWidth(),
            bottom: i.top + e.outerHeight(),
            startselected: !1,
            selected: e.hasClass("ui-selected"),
            selecting: e.hasClass("ui-selecting"),
            unselecting: e.hasClass("ui-unselecting")
          });
        });
      }, this.refresh(), this.selectees = e.addClass("ui-selectee"), this._mouseInit(), 
      this.helper = t("<div class='ui-selectable-helper'></div>");
    },
    _destroy: function() {
      this.selectees.removeClass("ui-selectee").removeData("selectable-item"), this.element.removeClass("ui-selectable ui-selectable-disabled"), 
      this._mouseDestroy();
    },
    _mouseStart: function(e) {
      var i = this, n = this.options;
      this.opos = [ e.pageX, e.pageY ], this.options.disabled || (this.selectees = t(n.filter, this.element[0]), 
      this._trigger("start", e), t(n.appendTo).append(this.helper), this.helper.css({
        left: e.pageX,
        top: e.pageY,
        width: 0,
        height: 0
      }), n.autoRefresh && this.refresh(), this.selectees.filter(".ui-selected").each(function() {
        var n = t.data(this, "selectable-item");
        n.startselected = !0, e.metaKey || e.ctrlKey || (n.$element.removeClass("ui-selected"), 
        n.selected = !1, n.$element.addClass("ui-unselecting"), n.unselecting = !0, i._trigger("unselecting", e, {
          unselecting: n.element
        }));
      }), t(e.target).parents().addBack().each(function() {
        var n, o = t.data(this, "selectable-item");
        return o ? (n = !e.metaKey && !e.ctrlKey || !o.$element.hasClass("ui-selected"), 
        o.$element.removeClass(n ? "ui-unselecting" : "ui-selected").addClass(n ? "ui-selecting" : "ui-unselecting"), 
        o.unselecting = !n, o.selecting = n, o.selected = n, n ? i._trigger("selecting", e, {
          selecting: o.element
        }) : i._trigger("unselecting", e, {
          unselecting: o.element
        }), !1) : void 0;
      }));
    },
    _mouseDrag: function(e) {
      if (this.dragged = !0, !this.options.disabled) {
        var i, n = this, o = this.options, s = this.opos[0], a = this.opos[1], r = e.pageX, l = e.pageY;
        return s > r && (i = r, r = s, s = i), a > l && (i = l, l = a, a = i), this.helper.css({
          left: s,
          top: a,
          width: r - s,
          height: l - a
        }), this.selectees.each(function() {
          var i = t.data(this, "selectable-item"), c = !1;
          i && i.element !== n.element[0] && ("touch" === o.tolerance ? c = !(i.left > r || s > i.right || i.top > l || a > i.bottom) : "fit" === o.tolerance && (c = i.left > s && r > i.right && i.top > a && l > i.bottom), 
          c ? (i.selected && (i.$element.removeClass("ui-selected"), i.selected = !1), i.unselecting && (i.$element.removeClass("ui-unselecting"), 
          i.unselecting = !1), i.selecting || (i.$element.addClass("ui-selecting"), i.selecting = !0, 
          n._trigger("selecting", e, {
            selecting: i.element
          }))) : (i.selecting && ((e.metaKey || e.ctrlKey) && i.startselected ? (i.$element.removeClass("ui-selecting"), 
          i.selecting = !1, i.$element.addClass("ui-selected"), i.selected = !0) : (i.$element.removeClass("ui-selecting"), 
          i.selecting = !1, i.startselected && (i.$element.addClass("ui-unselecting"), i.unselecting = !0), 
          n._trigger("unselecting", e, {
            unselecting: i.element
          }))), i.selected && (e.metaKey || e.ctrlKey || i.startselected || (i.$element.removeClass("ui-selected"), 
          i.selected = !1, i.$element.addClass("ui-unselecting"), i.unselecting = !0, n._trigger("unselecting", e, {
            unselecting: i.element
          })))));
        }), !1;
      }
    },
    _mouseStop: function(e) {
      var i = this;
      return this.dragged = !1, t(".ui-unselecting", this.element[0]).each(function() {
        var n = t.data(this, "selectable-item");
        n.$element.removeClass("ui-unselecting"), n.unselecting = !1, n.startselected = !1, 
        i._trigger("unselected", e, {
          unselected: n.element
        });
      }), t(".ui-selecting", this.element[0]).each(function() {
        var n = t.data(this, "selectable-item");
        n.$element.removeClass("ui-selecting").addClass("ui-selected"), n.selecting = !1, 
        n.selected = !0, n.startselected = !0, i._trigger("selected", e, {
          selected: n.element
        });
      }), this._trigger("stop", e), this.helper.remove(), !1;
    }
  }), t.widget("ui.sortable", t.ui.mouse, {
    version: "1.11.2",
    widgetEventPrefix: "sort",
    ready: !1,
    options: {
      appendTo: "parent",
      axis: !1,
      connectWith: !1,
      containment: !1,
      cursor: "auto",
      cursorAt: !1,
      dropOnEmpty: !0,
      forcePlaceholderSize: !1,
      forceHelperSize: !1,
      grid: !1,
      handle: !1,
      helper: "original",
      items: "> *",
      opacity: !1,
      placeholder: !1,
      revert: !1,
      scroll: !0,
      scrollSensitivity: 20,
      scrollSpeed: 20,
      scope: "default",
      tolerance: "intersect",
      zIndex: 1e3,
      activate: null,
      beforeStop: null,
      change: null,
      deactivate: null,
      out: null,
      over: null,
      receive: null,
      remove: null,
      sort: null,
      start: null,
      stop: null,
      update: null
    },
    _isOverAxis: function(t, e, i) {
      return t >= e && e + i > t;
    },
    _isFloating: function(t) {
      return /left|right/.test(t.css("float")) || /inline|table-cell/.test(t.css("display"));
    },
    _create: function() {
      var t = this.options;
      this.containerCache = {}, this.element.addClass("ui-sortable"), this.refresh(), 
      this.floating = !!this.items.length && ("x" === t.axis || this._isFloating(this.items[0].item)), 
      this.offset = this.element.offset(), this._mouseInit(), this._setHandleClassName(), 
      this.ready = !0;
    },
    _setOption: function(t, e) {
      this._super(t, e), "handle" === t && this._setHandleClassName();
    },
    _setHandleClassName: function() {
      this.element.find(".ui-sortable-handle").removeClass("ui-sortable-handle"), t.each(this.items, function() {
        (this.instance.options.handle ? this.item.find(this.instance.options.handle) : this.item).addClass("ui-sortable-handle");
      });
    },
    _destroy: function() {
      this.element.removeClass("ui-sortable ui-sortable-disabled").find(".ui-sortable-handle").removeClass("ui-sortable-handle"), 
      this._mouseDestroy();
      for (var t = this.items.length - 1; t >= 0; t--) this.items[t].item.removeData(this.widgetName + "-item");
      return this;
    },
    _mouseCapture: function(e, i) {
      var n = null, o = !1, s = this;
      return !this.reverting && (!this.options.disabled && "static" !== this.options.type && (this._refreshItems(e), 
      t(e.target).parents().each(function() {
        return t.data(this, s.widgetName + "-item") === s ? (n = t(this), !1) : void 0;
      }), t.data(e.target, s.widgetName + "-item") === s && (n = t(e.target)), !!n && (!(this.options.handle && !i && (t(this.options.handle, n).find("*").addBack().each(function() {
        this === e.target && (o = !0);
      }), !o)) && (this.currentItem = n, this._removeCurrentsFromItems(), !0))));
    },
    _mouseStart: function(e, i, n) {
      var o, s, a = this.options;
      if (this.currentContainer = this, this.refreshPositions(), this.helper = this._createHelper(e), 
      this._cacheHelperProportions(), this._cacheMargins(), this.scrollParent = this.helper.scrollParent(), 
      this.offset = this.currentItem.offset(), this.offset = {
        top: this.offset.top - this.margins.top,
        left: this.offset.left - this.margins.left
      }, t.extend(this.offset, {
        click: {
          left: e.pageX - this.offset.left,
          top: e.pageY - this.offset.top
        },
        parent: this._getParentOffset(),
        relative: this._getRelativeOffset()
      }), this.helper.css("position", "absolute"), this.cssPosition = this.helper.css("position"), 
      this.originalPosition = this._generatePosition(e), this.originalPageX = e.pageX, 
      this.originalPageY = e.pageY, a.cursorAt && this._adjustOffsetFromHelper(a.cursorAt), 
      this.domPosition = {
        prev: this.currentItem.prev()[0],
        parent: this.currentItem.parent()[0]
      }, this.helper[0] !== this.currentItem[0] && this.currentItem.hide(), this._createPlaceholder(), 
      a.containment && this._setContainment(), a.cursor && "auto" !== a.cursor && (s = this.document.find("body"), 
      this.storedCursor = s.css("cursor"), s.css("cursor", a.cursor), this.storedStylesheet = t("<style>*{ cursor: " + a.cursor + " !important; }</style>").appendTo(s)), 
      a.opacity && (this.helper.css("opacity") && (this._storedOpacity = this.helper.css("opacity")), 
      this.helper.css("opacity", a.opacity)), a.zIndex && (this.helper.css("zIndex") && (this._storedZIndex = this.helper.css("zIndex")), 
      this.helper.css("zIndex", a.zIndex)), this.scrollParent[0] !== document && "HTML" !== this.scrollParent[0].tagName && (this.overflowOffset = this.scrollParent.offset()), 
      this._trigger("start", e, this._uiHash()), this._preserveHelperProportions || this._cacheHelperProportions(), 
      !n) for (o = this.containers.length - 1; o >= 0; o--) this.containers[o]._trigger("activate", e, this._uiHash(this));
      return t.ui.ddmanager && (t.ui.ddmanager.current = this), t.ui.ddmanager && !a.dropBehaviour && t.ui.ddmanager.prepareOffsets(this, e), 
      this.dragging = !0, this.helper.addClass("ui-sortable-helper"), this._mouseDrag(e), 
      !0;
    },
    _mouseDrag: function(e) {
      var i, n, o, s, a = this.options, r = !1;
      for (this.position = this._generatePosition(e), this.positionAbs = this._convertPositionTo("absolute"), 
      this.lastPositionAbs || (this.lastPositionAbs = this.positionAbs), this.options.scroll && (this.scrollParent[0] !== document && "HTML" !== this.scrollParent[0].tagName ? (this.overflowOffset.top + this.scrollParent[0].offsetHeight - e.pageY < a.scrollSensitivity ? this.scrollParent[0].scrollTop = r = this.scrollParent[0].scrollTop + a.scrollSpeed : e.pageY - this.overflowOffset.top < a.scrollSensitivity && (this.scrollParent[0].scrollTop = r = this.scrollParent[0].scrollTop - a.scrollSpeed), 
      this.overflowOffset.left + this.scrollParent[0].offsetWidth - e.pageX < a.scrollSensitivity ? this.scrollParent[0].scrollLeft = r = this.scrollParent[0].scrollLeft + a.scrollSpeed : e.pageX - this.overflowOffset.left < a.scrollSensitivity && (this.scrollParent[0].scrollLeft = r = this.scrollParent[0].scrollLeft - a.scrollSpeed)) : (e.pageY - t(document).scrollTop() < a.scrollSensitivity ? r = t(document).scrollTop(t(document).scrollTop() - a.scrollSpeed) : t(window).height() - (e.pageY - t(document).scrollTop()) < a.scrollSensitivity && (r = t(document).scrollTop(t(document).scrollTop() + a.scrollSpeed)), 
      e.pageX - t(document).scrollLeft() < a.scrollSensitivity ? r = t(document).scrollLeft(t(document).scrollLeft() - a.scrollSpeed) : t(window).width() - (e.pageX - t(document).scrollLeft()) < a.scrollSensitivity && (r = t(document).scrollLeft(t(document).scrollLeft() + a.scrollSpeed))), 
      !1 !== r && t.ui.ddmanager && !a.dropBehaviour && t.ui.ddmanager.prepareOffsets(this, e)), 
      this.positionAbs = this._convertPositionTo("absolute"), this.options.axis && "y" === this.options.axis || (this.helper[0].style.left = this.position.left + "px"), 
      this.options.axis && "x" === this.options.axis || (this.helper[0].style.top = this.position.top + "px"), 
      i = this.items.length - 1; i >= 0; i--) if (n = this.items[i], o = n.item[0], (s = this._intersectsWithPointer(n)) && n.instance === this.currentContainer && o !== this.currentItem[0] && this.placeholder[1 === s ? "next" : "prev"]()[0] !== o && !t.contains(this.placeholder[0], o) && ("semi-dynamic" !== this.options.type || !t.contains(this.element[0], o))) {
        if (this.direction = 1 === s ? "down" : "up", "pointer" !== this.options.tolerance && !this._intersectsWithSides(n)) break;
        this._rearrange(e, n), this._trigger("change", e, this._uiHash());
        break;
      }
      return this._contactContainers(e), t.ui.ddmanager && t.ui.ddmanager.drag(this, e), 
      this._trigger("sort", e, this._uiHash()), this.lastPositionAbs = this.positionAbs, 
      !1;
    },
    _mouseStop: function(e, i) {
      if (e) {
        if (t.ui.ddmanager && !this.options.dropBehaviour && t.ui.ddmanager.drop(this, e), 
        this.options.revert) {
          var n = this, o = this.placeholder.offset(), s = this.options.axis, a = {};
          s && "x" !== s || (a.left = o.left - this.offset.parent.left - this.margins.left + (this.offsetParent[0] === document.body ? 0 : this.offsetParent[0].scrollLeft)), 
          s && "y" !== s || (a.top = o.top - this.offset.parent.top - this.margins.top + (this.offsetParent[0] === document.body ? 0 : this.offsetParent[0].scrollTop)), 
          this.reverting = !0, t(this.helper).animate(a, parseInt(this.options.revert, 10) || 500, function() {
            n._clear(e);
          });
        } else this._clear(e, i);
        return !1;
      }
    },
    cancel: function() {
      if (this.dragging) {
        this._mouseUp({
          target: null
        }), "original" === this.options.helper ? this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper") : this.currentItem.show();
        for (var e = this.containers.length - 1; e >= 0; e--) this.containers[e]._trigger("deactivate", null, this._uiHash(this)), 
        this.containers[e].containerCache.over && (this.containers[e]._trigger("out", null, this._uiHash(this)), 
        this.containers[e].containerCache.over = 0);
      }
      return this.placeholder && (this.placeholder[0].parentNode && this.placeholder[0].parentNode.removeChild(this.placeholder[0]), 
      "original" !== this.options.helper && this.helper && this.helper[0].parentNode && this.helper.remove(), 
      t.extend(this, {
        helper: null,
        dragging: !1,
        reverting: !1,
        _noFinalSort: null
      }), this.domPosition.prev ? t(this.domPosition.prev).after(this.currentItem) : t(this.domPosition.parent).prepend(this.currentItem)), 
      this;
    },
    serialize: function(e) {
      var i = this._getItemsAsjQuery(e && e.connected), n = [];
      return e = e || {}, t(i).each(function() {
        var i = (t(e.item || this).attr(e.attribute || "id") || "").match(e.expression || /(.+)[\-=_](.+)/);
        i && n.push((e.key || i[1] + "[]") + "=" + (e.key && e.expression ? i[1] : i[2]));
      }), !n.length && e.key && n.push(e.key + "="), n.join("&");
    },
    toArray: function(e) {
      var i = this._getItemsAsjQuery(e && e.connected), n = [];
      return e = e || {}, i.each(function() {
        n.push(t(e.item || this).attr(e.attribute || "id") || "");
      }), n;
    },
    _intersectsWith: function(t) {
      var e = this.positionAbs.left, i = e + this.helperProportions.width, n = this.positionAbs.top, o = n + this.helperProportions.height, s = t.left, a = s + t.width, r = t.top, l = r + t.height, c = this.offset.click.top, d = this.offset.click.left, u = "x" === this.options.axis || n + c > r && l > n + c, h = "y" === this.options.axis || e + d > s && a > e + d, p = u && h;
      return "pointer" === this.options.tolerance || this.options.forcePointerForContainers || "pointer" !== this.options.tolerance && this.helperProportions[this.floating ? "width" : "height"] > t[this.floating ? "width" : "height"] ? p : e + this.helperProportions.width / 2 > s && a > i - this.helperProportions.width / 2 && n + this.helperProportions.height / 2 > r && l > o - this.helperProportions.height / 2;
    },
    _intersectsWithPointer: function(t) {
      var e = "x" === this.options.axis || this._isOverAxis(this.positionAbs.top + this.offset.click.top, t.top, t.height), i = "y" === this.options.axis || this._isOverAxis(this.positionAbs.left + this.offset.click.left, t.left, t.width), n = e && i, o = this._getDragVerticalDirection(), s = this._getDragHorizontalDirection();
      return !!n && (this.floating ? s && "right" === s || "down" === o ? 2 : 1 : o && ("down" === o ? 2 : 1));
    },
    _intersectsWithSides: function(t) {
      var e = this._isOverAxis(this.positionAbs.top + this.offset.click.top, t.top + t.height / 2, t.height), i = this._isOverAxis(this.positionAbs.left + this.offset.click.left, t.left + t.width / 2, t.width), n = this._getDragVerticalDirection(), o = this._getDragHorizontalDirection();
      return this.floating && o ? "right" === o && i || "left" === o && !i : n && ("down" === n && e || "up" === n && !e);
    },
    _getDragVerticalDirection: function() {
      var t = this.positionAbs.top - this.lastPositionAbs.top;
      return 0 !== t && (t > 0 ? "down" : "up");
    },
    _getDragHorizontalDirection: function() {
      var t = this.positionAbs.left - this.lastPositionAbs.left;
      return 0 !== t && (t > 0 ? "right" : "left");
    },
    refresh: function(t) {
      return this._refreshItems(t), this._setHandleClassName(), this.refreshPositions(), 
      this;
    },
    _connectWith: function() {
      var t = this.options;
      return t.connectWith.constructor === String ? [ t.connectWith ] : t.connectWith;
    },
    _getItemsAsjQuery: function(e) {
      var i, n, o, s, a = [], r = [], l = this._connectWith();
      if (l && e) for (i = l.length - 1; i >= 0; i--) for (o = t(l[i]), n = o.length - 1; n >= 0; n--) (s = t.data(o[n], this.widgetFullName)) && s !== this && !s.options.disabled && r.push([ t.isFunction(s.options.items) ? s.options.items.call(s.element) : t(s.options.items, s.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"), s ]);
      for (r.push([ t.isFunction(this.options.items) ? this.options.items.call(this.element, null, {
        options: this.options,
        item: this.currentItem
      }) : t(this.options.items, this.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"), this ]), 
      i = r.length - 1; i >= 0; i--) r[i][0].each(function() {
        a.push(this);
      });
      return t(a);
    },
    _removeCurrentsFromItems: function() {
      var e = this.currentItem.find(":data(" + this.widgetName + "-item)");
      this.items = t.grep(this.items, function(t) {
        for (var i = 0; e.length > i; i++) if (e[i] === t.item[0]) return !1;
        return !0;
      });
    },
    _refreshItems: function(e) {
      this.items = [], this.containers = [ this ];
      var i, n, o, s, a, r, l, c, d = this.items, u = [ [ t.isFunction(this.options.items) ? this.options.items.call(this.element[0], e, {
        item: this.currentItem
      }) : t(this.options.items, this.element), this ] ], h = this._connectWith();
      if (h && this.ready) for (i = h.length - 1; i >= 0; i--) for (o = t(h[i]), n = o.length - 1; n >= 0; n--) (s = t.data(o[n], this.widgetFullName)) && s !== this && !s.options.disabled && (u.push([ t.isFunction(s.options.items) ? s.options.items.call(s.element[0], e, {
        item: this.currentItem
      }) : t(s.options.items, s.element), s ]), this.containers.push(s));
      for (i = u.length - 1; i >= 0; i--) for (a = u[i][1], r = u[i][0], n = 0, c = r.length; c > n; n++) (l = t(r[n])).data(this.widgetName + "-item", a), 
      d.push({
        item: l,
        instance: a,
        width: 0,
        height: 0,
        left: 0,
        top: 0
      });
    },
    refreshPositions: function(e) {
      this.offsetParent && this.helper && (this.offset.parent = this._getParentOffset());
      var i, n, o, s;
      for (i = this.items.length - 1; i >= 0; i--) (n = this.items[i]).instance !== this.currentContainer && this.currentContainer && n.item[0] !== this.currentItem[0] || (o = this.options.toleranceElement ? t(this.options.toleranceElement, n.item) : n.item, 
      e || (n.width = o.outerWidth(), n.height = o.outerHeight()), s = o.offset(), n.left = s.left, 
      n.top = s.top);
      if (this.options.custom && this.options.custom.refreshContainers) this.options.custom.refreshContainers.call(this); else for (i = this.containers.length - 1; i >= 0; i--) s = this.containers[i].element.offset(), 
      this.containers[i].containerCache.left = s.left, this.containers[i].containerCache.top = s.top, 
      this.containers[i].containerCache.width = this.containers[i].element.outerWidth(), 
      this.containers[i].containerCache.height = this.containers[i].element.outerHeight();
      return this;
    },
    _createPlaceholder: function(e) {
      var i, n = (e = e || this).options;
      n.placeholder && n.placeholder.constructor !== String || (i = n.placeholder, n.placeholder = {
        element: function() {
          var n = e.currentItem[0].nodeName.toLowerCase(), o = t("<" + n + ">", e.document[0]).addClass(i || e.currentItem[0].className + " ui-sortable-placeholder").removeClass("ui-sortable-helper");
          return "tr" === n ? e.currentItem.children().each(function() {
            t("<td>&#160;</td>", e.document[0]).attr("colspan", t(this).attr("colspan") || 1).appendTo(o);
          }) : "img" === n && o.attr("src", e.currentItem.attr("src")), i || o.css("visibility", "hidden"), 
          o;
        },
        update: function(t, o) {
          (!i || n.forcePlaceholderSize) && (o.height() || o.height(e.currentItem.innerHeight() - parseInt(e.currentItem.css("paddingTop") || 0, 10) - parseInt(e.currentItem.css("paddingBottom") || 0, 10)), 
          o.width() || o.width(e.currentItem.innerWidth() - parseInt(e.currentItem.css("paddingLeft") || 0, 10) - parseInt(e.currentItem.css("paddingRight") || 0, 10)));
        }
      }), e.placeholder = t(n.placeholder.element.call(e.element, e.currentItem)), e.currentItem.after(e.placeholder), 
      n.placeholder.update(e, e.placeholder);
    },
    _contactContainers: function(e) {
      var i, n, o, s, a, r, l, c, d, u, h = null, p = null;
      for (i = this.containers.length - 1; i >= 0; i--) if (!t.contains(this.currentItem[0], this.containers[i].element[0])) if (this._intersectsWith(this.containers[i].containerCache)) {
        if (h && t.contains(this.containers[i].element[0], h.element[0])) continue;
        h = this.containers[i], p = i;
      } else this.containers[i].containerCache.over && (this.containers[i]._trigger("out", e, this._uiHash(this)), 
      this.containers[i].containerCache.over = 0);
      if (h) if (1 === this.containers.length) this.containers[p].containerCache.over || (this.containers[p]._trigger("over", e, this._uiHash(this)), 
      this.containers[p].containerCache.over = 1); else {
        for (o = 1e4, s = null, a = (d = h.floating || this._isFloating(this.currentItem)) ? "left" : "top", 
        r = d ? "width" : "height", u = d ? "clientX" : "clientY", n = this.items.length - 1; n >= 0; n--) t.contains(this.containers[p].element[0], this.items[n].item[0]) && this.items[n].item[0] !== this.currentItem[0] && (l = this.items[n].item.offset()[a], 
        c = !1, e[u] - l > this.items[n][r] / 2 && (c = !0), o > Math.abs(e[u] - l) && (o = Math.abs(e[u] - l), 
        s = this.items[n], this.direction = c ? "up" : "down"));
        if (!s && !this.options.dropOnEmpty) return;
        if (this.currentContainer === this.containers[p]) return void (this.currentContainer.containerCache.over || (this.containers[p]._trigger("over", e, this._uiHash()), 
        this.currentContainer.containerCache.over = 1));
        s ? this._rearrange(e, s, null, !0) : this._rearrange(e, null, this.containers[p].element, !0), 
        this._trigger("change", e, this._uiHash()), this.containers[p]._trigger("change", e, this._uiHash(this)), 
        this.currentContainer = this.containers[p], this.options.placeholder.update(this.currentContainer, this.placeholder), 
        this.containers[p]._trigger("over", e, this._uiHash(this)), this.containers[p].containerCache.over = 1;
      }
    },
    _createHelper: function(e) {
      var i = this.options, n = t.isFunction(i.helper) ? t(i.helper.apply(this.element[0], [ e, this.currentItem ])) : "clone" === i.helper ? this.currentItem.clone() : this.currentItem;
      return n.parents("body").length || t("parent" !== i.appendTo ? i.appendTo : this.currentItem[0].parentNode)[0].appendChild(n[0]), 
      n[0] === this.currentItem[0] && (this._storedCSS = {
        width: this.currentItem[0].style.width,
        height: this.currentItem[0].style.height,
        position: this.currentItem.css("position"),
        top: this.currentItem.css("top"),
        left: this.currentItem.css("left")
      }), (!n[0].style.width || i.forceHelperSize) && n.width(this.currentItem.width()), 
      (!n[0].style.height || i.forceHelperSize) && n.height(this.currentItem.height()), 
      n;
    },
    _adjustOffsetFromHelper: function(e) {
      "string" == typeof e && (e = e.split(" ")), t.isArray(e) && (e = {
        left: +e[0],
        top: +e[1] || 0
      }), "left" in e && (this.offset.click.left = e.left + this.margins.left), "right" in e && (this.offset.click.left = this.helperProportions.width - e.right + this.margins.left), 
      "top" in e && (this.offset.click.top = e.top + this.margins.top), "bottom" in e && (this.offset.click.top = this.helperProportions.height - e.bottom + this.margins.top);
    },
    _getParentOffset: function() {
      this.offsetParent = this.helper.offsetParent();
      var e = this.offsetParent.offset();
      return "absolute" === this.cssPosition && this.scrollParent[0] !== document && t.contains(this.scrollParent[0], this.offsetParent[0]) && (e.left += this.scrollParent.scrollLeft(), 
      e.top += this.scrollParent.scrollTop()), (this.offsetParent[0] === document.body || this.offsetParent[0].tagName && "html" === this.offsetParent[0].tagName.toLowerCase() && t.ui.ie) && (e = {
        top: 0,
        left: 0
      }), {
        top: e.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0),
        left: e.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0)
      };
    },
    _getRelativeOffset: function() {
      if ("relative" === this.cssPosition) {
        var t = this.currentItem.position();
        return {
          top: t.top - (parseInt(this.helper.css("top"), 10) || 0) + this.scrollParent.scrollTop(),
          left: t.left - (parseInt(this.helper.css("left"), 10) || 0) + this.scrollParent.scrollLeft()
        };
      }
      return {
        top: 0,
        left: 0
      };
    },
    _cacheMargins: function() {
      this.margins = {
        left: parseInt(this.currentItem.css("marginLeft"), 10) || 0,
        top: parseInt(this.currentItem.css("marginTop"), 10) || 0
      };
    },
    _cacheHelperProportions: function() {
      this.helperProportions = {
        width: this.helper.outerWidth(),
        height: this.helper.outerHeight()
      };
    },
    _setContainment: function() {
      var e, i, n, o = this.options;
      "parent" === o.containment && (o.containment = this.helper[0].parentNode), ("document" === o.containment || "window" === o.containment) && (this.containment = [ 0 - this.offset.relative.left - this.offset.parent.left, 0 - this.offset.relative.top - this.offset.parent.top, t("document" === o.containment ? document : window).width() - this.helperProportions.width - this.margins.left, (t("document" === o.containment ? document : window).height() || document.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top ]), 
      /^(document|window|parent)$/.test(o.containment) || (e = t(o.containment)[0], i = t(o.containment).offset(), 
      n = "hidden" !== t(e).css("overflow"), this.containment = [ i.left + (parseInt(t(e).css("borderLeftWidth"), 10) || 0) + (parseInt(t(e).css("paddingLeft"), 10) || 0) - this.margins.left, i.top + (parseInt(t(e).css("borderTopWidth"), 10) || 0) + (parseInt(t(e).css("paddingTop"), 10) || 0) - this.margins.top, i.left + (n ? Math.max(e.scrollWidth, e.offsetWidth) : e.offsetWidth) - (parseInt(t(e).css("borderLeftWidth"), 10) || 0) - (parseInt(t(e).css("paddingRight"), 10) || 0) - this.helperProportions.width - this.margins.left, i.top + (n ? Math.max(e.scrollHeight, e.offsetHeight) : e.offsetHeight) - (parseInt(t(e).css("borderTopWidth"), 10) || 0) - (parseInt(t(e).css("paddingBottom"), 10) || 0) - this.helperProportions.height - this.margins.top ]);
    },
    _convertPositionTo: function(e, i) {
      i || (i = this.position);
      var n = "absolute" === e ? 1 : -1, o = "absolute" !== this.cssPosition || this.scrollParent[0] !== document && t.contains(this.scrollParent[0], this.offsetParent[0]) ? this.scrollParent : this.offsetParent, s = /(html|body)/i.test(o[0].tagName);
      return {
        top: i.top + this.offset.relative.top * n + this.offset.parent.top * n - ("fixed" === this.cssPosition ? -this.scrollParent.scrollTop() : s ? 0 : o.scrollTop()) * n,
        left: i.left + this.offset.relative.left * n + this.offset.parent.left * n - ("fixed" === this.cssPosition ? -this.scrollParent.scrollLeft() : s ? 0 : o.scrollLeft()) * n
      };
    },
    _generatePosition: function(e) {
      var i, n, o = this.options, s = e.pageX, a = e.pageY, r = "absolute" !== this.cssPosition || this.scrollParent[0] !== document && t.contains(this.scrollParent[0], this.offsetParent[0]) ? this.scrollParent : this.offsetParent, l = /(html|body)/i.test(r[0].tagName);
      return "relative" !== this.cssPosition || this.scrollParent[0] !== document && this.scrollParent[0] !== this.offsetParent[0] || (this.offset.relative = this._getRelativeOffset()), 
      this.originalPosition && (this.containment && (e.pageX - this.offset.click.left < this.containment[0] && (s = this.containment[0] + this.offset.click.left), 
      e.pageY - this.offset.click.top < this.containment[1] && (a = this.containment[1] + this.offset.click.top), 
      e.pageX - this.offset.click.left > this.containment[2] && (s = this.containment[2] + this.offset.click.left), 
      e.pageY - this.offset.click.top > this.containment[3] && (a = this.containment[3] + this.offset.click.top)), 
      o.grid && (i = this.originalPageY + Math.round((a - this.originalPageY) / o.grid[1]) * o.grid[1], 
      a = this.containment ? i - this.offset.click.top >= this.containment[1] && i - this.offset.click.top <= this.containment[3] ? i : i - this.offset.click.top >= this.containment[1] ? i - o.grid[1] : i + o.grid[1] : i, 
      n = this.originalPageX + Math.round((s - this.originalPageX) / o.grid[0]) * o.grid[0], 
      s = this.containment ? n - this.offset.click.left >= this.containment[0] && n - this.offset.click.left <= this.containment[2] ? n : n - this.offset.click.left >= this.containment[0] ? n - o.grid[0] : n + o.grid[0] : n)), 
      {
        top: a - this.offset.click.top - this.offset.relative.top - this.offset.parent.top + ("fixed" === this.cssPosition ? -this.scrollParent.scrollTop() : l ? 0 : r.scrollTop()),
        left: s - this.offset.click.left - this.offset.relative.left - this.offset.parent.left + ("fixed" === this.cssPosition ? -this.scrollParent.scrollLeft() : l ? 0 : r.scrollLeft())
      };
    },
    _rearrange: function(t, e, i, n) {
      i ? i[0].appendChild(this.placeholder[0]) : e.item[0].parentNode.insertBefore(this.placeholder[0], "down" === this.direction ? e.item[0] : e.item[0].nextSibling), 
      this.counter = this.counter ? ++this.counter : 1;
      var o = this.counter;
      this._delay(function() {
        o === this.counter && this.refreshPositions(!n);
      });
    },
    _clear: function(t, e) {
      function i(t, e, i) {
        return function(n) {
          i._trigger(t, n, e._uiHash(e));
        };
      }
      this.reverting = !1;
      var n, o = [];
      if (!this._noFinalSort && this.currentItem.parent().length && this.placeholder.before(this.currentItem), 
      this._noFinalSort = null, this.helper[0] === this.currentItem[0]) {
        for (n in this._storedCSS) ("auto" === this._storedCSS[n] || "static" === this._storedCSS[n]) && (this._storedCSS[n] = "");
        this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper");
      } else this.currentItem.show();
      for (this.fromOutside && !e && o.push(function(t) {
        this._trigger("receive", t, this._uiHash(this.fromOutside));
      }), !this.fromOutside && this.domPosition.prev === this.currentItem.prev().not(".ui-sortable-helper")[0] && this.domPosition.parent === this.currentItem.parent()[0] || e || o.push(function(t) {
        this._trigger("update", t, this._uiHash());
      }), this !== this.currentContainer && (e || (o.push(function(t) {
        this._trigger("remove", t, this._uiHash());
      }), o.push(function(t) {
        return function(e) {
          t._trigger("receive", e, this._uiHash(this));
        };
      }.call(this, this.currentContainer)), o.push(function(t) {
        return function(e) {
          t._trigger("update", e, this._uiHash(this));
        };
      }.call(this, this.currentContainer)))), n = this.containers.length - 1; n >= 0; n--) e || o.push(i("deactivate", this, this.containers[n])), 
      this.containers[n].containerCache.over && (o.push(i("out", this, this.containers[n])), 
      this.containers[n].containerCache.over = 0);
      if (this.storedCursor && (this.document.find("body").css("cursor", this.storedCursor), 
      this.storedStylesheet.remove()), this._storedOpacity && this.helper.css("opacity", this._storedOpacity), 
      this._storedZIndex && this.helper.css("zIndex", "auto" === this._storedZIndex ? "" : this._storedZIndex), 
      this.dragging = !1, e || this._trigger("beforeStop", t, this._uiHash()), this.placeholder[0].parentNode.removeChild(this.placeholder[0]), 
      this.cancelHelperRemoval || (this.helper[0] !== this.currentItem[0] && this.helper.remove(), 
      this.helper = null), !e) {
        for (n = 0; o.length > n; n++) o[n].call(this, t);
        this._trigger("stop", t, this._uiHash());
      }
      return this.fromOutside = !1, !this.cancelHelperRemoval;
    },
    _trigger: function() {
      !1 === t.Widget.prototype._trigger.apply(this, arguments) && this.cancel();
    },
    _uiHash: function(e) {
      var i = e || this;
      return {
        helper: i.helper,
        placeholder: i.placeholder || t([]),
        position: i.position,
        originalPosition: i.originalPosition,
        offset: i.positionAbs,
        item: i.currentItem,
        sender: e ? e.element : null
      };
    }
  }), t.widget("ui.accordion", {
    version: "1.11.2",
    options: {
      active: 0,
      animate: {},
      collapsible: !1,
      event: "click",
      header: "> li > :first-child,> :not(li):even",
      heightStyle: "auto",
      icons: {
        activeHeader: "ui-icon-triangle-1-s",
        header: "ui-icon-triangle-1-e"
      },
      activate: null,
      beforeActivate: null
    },
    hideProps: {
      borderTopWidth: "hide",
      borderBottomWidth: "hide",
      paddingTop: "hide",
      paddingBottom: "hide",
      height: "hide"
    },
    showProps: {
      borderTopWidth: "show",
      borderBottomWidth: "show",
      paddingTop: "show",
      paddingBottom: "show",
      height: "show"
    },
    _create: function() {
      var e = this.options;
      this.prevShow = this.prevHide = t(), this.element.addClass("ui-accordion ui-widget ui-helper-reset").attr("role", "tablist"), 
      e.collapsible || !1 !== e.active && null != e.active || (e.active = 0), this._processPanels(), 
      0 > e.active && (e.active += this.headers.length), this._refresh();
    },
    _getCreateEventData: function() {
      return {
        header: this.active,
        panel: this.active.length ? this.active.next() : t()
      };
    },
    _createIcons: function() {
      var e = this.options.icons;
      e && (t("<span>").addClass("ui-accordion-header-icon ui-icon " + e.header).prependTo(this.headers), 
      this.active.children(".ui-accordion-header-icon").removeClass(e.header).addClass(e.activeHeader), 
      this.headers.addClass("ui-accordion-icons"));
    },
    _destroyIcons: function() {
      this.headers.removeClass("ui-accordion-icons").children(".ui-accordion-header-icon").remove();
    },
    _destroy: function() {
      var t;
      this.element.removeClass("ui-accordion ui-widget ui-helper-reset").removeAttr("role"), 
      this.headers.removeClass("ui-accordion-header ui-accordion-header-active ui-state-default ui-corner-all ui-state-active ui-state-disabled ui-corner-top").removeAttr("role").removeAttr("aria-expanded").removeAttr("aria-selected").removeAttr("aria-controls").removeAttr("tabIndex").removeUniqueId(), 
      this._destroyIcons(), t = this.headers.next().removeClass("ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content ui-accordion-content-active ui-state-disabled").css("display", "").removeAttr("role").removeAttr("aria-hidden").removeAttr("aria-labelledby").removeUniqueId(), 
      "content" !== this.options.heightStyle && t.css("height", "");
    },
    _setOption: function(t, e) {
      return "active" === t ? void this._activate(e) : ("event" === t && (this.options.event && this._off(this.headers, this.options.event), 
      this._setupEvents(e)), this._super(t, e), "collapsible" !== t || e || !1 !== this.options.active || this._activate(0), 
      "icons" === t && (this._destroyIcons(), e && this._createIcons()), void ("disabled" === t && (this.element.toggleClass("ui-state-disabled", !!e).attr("aria-disabled", e), 
      this.headers.add(this.headers.next()).toggleClass("ui-state-disabled", !!e))));
    },
    _keydown: function(e) {
      if (!e.altKey && !e.ctrlKey) {
        var i = t.ui.keyCode, n = this.headers.length, o = this.headers.index(e.target), s = !1;
        switch (e.keyCode) {
         case i.RIGHT:
         case i.DOWN:
          s = this.headers[(o + 1) % n];
          break;

         case i.LEFT:
         case i.UP:
          s = this.headers[(o - 1 + n) % n];
          break;

         case i.SPACE:
         case i.ENTER:
          this._eventHandler(e);
          break;

         case i.HOME:
          s = this.headers[0];
          break;

         case i.END:
          s = this.headers[n - 1];
        }
        s && (t(e.target).attr("tabIndex", -1), t(s).attr("tabIndex", 0), s.focus(), e.preventDefault());
      }
    },
    _panelKeyDown: function(e) {
      e.keyCode === t.ui.keyCode.UP && e.ctrlKey && t(e.currentTarget).prev().focus();
    },
    refresh: function() {
      var e = this.options;
      this._processPanels(), !1 === e.active && !0 === e.collapsible || !this.headers.length ? (e.active = !1, 
      this.active = t()) : !1 === e.active ? this._activate(0) : this.active.length && !t.contains(this.element[0], this.active[0]) ? this.headers.length === this.headers.find(".ui-state-disabled").length ? (e.active = !1, 
      this.active = t()) : this._activate(Math.max(0, e.active - 1)) : e.active = this.headers.index(this.active), 
      this._destroyIcons(), this._refresh();
    },
    _processPanels: function() {
      var t = this.headers, e = this.panels;
      this.headers = this.element.find(this.options.header).addClass("ui-accordion-header ui-state-default ui-corner-all"), 
      this.panels = this.headers.next().addClass("ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom").filter(":not(.ui-accordion-content-active)").hide(), 
      e && (this._off(t.not(this.headers)), this._off(e.not(this.panels)));
    },
    _refresh: function() {
      var e, i = this.options, n = i.heightStyle, o = this.element.parent();
      this.active = this._findActive(i.active).addClass("ui-accordion-header-active ui-state-active ui-corner-top").removeClass("ui-corner-all"), 
      this.active.next().addClass("ui-accordion-content-active").show(), this.headers.attr("role", "tab").each(function() {
        var e = t(this), i = e.uniqueId().attr("id"), n = e.next(), o = n.uniqueId().attr("id");
        e.attr("aria-controls", o), n.attr("aria-labelledby", i);
      }).next().attr("role", "tabpanel"), this.headers.not(this.active).attr({
        "aria-selected": "false",
        "aria-expanded": "false",
        tabIndex: -1
      }).next().attr({
        "aria-hidden": "true"
      }).hide(), this.active.length ? this.active.attr({
        "aria-selected": "true",
        "aria-expanded": "true",
        tabIndex: 0
      }).next().attr({
        "aria-hidden": "false"
      }) : this.headers.eq(0).attr("tabIndex", 0), this._createIcons(), this._setupEvents(i.event), 
      "fill" === n ? (e = o.height(), this.element.siblings(":visible").each(function() {
        var i = t(this), n = i.css("position");
        "absolute" !== n && "fixed" !== n && (e -= i.outerHeight(!0));
      }), this.headers.each(function() {
        e -= t(this).outerHeight(!0);
      }), this.headers.next().each(function() {
        t(this).height(Math.max(0, e - t(this).innerHeight() + t(this).height()));
      }).css("overflow", "auto")) : "auto" === n && (e = 0, this.headers.next().each(function() {
        e = Math.max(e, t(this).css("height", "").height());
      }).height(e));
    },
    _activate: function(e) {
      var i = this._findActive(e)[0];
      i !== this.active[0] && (i = i || this.active[0], this._eventHandler({
        target: i,
        currentTarget: i,
        preventDefault: t.noop
      }));
    },
    _findActive: function(e) {
      return "number" == typeof e ? this.headers.eq(e) : t();
    },
    _setupEvents: function(e) {
      var i = {
        keydown: "_keydown"
      };
      e && t.each(e.split(" "), function(t, e) {
        i[e] = "_eventHandler";
      }), this._off(this.headers.add(this.headers.next())), this._on(this.headers, i), 
      this._on(this.headers.next(), {
        keydown: "_panelKeyDown"
      }), this._hoverable(this.headers), this._focusable(this.headers);
    },
    _eventHandler: function(e) {
      var i = this.options, n = this.active, o = t(e.currentTarget), s = o[0] === n[0], a = s && i.collapsible, r = a ? t() : o.next(), l = {
        oldHeader: n,
        oldPanel: n.next(),
        newHeader: a ? t() : o,
        newPanel: r
      };
      e.preventDefault(), s && !i.collapsible || !1 === this._trigger("beforeActivate", e, l) || (i.active = !a && this.headers.index(o), 
      this.active = s ? t() : o, this._toggle(l), n.removeClass("ui-accordion-header-active ui-state-active"), 
      i.icons && n.children(".ui-accordion-header-icon").removeClass(i.icons.activeHeader).addClass(i.icons.header), 
      s || (o.removeClass("ui-corner-all").addClass("ui-accordion-header-active ui-state-active ui-corner-top"), 
      i.icons && o.children(".ui-accordion-header-icon").removeClass(i.icons.header).addClass(i.icons.activeHeader), 
      o.next().addClass("ui-accordion-content-active")));
    },
    _toggle: function(e) {
      var i = e.newPanel, n = this.prevShow.length ? this.prevShow : e.oldPanel;
      this.prevShow.add(this.prevHide).stop(!0, !0), this.prevShow = i, this.prevHide = n, 
      this.options.animate ? this._animate(i, n, e) : (n.hide(), i.show(), this._toggleComplete(e)), 
      n.attr({
        "aria-hidden": "true"
      }), n.prev().attr("aria-selected", "false"), i.length && n.length ? n.prev().attr({
        tabIndex: -1,
        "aria-expanded": "false"
      }) : i.length && this.headers.filter(function() {
        return 0 === t(this).attr("tabIndex");
      }).attr("tabIndex", -1), i.attr("aria-hidden", "false").prev().attr({
        "aria-selected": "true",
        tabIndex: 0,
        "aria-expanded": "true"
      });
    },
    _animate: function(t, e, i) {
      var n, o, s, a = this, r = 0, l = t.length && (!e.length || t.index() < e.index()), c = this.options.animate || {}, d = l && c.down || c, u = function() {
        a._toggleComplete(i);
      };
      return "number" == typeof d && (s = d), "string" == typeof d && (o = d), o = o || d.easing || c.easing, 
      s = s || d.duration || c.duration, e.length ? t.length ? (n = t.show().outerHeight(), 
      e.animate(this.hideProps, {
        duration: s,
        easing: o,
        step: function(t, e) {
          e.now = Math.round(t);
        }
      }), void t.hide().animate(this.showProps, {
        duration: s,
        easing: o,
        complete: u,
        step: function(t, i) {
          i.now = Math.round(t), "height" !== i.prop ? r += i.now : "content" !== a.options.heightStyle && (i.now = Math.round(n - e.outerHeight() - r), 
          r = 0);
        }
      })) : e.animate(this.hideProps, s, o, u) : t.animate(this.showProps, s, o, u);
    },
    _toggleComplete: function(t) {
      var e = t.oldPanel;
      e.removeClass("ui-accordion-content-active").prev().removeClass("ui-corner-top").addClass("ui-corner-all"), 
      e.length && (e.parent()[0].className = e.parent()[0].className), this._trigger("activate", null, t);
    }
  }), t.widget("ui.menu", {
    version: "1.11.2",
    defaultElement: "<ul>",
    delay: 300,
    options: {
      icons: {
        submenu: "ui-icon-carat-1-e"
      },
      items: "> *",
      menus: "ul",
      position: {
        my: "left-1 top",
        at: "right top"
      },
      role: "menu",
      blur: null,
      focus: null,
      select: null
    },
    _create: function() {
      this.activeMenu = this.element, this.mouseHandled = !1, this.element.uniqueId().addClass("ui-menu ui-widget ui-widget-content").toggleClass("ui-menu-icons", !!this.element.find(".ui-icon").length).attr({
        role: this.options.role,
        tabIndex: 0
      }), this.options.disabled && this.element.addClass("ui-state-disabled").attr("aria-disabled", "true"), 
      this._on({
        "mousedown .ui-menu-item": function(t) {
          t.preventDefault();
        },
        "click .ui-menu-item": function(e) {
          var i = t(e.target);
          !this.mouseHandled && i.not(".ui-state-disabled").length && (this.select(e), e.isPropagationStopped() || (this.mouseHandled = !0), 
          i.has(".ui-menu").length ? this.expand(e) : !this.element.is(":focus") && t(this.document[0].activeElement).closest(".ui-menu").length && (this.element.trigger("focus", [ !0 ]), 
          this.active && 1 === this.active.parents(".ui-menu").length && clearTimeout(this.timer)));
        },
        "mouseenter .ui-menu-item": function(e) {
          if (!this.previousFilter) {
            var i = t(e.currentTarget);
            i.siblings(".ui-state-active").removeClass("ui-state-active"), this.focus(e, i);
          }
        },
        mouseleave: "collapseAll",
        "mouseleave .ui-menu": "collapseAll",
        focus: function(t, e) {
          var i = this.active || this.element.find(this.options.items).eq(0);
          e || this.focus(t, i);
        },
        blur: function(e) {
          this._delay(function() {
            t.contains(this.element[0], this.document[0].activeElement) || this.collapseAll(e);
          });
        },
        keydown: "_keydown"
      }), this.refresh(), this._on(this.document, {
        click: function(t) {
          this._closeOnDocumentClick(t) && this.collapseAll(t), this.mouseHandled = !1;
        }
      });
    },
    _destroy: function() {
      this.element.removeAttr("aria-activedescendant").find(".ui-menu").addBack().removeClass("ui-menu ui-widget ui-widget-content ui-menu-icons ui-front").removeAttr("role").removeAttr("tabIndex").removeAttr("aria-labelledby").removeAttr("aria-expanded").removeAttr("aria-hidden").removeAttr("aria-disabled").removeUniqueId().show(), 
      this.element.find(".ui-menu-item").removeClass("ui-menu-item").removeAttr("role").removeAttr("aria-disabled").removeUniqueId().removeClass("ui-state-hover").removeAttr("tabIndex").removeAttr("role").removeAttr("aria-haspopup").children().each(function() {
        var e = t(this);
        e.data("ui-menu-submenu-carat") && e.remove();
      }), this.element.find(".ui-menu-divider").removeClass("ui-menu-divider ui-widget-content");
    },
    _keydown: function(e) {
      var i, n, o, s, a = !0;
      switch (e.keyCode) {
       case t.ui.keyCode.PAGE_UP:
        this.previousPage(e);
        break;

       case t.ui.keyCode.PAGE_DOWN:
        this.nextPage(e);
        break;

       case t.ui.keyCode.HOME:
        this._move("first", "first", e);
        break;

       case t.ui.keyCode.END:
        this._move("last", "last", e);
        break;

       case t.ui.keyCode.UP:
        this.previous(e);
        break;

       case t.ui.keyCode.DOWN:
        this.next(e);
        break;

       case t.ui.keyCode.LEFT:
        this.collapse(e);
        break;

       case t.ui.keyCode.RIGHT:
        this.active && !this.active.is(".ui-state-disabled") && this.expand(e);
        break;

       case t.ui.keyCode.ENTER:
       case t.ui.keyCode.SPACE:
        this._activate(e);
        break;

       case t.ui.keyCode.ESCAPE:
        this.collapse(e);
        break;

       default:
        a = !1, n = this.previousFilter || "", o = String.fromCharCode(e.keyCode), s = !1, 
        clearTimeout(this.filterTimer), o === n ? s = !0 : o = n + o, i = this._filterMenuItems(o), 
        (i = s && -1 !== i.index(this.active.next()) ? this.active.nextAll(".ui-menu-item") : i).length || (o = String.fromCharCode(e.keyCode), 
        i = this._filterMenuItems(o)), i.length ? (this.focus(e, i), this.previousFilter = o, 
        this.filterTimer = this._delay(function() {
          delete this.previousFilter;
        }, 1e3)) : delete this.previousFilter;
      }
      a && e.preventDefault();
    },
    _activate: function(t) {
      this.active.is(".ui-state-disabled") || (this.active.is("[aria-haspopup='true']") ? this.expand(t) : this.select(t));
    },
    refresh: function() {
      var e, i = this, n = this.options.icons.submenu, o = this.element.find(this.options.menus);
      this.element.toggleClass("ui-menu-icons", !!this.element.find(".ui-icon").length), 
      o.filter(":not(.ui-menu)").addClass("ui-menu ui-widget ui-widget-content ui-front").hide().attr({
        role: this.options.role,
        "aria-hidden": "true",
        "aria-expanded": "false"
      }).each(function() {
        var e = t(this), i = e.parent(), o = t("<span>").addClass("ui-menu-icon ui-icon " + n).data("ui-menu-submenu-carat", !0);
        i.attr("aria-haspopup", "true").prepend(o), e.attr("aria-labelledby", i.attr("id"));
      }), (e = o.add(this.element).find(this.options.items)).not(".ui-menu-item").each(function() {
        var e = t(this);
        i._isDivider(e) && e.addClass("ui-widget-content ui-menu-divider");
      }), e.not(".ui-menu-item, .ui-menu-divider").addClass("ui-menu-item").uniqueId().attr({
        tabIndex: -1,
        role: this._itemRole()
      }), e.filter(".ui-state-disabled").attr("aria-disabled", "true"), this.active && !t.contains(this.element[0], this.active[0]) && this.blur();
    },
    _itemRole: function() {
      return {
        menu: "menuitem",
        listbox: "option"
      }[this.options.role];
    },
    _setOption: function(t, e) {
      "icons" === t && this.element.find(".ui-menu-icon").removeClass(this.options.icons.submenu).addClass(e.submenu), 
      "disabled" === t && this.element.toggleClass("ui-state-disabled", !!e).attr("aria-disabled", e), 
      this._super(t, e);
    },
    focus: function(t, e) {
      var i, n;
      this.blur(t, t && "focus" === t.type), this._scrollIntoView(e), this.active = e.first(), 
      n = this.active.addClass("ui-state-focus").removeClass("ui-state-active"), this.options.role && this.element.attr("aria-activedescendant", n.attr("id")), 
      this.active.parent().closest(".ui-menu-item").addClass("ui-state-active"), t && "keydown" === t.type ? this._close() : this.timer = this._delay(function() {
        this._close();
      }, this.delay), (i = e.children(".ui-menu")).length && t && /^mouse/.test(t.type) && this._startOpening(i), 
      this.activeMenu = e.parent(), this._trigger("focus", t, {
        item: e
      });
    },
    _scrollIntoView: function(e) {
      var i, n, o, s, a, r;
      this._hasScroll() && (i = parseFloat(t.css(this.activeMenu[0], "borderTopWidth")) || 0, 
      n = parseFloat(t.css(this.activeMenu[0], "paddingTop")) || 0, o = e.offset().top - this.activeMenu.offset().top - i - n, 
      s = this.activeMenu.scrollTop(), a = this.activeMenu.height(), r = e.outerHeight(), 
      0 > o ? this.activeMenu.scrollTop(s + o) : o + r > a && this.activeMenu.scrollTop(s + o - a + r));
    },
    blur: function(t, e) {
      e || clearTimeout(this.timer), this.active && (this.active.removeClass("ui-state-focus"), 
      this.active = null, this._trigger("blur", t, {
        item: this.active
      }));
    },
    _startOpening: function(t) {
      clearTimeout(this.timer), "true" === t.attr("aria-hidden") && (this.timer = this._delay(function() {
        this._close(), this._open(t);
      }, this.delay));
    },
    _open: function(e) {
      var i = t.extend({
        of: this.active
      }, this.options.position);
      clearTimeout(this.timer), this.element.find(".ui-menu").not(e.parents(".ui-menu")).hide().attr("aria-hidden", "true"), 
      e.show().removeAttr("aria-hidden").attr("aria-expanded", "true").position(i);
    },
    collapseAll: function(e, i) {
      clearTimeout(this.timer), this.timer = this._delay(function() {
        var n = i ? this.element : t(e && e.target).closest(this.element.find(".ui-menu"));
        n.length || (n = this.element), this._close(n), this.blur(e), this.activeMenu = n;
      }, this.delay);
    },
    _close: function(t) {
      t || (t = this.active ? this.active.parent() : this.element), t.find(".ui-menu").hide().attr("aria-hidden", "true").attr("aria-expanded", "false").end().find(".ui-state-active").not(".ui-state-focus").removeClass("ui-state-active");
    },
    _closeOnDocumentClick: function(e) {
      return !t(e.target).closest(".ui-menu").length;
    },
    _isDivider: function(t) {
      return !/[^\-\u2014\u2013\s]/.test(t.text());
    },
    collapse: function(t) {
      var e = this.active && this.active.parent().closest(".ui-menu-item", this.element);
      e && e.length && (this._close(), this.focus(t, e));
    },
    expand: function(t) {
      var e = this.active && this.active.children(".ui-menu ").find(this.options.items).first();
      e && e.length && (this._open(e.parent()), this._delay(function() {
        this.focus(t, e);
      }));
    },
    next: function(t) {
      this._move("next", "first", t);
    },
    previous: function(t) {
      this._move("prev", "last", t);
    },
    isFirstItem: function() {
      return this.active && !this.active.prevAll(".ui-menu-item").length;
    },
    isLastItem: function() {
      return this.active && !this.active.nextAll(".ui-menu-item").length;
    },
    _move: function(t, e, i) {
      var n;
      this.active && (n = "first" === t || "last" === t ? this.active["first" === t ? "prevAll" : "nextAll"](".ui-menu-item").eq(-1) : this.active[t + "All"](".ui-menu-item").eq(0)), 
      n && n.length && this.active || (n = this.activeMenu.find(this.options.items)[e]()), 
      this.focus(i, n);
    },
    nextPage: function(e) {
      var i, n, o;
      return this.active ? void (this.isLastItem() || (this._hasScroll() ? (n = this.active.offset().top, 
      o = this.element.height(), this.active.nextAll(".ui-menu-item").each(function() {
        return 0 > (i = t(this)).offset().top - n - o;
      }), this.focus(e, i)) : this.focus(e, this.activeMenu.find(this.options.items)[this.active ? "last" : "first"]()))) : void this.next(e);
    },
    previousPage: function(e) {
      var i, n, o;
      return this.active ? void (this.isFirstItem() || (this._hasScroll() ? (n = this.active.offset().top, 
      o = this.element.height(), this.active.prevAll(".ui-menu-item").each(function() {
        return (i = t(this)).offset().top - n + o > 0;
      }), this.focus(e, i)) : this.focus(e, this.activeMenu.find(this.options.items).first()))) : void this.next(e);
    },
    _hasScroll: function() {
      return this.element.outerHeight() < this.element.prop("scrollHeight");
    },
    select: function(e) {
      this.active = this.active || t(e.target).closest(".ui-menu-item");
      var i = {
        item: this.active
      };
      this.active.has(".ui-menu").length || this.collapseAll(e, !0), this._trigger("select", e, i);
    },
    _filterMenuItems: function(e) {
      var i = e.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&"), n = RegExp("^" + i, "i");
      return this.activeMenu.find(this.options.items).filter(".ui-menu-item").filter(function() {
        return n.test(t.trim(t(this).text()));
      });
    }
  }), t.widget("ui.autocomplete", {
    version: "1.11.2",
    defaultElement: "<input>",
    options: {
      appendTo: null,
      autoFocus: !1,
      delay: 300,
      minLength: 1,
      position: {
        my: "left top",
        at: "left bottom",
        collision: "none"
      },
      source: null,
      change: null,
      close: null,
      focus: null,
      open: null,
      response: null,
      search: null,
      select: null
    },
    requestIndex: 0,
    pending: 0,
    _create: function() {
      var e, i, n, o = this.element[0].nodeName.toLowerCase(), s = "textarea" === o, a = "input" === o;
      this.isMultiLine = !!s || !a && this.element.prop("isContentEditable"), this.valueMethod = this.element[s || a ? "val" : "text"], 
      this.isNewMenu = !0, this.element.addClass("ui-autocomplete-input").attr("autocomplete", "off"), 
      this._on(this.element, {
        keydown: function(o) {
          if (this.element.prop("readOnly")) return e = !0, n = !0, void (i = !0);
          e = !1, n = !1, i = !1;
          var s = t.ui.keyCode;
          switch (o.keyCode) {
           case s.PAGE_UP:
            e = !0, this._move("previousPage", o);
            break;

           case s.PAGE_DOWN:
            e = !0, this._move("nextPage", o);
            break;

           case s.UP:
            e = !0, this._keyEvent("previous", o);
            break;

           case s.DOWN:
            e = !0, this._keyEvent("next", o);
            break;

           case s.ENTER:
            this.menu.active && (e = !0, o.preventDefault(), this.menu.select(o));
            break;

           case s.TAB:
            this.menu.active && this.menu.select(o);
            break;

           case s.ESCAPE:
            this.menu.element.is(":visible") && (this.isMultiLine || this._value(this.term), 
            this.close(o), o.preventDefault());
            break;

           default:
            i = !0, this._searchTimeout(o);
          }
        },
        keypress: function(n) {
          if (e) return e = !1, void ((!this.isMultiLine || this.menu.element.is(":visible")) && n.preventDefault());
          if (!i) {
            var o = t.ui.keyCode;
            switch (n.keyCode) {
             case o.PAGE_UP:
              this._move("previousPage", n);
              break;

             case o.PAGE_DOWN:
              this._move("nextPage", n);
              break;

             case o.UP:
              this._keyEvent("previous", n);
              break;

             case o.DOWN:
              this._keyEvent("next", n);
            }
          }
        },
        input: function(t) {
          return n ? (n = !1, void t.preventDefault()) : void this._searchTimeout(t);
        },
        focus: function() {
          this.selectedItem = null, this.previous = this._value();
        },
        blur: function(t) {
          return this.cancelBlur ? void delete this.cancelBlur : (clearTimeout(this.searching), 
          this.close(t), void this._change(t));
        }
      }), this._initSource(), this.menu = t("<ul>").addClass("ui-autocomplete ui-front").appendTo(this._appendTo()).menu({
        role: null
      }).hide().menu("instance"), this._on(this.menu.element, {
        mousedown: function(e) {
          e.preventDefault(), this.cancelBlur = !0, this._delay(function() {
            delete this.cancelBlur;
          });
          var i = this.menu.element[0];
          t(e.target).closest(".ui-menu-item").length || this._delay(function() {
            var e = this;
            this.document.one("mousedown", function(n) {
              n.target === e.element[0] || n.target === i || t.contains(i, n.target) || e.close();
            });
          });
        },
        menufocus: function(e, i) {
          var n, o;
          return this.isNewMenu && (this.isNewMenu = !1, e.originalEvent && /^mouse/.test(e.originalEvent.type)) ? (this.menu.blur(), 
          void this.document.one("mousemove", function() {
            t(e.target).trigger(e.originalEvent);
          })) : (o = i.item.data("ui-autocomplete-item"), !1 !== this._trigger("focus", e, {
            item: o
          }) && e.originalEvent && /^key/.test(e.originalEvent.type) && this._value(o.value), 
          void ((n = i.item.attr("aria-label") || o.value) && t.trim(n).length && (this.liveRegion.children().hide(), 
          t("<div>").text(n).appendTo(this.liveRegion))));
        },
        menuselect: function(t, e) {
          var i = e.item.data("ui-autocomplete-item"), n = this.previous;
          this.element[0] !== this.document[0].activeElement && (this.element.focus(), this.previous = n, 
          this._delay(function() {
            this.previous = n, this.selectedItem = i;
          })), !1 !== this._trigger("select", t, {
            item: i
          }) && this._value(i.value), this.term = this._value(), this.close(t), this.selectedItem = i;
        }
      }), this.liveRegion = t("<span>", {
        role: "status",
        "aria-live": "assertive",
        "aria-relevant": "additions"
      }).addClass("ui-helper-hidden-accessible").appendTo(this.document[0].body), this._on(this.window, {
        beforeunload: function() {
          this.element.removeAttr("autocomplete");
        }
      });
    },
    _destroy: function() {
      clearTimeout(this.searching), this.element.removeClass("ui-autocomplete-input").removeAttr("autocomplete"), 
      this.menu.element.remove(), this.liveRegion.remove();
    },
    _setOption: function(t, e) {
      this._super(t, e), "source" === t && this._initSource(), "appendTo" === t && this.menu.element.appendTo(this._appendTo()), 
      "disabled" === t && e && this.xhr && this.xhr.abort();
    },
    _appendTo: function() {
      var e = this.options.appendTo;
      return e && (e = e.jquery || e.nodeType ? t(e) : this.document.find(e).eq(0)), e && e[0] || (e = this.element.closest(".ui-front")), 
      e.length || (e = this.document[0].body), e;
    },
    _initSource: function() {
      var e, i, n = this;
      t.isArray(this.options.source) ? (e = this.options.source, this.source = function(i, n) {
        n(t.ui.autocomplete.filter(e, i.term));
      }) : "string" == typeof this.options.source ? (i = this.options.source, this.source = function(e, o) {
        n.xhr && n.xhr.abort(), n.xhr = t.ajax({
          url: i,
          data: e,
          dataType: "json",
          success: function(t) {
            o(t);
          },
          error: function() {
            o([]);
          }
        });
      }) : this.source = this.options.source;
    },
    _searchTimeout: function(t) {
      clearTimeout(this.searching), this.searching = this._delay(function() {
        var e = this.term === this._value(), i = this.menu.element.is(":visible"), n = t.altKey || t.ctrlKey || t.metaKey || t.shiftKey;
        (!e || e && !i && !n) && (this.selectedItem = null, this.search(null, t));
      }, this.options.delay);
    },
    search: function(t, e) {
      return t = null != t ? t : this._value(), this.term = this._value(), t.length < this.options.minLength ? this.close(e) : !1 !== this._trigger("search", e) ? this._search(t) : void 0;
    },
    _search: function(t) {
      this.pending++, this.element.addClass("ui-autocomplete-loading"), this.cancelSearch = !1, 
      this.source({
        term: t
      }, this._response());
    },
    _response: function() {
      var e = ++this.requestIndex;
      return t.proxy(function(t) {
        e === this.requestIndex && this.__response(t), --this.pending || this.element.removeClass("ui-autocomplete-loading");
      }, this);
    },
    __response: function(t) {
      t && (t = this._normalize(t)), this._trigger("response", null, {
        content: t
      }), !this.options.disabled && t && t.length && !this.cancelSearch ? (this._suggest(t), 
      this._trigger("open")) : this._close();
    },
    close: function(t) {
      this.cancelSearch = !0, this._close(t);
    },
    _close: function(t) {
      this.menu.element.is(":visible") && (this.menu.element.hide(), this.menu.blur(), 
      this.isNewMenu = !0, this._trigger("close", t));
    },
    _change: function(t) {
      this.previous !== this._value() && this._trigger("change", t, {
        item: this.selectedItem
      });
    },
    _normalize: function(e) {
      return e.length && e[0].label && e[0].value ? e : t.map(e, function(e) {
        return "string" == typeof e ? {
          label: e,
          value: e
        } : t.extend({}, e, {
          label: e.label || e.value,
          value: e.value || e.label
        });
      });
    },
    _suggest: function(e) {
      var i = this.menu.element.empty();
      this._renderMenu(i, e), this.isNewMenu = !0, this.menu.refresh(), i.show(), this._resizeMenu(), 
      i.position(t.extend({
        of: this.element
      }, this.options.position)), this.options.autoFocus && this.menu.next();
    },
    _resizeMenu: function() {
      var t = this.menu.element;
      t.outerWidth(Math.max(t.width("").outerWidth() + 1, this.element.outerWidth()));
    },
    _renderMenu: function(e, i) {
      var n = this;
      t.each(i, function(t, i) {
        n._renderItemData(e, i);
      });
    },
    _renderItemData: function(t, e) {
      return this._renderItem(t, e).data("ui-autocomplete-item", e);
    },
    _renderItem: function(e, i) {
      return t("<li>").text(i.label).appendTo(e);
    },
    _move: function(t, e) {
      return this.menu.element.is(":visible") ? this.menu.isFirstItem() && /^previous/.test(t) || this.menu.isLastItem() && /^next/.test(t) ? (this.isMultiLine || this._value(this.term), 
      void this.menu.blur()) : void this.menu[t](e) : void this.search(null, e);
    },
    widget: function() {
      return this.menu.element;
    },
    _value: function() {
      return this.valueMethod.apply(this.element, arguments);
    },
    _keyEvent: function(t, e) {
      (!this.isMultiLine || this.menu.element.is(":visible")) && (this._move(t, e), e.preventDefault());
    }
  }), t.extend(t.ui.autocomplete, {
    escapeRegex: function(t) {
      return t.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&");
    },
    filter: function(e, i) {
      var n = RegExp(t.ui.autocomplete.escapeRegex(i), "i");
      return t.grep(e, function(t) {
        return n.test(t.label || t.value || t);
      });
    }
  }), t.widget("ui.autocomplete", t.ui.autocomplete, {
    options: {
      messages: {
        noResults: "No search results.",
        results: function(t) {
          return t + (t > 1 ? " results are" : " result is") + " available, use up and down arrow keys to navigate.";
        }
      }
    },
    __response: function(e) {
      var i;
      this._superApply(arguments), this.options.disabled || this.cancelSearch || (i = e && e.length ? this.options.messages.results(e.length) : this.options.messages.noResults, 
      this.liveRegion.children().hide(), t("<div>").text(i).appendTo(this.liveRegion));
    }
  }), t.ui.autocomplete;
  var h, p = "ui-button ui-widget ui-state-default ui-corner-all", f = "ui-button-icons-only ui-button-icon-only ui-button-text-icons ui-button-text-icon-primary ui-button-text-icon-secondary ui-button-text-only", m = function() {
    var e = t(this);
    setTimeout(function() {
      e.find(":ui-button").button("refresh");
    }, 1);
  }, g = function(e) {
    var i = e.name, n = e.form, o = t([]);
    return i && (i = i.replace(/'/g, "\\'"), o = n ? t(n).find("[name='" + i + "'][type=radio]") : t("[name='" + i + "'][type=radio]", e.ownerDocument).filter(function() {
      return !this.form;
    })), o;
  };
  t.widget("ui.button", {
    version: "1.11.2",
    defaultElement: "<button>",
    options: {
      disabled: null,
      text: !0,
      label: null,
      icons: {
        primary: null,
        secondary: null
      }
    },
    _create: function() {
      this.element.closest("form").unbind("reset" + this.eventNamespace).bind("reset" + this.eventNamespace, m), 
      "boolean" != typeof this.options.disabled ? this.options.disabled = !!this.element.prop("disabled") : this.element.prop("disabled", this.options.disabled), 
      this._determineButtonType(), this.hasTitle = !!this.buttonElement.attr("title");
      var e = this, i = this.options, n = "checkbox" === this.type || "radio" === this.type, o = n ? "" : "ui-state-active";
      null === i.label && (i.label = "input" === this.type ? this.buttonElement.val() : this.buttonElement.html()), 
      this._hoverable(this.buttonElement), this.buttonElement.addClass(p).attr("role", "button").bind("mouseenter" + this.eventNamespace, function() {
        i.disabled || this === h && t(this).addClass("ui-state-active");
      }).bind("mouseleave" + this.eventNamespace, function() {
        i.disabled || t(this).removeClass(o);
      }).bind("click" + this.eventNamespace, function(t) {
        i.disabled && (t.preventDefault(), t.stopImmediatePropagation());
      }), this._on({
        focus: function() {
          this.buttonElement.addClass("ui-state-focus");
        },
        blur: function() {
          this.buttonElement.removeClass("ui-state-focus");
        }
      }), n && this.element.bind("change" + this.eventNamespace, function() {
        e.refresh();
      }), "checkbox" === this.type ? this.buttonElement.bind("click" + this.eventNamespace, function() {
        return !i.disabled && void 0;
      }) : "radio" === this.type ? this.buttonElement.bind("click" + this.eventNamespace, function() {
        if (i.disabled) return !1;
        t(this).addClass("ui-state-active"), e.buttonElement.attr("aria-pressed", "true");
        var n = e.element[0];
        g(n).not(n).map(function() {
          return t(this).button("widget")[0];
        }).removeClass("ui-state-active").attr("aria-pressed", "false");
      }) : (this.buttonElement.bind("mousedown" + this.eventNamespace, function() {
        return !i.disabled && (t(this).addClass("ui-state-active"), h = this, void e.document.one("mouseup", function() {
          h = null;
        }));
      }).bind("mouseup" + this.eventNamespace, function() {
        return !i.disabled && void t(this).removeClass("ui-state-active");
      }).bind("keydown" + this.eventNamespace, function(e) {
        return !i.disabled && void ((e.keyCode === t.ui.keyCode.SPACE || e.keyCode === t.ui.keyCode.ENTER) && t(this).addClass("ui-state-active"));
      }).bind("keyup" + this.eventNamespace + " blur" + this.eventNamespace, function() {
        t(this).removeClass("ui-state-active");
      }), this.buttonElement.is("a") && this.buttonElement.keyup(function(e) {
        e.keyCode === t.ui.keyCode.SPACE && t(this).click();
      })), this._setOption("disabled", i.disabled), this._resetButton();
    },
    _determineButtonType: function() {
      var t, e, i;
      this.type = this.element.is("[type=checkbox]") ? "checkbox" : this.element.is("[type=radio]") ? "radio" : this.element.is("input") ? "input" : "button", 
      "checkbox" === this.type || "radio" === this.type ? (t = this.element.parents().last(), 
      e = "label[for='" + this.element.attr("id") + "']", this.buttonElement = t.find(e), 
      this.buttonElement.length || (t = t.length ? t.siblings() : this.element.siblings(), 
      this.buttonElement = t.filter(e), this.buttonElement.length || (this.buttonElement = t.find(e))), 
      this.element.addClass("ui-helper-hidden-accessible"), (i = this.element.is(":checked")) && this.buttonElement.addClass("ui-state-active"), 
      this.buttonElement.prop("aria-pressed", i)) : this.buttonElement = this.element;
    },
    widget: function() {
      return this.buttonElement;
    },
    _destroy: function() {
      this.element.removeClass("ui-helper-hidden-accessible"), this.buttonElement.removeClass(p + " ui-state-active " + f).removeAttr("role").removeAttr("aria-pressed").html(this.buttonElement.find(".ui-button-text").html()), 
      this.hasTitle || this.buttonElement.removeAttr("title");
    },
    _setOption: function(t, e) {
      return this._super(t, e), "disabled" === t ? (this.widget().toggleClass("ui-state-disabled", !!e), 
      this.element.prop("disabled", !!e), void (e && ("checkbox" === this.type || "radio" === this.type ? this.buttonElement.removeClass("ui-state-focus") : this.buttonElement.removeClass("ui-state-focus ui-state-active")))) : void this._resetButton();
    },
    refresh: function() {
      var e = this.element.is("input, button") ? this.element.is(":disabled") : this.element.hasClass("ui-button-disabled");
      e !== this.options.disabled && this._setOption("disabled", e), "radio" === this.type ? g(this.element[0]).each(function() {
        t(this).is(":checked") ? t(this).button("widget").addClass("ui-state-active").attr("aria-pressed", "true") : t(this).button("widget").removeClass("ui-state-active").attr("aria-pressed", "false");
      }) : "checkbox" === this.type && (this.element.is(":checked") ? this.buttonElement.addClass("ui-state-active").attr("aria-pressed", "true") : this.buttonElement.removeClass("ui-state-active").attr("aria-pressed", "false"));
    },
    _resetButton: function() {
      if ("input" !== this.type) {
        var e = this.buttonElement.removeClass(f), i = t("<span></span>", this.document[0]).addClass("ui-button-text").html(this.options.label).appendTo(e.empty()).text(), n = this.options.icons, o = n.primary && n.secondary, s = [];
        n.primary || n.secondary ? (this.options.text && s.push("ui-button-text-icon" + (o ? "s" : n.primary ? "-primary" : "-secondary")), 
        n.primary && e.prepend("<span class='ui-button-icon-primary ui-icon " + n.primary + "'></span>"), 
        n.secondary && e.append("<span class='ui-button-icon-secondary ui-icon " + n.secondary + "'></span>"), 
        this.options.text || (s.push(o ? "ui-button-icons-only" : "ui-button-icon-only"), 
        this.hasTitle || e.attr("title", t.trim(i)))) : s.push("ui-button-text-only"), e.addClass(s.join(" "));
      } else this.options.label && this.element.val(this.options.label);
    }
  }), t.widget("ui.buttonset", {
    version: "1.11.2",
    options: {
      items: "button, input[type=button], input[type=submit], input[type=reset], input[type=checkbox], input[type=radio], a, :data(ui-button)"
    },
    _create: function() {
      this.element.addClass("ui-buttonset");
    },
    _init: function() {
      this.refresh();
    },
    _setOption: function(t, e) {
      "disabled" === t && this.buttons.button("option", t, e), this._super(t, e);
    },
    refresh: function() {
      var e = "rtl" === this.element.css("direction"), i = this.element.find(this.options.items), n = i.filter(":ui-button");
      i.not(":ui-button").button(), n.button("refresh"), this.buttons = i.map(function() {
        return t(this).button("widget")[0];
      }).removeClass("ui-corner-all ui-corner-left ui-corner-right").filter(":first").addClass(e ? "ui-corner-right" : "ui-corner-left").end().filter(":last").addClass(e ? "ui-corner-left" : "ui-corner-right").end().end();
    },
    _destroy: function() {
      this.element.removeClass("ui-buttonset"), this.buttons.map(function() {
        return t(this).button("widget")[0];
      }).removeClass("ui-corner-left ui-corner-right").end().button("destroy");
    }
  }), t.ui.button, t.extend(t.ui, {
    datepicker: {
      version: "1.11.2"
    }
  });
  var v;
  t.extend(o.prototype, {
    markerClassName: "hasDatepicker",
    maxRows: 4,
    _widgetDatepicker: function() {
      return this.dpDiv;
    },
    setDefaults: function(t) {
      return r(this._defaults, t || {}), this;
    },
    _attachDatepicker: function(e, i) {
      var n, o, s;
      o = "div" === (n = e.nodeName.toLowerCase()) || "span" === n, e.id || (this.uuid += 1, 
      e.id = "dp" + this.uuid), (s = this._newInst(t(e), o)).settings = t.extend({}, i || {}), 
      "input" === n ? this._connectDatepicker(e, s) : o && this._inlineDatepicker(e, s);
    },
    _newInst: function(e, i) {
      return {
        id: e[0].id.replace(/([^A-Za-z0-9_\-])/g, "\\\\$1"),
        input: e,
        selectedDay: 0,
        selectedMonth: 0,
        selectedYear: 0,
        drawMonth: 0,
        drawYear: 0,
        inline: i,
        dpDiv: i ? s(t("<div class='" + this._inlineClass + " ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>")) : this.dpDiv
      };
    },
    _connectDatepicker: function(e, i) {
      var n = t(e);
      i.append = t([]), i.trigger = t([]), n.hasClass(this.markerClassName) || (this._attachments(n, i), 
      n.addClass(this.markerClassName).keydown(this._doKeyDown).keypress(this._doKeyPress).keyup(this._doKeyUp), 
      this._autoSize(i), t.data(e, "datepicker", i), i.settings.disabled && this._disableDatepicker(e));
    },
    _attachments: function(e, i) {
      var n, o, s, a = this._get(i, "appendText"), r = this._get(i, "isRTL");
      i.append && i.append.remove(), a && (i.append = t("<span class='" + this._appendClass + "'>" + a + "</span>"), 
      e[r ? "before" : "after"](i.append)), e.unbind("focus", this._showDatepicker), i.trigger && i.trigger.remove(), 
      ("focus" === (n = this._get(i, "showOn")) || "both" === n) && e.focus(this._showDatepicker), 
      ("button" === n || "both" === n) && (o = this._get(i, "buttonText"), s = this._get(i, "buttonImage"), 
      i.trigger = t(this._get(i, "buttonImageOnly") ? t("<img/>").addClass(this._triggerClass).attr({
        src: s,
        alt: o,
        title: o
      }) : t("<button type='button'></button>").addClass(this._triggerClass).html(s ? t("<img/>").attr({
        src: s,
        alt: o,
        title: o
      }) : o)), e[r ? "before" : "after"](i.trigger), i.trigger.click(function() {
        return t.datepicker._datepickerShowing && t.datepicker._lastInput === e[0] ? t.datepicker._hideDatepicker() : t.datepicker._datepickerShowing && t.datepicker._lastInput !== e[0] ? (t.datepicker._hideDatepicker(), 
        t.datepicker._showDatepicker(e[0])) : t.datepicker._showDatepicker(e[0]), !1;
      }));
    },
    _autoSize: function(t) {
      if (this._get(t, "autoSize") && !t.inline) {
        var e, i, n, o, s = new Date(2009, 11, 20), a = this._get(t, "dateFormat");
        a.match(/[DM]/) && (e = function(t) {
          for (i = 0, n = 0, o = 0; t.length > o; o++) t[o].length > i && (i = t[o].length, 
          n = o);
          return n;
        }, s.setMonth(e(this._get(t, a.match(/MM/) ? "monthNames" : "monthNamesShort"))), 
        s.setDate(e(this._get(t, a.match(/DD/) ? "dayNames" : "dayNamesShort")) + 20 - s.getDay())), 
        t.input.attr("size", this._formatDate(t, s).length);
      }
    },
    _inlineDatepicker: function(e, i) {
      var n = t(e);
      n.hasClass(this.markerClassName) || (n.addClass(this.markerClassName).append(i.dpDiv), 
      t.data(e, "datepicker", i), this._setDate(i, this._getDefaultDate(i), !0), this._updateDatepicker(i), 
      this._updateAlternate(i), i.settings.disabled && this._disableDatepicker(e), i.dpDiv.css("display", "block"));
    },
    _dialogDatepicker: function(e, i, n, o, s) {
      var a, l, c, d, u, h = this._dialogInst;
      return h || (this.uuid += 1, a = "dp" + this.uuid, this._dialogInput = t("<input type='text' id='" + a + "' style='position: absolute; top: -100px; width: 0px;'/>"), 
      this._dialogInput.keydown(this._doKeyDown), t("body").append(this._dialogInput), 
      h = this._dialogInst = this._newInst(this._dialogInput, !1), h.settings = {}, t.data(this._dialogInput[0], "datepicker", h)), 
      r(h.settings, o || {}), i = i && i.constructor === Date ? this._formatDate(h, i) : i, 
      this._dialogInput.val(i), this._pos = s ? s.length ? s : [ s.pageX, s.pageY ] : null, 
      this._pos || (l = document.documentElement.clientWidth, c = document.documentElement.clientHeight, 
      d = document.documentElement.scrollLeft || document.body.scrollLeft, u = document.documentElement.scrollTop || document.body.scrollTop, 
      this._pos = [ l / 2 - 100 + d, c / 2 - 150 + u ]), this._dialogInput.css("left", this._pos[0] + 20 + "px").css("top", this._pos[1] + "px"), 
      h.settings.onSelect = n, this._inDialog = !0, this.dpDiv.addClass(this._dialogClass), 
      this._showDatepicker(this._dialogInput[0]), t.blockUI && t.blockUI(this.dpDiv), 
      t.data(this._dialogInput[0], "datepicker", h), this;
    },
    _destroyDatepicker: function(e) {
      var i, n = t(e), o = t.data(e, "datepicker");
      n.hasClass(this.markerClassName) && (i = e.nodeName.toLowerCase(), t.removeData(e, "datepicker"), 
      "input" === i ? (o.append.remove(), o.trigger.remove(), n.removeClass(this.markerClassName).unbind("focus", this._showDatepicker).unbind("keydown", this._doKeyDown).unbind("keypress", this._doKeyPress).unbind("keyup", this._doKeyUp)) : ("div" === i || "span" === i) && n.removeClass(this.markerClassName).empty());
    },
    _enableDatepicker: function(e) {
      var i, n, o = t(e), s = t.data(e, "datepicker");
      o.hasClass(this.markerClassName) && ("input" === (i = e.nodeName.toLowerCase()) ? (e.disabled = !1, 
      s.trigger.filter("button").each(function() {
        this.disabled = !1;
      }).end().filter("img").css({
        opacity: "1.0",
        cursor: ""
      })) : ("div" === i || "span" === i) && ((n = o.children("." + this._inlineClass)).children().removeClass("ui-state-disabled"), 
      n.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled", !1)), 
      this._disabledInputs = t.map(this._disabledInputs, function(t) {
        return t === e ? null : t;
      }));
    },
    _disableDatepicker: function(e) {
      var i, n, o = t(e), s = t.data(e, "datepicker");
      o.hasClass(this.markerClassName) && ("input" === (i = e.nodeName.toLowerCase()) ? (e.disabled = !0, 
      s.trigger.filter("button").each(function() {
        this.disabled = !0;
      }).end().filter("img").css({
        opacity: "0.5",
        cursor: "default"
      })) : ("div" === i || "span" === i) && ((n = o.children("." + this._inlineClass)).children().addClass("ui-state-disabled"), 
      n.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled", !0)), 
      this._disabledInputs = t.map(this._disabledInputs, function(t) {
        return t === e ? null : t;
      }), this._disabledInputs[this._disabledInputs.length] = e);
    },
    _isDisabledDatepicker: function(t) {
      if (!t) return !1;
      for (var e = 0; this._disabledInputs.length > e; e++) if (this._disabledInputs[e] === t) return !0;
      return !1;
    },
    _getInst: function(e) {
      try {
        return t.data(e, "datepicker");
      } catch (t) {
        throw "Missing instance data for this datepicker";
      }
    },
    _optionDatepicker: function(e, i, n) {
      var o, s, a, l, c = this._getInst(e);
      return 2 === arguments.length && "string" == typeof i ? "defaults" === i ? t.extend({}, t.datepicker._defaults) : c ? "all" === i ? t.extend({}, c.settings) : this._get(c, i) : null : (o = i || {}, 
      "string" == typeof i && (o = {}, o[i] = n), void (c && (this._curInst === c && this._hideDatepicker(), 
      s = this._getDateDatepicker(e, !0), a = this._getMinMaxDate(c, "min"), l = this._getMinMaxDate(c, "max"), 
      r(c.settings, o), null !== a && void 0 !== o.dateFormat && void 0 === o.minDate && (c.settings.minDate = this._formatDate(c, a)), 
      null !== l && void 0 !== o.dateFormat && void 0 === o.maxDate && (c.settings.maxDate = this._formatDate(c, l)), 
      "disabled" in o && (o.disabled ? this._disableDatepicker(e) : this._enableDatepicker(e)), 
      this._attachments(t(e), c), this._autoSize(c), this._setDate(c, s), this._updateAlternate(c), 
      this._updateDatepicker(c))));
    },
    _changeDatepicker: function(t, e, i) {
      this._optionDatepicker(t, e, i);
    },
    _refreshDatepicker: function(t) {
      var e = this._getInst(t);
      e && this._updateDatepicker(e);
    },
    _setDateDatepicker: function(t, e) {
      var i = this._getInst(t);
      i && (this._setDate(i, e), this._updateDatepicker(i), this._updateAlternate(i));
    },
    _getDateDatepicker: function(t, e) {
      var i = this._getInst(t);
      return i && !i.inline && this._setDateFromField(i, e), i ? this._getDate(i) : null;
    },
    _doKeyDown: function(e) {
      var i, n, o, s = t.datepicker._getInst(e.target), a = !0, r = s.dpDiv.is(".ui-datepicker-rtl");
      if (s._keyEvent = !0, t.datepicker._datepickerShowing) switch (e.keyCode) {
       case 9:
        t.datepicker._hideDatepicker(), a = !1;
        break;

       case 13:
        return (o = t("td." + t.datepicker._dayOverClass + ":not(." + t.datepicker._currentClass + ")", s.dpDiv))[0] && t.datepicker._selectDay(e.target, s.selectedMonth, s.selectedYear, o[0]), 
        (i = t.datepicker._get(s, "onSelect")) ? (n = t.datepicker._formatDate(s), i.apply(s.input ? s.input[0] : null, [ n, s ])) : t.datepicker._hideDatepicker(), 
        !1;

       case 27:
        t.datepicker._hideDatepicker();
        break;

       case 33:
        t.datepicker._adjustDate(e.target, e.ctrlKey ? -t.datepicker._get(s, "stepBigMonths") : -t.datepicker._get(s, "stepMonths"), "M");
        break;

       case 34:
        t.datepicker._adjustDate(e.target, e.ctrlKey ? +t.datepicker._get(s, "stepBigMonths") : +t.datepicker._get(s, "stepMonths"), "M");
        break;

       case 35:
        (e.ctrlKey || e.metaKey) && t.datepicker._clearDate(e.target), a = e.ctrlKey || e.metaKey;
        break;

       case 36:
        (e.ctrlKey || e.metaKey) && t.datepicker._gotoToday(e.target), a = e.ctrlKey || e.metaKey;
        break;

       case 37:
        (e.ctrlKey || e.metaKey) && t.datepicker._adjustDate(e.target, r ? 1 : -1, "D"), 
        a = e.ctrlKey || e.metaKey, e.originalEvent.altKey && t.datepicker._adjustDate(e.target, e.ctrlKey ? -t.datepicker._get(s, "stepBigMonths") : -t.datepicker._get(s, "stepMonths"), "M");
        break;

       case 38:
        (e.ctrlKey || e.metaKey) && t.datepicker._adjustDate(e.target, -7, "D"), a = e.ctrlKey || e.metaKey;
        break;

       case 39:
        (e.ctrlKey || e.metaKey) && t.datepicker._adjustDate(e.target, r ? -1 : 1, "D"), 
        a = e.ctrlKey || e.metaKey, e.originalEvent.altKey && t.datepicker._adjustDate(e.target, e.ctrlKey ? +t.datepicker._get(s, "stepBigMonths") : +t.datepicker._get(s, "stepMonths"), "M");
        break;

       case 40:
        (e.ctrlKey || e.metaKey) && t.datepicker._adjustDate(e.target, 7, "D"), a = e.ctrlKey || e.metaKey;
        break;

       default:
        a = !1;
      } else 36 === e.keyCode && e.ctrlKey ? t.datepicker._showDatepicker(this) : a = !1;
      a && (e.preventDefault(), e.stopPropagation());
    },
    _doKeyPress: function(e) {
      var i, n, o = t.datepicker._getInst(e.target);
      return t.datepicker._get(o, "constrainInput") ? (i = t.datepicker._possibleChars(t.datepicker._get(o, "dateFormat")), 
      n = String.fromCharCode(null == e.charCode ? e.keyCode : e.charCode), e.ctrlKey || e.metaKey || " " > n || !i || i.indexOf(n) > -1) : void 0;
    },
    _doKeyUp: function(e) {
      var i = t.datepicker._getInst(e.target);
      if (i.input.val() !== i.lastVal) try {
        t.datepicker.parseDate(t.datepicker._get(i, "dateFormat"), i.input ? i.input.val() : null, t.datepicker._getFormatConfig(i)) && (t.datepicker._setDateFromField(i), 
        t.datepicker._updateAlternate(i), t.datepicker._updateDatepicker(i));
      } catch (t) {}
      return !0;
    },
    _showDatepicker: function(e) {
      if ("input" !== (e = e.target || e).nodeName.toLowerCase() && (e = t("input", e.parentNode)[0]), 
      !t.datepicker._isDisabledDatepicker(e) && t.datepicker._lastInput !== e) {
        var i, o, s, a, l, c, d;
        i = t.datepicker._getInst(e), t.datepicker._curInst && t.datepicker._curInst !== i && (t.datepicker._curInst.dpDiv.stop(!0, !0), 
        i && t.datepicker._datepickerShowing && t.datepicker._hideDatepicker(t.datepicker._curInst.input[0])), 
        !1 !== (s = (o = t.datepicker._get(i, "beforeShow")) ? o.apply(e, [ e, i ]) : {}) && (r(i.settings, s), 
        i.lastVal = null, t.datepicker._lastInput = e, t.datepicker._setDateFromField(i), 
        t.datepicker._inDialog && (e.value = ""), t.datepicker._pos || (t.datepicker._pos = t.datepicker._findPos(e), 
        t.datepicker._pos[1] += e.offsetHeight), a = !1, t(e).parents().each(function() {
          return !(a |= "fixed" === t(this).css("position"));
        }), l = {
          left: t.datepicker._pos[0],
          top: t.datepicker._pos[1]
        }, t.datepicker._pos = null, i.dpDiv.empty(), i.dpDiv.css({
          position: "absolute",
          display: "block",
          top: "-1000px"
        }), t.datepicker._updateDatepicker(i), l = t.datepicker._checkOffset(i, l, a), i.dpDiv.css({
          position: t.datepicker._inDialog && t.blockUI ? "static" : a ? "fixed" : "absolute",
          display: "none",
          left: l.left + "px",
          top: l.top + "px"
        }), i.inline || (c = t.datepicker._get(i, "showAnim"), d = t.datepicker._get(i, "duration"), 
        i.dpDiv.css("z-index", n(t(e)) + 1), t.datepicker._datepickerShowing = !0, t.effects && t.effects.effect[c] ? i.dpDiv.show(c, t.datepicker._get(i, "showOptions"), d) : i.dpDiv[c || "show"](c ? d : null), 
        t.datepicker._shouldFocusInput(i) && i.input.focus(), t.datepicker._curInst = i));
      }
    },
    _updateDatepicker: function(e) {
      this.maxRows = 4, v = e, e.dpDiv.empty().append(this._generateHTML(e)), this._attachHandlers(e);
      var i, n = this._getNumberOfMonths(e), o = n[1], s = e.dpDiv.find("." + this._dayOverClass + " a");
      s.length > 0 && a.apply(s.get(0)), e.dpDiv.removeClass("ui-datepicker-multi-2 ui-datepicker-multi-3 ui-datepicker-multi-4").width(""), 
      o > 1 && e.dpDiv.addClass("ui-datepicker-multi-" + o).css("width", 17 * o + "em"), 
      e.dpDiv[(1 !== n[0] || 1 !== n[1] ? "add" : "remove") + "Class"]("ui-datepicker-multi"), 
      e.dpDiv[(this._get(e, "isRTL") ? "add" : "remove") + "Class"]("ui-datepicker-rtl"), 
      e === t.datepicker._curInst && t.datepicker._datepickerShowing && t.datepicker._shouldFocusInput(e) && e.input.focus(), 
      e.yearshtml && (i = e.yearshtml, setTimeout(function() {
        i === e.yearshtml && e.yearshtml && e.dpDiv.find("select.ui-datepicker-year:first").replaceWith(e.yearshtml), 
        i = e.yearshtml = null;
      }, 0));
    },
    _shouldFocusInput: function(t) {
      return t.input && t.input.is(":visible") && !t.input.is(":disabled") && !t.input.is(":focus");
    },
    _checkOffset: function(e, i, n) {
      var o = e.dpDiv.outerWidth(), s = e.dpDiv.outerHeight(), a = e.input ? e.input.outerWidth() : 0, r = e.input ? e.input.outerHeight() : 0, l = document.documentElement.clientWidth + (n ? 0 : t(document).scrollLeft()), c = document.documentElement.clientHeight + (n ? 0 : t(document).scrollTop());
      return i.left -= this._get(e, "isRTL") ? o - a : 0, i.left -= n && i.left === e.input.offset().left ? t(document).scrollLeft() : 0, 
      i.top -= n && i.top === e.input.offset().top + r ? t(document).scrollTop() : 0, 
      i.left -= Math.min(i.left, i.left + o > l && l > o ? Math.abs(i.left + o - l) : 0), 
      i.top -= Math.min(i.top, i.top + s > c && c > s ? Math.abs(s + r) : 0), i;
    },
    _findPos: function(e) {
      for (var i, n = this._getInst(e), o = this._get(n, "isRTL"); e && ("hidden" === e.type || 1 !== e.nodeType || t.expr.filters.hidden(e)); ) e = e[o ? "previousSibling" : "nextSibling"];
      return i = t(e).offset(), [ i.left, i.top ];
    },
    _hideDatepicker: function(e) {
      var i, n, o, s, a = this._curInst;
      !a || e && a !== t.data(e, "datepicker") || this._datepickerShowing && (i = this._get(a, "showAnim"), 
      n = this._get(a, "duration"), o = function() {
        t.datepicker._tidyDialog(a);
      }, t.effects && (t.effects.effect[i] || t.effects[i]) ? a.dpDiv.hide(i, t.datepicker._get(a, "showOptions"), n, o) : a.dpDiv["slideDown" === i ? "slideUp" : "fadeIn" === i ? "fadeOut" : "hide"](i ? n : null, o), 
      i || o(), this._datepickerShowing = !1, (s = this._get(a, "onClose")) && s.apply(a.input ? a.input[0] : null, [ a.input ? a.input.val() : "", a ]), 
      this._lastInput = null, this._inDialog && (this._dialogInput.css({
        position: "absolute",
        left: "0",
        top: "-100px"
      }), t.blockUI && (t.unblockUI(), t("body").append(this.dpDiv))), this._inDialog = !1);
    },
    _tidyDialog: function(t) {
      t.dpDiv.removeClass(this._dialogClass).unbind(".ui-datepicker-calendar");
    },
    _checkExternalClick: function(e) {
      if (t.datepicker._curInst) {
        var i = t(e.target), n = t.datepicker._getInst(i[0]);
        (i[0].id !== t.datepicker._mainDivId && 0 === i.parents("#" + t.datepicker._mainDivId).length && !i.hasClass(t.datepicker.markerClassName) && !i.closest("." + t.datepicker._triggerClass).length && t.datepicker._datepickerShowing && (!t.datepicker._inDialog || !t.blockUI) || i.hasClass(t.datepicker.markerClassName) && t.datepicker._curInst !== n) && t.datepicker._hideDatepicker();
      }
    },
    _adjustDate: function(e, i, n) {
      var o = t(e), s = this._getInst(o[0]);
      this._isDisabledDatepicker(o[0]) || (this._adjustInstDate(s, i + ("M" === n ? this._get(s, "showCurrentAtPos") : 0), n), 
      this._updateDatepicker(s));
    },
    _gotoToday: function(e) {
      var i, n = t(e), o = this._getInst(n[0]);
      this._get(o, "gotoCurrent") && o.currentDay ? (o.selectedDay = o.currentDay, o.drawMonth = o.selectedMonth = o.currentMonth, 
      o.drawYear = o.selectedYear = o.currentYear) : (i = new Date(), o.selectedDay = i.getDate(), 
      o.drawMonth = o.selectedMonth = i.getMonth(), o.drawYear = o.selectedYear = i.getFullYear()), 
      this._notifyChange(o), this._adjustDate(n);
    },
    _selectMonthYear: function(e, i, n) {
      var o = t(e), s = this._getInst(o[0]);
      s["selected" + ("M" === n ? "Month" : "Year")] = s["draw" + ("M" === n ? "Month" : "Year")] = parseInt(i.options[i.selectedIndex].value, 10), 
      this._notifyChange(s), this._adjustDate(o);
    },
    _selectDay: function(e, i, n, o) {
      var s, a = t(e);
      t(o).hasClass(this._unselectableClass) || this._isDisabledDatepicker(a[0]) || (s = this._getInst(a[0]), 
      s.selectedDay = s.currentDay = t("a", o).html(), s.selectedMonth = s.currentMonth = i, 
      s.selectedYear = s.currentYear = n, this._selectDate(e, this._formatDate(s, s.currentDay, s.currentMonth, s.currentYear)));
    },
    _clearDate: function(e) {
      var i = t(e);
      this._selectDate(i, "");
    },
    _selectDate: function(e, i) {
      var n, o = t(e), s = this._getInst(o[0]);
      i = null != i ? i : this._formatDate(s), s.input && s.input.val(i), this._updateAlternate(s), 
      (n = this._get(s, "onSelect")) ? n.apply(s.input ? s.input[0] : null, [ i, s ]) : s.input && s.input.trigger("change"), 
      s.inline ? this._updateDatepicker(s) : (this._hideDatepicker(), this._lastInput = s.input[0], 
      "object" != typeof s.input[0] && s.input.focus(), this._lastInput = null);
    },
    _updateAlternate: function(e) {
      var i, n, o, s = this._get(e, "altField");
      s && (i = this._get(e, "altFormat") || this._get(e, "dateFormat"), n = this._getDate(e), 
      o = this.formatDate(i, n, this._getFormatConfig(e)), t(s).each(function() {
        t(this).val(o);
      }));
    },
    noWeekends: function(t) {
      var e = t.getDay();
      return [ e > 0 && 6 > e, "" ];
    },
    iso8601Week: function(t) {
      var e, i = new Date(t.getTime());
      return i.setDate(i.getDate() + 4 - (i.getDay() || 7)), e = i.getTime(), i.setMonth(0), 
      i.setDate(1), Math.floor(Math.round((e - i) / 864e5) / 7) + 1;
    },
    parseDate: function(e, i, n) {
      if (null == e || null == i) throw "Invalid arguments";
      if ("" == (i = "object" == typeof i ? "" + i : i + "")) return null;
      var o, s, a, r, l = 0, c = (n ? n.shortYearCutoff : null) || this._defaults.shortYearCutoff, d = "string" != typeof c ? c : new Date().getFullYear() % 100 + parseInt(c, 10), u = (n ? n.dayNamesShort : null) || this._defaults.dayNamesShort, h = (n ? n.dayNames : null) || this._defaults.dayNames, p = (n ? n.monthNamesShort : null) || this._defaults.monthNamesShort, f = (n ? n.monthNames : null) || this._defaults.monthNames, m = -1, g = -1, v = -1, b = -1, C = !1, y = function(t) {
        var i = e.length > o + 1 && e.charAt(o + 1) === t;
        return i && o++, i;
      }, w = function(t) {
        var e = y(t), n = "@" === t ? 14 : "!" === t ? 20 : "y" === t && e ? 4 : "o" === t ? 3 : 2, o = "y" === t ? n : 1, s = RegExp("^\\d{" + o + "," + n + "}"), a = i.substring(l).match(s);
        if (!a) throw "Missing number at position " + l;
        return l += a[0].length, parseInt(a[0], 10);
      }, x = function(e, n, o) {
        var s = -1, a = t.map(y(e) ? o : n, function(t, e) {
          return [ [ e, t ] ];
        }).sort(function(t, e) {
          return -(t[1].length - e[1].length);
        });
        if (t.each(a, function(t, e) {
          var n = e[1];
          return i.substr(l, n.length).toLowerCase() === n.toLowerCase() ? (s = e[0], l += n.length, 
          !1) : void 0;
        }), -1 !== s) return s + 1;
        throw "Unknown name at position " + l;
      }, k = function() {
        if (i.charAt(l) !== e.charAt(o)) throw "Unexpected literal at position " + l;
        l++;
      };
      for (o = 0; e.length > o; o++) if (C) "'" !== e.charAt(o) || y("'") ? k() : C = !1; else switch (e.charAt(o)) {
       case "d":
        v = w("d");
        break;

       case "D":
        x("D", u, h);
        break;

       case "o":
        b = w("o");
        break;

       case "m":
        g = w("m");
        break;

       case "M":
        g = x("M", p, f);
        break;

       case "y":
        m = w("y");
        break;

       case "@":
        m = (r = new Date(w("@"))).getFullYear(), g = r.getMonth() + 1, v = r.getDate();
        break;

       case "!":
        m = (r = new Date((w("!") - this._ticksTo1970) / 1e4)).getFullYear(), g = r.getMonth() + 1, 
        v = r.getDate();
        break;

       case "'":
        y("'") ? k() : C = !0;
        break;

       default:
        k();
      }
      if (i.length > l && (a = i.substr(l), !/^\s+/.test(a))) throw "Extra/unparsed characters found in date: " + a;
      if (-1 === m ? m = new Date().getFullYear() : 100 > m && (m += new Date().getFullYear() - new Date().getFullYear() % 100 + (d >= m ? 0 : -100)), 
      b > -1) for (g = 1, v = b; !((s = this._getDaysInMonth(m, g - 1)) >= v); ) g++, 
      v -= s;
      if ((r = this._daylightSavingAdjust(new Date(m, g - 1, v))).getFullYear() !== m || r.getMonth() + 1 !== g || r.getDate() !== v) throw "Invalid date";
      return r;
    },
    ATOM: "yy-mm-dd",
    COOKIE: "D, dd M yy",
    ISO_8601: "yy-mm-dd",
    RFC_822: "D, d M y",
    RFC_850: "DD, dd-M-y",
    RFC_1036: "D, d M y",
    RFC_1123: "D, d M yy",
    RFC_2822: "D, d M yy",
    RSS: "D, d M y",
    TICKS: "!",
    TIMESTAMP: "@",
    W3C: "yy-mm-dd",
    _ticksTo1970: 864e9 * (718685 + Math.floor(492.5) - Math.floor(19.7) + Math.floor(4.925)),
    formatDate: function(t, e, i) {
      if (!e) return "";
      var n, o = (i ? i.dayNamesShort : null) || this._defaults.dayNamesShort, s = (i ? i.dayNames : null) || this._defaults.dayNames, a = (i ? i.monthNamesShort : null) || this._defaults.monthNamesShort, r = (i ? i.monthNames : null) || this._defaults.monthNames, l = function(e) {
        var i = t.length > n + 1 && t.charAt(n + 1) === e;
        return i && n++, i;
      }, c = function(t, e, i) {
        var n = "" + e;
        if (l(t)) for (;i > n.length; ) n = "0" + n;
        return n;
      }, d = function(t, e, i, n) {
        return l(t) ? n[e] : i[e];
      }, u = "", h = !1;
      if (e) for (n = 0; t.length > n; n++) if (h) "'" !== t.charAt(n) || l("'") ? u += t.charAt(n) : h = !1; else switch (t.charAt(n)) {
       case "d":
        u += c("d", e.getDate(), 2);
        break;

       case "D":
        u += d("D", e.getDay(), o, s);
        break;

       case "o":
        u += c("o", Math.round((new Date(e.getFullYear(), e.getMonth(), e.getDate()).getTime() - new Date(e.getFullYear(), 0, 0).getTime()) / 864e5), 3);
        break;

       case "m":
        u += c("m", e.getMonth() + 1, 2);
        break;

       case "M":
        u += d("M", e.getMonth(), a, r);
        break;

       case "y":
        u += l("y") ? e.getFullYear() : (10 > e.getYear() % 100 ? "0" : "") + e.getYear() % 100;
        break;

       case "@":
        u += e.getTime();
        break;

       case "!":
        u += 1e4 * e.getTime() + this._ticksTo1970;
        break;

       case "'":
        l("'") ? u += "'" : h = !0;
        break;

       default:
        u += t.charAt(n);
      }
      return u;
    },
    _possibleChars: function(t) {
      var e, i = "", n = !1, o = function(i) {
        var n = t.length > e + 1 && t.charAt(e + 1) === i;
        return n && e++, n;
      };
      for (e = 0; t.length > e; e++) if (n) "'" !== t.charAt(e) || o("'") ? i += t.charAt(e) : n = !1; else switch (t.charAt(e)) {
       case "d":
       case "m":
       case "y":
       case "@":
        i += "0123456789";
        break;

       case "D":
       case "M":
        return null;

       case "'":
        o("'") ? i += "'" : n = !0;
        break;

       default:
        i += t.charAt(e);
      }
      return i;
    },
    _get: function(t, e) {
      return void 0 !== t.settings[e] ? t.settings[e] : this._defaults[e];
    },
    _setDateFromField: function(t, e) {
      if (t.input.val() !== t.lastVal) {
        var i = this._get(t, "dateFormat"), n = t.lastVal = t.input ? t.input.val() : null, o = this._getDefaultDate(t), s = o, a = this._getFormatConfig(t);
        try {
          s = this.parseDate(i, n, a) || o;
        } catch (t) {
          n = e ? "" : n;
        }
        t.selectedDay = s.getDate(), t.drawMonth = t.selectedMonth = s.getMonth(), t.drawYear = t.selectedYear = s.getFullYear(), 
        t.currentDay = n ? s.getDate() : 0, t.currentMonth = n ? s.getMonth() : 0, t.currentYear = n ? s.getFullYear() : 0, 
        this._adjustInstDate(t);
      }
    },
    _getDefaultDate: function(t) {
      return this._restrictMinMax(t, this._determineDate(t, this._get(t, "defaultDate"), new Date()));
    },
    _determineDate: function(e, i, n) {
      var o = null == i || "" === i ? n : "string" == typeof i ? function(i) {
        try {
          return t.datepicker.parseDate(t.datepicker._get(e, "dateFormat"), i, t.datepicker._getFormatConfig(e));
        } catch (t) {}
        for (var n = (i.toLowerCase().match(/^c/) ? t.datepicker._getDate(e) : null) || new Date(), o = n.getFullYear(), s = n.getMonth(), a = n.getDate(), r = /([+\-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?/g, l = r.exec(i); l; ) {
          switch (l[2] || "d") {
           case "d":
           case "D":
            a += parseInt(l[1], 10);
            break;

           case "w":
           case "W":
            a += 7 * parseInt(l[1], 10);
            break;

           case "m":
           case "M":
            s += parseInt(l[1], 10), a = Math.min(a, t.datepicker._getDaysInMonth(o, s));
            break;

           case "y":
           case "Y":
            o += parseInt(l[1], 10), a = Math.min(a, t.datepicker._getDaysInMonth(o, s));
          }
          l = r.exec(i);
        }
        return new Date(o, s, a);
      }(i) : "number" == typeof i ? isNaN(i) ? n : function(t) {
        var e = new Date();
        return e.setDate(e.getDate() + t), e;
      }(i) : new Date(i.getTime());
      return (o = o && "Invalid Date" == "" + o ? n : o) && (o.setHours(0), o.setMinutes(0), 
      o.setSeconds(0), o.setMilliseconds(0)), this._daylightSavingAdjust(o);
    },
    _daylightSavingAdjust: function(t) {
      return t ? (t.setHours(t.getHours() > 12 ? t.getHours() + 2 : 0), t) : null;
    },
    _setDate: function(t, e, i) {
      var n = !e, o = t.selectedMonth, s = t.selectedYear, a = this._restrictMinMax(t, this._determineDate(t, e, new Date()));
      t.selectedDay = t.currentDay = a.getDate(), t.drawMonth = t.selectedMonth = t.currentMonth = a.getMonth(), 
      t.drawYear = t.selectedYear = t.currentYear = a.getFullYear(), o === t.selectedMonth && s === t.selectedYear || i || this._notifyChange(t), 
      this._adjustInstDate(t), t.input && t.input.val(n ? "" : this._formatDate(t));
    },
    _getDate: function(t) {
      return !t.currentYear || t.input && "" === t.input.val() ? null : this._daylightSavingAdjust(new Date(t.currentYear, t.currentMonth, t.currentDay));
    },
    _attachHandlers: function(e) {
      var i = this._get(e, "stepMonths"), n = "#" + e.id.replace(/\\\\/g, "\\");
      e.dpDiv.find("[data-handler]").map(function() {
        var e = {
          prev: function() {
            t.datepicker._adjustDate(n, -i, "M");
          },
          next: function() {
            t.datepicker._adjustDate(n, +i, "M");
          },
          hide: function() {
            t.datepicker._hideDatepicker();
          },
          today: function() {
            t.datepicker._gotoToday(n);
          },
          selectDay: function() {
            return t.datepicker._selectDay(n, +this.getAttribute("data-month"), +this.getAttribute("data-year"), this), 
            !1;
          },
          selectMonth: function() {
            return t.datepicker._selectMonthYear(n, this, "M"), !1;
          },
          selectYear: function() {
            return t.datepicker._selectMonthYear(n, this, "Y"), !1;
          }
        };
        t(this).bind(this.getAttribute("data-event"), e[this.getAttribute("data-handler")]);
      });
    },
    _generateHTML: function(t) {
      var e, i, n, o, s, a, r, l, c, d, u, h, p, f, m, g, v, b, C, y, w, x, k, _, $, A, S, T, D, I, j, P, E, N, O, M, F, q, H, L = new Date(), W = this._daylightSavingAdjust(new Date(L.getFullYear(), L.getMonth(), L.getDate())), R = this._get(t, "isRTL"), z = this._get(t, "showButtonPanel"), B = this._get(t, "hideIfNoPrevNext"), U = this._get(t, "navigationAsDateFormat"), V = this._getNumberOfMonths(t), Q = this._get(t, "showCurrentAtPos"), X = this._get(t, "stepMonths"), Y = 1 !== V[0] || 1 !== V[1], G = this._daylightSavingAdjust(t.currentDay ? new Date(t.currentYear, t.currentMonth, t.currentDay) : new Date(9999, 9, 9)), K = this._getMinMaxDate(t, "min"), J = this._getMinMaxDate(t, "max"), Z = t.drawMonth - Q, tt = t.drawYear;
      if (0 > Z && (Z += 12, tt--), J) for (e = this._daylightSavingAdjust(new Date(J.getFullYear(), J.getMonth() - V[0] * V[1] + 1, J.getDate())), 
      e = K && K > e ? K : e; this._daylightSavingAdjust(new Date(tt, Z, 1)) > e; ) 0 > --Z && (Z = 11, 
      tt--);
      for (t.drawMonth = Z, t.drawYear = tt, i = this._get(t, "prevText"), i = U ? this.formatDate(i, this._daylightSavingAdjust(new Date(tt, Z - X, 1)), this._getFormatConfig(t)) : i, 
      n = this._canAdjustMonth(t, -1, tt, Z) ? "<a class='ui-datepicker-prev ui-corner-all' data-handler='prev' data-event='click' title='" + i + "'><span class='ui-icon ui-icon-circle-triangle-" + (R ? "e" : "w") + "'>" + i + "</span></a>" : B ? "" : "<a class='ui-datepicker-prev ui-corner-all ui-state-disabled' title='" + i + "'><span class='ui-icon ui-icon-circle-triangle-" + (R ? "e" : "w") + "'>" + i + "</span></a>", 
      o = this._get(t, "nextText"), o = U ? this.formatDate(o, this._daylightSavingAdjust(new Date(tt, Z + X, 1)), this._getFormatConfig(t)) : o, 
      s = this._canAdjustMonth(t, 1, tt, Z) ? "<a class='ui-datepicker-next ui-corner-all' data-handler='next' data-event='click' title='" + o + "'><span class='ui-icon ui-icon-circle-triangle-" + (R ? "w" : "e") + "'>" + o + "</span></a>" : B ? "" : "<a class='ui-datepicker-next ui-corner-all ui-state-disabled' title='" + o + "'><span class='ui-icon ui-icon-circle-triangle-" + (R ? "w" : "e") + "'>" + o + "</span></a>", 
      a = this._get(t, "currentText"), r = this._get(t, "gotoCurrent") && t.currentDay ? G : W, 
      a = U ? this.formatDate(a, r, this._getFormatConfig(t)) : a, l = t.inline ? "" : "<button type='button' class='ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all' data-handler='hide' data-event='click'>" + this._get(t, "closeText") + "</button>", 
      c = z ? "<div class='ui-datepicker-buttonpane ui-widget-content'>" + (R ? l : "") + (this._isInRange(t, r) ? "<button type='button' class='ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all' data-handler='today' data-event='click'>" + a + "</button>" : "") + (R ? "" : l) + "</div>" : "", 
      d = parseInt(this._get(t, "firstDay"), 10), d = isNaN(d) ? 0 : d, u = this._get(t, "showWeek"), 
      h = this._get(t, "dayNames"), p = this._get(t, "dayNamesMin"), f = this._get(t, "monthNames"), 
      m = this._get(t, "monthNamesShort"), g = this._get(t, "beforeShowDay"), v = this._get(t, "showOtherMonths"), 
      b = this._get(t, "selectOtherMonths"), C = this._getDefaultDate(t), y = "", x = 0; V[0] > x; x++) {
        for (k = "", this.maxRows = 4, _ = 0; V[1] > _; _++) {
          if ($ = this._daylightSavingAdjust(new Date(tt, Z, t.selectedDay)), A = " ui-corner-all", 
          S = "", Y) {
            if (S += "<div class='ui-datepicker-group", V[1] > 1) switch (_) {
             case 0:
              S += " ui-datepicker-group-first", A = " ui-corner-" + (R ? "right" : "left");
              break;

             case V[1] - 1:
              S += " ui-datepicker-group-last", A = " ui-corner-" + (R ? "left" : "right");
              break;

             default:
              S += " ui-datepicker-group-middle", A = "";
            }
            S += "'>";
          }
          for (S += "<div class='ui-datepicker-header ui-widget-header ui-helper-clearfix" + A + "'>" + (/all|left/.test(A) && 0 === x ? R ? s : n : "") + (/all|right/.test(A) && 0 === x ? R ? n : s : "") + this._generateMonthYearHeader(t, Z, tt, K, J, x > 0 || _ > 0, f, m) + "</div><table class='ui-datepicker-calendar'><thead><tr>", 
          T = u ? "<th class='ui-datepicker-week-col'>" + this._get(t, "weekHeader") + "</th>" : "", 
          w = 0; 7 > w; w++) D = (w + d) % 7, T += "<th scope='col'" + ((w + d + 6) % 7 >= 5 ? " class='ui-datepicker-week-end'" : "") + "><span title='" + h[D] + "'>" + p[D] + "</span></th>";
          for (S += T + "</tr></thead><tbody>", I = this._getDaysInMonth(tt, Z), tt === t.selectedYear && Z === t.selectedMonth && (t.selectedDay = Math.min(t.selectedDay, I)), 
          j = (this._getFirstDayOfMonth(tt, Z) - d + 7) % 7, P = Math.ceil((j + I) / 7), E = Y && this.maxRows > P ? this.maxRows : P, 
          this.maxRows = E, N = this._daylightSavingAdjust(new Date(tt, Z, 1 - j)), O = 0; E > O; O++) {
            for (S += "<tr>", M = u ? "<td class='ui-datepicker-week-col'>" + this._get(t, "calculateWeek")(N) + "</td>" : "", 
            w = 0; 7 > w; w++) F = g ? g.apply(t.input ? t.input[0] : null, [ N ]) : [ !0, "" ], 
            q = N.getMonth() !== Z, H = q && !b || !F[0] || K && K > N || J && N > J, M += "<td class='" + ((w + d + 6) % 7 >= 5 ? " ui-datepicker-week-end" : "") + (q ? " ui-datepicker-other-month" : "") + (N.getTime() === $.getTime() && Z === t.selectedMonth && t._keyEvent || C.getTime() === N.getTime() && C.getTime() === $.getTime() ? " " + this._dayOverClass : "") + (H ? " " + this._unselectableClass + " ui-state-disabled" : "") + (q && !v ? "" : " " + F[1] + (N.getTime() === G.getTime() ? " " + this._currentClass : "") + (N.getTime() === W.getTime() ? " ui-datepicker-today" : "")) + "'" + (q && !v || !F[2] ? "" : " title='" + F[2].replace(/'/g, "&#39;") + "'") + (H ? "" : " data-handler='selectDay' data-event='click' data-month='" + N.getMonth() + "' data-year='" + N.getFullYear() + "'") + ">" + (q && !v ? "&#xa0;" : H ? "<span class='ui-state-default'>" + N.getDate() + "</span>" : "<a class='ui-state-default" + (N.getTime() === W.getTime() ? " ui-state-highlight" : "") + (N.getTime() === G.getTime() ? " ui-state-active" : "") + (q ? " ui-priority-secondary" : "") + "' href='#'>" + N.getDate() + "</a>") + "</td>", 
            N.setDate(N.getDate() + 1), N = this._daylightSavingAdjust(N);
            S += M + "</tr>";
          }
          ++Z > 11 && (Z = 0, tt++), k += S += "</tbody></table>" + (Y ? "</div>" + (V[0] > 0 && _ === V[1] - 1 ? "<div class='ui-datepicker-row-break'></div>" : "") : "");
        }
        y += k;
      }
      return y += c, t._keyEvent = !1, y;
    },
    _generateMonthYearHeader: function(t, e, i, n, o, s, a, r) {
      var l, c, d, u, h, p, f, m, g = this._get(t, "changeMonth"), v = this._get(t, "changeYear"), b = this._get(t, "showMonthAfterYear"), C = "<div class='ui-datepicker-title'>", y = "";
      if (s || !g) y += "<span class='ui-datepicker-month'>" + a[e] + "</span>"; else {
        for (l = n && n.getFullYear() === i, c = o && o.getFullYear() === i, y += "<select class='ui-datepicker-month' data-handler='selectMonth' data-event='change'>", 
        d = 0; 12 > d; d++) (!l || d >= n.getMonth()) && (!c || o.getMonth() >= d) && (y += "<option value='" + d + "'" + (d === e ? " selected='selected'" : "") + ">" + r[d] + "</option>");
        y += "</select>";
      }
      if (b || (C += y + (!s && g && v ? "" : "&#xa0;")), !t.yearshtml) if (t.yearshtml = "", 
      s || !v) C += "<span class='ui-datepicker-year'>" + i + "</span>"; else {
        for (u = this._get(t, "yearRange").split(":"), h = new Date().getFullYear(), f = (p = function(t) {
          var e = t.match(/c[+\-].*/) ? i + parseInt(t.substring(1), 10) : t.match(/[+\-].*/) ? h + parseInt(t, 10) : parseInt(t, 10);
          return isNaN(e) ? h : e;
        })(u[0]), m = Math.max(f, p(u[1] || "")), f = n ? Math.max(f, n.getFullYear()) : f, 
        m = o ? Math.min(m, o.getFullYear()) : m, t.yearshtml += "<select class='ui-datepicker-year' data-handler='selectYear' data-event='change'>"; m >= f; f++) t.yearshtml += "<option value='" + f + "'" + (f === i ? " selected='selected'" : "") + ">" + f + "</option>";
        t.yearshtml += "</select>", C += t.yearshtml, t.yearshtml = null;
      }
      return C += this._get(t, "yearSuffix"), b && (C += (!s && g && v ? "" : "&#xa0;") + y), 
      C += "</div>";
    },
    _adjustInstDate: function(t, e, i) {
      var n = t.drawYear + ("Y" === i ? e : 0), o = t.drawMonth + ("M" === i ? e : 0), s = Math.min(t.selectedDay, this._getDaysInMonth(n, o)) + ("D" === i ? e : 0), a = this._restrictMinMax(t, this._daylightSavingAdjust(new Date(n, o, s)));
      t.selectedDay = a.getDate(), t.drawMonth = t.selectedMonth = a.getMonth(), t.drawYear = t.selectedYear = a.getFullYear(), 
      ("M" === i || "Y" === i) && this._notifyChange(t);
    },
    _restrictMinMax: function(t, e) {
      var i = this._getMinMaxDate(t, "min"), n = this._getMinMaxDate(t, "max"), o = i && i > e ? i : e;
      return n && o > n ? n : o;
    },
    _notifyChange: function(t) {
      var e = this._get(t, "onChangeMonthYear");
      e && e.apply(t.input ? t.input[0] : null, [ t.selectedYear, t.selectedMonth + 1, t ]);
    },
    _getNumberOfMonths: function(t) {
      var e = this._get(t, "numberOfMonths");
      return null == e ? [ 1, 1 ] : "number" == typeof e ? [ 1, e ] : e;
    },
    _getMinMaxDate: function(t, e) {
      return this._determineDate(t, this._get(t, e + "Date"), null);
    },
    _getDaysInMonth: function(t, e) {
      return 32 - this._daylightSavingAdjust(new Date(t, e, 32)).getDate();
    },
    _getFirstDayOfMonth: function(t, e) {
      return new Date(t, e, 1).getDay();
    },
    _canAdjustMonth: function(t, e, i, n) {
      var o = this._getNumberOfMonths(t), s = this._daylightSavingAdjust(new Date(i, n + (0 > e ? e : o[0] * o[1]), 1));
      return 0 > e && s.setDate(this._getDaysInMonth(s.getFullYear(), s.getMonth())), 
      this._isInRange(t, s);
    },
    _isInRange: function(t, e) {
      var i, n, o = this._getMinMaxDate(t, "min"), s = this._getMinMaxDate(t, "max"), a = null, r = null, l = this._get(t, "yearRange");
      return l && (i = l.split(":"), n = new Date().getFullYear(), a = parseInt(i[0], 10), 
      r = parseInt(i[1], 10), i[0].match(/[+\-].*/) && (a += n), i[1].match(/[+\-].*/) && (r += n)), 
      (!o || e.getTime() >= o.getTime()) && (!s || e.getTime() <= s.getTime()) && (!a || e.getFullYear() >= a) && (!r || r >= e.getFullYear());
    },
    _getFormatConfig: function(t) {
      var e = this._get(t, "shortYearCutoff");
      return e = "string" != typeof e ? e : new Date().getFullYear() % 100 + parseInt(e, 10), 
      {
        shortYearCutoff: e,
        dayNamesShort: this._get(t, "dayNamesShort"),
        dayNames: this._get(t, "dayNames"),
        monthNamesShort: this._get(t, "monthNamesShort"),
        monthNames: this._get(t, "monthNames")
      };
    },
    _formatDate: function(t, e, i, n) {
      e || (t.currentDay = t.selectedDay, t.currentMonth = t.selectedMonth, t.currentYear = t.selectedYear);
      var o = e ? "object" == typeof e ? e : this._daylightSavingAdjust(new Date(n, i, e)) : this._daylightSavingAdjust(new Date(t.currentYear, t.currentMonth, t.currentDay));
      return this.formatDate(this._get(t, "dateFormat"), o, this._getFormatConfig(t));
    }
  }), t.fn.datepicker = function(e) {
    if (!this.length) return this;
    t.datepicker.initialized || (t(document).mousedown(t.datepicker._checkExternalClick), 
    t.datepicker.initialized = !0), 0 === t("#" + t.datepicker._mainDivId).length && t("body").append(t.datepicker.dpDiv);
    var i = Array.prototype.slice.call(arguments, 1);
    return "string" != typeof e || "isDisabled" !== e && "getDate" !== e && "widget" !== e ? "option" === e && 2 === arguments.length && "string" == typeof arguments[1] ? t.datepicker["_" + e + "Datepicker"].apply(t.datepicker, [ this[0] ].concat(i)) : this.each(function() {
      "string" == typeof e ? t.datepicker["_" + e + "Datepicker"].apply(t.datepicker, [ this ].concat(i)) : t.datepicker._attachDatepicker(this, e);
    }) : t.datepicker["_" + e + "Datepicker"].apply(t.datepicker, [ this[0] ].concat(i));
  }, t.datepicker = new o(), t.datepicker.initialized = !1, t.datepicker.uuid = new Date().getTime(), 
  t.datepicker.version = "1.11.2", t.datepicker, t.widget("ui.dialog", {
    version: "1.11.2",
    options: {
      appendTo: "body",
      autoOpen: !0,
      buttons: [],
      closeOnEscape: !0,
      closeText: "Close",
      dialogClass: "",
      draggable: !0,
      hide: null,
      height: "auto",
      maxHeight: null,
      maxWidth: null,
      minHeight: 150,
      minWidth: 150,
      modal: !1,
      position: {
        my: "center",
        at: "center",
        of: window,
        collision: "fit",
        using: function(e) {
          var i = t(this).css(e).offset().top;
          0 > i && t(this).css("top", e.top - i);
        }
      },
      resizable: !0,
      show: null,
      title: null,
      width: 300,
      beforeClose: null,
      close: null,
      drag: null,
      dragStart: null,
      dragStop: null,
      focus: null,
      open: null,
      resize: null,
      resizeStart: null,
      resizeStop: null
    },
    sizeRelatedOptions: {
      buttons: !0,
      height: !0,
      maxHeight: !0,
      maxWidth: !0,
      minHeight: !0,
      minWidth: !0,
      width: !0
    },
    resizableRelatedOptions: {
      maxHeight: !0,
      maxWidth: !0,
      minHeight: !0,
      minWidth: !0
    },
    _create: function() {
      this.originalCss = {
        display: this.element[0].style.display,
        width: this.element[0].style.width,
        minHeight: this.element[0].style.minHeight,
        maxHeight: this.element[0].style.maxHeight,
        height: this.element[0].style.height
      }, this.originalPosition = {
        parent: this.element.parent(),
        index: this.element.parent().children().index(this.element)
      }, this.originalTitle = this.element.attr("title"), this.options.title = this.options.title || this.originalTitle, 
      this._createWrapper(), this.element.show().removeAttr("title").addClass("ui-dialog-content ui-widget-content").appendTo(this.uiDialog), 
      this._createTitlebar(), this._createButtonPane(), this.options.draggable && t.fn.draggable && this._makeDraggable(), 
      this.options.resizable && t.fn.resizable && this._makeResizable(), this._isOpen = !1, 
      this._trackFocus();
    },
    _init: function() {
      this.options.autoOpen && this.open();
    },
    _appendTo: function() {
      var e = this.options.appendTo;
      return e && (e.jquery || e.nodeType) ? t(e) : this.document.find(e || "body").eq(0);
    },
    _destroy: function() {
      var t, e = this.originalPosition;
      this._destroyOverlay(), this.element.removeUniqueId().removeClass("ui-dialog-content ui-widget-content").css(this.originalCss).detach(), 
      this.uiDialog.stop(!0, !0).remove(), this.originalTitle && this.element.attr("title", this.originalTitle), 
      (t = e.parent.children().eq(e.index)).length && t[0] !== this.element[0] ? t.before(this.element) : e.parent.append(this.element);
    },
    widget: function() {
      return this.uiDialog;
    },
    disable: t.noop,
    enable: t.noop,
    close: function(e) {
      var i, n = this;
      if (this._isOpen && !1 !== this._trigger("beforeClose", e)) {
        if (this._isOpen = !1, this._focusedElement = null, this._destroyOverlay(), this._untrackInstance(), 
        !this.opener.filter(":focusable").focus().length) try {
          (i = this.document[0].activeElement) && "body" !== i.nodeName.toLowerCase() && t(i).blur();
        } catch (t) {}
        this._hide(this.uiDialog, this.options.hide, function() {
          n._trigger("close", e);
        });
      }
    },
    isOpen: function() {
      return this._isOpen;
    },
    moveToTop: function() {
      this._moveToTop();
    },
    _moveToTop: function(e, i) {
      var n = !1, o = this.uiDialog.siblings(".ui-front:visible").map(function() {
        return +t(this).css("z-index");
      }).get(), s = Math.max.apply(null, o);
      return s >= +this.uiDialog.css("z-index") && (this.uiDialog.css("z-index", s + 1), 
      n = !0), n && !i && this._trigger("focus", e), n;
    },
    open: function() {
      var e = this;
      return this._isOpen ? void (this._moveToTop() && this._focusTabbable()) : (this._isOpen = !0, 
      this.opener = t(this.document[0].activeElement), this._size(), this._position(), 
      this._createOverlay(), this._moveToTop(null, !0), this.overlay && this.overlay.css("z-index", this.uiDialog.css("z-index") - 1), 
      this._show(this.uiDialog, this.options.show, function() {
        e._focusTabbable(), e._trigger("focus");
      }), this._makeFocusTarget(), void this._trigger("open"));
    },
    _focusTabbable: function() {
      var t = this._focusedElement;
      t || (t = this.element.find("[autofocus]")), t.length || (t = this.element.find(":tabbable")), 
      t.length || (t = this.uiDialogButtonPane.find(":tabbable")), t.length || (t = this.uiDialogTitlebarClose.filter(":tabbable")), 
      t.length || (t = this.uiDialog), t.eq(0).focus();
    },
    _keepFocus: function(e) {
      function i() {
        var e = this.document[0].activeElement;
        this.uiDialog[0] === e || t.contains(this.uiDialog[0], e) || this._focusTabbable();
      }
      e.preventDefault(), i.call(this), this._delay(i);
    },
    _createWrapper: function() {
      this.uiDialog = t("<div>").addClass("ui-dialog ui-widget ui-widget-content ui-corner-all ui-front " + this.options.dialogClass).hide().attr({
        tabIndex: -1,
        role: "dialog"
      }).appendTo(this._appendTo()), this._on(this.uiDialog, {
        keydown: function(e) {
          if (this.options.closeOnEscape && !e.isDefaultPrevented() && e.keyCode && e.keyCode === t.ui.keyCode.ESCAPE) return e.preventDefault(), 
          void this.close(e);
          if (e.keyCode === t.ui.keyCode.TAB && !e.isDefaultPrevented()) {
            var i = this.uiDialog.find(":tabbable"), n = i.filter(":first"), o = i.filter(":last");
            e.target !== o[0] && e.target !== this.uiDialog[0] || e.shiftKey ? e.target !== n[0] && e.target !== this.uiDialog[0] || !e.shiftKey || (this._delay(function() {
              o.focus();
            }), e.preventDefault()) : (this._delay(function() {
              n.focus();
            }), e.preventDefault());
          }
        },
        mousedown: function(t) {
          this._moveToTop(t) && this._focusTabbable();
        }
      }), this.element.find("[aria-describedby]").length || this.uiDialog.attr({
        "aria-describedby": this.element.uniqueId().attr("id")
      });
    },
    _createTitlebar: function() {
      var e;
      this.uiDialogTitlebar = t("<div>").addClass("ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix").prependTo(this.uiDialog), 
      this._on(this.uiDialogTitlebar, {
        mousedown: function(e) {
          t(e.target).closest(".ui-dialog-titlebar-close") || this.uiDialog.focus();
        }
      }), this.uiDialogTitlebarClose = t("<button type='button'></button>").button({
        label: this.options.closeText,
        icons: {
          primary: "ui-icon-closethick"
        },
        text: !1
      }).addClass("ui-dialog-titlebar-close").appendTo(this.uiDialogTitlebar), this._on(this.uiDialogTitlebarClose, {
        click: function(t) {
          t.preventDefault(), this.close(t);
        }
      }), e = t("<span>").uniqueId().addClass("ui-dialog-title").prependTo(this.uiDialogTitlebar), 
      this._title(e), this.uiDialog.attr({
        "aria-labelledby": e.attr("id")
      });
    },
    _title: function(t) {
      this.options.title || t.html("&#160;"), t.text(this.options.title);
    },
    _createButtonPane: function() {
      this.uiDialogButtonPane = t("<div>").addClass("ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"), 
      this.uiButtonSet = t("<div>").addClass("ui-dialog-buttonset").appendTo(this.uiDialogButtonPane), 
      this._createButtons();
    },
    _createButtons: function() {
      var e = this, i = this.options.buttons;
      return this.uiDialogButtonPane.remove(), this.uiButtonSet.empty(), t.isEmptyObject(i) || t.isArray(i) && !i.length ? void this.uiDialog.removeClass("ui-dialog-buttons") : (t.each(i, function(i, n) {
        var o, s;
        n = t.isFunction(n) ? {
          click: n,
          text: i
        } : n, n = t.extend({
          type: "button"
        }, n), o = n.click, n.click = function() {
          o.apply(e.element[0], arguments);
        }, s = {
          icons: n.icons,
          text: n.showText
        }, delete n.icons, delete n.showText, t("<button></button>", n).button(s).appendTo(e.uiButtonSet);
      }), this.uiDialog.addClass("ui-dialog-buttons"), void this.uiDialogButtonPane.appendTo(this.uiDialog));
    },
    _makeDraggable: function() {
      function e(t) {
        return {
          position: t.position,
          offset: t.offset
        };
      }
      var i = this, n = this.options;
      this.uiDialog.draggable({
        cancel: ".ui-dialog-content, .ui-dialog-titlebar-close",
        handle: ".ui-dialog-titlebar",
        containment: "document",
        start: function(n, o) {
          t(this).addClass("ui-dialog-dragging"), i._blockFrames(), i._trigger("dragStart", n, e(o));
        },
        drag: function(t, n) {
          i._trigger("drag", t, e(n));
        },
        stop: function(o, s) {
          var a = s.offset.left - i.document.scrollLeft(), r = s.offset.top - i.document.scrollTop();
          n.position = {
            my: "left top",
            at: "left" + (a >= 0 ? "+" : "") + a + " top" + (r >= 0 ? "+" : "") + r,
            of: i.window
          }, t(this).removeClass("ui-dialog-dragging"), i._unblockFrames(), i._trigger("dragStop", o, e(s));
        }
      });
    },
    _makeResizable: function() {
      function e(t) {
        return {
          originalPosition: t.originalPosition,
          originalSize: t.originalSize,
          position: t.position,
          size: t.size
        };
      }
      var i = this, n = this.options, o = n.resizable, s = this.uiDialog.css("position"), a = "string" == typeof o ? o : "n,e,s,w,se,sw,ne,nw";
      this.uiDialog.resizable({
        cancel: ".ui-dialog-content",
        containment: "document",
        alsoResize: this.element,
        maxWidth: n.maxWidth,
        maxHeight: n.maxHeight,
        minWidth: n.minWidth,
        minHeight: this._minHeight(),
        handles: a,
        start: function(n, o) {
          t(this).addClass("ui-dialog-resizing"), i._blockFrames(), i._trigger("resizeStart", n, e(o));
        },
        resize: function(t, n) {
          i._trigger("resize", t, e(n));
        },
        stop: function(o, s) {
          var a = i.uiDialog.offset(), r = a.left - i.document.scrollLeft(), l = a.top - i.document.scrollTop();
          n.height = i.uiDialog.height(), n.width = i.uiDialog.width(), n.position = {
            my: "left top",
            at: "left" + (r >= 0 ? "+" : "") + r + " top" + (l >= 0 ? "+" : "") + l,
            of: i.window
          }, t(this).removeClass("ui-dialog-resizing"), i._unblockFrames(), i._trigger("resizeStop", o, e(s));
        }
      }).css("position", s);
    },
    _trackFocus: function() {
      this._on(this.widget(), {
        focusin: function(e) {
          this._makeFocusTarget(), this._focusedElement = t(e.target);
        }
      });
    },
    _makeFocusTarget: function() {
      this._untrackInstance(), this._trackingInstances().unshift(this);
    },
    _untrackInstance: function() {
      var e = this._trackingInstances(), i = t.inArray(this, e);
      -1 !== i && e.splice(i, 1);
    },
    _trackingInstances: function() {
      var t = this.document.data("ui-dialog-instances");
      return t || (t = [], this.document.data("ui-dialog-instances", t)), t;
    },
    _minHeight: function() {
      var t = this.options;
      return "auto" === t.height ? t.minHeight : Math.min(t.minHeight, t.height);
    },
    _position: function() {
      var t = this.uiDialog.is(":visible");
      t || this.uiDialog.show(), this.uiDialog.position(this.options.position), t || this.uiDialog.hide();
    },
    _setOptions: function(e) {
      var i = this, n = !1, o = {};
      t.each(e, function(t, e) {
        i._setOption(t, e), t in i.sizeRelatedOptions && (n = !0), t in i.resizableRelatedOptions && (o[t] = e);
      }), n && (this._size(), this._position()), this.uiDialog.is(":data(ui-resizable)") && this.uiDialog.resizable("option", o);
    },
    _setOption: function(t, e) {
      var i, n, o = this.uiDialog;
      "dialogClass" === t && o.removeClass(this.options.dialogClass).addClass(e), "disabled" !== t && (this._super(t, e), 
      "appendTo" === t && this.uiDialog.appendTo(this._appendTo()), "buttons" === t && this._createButtons(), 
      "closeText" === t && this.uiDialogTitlebarClose.button({
        label: "" + e
      }), "draggable" === t && ((i = o.is(":data(ui-draggable)")) && !e && o.draggable("destroy"), 
      !i && e && this._makeDraggable()), "position" === t && this._position(), "resizable" === t && ((n = o.is(":data(ui-resizable)")) && !e && o.resizable("destroy"), 
      n && "string" == typeof e && o.resizable("option", "handles", e), n || !1 === e || this._makeResizable()), 
      "title" === t && this._title(this.uiDialogTitlebar.find(".ui-dialog-title")));
    },
    _size: function() {
      var t, e, i, n = this.options;
      this.element.show().css({
        width: "auto",
        minHeight: 0,
        maxHeight: "none",
        height: 0
      }), n.minWidth > n.width && (n.width = n.minWidth), t = this.uiDialog.css({
        height: "auto",
        width: n.width
      }).outerHeight(), e = Math.max(0, n.minHeight - t), i = "number" == typeof n.maxHeight ? Math.max(0, n.maxHeight - t) : "none", 
      "auto" === n.height ? this.element.css({
        minHeight: e,
        maxHeight: i,
        height: "auto"
      }) : this.element.height(Math.max(0, n.height - t)), this.uiDialog.is(":data(ui-resizable)") && this.uiDialog.resizable("option", "minHeight", this._minHeight());
    },
    _blockFrames: function() {
      this.iframeBlocks = this.document.find("iframe").map(function() {
        var e = t(this);
        return t("<div>").css({
          position: "absolute",
          width: e.outerWidth(),
          height: e.outerHeight()
        }).appendTo(e.parent()).offset(e.offset())[0];
      });
    },
    _unblockFrames: function() {
      this.iframeBlocks && (this.iframeBlocks.remove(), delete this.iframeBlocks);
    },
    _allowInteraction: function(e) {
      return !!t(e.target).closest(".ui-dialog").length || !!t(e.target).closest(".ui-datepicker").length;
    },
    _createOverlay: function() {
      if (this.options.modal) {
        var e = !0;
        this._delay(function() {
          e = !1;
        }), this.document.data("ui-dialog-overlays") || this._on(this.document, {
          focusin: function(t) {
            e || this._allowInteraction(t) || (t.preventDefault(), this._trackingInstances()[0]._focusTabbable());
          }
        }), this.overlay = t("<div>").addClass("ui-widget-overlay ui-front").appendTo(this._appendTo()), 
        this._on(this.overlay, {
          mousedown: "_keepFocus"
        }), this.document.data("ui-dialog-overlays", (this.document.data("ui-dialog-overlays") || 0) + 1);
      }
    },
    _destroyOverlay: function() {
      if (this.options.modal && this.overlay) {
        var t = this.document.data("ui-dialog-overlays") - 1;
        t ? this.document.data("ui-dialog-overlays", t) : this.document.unbind("focusin").removeData("ui-dialog-overlays"), 
        this.overlay.remove(), this.overlay = null;
      }
    }
  }), t.widget("ui.progressbar", {
    version: "1.11.2",
    options: {
      max: 100,
      value: 0,
      change: null,
      complete: null
    },
    min: 0,
    _create: function() {
      this.oldValue = this.options.value = this._constrainedValue(), this.element.addClass("ui-progressbar ui-widget ui-widget-content ui-corner-all").attr({
        role: "progressbar",
        "aria-valuemin": this.min
      }), this.valueDiv = t("<div class='ui-progressbar-value ui-widget-header ui-corner-left'></div>").appendTo(this.element), 
      this._refreshValue();
    },
    _destroy: function() {
      this.element.removeClass("ui-progressbar ui-widget ui-widget-content ui-corner-all").removeAttr("role").removeAttr("aria-valuemin").removeAttr("aria-valuemax").removeAttr("aria-valuenow"), 
      this.valueDiv.remove();
    },
    value: function(t) {
      return void 0 === t ? this.options.value : (this.options.value = this._constrainedValue(t), 
      void this._refreshValue());
    },
    _constrainedValue: function(t) {
      return void 0 === t && (t = this.options.value), this.indeterminate = !1 === t, 
      "number" != typeof t && (t = 0), !this.indeterminate && Math.min(this.options.max, Math.max(this.min, t));
    },
    _setOptions: function(t) {
      var e = t.value;
      delete t.value, this._super(t), this.options.value = this._constrainedValue(e), 
      this._refreshValue();
    },
    _setOption: function(t, e) {
      "max" === t && (e = Math.max(this.min, e)), "disabled" === t && this.element.toggleClass("ui-state-disabled", !!e).attr("aria-disabled", e), 
      this._super(t, e);
    },
    _percentage: function() {
      return this.indeterminate ? 100 : 100 * (this.options.value - this.min) / (this.options.max - this.min);
    },
    _refreshValue: function() {
      var e = this.options.value, i = this._percentage();
      this.valueDiv.toggle(this.indeterminate || e > this.min).toggleClass("ui-corner-right", e === this.options.max).width(i.toFixed(0) + "%"), 
      this.element.toggleClass("ui-progressbar-indeterminate", this.indeterminate), this.indeterminate ? (this.element.removeAttr("aria-valuenow"), 
      this.overlayDiv || (this.overlayDiv = t("<div class='ui-progressbar-overlay'></div>").appendTo(this.valueDiv))) : (this.element.attr({
        "aria-valuemax": this.options.max,
        "aria-valuenow": e
      }), this.overlayDiv && (this.overlayDiv.remove(), this.overlayDiv = null)), this.oldValue !== e && (this.oldValue = e, 
      this._trigger("change")), e === this.options.max && this._trigger("complete");
    }
  }), t.widget("ui.selectmenu", {
    version: "1.11.2",
    defaultElement: "<select>",
    options: {
      appendTo: null,
      disabled: null,
      icons: {
        button: "ui-icon-triangle-1-s"
      },
      position: {
        my: "left top",
        at: "left bottom",
        collision: "none"
      },
      width: null,
      change: null,
      close: null,
      focus: null,
      open: null,
      select: null
    },
    _create: function() {
      var t = this.element.uniqueId().attr("id");
      this.ids = {
        element: t,
        button: t + "-button",
        menu: t + "-menu"
      }, this._drawButton(), this._drawMenu(), this.options.disabled && this.disable();
    },
    _drawButton: function() {
      var e = this, i = this.element.attr("tabindex");
      this.label = t("label[for='" + this.ids.element + "']").attr("for", this.ids.button), 
      this._on(this.label, {
        click: function(t) {
          this.button.focus(), t.preventDefault();
        }
      }), this.element.hide(), this.button = t("<span>", {
        class: "ui-selectmenu-button ui-widget ui-state-default ui-corner-all",
        tabindex: i || this.options.disabled ? -1 : 0,
        id: this.ids.button,
        role: "combobox",
        "aria-expanded": "false",
        "aria-autocomplete": "list",
        "aria-owns": this.ids.menu,
        "aria-haspopup": "true"
      }).insertAfter(this.element), t("<span>", {
        class: "ui-icon " + this.options.icons.button
      }).prependTo(this.button), this.buttonText = t("<span>", {
        class: "ui-selectmenu-text"
      }).appendTo(this.button), this._setText(this.buttonText, this.element.find("option:selected").text()), 
      this._resizeButton(), this._on(this.button, this._buttonEvents), this.button.one("focusin", function() {
        e.menuItems || e._refreshMenu();
      }), this._hoverable(this.button), this._focusable(this.button);
    },
    _drawMenu: function() {
      var e = this;
      this.menu = t("<ul>", {
        "aria-hidden": "true",
        "aria-labelledby": this.ids.button,
        id: this.ids.menu
      }), this.menuWrap = t("<div>", {
        class: "ui-selectmenu-menu ui-front"
      }).append(this.menu).appendTo(this._appendTo()), this.menuInstance = this.menu.menu({
        role: "listbox",
        select: function(t, i) {
          t.preventDefault(), e._setSelection(), e._select(i.item.data("ui-selectmenu-item"), t);
        },
        focus: function(t, i) {
          var n = i.item.data("ui-selectmenu-item");
          null != e.focusIndex && n.index !== e.focusIndex && (e._trigger("focus", t, {
            item: n
          }), e.isOpen || e._select(n, t)), e.focusIndex = n.index, e.button.attr("aria-activedescendant", e.menuItems.eq(n.index).attr("id"));
        }
      }).menu("instance"), this.menu.addClass("ui-corner-bottom").removeClass("ui-corner-all"), 
      this.menuInstance._off(this.menu, "mouseleave"), this.menuInstance._closeOnDocumentClick = function() {
        return !1;
      }, this.menuInstance._isDivider = function() {
        return !1;
      };
    },
    refresh: function() {
      this._refreshMenu(), this._setText(this.buttonText, this._getSelectedItem().text()), 
      this.options.width || this._resizeButton();
    },
    _refreshMenu: function() {
      this.menu.empty();
      var t, e = this.element.find("option");
      e.length && (this._parseOptions(e), this._renderMenu(this.menu, this.items), this.menuInstance.refresh(), 
      this.menuItems = this.menu.find("li").not(".ui-selectmenu-optgroup"), t = this._getSelectedItem(), 
      this.menuInstance.focus(null, t), this._setAria(t.data("ui-selectmenu-item")), this._setOption("disabled", this.element.prop("disabled")));
    },
    open: function(t) {
      this.options.disabled || (this.menuItems ? (this.menu.find(".ui-state-focus").removeClass("ui-state-focus"), 
      this.menuInstance.focus(null, this._getSelectedItem())) : this._refreshMenu(), this.isOpen = !0, 
      this._toggleAttr(), this._resizeMenu(), this._position(), this._on(this.document, this._documentClick), 
      this._trigger("open", t));
    },
    _position: function() {
      this.menuWrap.position(t.extend({
        of: this.button
      }, this.options.position));
    },
    close: function(t) {
      this.isOpen && (this.isOpen = !1, this._toggleAttr(), this.range = null, this._off(this.document), 
      this._trigger("close", t));
    },
    widget: function() {
      return this.button;
    },
    menuWidget: function() {
      return this.menu;
    },
    _renderMenu: function(e, i) {
      var n = this, o = "";
      t.each(i, function(i, s) {
        s.optgroup !== o && (t("<li>", {
          class: "ui-selectmenu-optgroup ui-menu-divider" + (s.element.parent("optgroup").prop("disabled") ? " ui-state-disabled" : ""),
          text: s.optgroup
        }).appendTo(e), o = s.optgroup), n._renderItemData(e, s);
      });
    },
    _renderItemData: function(t, e) {
      return this._renderItem(t, e).data("ui-selectmenu-item", e);
    },
    _renderItem: function(e, i) {
      var n = t("<li>");
      return i.disabled && n.addClass("ui-state-disabled"), this._setText(n, i.label), 
      n.appendTo(e);
    },
    _setText: function(t, e) {
      e ? t.text(e) : t.html("&#160;");
    },
    _move: function(t, e) {
      var i, n, o = ".ui-menu-item";
      this.isOpen ? i = this.menuItems.eq(this.focusIndex) : (i = this.menuItems.eq(this.element[0].selectedIndex), 
      o += ":not(.ui-state-disabled)"), (n = "first" === t || "last" === t ? i["first" === t ? "prevAll" : "nextAll"](o).eq(-1) : i[t + "All"](o).eq(0)).length && this.menuInstance.focus(e, n);
    },
    _getSelectedItem: function() {
      return this.menuItems.eq(this.element[0].selectedIndex);
    },
    _toggle: function(t) {
      this[this.isOpen ? "close" : "open"](t);
    },
    _setSelection: function() {
      var t;
      this.range && (window.getSelection ? ((t = window.getSelection()).removeAllRanges(), 
      t.addRange(this.range)) : this.range.select(), this.button.focus());
    },
    _documentClick: {
      mousedown: function(e) {
        this.isOpen && (t(e.target).closest(".ui-selectmenu-menu, #" + this.ids.button).length || this.close(e));
      }
    },
    _buttonEvents: {
      mousedown: function() {
        var t;
        window.getSelection ? (t = window.getSelection()).rangeCount && (this.range = t.getRangeAt(0)) : this.range = document.selection.createRange();
      },
      click: function(t) {
        this._setSelection(), this._toggle(t);
      },
      keydown: function(e) {
        var i = !0;
        switch (e.keyCode) {
         case t.ui.keyCode.TAB:
         case t.ui.keyCode.ESCAPE:
          this.close(e), i = !1;
          break;

         case t.ui.keyCode.ENTER:
          this.isOpen && this._selectFocusedItem(e);
          break;

         case t.ui.keyCode.UP:
          e.altKey ? this._toggle(e) : this._move("prev", e);
          break;

         case t.ui.keyCode.DOWN:
          e.altKey ? this._toggle(e) : this._move("next", e);
          break;

         case t.ui.keyCode.SPACE:
          this.isOpen ? this._selectFocusedItem(e) : this._toggle(e);
          break;

         case t.ui.keyCode.LEFT:
          this._move("prev", e);
          break;

         case t.ui.keyCode.RIGHT:
          this._move("next", e);
          break;

         case t.ui.keyCode.HOME:
         case t.ui.keyCode.PAGE_UP:
          this._move("first", e);
          break;

         case t.ui.keyCode.END:
         case t.ui.keyCode.PAGE_DOWN:
          this._move("last", e);
          break;

         default:
          this.menu.trigger(e), i = !1;
        }
        i && e.preventDefault();
      }
    },
    _selectFocusedItem: function(t) {
      var e = this.menuItems.eq(this.focusIndex);
      e.hasClass("ui-state-disabled") || this._select(e.data("ui-selectmenu-item"), t);
    },
    _select: function(t, e) {
      var i = this.element[0].selectedIndex;
      this.element[0].selectedIndex = t.index, this._setText(this.buttonText, t.label), 
      this._setAria(t), this._trigger("select", e, {
        item: t
      }), t.index !== i && this._trigger("change", e, {
        item: t
      }), this.close(e);
    },
    _setAria: function(t) {
      var e = this.menuItems.eq(t.index).attr("id");
      this.button.attr({
        "aria-labelledby": e,
        "aria-activedescendant": e
      }), this.menu.attr("aria-activedescendant", e);
    },
    _setOption: function(t, e) {
      "icons" === t && this.button.find("span.ui-icon").removeClass(this.options.icons.button).addClass(e.button), 
      this._super(t, e), "appendTo" === t && this.menuWrap.appendTo(this._appendTo()), 
      "disabled" === t && (this.menuInstance.option("disabled", e), this.button.toggleClass("ui-state-disabled", e).attr("aria-disabled", e), 
      this.element.prop("disabled", e), e ? (this.button.attr("tabindex", -1), this.close()) : this.button.attr("tabindex", 0)), 
      "width" === t && this._resizeButton();
    },
    _appendTo: function() {
      var e = this.options.appendTo;
      return e && (e = e.jquery || e.nodeType ? t(e) : this.document.find(e).eq(0)), e && e[0] || (e = this.element.closest(".ui-front")), 
      e.length || (e = this.document[0].body), e;
    },
    _toggleAttr: function() {
      this.button.toggleClass("ui-corner-top", this.isOpen).toggleClass("ui-corner-all", !this.isOpen).attr("aria-expanded", this.isOpen), 
      this.menuWrap.toggleClass("ui-selectmenu-open", this.isOpen), this.menu.attr("aria-hidden", !this.isOpen);
    },
    _resizeButton: function() {
      var t = this.options.width;
      t || (t = this.element.show().outerWidth(), this.element.hide()), this.button.outerWidth(t);
    },
    _resizeMenu: function() {
      this.menu.outerWidth(Math.max(this.button.outerWidth(), this.menu.width("").outerWidth() + 1));
    },
    _getCreateOptions: function() {
      return {
        disabled: this.element.prop("disabled")
      };
    },
    _parseOptions: function(e) {
      var i = [];
      e.each(function(e, n) {
        var o = t(n), s = o.parent("optgroup");
        i.push({
          element: o,
          index: e,
          value: o.attr("value"),
          label: o.text(),
          optgroup: s.attr("label") || "",
          disabled: s.prop("disabled") || o.prop("disabled")
        });
      }), this.items = i;
    },
    _destroy: function() {
      this.menuWrap.remove(), this.button.remove(), this.element.show(), this.element.removeUniqueId(), 
      this.label.attr("for", this.ids.element);
    }
  }), t.widget("ui.slider", t.ui.mouse, {
    version: "1.11.2",
    widgetEventPrefix: "slide",
    options: {
      animate: !1,
      distance: 0,
      max: 100,
      min: 0,
      orientation: "horizontal",
      range: !1,
      step: 1,
      value: 0,
      values: null,
      change: null,
      slide: null,
      start: null,
      stop: null
    },
    numPages: 5,
    _create: function() {
      this._keySliding = !1, this._mouseSliding = !1, this._animateOff = !0, this._handleIndex = null, 
      this._detectOrientation(), this._mouseInit(), this._calculateNewMax(), this.element.addClass("ui-slider ui-slider-" + this.orientation + " ui-widget ui-widget-content ui-corner-all"), 
      this._refresh(), this._setOption("disabled", this.options.disabled), this._animateOff = !1;
    },
    _refresh: function() {
      this._createRange(), this._createHandles(), this._setupEvents(), this._refreshValue();
    },
    _createHandles: function() {
      var e, i, n = this.options, o = this.element.find(".ui-slider-handle").addClass("ui-state-default ui-corner-all"), s = [];
      for (i = n.values && n.values.length || 1, o.length > i && (o.slice(i).remove(), 
      o = o.slice(0, i)), e = o.length; i > e; e++) s.push("<span class='ui-slider-handle ui-state-default ui-corner-all' tabindex='0'></span>");
      this.handles = o.add(t(s.join("")).appendTo(this.element)), this.handle = this.handles.eq(0), 
      this.handles.each(function(e) {
        t(this).data("ui-slider-handle-index", e);
      });
    },
    _createRange: function() {
      var e = this.options, i = "";
      e.range ? (!0 === e.range && (e.values ? e.values.length && 2 !== e.values.length ? e.values = [ e.values[0], e.values[0] ] : t.isArray(e.values) && (e.values = e.values.slice(0)) : e.values = [ this._valueMin(), this._valueMin() ]), 
      this.range && this.range.length ? this.range.removeClass("ui-slider-range-min ui-slider-range-max").css({
        left: "",
        bottom: ""
      }) : (this.range = t("<div></div>").appendTo(this.element), i = "ui-slider-range ui-widget-header ui-corner-all"), 
      this.range.addClass(i + ("min" === e.range || "max" === e.range ? " ui-slider-range-" + e.range : ""))) : (this.range && this.range.remove(), 
      this.range = null);
    },
    _setupEvents: function() {
      this._off(this.handles), this._on(this.handles, this._handleEvents), this._hoverable(this.handles), 
      this._focusable(this.handles);
    },
    _destroy: function() {
      this.handles.remove(), this.range && this.range.remove(), this.element.removeClass("ui-slider ui-slider-horizontal ui-slider-vertical ui-widget ui-widget-content ui-corner-all"), 
      this._mouseDestroy();
    },
    _mouseCapture: function(e) {
      var i, n, o, s, a, r, l, c = this, d = this.options;
      return !d.disabled && (this.elementSize = {
        width: this.element.outerWidth(),
        height: this.element.outerHeight()
      }, this.elementOffset = this.element.offset(), i = {
        x: e.pageX,
        y: e.pageY
      }, n = this._normValueFromMouse(i), o = this._valueMax() - this._valueMin() + 1, 
      this.handles.each(function(e) {
        var i = Math.abs(n - c.values(e));
        (o > i || o === i && (e === c._lastChangedValue || c.values(e) === d.min)) && (o = i, 
        s = t(this), a = e);
      }), !1 !== this._start(e, a) && (this._mouseSliding = !0, this._handleIndex = a, 
      s.addClass("ui-state-active").focus(), r = s.offset(), l = !t(e.target).parents().addBack().is(".ui-slider-handle"), 
      this._clickOffset = l ? {
        left: 0,
        top: 0
      } : {
        left: e.pageX - r.left - s.width() / 2,
        top: e.pageY - r.top - s.height() / 2 - (parseInt(s.css("borderTopWidth"), 10) || 0) - (parseInt(s.css("borderBottomWidth"), 10) || 0) + (parseInt(s.css("marginTop"), 10) || 0)
      }, this.handles.hasClass("ui-state-hover") || this._slide(e, a, n), this._animateOff = !0, 
      !0));
    },
    _mouseStart: function() {
      return !0;
    },
    _mouseDrag: function(t) {
      var e = {
        x: t.pageX,
        y: t.pageY
      }, i = this._normValueFromMouse(e);
      return this._slide(t, this._handleIndex, i), !1;
    },
    _mouseStop: function(t) {
      return this.handles.removeClass("ui-state-active"), this._mouseSliding = !1, this._stop(t, this._handleIndex), 
      this._change(t, this._handleIndex), this._handleIndex = null, this._clickOffset = null, 
      this._animateOff = !1, !1;
    },
    _detectOrientation: function() {
      this.orientation = "vertical" === this.options.orientation ? "vertical" : "horizontal";
    },
    _normValueFromMouse: function(t) {
      var e, i, n, o, s;
      return "horizontal" === this.orientation ? (e = this.elementSize.width, i = t.x - this.elementOffset.left - (this._clickOffset ? this._clickOffset.left : 0)) : (e = this.elementSize.height, 
      i = t.y - this.elementOffset.top - (this._clickOffset ? this._clickOffset.top : 0)), 
      (n = i / e) > 1 && (n = 1), 0 > n && (n = 0), "vertical" === this.orientation && (n = 1 - n), 
      o = this._valueMax() - this._valueMin(), s = this._valueMin() + n * o, this._trimAlignValue(s);
    },
    _start: function(t, e) {
      var i = {
        handle: this.handles[e],
        value: this.value()
      };
      return this.options.values && this.options.values.length && (i.value = this.values(e), 
      i.values = this.values()), this._trigger("start", t, i);
    },
    _slide: function(t, e, i) {
      var n, o, s;
      this.options.values && this.options.values.length ? (n = this.values(e ? 0 : 1), 
      2 === this.options.values.length && !0 === this.options.range && (0 === e && i > n || 1 === e && n > i) && (i = n), 
      i !== this.values(e) && (o = this.values(), o[e] = i, s = this._trigger("slide", t, {
        handle: this.handles[e],
        value: i,
        values: o
      }), n = this.values(e ? 0 : 1), !1 !== s && this.values(e, i))) : i !== this.value() && !1 !== (s = this._trigger("slide", t, {
        handle: this.handles[e],
        value: i
      })) && this.value(i);
    },
    _stop: function(t, e) {
      var i = {
        handle: this.handles[e],
        value: this.value()
      };
      this.options.values && this.options.values.length && (i.value = this.values(e), 
      i.values = this.values()), this._trigger("stop", t, i);
    },
    _change: function(t, e) {
      if (!this._keySliding && !this._mouseSliding) {
        var i = {
          handle: this.handles[e],
          value: this.value()
        };
        this.options.values && this.options.values.length && (i.value = this.values(e), 
        i.values = this.values()), this._lastChangedValue = e, this._trigger("change", t, i);
      }
    },
    value: function(t) {
      return arguments.length ? (this.options.value = this._trimAlignValue(t), this._refreshValue(), 
      void this._change(null, 0)) : this._value();
    },
    values: function(e, i) {
      var n, o, s;
      if (arguments.length > 1) return this.options.values[e] = this._trimAlignValue(i), 
      this._refreshValue(), void this._change(null, e);
      if (!arguments.length) return this._values();
      if (!t.isArray(arguments[0])) return this.options.values && this.options.values.length ? this._values(e) : this.value();
      for (n = this.options.values, o = arguments[0], s = 0; n.length > s; s += 1) n[s] = this._trimAlignValue(o[s]), 
      this._change(null, s);
      this._refreshValue();
    },
    _setOption: function(e, i) {
      var n, o = 0;
      switch ("range" === e && !0 === this.options.range && ("min" === i ? (this.options.value = this._values(0), 
      this.options.values = null) : "max" === i && (this.options.value = this._values(this.options.values.length - 1), 
      this.options.values = null)), t.isArray(this.options.values) && (o = this.options.values.length), 
      "disabled" === e && this.element.toggleClass("ui-state-disabled", !!i), this._super(e, i), 
      e) {
       case "orientation":
        this._detectOrientation(), this.element.removeClass("ui-slider-horizontal ui-slider-vertical").addClass("ui-slider-" + this.orientation), 
        this._refreshValue(), this.handles.css("horizontal" === i ? "bottom" : "left", "");
        break;

       case "value":
        this._animateOff = !0, this._refreshValue(), this._change(null, 0), this._animateOff = !1;
        break;

       case "values":
        for (this._animateOff = !0, this._refreshValue(), n = 0; o > n; n += 1) this._change(null, n);
        this._animateOff = !1;
        break;

       case "step":
       case "min":
       case "max":
        this._animateOff = !0, this._calculateNewMax(), this._refreshValue(), this._animateOff = !1;
        break;

       case "range":
        this._animateOff = !0, this._refresh(), this._animateOff = !1;
      }
    },
    _value: function() {
      var t = this.options.value;
      return t = this._trimAlignValue(t);
    },
    _values: function(t) {
      var e, i, n;
      if (arguments.length) return e = this.options.values[t], e = this._trimAlignValue(e);
      if (this.options.values && this.options.values.length) {
        for (i = this.options.values.slice(), n = 0; i.length > n; n += 1) i[n] = this._trimAlignValue(i[n]);
        return i;
      }
      return [];
    },
    _trimAlignValue: function(t) {
      if (this._valueMin() >= t) return this._valueMin();
      if (t >= this._valueMax()) return this._valueMax();
      var e = this.options.step > 0 ? this.options.step : 1, i = (t - this._valueMin()) % e, n = t - i;
      return 2 * Math.abs(i) >= e && (n += i > 0 ? e : -e), parseFloat(n.toFixed(5));
    },
    _calculateNewMax: function() {
      var t = (this.options.max - this._valueMin()) % this.options.step;
      this.max = this.options.max - t;
    },
    _valueMin: function() {
      return this.options.min;
    },
    _valueMax: function() {
      return this.max;
    },
    _refreshValue: function() {
      var e, i, n, o, s, a = this.options.range, r = this.options, l = this, c = !this._animateOff && r.animate, d = {};
      this.options.values && this.options.values.length ? this.handles.each(function(n) {
        i = (l.values(n) - l._valueMin()) / (l._valueMax() - l._valueMin()) * 100, d["horizontal" === l.orientation ? "left" : "bottom"] = i + "%", 
        t(this).stop(1, 1)[c ? "animate" : "css"](d, r.animate), !0 === l.options.range && ("horizontal" === l.orientation ? (0 === n && l.range.stop(1, 1)[c ? "animate" : "css"]({
          left: i + "%"
        }, r.animate), 1 === n && l.range[c ? "animate" : "css"]({
          width: i - e + "%"
        }, {
          queue: !1,
          duration: r.animate
        })) : (0 === n && l.range.stop(1, 1)[c ? "animate" : "css"]({
          bottom: i + "%"
        }, r.animate), 1 === n && l.range[c ? "animate" : "css"]({
          height: i - e + "%"
        }, {
          queue: !1,
          duration: r.animate
        }))), e = i;
      }) : (n = this.value(), o = this._valueMin(), s = this._valueMax(), i = s !== o ? (n - o) / (s - o) * 100 : 0, 
      d["horizontal" === this.orientation ? "left" : "bottom"] = i + "%", this.handle.stop(1, 1)[c ? "animate" : "css"](d, r.animate), 
      "min" === a && "horizontal" === this.orientation && this.range.stop(1, 1)[c ? "animate" : "css"]({
        width: i + "%"
      }, r.animate), "max" === a && "horizontal" === this.orientation && this.range[c ? "animate" : "css"]({
        width: 100 - i + "%"
      }, {
        queue: !1,
        duration: r.animate
      }), "min" === a && "vertical" === this.orientation && this.range.stop(1, 1)[c ? "animate" : "css"]({
        height: i + "%"
      }, r.animate), "max" === a && "vertical" === this.orientation && this.range[c ? "animate" : "css"]({
        height: 100 - i + "%"
      }, {
        queue: !1,
        duration: r.animate
      }));
    },
    _handleEvents: {
      keydown: function(e) {
        var i, n, o, s = t(e.target).data("ui-slider-handle-index");
        switch (e.keyCode) {
         case t.ui.keyCode.HOME:
         case t.ui.keyCode.END:
         case t.ui.keyCode.PAGE_UP:
         case t.ui.keyCode.PAGE_DOWN:
         case t.ui.keyCode.UP:
         case t.ui.keyCode.RIGHT:
         case t.ui.keyCode.DOWN:
         case t.ui.keyCode.LEFT:
          if (e.preventDefault(), !this._keySliding && (this._keySliding = !0, t(e.target).addClass("ui-state-active"), 
          !1 === this._start(e, s))) return;
        }
        switch (o = this.options.step, i = n = this.options.values && this.options.values.length ? this.values(s) : this.value(), 
        e.keyCode) {
         case t.ui.keyCode.HOME:
          n = this._valueMin();
          break;

         case t.ui.keyCode.END:
          n = this._valueMax();
          break;

         case t.ui.keyCode.PAGE_UP:
          n = this._trimAlignValue(i + (this._valueMax() - this._valueMin()) / this.numPages);
          break;

         case t.ui.keyCode.PAGE_DOWN:
          n = this._trimAlignValue(i - (this._valueMax() - this._valueMin()) / this.numPages);
          break;

         case t.ui.keyCode.UP:
         case t.ui.keyCode.RIGHT:
          if (i === this._valueMax()) return;
          n = this._trimAlignValue(i + o);
          break;

         case t.ui.keyCode.DOWN:
         case t.ui.keyCode.LEFT:
          if (i === this._valueMin()) return;
          n = this._trimAlignValue(i - o);
        }
        this._slide(e, s, n);
      },
      keyup: function(e) {
        var i = t(e.target).data("ui-slider-handle-index");
        this._keySliding && (this._keySliding = !1, this._stop(e, i), this._change(e, i), 
        t(e.target).removeClass("ui-state-active"));
      }
    }
  }), t.widget("ui.spinner", {
    version: "1.11.2",
    defaultElement: "<input>",
    widgetEventPrefix: "spin",
    options: {
      culture: null,
      icons: {
        down: "ui-icon-triangle-1-s",
        up: "ui-icon-triangle-1-n"
      },
      incremental: !0,
      max: null,
      min: null,
      numberFormat: null,
      page: 10,
      step: 1,
      change: null,
      spin: null,
      start: null,
      stop: null
    },
    _create: function() {
      this._setOption("max", this.options.max), this._setOption("min", this.options.min), 
      this._setOption("step", this.options.step), "" !== this.value() && this._value(this.element.val(), !0), 
      this._draw(), this._on(this._events), this._refresh(), this._on(this.window, {
        beforeunload: function() {
          this.element.removeAttr("autocomplete");
        }
      });
    },
    _getCreateOptions: function() {
      var e = {}, i = this.element;
      return t.each([ "min", "max", "step" ], function(t, n) {
        var o = i.attr(n);
        void 0 !== o && o.length && (e[n] = o);
      }), e;
    },
    _events: {
      keydown: function(t) {
        this._start(t) && this._keydown(t) && t.preventDefault();
      },
      keyup: "_stop",
      focus: function() {
        this.previous = this.element.val();
      },
      blur: function(t) {
        return this.cancelBlur ? void delete this.cancelBlur : (this._stop(), this._refresh(), 
        void (this.previous !== this.element.val() && this._trigger("change", t)));
      },
      mousewheel: function(t, e) {
        if (e) {
          if (!this.spinning && !this._start(t)) return !1;
          this._spin((e > 0 ? 1 : -1) * this.options.step, t), clearTimeout(this.mousewheelTimer), 
          this.mousewheelTimer = this._delay(function() {
            this.spinning && this._stop(t);
          }, 100), t.preventDefault();
        }
      },
      "mousedown .ui-spinner-button": function(e) {
        function i() {
          this.element[0] === this.document[0].activeElement || (this.element.focus(), this.previous = n, 
          this._delay(function() {
            this.previous = n;
          }));
        }
        var n;
        n = this.element[0] === this.document[0].activeElement ? this.previous : this.element.val(), 
        e.preventDefault(), i.call(this), this.cancelBlur = !0, this._delay(function() {
          delete this.cancelBlur, i.call(this);
        }), !1 !== this._start(e) && this._repeat(null, t(e.currentTarget).hasClass("ui-spinner-up") ? 1 : -1, e);
      },
      "mouseup .ui-spinner-button": "_stop",
      "mouseenter .ui-spinner-button": function(e) {
        return t(e.currentTarget).hasClass("ui-state-active") ? !1 !== this._start(e) && void this._repeat(null, t(e.currentTarget).hasClass("ui-spinner-up") ? 1 : -1, e) : void 0;
      },
      "mouseleave .ui-spinner-button": "_stop"
    },
    _draw: function() {
      var t = this.uiSpinner = this.element.addClass("ui-spinner-input").attr("autocomplete", "off").wrap(this._uiSpinnerHtml()).parent().append(this._buttonHtml());
      this.element.attr("role", "spinbutton"), this.buttons = t.find(".ui-spinner-button").attr("tabIndex", -1).button().removeClass("ui-corner-all"), 
      this.buttons.height() > Math.ceil(.5 * t.height()) && t.height() > 0 && t.height(t.height()), 
      this.options.disabled && this.disable();
    },
    _keydown: function(e) {
      var i = this.options, n = t.ui.keyCode;
      switch (e.keyCode) {
       case n.UP:
        return this._repeat(null, 1, e), !0;

       case n.DOWN:
        return this._repeat(null, -1, e), !0;

       case n.PAGE_UP:
        return this._repeat(null, i.page, e), !0;

       case n.PAGE_DOWN:
        return this._repeat(null, -i.page, e), !0;
      }
      return !1;
    },
    _uiSpinnerHtml: function() {
      return "<span class='ui-spinner ui-widget ui-widget-content ui-corner-all'></span>";
    },
    _buttonHtml: function() {
      return "<a class='ui-spinner-button ui-spinner-up ui-corner-tr'><span class='ui-icon " + this.options.icons.up + "'>&#9650;</span></a><a class='ui-spinner-button ui-spinner-down ui-corner-br'><span class='ui-icon " + this.options.icons.down + "'>&#9660;</span></a>";
    },
    _start: function(t) {
      return !(!this.spinning && !1 === this._trigger("start", t)) && (this.counter || (this.counter = 1), 
      this.spinning = !0, !0);
    },
    _repeat: function(t, e, i) {
      t = t || 500, clearTimeout(this.timer), this.timer = this._delay(function() {
        this._repeat(40, e, i);
      }, t), this._spin(e * this.options.step, i);
    },
    _spin: function(t, e) {
      var i = this.value() || 0;
      this.counter || (this.counter = 1), i = this._adjustValue(i + t * this._increment(this.counter)), 
      this.spinning && !1 === this._trigger("spin", e, {
        value: i
      }) || (this._value(i), this.counter++);
    },
    _increment: function(e) {
      var i = this.options.incremental;
      return i ? t.isFunction(i) ? i(e) : Math.floor(e * e * e / 5e4 - e * e / 500 + 17 * e / 200 + 1) : 1;
    },
    _precision: function() {
      var t = this._precisionOf(this.options.step);
      return null !== this.options.min && (t = Math.max(t, this._precisionOf(this.options.min))), 
      t;
    },
    _precisionOf: function(t) {
      var e = "" + t, i = e.indexOf(".");
      return -1 === i ? 0 : e.length - i - 1;
    },
    _adjustValue: function(t) {
      var e, i, n = this.options;
      return e = null !== n.min ? n.min : 0, i = t - e, i = Math.round(i / n.step) * n.step, 
      t = e + i, t = parseFloat(t.toFixed(this._precision())), null !== n.max && t > n.max ? n.max : null !== n.min && n.min > t ? n.min : t;
    },
    _stop: function(t) {
      this.spinning && (clearTimeout(this.timer), clearTimeout(this.mousewheelTimer), 
      this.counter = 0, this.spinning = !1, this._trigger("stop", t));
    },
    _setOption: function(t, e) {
      if ("culture" === t || "numberFormat" === t) {
        var i = this._parse(this.element.val());
        return this.options[t] = e, void this.element.val(this._format(i));
      }
      ("max" === t || "min" === t || "step" === t) && "string" == typeof e && (e = this._parse(e)), 
      "icons" === t && (this.buttons.first().find(".ui-icon").removeClass(this.options.icons.up).addClass(e.up), 
      this.buttons.last().find(".ui-icon").removeClass(this.options.icons.down).addClass(e.down)), 
      this._super(t, e), "disabled" === t && (this.widget().toggleClass("ui-state-disabled", !!e), 
      this.element.prop("disabled", !!e), this.buttons.button(e ? "disable" : "enable"));
    },
    _setOptions: l(function(t) {
      this._super(t);
    }),
    _parse: function(t) {
      return "string" == typeof t && "" !== t && (t = window.Globalize && this.options.numberFormat ? Globalize.parseFloat(t, 10, this.options.culture) : +t), 
      "" === t || isNaN(t) ? null : t;
    },
    _format: function(t) {
      return "" === t ? "" : window.Globalize && this.options.numberFormat ? Globalize.format(t, this.options.numberFormat, this.options.culture) : t;
    },
    _refresh: function() {
      this.element.attr({
        "aria-valuemin": this.options.min,
        "aria-valuemax": this.options.max,
        "aria-valuenow": this._parse(this.element.val())
      });
    },
    isValid: function() {
      var t = this.value();
      return null !== t && t === this._adjustValue(t);
    },
    _value: function(t, e) {
      var i;
      "" !== t && null !== (i = this._parse(t)) && (e || (i = this._adjustValue(i)), t = this._format(i)), 
      this.element.val(t), this._refresh();
    },
    _destroy: function() {
      this.element.removeClass("ui-spinner-input").prop("disabled", !1).removeAttr("autocomplete").removeAttr("role").removeAttr("aria-valuemin").removeAttr("aria-valuemax").removeAttr("aria-valuenow"), 
      this.uiSpinner.replaceWith(this.element);
    },
    stepUp: l(function(t) {
      this._stepUp(t);
    }),
    _stepUp: function(t) {
      this._start() && (this._spin((t || 1) * this.options.step), this._stop());
    },
    stepDown: l(function(t) {
      this._stepDown(t);
    }),
    _stepDown: function(t) {
      this._start() && (this._spin((t || 1) * -this.options.step), this._stop());
    },
    pageUp: l(function(t) {
      this._stepUp((t || 1) * this.options.page);
    }),
    pageDown: l(function(t) {
      this._stepDown((t || 1) * this.options.page);
    }),
    value: function(t) {
      return arguments.length ? void l(this._value).call(this, t) : this._parse(this.element.val());
    },
    widget: function() {
      return this.uiSpinner;
    }
  }), t.widget("ui.tabs", {
    version: "1.11.2",
    delay: 300,
    options: {
      active: null,
      collapsible: !1,
      event: "click",
      heightStyle: "content",
      hide: null,
      show: null,
      activate: null,
      beforeActivate: null,
      beforeLoad: null,
      load: null
    },
    _isLocal: function() {
      var t = /#.*$/;
      return function(e) {
        var i, n;
        i = (e = e.cloneNode(!1)).href.replace(t, ""), n = location.href.replace(t, "");
        try {
          i = decodeURIComponent(i);
        } catch (t) {}
        try {
          n = decodeURIComponent(n);
        } catch (t) {}
        return e.hash.length > 1 && i === n;
      };
    }(),
    _create: function() {
      var e = this, i = this.options;
      this.running = !1, this.element.addClass("ui-tabs ui-widget ui-widget-content ui-corner-all").toggleClass("ui-tabs-collapsible", i.collapsible), 
      this._processTabs(), i.active = this._initialActive(), t.isArray(i.disabled) && (i.disabled = t.unique(i.disabled.concat(t.map(this.tabs.filter(".ui-state-disabled"), function(t) {
        return e.tabs.index(t);
      }))).sort()), this.active = !1 !== this.options.active && this.anchors.length ? this._findActive(i.active) : t(), 
      this._refresh(), this.active.length && this.load(i.active);
    },
    _initialActive: function() {
      var e = this.options.active, i = this.options.collapsible, n = location.hash.substring(1);
      return null === e && (n && this.tabs.each(function(i, o) {
        return t(o).attr("aria-controls") === n ? (e = i, !1) : void 0;
      }), null === e && (e = this.tabs.index(this.tabs.filter(".ui-tabs-active"))), (null === e || -1 === e) && (e = !!this.tabs.length && 0)), 
      !1 !== e && -1 === (e = this.tabs.index(this.tabs.eq(e))) && (e = !i && 0), !i && !1 === e && this.anchors.length && (e = 0), 
      e;
    },
    _getCreateEventData: function() {
      return {
        tab: this.active,
        panel: this.active.length ? this._getPanelForTab(this.active) : t()
      };
    },
    _tabKeydown: function(e) {
      var i = t(this.document[0].activeElement).closest("li"), n = this.tabs.index(i), o = !0;
      if (!this._handlePageNav(e)) {
        switch (e.keyCode) {
         case t.ui.keyCode.RIGHT:
         case t.ui.keyCode.DOWN:
          n++;
          break;

         case t.ui.keyCode.UP:
         case t.ui.keyCode.LEFT:
          o = !1, n--;
          break;

         case t.ui.keyCode.END:
          n = this.anchors.length - 1;
          break;

         case t.ui.keyCode.HOME:
          n = 0;
          break;

         case t.ui.keyCode.SPACE:
          return e.preventDefault(), clearTimeout(this.activating), void this._activate(n);

         case t.ui.keyCode.ENTER:
          return e.preventDefault(), clearTimeout(this.activating), void this._activate(n !== this.options.active && n);

         default:
          return;
        }
        e.preventDefault(), clearTimeout(this.activating), n = this._focusNextTab(n, o), 
        e.ctrlKey || (i.attr("aria-selected", "false"), this.tabs.eq(n).attr("aria-selected", "true"), 
        this.activating = this._delay(function() {
          this.option("active", n);
        }, this.delay));
      }
    },
    _panelKeydown: function(e) {
      this._handlePageNav(e) || e.ctrlKey && e.keyCode === t.ui.keyCode.UP && (e.preventDefault(), 
      this.active.focus());
    },
    _handlePageNav: function(e) {
      return e.altKey && e.keyCode === t.ui.keyCode.PAGE_UP ? (this._activate(this._focusNextTab(this.options.active - 1, !1)), 
      !0) : e.altKey && e.keyCode === t.ui.keyCode.PAGE_DOWN ? (this._activate(this._focusNextTab(this.options.active + 1, !0)), 
      !0) : void 0;
    },
    _findNextTab: function(e, i) {
      for (var n = this.tabs.length - 1; -1 !== t.inArray((e > n && (e = 0), 0 > e && (e = n), 
      e), this.options.disabled); ) e = i ? e + 1 : e - 1;
      return e;
    },
    _focusNextTab: function(t, e) {
      return t = this._findNextTab(t, e), this.tabs.eq(t).focus(), t;
    },
    _setOption: function(t, e) {
      return "active" === t ? void this._activate(e) : "disabled" === t ? void this._setupDisabled(e) : (this._super(t, e), 
      "collapsible" === t && (this.element.toggleClass("ui-tabs-collapsible", e), e || !1 !== this.options.active || this._activate(0)), 
      "event" === t && this._setupEvents(e), void ("heightStyle" === t && this._setupHeightStyle(e)));
    },
    _sanitizeSelector: function(t) {
      return t ? t.replace(/[!"$%&'()*+,.\/:;<=>?@\[\]\^`{|}~]/g, "\\$&") : "";
    },
    refresh: function() {
      var e = this.options, i = this.tablist.children(":has(a[href])");
      e.disabled = t.map(i.filter(".ui-state-disabled"), function(t) {
        return i.index(t);
      }), this._processTabs(), !1 !== e.active && this.anchors.length ? this.active.length && !t.contains(this.tablist[0], this.active[0]) ? this.tabs.length === e.disabled.length ? (e.active = !1, 
      this.active = t()) : this._activate(this._findNextTab(Math.max(0, e.active - 1), !1)) : e.active = this.tabs.index(this.active) : (e.active = !1, 
      this.active = t()), this._refresh();
    },
    _refresh: function() {
      this._setupDisabled(this.options.disabled), this._setupEvents(this.options.event), 
      this._setupHeightStyle(this.options.heightStyle), this.tabs.not(this.active).attr({
        "aria-selected": "false",
        "aria-expanded": "false",
        tabIndex: -1
      }), this.panels.not(this._getPanelForTab(this.active)).hide().attr({
        "aria-hidden": "true"
      }), this.active.length ? (this.active.addClass("ui-tabs-active ui-state-active").attr({
        "aria-selected": "true",
        "aria-expanded": "true",
        tabIndex: 0
      }), this._getPanelForTab(this.active).show().attr({
        "aria-hidden": "false"
      })) : this.tabs.eq(0).attr("tabIndex", 0);
    },
    _processTabs: function() {
      var e = this, i = this.tabs, n = this.anchors, o = this.panels;
      this.tablist = this._getList().addClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all").attr("role", "tablist").delegate("> li", "mousedown" + this.eventNamespace, function(e) {
        t(this).is(".ui-state-disabled") && e.preventDefault();
      }).delegate(".ui-tabs-anchor", "focus" + this.eventNamespace, function() {
        t(this).closest("li").is(".ui-state-disabled") && this.blur();
      }), this.tabs = this.tablist.find("> li:has(a[href])").addClass("ui-state-default ui-corner-top").attr({
        role: "tab",
        tabIndex: -1
      }), this.anchors = this.tabs.map(function() {
        return t("a", this)[0];
      }).addClass("ui-tabs-anchor").attr({
        role: "presentation",
        tabIndex: -1
      }), this.panels = t(), this.anchors.each(function(i, n) {
        var o, s, a, r = t(n).uniqueId().attr("id"), l = t(n).closest("li"), c = l.attr("aria-controls");
        e._isLocal(n) ? (o = n.hash, a = o.substring(1), s = e.element.find(e._sanitizeSelector(o))) : (a = l.attr("aria-controls") || t({}).uniqueId()[0].id, 
        o = "#" + a, (s = e.element.find(o)).length || (s = e._createPanel(a)).insertAfter(e.panels[i - 1] || e.tablist), 
        s.attr("aria-live", "polite")), s.length && (e.panels = e.panels.add(s)), c && l.data("ui-tabs-aria-controls", c), 
        l.attr({
          "aria-controls": a,
          "aria-labelledby": r
        }), s.attr("aria-labelledby", r);
      }), this.panels.addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").attr("role", "tabpanel"), 
      i && (this._off(i.not(this.tabs)), this._off(n.not(this.anchors)), this._off(o.not(this.panels)));
    },
    _getList: function() {
      return this.tablist || this.element.find("ol,ul").eq(0);
    },
    _createPanel: function(e) {
      return t("<div>").attr("id", e).addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").data("ui-tabs-destroy", !0);
    },
    _setupDisabled: function(e) {
      t.isArray(e) && (e.length ? e.length === this.anchors.length && (e = !0) : e = !1);
      for (var i, n = 0; i = this.tabs[n]; n++) !0 === e || -1 !== t.inArray(n, e) ? t(i).addClass("ui-state-disabled").attr("aria-disabled", "true") : t(i).removeClass("ui-state-disabled").removeAttr("aria-disabled");
      this.options.disabled = e;
    },
    _setupEvents: function(e) {
      var i = {};
      e && t.each(e.split(" "), function(t, e) {
        i[e] = "_eventHandler";
      }), this._off(this.anchors.add(this.tabs).add(this.panels)), this._on(!0, this.anchors, {
        click: function(t) {
          t.preventDefault();
        }
      }), this._on(this.anchors, i), this._on(this.tabs, {
        keydown: "_tabKeydown"
      }), this._on(this.panels, {
        keydown: "_panelKeydown"
      }), this._focusable(this.tabs), this._hoverable(this.tabs);
    },
    _setupHeightStyle: function(e) {
      var i, n = this.element.parent();
      "fill" === e ? (i = n.height(), i -= this.element.outerHeight() - this.element.height(), 
      this.element.siblings(":visible").each(function() {
        var e = t(this), n = e.css("position");
        "absolute" !== n && "fixed" !== n && (i -= e.outerHeight(!0));
      }), this.element.children().not(this.panels).each(function() {
        i -= t(this).outerHeight(!0);
      }), this.panels.each(function() {
        t(this).height(Math.max(0, i - t(this).innerHeight() + t(this).height()));
      }).css("overflow", "auto")) : "auto" === e && (i = 0, this.panels.each(function() {
        i = Math.max(i, t(this).height("").height());
      }).height(i));
    },
    _eventHandler: function(e) {
      var i = this.options, n = this.active, o = t(e.currentTarget).closest("li"), s = o[0] === n[0], a = s && i.collapsible, r = a ? t() : this._getPanelForTab(o), l = n.length ? this._getPanelForTab(n) : t(), c = {
        oldTab: n,
        oldPanel: l,
        newTab: a ? t() : o,
        newPanel: r
      };
      e.preventDefault(), o.hasClass("ui-state-disabled") || o.hasClass("ui-tabs-loading") || this.running || s && !i.collapsible || !1 === this._trigger("beforeActivate", e, c) || (i.active = !a && this.tabs.index(o), 
      this.active = s ? t() : o, this.xhr && this.xhr.abort(), l.length || r.length || t.error("jQuery UI Tabs: Mismatching fragment identifier."), 
      r.length && this.load(this.tabs.index(o), e), this._toggle(e, c));
    },
    _toggle: function(e, i) {
      function n() {
        s.running = !1, s._trigger("activate", e, i);
      }
      function o() {
        i.newTab.closest("li").addClass("ui-tabs-active ui-state-active"), a.length && s.options.show ? s._show(a, s.options.show, n) : (a.show(), 
        n());
      }
      var s = this, a = i.newPanel, r = i.oldPanel;
      this.running = !0, r.length && this.options.hide ? this._hide(r, this.options.hide, function() {
        i.oldTab.closest("li").removeClass("ui-tabs-active ui-state-active"), o();
      }) : (i.oldTab.closest("li").removeClass("ui-tabs-active ui-state-active"), r.hide(), 
      o()), r.attr("aria-hidden", "true"), i.oldTab.attr({
        "aria-selected": "false",
        "aria-expanded": "false"
      }), a.length && r.length ? i.oldTab.attr("tabIndex", -1) : a.length && this.tabs.filter(function() {
        return 0 === t(this).attr("tabIndex");
      }).attr("tabIndex", -1), a.attr("aria-hidden", "false"), i.newTab.attr({
        "aria-selected": "true",
        "aria-expanded": "true",
        tabIndex: 0
      });
    },
    _activate: function(e) {
      var i, n = this._findActive(e);
      n[0] !== this.active[0] && (n.length || (n = this.active), i = n.find(".ui-tabs-anchor")[0], 
      this._eventHandler({
        target: i,
        currentTarget: i,
        preventDefault: t.noop
      }));
    },
    _findActive: function(e) {
      return !1 === e ? t() : this.tabs.eq(e);
    },
    _getIndex: function(t) {
      return "string" == typeof t && (t = this.anchors.index(this.anchors.filter("[href$='" + t + "']"))), 
      t;
    },
    _destroy: function() {
      this.xhr && this.xhr.abort(), this.element.removeClass("ui-tabs ui-widget ui-widget-content ui-corner-all ui-tabs-collapsible"), 
      this.tablist.removeClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all").removeAttr("role"), 
      this.anchors.removeClass("ui-tabs-anchor").removeAttr("role").removeAttr("tabIndex").removeUniqueId(), 
      this.tablist.unbind(this.eventNamespace), this.tabs.add(this.panels).each(function() {
        t.data(this, "ui-tabs-destroy") ? t(this).remove() : t(this).removeClass("ui-state-default ui-state-active ui-state-disabled ui-corner-top ui-corner-bottom ui-widget-content ui-tabs-active ui-tabs-panel").removeAttr("tabIndex").removeAttr("aria-live").removeAttr("aria-busy").removeAttr("aria-selected").removeAttr("aria-labelledby").removeAttr("aria-hidden").removeAttr("aria-expanded").removeAttr("role");
      }), this.tabs.each(function() {
        var e = t(this), i = e.data("ui-tabs-aria-controls");
        i ? e.attr("aria-controls", i).removeData("ui-tabs-aria-controls") : e.removeAttr("aria-controls");
      }), this.panels.show(), "content" !== this.options.heightStyle && this.panels.css("height", "");
    },
    enable: function(e) {
      var i = this.options.disabled;
      !1 !== i && (void 0 === e ? i = !1 : (e = this._getIndex(e), i = t.isArray(i) ? t.map(i, function(t) {
        return t !== e ? t : null;
      }) : t.map(this.tabs, function(t, i) {
        return i !== e ? i : null;
      })), this._setupDisabled(i));
    },
    disable: function(e) {
      var i = this.options.disabled;
      if (!0 !== i) {
        if (void 0 === e) i = !0; else {
          if (e = this._getIndex(e), -1 !== t.inArray(e, i)) return;
          i = t.isArray(i) ? t.merge([ e ], i).sort() : [ e ];
        }
        this._setupDisabled(i);
      }
    },
    load: function(e, i) {
      e = this._getIndex(e);
      var n = this, o = this.tabs.eq(e), s = o.find(".ui-tabs-anchor"), a = this._getPanelForTab(o), r = {
        tab: o,
        panel: a
      };
      this._isLocal(s[0]) || (this.xhr = t.ajax(this._ajaxSettings(s, i, r)), this.xhr && "canceled" !== this.xhr.statusText && (o.addClass("ui-tabs-loading"), 
      a.attr("aria-busy", "true"), this.xhr.success(function(t) {
        setTimeout(function() {
          a.html(t), n._trigger("load", i, r);
        }, 1);
      }).complete(function(t, e) {
        setTimeout(function() {
          "abort" === e && n.panels.stop(!1, !0), o.removeClass("ui-tabs-loading"), a.removeAttr("aria-busy"), 
          t === n.xhr && delete n.xhr;
        }, 1);
      })));
    },
    _ajaxSettings: function(e, i, n) {
      var o = this;
      return {
        url: e.attr("href"),
        beforeSend: function(e, s) {
          return o._trigger("beforeLoad", i, t.extend({
            jqXHR: e,
            ajaxSettings: s
          }, n));
        }
      };
    },
    _getPanelForTab: function(e) {
      var i = t(e).attr("aria-controls");
      return this.element.find(this._sanitizeSelector("#" + i));
    }
  }), t.widget("ui.tooltip", {
    version: "1.11.2",
    options: {
      content: function() {
        var e = t(this).attr("title") || "";
        return t("<a>").text(e).html();
      },
      hide: !0,
      items: "[title]:not([disabled])",
      position: {
        my: "left top+15",
        at: "left bottom",
        collision: "flipfit flip"
      },
      show: !0,
      tooltipClass: null,
      track: !1,
      close: null,
      open: null
    },
    _addDescribedBy: function(e, i) {
      var n = (e.attr("aria-describedby") || "").split(/\s+/);
      n.push(i), e.data("ui-tooltip-id", i).attr("aria-describedby", t.trim(n.join(" ")));
    },
    _removeDescribedBy: function(e) {
      var i = e.data("ui-tooltip-id"), n = (e.attr("aria-describedby") || "").split(/\s+/), o = t.inArray(i, n);
      -1 !== o && n.splice(o, 1), e.removeData("ui-tooltip-id"), (n = t.trim(n.join(" "))) ? e.attr("aria-describedby", n) : e.removeAttr("aria-describedby");
    },
    _create: function() {
      this._on({
        mouseover: "open",
        focusin: "open"
      }), this.tooltips = {}, this.parents = {}, this.options.disabled && this._disable(), 
      this.liveRegion = t("<div>").attr({
        role: "log",
        "aria-live": "assertive",
        "aria-relevant": "additions"
      }).addClass("ui-helper-hidden-accessible").appendTo(this.document[0].body);
    },
    _setOption: function(e, i) {
      var n = this;
      return "disabled" === e ? (this[i ? "_disable" : "_enable"](), void (this.options[e] = i)) : (this._super(e, i), 
      void ("content" === e && t.each(this.tooltips, function(t, e) {
        n._updateContent(e.element);
      })));
    },
    _disable: function() {
      var e = this;
      t.each(this.tooltips, function(i, n) {
        var o = t.Event("blur");
        o.target = o.currentTarget = n.element[0], e.close(o, !0);
      }), this.element.find(this.options.items).addBack().each(function() {
        var e = t(this);
        e.is("[title]") && e.data("ui-tooltip-title", e.attr("title")).removeAttr("title");
      });
    },
    _enable: function() {
      this.element.find(this.options.items).addBack().each(function() {
        var e = t(this);
        e.data("ui-tooltip-title") && e.attr("title", e.data("ui-tooltip-title"));
      });
    },
    open: function(e) {
      var i = this, n = t(e ? e.target : this.element).closest(this.options.items);
      n.length && !n.data("ui-tooltip-id") && (n.attr("title") && n.data("ui-tooltip-title", n.attr("title")), 
      n.data("ui-tooltip-open", !0), e && "mouseover" === e.type && n.parents().each(function() {
        var e, n = t(this);
        n.data("ui-tooltip-open") && (e = t.Event("blur"), e.target = e.currentTarget = this, 
        i.close(e, !0)), n.attr("title") && (n.uniqueId(), i.parents[this.id] = {
          element: this,
          title: n.attr("title")
        }, n.attr("title", ""));
      }), this._updateContent(n, e));
    },
    _updateContent: function(t, e) {
      var i, n = this.options.content, o = this, s = e ? e.type : null;
      return "string" == typeof n ? this._open(e, t, n) : void ((i = n.call(t[0], function(i) {
        t.data("ui-tooltip-open") && o._delay(function() {
          e && (e.type = s), this._open(e, t, i);
        });
      })) && this._open(e, t, i));
    },
    _open: function(e, i, n) {
      function o(t) {
        d.of = t, a.is(":hidden") || a.position(d);
      }
      var s, a, r, l, c, d = t.extend({}, this.options.position);
      if (n) {
        if (s = this._find(i)) return void s.tooltip.find(".ui-tooltip-content").html(n);
        i.is("[title]") && (e && "mouseover" === e.type ? i.attr("title", "") : i.removeAttr("title")), 
        s = this._tooltip(i), a = s.tooltip, this._addDescribedBy(i, a.attr("id")), a.find(".ui-tooltip-content").html(n), 
        this.liveRegion.children().hide(), n.clone ? (c = n.clone()).removeAttr("id").find("[id]").removeAttr("id") : c = n, 
        t("<div>").html(c).appendTo(this.liveRegion), this.options.track && e && /^mouse/.test(e.type) ? (this._on(this.document, {
          mousemove: o
        }), o(e)) : a.position(t.extend({
          of: i
        }, this.options.position)), a.hide(), this._show(a, this.options.show), this.options.show && this.options.show.delay && (l = this.delayedShow = setInterval(function() {
          a.is(":visible") && (o(d.of), clearInterval(l));
        }, t.fx.interval)), this._trigger("open", e, {
          tooltip: a
        }), r = {
          keyup: function(e) {
            if (e.keyCode === t.ui.keyCode.ESCAPE) {
              var n = t.Event(e);
              n.currentTarget = i[0], this.close(n, !0);
            }
          }
        }, i[0] !== this.element[0] && (r.remove = function() {
          this._removeTooltip(a);
        }), e && "mouseover" !== e.type || (r.mouseleave = "close"), e && "focusin" !== e.type || (r.focusout = "close"), 
        this._on(!0, i, r);
      }
    },
    close: function(e) {
      var i, n = this, o = t(e ? e.currentTarget : this.element), s = this._find(o);
      s && (i = s.tooltip, s.closing || (clearInterval(this.delayedShow), o.data("ui-tooltip-title") && !o.attr("title") && o.attr("title", o.data("ui-tooltip-title")), 
      this._removeDescribedBy(o), s.hiding = !0, i.stop(!0), this._hide(i, this.options.hide, function() {
        n._removeTooltip(t(this));
      }), o.removeData("ui-tooltip-open"), this._off(o, "mouseleave focusout keyup"), 
      o[0] !== this.element[0] && this._off(o, "remove"), this._off(this.document, "mousemove"), 
      e && "mouseleave" === e.type && t.each(this.parents, function(e, i) {
        t(i.element).attr("title", i.title), delete n.parents[e];
      }), s.closing = !0, this._trigger("close", e, {
        tooltip: i
      }), s.hiding || (s.closing = !1)));
    },
    _tooltip: function(e) {
      var i = t("<div>").attr("role", "tooltip").addClass("ui-tooltip ui-widget ui-corner-all ui-widget-content " + (this.options.tooltipClass || "")), n = i.uniqueId().attr("id");
      return t("<div>").addClass("ui-tooltip-content").appendTo(i), i.appendTo(this.document[0].body), 
      this.tooltips[n] = {
        element: e,
        tooltip: i
      };
    },
    _find: function(t) {
      var e = t.data("ui-tooltip-id");
      return e ? this.tooltips[e] : null;
    },
    _removeTooltip: function(t) {
      t.remove(), delete this.tooltips[t.attr("id")];
    },
    _destroy: function() {
      var e = this;
      t.each(this.tooltips, function(i, n) {
        var o = t.Event("blur"), s = n.element;
        o.target = o.currentTarget = s[0], e.close(o, !0), t("#" + i).remove(), s.data("ui-tooltip-title") && (s.attr("title") || s.attr("title", s.data("ui-tooltip-title")), 
        s.removeData("ui-tooltip-title"));
      }), this.liveRegion.remove();
    }
  });
  var b = "ui-effects-", C = t;
  t.effects = {
    effect: {}
  }, function(t, e) {
    function i(t, e, i) {
      var n = d[e.type] || {};
      return null == t ? i || !e.def ? null : e.def : (t = n.floor ? ~~t : parseFloat(t), 
      isNaN(t) ? e.def : n.mod ? (t + n.mod) % n.mod : 0 > t ? 0 : t > n.max ? n.max : t);
    }
    function n(i) {
      var n = l(), o = n._rgba = [];
      return i = i.toLowerCase(), p(r, function(t, s) {
        var a, r = s.re.exec(i), l = r && s.parse(r), d = s.space || "rgba";
        return l ? (a = n[d](l), n[c[d].cache] = a[c[d].cache], o = n._rgba = a._rgba, !1) : e;
      }), o.length ? ("0,0,0,0" === o.join() && t.extend(o, s.transparent), n) : s[i];
    }
    function o(t, e, i) {
      return 1 > 6 * (i = (i + 1) % 1) ? t + 6 * (e - t) * i : 1 > 2 * i ? e : 2 > 3 * i ? t + 6 * (e - t) * (2 / 3 - i) : t;
    }
    var s, a = /^([\-+])=\s*(\d+\.?\d*)/, r = [ {
      re: /rgba?\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
      parse: function(t) {
        return [ t[1], t[2], t[3], t[4] ];
      }
    }, {
      re: /rgba?\(\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
      parse: function(t) {
        return [ 2.55 * t[1], 2.55 * t[2], 2.55 * t[3], t[4] ];
      }
    }, {
      re: /#([a-f0-9]{2})([a-f0-9]{2})([a-f0-9]{2})/,
      parse: function(t) {
        return [ parseInt(t[1], 16), parseInt(t[2], 16), parseInt(t[3], 16) ];
      }
    }, {
      re: /#([a-f0-9])([a-f0-9])([a-f0-9])/,
      parse: function(t) {
        return [ parseInt(t[1] + t[1], 16), parseInt(t[2] + t[2], 16), parseInt(t[3] + t[3], 16) ];
      }
    }, {
      re: /hsla?\(\s*(\d+(?:\.\d+)?)\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
      space: "hsla",
      parse: function(t) {
        return [ t[1], t[2] / 100, t[3] / 100, t[4] ];
      }
    } ], l = t.Color = function(e, i, n, o) {
      return new t.Color.fn.parse(e, i, n, o);
    }, c = {
      rgba: {
        props: {
          red: {
            idx: 0,
            type: "byte"
          },
          green: {
            idx: 1,
            type: "byte"
          },
          blue: {
            idx: 2,
            type: "byte"
          }
        }
      },
      hsla: {
        props: {
          hue: {
            idx: 0,
            type: "degrees"
          },
          saturation: {
            idx: 1,
            type: "percent"
          },
          lightness: {
            idx: 2,
            type: "percent"
          }
        }
      }
    }, d = {
      byte: {
        floor: !0,
        max: 255
      },
      percent: {
        max: 1
      },
      degrees: {
        mod: 360,
        floor: !0
      }
    }, u = l.support = {}, h = t("<p>")[0], p = t.each;
    h.style.cssText = "background-color:rgba(1,1,1,.5)", u.rgba = h.style.backgroundColor.indexOf("rgba") > -1, 
    p(c, function(t, e) {
      e.cache = "_" + t, e.props.alpha = {
        idx: 3,
        type: "percent",
        def: 1
      };
    }), l.fn = t.extend(l.prototype, {
      parse: function(o, a, r, d) {
        if (o === e) return this._rgba = [ null, null, null, null ], this;
        (o.jquery || o.nodeType) && (o = t(o).css(a), a = e);
        var u = this, h = t.type(o), f = this._rgba = [];
        return a !== e && (o = [ o, a, r, d ], h = "array"), "string" === h ? this.parse(n(o) || s._default) : "array" === h ? (p(c.rgba.props, function(t, e) {
          f[e.idx] = i(o[e.idx], e);
        }), this) : "object" === h ? (o instanceof l ? p(c, function(t, e) {
          o[e.cache] && (u[e.cache] = o[e.cache].slice());
        }) : p(c, function(e, n) {
          var s = n.cache;
          p(n.props, function(t, e) {
            if (!u[s] && n.to) {
              if ("alpha" === t || null == o[t]) return;
              u[s] = n.to(u._rgba);
            }
            u[s][e.idx] = i(o[t], e, !0);
          }), u[s] && 0 > t.inArray(null, u[s].slice(0, 3)) && (u[s][3] = 1, n.from && (u._rgba = n.from(u[s])));
        }), this) : e;
      },
      is: function(t) {
        var i = l(t), n = !0, o = this;
        return p(c, function(t, s) {
          var a, r = i[s.cache];
          return r && (a = o[s.cache] || s.to && s.to(o._rgba) || [], p(s.props, function(t, i) {
            return null != r[i.idx] ? n = r[i.idx] === a[i.idx] : e;
          })), n;
        }), n;
      },
      _space: function() {
        var t = [], e = this;
        return p(c, function(i, n) {
          e[n.cache] && t.push(i);
        }), t.pop();
      },
      transition: function(t, e) {
        var n = l(t), o = n._space(), s = c[o], a = 0 === this.alpha() ? l("transparent") : this, r = a[s.cache] || s.to(a._rgba), u = r.slice();
        return n = n[s.cache], p(s.props, function(t, o) {
          var s = o.idx, a = r[s], l = n[s], c = d[o.type] || {};
          null !== l && (null === a ? u[s] = l : (c.mod && (l - a > c.mod / 2 ? a += c.mod : a - l > c.mod / 2 && (a -= c.mod)), 
          u[s] = i((l - a) * e + a, o)));
        }), this[o](u);
      },
      blend: function(e) {
        if (1 === this._rgba[3]) return this;
        var i = this._rgba.slice(), n = i.pop(), o = l(e)._rgba;
        return l(t.map(i, function(t, e) {
          return (1 - n) * o[e] + n * t;
        }));
      },
      toRgbaString: function() {
        var e = "rgba(", i = t.map(this._rgba, function(t, e) {
          return null == t ? e > 2 ? 1 : 0 : t;
        });
        return 1 === i[3] && (i.pop(), e = "rgb("), e + i.join() + ")";
      },
      toHslaString: function() {
        var e = "hsla(", i = t.map(this.hsla(), function(t, e) {
          return null == t && (t = e > 2 ? 1 : 0), e && 3 > e && (t = Math.round(100 * t) + "%"), 
          t;
        });
        return 1 === i[3] && (i.pop(), e = "hsl("), e + i.join() + ")";
      },
      toHexString: function(e) {
        var i = this._rgba.slice(), n = i.pop();
        return e && i.push(~~(255 * n)), "#" + t.map(i, function(t) {
          return 1 === (t = (t || 0).toString(16)).length ? "0" + t : t;
        }).join("");
      },
      toString: function() {
        return 0 === this._rgba[3] ? "transparent" : this.toRgbaString();
      }
    }), l.fn.parse.prototype = l.fn, c.hsla.to = function(t) {
      if (null == t[0] || null == t[1] || null == t[2]) return [ null, null, null, t[3] ];
      var e, i, n = t[0] / 255, o = t[1] / 255, s = t[2] / 255, a = t[3], r = Math.max(n, o, s), l = Math.min(n, o, s), c = r - l, d = r + l, u = .5 * d;
      return e = l === r ? 0 : n === r ? 60 * (o - s) / c + 360 : o === r ? 60 * (s - n) / c + 120 : 60 * (n - o) / c + 240, 
      i = 0 === c ? 0 : .5 >= u ? c / d : c / (2 - d), [ Math.round(e) % 360, i, u, null == a ? 1 : a ];
    }, c.hsla.from = function(t) {
      if (null == t[0] || null == t[1] || null == t[2]) return [ null, null, null, t[3] ];
      var e = t[0] / 360, i = t[1], n = t[2], s = t[3], a = .5 >= n ? n * (1 + i) : n + i - n * i, r = 2 * n - a;
      return [ Math.round(255 * o(r, a, e + 1 / 3)), Math.round(255 * o(r, a, e)), Math.round(255 * o(r, a, e - 1 / 3)), s ];
    }, p(c, function(n, o) {
      var s = o.props, r = o.cache, c = o.to, d = o.from;
      l.fn[n] = function(n) {
        if (c && !this[r] && (this[r] = c(this._rgba)), n === e) return this[r].slice();
        var o, a = t.type(n), u = "array" === a || "object" === a ? n : arguments, h = this[r].slice();
        return p(s, function(t, e) {
          var n = u["object" === a ? t : e.idx];
          null == n && (n = h[e.idx]), h[e.idx] = i(n, e);
        }), d ? (o = l(d(h)), o[r] = h, o) : l(h);
      }, p(s, function(e, i) {
        l.fn[e] || (l.fn[e] = function(o) {
          var s, r = t.type(o), l = "alpha" === e ? this._hsla ? "hsla" : "rgba" : n, c = this[l](), d = c[i.idx];
          return "undefined" === r ? d : ("function" === r && (o = o.call(this, d), r = t.type(o)), 
          null == o && i.empty ? this : ("string" === r && (s = a.exec(o)) && (o = d + parseFloat(s[2]) * ("+" === s[1] ? 1 : -1)), 
          c[i.idx] = o, this[l](c)));
        });
      });
    }), l.hook = function(e) {
      var i = e.split(" ");
      p(i, function(e, i) {
        t.cssHooks[i] = {
          set: function(e, o) {
            var s, a, r = "";
            if ("transparent" !== o && ("string" !== t.type(o) || (s = n(o)))) {
              if (o = l(s || o), !u.rgba && 1 !== o._rgba[3]) {
                for (a = "backgroundColor" === i ? e.parentNode : e; ("" === r || "transparent" === r) && a && a.style; ) try {
                  r = t.css(a, "backgroundColor"), a = a.parentNode;
                } catch (t) {}
                o = o.blend(r && "transparent" !== r ? r : "_default");
              }
              o = o.toRgbaString();
            }
            try {
              e.style[i] = o;
            } catch (t) {}
          }
        }, t.fx.step[i] = function(e) {
          e.colorInit || (e.start = l(e.elem, i), e.end = l(e.end), e.colorInit = !0), t.cssHooks[i].set(e.elem, e.start.transition(e.end, e.pos));
        };
      });
    }, l.hook("backgroundColor borderBottomColor borderLeftColor borderRightColor borderTopColor color columnRuleColor outlineColor textDecorationColor textEmphasisColor"), 
    t.cssHooks.borderColor = {
      expand: function(t) {
        var e = {};
        return p([ "Top", "Right", "Bottom", "Left" ], function(i, n) {
          e["border" + n + "Color"] = t;
        }), e;
      }
    }, s = t.Color.names = {
      aqua: "#00ffff",
      black: "#000000",
      blue: "#0000ff",
      fuchsia: "#ff00ff",
      gray: "#808080",
      green: "#008000",
      lime: "#00ff00",
      maroon: "#800000",
      navy: "#000080",
      olive: "#808000",
      purple: "#800080",
      red: "#ff0000",
      silver: "#c0c0c0",
      teal: "#008080",
      white: "#ffffff",
      yellow: "#ffff00",
      transparent: [ null, null, null, 0 ],
      _default: "#ffffff"
    };
  }(C), function() {
    function e(e) {
      var i, n, o = e.ownerDocument.defaultView ? e.ownerDocument.defaultView.getComputedStyle(e, null) : e.currentStyle, s = {};
      if (o && o.length && o[0] && o[o[0]]) for (n = o.length; n--; ) i = o[n], "string" == typeof o[i] && (s[t.camelCase(i)] = o[i]); else for (i in o) "string" == typeof o[i] && (s[i] = o[i]);
      return s;
    }
    function i(e, i) {
      var n, s, a = {};
      for (n in i) s = i[n], e[n] !== s && (o[n] || (t.fx.step[n] || !isNaN(parseFloat(s))) && (a[n] = s));
      return a;
    }
    var n = [ "add", "remove", "toggle" ], o = {
      border: 1,
      borderBottom: 1,
      borderColor: 1,
      borderLeft: 1,
      borderRight: 1,
      borderTop: 1,
      borderWidth: 1,
      margin: 1,
      padding: 1
    };
    t.each([ "borderLeftStyle", "borderRightStyle", "borderBottomStyle", "borderTopStyle" ], function(e, i) {
      t.fx.step[i] = function(t) {
        ("none" !== t.end && !t.setAttr || 1 === t.pos && !t.setAttr) && (C.style(t.elem, i, t.end), 
        t.setAttr = !0);
      };
    }), t.fn.addBack || (t.fn.addBack = function(t) {
      return this.add(null == t ? this.prevObject : this.prevObject.filter(t));
    }), t.effects.animateClass = function(o, s, a, r) {
      var l = t.speed(s, a, r);
      return this.queue(function() {
        var s, a = t(this), r = a.attr("class") || "", c = l.children ? a.find("*").addBack() : a;
        c = c.map(function() {
          return {
            el: t(this),
            start: e(this)
          };
        }), (s = function() {
          t.each(n, function(t, e) {
            o[e] && a[e + "Class"](o[e]);
          });
        })(), c = c.map(function() {
          return this.end = e(this.el[0]), this.diff = i(this.start, this.end), this;
        }), a.attr("class", r), c = c.map(function() {
          var e = this, i = t.Deferred(), n = t.extend({}, l, {
            queue: !1,
            complete: function() {
              i.resolve(e);
            }
          });
          return this.el.animate(this.diff, n), i.promise();
        }), t.when.apply(t, c.get()).done(function() {
          s(), t.each(arguments, function() {
            var e = this.el;
            t.each(this.diff, function(t) {
              e.css(t, "");
            });
          }), l.complete.call(a[0]);
        });
      });
    }, t.fn.extend({
      addClass: function(e) {
        return function(i, n, o, s) {
          return n ? t.effects.animateClass.call(this, {
            add: i
          }, n, o, s) : e.apply(this, arguments);
        };
      }(t.fn.addClass),
      removeClass: function(e) {
        return function(i, n, o, s) {
          return arguments.length > 1 ? t.effects.animateClass.call(this, {
            remove: i
          }, n, o, s) : e.apply(this, arguments);
        };
      }(t.fn.removeClass),
      toggleClass: function(e) {
        return function(i, n, o, s, a) {
          return "boolean" == typeof n || void 0 === n ? o ? t.effects.animateClass.call(this, n ? {
            add: i
          } : {
            remove: i
          }, o, s, a) : e.apply(this, arguments) : t.effects.animateClass.call(this, {
            toggle: i
          }, n, o, s);
        };
      }(t.fn.toggleClass),
      switchClass: function(e, i, n, o, s) {
        return t.effects.animateClass.call(this, {
          add: i,
          remove: e
        }, n, o, s);
      }
    });
  }(), function() {
    function e(e, i, n, o) {
      return t.isPlainObject(e) && (i = e, e = e.effect), e = {
        effect: e
      }, null == i && (i = {}), t.isFunction(i) && (o = i, n = null, i = {}), ("number" == typeof i || t.fx.speeds[i]) && (o = n, 
      n = i, i = {}), t.isFunction(n) && (o = n, n = null), i && t.extend(e, i), n = n || i.duration, 
      e.duration = t.fx.off ? 0 : "number" == typeof n ? n : n in t.fx.speeds ? t.fx.speeds[n] : t.fx.speeds._default, 
      e.complete = o || i.complete, e;
    }
    function i(e) {
      return !(e && "number" != typeof e && !t.fx.speeds[e]) || ("string" == typeof e && !t.effects.effect[e] || (!!t.isFunction(e) || "object" == typeof e && !e.effect));
    }
    t.extend(t.effects, {
      version: "1.11.2",
      save: function(t, e) {
        for (var i = 0; e.length > i; i++) null !== e[i] && t.data(b + e[i], t[0].style[e[i]]);
      },
      restore: function(t, e) {
        var i, n;
        for (n = 0; e.length > n; n++) null !== e[n] && (void 0 === (i = t.data(b + e[n])) && (i = ""), 
        t.css(e[n], i));
      },
      setMode: function(t, e) {
        return "toggle" === e && (e = t.is(":hidden") ? "show" : "hide"), e;
      },
      getBaseline: function(t, e) {
        var i, n;
        switch (t[0]) {
         case "top":
          i = 0;
          break;

         case "middle":
          i = .5;
          break;

         case "bottom":
          i = 1;
          break;

         default:
          i = t[0] / e.height;
        }
        switch (t[1]) {
         case "left":
          n = 0;
          break;

         case "center":
          n = .5;
          break;

         case "right":
          n = 1;
          break;

         default:
          n = t[1] / e.width;
        }
        return {
          x: n,
          y: i
        };
      },
      createWrapper: function(e) {
        if (e.parent().is(".ui-effects-wrapper")) return e.parent();
        var i = {
          width: e.outerWidth(!0),
          height: e.outerHeight(!0),
          float: e.css("float")
        }, n = t("<div></div>").addClass("ui-effects-wrapper").css({
          fontSize: "100%",
          background: "transparent",
          border: "none",
          margin: 0,
          padding: 0
        }), o = {
          width: e.width(),
          height: e.height()
        }, s = document.activeElement;
        try {
          s.id;
        } catch (t) {
          s = document.body;
        }
        return e.wrap(n), (e[0] === s || t.contains(e[0], s)) && t(s).focus(), n = e.parent(), 
        "static" === e.css("position") ? (n.css({
          position: "relative"
        }), e.css({
          position: "relative"
        })) : (t.extend(i, {
          position: e.css("position"),
          zIndex: e.css("z-index")
        }), t.each([ "top", "left", "bottom", "right" ], function(t, n) {
          i[n] = e.css(n), isNaN(parseInt(i[n], 10)) && (i[n] = "auto");
        }), e.css({
          position: "relative",
          top: 0,
          left: 0,
          right: "auto",
          bottom: "auto"
        })), e.css(o), n.css(i).show();
      },
      removeWrapper: function(e) {
        var i = document.activeElement;
        return e.parent().is(".ui-effects-wrapper") && (e.parent().replaceWith(e), (e[0] === i || t.contains(e[0], i)) && t(i).focus()), 
        e;
      },
      setTransition: function(e, i, n, o) {
        return o = o || {}, t.each(i, function(t, i) {
          var s = e.cssUnit(i);
          s[0] > 0 && (o[i] = s[0] * n + s[1]);
        }), o;
      }
    }), t.fn.extend({
      effect: function() {
        function i(e) {
          function i() {
            t.isFunction(s) && s.call(o[0]), t.isFunction(e) && e();
          }
          var o = t(this), s = n.complete, r = n.mode;
          (o.is(":hidden") ? "hide" === r : "show" === r) ? (o[r](), i()) : a.call(o[0], n, i);
        }
        var n = e.apply(this, arguments), o = n.mode, s = n.queue, a = t.effects.effect[n.effect];
        return t.fx.off || !a ? o ? this[o](n.duration, n.complete) : this.each(function() {
          n.complete && n.complete.call(this);
        }) : !1 === s ? this.each(i) : this.queue(s || "fx", i);
      },
      show: function(t) {
        return function(n) {
          if (i(n)) return t.apply(this, arguments);
          var o = e.apply(this, arguments);
          return o.mode = "show", this.effect.call(this, o);
        };
      }(t.fn.show),
      hide: function(t) {
        return function(n) {
          if (i(n)) return t.apply(this, arguments);
          var o = e.apply(this, arguments);
          return o.mode = "hide", this.effect.call(this, o);
        };
      }(t.fn.hide),
      toggle: function(t) {
        return function(n) {
          if (i(n) || "boolean" == typeof n) return t.apply(this, arguments);
          var o = e.apply(this, arguments);
          return o.mode = "toggle", this.effect.call(this, o);
        };
      }(t.fn.toggle),
      cssUnit: function(e) {
        var i = this.css(e), n = [];
        return t.each([ "em", "px", "%", "pt" ], function(t, e) {
          i.indexOf(e) > 0 && (n = [ parseFloat(i), e ]);
        }), n;
      }
    });
  }(), function() {
    var e = {};
    t.each([ "Quad", "Cubic", "Quart", "Quint", "Expo" ], function(t, i) {
      e[i] = function(e) {
        return Math.pow(e, t + 2);
      };
    }), t.extend(e, {
      Sine: function(t) {
        return 1 - Math.cos(t * Math.PI / 2);
      },
      Circ: function(t) {
        return 1 - Math.sqrt(1 - t * t);
      },
      Elastic: function(t) {
        return 0 === t || 1 === t ? t : -Math.pow(2, 8 * (t - 1)) * Math.sin((80 * (t - 1) - 7.5) * Math.PI / 15);
      },
      Back: function(t) {
        return t * t * (3 * t - 2);
      },
      Bounce: function(t) {
        for (var e, i = 4; ((e = Math.pow(2, --i)) - 1) / 11 > t; ) ;
        return 1 / Math.pow(4, 3 - i) - 7.5625 * Math.pow((3 * e - 2) / 22 - t, 2);
      }
    }), t.each(e, function(e, i) {
      t.easing["easeIn" + e] = i, t.easing["easeOut" + e] = function(t) {
        return 1 - i(1 - t);
      }, t.easing["easeInOut" + e] = function(t) {
        return .5 > t ? i(2 * t) / 2 : 1 - i(-2 * t + 2) / 2;
      };
    });
  }(), t.effects, t.effects.effect.blind = function(e, i) {
    var n, o, s, a = t(this), r = /up|down|vertical/, l = /up|left|vertical|horizontal/, c = [ "position", "top", "bottom", "left", "right", "height", "width" ], d = t.effects.setMode(a, e.mode || "hide"), u = e.direction || "up", h = r.test(u), p = h ? "height" : "width", f = h ? "top" : "left", m = l.test(u), g = {}, v = "show" === d;
    a.parent().is(".ui-effects-wrapper") ? t.effects.save(a.parent(), c) : t.effects.save(a, c), 
    a.show(), o = (n = t.effects.createWrapper(a).css({
      overflow: "hidden"
    }))[p](), s = parseFloat(n.css(f)) || 0, g[p] = v ? o : 0, m || (a.css(h ? "bottom" : "right", 0).css(h ? "top" : "left", "auto").css({
      position: "absolute"
    }), g[f] = v ? s : o + s), v && (n.css(p, 0), m || n.css(f, s + o)), n.animate(g, {
      duration: e.duration,
      easing: e.easing,
      queue: !1,
      complete: function() {
        "hide" === d && a.hide(), t.effects.restore(a, c), t.effects.removeWrapper(a), i();
      }
    });
  }, t.effects.effect.bounce = function(e, i) {
    var n, o, s, a = t(this), r = [ "position", "top", "bottom", "left", "right", "height", "width" ], l = t.effects.setMode(a, e.mode || "effect"), c = "hide" === l, d = "show" === l, u = e.direction || "up", h = e.distance, p = e.times || 5, f = 2 * p + (d || c ? 1 : 0), m = e.duration / f, g = e.easing, v = "up" === u || "down" === u ? "top" : "left", b = "up" === u || "left" === u, C = a.queue(), y = C.length;
    for ((d || c) && r.push("opacity"), t.effects.save(a, r), a.show(), t.effects.createWrapper(a), 
    h || (h = a["top" === v ? "outerHeight" : "outerWidth"]() / 3), d && (s = {
      opacity: 1
    }, s[v] = 0, a.css("opacity", 0).css(v, b ? 2 * -h : 2 * h).animate(s, m, g)), c && (h /= Math.pow(2, p - 1)), 
    (s = {})[v] = 0, n = 0; p > n; n++) o = {}, o[v] = (b ? "-=" : "+=") + h, a.animate(o, m, g).animate(s, m, g), 
    h = c ? 2 * h : h / 2;
    c && (o = {
      opacity: 0
    }, o[v] = (b ? "-=" : "+=") + h, a.animate(o, m, g)), a.queue(function() {
      c && a.hide(), t.effects.restore(a, r), t.effects.removeWrapper(a), i();
    }), y > 1 && C.splice.apply(C, [ 1, 0 ].concat(C.splice(y, f + 1))), a.dequeue();
  }, t.effects.effect.clip = function(e, i) {
    var n, o, s, a = t(this), r = [ "position", "top", "bottom", "left", "right", "height", "width" ], l = "show" === t.effects.setMode(a, e.mode || "hide"), c = "vertical" === (e.direction || "vertical"), d = c ? "height" : "width", u = c ? "top" : "left", h = {};
    t.effects.save(a, r), a.show(), n = t.effects.createWrapper(a).css({
      overflow: "hidden"
    }), s = (o = "IMG" === a[0].tagName ? n : a)[d](), l && (o.css(d, 0), o.css(u, s / 2)), 
    h[d] = l ? s : 0, h[u] = l ? 0 : s / 2, o.animate(h, {
      queue: !1,
      duration: e.duration,
      easing: e.easing,
      complete: function() {
        l || a.hide(), t.effects.restore(a, r), t.effects.removeWrapper(a), i();
      }
    });
  }, t.effects.effect.drop = function(e, i) {
    var n, o = t(this), s = [ "position", "top", "bottom", "left", "right", "opacity", "height", "width" ], a = t.effects.setMode(o, e.mode || "hide"), r = "show" === a, l = e.direction || "left", c = "up" === l || "down" === l ? "top" : "left", d = "up" === l || "left" === l ? "pos" : "neg", u = {
      opacity: r ? 1 : 0
    };
    t.effects.save(o, s), o.show(), t.effects.createWrapper(o), n = e.distance || o["top" === c ? "outerHeight" : "outerWidth"](!0) / 2, 
    r && o.css("opacity", 0).css(c, "pos" === d ? -n : n), u[c] = (r ? "pos" === d ? "+=" : "-=" : "pos" === d ? "-=" : "+=") + n, 
    o.animate(u, {
      queue: !1,
      duration: e.duration,
      easing: e.easing,
      complete: function() {
        "hide" === a && o.hide(), t.effects.restore(o, s), t.effects.removeWrapper(o), i();
      }
    });
  }, t.effects.effect.explode = function(e, i) {
    function n() {
      h.css({
        visibility: "visible"
      }), t(v).remove(), p || h.hide(), i();
    }
    var o, s, a, r, l, c, d = e.pieces ? Math.round(Math.sqrt(e.pieces)) : 3, u = d, h = t(this), p = "show" === t.effects.setMode(h, e.mode || "hide"), f = h.show().css("visibility", "hidden").offset(), m = Math.ceil(h.outerWidth() / u), g = Math.ceil(h.outerHeight() / d), v = [];
    for (o = 0; d > o; o++) for (r = f.top + o * g, c = o - (d - 1) / 2, s = 0; u > s; s++) a = f.left + s * m, 
    l = s - (u - 1) / 2, h.clone().appendTo("body").wrap("<div></div>").css({
      position: "absolute",
      visibility: "visible",
      left: -s * m,
      top: -o * g
    }).parent().addClass("ui-effects-explode").css({
      position: "absolute",
      overflow: "hidden",
      width: m,
      height: g,
      left: a + (p ? l * m : 0),
      top: r + (p ? c * g : 0),
      opacity: p ? 0 : 1
    }).animate({
      left: a + (p ? 0 : l * m),
      top: r + (p ? 0 : c * g),
      opacity: p ? 1 : 0
    }, e.duration || 500, e.easing, function() {
      v.push(this), v.length === d * u && n();
    });
  }, t.effects.effect.fade = function(e, i) {
    var n = t(this), o = t.effects.setMode(n, e.mode || "toggle");
    n.animate({
      opacity: o
    }, {
      queue: !1,
      duration: e.duration,
      easing: e.easing,
      complete: i
    });
  }, t.effects.effect.fold = function(e, i) {
    var n, o, s = t(this), a = [ "position", "top", "bottom", "left", "right", "height", "width" ], r = t.effects.setMode(s, e.mode || "hide"), l = "show" === r, c = "hide" === r, d = e.size || 15, u = /([0-9]+)%/.exec(d), h = !!e.horizFirst, p = l !== h, f = p ? [ "width", "height" ] : [ "height", "width" ], m = e.duration / 2, g = {}, v = {};
    t.effects.save(s, a), s.show(), n = t.effects.createWrapper(s).css({
      overflow: "hidden"
    }), o = p ? [ n.width(), n.height() ] : [ n.height(), n.width() ], u && (d = parseInt(u[1], 10) / 100 * o[c ? 0 : 1]), 
    l && n.css(h ? {
      height: 0,
      width: d
    } : {
      height: d,
      width: 0
    }), g[f[0]] = l ? o[0] : d, v[f[1]] = l ? o[1] : 0, n.animate(g, m, e.easing).animate(v, m, e.easing, function() {
      c && s.hide(), t.effects.restore(s, a), t.effects.removeWrapper(s), i();
    });
  }, t.effects.effect.highlight = function(e, i) {
    var n = t(this), o = [ "backgroundImage", "backgroundColor", "opacity" ], s = t.effects.setMode(n, e.mode || "show"), a = {
      backgroundColor: n.css("backgroundColor")
    };
    "hide" === s && (a.opacity = 0), t.effects.save(n, o), n.show().css({
      backgroundImage: "none",
      backgroundColor: e.color || "#ffff99"
    }).animate(a, {
      queue: !1,
      duration: e.duration,
      easing: e.easing,
      complete: function() {
        "hide" === s && n.hide(), t.effects.restore(n, o), i();
      }
    });
  }, t.effects.effect.size = function(e, i) {
    var n, o, s, a = t(this), r = [ "position", "top", "bottom", "left", "right", "width", "height", "overflow", "opacity" ], l = [ "position", "top", "bottom", "left", "right", "overflow", "opacity" ], c = [ "width", "height", "overflow" ], d = [ "fontSize" ], u = [ "borderTopWidth", "borderBottomWidth", "paddingTop", "paddingBottom" ], h = [ "borderLeftWidth", "borderRightWidth", "paddingLeft", "paddingRight" ], p = t.effects.setMode(a, e.mode || "effect"), f = e.restore || "effect" !== p, m = e.scale || "both", g = e.origin || [ "middle", "center" ], v = a.css("position"), b = f ? r : l, C = {
      height: 0,
      width: 0,
      outerHeight: 0,
      outerWidth: 0
    };
    "show" === p && a.show(), n = {
      height: a.height(),
      width: a.width(),
      outerHeight: a.outerHeight(),
      outerWidth: a.outerWidth()
    }, "toggle" === e.mode && "show" === p ? (a.from = e.to || C, a.to = e.from || n) : (a.from = e.from || ("show" === p ? C : n), 
    a.to = e.to || ("hide" === p ? C : n)), s = {
      from: {
        y: a.from.height / n.height,
        x: a.from.width / n.width
      },
      to: {
        y: a.to.height / n.height,
        x: a.to.width / n.width
      }
    }, ("box" === m || "both" === m) && (s.from.y !== s.to.y && (b = b.concat(u), a.from = t.effects.setTransition(a, u, s.from.y, a.from), 
    a.to = t.effects.setTransition(a, u, s.to.y, a.to)), s.from.x !== s.to.x && (b = b.concat(h), 
    a.from = t.effects.setTransition(a, h, s.from.x, a.from), a.to = t.effects.setTransition(a, h, s.to.x, a.to))), 
    ("content" === m || "both" === m) && s.from.y !== s.to.y && (b = b.concat(d).concat(c), 
    a.from = t.effects.setTransition(a, d, s.from.y, a.from), a.to = t.effects.setTransition(a, d, s.to.y, a.to)), 
    t.effects.save(a, b), a.show(), t.effects.createWrapper(a), a.css("overflow", "hidden").css(a.from), 
    g && (o = t.effects.getBaseline(g, n), a.from.top = (n.outerHeight - a.outerHeight()) * o.y, 
    a.from.left = (n.outerWidth - a.outerWidth()) * o.x, a.to.top = (n.outerHeight - a.to.outerHeight) * o.y, 
    a.to.left = (n.outerWidth - a.to.outerWidth) * o.x), a.css(a.from), ("content" === m || "both" === m) && (u = u.concat([ "marginTop", "marginBottom" ]).concat(d), 
    h = h.concat([ "marginLeft", "marginRight" ]), c = r.concat(u).concat(h), a.find("*[width]").each(function() {
      var i = t(this), n = {
        height: i.height(),
        width: i.width(),
        outerHeight: i.outerHeight(),
        outerWidth: i.outerWidth()
      };
      f && t.effects.save(i, c), i.from = {
        height: n.height * s.from.y,
        width: n.width * s.from.x,
        outerHeight: n.outerHeight * s.from.y,
        outerWidth: n.outerWidth * s.from.x
      }, i.to = {
        height: n.height * s.to.y,
        width: n.width * s.to.x,
        outerHeight: n.height * s.to.y,
        outerWidth: n.width * s.to.x
      }, s.from.y !== s.to.y && (i.from = t.effects.setTransition(i, u, s.from.y, i.from), 
      i.to = t.effects.setTransition(i, u, s.to.y, i.to)), s.from.x !== s.to.x && (i.from = t.effects.setTransition(i, h, s.from.x, i.from), 
      i.to = t.effects.setTransition(i, h, s.to.x, i.to)), i.css(i.from), i.animate(i.to, e.duration, e.easing, function() {
        f && t.effects.restore(i, c);
      });
    })), a.animate(a.to, {
      queue: !1,
      duration: e.duration,
      easing: e.easing,
      complete: function() {
        0 === a.to.opacity && a.css("opacity", a.from.opacity), "hide" === p && a.hide(), 
        t.effects.restore(a, b), f || ("static" === v ? a.css({
          position: "relative",
          top: a.to.top,
          left: a.to.left
        }) : t.each([ "top", "left" ], function(t, e) {
          a.css(e, function(e, i) {
            var n = parseInt(i, 10), o = t ? a.to.left : a.to.top;
            return "auto" === i ? o + "px" : n + o + "px";
          });
        })), t.effects.removeWrapper(a), i();
      }
    });
  }, t.effects.effect.scale = function(e, i) {
    var n = t(this), o = t.extend(!0, {}, e), s = t.effects.setMode(n, e.mode || "effect"), a = parseInt(e.percent, 10) || (0 === parseInt(e.percent, 10) ? 0 : "hide" === s ? 0 : 100), r = e.direction || "both", l = e.origin, c = {
      height: n.height(),
      width: n.width(),
      outerHeight: n.outerHeight(),
      outerWidth: n.outerWidth()
    }, d = {
      y: "horizontal" !== r ? a / 100 : 1,
      x: "vertical" !== r ? a / 100 : 1
    };
    o.effect = "size", o.queue = !1, o.complete = i, "effect" !== s && (o.origin = l || [ "middle", "center" ], 
    o.restore = !0), o.from = e.from || ("show" === s ? {
      height: 0,
      width: 0,
      outerHeight: 0,
      outerWidth: 0
    } : c), o.to = {
      height: c.height * d.y,
      width: c.width * d.x,
      outerHeight: c.outerHeight * d.y,
      outerWidth: c.outerWidth * d.x
    }, o.fade && ("show" === s && (o.from.opacity = 0, o.to.opacity = 1), "hide" === s && (o.from.opacity = 1, 
    o.to.opacity = 0)), n.effect(o);
  }, t.effects.effect.puff = function(e, i) {
    var n = t(this), o = t.effects.setMode(n, e.mode || "hide"), s = "hide" === o, a = parseInt(e.percent, 10) || 150, r = a / 100, l = {
      height: n.height(),
      width: n.width(),
      outerHeight: n.outerHeight(),
      outerWidth: n.outerWidth()
    };
    t.extend(e, {
      effect: "scale",
      queue: !1,
      fade: !0,
      mode: o,
      complete: i,
      percent: s ? a : 100,
      from: s ? l : {
        height: l.height * r,
        width: l.width * r,
        outerHeight: l.outerHeight * r,
        outerWidth: l.outerWidth * r
      }
    }), n.effect(e);
  }, t.effects.effect.pulsate = function(e, i) {
    var n, o = t(this), s = t.effects.setMode(o, e.mode || "show"), a = "show" === s, r = "hide" === s, l = a || "hide" === s, c = 2 * (e.times || 5) + (l ? 1 : 0), d = e.duration / c, u = 0, h = o.queue(), p = h.length;
    for ((a || !o.is(":visible")) && (o.css("opacity", 0).show(), u = 1), n = 1; c > n; n++) o.animate({
      opacity: u
    }, d, e.easing), u = 1 - u;
    o.animate({
      opacity: u
    }, d, e.easing), o.queue(function() {
      r && o.hide(), i();
    }), p > 1 && h.splice.apply(h, [ 1, 0 ].concat(h.splice(p, c + 1))), o.dequeue();
  }, t.effects.effect.shake = function(e, i) {
    var n, o = t(this), s = [ "position", "top", "bottom", "left", "right", "height", "width" ], a = t.effects.setMode(o, e.mode || "effect"), r = e.direction || "left", l = e.distance || 20, c = e.times || 3, d = 2 * c + 1, u = Math.round(e.duration / d), h = "up" === r || "down" === r ? "top" : "left", p = "up" === r || "left" === r, f = {}, m = {}, g = {}, v = o.queue(), b = v.length;
    for (t.effects.save(o, s), o.show(), t.effects.createWrapper(o), f[h] = (p ? "-=" : "+=") + l, 
    m[h] = (p ? "+=" : "-=") + 2 * l, g[h] = (p ? "-=" : "+=") + 2 * l, o.animate(f, u, e.easing), 
    n = 1; c > n; n++) o.animate(m, u, e.easing).animate(g, u, e.easing);
    o.animate(m, u, e.easing).animate(f, u / 2, e.easing).queue(function() {
      "hide" === a && o.hide(), t.effects.restore(o, s), t.effects.removeWrapper(o), i();
    }), b > 1 && v.splice.apply(v, [ 1, 0 ].concat(v.splice(b, d + 1))), o.dequeue();
  }, t.effects.effect.slide = function(e, i) {
    var n, o = t(this), s = [ "position", "top", "bottom", "left", "right", "width", "height" ], a = t.effects.setMode(o, e.mode || "show"), r = "show" === a, l = e.direction || "left", c = "up" === l || "down" === l ? "top" : "left", d = "up" === l || "left" === l, u = {};
    t.effects.save(o, s), o.show(), n = e.distance || o["top" === c ? "outerHeight" : "outerWidth"](!0), 
    t.effects.createWrapper(o).css({
      overflow: "hidden"
    }), r && o.css(c, d ? isNaN(n) ? "-" + n : -n : n), u[c] = (r ? d ? "+=" : "-=" : d ? "-=" : "+=") + n, 
    o.animate(u, {
      queue: !1,
      duration: e.duration,
      easing: e.easing,
      complete: function() {
        "hide" === a && o.hide(), t.effects.restore(o, s), t.effects.removeWrapper(o), i();
      }
    });
  }, t.effects.effect.transfer = function(e, i) {
    var n = t(this), o = t(e.to), s = "fixed" === o.css("position"), a = t("body"), r = s ? a.scrollTop() : 0, l = s ? a.scrollLeft() : 0, c = o.offset(), d = {
      top: c.top - r,
      left: c.left - l,
      height: o.innerHeight(),
      width: o.innerWidth()
    }, u = n.offset(), h = t("<div class='ui-effects-transfer'></div>").appendTo(document.body).addClass(e.className).css({
      top: u.top - r,
      left: u.left - l,
      height: n.innerHeight(),
      width: n.innerWidth(),
      position: s ? "fixed" : "absolute"
    }).animate(d, e.duration, e.easing, function() {
      h.remove(), i();
    });
  };
}), "undefined" == typeof jQuery) throw new Error("Bootstrap's JavaScript requires jQuery");

+function(t) {
  "use strict";
  var e = jQuery.fn.jquery.split(" ")[0].split(".");
  if (e[0] < 2 && e[1] < 9 || 1 == e[0] && 9 == e[1] && e[2] < 1 || e[0] > 3) throw new Error("Bootstrap's JavaScript requires jQuery version 1.9.1 or higher, but lower than version 4");
}(), function(t) {
  "use strict";
  function e() {
    var t = document.createElement("bootstrap"), e = {
      WebkitTransition: "webkitTransitionEnd",
      MozTransition: "transitionend",
      OTransition: "oTransitionEnd otransitionend",
      transition: "transitionend"
    };
    for (var i in e) if (void 0 !== t.style[i]) return {
      end: e[i]
    };
    return !1;
  }
  t.fn.emulateTransitionEnd = function(e) {
    var i = !1, n = this;
    t(this).one("bsTransitionEnd", function() {
      i = !0;
    });
    return setTimeout(function() {
      i || t(n).trigger(t.support.transition.end);
    }, e), this;
  }, t(function() {
    t.support.transition = e(), t.support.transition && (t.event.special.bsTransitionEnd = {
      bindType: t.support.transition.end,
      delegateType: t.support.transition.end,
      handle: function(e) {
        if (t(e.target).is(this)) return e.handleObj.handler.apply(this, arguments);
      }
    });
  });
}(jQuery), function(t) {
  "use strict";
  var e = '[data-dismiss="alert"]', i = function(i) {
    t(i).on("click", e, this.close);
  };
  i.VERSION = "3.3.7", i.TRANSITION_DURATION = 150, i.prototype.close = function(e) {
    function n() {
      a.detach().trigger("closed.bs.alert").remove();
    }
    var o = t(this), s = o.attr("data-target");
    s || (s = o.attr("href"), s = s && s.replace(/.*(?=#[^\s]*$)/, ""));
    var a = t("#" === s ? [] : s);
    e && e.preventDefault(), a.length || (a = o.closest(".alert")), a.trigger(e = t.Event("close.bs.alert")), 
    e.isDefaultPrevented() || (a.removeClass("in"), t.support.transition && a.hasClass("fade") ? a.one("bsTransitionEnd", n).emulateTransitionEnd(i.TRANSITION_DURATION) : n());
  };
  var n = t.fn.alert;
  t.fn.alert = function(e) {
    return this.each(function() {
      var n = t(this), o = n.data("bs.alert");
      o || n.data("bs.alert", o = new i(this)), "string" == typeof e && o[e].call(n);
    });
  }, t.fn.alert.Constructor = i, t.fn.alert.noConflict = function() {
    return t.fn.alert = n, this;
  }, t(document).on("click.bs.alert.data-api", e, i.prototype.close);
}(jQuery), function(t) {
  "use strict";
  function e(e) {
    return this.each(function() {
      var n = t(this), o = n.data("bs.button"), s = "object" == typeof e && e;
      o || n.data("bs.button", o = new i(this, s)), "toggle" == e ? o.toggle() : e && o.setState(e);
    });
  }
  var i = function(e, n) {
    this.$element = t(e), this.options = t.extend({}, i.DEFAULTS, n), this.isLoading = !1;
  };
  i.VERSION = "3.3.7", i.DEFAULTS = {
    loadingText: "loading..."
  }, i.prototype.setState = function(e) {
    var i = "disabled", n = this.$element, o = n.is("input") ? "val" : "html", s = n.data();
    e += "Text", null == s.resetText && n.data("resetText", n[o]()), setTimeout(t.proxy(function() {
      n[o](null == s[e] ? this.options[e] : s[e]), "loadingText" == e ? (this.isLoading = !0, 
      n.addClass(i).attr(i, i).prop(i, !0)) : this.isLoading && (this.isLoading = !1, 
      n.removeClass(i).removeAttr(i).prop(i, !1));
    }, this), 0);
  }, i.prototype.toggle = function() {
    var t = !0, e = this.$element.closest('[data-toggle="buttons"]');
    if (e.length) {
      var i = this.$element.find("input");
      "radio" == i.prop("type") ? (i.prop("checked") && (t = !1), e.find(".active").removeClass("active"), 
      this.$element.addClass("active")) : "checkbox" == i.prop("type") && (i.prop("checked") !== this.$element.hasClass("active") && (t = !1), 
      this.$element.toggleClass("active")), i.prop("checked", this.$element.hasClass("active")), 
      t && i.trigger("change");
    } else this.$element.attr("aria-pressed", !this.$element.hasClass("active")), this.$element.toggleClass("active");
  };
  var n = t.fn.button;
  t.fn.button = e, t.fn.button.Constructor = i, t.fn.button.noConflict = function() {
    return t.fn.button = n, this;
  }, t(document).on("click.bs.button.data-api", '[data-toggle^="button"]', function(i) {
    var n = t(i.target).closest(".btn");
    e.call(n, "toggle"), t(i.target).is('input[type="radio"], input[type="checkbox"]') || (i.preventDefault(), 
    n.is("input,button") ? n.trigger("focus") : n.find("input:visible,button:visible").first().trigger("focus"));
  }).on("focus.bs.button.data-api blur.bs.button.data-api", '[data-toggle^="button"]', function(e) {
    t(e.target).closest(".btn").toggleClass("focus", /^focus(in)?$/.test(e.type));
  });
}(jQuery), function(t) {
  "use strict";
  function e(e) {
    return this.each(function() {
      var n = t(this), o = n.data("bs.carousel"), s = t.extend({}, i.DEFAULTS, n.data(), "object" == typeof e && e), a = "string" == typeof e ? e : s.slide;
      o || n.data("bs.carousel", o = new i(this, s)), "number" == typeof e ? o.to(e) : a ? o[a]() : s.interval && o.pause().cycle();
    });
  }
  var i = function(e, i) {
    this.$element = t(e), this.$indicators = this.$element.find(".carousel-indicators"), 
    this.options = i, this.paused = null, this.sliding = null, this.interval = null, 
    this.$active = null, this.$items = null, this.options.keyboard && this.$element.on("keydown.bs.carousel", t.proxy(this.keydown, this)), 
    "hover" == this.options.pause && !("ontouchstart" in document.documentElement) && this.$element.on("mouseenter.bs.carousel", t.proxy(this.pause, this)).on("mouseleave.bs.carousel", t.proxy(this.cycle, this));
  };
  i.VERSION = "3.3.7", i.TRANSITION_DURATION = 600, i.DEFAULTS = {
    interval: 5e3,
    pause: "hover",
    wrap: !0,
    keyboard: !0
  }, i.prototype.keydown = function(t) {
    if (!/input|textarea/i.test(t.target.tagName)) {
      switch (t.which) {
       case 37:
        this.prev();
        break;

       case 39:
        this.next();
        break;

       default:
        return;
      }
      t.preventDefault();
    }
  }, i.prototype.cycle = function(e) {
    return e || (this.paused = !1), this.interval && clearInterval(this.interval), this.options.interval && !this.paused && (this.interval = setInterval(t.proxy(this.next, this), this.options.interval)), 
    this;
  }, i.prototype.getItemIndex = function(t) {
    return this.$items = t.parent().children(".item"), this.$items.index(t || this.$active);
  }, i.prototype.getItemForDirection = function(t, e) {
    var i = this.getItemIndex(e);
    if (("prev" == t && 0 === i || "next" == t && i == this.$items.length - 1) && !this.options.wrap) return e;
    var n = (i + ("prev" == t ? -1 : 1)) % this.$items.length;
    return this.$items.eq(n);
  }, i.prototype.to = function(t) {
    var e = this, i = this.getItemIndex(this.$active = this.$element.find(".item.active"));
    if (!(t > this.$items.length - 1 || t < 0)) return this.sliding ? this.$element.one("slid.bs.carousel", function() {
      e.to(t);
    }) : i == t ? this.pause().cycle() : this.slide(t > i ? "next" : "prev", this.$items.eq(t));
  }, i.prototype.pause = function(e) {
    return e || (this.paused = !0), this.$element.find(".next, .prev").length && t.support.transition && (this.$element.trigger(t.support.transition.end), 
    this.cycle(!0)), this.interval = clearInterval(this.interval), this;
  }, i.prototype.next = function() {
    if (!this.sliding) return this.slide("next");
  }, i.prototype.prev = function() {
    if (!this.sliding) return this.slide("prev");
  }, i.prototype.slide = function(e, n) {
    var o = this.$element.find(".item.active"), s = n || this.getItemForDirection(e, o), a = this.interval, r = "next" == e ? "left" : "right", l = this;
    if (s.hasClass("active")) return this.sliding = !1;
    var c = s[0], d = t.Event("slide.bs.carousel", {
      relatedTarget: c,
      direction: r
    });
    if (this.$element.trigger(d), !d.isDefaultPrevented()) {
      if (this.sliding = !0, a && this.pause(), this.$indicators.length) {
        this.$indicators.find(".active").removeClass("active");
        var u = t(this.$indicators.children()[this.getItemIndex(s)]);
        u && u.addClass("active");
      }
      var h = t.Event("slid.bs.carousel", {
        relatedTarget: c,
        direction: r
      });
      return t.support.transition && this.$element.hasClass("slide") ? (s.addClass(e), 
      s[0].offsetWidth, o.addClass(r), s.addClass(r), o.one("bsTransitionEnd", function() {
        s.removeClass([ e, r ].join(" ")).addClass("active"), o.removeClass([ "active", r ].join(" ")), 
        l.sliding = !1, setTimeout(function() {
          l.$element.trigger(h);
        }, 0);
      }).emulateTransitionEnd(i.TRANSITION_DURATION)) : (o.removeClass("active"), s.addClass("active"), 
      this.sliding = !1, this.$element.trigger(h)), a && this.cycle(), this;
    }
  };
  var n = t.fn.carousel;
  t.fn.carousel = e, t.fn.carousel.Constructor = i, t.fn.carousel.noConflict = function() {
    return t.fn.carousel = n, this;
  };
  var o = function(i) {
    var n, o = t(this), s = t(o.attr("data-target") || (n = o.attr("href")) && n.replace(/.*(?=#[^\s]+$)/, ""));
    if (s.hasClass("carousel")) {
      var a = t.extend({}, s.data(), o.data()), r = o.attr("data-slide-to");
      r && (a.interval = !1), e.call(s, a), r && s.data("bs.carousel").to(r), i.preventDefault();
    }
  };
  t(document).on("click.bs.carousel.data-api", "[data-slide]", o).on("click.bs.carousel.data-api", "[data-slide-to]", o), 
  t(window).on("load", function() {
    t('[data-ride="carousel"]').each(function() {
      var i = t(this);
      e.call(i, i.data());
    });
  });
}(jQuery), function(t) {
  "use strict";
  function e(e) {
    var i, n = e.attr("data-target") || (i = e.attr("href")) && i.replace(/.*(?=#[^\s]+$)/, "");
    return t(n);
  }
  function i(e) {
    return this.each(function() {
      var i = t(this), o = i.data("bs.collapse"), s = t.extend({}, n.DEFAULTS, i.data(), "object" == typeof e && e);
      !o && s.toggle && /show|hide/.test(e) && (s.toggle = !1), o || i.data("bs.collapse", o = new n(this, s)), 
      "string" == typeof e && o[e]();
    });
  }
  var n = function(e, i) {
    this.$element = t(e), this.options = t.extend({}, n.DEFAULTS, i), this.$trigger = t('[data-toggle="collapse"][href="#' + e.id + '"],[data-toggle="collapse"][data-target="#' + e.id + '"]'), 
    this.transitioning = null, this.options.parent ? this.$parent = this.getParent() : this.addAriaAndCollapsedClass(this.$element, this.$trigger), 
    this.options.toggle && this.toggle();
  };
  n.VERSION = "3.3.7", n.TRANSITION_DURATION = 350, n.DEFAULTS = {
    toggle: !0
  }, n.prototype.dimension = function() {
    return this.$element.hasClass("width") ? "width" : "height";
  }, n.prototype.show = function() {
    if (!this.transitioning && !this.$element.hasClass("in")) {
      var e, o = this.$parent && this.$parent.children(".panel").children(".in, .collapsing");
      if (!(o && o.length && (e = o.data("bs.collapse")) && e.transitioning)) {
        var s = t.Event("show.bs.collapse");
        if (this.$element.trigger(s), !s.isDefaultPrevented()) {
          o && o.length && (i.call(o, "hide"), e || o.data("bs.collapse", null));
          var a = this.dimension();
          this.$element.removeClass("collapse").addClass("collapsing")[a](0).attr("aria-expanded", !0), 
          this.$trigger.removeClass("collapsed").attr("aria-expanded", !0), this.transitioning = 1;
          var r = function() {
            this.$element.removeClass("collapsing").addClass("collapse in")[a](""), this.transitioning = 0, 
            this.$element.trigger("shown.bs.collapse");
          };
          if (!t.support.transition) return r.call(this);
          var l = t.camelCase([ "scroll", a ].join("-"));
          this.$element.one("bsTransitionEnd", t.proxy(r, this)).emulateTransitionEnd(n.TRANSITION_DURATION)[a](this.$element[0][l]);
        }
      }
    }
  }, n.prototype.hide = function() {
    if (!this.transitioning && this.$element.hasClass("in")) {
      var e = t.Event("hide.bs.collapse");
      if (this.$element.trigger(e), !e.isDefaultPrevented()) {
        var i = this.dimension();
        this.$element[i](this.$element[i]())[0].offsetHeight, this.$element.addClass("collapsing").removeClass("collapse in").attr("aria-expanded", !1), 
        this.$trigger.addClass("collapsed").attr("aria-expanded", !1), this.transitioning = 1;
        var o = function() {
          this.transitioning = 0, this.$element.removeClass("collapsing").addClass("collapse").trigger("hidden.bs.collapse");
        };
        return t.support.transition ? void this.$element[i](0).one("bsTransitionEnd", t.proxy(o, this)).emulateTransitionEnd(n.TRANSITION_DURATION) : o.call(this);
      }
    }
  }, n.prototype.toggle = function() {
    this[this.$element.hasClass("in") ? "hide" : "show"]();
  }, n.prototype.getParent = function() {
    return t(this.options.parent).find('[data-toggle="collapse"][data-parent="' + this.options.parent + '"]').each(t.proxy(function(i, n) {
      var o = t(n);
      this.addAriaAndCollapsedClass(e(o), o);
    }, this)).end();
  }, n.prototype.addAriaAndCollapsedClass = function(t, e) {
    var i = t.hasClass("in");
    t.attr("aria-expanded", i), e.toggleClass("collapsed", !i).attr("aria-expanded", i);
  };
  var o = t.fn.collapse;
  t.fn.collapse = i, t.fn.collapse.Constructor = n, t.fn.collapse.noConflict = function() {
    return t.fn.collapse = o, this;
  }, t(document).on("click.bs.collapse.data-api", '[data-toggle="collapse"]', function(n) {
    var o = t(this);
    o.attr("data-target") || n.preventDefault();
    var s = e(o), a = s.data("bs.collapse") ? "toggle" : o.data();
    i.call(s, a);
  });
}(jQuery), function(t) {
  "use strict";
  function e(e) {
    var i = e.attr("data-target");
    i || (i = e.attr("href"), i = i && /#[A-Za-z]/.test(i) && i.replace(/.*(?=#[^\s]*$)/, ""));
    var n = i && t(i);
    return n && n.length ? n : e.parent();
  }
  function i(i) {
    i && 3 === i.which || (t(n).remove(), t(o).each(function() {
      var n = t(this), o = e(n), s = {
        relatedTarget: this
      };
      o.hasClass("open") && (i && "click" == i.type && /input|textarea/i.test(i.target.tagName) && t.contains(o[0], i.target) || (o.trigger(i = t.Event("hide.bs.dropdown", s)), 
      i.isDefaultPrevented() || (n.attr("aria-expanded", "false"), o.removeClass("open").trigger(t.Event("hidden.bs.dropdown", s)))));
    }));
  }
  var n = ".dropdown-backdrop", o = '[data-toggle="dropdown"]', s = function(e) {
    t(e).on("click.bs.dropdown", this.toggle);
  };
  s.VERSION = "3.3.7", s.prototype.toggle = function(n) {
    var o = t(this);
    if (!o.is(".disabled, :disabled")) {
      var s = e(o), a = s.hasClass("open");
      if (i(), !a) {
        "ontouchstart" in document.documentElement && !s.closest(".navbar-nav").length && t(document.createElement("div")).addClass("dropdown-backdrop").insertAfter(t(this)).on("click", i);
        var r = {
          relatedTarget: this
        };
        if (s.trigger(n = t.Event("show.bs.dropdown", r)), n.isDefaultPrevented()) return;
        o.trigger("focus").attr("aria-expanded", "true"), s.toggleClass("open").trigger(t.Event("shown.bs.dropdown", r));
      }
      return !1;
    }
  }, s.prototype.keydown = function(i) {
    if (/(38|40|27|32)/.test(i.which) && !/input|textarea/i.test(i.target.tagName)) {
      var n = t(this);
      if (i.preventDefault(), i.stopPropagation(), !n.is(".disabled, :disabled")) {
        var s = e(n), a = s.hasClass("open");
        if (!a && 27 != i.which || a && 27 == i.which) return 27 == i.which && s.find(o).trigger("focus"), 
        n.trigger("click");
        var r = s.find(".dropdown-menu li:not(.disabled):visible a");
        if (r.length) {
          var l = r.index(i.target);
          38 == i.which && l > 0 && l--, 40 == i.which && l < r.length - 1 && l++, ~l || (l = 0), 
          r.eq(l).trigger("focus");
        }
      }
    }
  };
  var a = t.fn.dropdown;
  t.fn.dropdown = function(e) {
    return this.each(function() {
      var i = t(this), n = i.data("bs.dropdown");
      n || i.data("bs.dropdown", n = new s(this)), "string" == typeof e && n[e].call(i);
    });
  }, t.fn.dropdown.Constructor = s, t.fn.dropdown.noConflict = function() {
    return t.fn.dropdown = a, this;
  }, t(document).on("click.bs.dropdown.data-api", i).on("click.bs.dropdown.data-api", ".dropdown form", function(t) {
    t.stopPropagation();
  }).on("click.bs.dropdown.data-api", o, s.prototype.toggle).on("keydown.bs.dropdown.data-api", o, s.prototype.keydown).on("keydown.bs.dropdown.data-api", ".dropdown-menu", s.prototype.keydown);
}(jQuery), function(t) {
  "use strict";
  function e(e, n) {
    return this.each(function() {
      var o = t(this), s = o.data("bs.modal"), a = t.extend({}, i.DEFAULTS, o.data(), "object" == typeof e && e);
      s || o.data("bs.modal", s = new i(this, a)), "string" == typeof e ? s[e](n) : a.show && s.show(n);
    });
  }
  var i = function(e, i) {
    this.options = i, this.$body = t(document.body), this.$element = t(e), this.$dialog = this.$element.find(".modal-dialog"), 
    this.$backdrop = null, this.isShown = null, this.originalBodyPad = null, this.scrollbarWidth = 0, 
    this.ignoreBackdropClick = !1, this.options.remote && this.$element.find(".modal-content").load(this.options.remote, t.proxy(function() {
      this.$element.trigger("loaded.bs.modal");
    }, this));
  };
  i.VERSION = "3.3.7", i.TRANSITION_DURATION = 300, i.BACKDROP_TRANSITION_DURATION = 150, 
  i.DEFAULTS = {
    backdrop: !0,
    keyboard: !0,
    show: !0
  }, i.prototype.toggle = function(t) {
    return this.isShown ? this.hide() : this.show(t);
  }, i.prototype.show = function(e) {
    var n = this, o = t.Event("show.bs.modal", {
      relatedTarget: e
    });
    this.$element.trigger(o), this.isShown || o.isDefaultPrevented() || (this.isShown = !0, 
    this.checkScrollbar(), this.setScrollbar(), this.$body.addClass("modal-open"), this.escape(), 
    this.resize(), this.$element.on("click.dismiss.bs.modal", '[data-dismiss="modal"]', t.proxy(this.hide, this)), 
    this.$dialog.on("mousedown.dismiss.bs.modal", function() {
      n.$element.one("mouseup.dismiss.bs.modal", function(e) {
        t(e.target).is(n.$element) && (n.ignoreBackdropClick = !0);
      });
    }), this.backdrop(function() {
      var o = t.support.transition && n.$element.hasClass("fade");
      n.$element.parent().length || n.$element.appendTo(n.$body), n.$element.show().scrollTop(0), 
      n.adjustDialog(), o && n.$element[0].offsetWidth, n.$element.addClass("in"), n.enforceFocus();
      var s = t.Event("shown.bs.modal", {
        relatedTarget: e
      });
      o ? n.$dialog.one("bsTransitionEnd", function() {
        n.$element.trigger("focus").trigger(s);
      }).emulateTransitionEnd(i.TRANSITION_DURATION) : n.$element.trigger("focus").trigger(s);
    }));
  }, i.prototype.hide = function(e) {
    e && e.preventDefault(), e = t.Event("hide.bs.modal"), this.$element.trigger(e), 
    this.isShown && !e.isDefaultPrevented() && (this.isShown = !1, this.escape(), this.resize(), 
    t(document).off("focusin.bs.modal"), this.$element.removeClass("in").off("click.dismiss.bs.modal").off("mouseup.dismiss.bs.modal"), 
    this.$dialog.off("mousedown.dismiss.bs.modal"), t.support.transition && this.$element.hasClass("fade") ? this.$element.one("bsTransitionEnd", t.proxy(this.hideModal, this)).emulateTransitionEnd(i.TRANSITION_DURATION) : this.hideModal());
  }, i.prototype.enforceFocus = function() {
    t(document).off("focusin.bs.modal").on("focusin.bs.modal", t.proxy(function(t) {
      document === t.target || this.$element[0] === t.target || this.$element.has(t.target).length || this.$element.trigger("focus");
    }, this));
  }, i.prototype.escape = function() {
    this.isShown && this.options.keyboard ? this.$element.on("keydown.dismiss.bs.modal", t.proxy(function(t) {
      27 == t.which && this.hide();
    }, this)) : this.isShown || this.$element.off("keydown.dismiss.bs.modal");
  }, i.prototype.resize = function() {
    this.isShown ? t(window).on("resize.bs.modal", t.proxy(this.handleUpdate, this)) : t(window).off("resize.bs.modal");
  }, i.prototype.hideModal = function() {
    var t = this;
    this.$element.hide(), this.backdrop(function() {
      t.$body.removeClass("modal-open"), t.resetAdjustments(), t.resetScrollbar(), t.$element.trigger("hidden.bs.modal");
    });
  }, i.prototype.removeBackdrop = function() {
    this.$backdrop && this.$backdrop.remove(), this.$backdrop = null;
  }, i.prototype.backdrop = function(e) {
    var n = this, o = this.$element.hasClass("fade") ? "fade" : "";
    if (this.isShown && this.options.backdrop) {
      var s = t.support.transition && o;
      if (this.$backdrop = t(document.createElement("div")).addClass("modal-backdrop " + o).appendTo(this.$body), 
      this.$element.on("click.dismiss.bs.modal", t.proxy(function(t) {
        return this.ignoreBackdropClick ? void (this.ignoreBackdropClick = !1) : void (t.target === t.currentTarget && ("static" == this.options.backdrop ? this.$element[0].focus() : this.hide()));
      }, this)), s && this.$backdrop[0].offsetWidth, this.$backdrop.addClass("in"), !e) return;
      s ? this.$backdrop.one("bsTransitionEnd", e).emulateTransitionEnd(i.BACKDROP_TRANSITION_DURATION) : e();
    } else if (!this.isShown && this.$backdrop) {
      this.$backdrop.removeClass("in");
      var a = function() {
        n.removeBackdrop(), e && e();
      };
      t.support.transition && this.$element.hasClass("fade") ? this.$backdrop.one("bsTransitionEnd", a).emulateTransitionEnd(i.BACKDROP_TRANSITION_DURATION) : a();
    } else e && e();
  }, i.prototype.handleUpdate = function() {
    this.adjustDialog();
  }, i.prototype.adjustDialog = function() {
    var t = this.$element[0].scrollHeight > document.documentElement.clientHeight;
    this.$element.css({
      paddingLeft: !this.bodyIsOverflowing && t ? this.scrollbarWidth : "",
      paddingRight: this.bodyIsOverflowing && !t ? this.scrollbarWidth : ""
    });
  }, i.prototype.resetAdjustments = function() {
    this.$element.css({
      paddingLeft: "",
      paddingRight: ""
    });
  }, i.prototype.checkScrollbar = function() {
    var t = window.innerWidth;
    if (!t) {
      var e = document.documentElement.getBoundingClientRect();
      t = e.right - Math.abs(e.left);
    }
    this.bodyIsOverflowing = document.body.clientWidth < t, this.scrollbarWidth = this.measureScrollbar();
  }, i.prototype.setScrollbar = function() {
    var t = parseInt(this.$body.css("padding-right") || 0, 10);
    this.originalBodyPad = document.body.style.paddingRight || "", this.bodyIsOverflowing && this.$body.css("padding-right", t + this.scrollbarWidth);
  }, i.prototype.resetScrollbar = function() {
    this.$body.css("padding-right", this.originalBodyPad);
  }, i.prototype.measureScrollbar = function() {
    var t = document.createElement("div");
    t.className = "modal-scrollbar-measure", this.$body.append(t);
    var e = t.offsetWidth - t.clientWidth;
    return this.$body[0].removeChild(t), e;
  };
  var n = t.fn.modal;
  t.fn.modal = e, t.fn.modal.Constructor = i, t.fn.modal.noConflict = function() {
    return t.fn.modal = n, this;
  }, t(document).on("click.bs.modal.data-api", '[data-toggle="modal"]', function(i) {
    var n = t(this), o = n.attr("href"), s = t(n.attr("data-target") || o && o.replace(/.*(?=#[^\s]+$)/, "")), a = s.data("bs.modal") ? "toggle" : t.extend({
      remote: !/#/.test(o) && o
    }, s.data(), n.data());
    n.is("a") && i.preventDefault(), s.one("show.bs.modal", function(t) {
      t.isDefaultPrevented() || s.one("hidden.bs.modal", function() {
        n.is(":visible") && n.trigger("focus");
      });
    }), e.call(s, a, this);
  });
}(jQuery), function(t) {
  "use strict";
  var e = function(t, e) {
    this.type = null, this.options = null, this.enabled = null, this.timeout = null, 
    this.hoverState = null, this.$element = null, this.inState = null, this.init("tooltip", t, e);
  };
  e.VERSION = "3.3.7", e.TRANSITION_DURATION = 150, e.DEFAULTS = {
    animation: !0,
    placement: "top",
    selector: !1,
    template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
    trigger: "hover focus",
    title: "",
    delay: 0,
    html: !1,
    container: !1,
    viewport: {
      selector: "body",
      padding: 0
    }
  }, e.prototype.init = function(e, i, n) {
    if (this.enabled = !0, this.type = e, this.$element = t(i), this.options = this.getOptions(n), 
    this.$viewport = this.options.viewport && t(t.isFunction(this.options.viewport) ? this.options.viewport.call(this, this.$element) : this.options.viewport.selector || this.options.viewport), 
    this.inState = {
      click: !1,
      hover: !1,
      focus: !1
    }, this.$element[0] instanceof document.constructor && !this.options.selector) throw new Error("`selector` option must be specified when initializing " + this.type + " on the window.document object!");
    for (var o = this.options.trigger.split(" "), s = o.length; s--; ) {
      var a = o[s];
      if ("click" == a) this.$element.on("click." + this.type, this.options.selector, t.proxy(this.toggle, this)); else if ("manual" != a) {
        var r = "hover" == a ? "mouseenter" : "focusin", l = "hover" == a ? "mouseleave" : "focusout";
        this.$element.on(r + "." + this.type, this.options.selector, t.proxy(this.enter, this)), 
        this.$element.on(l + "." + this.type, this.options.selector, t.proxy(this.leave, this));
      }
    }
    this.options.selector ? this._options = t.extend({}, this.options, {
      trigger: "manual",
      selector: ""
    }) : this.fixTitle();
  }, e.prototype.getDefaults = function() {
    return e.DEFAULTS;
  }, e.prototype.getOptions = function(e) {
    return (e = t.extend({}, this.getDefaults(), this.$element.data(), e)).delay && "number" == typeof e.delay && (e.delay = {
      show: e.delay,
      hide: e.delay
    }), e;
  }, e.prototype.getDelegateOptions = function() {
    var e = {}, i = this.getDefaults();
    return this._options && t.each(this._options, function(t, n) {
      i[t] != n && (e[t] = n);
    }), e;
  }, e.prototype.enter = function(e) {
    var i = e instanceof this.constructor ? e : t(e.currentTarget).data("bs." + this.type);
    return i || (i = new this.constructor(e.currentTarget, this.getDelegateOptions()), 
    t(e.currentTarget).data("bs." + this.type, i)), e instanceof t.Event && (i.inState["focusin" == e.type ? "focus" : "hover"] = !0), 
    i.tip().hasClass("in") || "in" == i.hoverState ? void (i.hoverState = "in") : (clearTimeout(i.timeout), 
    i.hoverState = "in", i.options.delay && i.options.delay.show ? void (i.timeout = setTimeout(function() {
      "in" == i.hoverState && i.show();
    }, i.options.delay.show)) : i.show());
  }, e.prototype.isInStateTrue = function() {
    for (var t in this.inState) if (this.inState[t]) return !0;
    return !1;
  }, e.prototype.leave = function(e) {
    var i = e instanceof this.constructor ? e : t(e.currentTarget).data("bs." + this.type);
    if (i || (i = new this.constructor(e.currentTarget, this.getDelegateOptions()), 
    t(e.currentTarget).data("bs." + this.type, i)), e instanceof t.Event && (i.inState["focusout" == e.type ? "focus" : "hover"] = !1), 
    !i.isInStateTrue()) return clearTimeout(i.timeout), i.hoverState = "out", i.options.delay && i.options.delay.hide ? void (i.timeout = setTimeout(function() {
      "out" == i.hoverState && i.hide();
    }, i.options.delay.hide)) : i.hide();
  }, e.prototype.show = function() {
    var i = t.Event("show.bs." + this.type);
    if (this.hasContent() && this.enabled) {
      this.$element.trigger(i);
      var n = t.contains(this.$element[0].ownerDocument.documentElement, this.$element[0]);
      if (i.isDefaultPrevented() || !n) return;
      var o = this, s = this.tip(), a = this.getUID(this.type);
      this.setContent(), s.attr("id", a), this.$element.attr("aria-describedby", a), this.options.animation && s.addClass("fade");
      var r = "function" == typeof this.options.placement ? this.options.placement.call(this, s[0], this.$element[0]) : this.options.placement, l = /\s?auto?\s?/i, c = l.test(r);
      c && (r = r.replace(l, "") || "top"), s.detach().css({
        top: 0,
        left: 0,
        display: "block"
      }).addClass(r).data("bs." + this.type, this), this.options.container ? s.appendTo(this.options.container) : s.insertAfter(this.$element), 
      this.$element.trigger("inserted.bs." + this.type);
      var d = this.getPosition(), u = s[0].offsetWidth, h = s[0].offsetHeight;
      if (c) {
        var p = r, f = this.getPosition(this.$viewport);
        r = "bottom" == r && d.bottom + h > f.bottom ? "top" : "top" == r && d.top - h < f.top ? "bottom" : "right" == r && d.right + u > f.width ? "left" : "left" == r && d.left - u < f.left ? "right" : r, 
        s.removeClass(p).addClass(r);
      }
      var m = this.getCalculatedOffset(r, d, u, h);
      this.applyPlacement(m, r);
      var g = function() {
        var t = o.hoverState;
        o.$element.trigger("shown.bs." + o.type), o.hoverState = null, "out" == t && o.leave(o);
      };
      t.support.transition && this.$tip.hasClass("fade") ? s.one("bsTransitionEnd", g).emulateTransitionEnd(e.TRANSITION_DURATION) : g();
    }
  }, e.prototype.applyPlacement = function(e, i) {
    var n = this.tip(), o = n[0].offsetWidth, s = n[0].offsetHeight, a = parseInt(n.css("margin-top"), 10), r = parseInt(n.css("margin-left"), 10);
    isNaN(a) && (a = 0), isNaN(r) && (r = 0), e.top += a, e.left += r, t.offset.setOffset(n[0], t.extend({
      using: function(t) {
        n.css({
          top: Math.round(t.top),
          left: Math.round(t.left)
        });
      }
    }, e), 0), n.addClass("in");
    var l = n[0].offsetWidth, c = n[0].offsetHeight;
    "top" == i && c != s && (e.top = e.top + s - c);
    var d = this.getViewportAdjustedDelta(i, e, l, c);
    d.left ? e.left += d.left : e.top += d.top;
    var u = /top|bottom/.test(i), h = u ? 2 * d.left - o + l : 2 * d.top - s + c, p = u ? "offsetWidth" : "offsetHeight";
    n.offset(e), this.replaceArrow(h, n[0][p], u);
  }, e.prototype.replaceArrow = function(t, e, i) {
    this.arrow().css(i ? "left" : "top", 50 * (1 - t / e) + "%").css(i ? "top" : "left", "");
  }, e.prototype.setContent = function() {
    var t = this.tip(), e = this.getTitle();
    t.find(".tooltip-inner")[this.options.html ? "html" : "text"](e), t.removeClass("fade in top bottom left right");
  }, e.prototype.hide = function(i) {
    function n() {
      "in" != o.hoverState && s.detach(), o.$element && o.$element.removeAttr("aria-describedby").trigger("hidden.bs." + o.type), 
      i && i();
    }
    var o = this, s = t(this.$tip), a = t.Event("hide.bs." + this.type);
    if (this.$element.trigger(a), !a.isDefaultPrevented()) return s.removeClass("in"), 
    t.support.transition && s.hasClass("fade") ? s.one("bsTransitionEnd", n).emulateTransitionEnd(e.TRANSITION_DURATION) : n(), 
    this.hoverState = null, this;
  }, e.prototype.fixTitle = function() {
    var t = this.$element;
    (t.attr("title") || "string" != typeof t.attr("data-original-title")) && t.attr("data-original-title", t.attr("title") || "").attr("title", "");
  }, e.prototype.hasContent = function() {
    return this.getTitle();
  }, e.prototype.getPosition = function(e) {
    var i = (e = e || this.$element)[0], n = "BODY" == i.tagName, o = i.getBoundingClientRect();
    null == o.width && (o = t.extend({}, o, {
      width: o.right - o.left,
      height: o.bottom - o.top
    }));
    var s = window.SVGElement && i instanceof window.SVGElement, a = n ? {
      top: 0,
      left: 0
    } : s ? null : e.offset(), r = {
      scroll: n ? document.documentElement.scrollTop || document.body.scrollTop : e.scrollTop()
    }, l = n ? {
      width: t(window).width(),
      height: t(window).height()
    } : null;
    return t.extend({}, o, r, l, a);
  }, e.prototype.getCalculatedOffset = function(t, e, i, n) {
    return "bottom" == t ? {
      top: e.top + e.height,
      left: e.left + e.width / 2 - i / 2
    } : "top" == t ? {
      top: e.top - n,
      left: e.left + e.width / 2 - i / 2
    } : "left" == t ? {
      top: e.top + e.height / 2 - n / 2,
      left: e.left - i
    } : {
      top: e.top + e.height / 2 - n / 2,
      left: e.left + e.width
    };
  }, e.prototype.getViewportAdjustedDelta = function(t, e, i, n) {
    var o = {
      top: 0,
      left: 0
    };
    if (!this.$viewport) return o;
    var s = this.options.viewport && this.options.viewport.padding || 0, a = this.getPosition(this.$viewport);
    if (/right|left/.test(t)) {
      var r = e.top - s - a.scroll, l = e.top + s - a.scroll + n;
      r < a.top ? o.top = a.top - r : l > a.top + a.height && (o.top = a.top + a.height - l);
    } else {
      var c = e.left - s, d = e.left + s + i;
      c < a.left ? o.left = a.left - c : d > a.right && (o.left = a.left + a.width - d);
    }
    return o;
  }, e.prototype.getTitle = function() {
    var t = this.$element, e = this.options;
    return t.attr("data-original-title") || ("function" == typeof e.title ? e.title.call(t[0]) : e.title);
  }, e.prototype.getUID = function(t) {
    do {
      t += ~~(1e6 * Math.random());
    } while (document.getElementById(t));
    return t;
  }, e.prototype.tip = function() {
    if (!this.$tip && (this.$tip = t(this.options.template), 1 != this.$tip.length)) throw new Error(this.type + " `template` option must consist of exactly 1 top-level element!");
    return this.$tip;
  }, e.prototype.arrow = function() {
    return this.$arrow = this.$arrow || this.tip().find(".tooltip-arrow");
  }, e.prototype.enable = function() {
    this.enabled = !0;
  }, e.prototype.disable = function() {
    this.enabled = !1;
  }, e.prototype.toggleEnabled = function() {
    this.enabled = !this.enabled;
  }, e.prototype.toggle = function(e) {
    var i = this;
    e && ((i = t(e.currentTarget).data("bs." + this.type)) || (i = new this.constructor(e.currentTarget, this.getDelegateOptions()), 
    t(e.currentTarget).data("bs." + this.type, i))), e ? (i.inState.click = !i.inState.click, 
    i.isInStateTrue() ? i.enter(i) : i.leave(i)) : i.tip().hasClass("in") ? i.leave(i) : i.enter(i);
  }, e.prototype.destroy = function() {
    var t = this;
    clearTimeout(this.timeout), this.hide(function() {
      t.$element.off("." + t.type).removeData("bs." + t.type), t.$tip && t.$tip.detach(), 
      t.$tip = null, t.$arrow = null, t.$viewport = null, t.$element = null;
    });
  };
  var i = t.fn.tooltip;
  t.fn.tooltip = function(i) {
    return this.each(function() {
      var n = t(this), o = n.data("bs.tooltip"), s = "object" == typeof i && i;
      !o && /destroy|hide/.test(i) || (o || n.data("bs.tooltip", o = new e(this, s)), 
      "string" == typeof i && o[i]());
    });
  }, t.fn.tooltip.Constructor = e, t.fn.tooltip.noConflict = function() {
    return t.fn.tooltip = i, this;
  };
}(jQuery), function(t) {
  "use strict";
  var e = function(t, e) {
    this.init("popover", t, e);
  };
  if (!t.fn.tooltip) throw new Error("Popover requires tooltip.js");
  e.VERSION = "3.3.7", e.DEFAULTS = t.extend({}, t.fn.tooltip.Constructor.DEFAULTS, {
    placement: "right",
    trigger: "click",
    content: "",
    template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
  }), e.prototype = t.extend({}, t.fn.tooltip.Constructor.prototype), e.prototype.constructor = e, 
  e.prototype.getDefaults = function() {
    return e.DEFAULTS;
  }, e.prototype.setContent = function() {
    var t = this.tip(), e = this.getTitle(), i = this.getContent();
    t.find(".popover-title")[this.options.html ? "html" : "text"](e), t.find(".popover-content").children().detach().end()[this.options.html ? "string" == typeof i ? "html" : "append" : "text"](i), 
    t.removeClass("fade top bottom left right in"), t.find(".popover-title").html() || t.find(".popover-title").hide();
  }, e.prototype.hasContent = function() {
    return this.getTitle() || this.getContent();
  }, e.prototype.getContent = function() {
    var t = this.$element, e = this.options;
    return t.attr("data-content") || ("function" == typeof e.content ? e.content.call(t[0]) : e.content);
  }, e.prototype.arrow = function() {
    return this.$arrow = this.$arrow || this.tip().find(".arrow");
  };
  var i = t.fn.popover;
  t.fn.popover = function(i) {
    return this.each(function() {
      var n = t(this), o = n.data("bs.popover"), s = "object" == typeof i && i;
      !o && /destroy|hide/.test(i) || (o || n.data("bs.popover", o = new e(this, s)), 
      "string" == typeof i && o[i]());
    });
  }, t.fn.popover.Constructor = e, t.fn.popover.noConflict = function() {
    return t.fn.popover = i, this;
  };
}(jQuery), function(t) {
  "use strict";
  function e(i, n) {
    this.$body = t(document.body), this.$scrollElement = t(t(i).is(document.body) ? window : i), 
    this.options = t.extend({}, e.DEFAULTS, n), this.selector = (this.options.target || "") + " .nav li > a", 
    this.offsets = [], this.targets = [], this.activeTarget = null, this.scrollHeight = 0, 
    this.$scrollElement.on("scroll.bs.scrollspy", t.proxy(this.process, this)), this.refresh(), 
    this.process();
  }
  function i(i) {
    return this.each(function() {
      var n = t(this), o = n.data("bs.scrollspy"), s = "object" == typeof i && i;
      o || n.data("bs.scrollspy", o = new e(this, s)), "string" == typeof i && o[i]();
    });
  }
  e.VERSION = "3.3.7", e.DEFAULTS = {
    offset: 10
  }, e.prototype.getScrollHeight = function() {
    return this.$scrollElement[0].scrollHeight || Math.max(this.$body[0].scrollHeight, document.documentElement.scrollHeight);
  }, e.prototype.refresh = function() {
    var e = this, i = "offset", n = 0;
    this.offsets = [], this.targets = [], this.scrollHeight = this.getScrollHeight(), 
    t.isWindow(this.$scrollElement[0]) || (i = "position", n = this.$scrollElement.scrollTop()), 
    this.$body.find(this.selector).map(function() {
      var e = t(this), o = e.data("target") || e.attr("href"), s = /^#./.test(o) && t(o);
      return s && s.length && s.is(":visible") && [ [ s[i]().top + n, o ] ] || null;
    }).sort(function(t, e) {
      return t[0] - e[0];
    }).each(function() {
      e.offsets.push(this[0]), e.targets.push(this[1]);
    });
  }, e.prototype.process = function() {
    var t, e = this.$scrollElement.scrollTop() + this.options.offset, i = this.getScrollHeight(), n = this.options.offset + i - this.$scrollElement.height(), o = this.offsets, s = this.targets, a = this.activeTarget;
    if (this.scrollHeight != i && this.refresh(), e >= n) return a != (t = s[s.length - 1]) && this.activate(t);
    if (a && e < o[0]) return this.activeTarget = null, this.clear();
    for (t = o.length; t--; ) a != s[t] && e >= o[t] && (void 0 === o[t + 1] || e < o[t + 1]) && this.activate(s[t]);
  }, e.prototype.activate = function(e) {
    this.activeTarget = e, this.clear();
    var i = this.selector + '[data-target="' + e + '"],' + this.selector + '[href="' + e + '"]', n = t(i).parents("li").addClass("active");
    n.parent(".dropdown-menu").length && (n = n.closest("li.dropdown").addClass("active")), 
    n.trigger("activate.bs.scrollspy");
  }, e.prototype.clear = function() {
    t(this.selector).parentsUntil(this.options.target, ".active").removeClass("active");
  };
  var n = t.fn.scrollspy;
  t.fn.scrollspy = i, t.fn.scrollspy.Constructor = e, t.fn.scrollspy.noConflict = function() {
    return t.fn.scrollspy = n, this;
  }, t(window).on("load.bs.scrollspy.data-api", function() {
    t('[data-spy="scroll"]').each(function() {
      var e = t(this);
      i.call(e, e.data());
    });
  });
}(jQuery), function(t) {
  "use strict";
  function e(e) {
    return this.each(function() {
      var n = t(this), o = n.data("bs.tab");
      o || n.data("bs.tab", o = new i(this)), "string" == typeof e && o[e]();
    });
  }
  var i = function(e) {
    this.element = t(e);
  };
  i.VERSION = "3.3.7", i.TRANSITION_DURATION = 150, i.prototype.show = function() {
    var e = this.element, i = e.closest("ul:not(.dropdown-menu)"), n = e.data("target");
    if (n || (n = e.attr("href"), n = n && n.replace(/.*(?=#[^\s]*$)/, "")), !e.parent("li").hasClass("active")) {
      var o = i.find(".active:last a"), s = t.Event("hide.bs.tab", {
        relatedTarget: e[0]
      }), a = t.Event("show.bs.tab", {
        relatedTarget: o[0]
      });
      if (o.trigger(s), e.trigger(a), !a.isDefaultPrevented() && !s.isDefaultPrevented()) {
        var r = t(n);
        this.activate(e.closest("li"), i), this.activate(r, r.parent(), function() {
          o.trigger({
            type: "hidden.bs.tab",
            relatedTarget: e[0]
          }), e.trigger({
            type: "shown.bs.tab",
            relatedTarget: o[0]
          });
        });
      }
    }
  }, i.prototype.activate = function(e, n, o) {
    function s() {
      a.removeClass("active").find("> .dropdown-menu > .active").removeClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded", !1), 
      e.addClass("active").find('[data-toggle="tab"]').attr("aria-expanded", !0), r ? (e[0].offsetWidth, 
      e.addClass("in")) : e.removeClass("fade"), e.parent(".dropdown-menu").length && e.closest("li.dropdown").addClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded", !0), 
      o && o();
    }
    var a = n.find("> .active"), r = o && t.support.transition && (a.length && a.hasClass("fade") || !!n.find("> .fade").length);
    a.length && r ? a.one("bsTransitionEnd", s).emulateTransitionEnd(i.TRANSITION_DURATION) : s(), 
    a.removeClass("in");
  };
  var n = t.fn.tab;
  t.fn.tab = e, t.fn.tab.Constructor = i, t.fn.tab.noConflict = function() {
    return t.fn.tab = n, this;
  };
  var o = function(i) {
    i.preventDefault(), e.call(t(this), "show");
  };
  t(document).on("click.bs.tab.data-api", '[data-toggle="tab"]', o).on("click.bs.tab.data-api", '[data-toggle="pill"]', o);
}(jQuery), function(t) {
  "use strict";
  function e(e) {
    return this.each(function() {
      var n = t(this), o = n.data("bs.affix"), s = "object" == typeof e && e;
      o || n.data("bs.affix", o = new i(this, s)), "string" == typeof e && o[e]();
    });
  }
  var i = function(e, n) {
    this.options = t.extend({}, i.DEFAULTS, n), this.$target = t(this.options.target).on("scroll.bs.affix.data-api", t.proxy(this.checkPosition, this)).on("click.bs.affix.data-api", t.proxy(this.checkPositionWithEventLoop, this)), 
    this.$element = t(e), this.affixed = null, this.unpin = null, this.pinnedOffset = null, 
    this.checkPosition();
  };
  i.VERSION = "3.3.7", i.RESET = "affix affix-top affix-bottom", i.DEFAULTS = {
    offset: 0,
    target: window
  }, i.prototype.getState = function(t, e, i, n) {
    var o = this.$target.scrollTop(), s = this.$element.offset(), a = this.$target.height();
    if (null != i && "top" == this.affixed) return o < i && "top";
    if ("bottom" == this.affixed) return null != i ? !(o + this.unpin <= s.top) && "bottom" : !(o + a <= t - n) && "bottom";
    var r = null == this.affixed, l = r ? o : s.top, c = r ? a : e;
    return null != i && o <= i ? "top" : null != n && l + c >= t - n && "bottom";
  }, i.prototype.getPinnedOffset = function() {
    if (this.pinnedOffset) return this.pinnedOffset;
    this.$element.removeClass(i.RESET).addClass("affix");
    var t = this.$target.scrollTop(), e = this.$element.offset();
    return this.pinnedOffset = e.top - t;
  }, i.prototype.checkPositionWithEventLoop = function() {
    setTimeout(t.proxy(this.checkPosition, this), 1);
  }, i.prototype.checkPosition = function() {
    if (this.$element.is(":visible")) {
      var e = this.$element.height(), n = this.options.offset, o = n.top, s = n.bottom, a = Math.max(t(document).height(), t(document.body).height());
      "object" != typeof n && (s = o = n), "function" == typeof o && (o = n.top(this.$element)), 
      "function" == typeof s && (s = n.bottom(this.$element));
      var r = this.getState(a, e, o, s);
      if (this.affixed != r) {
        null != this.unpin && this.$element.css("top", "");
        var l = "affix" + (r ? "-" + r : ""), c = t.Event(l + ".bs.affix");
        if (this.$element.trigger(c), c.isDefaultPrevented()) return;
        this.affixed = r, this.unpin = "bottom" == r ? this.getPinnedOffset() : null, this.$element.removeClass(i.RESET).addClass(l).trigger(l.replace("affix", "affixed") + ".bs.affix");
      }
      "bottom" == r && this.$element.offset({
        top: a - e - s
      });
    }
  };
  var n = t.fn.affix;
  t.fn.affix = e, t.fn.affix.Constructor = i, t.fn.affix.noConflict = function() {
    return t.fn.affix = n, this;
  }, t(window).on("load", function() {
    t('[data-spy="affix"]').each(function() {
      var i = t(this), n = i.data();
      n.offset = n.offset || {}, null != n.offsetBottom && (n.offset.bottom = n.offsetBottom), 
      null != n.offsetTop && (n.offset.top = n.offsetTop), e.call(i, n);
    });
  });
}(jQuery), function(t, e, i) {
  var n = window.matchMedia;
  "undefined" != typeof module && module.exports ? module.exports = i(n) : "function" == typeof define && define.amd ? define(function() {
    return e[t] = i(n);
  }) : e[t] = i(n);
}("enquire", this, function(t) {
  "use strict";
  function e(t, e) {
    var i = 0, n = t.length;
    for (i; n > i && !1 !== e(t[i], i); i++) ;
  }
  function i(t) {
    return "[object Array]" === Object.prototype.toString.apply(t);
  }
  function n(t) {
    return "function" == typeof t;
  }
  function o(t) {
    this.options = t, !t.deferSetup && this.setup();
  }
  function s(e, i) {
    this.query = e, this.isUnconditional = i, this.handlers = [], this.mql = t(e);
    var n = this;
    this.listener = function(t) {
      n.mql = t, n.assess();
    }, this.mql.addListener(this.listener);
  }
  function a() {
    if (!t) throw new Error("matchMedia not present, legacy browsers require a polyfill");
    this.queries = {}, this.browserIsIncapable = !t("only all").matches;
  }
  return o.prototype = {
    setup: function() {
      this.options.setup && this.options.setup(), this.initialised = !0;
    },
    on: function() {
      !this.initialised && this.setup(), this.options.match && this.options.match();
    },
    off: function() {
      this.options.unmatch && this.options.unmatch();
    },
    destroy: function() {
      this.options.destroy ? this.options.destroy() : this.off();
    },
    equals: function(t) {
      return this.options === t || this.options.match === t;
    }
  }, s.prototype = {
    addHandler: function(t) {
      var e = new o(t);
      this.handlers.push(e), this.matches() && e.on();
    },
    removeHandler: function(t) {
      var i = this.handlers;
      e(i, function(e, n) {
        return e.equals(t) ? (e.destroy(), !i.splice(n, 1)) : void 0;
      });
    },
    matches: function() {
      return this.mql.matches || this.isUnconditional;
    },
    clear: function() {
      e(this.handlers, function(t) {
        t.destroy();
      }), this.mql.removeListener(this.listener), this.handlers.length = 0;
    },
    assess: function() {
      var t = this.matches() ? "on" : "off";
      e(this.handlers, function(e) {
        e[t]();
      });
    }
  }, a.prototype = {
    register: function(t, o, a) {
      var r = this.queries, l = a && this.browserIsIncapable;
      return r[t] || (r[t] = new s(t, l)), n(o) && (o = {
        match: o
      }), i(o) || (o = [ o ]), e(o, function(e) {
        n(e) && (e = {
          match: e
        }), r[t].addHandler(e);
      }), this;
    },
    unregister: function(t, e) {
      var i = this.queries[t];
      return i && (e ? i.removeHandler(e) : (i.clear(), delete this.queries[t])), this;
    }
  }, new a();
}), function(t, e) {
  "use strict";
  function i(t, e) {
    var i = this;
    e = e || {}, this.elements = t || ".js-responsive-image", this.className = e.className || "js-responsive-image-replaced", 
    this.onResize = !e.hasOwnProperty("onResize") || e.onResize, this.onImagesReplaced = e.onImagesReplaced || function() {}, 
    this.eles = $(t), n(function() {
      i.init();
    });
  }
  var n, o;
  n = t.requestAnimationFrame || t.mozRequestAnimationFrame || t.webkitRequestAnimationFrame || function(e) {
    t.setTimeout(e, 1e3 / 60);
  }, o = e.addEventListener ? function(t, e, i) {
    return t.addEventListener(e, i, !1);
  } : function(t, e, i) {
    return t.attachEvent("on" + e, i);
  }, i.prototype.init = function() {
    this.checkImagesNeedReplacing(this.eles), this.onResize && this.registerResizeEvent();
  }, i.prototype.checkImagesNeedReplacing = function(t) {
    var e = this;
    this.isResizing || (this.isResizing = !0, t.each(function() {
      e.replaceImagesBasedOnScreenDimensions(this);
    }), this.isResizing = !1, this.onImagesReplaced(t));
  }, i.prototype.replaceImagesBasedOnScreenDimensions = function(e) {
    var n, o, s = [];
    if (void 0 != $(e).attr("data-media")) {
      var a = $(e).attr("data-media");
      $(e).removeAttr("data-media"), a = $.parseJSON(a), $.each(a, function(t, e) {
        s.push(parseInt(t));
      }), $(e).data("width", s), $(e).data("media", a);
    }
    n = $(e).data("media"), o = i.getClosestValue(t.innerWidth, $.extend([], $(e).data("width"))), 
    e.src != n[o] && (e.src = n[o]);
  }, i.getClosestValue = function t(e, i) {
    return e >= i[0] && i.length > 1 && (i.shift(), t(e, i)), i[0];
  }, i.prototype.registerResizeEvent = function() {
    var e = this;
    o(t, "resize", function() {
      e.checkImagesNeedReplacing(e.eles);
    });
  }, "object" == typeof module && "object" == typeof module.exports ? module.exports = exports = i : "function" == typeof define && define.amd ? define(function() {
    return i;
  }) : "object" == typeof t && (t.Imager = i);
}(window, document), function() {
  "use strict";
  function t(t) {
    function e(e, n) {
      var s, m, g = e == window, v = n && void 0 !== n.message ? n.message : void 0;
      if (!(n = t.extend({}, t.blockUI.defaults, n || {})).ignoreIfBlocked || !t(e).data("blockUI.isBlocked")) {
        if (n.overlayCSS = t.extend({}, t.blockUI.defaults.overlayCSS, n.overlayCSS || {}), 
        s = t.extend({}, t.blockUI.defaults.css, n.css || {}), n.onOverlayClick && (n.overlayCSS.cursor = "pointer"), 
        m = t.extend({}, t.blockUI.defaults.themedCSS, n.themedCSS || {}), v = void 0 === v ? n.message : v, 
        g && p && i(window, {
          fadeOut: 0
        }), v && "string" != typeof v && (v.parentNode || v.jquery)) {
          var b = v.jquery ? v[0] : v, C = {};
          t(e).data("blockUI.history", C), C.el = b, C.parent = b.parentNode, C.display = b.style.display, 
          C.position = b.style.position, C.parent && C.parent.removeChild(b);
        }
        t(e).data("blockUI.onUnblock", n.onUnblock);
        var y, w, x, k, _ = n.baseZ;
        y = t(d || n.forceIframe ? '<iframe class="blockUI" style="z-index:' + _++ + ';display:none;border:none;margin:0;padding:0;position:absolute;width:100%;height:100%;top:0;left:0" src="' + n.iframeSrc + '"></iframe>' : '<div class="blockUI" style="display:none"></div>'), 
        w = t(n.theme ? '<div class="blockUI blockOverlay ui-widget-overlay" style="z-index:' + _++ + ';display:none"></div>' : '<div class="blockUI blockOverlay" style="z-index:' + _++ + ';display:none;border:none;margin:0;padding:0;width:100%;height:100%;top:0;left:0"></div>'), 
        n.theme && g ? (k = '<div class="blockUI ' + n.blockMsgClass + ' blockPage ui-dialog ui-widget ui-corner-all" style="z-index:' + (_ + 10) + ';display:none;position:fixed">', 
        n.title && (k += '<div class="ui-widget-header ui-dialog-titlebar ui-corner-all blockTitle">' + (n.title || "&nbsp;") + "</div>"), 
        k += '<div class="ui-widget-content ui-dialog-content"></div>', k += "</div>") : n.theme ? (k = '<div class="blockUI ' + n.blockMsgClass + ' blockElement ui-dialog ui-widget ui-corner-all" style="z-index:' + (_ + 10) + ';display:none;position:absolute">', 
        n.title && (k += '<div class="ui-widget-header ui-dialog-titlebar ui-corner-all blockTitle">' + (n.title || "&nbsp;") + "</div>"), 
        k += '<div class="ui-widget-content ui-dialog-content"></div>', k += "</div>") : k = g ? '<div class="blockUI ' + n.blockMsgClass + ' blockPage" style="z-index:' + (_ + 10) + ';display:none;position:fixed"></div>' : '<div class="blockUI ' + n.blockMsgClass + ' blockElement" style="z-index:' + (_ + 10) + ';display:none;position:absolute"></div>', 
        x = t(k), v && (n.theme ? (x.css(m), x.addClass("ui-widget-content")) : x.css(s)), 
        n.theme || w.css(n.overlayCSS), w.css("position", g ? "fixed" : "absolute"), (d || n.forceIframe) && y.css("opacity", 0);
        var $ = [ y, w, x ], A = t(g ? "body" : e);
        t.each($, function() {
          this.appendTo(A);
        }), n.theme && n.draggable && t.fn.draggable && x.draggable({
          handle: ".ui-dialog-titlebar",
          cancel: "li"
        });
        var S = h && (!t.support.boxModel || t("object,embed", g ? null : e).length > 0);
        if (u || S) {
          if (g && n.allowBodyStretch && t.support.boxModel && t("html,body").css("height", "100%"), 
          (u || !t.support.boxModel) && !g) var T = l(e, "borderTopWidth"), D = l(e, "borderLeftWidth"), I = T ? "(0 - " + T + ")" : 0, j = D ? "(0 - " + D + ")" : 0;
          t.each($, function(t, e) {
            var i = e[0].style;
            if (i.position = "absolute", t < 2) g ? i.setExpression("height", "Math.max(document.body.scrollHeight, document.body.offsetHeight) - (jQuery.support.boxModel?0:" + n.quirksmodeOffsetHack + ') + "px"') : i.setExpression("height", 'this.parentNode.offsetHeight + "px"'), 
            g ? i.setExpression("width", 'jQuery.support.boxModel && document.documentElement.clientWidth || document.body.clientWidth + "px"') : i.setExpression("width", 'this.parentNode.offsetWidth + "px"'), 
            j && i.setExpression("left", j), I && i.setExpression("top", I); else if (n.centerY) g && i.setExpression("top", '(document.documentElement.clientHeight || document.body.clientHeight) / 2 - (this.offsetHeight / 2) + (blah = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + "px"'), 
            i.marginTop = 0; else if (!n.centerY && g) {
              var o = "((document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + " + (n.css && n.css.top ? parseInt(n.css.top, 10) : 0) + ') + "px"';
              i.setExpression("top", o);
            }
          });
        }
        if (v && (n.theme ? x.find(".ui-widget-content").append(v) : x.append(v), (v.jquery || v.nodeType) && t(v).show()), 
        (d || n.forceIframe) && n.showOverlay && y.show(), n.fadeIn) {
          var P = n.onBlock ? n.onBlock : c, E = n.showOverlay && !v ? P : c, N = v ? P : c;
          n.showOverlay && w._fadeIn(n.fadeIn, E), v && x._fadeIn(n.fadeIn, N);
        } else n.showOverlay && w.show(), v && x.show(), n.onBlock && n.onBlock();
        if (o(1, e, n), g ? (p = x[0], f = t(n.focusableElements, p), n.focusInput && setTimeout(a, 20)) : r(x[0], n.centerX, n.centerY), 
        n.timeout) {
          var O = setTimeout(function() {
            g ? t.unblockUI(n) : t(e).unblock(n);
          }, n.timeout);
          t(e).data("blockUI.timeout", O);
        }
      }
    }
    function i(e, i) {
      var s, a = e == window, r = t(e), l = r.data("blockUI.history"), c = r.data("blockUI.timeout");
      c && (clearTimeout(c), r.removeData("blockUI.timeout")), i = t.extend({}, t.blockUI.defaults, i || {}), 
      o(0, e, i), null === i.onUnblock && (i.onUnblock = r.data("blockUI.onUnblock"), 
      r.removeData("blockUI.onUnblock"));
      var d;
      d = a ? t("body").children().filter(".blockUI").add("body > .blockUI") : r.find(">.blockUI"), 
      i.cursorReset && (d.length > 1 && (d[1].style.cursor = i.cursorReset), d.length > 2 && (d[2].style.cursor = i.cursorReset)), 
      a && (p = f = null), i.fadeOut ? (s = d.length, d.stop().fadeOut(i.fadeOut, function() {
        0 == --s && n(d, l, i, e);
      })) : n(d, l, i, e);
    }
    function n(e, i, n, o) {
      var s = t(o);
      if (!s.data("blockUI.isBlocked")) {
        e.each(function(t, e) {
          this.parentNode && this.parentNode.removeChild(this);
        }), i && i.el && (i.el.style.display = i.display, i.el.style.position = i.position, 
        i.parent && i.parent.appendChild(i.el), s.removeData("blockUI.history")), s.data("blockUI.static") && s.css("position", "static"), 
        "function" == typeof n.onUnblock && n.onUnblock(o, n);
        var a = t(document.body), r = a.width(), l = a[0].style.width;
        a.width(r - 1).width(r), a[0].style.width = l;
      }
    }
    function o(e, i, n) {
      var o = i == window, a = t(i);
      if ((e || (!o || p) && (o || a.data("blockUI.isBlocked"))) && (a.data("blockUI.isBlocked", e), 
      o && n.bindEvents && (!e || n.showOverlay))) {
        var r = "mousedown mouseup keydown keypress keyup touchstart touchend touchmove";
        e ? t(document).bind(r, n, s) : t(document).unbind(r, s);
      }
    }
    function s(e) {
      if ("keydown" === e.type && e.keyCode && 9 == e.keyCode && p && e.data.constrainTabKey) {
        var i = f, n = !e.shiftKey && e.target === i[i.length - 1], o = e.shiftKey && e.target === i[0];
        if (n || o) return setTimeout(function() {
          a(o);
        }, 10), !1;
      }
      var s = e.data, r = t(e.target);
      return r.hasClass("blockOverlay") && s.onOverlayClick && s.onOverlayClick(e), r.parents("div." + s.blockMsgClass).length > 0 || 0 === r.parents().children().filter("div.blockUI").length;
    }
    function a(t) {
      if (f) {
        var e = f[!0 === t ? f.length - 1 : 0];
        e && e.focus();
      }
    }
    function r(t, e, i) {
      var n = t.parentNode, o = t.style, s = (n.offsetWidth - t.offsetWidth) / 2 - l(n, "borderLeftWidth"), a = (n.offsetHeight - t.offsetHeight) / 2 - l(n, "borderTopWidth");
      e && (o.left = s > 0 ? s + "px" : "0"), i && (o.top = a > 0 ? a + "px" : "0");
    }
    function l(e, i) {
      return parseInt(t.css(e, i), 10) || 0;
    }
    t.fn._fadeIn = t.fn.fadeIn;
    var c = t.noop || function() {}, d = /MSIE/.test(navigator.userAgent), u = /MSIE 6.0/.test(navigator.userAgent) && !/MSIE 8.0/.test(navigator.userAgent), h = (document.documentMode, 
    t.isFunction(document.createElement("div").style.setExpression));
    t.blockUI = function(t) {
      e(window, t);
    }, t.unblockUI = function(t) {
      i(window, t);
    }, t.growlUI = function(e, i, n, o) {
      var s = t('<div class="growlUI"></div>');
      e && s.append("<h1>" + e + "</h1>"), i && s.append("<h2>" + i + "</h2>"), void 0 === n && (n = 3e3);
      var a = function(e) {
        e = e || {}, t.blockUI({
          message: s,
          fadeIn: void 0 !== e.fadeIn ? e.fadeIn : 700,
          fadeOut: void 0 !== e.fadeOut ? e.fadeOut : 1e3,
          timeout: void 0 !== e.timeout ? e.timeout : n,
          centerY: !1,
          showOverlay: !1,
          onUnblock: o,
          css: t.blockUI.defaults.growlCSS
        });
      };
      a();
      s.css("opacity");
      s.mouseover(function() {
        a({
          fadeIn: 0,
          timeout: 3e4
        });
        var e = t(".blockMsg");
        e.stop(), e.fadeTo(300, 1);
      }).mouseout(function() {
        t(".blockMsg").fadeOut(1e3);
      });
    }, t.fn.block = function(i) {
      if (this[0] === window) return t.blockUI(i), this;
      var n = t.extend({}, t.blockUI.defaults, i || {});
      return this.each(function() {
        var e = t(this);
        n.ignoreIfBlocked && e.data("blockUI.isBlocked") || e.unblock({
          fadeOut: 0
        });
      }), this.each(function() {
        "static" == t.css(this, "position") && (this.style.position = "relative", t(this).data("blockUI.static", !0)), 
        this.style.zoom = 1, e(this, i);
      });
    }, t.fn.unblock = function(e) {
      return this[0] === window ? (t.unblockUI(e), this) : this.each(function() {
        i(this, e);
      });
    }, t.blockUI.version = 2.66, t.blockUI.defaults = {
      message: "<h1>Please wait...</h1>",
      title: null,
      draggable: !0,
      theme: !1,
      css: {
        padding: 0,
        margin: 0,
        width: "30%",
        top: "40%",
        left: "35%",
        textAlign: "center",
        color: "#000",
        border: "3px solid #aaa",
        backgroundColor: "#fff",
        cursor: "wait"
      },
      themedCSS: {
        width: "30%",
        top: "40%",
        left: "35%"
      },
      overlayCSS: {
        backgroundColor: "#000",
        opacity: .6,
        cursor: "wait"
      },
      cursorReset: "default",
      growlCSS: {
        width: "350px",
        top: "10px",
        left: "",
        right: "10px",
        border: "none",
        padding: "5px",
        opacity: .6,
        cursor: "default",
        color: "#fff",
        backgroundColor: "#000",
        "-webkit-border-radius": "10px",
        "-moz-border-radius": "10px",
        "border-radius": "10px"
      },
      iframeSrc: /^https/i.test(window.location.href || "") ? "javascript:false" : "about:blank",
      forceIframe: !1,
      baseZ: 1e3,
      centerX: !0,
      centerY: !0,
      allowBodyStretch: !0,
      bindEvents: !0,
      constrainTabKey: !0,
      fadeIn: 200,
      fadeOut: 400,
      timeout: 0,
      showOverlay: !0,
      focusInput: !0,
      focusableElements: ":input:enabled:visible",
      onBlock: null,
      onUnblock: null,
      onOverlayClick: null,
      quirksmodeOffsetHack: 4,
      blockMsgClass: "blockMsg",
      ignoreIfBlocked: !1
    };
    var p = null, f = [];
  }
  "function" == typeof define && define.amd && define.amd.jQuery ? define([ "jquery" ], t) : t(jQuery);
}(), function(t, e, i) {
  function n(i, n, o) {
    var s = e.createElement(i);
    return n && (s.id = Z + n), o && (s.style.cssText = o), t(s);
  }
  function o() {
    return i.innerHeight ? i.innerHeight : t(i).height();
  }
  function s(e, i) {
    i !== Object(i) && (i = {}), this.cache = {}, this.el = e, this.value = function(e) {
      var n;
      return void 0 === this.cache[e] && (void 0 !== (n = t(this.el).attr("data-cbox-" + e)) ? this.cache[e] = n : void 0 !== i[e] ? this.cache[e] = i[e] : void 0 !== K[e] && (this.cache[e] = K[e])), 
      this.cache[e];
    }, this.get = function(e) {
      var i = this.value(e);
      return t.isFunction(i) ? i.call(this.el, this) : i;
    };
  }
  function a(t) {
    var e = A.length, i = (z + t) % e;
    return 0 > i ? e + i : i;
  }
  function r(t, e) {
    return Math.round((/%/.test(t) ? ("x" === e ? S.width() : o()) / 100 : 1) * parseInt(t, 10));
  }
  function l(t, e) {
    return t.get("photo") || t.get("photoRegex").test(e);
  }
  function c(t, e) {
    return t.get("retinaUrl") && i.devicePixelRatio > 1 ? e.replace(t.get("photoRegex"), t.get("retinaSuffix")) : e;
  }
  function d(t) {
    "contains" in C[0] && !C[0].contains(t.target) && t.target !== b[0] && (t.stopPropagation(), 
    C.focus());
  }
  function u(t) {
    u.str !== t && (C.add(b).removeClass(u.str).addClass(t), u.str = t);
  }
  function h(e) {
    z = 0, e && !1 !== e && "nofollow" !== e ? (A = t("." + tt).filter(function() {
      return new s(this, t.data(this, J)).get("rel") === e;
    }), -1 === (z = A.index(q.el)) && (A = A.add(q.el), z = A.length - 1)) : A = t(q.el);
  }
  function p(i) {
    t(e).trigger(i), rt.triggerHandler(i);
  }
  function f(i) {
    var o;
    if (!Q) {
      if (o = t(i).data(J), q = new s(i, o), h(q.get("rel")), !U) {
        U = V = !0, u(q.get("className")), C.css({
          visibility: "hidden",
          display: "block",
          opacity: ""
        }), T = n(lt, "LoadedContent", "width:0; height:0; overflow:hidden; visibility:hidden"), 
        w.css({
          width: "",
          height: ""
        }).append(T), H = x.height() + $.height() + w.outerHeight(!0) - w.height(), L = k.width() + _.width() + w.outerWidth(!0) - w.width(), 
        W = T.outerHeight(!0), R = T.outerWidth(!0);
        var a = r(q.get("initialWidth"), "x"), l = r(q.get("initialHeight"), "y"), c = q.get("maxWidth"), f = q.get("maxHeight");
        q.w = (!1 !== c ? Math.min(a, r(c, "x")) : a) - R - L, q.h = (!1 !== f ? Math.min(l, r(f, "y")) : l) - W - H, 
        T.css({
          width: "",
          height: q.h
        }), Y.position(), p(et), q.get("onOpen"), F.add(j).hide(), C.focus(), q.get("trapFocus") && e.addEventListener && (e.addEventListener("focus", d, !0), 
        rt.one(st, function() {
          e.removeEventListener("focus", d, !0);
        })), q.get("returnFocus") && rt.one(st, function() {
          t(q.el).focus();
        });
      }
      var m = parseFloat(q.get("opacity"));
      b.css({
        opacity: m === m ? m : "",
        cursor: q.get("overlayClose") ? "pointer" : "",
        visibility: "visible"
      }).show(), q.get("closeButton") ? M.html(q.get("close")).appendTo(w) : M.appendTo("<div/>"), 
      v();
    }
  }
  function m() {
    C || (G = !1, S = t(i), C = n(lt).attr({
      id: J,
      class: !1 === t.support.opacity ? Z + "IE" : "",
      role: "dialog",
      tabindex: "-1"
    }).hide(), b = n(lt, "Overlay").hide(), I = t([ n(lt, "LoadingOverlay")[0], n(lt, "LoadingGraphic")[0] ]), 
    y = n(lt, "Wrapper"), w = n(lt, "Content").append(j = n(lt, "Title"), P = n(lt, "Current"), O = t('<button type="button"/>').attr({
      id: Z + "Previous"
    }), N = t('<button type="button"/>').attr({
      id: Z + "Next"
    }), E = n("button", "Slideshow"), I), M = t('<button type="button"/>').attr({
      id: Z + "Close"
    }), y.append(n(lt).append(n(lt, "TopLeft"), x = n(lt, "TopCenter"), n(lt, "TopRight")), n(lt, !1, "clear:left").append(k = n(lt, "MiddleLeft"), w, _ = n(lt, "MiddleRight")), n(lt, !1, "clear:left").append(n(lt, "BottomLeft"), $ = n(lt, "BottomCenter"), n(lt, "BottomRight"))).find("div div").css({
      float: "left"
    }), D = n(lt, !1, "position:absolute; width:9999px; visibility:hidden; display:none; max-width:none;"), 
    F = N.add(O).add(P).add(E)), e.body && !C.parent().length && t(e.body).append(b, C.append(y, D));
  }
  function g() {
    function i(t) {
      t.which > 1 || t.shiftKey || t.altKey || t.metaKey || t.ctrlKey || (t.preventDefault(), 
      f(this));
    }
    return !!C && (G || (G = !0, N.click(function() {
      Y.next();
    }), O.click(function() {
      Y.prev();
    }), M.click(function() {
      Y.close();
    }), b.click(function() {
      q.get("overlayClose") && Y.close();
    }), t(e).bind("keydown." + Z, function(t) {
      var e = t.keyCode;
      U && q.get("escKey") && 27 === e && (t.preventDefault(), Y.close()), U && q.get("arrowKey") && A[1] && !t.altKey && (37 === e ? (t.preventDefault(), 
      O.click()) : 39 === e && (t.preventDefault(), N.click()));
    }), t.isFunction(t.fn.on) ? t(e).on("click." + Z, "." + tt, i) : t("." + tt).live("click." + Z, i)), 
    !0);
  }
  function v() {
    var e, o, s, a = Y.prep, d = ++ct;
    if (V = !0, B = !1, p(at), p(it), q.get("onLoad"), q.h = q.get("height") ? r(q.get("height"), "y") - W - H : q.get("innerHeight") && r(q.get("innerHeight"), "y"), 
    q.w = q.get("width") ? r(q.get("width"), "x") - R - L : q.get("innerWidth") && r(q.get("innerWidth"), "x"), 
    q.mw = q.w, q.mh = q.h, q.get("maxWidth") && (q.mw = r(q.get("maxWidth"), "x") - R - L, 
    q.mw = q.w && q.w < q.mw ? q.w : q.mw), q.get("maxHeight") && (q.mh = r(q.get("maxHeight"), "y") - W - H, 
    q.mh = q.h && q.h < q.mh ? q.h : q.mh), e = q.get("href"), X = setTimeout(function() {
      I.show();
    }, 100), q.get("inline")) {
      var u = t(e);
      s = t("<div>").hide().insertBefore(u), rt.one(at, function() {
        s.replaceWith(u);
      }), a(u);
    } else q.get("iframe") ? a(" ") : q.get("html") ? a(q.get("html")) : l(q, e) ? (e = c(q, e), 
    B = new Image(), t(B).addClass(Z + "Photo").bind("error", function() {
      a(n(lt, "Error").html(q.get("imgError")));
    }).one("load", function() {
      d === ct && setTimeout(function() {
        var e;
        t.each([ "alt", "longdesc", "aria-describedby" ], function(e, i) {
          var n = t(q.el).attr(i) || t(q.el).attr("data-" + i);
          n && B.setAttribute(i, n);
        }), q.get("retinaImage") && i.devicePixelRatio > 1 && (B.height = B.height / i.devicePixelRatio, 
        B.width = B.width / i.devicePixelRatio), q.get("scalePhotos") && (o = function() {
          B.height -= B.height * e, B.width -= B.width * e;
        }, q.mw && B.width > q.mw && (e = (B.width - q.mw) / B.width, o()), q.mh && B.height > q.mh && (e = (B.height - q.mh) / B.height, 
        o())), q.h && (B.style.marginTop = Math.max(q.mh - B.height, 0) / 2 + "px"), A[1] && (q.get("loop") || A[z + 1]) && (B.style.cursor = "pointer", 
        B.onclick = function() {
          Y.next();
        }), B.style.width = B.width + "px", B.style.height = B.height + "px", a(B);
      }, 1);
    }), B.src = e) : e && D.load(e, q.get("data"), function(e, i) {
      d === ct && a("error" === i ? n(lt, "Error").html(q.get("xhrError")) : t(this).contents());
    });
  }
  var b, C, y, w, x, k, _, $, A, S, T, D, I, j, P, E, N, O, M, F, q, H, L, W, R, z, B, U, V, Q, X, Y, G, K = {
    html: !1,
    photo: !1,
    iframe: !1,
    inline: !1,
    transition: "elastic",
    speed: 300,
    fadeOut: 300,
    width: !1,
    initialWidth: "600",
    innerWidth: !1,
    maxWidth: !1,
    height: !1,
    initialHeight: "450",
    innerHeight: !1,
    maxHeight: !1,
    scalePhotos: !0,
    scrolling: !0,
    opacity: .9,
    preloading: !0,
    className: !1,
    overlayClose: !0,
    escKey: !0,
    arrowKey: !0,
    top: !1,
    bottom: !1,
    left: !1,
    right: !1,
    fixed: !1,
    data: void 0,
    closeButton: !0,
    fastIframe: !0,
    open: !1,
    reposition: !0,
    loop: !0,
    slideshow: !1,
    slideshowAuto: !0,
    slideshowSpeed: 2500,
    slideshowStart: "start slideshow",
    slideshowStop: "stop slideshow",
    photoRegex: /\.(gif|png|jp(e|g|eg)|bmp|ico|webp|jxr|svg)((#|\?).*)?$/i,
    retinaImage: !1,
    retinaUrl: !1,
    retinaSuffix: "@2x.$1",
    current: "image {current} of {total}",
    previous: "previous",
    next: "next",
    close: "close",
    xhrError: "This content failed to load.",
    imgError: "This image failed to load.",
    returnFocus: !0,
    trapFocus: !0,
    onOpen: !1,
    onLoad: !1,
    onComplete: !1,
    onCleanup: !1,
    onClosed: !1,
    rel: function() {
      return this.rel;
    },
    href: function() {
      return t(this).attr("href");
    },
    title: function() {
      return this.title;
    }
  }, J = "colorbox", Z = "cbox", tt = Z + "Element", et = Z + "_open", it = Z + "_load", nt = Z + "_complete", ot = Z + "_cleanup", st = Z + "_closed", at = Z + "_purge", rt = t("<a/>"), lt = "div", ct = 0, dt = {}, ut = function() {
    function t() {
      clearTimeout(a);
    }
    function e() {
      (q.get("loop") || A[z + 1]) && (t(), a = setTimeout(Y.next, q.get("slideshowSpeed")));
    }
    function i() {
      E.html(q.get("slideshowStop")).unbind(l).one(l, n), rt.bind(nt, e).bind(it, t), 
      C.removeClass(r + "off").addClass(r + "on");
    }
    function n() {
      t(), rt.unbind(nt, e).unbind(it, t), E.html(q.get("slideshowStart")).unbind(l).one(l, function() {
        Y.next(), i();
      }), C.removeClass(r + "on").addClass(r + "off");
    }
    function o() {
      s = !1, E.hide(), t(), rt.unbind(nt, e).unbind(it, t), C.removeClass(r + "off " + r + "on");
    }
    var s, a, r = Z + "Slideshow_", l = "click." + Z;
    return function() {
      s ? q.get("slideshow") || (rt.unbind(ot, o), o()) : q.get("slideshow") && A[1] && (s = !0, 
      rt.one(ot, o), q.get("slideshowAuto") ? i() : n(), E.show());
    };
  }();
  t[J] || (t(m), Y = t.fn[J] = t[J] = function(e, i) {
    var n = this;
    if (e = e || {}, t.isFunction(n)) n = t("<a/>"), e.open = !0; else if (!n[0]) return n;
    return n[0] ? (m(), g() && (i && (e.onComplete = i), n.each(function() {
      var i = t.data(this, J) || {};
      t.data(this, J, t.extend(i, e));
    }).addClass(tt), new s(n[0], e).get("open") && f(n[0])), n) : n;
  }, Y.position = function(e, i) {
    function n() {
      x[0].style.width = $[0].style.width = w[0].style.width = parseInt(C[0].style.width, 10) - L + "px", 
      w[0].style.height = k[0].style.height = _[0].style.height = parseInt(C[0].style.height, 10) - H + "px";
    }
    var s, a, l, c = 0, d = 0, u = C.offset();
    if (S.unbind("resize." + Z), C.css({
      top: -9e4,
      left: -9e4
    }), a = S.scrollTop(), l = S.scrollLeft(), q.get("fixed") ? (u.top -= a, u.left -= l, 
    C.css({
      position: "fixed"
    })) : (c = a, d = l, C.css({
      position: "absolute"
    })), d += !1 !== q.get("right") ? Math.max(S.width() - q.w - R - L - r(q.get("right"), "x"), 0) : !1 !== q.get("left") ? r(q.get("left"), "x") : Math.round(Math.max(S.width() - q.w - R - L, 0) / 2), 
    c += !1 !== q.get("bottom") ? Math.max(o() - q.h - W - H - r(q.get("bottom"), "y"), 0) : !1 !== q.get("top") ? r(q.get("top"), "y") : Math.round(Math.max(o() - q.h - W - H, 0) / 2), 
    C.css({
      top: u.top,
      left: u.left,
      visibility: "visible"
    }), y[0].style.width = y[0].style.height = "9999px", s = {
      width: q.w + R + L,
      height: q.h + W + H,
      top: c,
      left: d
    }, e) {
      var h = 0;
      t.each(s, function(t) {
        return s[t] !== dt[t] ? void (h = e) : void 0;
      }), e = h;
    }
    dt = s, e || C.css(s), C.dequeue().animate(s, {
      duration: e || 0,
      complete: function() {
        n(), V = !1, y[0].style.width = q.w + R + L + "px", y[0].style.height = q.h + W + H + "px", 
        q.get("reposition") && setTimeout(function() {
          S.bind("resize." + Z, Y.position);
        }, 1), t.isFunction(i) && i();
      },
      step: n
    });
  }, Y.resize = function(t) {
    var e;
    U && ((t = t || {}).width && (q.w = r(t.width, "x") - R - L), t.innerWidth && (q.w = r(t.innerWidth, "x")), 
    T.css({
      width: q.w
    }), t.height && (q.h = r(t.height, "y") - W - H), t.innerHeight && (q.h = r(t.innerHeight, "y")), 
    t.innerHeight || t.height || (e = T.scrollTop(), T.css({
      height: "auto"
    }), q.h = T.height()), T.css({
      height: q.h
    }), e && T.scrollTop(e), Y.position("none" === q.get("transition") ? 0 : q.get("speed")));
  }, Y.prep = function(i) {
    if (U) {
      var o, r = "none" === q.get("transition") ? 0 : q.get("speed");
      T.remove(), (T = n(lt, "LoadedContent").append(i)).hide().appendTo(D.show()).css({
        width: (q.w = q.w || T.width(), q.w = q.mw && q.mw < q.w ? q.mw : q.w, q.w),
        overflow: q.get("scrolling") ? "auto" : "hidden"
      }).css({
        height: (q.h = q.h || T.height(), q.h = q.mh && q.mh < q.h ? q.mh : q.h, q.h)
      }).prependTo(w), D.hide(), t(B).css({
        float: "none"
      }), u(q.get("className")), o = function() {
        function i() {
          !1 === t.support.opacity && C[0].style.removeAttribute("filter");
        }
        var n, o, d = A.length;
        U && (o = function() {
          clearTimeout(X), I.hide(), p(nt), q.get("onComplete");
        }, j.html(q.get("title")).show(), T.show(), d > 1 ? ("string" == typeof q.get("current") && P.html(q.get("current").replace("{current}", z + 1).replace("{total}", d)).show(), 
        N[q.get("loop") || d - 1 > z ? "show" : "hide"]().html(q.get("next")), O[q.get("loop") || z ? "show" : "hide"]().html(q.get("previous")), 
        ut(), q.get("preloading") && t.each([ a(-1), a(1) ], function() {
          var i, n = A[this], o = new s(n, t.data(n, J)), a = o.get("href");
          a && l(o, a) && (a = c(o, a), i = e.createElement("img"), i.src = a);
        })) : F.hide(), q.get("iframe") ? ("frameBorder" in (n = e.createElement("iframe")) && (n.frameBorder = 0), 
        "allowTransparency" in n && (n.allowTransparency = "true"), q.get("scrolling") || (n.scrolling = "no"), 
        t(n).attr({
          src: q.get("href"),
          name: new Date().getTime(),
          class: Z + "Iframe",
          allowFullScreen: !0
        }).one("load", o).appendTo(T), rt.one(at, function() {
          n.src = "//about:blank";
        }), q.get("fastIframe") && t(n).trigger("load")) : o(), "fade" === q.get("transition") ? C.fadeTo(r, 1, i) : i());
      }, "fade" === q.get("transition") ? C.fadeTo(r, 0, function() {
        Y.position(0, o);
      }) : Y.position(r, o);
    }
  }, Y.next = function() {
    !V && A[1] && (q.get("loop") || A[z + 1]) && (z = a(1), f(A[z]));
  }, Y.prev = function() {
    !V && A[1] && (q.get("loop") || z) && (z = a(-1), f(A[z]));
  }, Y.close = function() {
    U && !Q && (Q = !0, U = !1, p(ot), q.get("onCleanup"), S.unbind("." + Z), b.fadeTo(q.get("fadeOut") || 0, 0), 
    C.stop().fadeTo(q.get("fadeOut") || 0, 0, function() {
      C.hide(), b.hide(), p(at), T.remove(), setTimeout(function() {
        Q = !1, p(st), q.get("onClosed");
      }, 1);
    }));
  }, Y.remove = function() {
    C && (C.stop(), t[J].close(), C.stop(!1, !0).remove(), b.remove(), Q = !1, C = null, 
    t("." + tt).removeData(J).removeClass(tt), t(e).unbind("click." + Z).unbind("keydown." + Z));
  }, Y.element = function() {
    return t(q.el);
  }, Y.settings = K);
}(jQuery, document, window), function(t) {
  "use strict";
  "function" == typeof define && define.amd ? define([ "jquery" ], t) : t("undefined" != typeof jQuery ? jQuery : window.Zepto);
}(function(t) {
  "use strict";
  function e(e) {
    var i = e.data;
    e.isDefaultPrevented() || (e.preventDefault(), t(e.target).ajaxSubmit(i));
  }
  function i(e) {
    var i = e.target, n = t(i);
    if (!n.is("[type=submit],[type=image]")) {
      var o = n.closest("[type=submit]");
      if (0 === o.length) return;
      i = o[0];
    }
    var s = this;
    if (s.clk = i, "image" == i.type) if (void 0 !== e.offsetX) s.clk_x = e.offsetX, 
    s.clk_y = e.offsetY; else if ("function" == typeof t.fn.offset) {
      var a = n.offset();
      s.clk_x = e.pageX - a.left, s.clk_y = e.pageY - a.top;
    } else s.clk_x = e.pageX - i.offsetLeft, s.clk_y = e.pageY - i.offsetTop;
    setTimeout(function() {
      s.clk = s.clk_x = s.clk_y = null;
    }, 100);
  }
  function n() {
    if (t.fn.ajaxSubmit.debug) {
      var e = "[jquery.form] " + Array.prototype.join.call(arguments, "");
      window.console && window.console.log ? window.console.log(e) : window.opera && window.opera.postError && window.opera.postError(e);
    }
  }
  var o = {};
  o.fileapi = void 0 !== t("<input type='file'/>").get(0).files, o.formdata = void 0 !== window.FormData;
  var s = !!t.fn.prop;
  t.fn.attr2 = function() {
    if (!s) return this.attr.apply(this, arguments);
    var t = this.prop.apply(this, arguments);
    return t && t.jquery || "string" == typeof t ? t : this.attr.apply(this, arguments);
  }, t.fn.ajaxSubmit = function(e) {
    function i(i) {
      var n, o, s = t.param(i, e.traditional).split("&"), a = s.length, r = [];
      for (n = 0; a > n; n++) s[n] = s[n].replace(/\+/g, " "), o = s[n].split("="), r.push([ decodeURIComponent(o[0]), decodeURIComponent(o[1]) ]);
      return r;
    }
    function a(i) {
      function o(t) {
        var e = null;
        try {
          t.contentWindow && (e = t.contentWindow.document);
        } catch (t) {
          n("cannot get iframe.contentWindow document: " + t);
        }
        if (e) return e;
        try {
          e = t.contentDocument ? t.contentDocument : t.document;
        } catch (i) {
          n("cannot get iframe.contentDocument: " + i), e = t.document;
        }
        return e;
      }
      function a() {
        function e() {
          try {
            var t = o(v).readyState;
            n("state = " + t), t && "uninitialized" == t.toLowerCase() && setTimeout(e, 50);
          } catch (t) {
            n("Server abort: ", t, " (", t.name, ")"), l(A), x && clearTimeout(x), x = void 0;
          }
        }
        var i = d.attr2("target"), s = d.attr2("action"), a = d.attr("enctype") || d.attr("encoding") || "multipart/form-data";
        k.setAttribute("target", m), (!r || /post/i.test(r)) && k.setAttribute("method", "POST"), 
        s != h.url && k.setAttribute("action", h.url), h.skipEncodingOverride || r && !/post/i.test(r) || d.attr({
          encoding: "multipart/form-data",
          enctype: "multipart/form-data"
        }), h.timeout && (x = setTimeout(function() {
          w = !0, l($);
        }, h.timeout));
        var c = [];
        try {
          if (h.extraData) for (var u in h.extraData) h.extraData.hasOwnProperty(u) && c.push(t.isPlainObject(h.extraData[u]) && h.extraData[u].hasOwnProperty("name") && h.extraData[u].hasOwnProperty("value") ? t('<input type="hidden" name="' + h.extraData[u].name + '">').val(h.extraData[u].value).appendTo(k)[0] : t('<input type="hidden" name="' + u + '">').val(h.extraData[u]).appendTo(k)[0]);
          h.iframeTarget || g.appendTo("body"), v.attachEvent ? v.attachEvent("onload", l) : v.addEventListener("load", l, !1), 
          setTimeout(e, 15);
          try {
            k.submit();
          } catch (t) {
            document.createElement("form").submit.apply(k);
          }
        } finally {
          k.setAttribute("action", s), k.setAttribute("enctype", a), i ? k.setAttribute("target", i) : d.removeAttr("target"), 
          t(c).remove();
        }
      }
      function l(e) {
        if (!b.aborted && !j) {
          if ((I = o(v)) || (n("cannot access response document"), e = A), e === $ && b) return b.abort("timeout"), 
          void _.reject(b, "timeout");
          if (e == A && b) return b.abort("server abort"), void _.reject(b, "error", "server abort");
          if (I && I.location.href != h.iframeSrc || w) {
            v.detachEvent ? v.detachEvent("onload", l) : v.removeEventListener("load", l, !1);
            var i, s = "success";
            try {
              if (w) throw "timeout";
              var a = "xml" == h.dataType || I.XMLDocument || t.isXMLDoc(I);
              if (n("isXml=" + a), !a && window.opera && (null === I.body || !I.body.innerHTML) && --P) return n("requeing onLoad callback, DOM not available"), 
              void setTimeout(l, 250);
              var r = I.body ? I.body : I.documentElement;
              b.responseText = r ? r.innerHTML : null, b.responseXML = I.XMLDocument ? I.XMLDocument : I, 
              a && (h.dataType = "xml"), b.getResponseHeader = function(t) {
                return {
                  "content-type": h.dataType
                }[t.toLowerCase()];
              }, r && (b.status = Number(r.getAttribute("status")) || b.status, b.statusText = r.getAttribute("statusText") || b.statusText);
              var c = (h.dataType || "").toLowerCase(), d = /(json|script|text)/.test(c);
              if (d || h.textarea) {
                var u = I.getElementsByTagName("textarea")[0];
                if (u) b.responseText = u.value, b.status = Number(u.getAttribute("status")) || b.status, 
                b.statusText = u.getAttribute("statusText") || b.statusText; else if (d) {
                  var f = I.getElementsByTagName("pre")[0], m = I.getElementsByTagName("body")[0];
                  f ? b.responseText = f.textContent ? f.textContent : f.innerText : m && (b.responseText = m.textContent ? m.textContent : m.innerText);
                }
              } else "xml" == c && !b.responseXML && b.responseText && (b.responseXML = E(b.responseText));
              try {
                D = O(b, c, h);
              } catch (t) {
                s = "parsererror", b.error = i = t || s;
              }
            } catch (t) {
              n("error caught: ", t), s = "error", b.error = i = t || s;
            }
            b.aborted && (n("upload aborted"), s = null), b.status && (s = b.status >= 200 && b.status < 300 || 304 === b.status ? "success" : "error"), 
            "success" === s ? (h.success && h.success.call(h.context, D, "success", b), _.resolve(b.responseText, "success", b), 
            p && t.event.trigger("ajaxSuccess", [ b, h ])) : s && (void 0 === i && (i = b.statusText), 
            h.error && h.error.call(h.context, b, s, i), _.reject(b, "error", i), p && t.event.trigger("ajaxError", [ b, h, i ])), 
            p && t.event.trigger("ajaxComplete", [ b, h ]), p && !--t.active && t.event.trigger("ajaxStop"), 
            h.complete && h.complete.call(h.context, b, s), j = !0, h.timeout && clearTimeout(x), 
            setTimeout(function() {
              h.iframeTarget ? g.attr("src", h.iframeSrc) : g.remove(), b.responseXML = null;
            }, 100);
          }
        }
      }
      var c, u, h, p, m, g, v, b, C, y, w, x, k = d[0], _ = t.Deferred();
      if (_.abort = function(t) {
        b.abort(t);
      }, i) for (u = 0; u < f.length; u++) c = t(f[u]), s ? c.prop("disabled", !1) : c.removeAttr("disabled");
      if (h = t.extend(!0, {}, t.ajaxSettings, e), h.context = h.context || h, m = "jqFormIO" + new Date().getTime(), 
      h.iframeTarget ? (g = t(h.iframeTarget), (y = g.attr2("name")) ? m = y : g.attr2("name", m)) : (g = t('<iframe name="' + m + '" src="' + h.iframeSrc + '" />')).css({
        position: "absolute",
        top: "-1000px",
        left: "-1000px"
      }), v = g[0], b = {
        aborted: 0,
        responseText: null,
        responseXML: null,
        status: 0,
        statusText: "n/a",
        getAllResponseHeaders: function() {},
        getResponseHeader: function() {},
        setRequestHeader: function() {},
        abort: function(e) {
          var i = "timeout" === e ? "timeout" : "aborted";
          n("aborting upload... " + i), this.aborted = 1;
          try {
            v.contentWindow.document.execCommand && v.contentWindow.document.execCommand("Stop");
          } catch (t) {}
          g.attr("src", h.iframeSrc), b.error = i, h.error && h.error.call(h.context, b, i, e), 
          p && t.event.trigger("ajaxError", [ b, h, i ]), h.complete && h.complete.call(h.context, b, i);
        }
      }, (p = h.global) && 0 == t.active++ && t.event.trigger("ajaxStart"), p && t.event.trigger("ajaxSend", [ b, h ]), 
      h.beforeSend && !1 === h.beforeSend.call(h.context, b, h)) return h.global && t.active--, 
      _.reject(), _;
      if (b.aborted) return _.reject(), _;
      (C = k.clk) && (y = C.name) && !C.disabled && (h.extraData = h.extraData || {}, 
      h.extraData[y] = C.value, "image" == C.type && (h.extraData[y + ".x"] = k.clk_x, 
      h.extraData[y + ".y"] = k.clk_y));
      var $ = 1, A = 2, S = t("meta[name=csrf-token]").attr("content"), T = t("meta[name=csrf-param]").attr("content");
      T && S && (h.extraData = h.extraData || {}, h.extraData[T] = S), h.forceSync ? a() : setTimeout(a, 10);
      var D, I, j, P = 50, E = t.parseXML || function(t, e) {
        return window.ActiveXObject ? (e = new ActiveXObject("Microsoft.XMLDOM"), e.async = "false", 
        e.loadXML(t)) : e = new DOMParser().parseFromString(t, "text/xml"), e && e.documentElement && "parsererror" != e.documentElement.nodeName ? e : null;
      }, N = t.parseJSON || function(t) {
        return window.eval("(" + t + ")");
      }, O = function(e, i, n) {
        var o = e.getResponseHeader("content-type") || "", s = "xml" === i || !i && o.indexOf("xml") >= 0, a = s ? e.responseXML : e.responseText;
        return s && "parsererror" === a.documentElement.nodeName && t.error && t.error("parsererror"), 
        n && n.dataFilter && (a = n.dataFilter(a, i)), "string" == typeof a && ("json" === i || !i && o.indexOf("json") >= 0 ? a = N(a) : ("script" === i || !i && o.indexOf("javascript") >= 0) && t.globalEval(a)), 
        a;
      };
      return _;
    }
    if (!this.length) return n("ajaxSubmit: skipping submit process - no element selected"), 
    this;
    var r, l, c, d = this;
    "function" == typeof e ? e = {
      success: e
    } : void 0 === e && (e = {}), r = e.type || this.attr2("method"), (c = (c = "string" == typeof (l = e.url || this.attr2("action")) ? t.trim(l) : "") || window.location.href || "") && (c = (c.match(/^([^#]+)/) || [])[1]), 
    e = t.extend(!0, {
      url: c,
      success: t.ajaxSettings.success,
      type: r || t.ajaxSettings.type,
      iframeSrc: /^https/i.test(window.location.href || "") ? "javascript:false" : "about:blank"
    }, e);
    var u = {};
    if (this.trigger("form-pre-serialize", [ this, e, u ]), u.veto) return n("ajaxSubmit: submit vetoed via form-pre-serialize trigger"), 
    this;
    if (e.beforeSerialize && !1 === e.beforeSerialize(this, e)) return n("ajaxSubmit: submit aborted via beforeSerialize callback"), 
    this;
    var h = e.traditional;
    void 0 === h && (h = t.ajaxSettings.traditional);
    var p, f = [], m = this.formToArray(e.semantic, f);
    if (e.data && (e.extraData = e.data, p = t.param(e.data, h)), e.beforeSubmit && !1 === e.beforeSubmit(m, this, e)) return n("ajaxSubmit: submit aborted via beforeSubmit callback"), 
    this;
    if (this.trigger("form-submit-validate", [ m, this, e, u ]), u.veto) return n("ajaxSubmit: submit vetoed via form-submit-validate trigger"), 
    this;
    var g = t.param(m, h);
    p && (g = g ? g + "&" + p : p), "GET" == e.type.toUpperCase() ? (e.url += (e.url.indexOf("?") >= 0 ? "&" : "?") + g, 
    e.data = null) : e.data = g;
    var v = [];
    if (e.resetForm && v.push(function() {
      d.resetForm();
    }), e.clearForm && v.push(function() {
      d.clearForm(e.includeHidden);
    }), !e.dataType && e.target) {
      var b = e.success || function() {};
      v.push(function(i) {
        var n = e.replaceTarget ? "replaceWith" : "html";
        t(e.target)[n](i).each(b, arguments);
      });
    } else e.success && v.push(e.success);
    if (e.success = function(t, i, n) {
      for (var o = e.context || this, s = 0, a = v.length; a > s; s++) v[s].apply(o, [ t, i, n || d, d ]);
    }, e.error) {
      var C = e.error;
      e.error = function(t, i, n) {
        var o = e.context || this;
        C.apply(o, [ t, i, n, d ]);
      };
    }
    if (e.complete) {
      var y = e.complete;
      e.complete = function(t, i) {
        var n = e.context || this;
        y.apply(n, [ t, i, d ]);
      };
    }
    var w = t("input[type=file]:enabled", this).filter(function() {
      return "" !== t(this).val();
    }).length > 0, x = "multipart/form-data", k = d.attr("enctype") == x || d.attr("encoding") == x, _ = o.fileapi && o.formdata;
    n("fileAPI :" + _);
    var $, A = (w || k) && !_;
    !1 !== e.iframe && (e.iframe || A) ? e.closeKeepAlive ? t.get(e.closeKeepAlive, function() {
      $ = a(m);
    }) : $ = a(m) : $ = (w || k) && _ ? function(n) {
      for (var o = new FormData(), s = 0; s < n.length; s++) o.append(n[s].name, n[s].value);
      if (e.extraData) {
        var a = i(e.extraData);
        for (s = 0; s < a.length; s++) a[s] && o.append(a[s][0], a[s][1]);
      }
      e.data = null;
      var l = t.extend(!0, {}, t.ajaxSettings, e, {
        contentType: !1,
        processData: !1,
        cache: !1,
        type: r || "POST"
      });
      e.uploadProgress && (l.xhr = function() {
        var i = t.ajaxSettings.xhr();
        return i.upload && i.upload.addEventListener("progress", function(t) {
          var i = 0, n = t.loaded || t.position, o = t.total;
          t.lengthComputable && (i = Math.ceil(n / o * 100)), e.uploadProgress(t, n, o, i);
        }, !1), i;
      }), l.data = null;
      var c = l.beforeSend;
      return l.beforeSend = function(t, i) {
        i.data = e.formData ? e.formData : o, c && c.call(this, t, i);
      }, t.ajax(l);
    }(m) : t.ajax(e), d.removeData("jqxhr").data("jqxhr", $);
    for (var S = 0; S < f.length; S++) f[S] = null;
    return this.trigger("form-submit-notify", [ this, e ]), this;
  }, t.fn.ajaxForm = function(o) {
    if (o = o || {}, o.delegation = o.delegation && t.isFunction(t.fn.on), !o.delegation && 0 === this.length) {
      var s = {
        s: this.selector,
        c: this.context
      };
      return !t.isReady && s.s ? (n("DOM not ready, queuing ajaxForm"), t(function() {
        t(s.s, s.c).ajaxForm(o);
      }), this) : (n("terminating; zero elements found by selector" + (t.isReady ? "" : " (DOM not ready)")), 
      this);
    }
    return o.delegation ? (t(document).off("submit.form-plugin", this.selector, e).off("click.form-plugin", this.selector, i).on("submit.form-plugin", this.selector, o, e).on("click.form-plugin", this.selector, o, i), 
    this) : this.ajaxFormUnbind().bind("submit.form-plugin", o, e).bind("click.form-plugin", o, i);
  }, t.fn.ajaxFormUnbind = function() {
    return this.unbind("submit.form-plugin click.form-plugin");
  }, t.fn.formToArray = function(e, i) {
    var n = [];
    if (0 === this.length) return n;
    var s, a = this[0], r = this.attr("id"), l = e ? a.getElementsByTagName("*") : a.elements;
    if (l && !/MSIE [678]/.test(navigator.userAgent) && (l = t(l).get()), r && (s = t(':input[form="' + r + '"]').get()).length && (l = (l || []).concat(s)), 
    !l || !l.length) return n;
    var c, d, u, h, p, f, m;
    for (c = 0, f = l.length; f > c; c++) if (p = l[c], (u = p.name) && !p.disabled) if (e && a.clk && "image" == p.type) a.clk == p && (n.push({
      name: u,
      value: t(p).val(),
      type: p.type
    }), n.push({
      name: u + ".x",
      value: a.clk_x
    }, {
      name: u + ".y",
      value: a.clk_y
    })); else if ((h = t.fieldValue(p, !0)) && h.constructor == Array) for (i && i.push(p), 
    d = 0, m = h.length; m > d; d++) n.push({
      name: u,
      value: h[d]
    }); else if (o.fileapi && "file" == p.type) {
      i && i.push(p);
      var g = p.files;
      if (g.length) for (d = 0; d < g.length; d++) n.push({
        name: u,
        value: g[d],
        type: p.type
      }); else n.push({
        name: u,
        value: "",
        type: p.type
      });
    } else null !== h && void 0 !== h && (i && i.push(p), n.push({
      name: u,
      value: h,
      type: p.type,
      required: p.required
    }));
    if (!e && a.clk) {
      var v = t(a.clk), b = v[0];
      (u = b.name) && !b.disabled && "image" == b.type && (n.push({
        name: u,
        value: v.val()
      }), n.push({
        name: u + ".x",
        value: a.clk_x
      }, {
        name: u + ".y",
        value: a.clk_y
      }));
    }
    return n;
  }, t.fn.formSerialize = function(e) {
    return t.param(this.formToArray(e));
  }, t.fn.fieldSerialize = function(e) {
    var i = [];
    return this.each(function() {
      var n = this.name;
      if (n) {
        var o = t.fieldValue(this, e);
        if (o && o.constructor == Array) for (var s = 0, a = o.length; a > s; s++) i.push({
          name: n,
          value: o[s]
        }); else null !== o && void 0 !== o && i.push({
          name: this.name,
          value: o
        });
      }
    }), t.param(i);
  }, t.fn.fieldValue = function(e) {
    for (var i = [], n = 0, o = this.length; o > n; n++) {
      var s = this[n], a = t.fieldValue(s, e);
      null === a || void 0 === a || a.constructor == Array && !a.length || (a.constructor == Array ? t.merge(i, a) : i.push(a));
    }
    return i;
  }, t.fieldValue = function(e, i) {
    var n = e.name, o = e.type, s = e.tagName.toLowerCase();
    if (void 0 === i && (i = !0), i && (!n || e.disabled || "reset" == o || "button" == o || ("checkbox" == o || "radio" == o) && !e.checked || ("submit" == o || "image" == o) && e.form && e.form.clk != e || "select" == s && -1 == e.selectedIndex)) return null;
    if ("select" == s) {
      var a = e.selectedIndex;
      if (0 > a) return null;
      for (var r = [], l = e.options, c = "select-one" == o, d = c ? a + 1 : l.length, u = c ? a : 0; d > u; u++) {
        var h = l[u];
        if (h.selected) {
          var p = h.value;
          if (p || (p = h.attributes && h.attributes.value && !h.attributes.value.specified ? h.text : h.value), 
          c) return p;
          r.push(p);
        }
      }
      return r;
    }
    return t(e).val();
  }, t.fn.clearForm = function(e) {
    return this.each(function() {
      t("input,select,textarea", this).clearFields(e);
    });
  }, t.fn.clearFields = t.fn.clearInputs = function(e) {
    var i = /^(?:color|date|datetime|email|month|number|password|range|search|tel|text|time|url|week)$/i;
    return this.each(function() {
      var n = this.type, o = this.tagName.toLowerCase();
      i.test(n) || "textarea" == o ? this.value = "" : "checkbox" == n || "radio" == n ? this.checked = !1 : "select" == o ? this.selectedIndex = -1 : "file" == n ? /MSIE/.test(navigator.userAgent) ? t(this).replaceWith(t(this).clone(!0)) : t(this).val("") : e && (!0 === e && /hidden/.test(n) || "string" == typeof e && t(this).is(e)) && (this.value = "");
    });
  }, t.fn.resetForm = function() {
    return this.each(function() {
      ("function" == typeof this.reset || "object" == typeof this.reset && !this.reset.nodeType) && this.reset();
    });
  }, t.fn.enable = function(t) {
    return void 0 === t && (t = !0), this.each(function() {
      this.disabled = !t;
    });
  }, t.fn.selected = function(e) {
    return void 0 === e && (e = !0), this.each(function() {
      var i = this.type;
      if ("checkbox" == i || "radio" == i) this.checked = e; else if ("option" == this.tagName.toLowerCase()) {
        var n = t(this).parent("select");
        e && n[0] && "select-one" == n[0].type && n.find("option").selected(!1), this.selected = e;
      }
    });
  }, t.fn.ajaxSubmit.debug = !1;
}), function(t) {
  t.fn.hoverIntent = function(e, i, n) {
    var o = {
      interval: 100,
      sensitivity: 6,
      timeout: 0
    };
    o = "object" == typeof e ? t.extend(o, e) : t.isFunction(i) ? t.extend(o, {
      over: e,
      out: i,
      selector: n
    }) : t.extend(o, {
      over: e,
      out: e,
      selector: i
    });
    var s, a, r, l, c = function(t) {
      s = t.pageX, a = t.pageY;
    }, d = function(e, i) {
      if (i.hoverIntent_t = clearTimeout(i.hoverIntent_t), Math.sqrt((r - s) * (r - s) + (l - a) * (l - a)) < o.sensitivity) return t(i).off("mousemove.hoverIntent", c), 
      i.hoverIntent_s = !0, o.over.apply(i, [ e ]);
      r = s, l = a, i.hoverIntent_t = setTimeout(function() {
        d(e, i);
      }, o.interval);
    }, u = function(t, e) {
      return e.hoverIntent_t = clearTimeout(e.hoverIntent_t), e.hoverIntent_s = !1, o.out.apply(e, [ t ]);
    }, h = function(e) {
      var i = t.extend({}, e), n = this;
      n.hoverIntent_t && (n.hoverIntent_t = clearTimeout(n.hoverIntent_t)), "mouseenter" === e.type ? (r = i.pageX, 
      l = i.pageY, t(n).on("mousemove.hoverIntent", c), n.hoverIntent_s || (n.hoverIntent_t = setTimeout(function() {
        d(i, n);
      }, o.interval))) : (t(n).off("mousemove.hoverIntent", c), n.hoverIntent_s && (n.hoverIntent_t = setTimeout(function() {
        u(i, n);
      }, o.timeout)));
    };
    return this.on({
      "mouseenter.hoverIntent": h,
      "mouseleave.hoverIntent": h
    }, o.selector);
  };
}(jQuery), function(t) {
  t.extend(t.fn, {
    pstrength: function(e) {
      var e = t.extend({
        verdicts: [ "Too Short!", "Very Weak", "Weak", "Medium", "Strong", "Very Strong" ],
        colors: [ "#ccc", "#f00", "#c06", "#f60", "#3c0", "#3f0" ],
        values: [ 20, 30, 40, 60, 80, 100 ],
        scores: [ 10, 15, 30, 40 ],
        minchar: 6,
        minCharText: "Minimum length is %d characters"
      }, e);
      return this.each(function() {
        var i = t(this).attr("id");
        t(this).after('<div id="' + i + '_bar" class="progress" style="display: none;"><div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"><span  id="' + i + '_text">0%<span></div></div>'), 
        e.minchar > 0 && t(this).after('<div class="help-block" id="' + i + '_minchar">' + e.minCharText.replace("%d", e.minchar) + "</div>"), 
        e.$ctlBar = t("#" + i + "_bar .progress-bar"), e.$ctlText = t("#" + i + "_text"), 
        t(this).keyup(function() {
          e.minchar <= t(this).val().length && t("#" + i + "_bar:hidden") && t("#" + i + "_bar").show(), 
          t.fn.runPassword(t(this).val(), i, e);
        });
      });
    },
    runPassword: function(e, i, n) {
      nPerc = t.fn.checkPassword(e, n);
      var o;
      o = nPerc < 0 ? 0 : nPerc <= n.scores[0] ? 1 : nPerc > n.scores[0] && nPerc <= n.scores[1] ? 2 : nPerc > n.scores[1] && nPerc <= n.scores[2] ? 3 : nPerc > n.scores[2] && nPerc <= n.scores[3] ? 4 : 5, 
      n.$ctlBar.css({
        width: n.values[o] + "%"
      }), n.$ctlBar.attr("aria-valuenow", n.values[o]), n.$ctlBar.css({
        backgroundColor: n.colors[o]
      }), n.$ctlText.html(n.verdicts[o]);
    },
    checkPassword: function(t, e) {
      var i = 0;
      e.verdicts[0];
      return t.length < e.minchar ? i -= 100 : t.length > e.minchar + 1 && t.length < e.minchar + 3 ? i += 6 : t.length > e.minchar + 2 && t.length < e.minchar + 4 ? i += 12 : t.length > 2 * e.minchar && (i += 18), 
      t.match(/[a-z]/) && (i += 1), t.match(/[A-Z]/) && (i += 5), t.match(/\d+/) && (i += 5), 
      t.match(/(.*[0-9].*[0-9].*[0-9])/) && (i += 5), t.match(/.[!,@,#,$,%,^,&,*,?,_,~]/) && (i += 5), 
      t.match(/(.*[!,@,#,$,%,^,&,*,?,_,~].*[!,@,#,$,%,^,&,*,?,_,~])/) && (i += 5), t.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/) && (i += 2), 
      t.match(/([a-zA-Z])/) && t.match(/([0-9])/) && (i += 2), t.match(/([a-zA-Z0-9].*[!,@,#,$,%,^,&,*,?,_,~])|([!,@,#,$,%,^,&,*,?,_,~].*[a-zA-Z0-9])/) && (i += 2), 
      i;
    }
  });
}(jQuery), jQuery(function() {
  jQuery("div.svw").prepend("<img src='" + ACC.config.commonResourcePath + "/images/spinner.gif' class='ldrgif' alt='loading...'/ >");
});

var j = 0, quantofamo = 0, currentActive = 0, total = 0, listelements = [];

jQuery.fn.slideView = function(t) {
  return t = jQuery.extend({
    easeFunc: "easeInOutExpo",
    easeTime: 750,
    uiBefore: !1,
    toolTip: !1,
    ttOpacity: .9,
    autoPlay: !1,
    autoPlayTime: "8000"
  }, t), this.each(function() {
    var e = jQuery(this);
    e.find("img.ldrgif").remove(), e.removeClass("svw").addClass("stripViewer");
    var i = e.find("img").width(), n = e.find("img").height(), o = e.find("li").size(), s = i * o;
    e.find("ul").css("width", s), e.css("width", i), e.css("height", n), e.each(function(n) {
      function o(n, o) {
        n.addClass("current").parent().parent().find("a").not(n).removeClass("current"), 
        currentActive = o;
        var s = -i * o;
        e.find("ul").animate({
          left: s
        }, t.easeTime, t.easeFunc);
      }
      if (t.uiBefore ? jQuery(this).before("<div class='stripTransmitter' id='stripTransmitter" + j + "'><ul></ul></div>") : jQuery(this).after("<div class='stripTransmitter' id='stripTransmitter" + j + "'><ul></ul></div>"), 
      jQuery(this).find("li").each(function(t) {
        jQuery("div#stripTransmitter" + j + " ul").append("<li><a title='" + jQuery(this).find("img").attr("alt") + "' href='#'>" + (t + 1) + "</a></li>");
      }), jQuery("div#stripTransmitter" + j + " a").each(function(t) {
        jQuery(this).bind("click", function() {
          return o(jQuery(this), t), !1;
        }), listelements.push(jQuery(this)), total++;
      }), e.bind("click", function(e) {
        var n = t.uiBefore ? jQuery(this).prev().find("a.current") : jQuery(this).next().find("a.current"), o = parseFloat(jQuery(this).css("borderLeftWidth").replace("px", "")) + parseFloat(jQuery(this).css("borderRightWidth").replace("px", "")), s = jQuery(this).offset();
        if (o / 2 + i - (e.pageX - s.left) >= i / 2) {
          var a = n.parent().prev().find("a");
          0 != jQuery(a).length ? a.trigger("click") : n.parent().parent().find("a:last").trigger("click");
        } else {
          var r = n.parent().next().find("a");
          0 != jQuery(r).length ? r.trigger("click") : n.parent().parent().find("a:first").trigger("click");
        }
      }), jQuery("div#stripTransmitter" + j).css("width", i), jQuery("div#stripTransmitter" + j + " a:first").addClass("current"), 
      jQuery("body").append('<div class="tooltip" style="display:none;"></div>'), t.autoPlay) setInterval(function() {
        o(total > currentActive + 1 ? listelements[currentActive + 1] : listelements[0], total > currentActive + 1 ? currentActive + 1 : 0);
      }, t.autoPlayTime);
      if (t.toolTip) {
        var s = jQuery("div#stripTransmitter" + j + " a");
        s.each(function() {
          jQuery(this).data("title", jQuery(this).prop("title"));
        }), s.on("mousemove", function(e) {
          var i = jQuery(this).data("title");
          posX = e.pageX + 10, posY = e.pageY + 10, jQuery(".tooltip").html(i).css({
            position: "absolute",
            top: posY + "px",
            left: posX + "px",
            display: "block",
            opacity: t.ttOpacity
          });
        }), s.on("mouseout", function() {
          jQuery(".tooltip").hide(), this.title = jQuery(this).data("title");
        }), s.on("mouseover", function() {
          this.title = "";
        });
      }
    }), j++;
  });
}, function(t) {
  var e = function() {
    var t = 0, e = [ [ "min-height", "0px" ], [ "height", "1%" ] ], i = /(msie) ([\w.]+)/.exec(navigator.userAgent.toLowerCase()) || [], n = i[1] || "", o = i[2] || "0";
    return "msie" === n && o < 7 && (t = 1), {
      name: e[t][0],
      autoheightVal: e[t][1]
    };
  };
  t.getSyncedHeight = function(i) {
    var n = 0, o = e();
    return t(i).each(function() {
      t(this).css(o.name, o.autoheightVal);
      var e = parseInt(t(this).css("height"), 10);
      e > n && (n = e);
    }), n;
  }, t.fn.syncHeight = function(i) {
    var n = {
      updateOnResize: !1,
      height: !1
    }, o = t.extend(n, i), s = this, a = 0, r = e().name;
    return 1 == o.parent ? (t(this).each(function() {
      t(this).css(r, "0px");
    }), t(this).each(function() {
      t(this).css(r, t(this).parent().innerHeight() + "px");
    })) : (a = "number" == typeof o.height ? o.height : t.getSyncedHeight(this), t(this).each(function() {
      t(this).css(r, a + "px");
    })), !0 === o.updateOnResize && t(window).bind("resize.syncHeight", function() {
      o.updateOnResize = !1, t(s).syncHeight(o);
    }), this;
  }, t.fn.unSyncHeight = function() {
    t(window).unbind("resize.syncHeight");
    var i = e().name;
    t(this).each(function() {
      t(this).css(i, "");
    });
  };
}(jQuery), function(t) {
  function e(t, e) {
    i && window.console && window.console.log && (e ? window.console.log(e + ": ", t) : window.console.log(t));
  }
  var i = !1;
  t.fn.extend({
    getUniqueId: function(t, e, i) {
      return i = void 0 === i ? "" : "-" + i, t + e + i;
    },
    accessibleTabs: function(i) {
      var n = {
        wrapperClass: "content",
        currentClass: "current",
        tabhead: "h4",
        tabheadClass: "tabhead",
        tabbody: ".tabbody",
        fx: "show",
        fxspeed: "normal",
        currentInfoText: "current tab: ",
        currentInfoPosition: "prepend",
        currentInfoClass: "current-info",
        tabsListClass: "tabs-list",
        syncheights: !1,
        syncHeightMethodName: "syncHeight",
        cssClassAvailable: !1,
        saveState: !1,
        autoAnchor: !1,
        pagination: !1,
        position: "top",
        wrapInnerNavLinks: "",
        firstNavItemClass: "first",
        lastNavItemClass: "last",
        clearfixClass: "clearfix"
      }, o = {
        37: -1,
        38: -1,
        39: 1,
        40: 1
      }, s = {
        top: "prepend",
        bottom: "append"
      };
      this.options = t.extend(n, i);
      var a = 0;
      void 0 !== t("body").data("accessibleTabsCount") && (a = t("body").data("accessibleTabsCount")), 
      t("body").data("accessibleTabsCount", this.size() + a);
      var r = this;
      return this.each(function(i) {
        var n = t(this), l = "", c = 0, d = [];
        t(n).wrapInner('<div class="' + r.options.wrapperClass + '"></div>'), e(t(n).find(r.options.tabhead).children("a")), 
        t(n).find(r.options.tabhead).each(function(n) {
          e(t(this));
          var o = "", s = t(this).attr("id");
          if (s) {
            if (0 === s.indexOf("accessibletabscontent")) return;
            o = ' id="' + s + '"';
          }
          var u = r.getUniqueId("accessibletabscontent", a + i, n), h = r.getUniqueId("accessibletabsnavigation", a + i, n);
          d.push(u);
          var p = t(this).children("a").length > 0 ? t(this).children("a").html() : t(this).html(), f = "";
          if (t(this).attr("aria-label") && (f = 'aria-label="' + t(this).attr("aria-label") + '"'), 
          !0 === r.options.cssClassAvailable) {
            var m = "";
            t(this).attr("class") && (m = ' class="' + (m = t(this).attr("class")) + '"'), l += '<li id="' + h + '"><a' + o + m + ' href="#' + u + '" ' + f + ">" + p + "</a></li>";
          } else l += '<li id="' + h + '"><a' + o + ' href="#' + u + '" ' + f + ">" + p + "</a></li>";
          t(this).attr({
            id: u,
            class: r.options.tabheadClass,
            tabindex: "-1"
          }), c++;
        }), r.options.syncheights && t.fn[r.options.syncHeightMethodName] && (t(n).find(r.options.tabbody)[r.options.syncHeightMethodName](), 
        t(window).resize(function() {
          t(n).find(r.options.tabbody)[r.options.syncHeightMethodName]();
        }));
        var u = "." + r.options.tabsListClass;
        t(n).find(u).length || t(n)[s[r.options.position]]('<ul class="' + r.options.clearfixClass + " " + r.options.tabsListClass + " tabamount" + c + '"></ul>'), 
        t(n).find(u).append(l);
        var h = t(n).find(r.options.tabbody);
        h.length > 0 && (t(h).hide(), t(h[0]).show()), t(n).find("ul." + r.options.tabsListClass + ">li:first").addClass(r.options.currentClass).addClass(r.options.firstNavItemClass).find("a")[r.options.currentInfoPosition]('<span class="' + r.options.currentInfoClass + '">' + r.options.currentInfoText + "</span>").parents("ul." + r.options.tabsListClass).children("li:last").addClass(r.options.lastNavItemClass);
        var p = t(n).find(r.options.tabhead);
        if (p.length > 0 && (t(p[0]).addClass(r.options.currentClass).addClass(r.options.firstNavItemClass), 
        t(p[p.length - 1]).addClass(r.options.lastNavItemClass)), r.options.wrapInnerNavLinks && t(n).find("ul." + r.options.tabsListClass + ">li>a").wrapInner(r.options.wrapInnerNavLinks), 
        t(n).find("ul." + r.options.tabsListClass + ">li>a").each(function(e) {
          t(this).click(function(i) {
            i.preventDefault(), n.trigger("showTab.accessibleTabs", [ t(i.target) ]), r.options.saveState && t.cookie && t.cookie("accessibletab_" + n.attr("id") + "_active", e), 
            t(n).find("ul." + r.options.tabsListClass + ">li." + r.options.currentClass).removeClass(r.options.currentClass).find("span." + r.options.currentInfoClass).remove(), 
            t(n).find(r.options.tabhead).removeClass(r.options.currentClass), t(this).blur(), 
            t(n).find(r.options.tabbody + ":visible").hide(), t(n).find(r.options.tabbody).eq(e)[r.options.fx](r.options.fxspeed), 
            t(this)[r.options.currentInfoPosition]('<span class="' + r.options.currentInfoClass + '">' + r.options.currentInfoText + "</span>").parent().addClass(r.options.currentClass), 
            t(t(this).attr("href")).addClass(r.options.currentClass), t(t(this).attr("href")).focus().keyup(function(i) {
              o[i.keyCode] && (r.showAccessibleTab(e + o[i.keyCode]), t(this).unbind("keyup"));
            });
          }), t(this).focus(function() {
            t(document).keyup(function(t) {
              o[t.keyCode] && r.showAccessibleTab(e + o[t.keyCode]);
            });
          }), t(this).blur(function() {
            t(document).unbind("keyup");
          });
        }), r.options.saveState && t.cookie) {
          var f = t.cookie("accessibletab_" + n.attr("id") + "_active");
          e(t.cookie("accessibletab_" + n.attr("id") + "_active")), null !== f && r.showAccessibleTab(f, n.attr("id"));
        }
        if (r.options.autoAnchor && window.location.hash) {
          var m = t("." + r.options.tabsListClass).find(window.location.hash);
          m.size() && m.click();
        }
        if (r.options.pagination) {
          var g = '<ul class="pagination">';
          g += '    <li class="previous"><a href="#{previousAnchor}"><span>{previousHeadline}</span></a></li>', 
          g += '    <li class="next"><a href="#{nextAnchor}"><span>{nextHeadline}</span></a></li>', 
          g += "</ul>";
          var v = t(n).find(".tabbody"), b = v.size();
          v.each(function(e) {
            t(this).append(g);
            var i = e + 1;
            i >= b && (i = 0);
            var o = e - 1;
            o < 0 && (o = b - 1);
            var s = t(this).find(".pagination"), a = s.find(".previous");
            a.find("span").text(t("#" + d[o]).text()), a.find("a").attr("href", "#" + d[o]).click(function(e) {
              e.preventDefault(), t(n).find(".tabs-list a").eq(o).click();
            });
            var r = s.find(".next");
            r.find("span").text(t("#" + d[i]).text()), r.find("a").attr("href", "#" + d[i]).click(function(e) {
              e.preventDefault(), t(n).find(".tabs-list a").eq(i).click();
            });
          });
        }
      });
    },
    showAccessibleTab: function(i, n) {
      e("showAccessibleTab");
      var o = this;
      if (!n) return this.each(function() {
        var e = t(this);
        e.trigger("showTab.accessibleTabs");
        var n = e.find("ul." + o.options.tabsListClass + ">li>a");
        e.trigger("showTab.accessibleTabs", [ n.eq(i) ]), n.eq(i).click();
      });
      var s = t("#" + n), a = s.find("ul." + o.options.tabsListClass + ">li>a");
      s.trigger("showTab.accessibleTabs", [ a.eq(i) ]), a.eq(i).click();
    },
    showAccessibleTabSelector: function(i) {
      e("showAccessibleTabSelector");
      var n = t(i);
      n && ("a" === n.get(0).nodeName.toLowerCase() ? n.click() : e("the selector of a showAccessibleTabSelector() call needs to point to a tabs headline!"));
    }
  });
}(jQuery), function(t) {
  var e = "waitForImages";
  t.waitForImages = {
    hasImageProperties: [ "backgroundImage", "listStyleImage", "borderImage", "borderCornerImage" ]
  }, t.expr[":"].uncached = function(e) {
    if (!t(e).is('img[src!=""]')) return !1;
    var i = new Image();
    return i.src = e.src, !i.complete;
  }, t.fn.waitForImages = function(i, n, o) {
    var s = 0, a = 0;
    if (t.isPlainObject(arguments[0]) && (o = arguments[0].waitForAll, n = arguments[0].each, 
    i = arguments[0].finished), i = i || t.noop, n = n || t.noop, o = !!o, !t.isFunction(i) || !t.isFunction(n)) throw new TypeError("An invalid callback was supplied.");
    return this.each(function() {
      var r = t(this), l = [], c = t.waitForImages.hasImageProperties || [], d = /url\(\s*(['"]?)(.*?)\1\s*\)/g;
      o ? r.find("*").andSelf().each(function() {
        var e = t(this);
        e.is("img:uncached") && l.push({
          src: e.attr("src"),
          element: e[0]
        }), t.each(c, function(t, i) {
          var n, o = e.css(i);
          if (!o) return !0;
          for (;n = d.exec(o); ) l.push({
            src: n[2],
            element: e[0]
          });
        });
      }) : r.find("img:uncached").each(function() {
        l.push({
          src: this.src,
          element: this
        });
      }), s = l.length, a = 0, 0 === s && i.call(r[0]), t.each(l, function(o, l) {
        var c = new Image();
        t(c).bind("load." + e + " error." + e, function(t) {
          if (a++, n.call(l.element, a, s, "load" == t.type), a == s) return i.call(r[0]), 
          !1;
        }), c.src = l.src;
      });
    });
  };
}(jQuery), function(t) {
  var e = {
    url: !1,
    callback: !1,
    target: !1,
    duration: 120,
    on: "mouseover",
    touch: !0,
    onZoomIn: !1,
    onZoomOut: !1,
    zoomEnableCallBack: !1,
    zoomEnable: !0,
    magnify: 1,
    longtouchtimer: function() {},
    touchduration: 500
  };
  t.zoom = function(e, i, n, o) {
    var s, a, r, l, c, d, u, h = t(e).css("position"), p = t(i);
    e.style.position = /(absolute|fixed)/.test(h) ? h : "relative", e.style.overflow = "hidden", 
    n.style.width = n.style.height = "";
    var f = t("<div class='zoomImg'><div class='zoomImgMask'></div></div>").css({
      width: n.width * o,
      height: n.height * o
    }).appendTo(e);
    return t(n).appendTo(f), {
      init: function() {
        a = t(e).outerWidth(), s = t(e).outerHeight(), i === e ? (l = a, r = s) : (l = p.outerWidth(), 
        r = p.outerHeight()), c = (n.width - a) / l, d = (n.height - s) / r, u = p.offset();
      },
      move: function(e) {
        void 0 == u && (u = {
          left: 0,
          top: 0
        });
        var i = e.pageX - u.left, o = e.pageY - u.top;
        o = Math.max(Math.min(o, r), 0), i = Math.max(Math.min(i, l), 0), t(n).parent(".zoomImg").css("left", i * -c + "px"), 
        t(n).parent(".zoomImg").css("top", o * -d + "px");
      }
    };
  }, t.fn.zoom = function(i) {
    return this.each(function() {
      var n, o = t.extend({}, e, i || {}), s = o.target || this, a = this, r = t(a), l = document.createElement("img"), c = t(l), d = "mousemove.zoom", u = !1, h = !1;
      (o.url || ((n = r.find("img"))[0] && (o.url = n.data("src") || n.attr("src")), o.url)) && (!function() {
        var t = s.style.position, e = s.style.overflow;
        r.one("zoom.destroy", function() {
          r.off(".zoom"), s.style.position = t, s.style.overflow = e, c.remove();
        });
      }(), l.onload = function() {
        function e(e) {
          if (t.isFunction(o.zoomEnableCallBack) && (o.zoomEnable = o.zoomEnableCallBack.call(l)), 
          !o.zoomEnable) return !1;
          n.init(), n.move(e), t(l).parent(".zoomImg").stop().fadeTo(t.support.opacity ? o.duration : 0, 1, !!t.isFunction(o.onZoomIn) && o.onZoomIn.call(l));
        }
        function i() {
          o.longtouchtimer && clearTimeout(o.longtouchtimer), t(l).parent(".zoomImg").stop().fadeTo(o.duration, 0, !!t.isFunction(o.onZoomOut) && o.onZoomOut.call(l));
        }
        var n = t.zoom(s, a, l, o.magnify);
        "grab" === o.on ? r.on("mousedown.zoom", function(s) {
          1 === s.which && (t(document).one("mouseup.zoom", function() {
            i(), t(document).off(d, n.move);
          }), o.longtouchtimer = setTimeout(function() {
            e(s), t(document).on(d, n.move);
          }, o.touchduration), s.preventDefault());
        }) : "click" === o.on ? r.on("click.zoom", function(o) {
          return u ? void 0 : (u = !0, e(o), t(document).on(d, n.move), t(document).one("click.zoom", function() {
            i(), u = !1, t(document).off(d, n.move);
          }), !1);
        }) : "toggle" === o.on ? r.on("click.zoom", function(t) {
          u ? i() : e(t), u = !u;
        }) : "mouseover" === o.on && (n.init(), r.on("mouseenter.zoom", e).on("mouseleave.zoom", i).on(d, function() {
          o.longtouchtimer && clearTimeout(o.longtouchtimer), n.move();
        })), o.touch && (h = !1, r.on("touchstart.zoom", function(t) {
          clearTimeout(o.longtouchtimer), o.longtouchtimer = setTimeout(function() {
            h ? (h = !1, i()) : (h = !0, t.preventDefault(), e(t.originalEvent.touches[0] || t.originalEvent.changedTouches[0]));
          }, o.touchduration);
        }).on("touchmove.zoom", function(t) {
          h && (t.preventDefault(), n.move(t.originalEvent.touches[0] || t.originalEvent.changedTouches[0]));
        }).on("touchend.zoom", function(t) {
          t.preventDefault(), o.longtouchtimer && clearTimeout(o.longtouchtimer), i(), h = !1;
        })), t.isFunction(o.callback) && o.callback.call(l);
      }, l.src = o.url);
    });
  }, t.fn.zoom.defaults = e;
}(window.jQuery), "function" != typeof Object.create && (Object.create = function(t) {
  function e() {}
  return e.prototype = t, new e();
}), function(t, e, i) {
  var n = {
    init: function(e, i) {
      var n = this;
      n.$elem = t(i), n.options = t.extend({}, t.fn.owlCarousel.options, n.$elem.data(), e), 
      n.userOptions = e, n.loadContent();
    },
    loadContent: function() {
      var e, i = this;
      "function" == typeof i.options.beforeInit && i.options.beforeInit.apply(this, [ i.$elem ]), 
      "string" == typeof i.options.jsonPath ? (e = i.options.jsonPath, t.getJSON(e, function(t) {
        var e, n = "";
        if ("function" == typeof i.options.jsonSuccess) i.options.jsonSuccess.apply(this, [ t ]); else {
          for (e in t.owl) t.owl.hasOwnProperty(e) && (n += t.owl[e].item);
          i.$elem.html(n);
        }
        i.logIn();
      })) : i.logIn();
    },
    logIn: function() {
      var t = this;
      t.$elem.data({
        "owl-originalStyles": t.$elem.attr("style"),
        "owl-originalClasses": t.$elem.attr("class")
      }), t.$elem.css({
        opacity: 0
      }), t.orignalItems = t.options.items, t.checkBrowser(), t.wrapperWidth = 0, t.checkVisible = null, 
      t.setVars();
    },
    setVars: function() {
      var t = this;
      if (0 === t.$elem.children().length) return !1;
      t.baseClass(), t.eventTypes(), t.$userItems = t.$elem.children(), t.itemsAmount = t.$userItems.length, 
      t.wrapItems(), t.$owlItems = t.$elem.find(".owl-item"), t.$owlWrapper = t.$elem.find(".owl-wrapper"), 
      t.playDirection = "next", t.prevItem = 0, t.prevArr = [ 0 ], t.currentItem = 0, 
      t.customEvents(), t.onStartup();
    },
    onStartup: function() {
      var t = this;
      t.updateItems(), t.calculateAll(), t.buildControls(), t.updateControls(), t.response(), 
      t.moveEvents(), t.stopOnHover(), t.owlStatus(), !1 !== t.options.transitionStyle && t.transitionTypes(t.options.transitionStyle), 
      !0 === t.options.autoPlay && (t.options.autoPlay = 5e3), t.play(), t.$elem.find(".owl-wrapper").css("display", "block"), 
      t.$elem.is(":visible") ? t.$elem.css("opacity", 1) : t.watchVisibility(), t.onstartup = !1, 
      t.eachMoveUpdate(), "function" == typeof t.options.afterInit && t.options.afterInit.apply(this, [ t.$elem ]);
    },
    eachMoveUpdate: function() {
      var t = this;
      !0 === t.options.lazyLoad && t.lazyLoad(), !0 === t.options.autoHeight && t.autoHeight(), 
      t.onVisibleItems(), "function" == typeof t.options.afterAction && t.options.afterAction.apply(this, [ t.$elem ]);
    },
    updateVars: function() {
      var t = this;
      "function" == typeof t.options.beforeUpdate && t.options.beforeUpdate.apply(this, [ t.$elem ]), 
      t.watchVisibility(), t.updateItems(), t.calculateAll(), t.updatePosition(), t.updateControls(), 
      t.eachMoveUpdate(), "function" == typeof t.options.afterUpdate && t.options.afterUpdate.apply(this, [ t.$elem ]);
    },
    reload: function() {
      var t = this;
      e.setTimeout(function() {
        t.updateVars();
      }, 0);
    },
    watchVisibility: function() {
      var t = this;
      if (!1 !== t.$elem.is(":visible")) return !1;
      t.$elem.css({
        opacity: 0
      }), e.clearInterval(t.autoPlayInterval), e.clearInterval(t.checkVisible), t.checkVisible = e.setInterval(function() {
        t.$elem.is(":visible") && (t.reload(), t.$elem.animate({
          opacity: 1
        }, 200), e.clearInterval(t.checkVisible));
      }, 500);
    },
    wrapItems: function() {
      var t = this;
      t.$userItems.wrapAll('<div class="owl-wrapper">').wrap('<div class="owl-item"></div>'), 
      t.$elem.find(".owl-wrapper").wrap('<div class="owl-wrapper-outer">'), t.wrapperOuter = t.$elem.find(".owl-wrapper-outer"), 
      t.$elem.css("display", "block");
    },
    baseClass: function() {
      var t = this, e = t.$elem.hasClass(t.options.baseClass), i = t.$elem.hasClass(t.options.theme);
      e || t.$elem.addClass(t.options.baseClass), i || t.$elem.addClass(t.options.theme);
    },
    updateItems: function() {
      var e, i, n = this;
      if (!1 === n.options.responsive) return !1;
      if (!0 === n.options.singleItem) return n.options.items = n.orignalItems = 1, n.options.itemsCustom = !1, 
      n.options.itemsDesktop = !1, n.options.itemsDesktopSmall = !1, n.options.itemsTablet = !1, 
      n.options.itemsTabletSmall = !1, n.options.itemsMobile = !1, !1;
      if ((e = t(n.options.responsiveBaseWidth).width()) > (n.options.itemsDesktop[0] || n.orignalItems) && (n.options.items = n.orignalItems), 
      !1 !== n.options.itemsCustom) for (n.options.itemsCustom.sort(function(t, e) {
        return t[0] - e[0];
      }), i = 0; i < n.options.itemsCustom.length; i += 1) n.options.itemsCustom[i][0] <= e && (n.options.items = n.options.itemsCustom[i][1]); else e <= n.options.itemsDesktop[0] && !1 !== n.options.itemsDesktop && (n.options.items = n.options.itemsDesktop[1]), 
      e <= n.options.itemsDesktopSmall[0] && !1 !== n.options.itemsDesktopSmall && (n.options.items = n.options.itemsDesktopSmall[1]), 
      e <= n.options.itemsTablet[0] && !1 !== n.options.itemsTablet && (n.options.items = n.options.itemsTablet[1]), 
      e <= n.options.itemsTabletSmall[0] && !1 !== n.options.itemsTabletSmall && (n.options.items = n.options.itemsTabletSmall[1]), 
      e <= n.options.itemsMobile[0] && !1 !== n.options.itemsMobile && (n.options.items = n.options.itemsMobile[1]);
      n.options.items > n.itemsAmount && !0 === n.options.itemsScaleUp && (n.options.items = n.itemsAmount);
    },
    response: function() {
      var i, n, o = this;
      if (!0 !== o.options.responsive) return !1;
      n = t(e).width(), o.resizer = function() {
        t(e).width() !== n && (!1 !== o.options.autoPlay && e.clearInterval(o.autoPlayInterval), 
        e.clearTimeout(i), i = e.setTimeout(function() {
          n = t(e).width(), o.updateVars();
        }, o.options.responsiveRefreshRate));
      }, t(e).resize(o.resizer);
    },
    updatePosition: function() {
      var t = this;
      t.jumpTo(t.currentItem), !1 !== t.options.autoPlay && t.checkAp();
    },
    appendItemsSizes: function() {
      var e = this, i = 0, n = e.itemsAmount - e.options.items;
      e.$owlItems.each(function(o) {
        var s = t(this);
        s.css({
          width: e.itemWidth
        }).data("owl-item", Number(o)), o % e.options.items != 0 && o !== n || o > n || (i += 1), 
        s.data("owl-roundPages", i);
      });
    },
    appendWrapperSizes: function() {
      var t = this, e = t.$owlItems.length * t.itemWidth;
      t.$owlWrapper.css({
        width: 2 * e,
        left: 0
      }), t.appendItemsSizes();
    },
    calculateAll: function() {
      var t = this;
      t.calculateWidth(), t.appendWrapperSizes(), t.loops(), t.max();
    },
    calculateWidth: function() {
      var t = this;
      t.itemWidth = Math.round(t.$elem.width() / t.options.items);
    },
    max: function() {
      var t = this, e = -1 * (t.itemsAmount * t.itemWidth - t.options.items * t.itemWidth);
      return t.options.items > t.itemsAmount ? (t.maximumItem = 0, e = 0, t.maximumPixels = 0) : (t.maximumItem = t.itemsAmount - t.options.items, 
      t.maximumPixels = e), e;
    },
    min: function() {
      return 0;
    },
    loops: function() {
      var e, i, n = this, o = 0, s = 0;
      for (n.positionsInArray = [ 0 ], n.pagesInArray = [], e = 0; e < n.itemsAmount; e += 1) s += n.itemWidth, 
      n.positionsInArray.push(-s), !0 === n.options.scrollPerPage && (i = t(n.$owlItems[e]).data("owl-roundPages")) !== o && (n.pagesInArray[o] = n.positionsInArray[e], 
      o = i);
    },
    buildControls: function() {
      var e = this;
      !0 !== e.options.navigation && !0 !== e.options.pagination || (e.owlControls = t('<div class="owl-controls"/>').toggleClass("clickable", !e.browser.isTouch).appendTo(e.$elem)), 
      !0 === e.options.pagination && e.buildPagination(), !0 === e.options.navigation && e.buildButtons();
    },
    buildButtons: function() {
      var e = this, i = t('<div class="owl-buttons"/>');
      e.owlControls.append(i), e.buttonPrev = t("<div/>", {
        class: "owl-prev",
        html: e.options.navigationText[0] || ""
      }), e.buttonNext = t("<div/>", {
        class: "owl-next",
        html: e.options.navigationText[1] || ""
      }), i.append(e.buttonPrev).append(e.buttonNext), i.on("touchstart.owlControls mousedown.owlControls", 'div[class^="owl"]', function(t) {
        t.preventDefault();
      }), i.on("touchend.owlControls mouseup.owlControls", 'div[class^="owl"]', function(i) {
        i.preventDefault(), t(this).hasClass("owl-next") ? e.next() : e.prev();
      });
    },
    buildPagination: function() {
      var e = this;
      e.paginationWrapper = t('<div class="owl-pagination"/>'), e.owlControls.append(e.paginationWrapper), 
      e.paginationWrapper.on("touchend.owlControls mouseup.owlControls", ".owl-page", function(i) {
        i.preventDefault(), Number(t(this).data("owl-page")) !== e.currentItem && e.goTo(Number(t(this).data("owl-page")), !0);
      });
    },
    updatePagination: function() {
      var e, i, n, o, s, a, r = this;
      if (!1 === r.options.pagination) return !1;
      for (r.paginationWrapper.html(""), e = 0, i = r.itemsAmount - r.itemsAmount % r.options.items, 
      o = 0; o < r.itemsAmount; o += 1) o % r.options.items == 0 && (e += 1, i === o && (n = r.itemsAmount - r.options.items), 
      s = t("<div/>", {
        class: "owl-page"
      }), a = t("<span></span>", {
        text: !0 === r.options.paginationNumbers ? e : "",
        class: !0 === r.options.paginationNumbers ? "owl-numbers" : ""
      }), s.append(a), s.data("owl-page", i === o ? n : o), s.data("owl-roundPages", e), 
      r.paginationWrapper.append(s));
      r.checkPagination();
    },
    checkPagination: function() {
      var e = this;
      if (!1 === e.options.pagination) return !1;
      e.paginationWrapper.find(".owl-page").each(function() {
        t(this).data("owl-roundPages") === t(e.$owlItems[e.currentItem]).data("owl-roundPages") && (e.paginationWrapper.find(".owl-page").removeClass("active"), 
        t(this).addClass("active"));
      });
    },
    checkNavigation: function() {
      var t = this;
      if (!1 === t.options.navigation) return !1;
      !1 === t.options.rewindNav && (0 === t.currentItem && 0 === t.maximumItem ? (t.buttonPrev.addClass("disabled"), 
      t.buttonNext.addClass("disabled")) : 0 === t.currentItem && 0 !== t.maximumItem ? (t.buttonPrev.addClass("disabled"), 
      t.buttonNext.removeClass("disabled")) : t.currentItem === t.maximumItem ? (t.buttonPrev.removeClass("disabled"), 
      t.buttonNext.addClass("disabled")) : 0 !== t.currentItem && t.currentItem !== t.maximumItem && (t.buttonPrev.removeClass("disabled"), 
      t.buttonNext.removeClass("disabled")));
    },
    updateControls: function() {
      var t = this;
      t.updatePagination(), t.checkNavigation(), t.owlControls && (t.options.items >= t.itemsAmount ? t.owlControls.hide() : t.owlControls.show());
    },
    destroyControls: function() {
      var t = this;
      t.owlControls && t.owlControls.remove();
    },
    next: function(t) {
      var e = this;
      if (e.isTransition) return !1;
      if (e.currentItem += !0 === e.options.scrollPerPage ? e.options.items : 1, e.currentItem > e.maximumItem + (!0 === e.options.scrollPerPage ? e.options.items - 1 : 0)) {
        if (!0 !== e.options.rewindNav) return e.currentItem = e.maximumItem, !1;
        e.currentItem = 0, t = "rewind";
      }
      e.goTo(e.currentItem, t);
    },
    prev: function(t) {
      var e = this;
      if (e.isTransition) return !1;
      if (!0 === e.options.scrollPerPage && e.currentItem > 0 && e.currentItem < e.options.items ? e.currentItem = 0 : e.currentItem -= !0 === e.options.scrollPerPage ? e.options.items : 1, 
      e.currentItem < 0) {
        if (!0 !== e.options.rewindNav) return e.currentItem = 0, !1;
        e.currentItem = e.maximumItem, t = "rewind";
      }
      e.goTo(e.currentItem, t);
    },
    goTo: function(t, i, n) {
      var o, s = this;
      return !s.isTransition && ("function" == typeof s.options.beforeMove && s.options.beforeMove.apply(this, [ s.$elem ]), 
      t >= s.maximumItem ? t = s.maximumItem : t <= 0 && (t = 0), s.currentItem = s.owl.currentItem = t, 
      !1 !== s.options.transitionStyle && "drag" !== n && 1 === s.options.items && !0 === s.browser.support3d ? (s.swapSpeed(0), 
      !0 === s.browser.support3d ? s.transition3d(s.positionsInArray[t]) : s.css2slide(s.positionsInArray[t], 1), 
      s.afterGo(), s.singleItemTransition(), !1) : (o = s.positionsInArray[t], !0 === s.browser.support3d ? (s.isCss3Finish = !1, 
      !0 === i ? (s.swapSpeed("paginationSpeed"), e.setTimeout(function() {
        s.isCss3Finish = !0;
      }, s.options.paginationSpeed)) : "rewind" === i ? (s.swapSpeed(s.options.rewindSpeed), 
      e.setTimeout(function() {
        s.isCss3Finish = !0;
      }, s.options.rewindSpeed)) : (s.swapSpeed("slideSpeed"), e.setTimeout(function() {
        s.isCss3Finish = !0;
      }, s.options.slideSpeed)), s.transition3d(o)) : !0 === i ? s.css2slide(o, s.options.paginationSpeed) : "rewind" === i ? s.css2slide(o, s.options.rewindSpeed) : s.css2slide(o, s.options.slideSpeed), 
      void s.afterGo()));
    },
    jumpTo: function(t) {
      var e = this;
      "function" == typeof e.options.beforeMove && e.options.beforeMove.apply(this, [ e.$elem ]), 
      t >= e.maximumItem || -1 === t ? t = e.maximumItem : t <= 0 && (t = 0), e.swapSpeed(0), 
      !0 === e.browser.support3d ? e.transition3d(e.positionsInArray[t]) : e.css2slide(e.positionsInArray[t], 1), 
      e.currentItem = e.owl.currentItem = t, e.afterGo();
    },
    afterGo: function() {
      var t = this;
      t.prevArr.push(t.currentItem), t.prevItem = t.owl.prevItem = t.prevArr[t.prevArr.length - 2], 
      t.prevArr.shift(0), t.prevItem !== t.currentItem && (t.checkPagination(), t.checkNavigation(), 
      t.eachMoveUpdate(), !1 !== t.options.autoPlay && t.checkAp()), "function" == typeof t.options.afterMove && t.prevItem !== t.currentItem && t.options.afterMove.apply(this, [ t.$elem ]);
    },
    stop: function() {
      var t = this;
      t.apStatus = "stop", e.clearInterval(t.autoPlayInterval);
    },
    dragging: function(t) {
      this.options.dragging = t;
    },
    checkAp: function() {
      var t = this;
      "stop" !== t.apStatus && t.play();
    },
    play: function() {
      var t = this;
      if (t.apStatus = "play", !1 === t.options.autoPlay) return !1;
      e.clearInterval(t.autoPlayInterval), t.autoPlayInterval = e.setInterval(function() {
        t.next(!0);
      }, t.options.autoPlay);
    },
    swapSpeed: function(t) {
      var e = this;
      "slideSpeed" === t ? e.$owlWrapper.css(e.addCssSpeed(e.options.slideSpeed)) : "paginationSpeed" === t ? e.$owlWrapper.css(e.addCssSpeed(e.options.paginationSpeed)) : "string" != typeof t && e.$owlWrapper.css(e.addCssSpeed(t));
    },
    addCssSpeed: function(t) {
      return {
        "-webkit-transition": "all " + t + "ms ease",
        "-moz-transition": "all " + t + "ms ease",
        "-o-transition": "all " + t + "ms ease",
        transition: "all " + t + "ms ease"
      };
    },
    removeTransition: function() {
      return {
        "-webkit-transition": "",
        "-moz-transition": "",
        "-o-transition": "",
        transition: ""
      };
    },
    doTranslate: function(t) {
      return {
        "-webkit-transform": "translate3d(" + t + "px, 0px, 0px)",
        "-moz-transform": "translate3d(" + t + "px, 0px, 0px)",
        "-o-transform": "translate3d(" + t + "px, 0px, 0px)",
        "-ms-transform": "translate3d(" + t + "px, 0px, 0px)",
        transform: "translate3d(" + t + "px, 0px,0px)"
      };
    },
    transition3d: function(t) {
      var e = this;
      e.$owlWrapper.css(e.doTranslate(t));
    },
    css2move: function(t) {
      this.$owlWrapper.css({
        left: t
      });
    },
    css2slide: function(t, e) {
      var i = this;
      i.isCssFinish = !1, i.$owlWrapper.stop(!0, !0).animate({
        left: t
      }, {
        duration: e || i.options.slideSpeed,
        complete: function() {
          i.isCssFinish = !0;
        }
      });
    },
    checkBrowser: function() {
      var t, n, o, s, a = this, r = "translate3d(0px, 0px, 0px)", l = i.createElement("div");
      l.style.cssText = "  -moz-transform:" + r + "; -ms-transform:" + r + "; -o-transform:" + r + "; -webkit-transform:" + r + "; transform:" + r, 
      t = /translate3d\(0px, 0px, 0px\)/g, o = null !== (n = l.style.cssText.match(t)) && 1 === n.length, 
      s = "ontouchstart" in e || e.navigator.msMaxTouchPoints, a.browser = {
        support3d: o,
        isTouch: s
      };
    },
    moveEvents: function() {
      var t = this;
      !1 === t.options.mouseDrag && !1 === t.options.touchDrag || (t.gestures(), t.disabledEvents());
    },
    eventTypes: function() {
      var t = this, e = [ "s", "e", "x" ];
      t.ev_types = {}, !0 === t.options.mouseDrag && !0 === t.options.touchDrag ? e = [ "touchstart.owl mousedown.owl", "touchmove.owl mousemove.owl", "touchend.owl touchcancel.owl mouseup.owl" ] : !1 === t.options.mouseDrag && !0 === t.options.touchDrag ? e = [ "touchstart.owl", "touchmove.owl", "touchend.owl touchcancel.owl" ] : !0 === t.options.mouseDrag && !1 === t.options.touchDrag && (e = [ "mousedown.owl", "mousemove.owl", "mouseup.owl" ]), 
      t.ev_types.start = e[0], t.ev_types.move = e[1], t.ev_types.end = e[2];
    },
    disabledEvents: function() {
      var e = this;
      e.$elem.on("dragstart.owl", function(t) {
        t.preventDefault();
      }), e.$elem.on("mousedown.disableTextSelect", function(e) {
        return t(e.target).is("input, textarea, select, option");
      });
    },
    gestures: function() {
      function n(t) {
        if (void 0 !== t.touches) return {
          x: t.touches[0].pageX,
          y: t.touches[0].pageY
        };
        if (void 0 === t.touches) {
          if (void 0 !== t.pageX) return {
            x: t.pageX,
            y: t.pageY
          };
          if (void 0 === t.pageX) return {
            x: t.clientX,
            y: t.clientY
          };
        }
      }
      function o(e) {
        "on" === e ? (t(i).on(r.ev_types.move, s), t(i).on(r.ev_types.end, a)) : "off" === e && (t(i).off(r.ev_types.move), 
        t(i).off(r.ev_types.end));
      }
      function s(o) {
        if (r.options.dragging) {
          var s, a, c = o.originalEvent || o || e.event;
          r.newPosX = n(c).x - l.offsetX, r.newPosY = n(c).y - l.offsetY, r.newRelativeX = r.newPosX - l.relativePos, 
          "function" == typeof r.options.startDragging && !0 !== l.dragging && 0 !== r.newRelativeX && (l.dragging = !0, 
          r.options.startDragging.apply(r, [ r.$elem ])), (r.newRelativeX > 8 || r.newRelativeX < -8) && !0 === r.browser.isTouch && (void 0 !== c.preventDefault ? c.preventDefault() : c.returnValue = !1, 
          l.sliding = !0), (r.newPosY > 10 || r.newPosY < -10) && !1 === l.sliding && t(i).off("touchmove.owl"), 
          s = function() {
            return r.newRelativeX / 5;
          }, a = function() {
            return r.maximumPixels + r.newRelativeX / 5;
          }, r.newPosX = Math.max(Math.min(r.newPosX, s()), a()), !0 === r.browser.support3d ? r.transition3d(r.newPosX) : r.css2move(r.newPosX);
        }
      }
      function a(i) {
        var n, s, a, c = i.originalEvent || i || e.event;
        c.target = c.target || c.srcElement, l.dragging = !1, !0 !== r.browser.isTouch && r.$owlWrapper.removeClass("grabbing"), 
        r.newRelativeX < 50 ? r.dragDirection = r.owl.dragDirection = "left" : r.dragDirection = r.owl.dragDirection = "right", 
        0 !== r.newRelativeX && (n = r.getNewPosition(), r.goTo(n, !1, "drag"), l.targetElement === c.target && !0 !== r.browser.isTouch && (t(c.target).on("click.disable", function(e) {
          e.stopImmediatePropagation(), e.stopPropagation(), e.preventDefault(), t(e.target).off("click.disable");
        }), a = (s = t._data(c.target, "events").click).pop(), s.splice(0, 0, a))), o("off");
      }
      var r = this, l = {
        offsetX: 0,
        offsetY: 0,
        baseElWidth: 0,
        relativePos: 0,
        position: null,
        minSwipe: null,
        maxSwipe: null,
        sliding: null,
        dargging: null,
        targetElement: null
      };
      r.isCssFinish = !0, r.$elem.on(r.ev_types.start, ".owl-wrapper", function(i) {
        var s, a = i.originalEvent || i || e.event;
        if (r.options.dragging) {
          if (3 === a.which) return !1;
          if (!(r.itemsAmount <= r.options.items)) {
            if (!1 === r.isCssFinish && !r.options.dragBeforeAnimFinish) return !1;
            if (!1 === r.isCss3Finish && !r.options.dragBeforeAnimFinish) return !1;
            !1 !== r.options.autoPlay && e.clearInterval(r.autoPlayInterval), !0 === r.browser.isTouch || r.$owlWrapper.hasClass("grabbing") || r.$owlWrapper.addClass("grabbing"), 
            r.newPosX = 0, r.newRelativeX = 0, t(this).css(r.removeTransition()), s = t(this).position(), 
            l.relativePos = s.left, l.offsetX = n(a).x - s.left, l.offsetY = n(a).y - s.top, 
            o("on"), l.sliding = !1, l.targetElement = a.target || a.srcElement;
          }
        }
      });
    },
    getNewPosition: function() {
      var t = this, e = t.closestItem();
      return e > t.maximumItem ? (t.currentItem = t.maximumItem, e = t.maximumItem) : t.newPosX >= 0 && (e = 0, 
      t.currentItem = 0), e;
    },
    closestItem: function() {
      var e = this, i = !0 === e.options.scrollPerPage ? e.pagesInArray : e.positionsInArray, n = e.newPosX, o = null;
      return t.each(i, function(s, a) {
        n - e.itemWidth / 20 > i[s + 1] && n - e.itemWidth / 20 < a && "left" === e.moveDirection() ? (o = a, 
        !0 === e.options.scrollPerPage ? e.currentItem = t.inArray(o, e.positionsInArray) : e.currentItem = s) : n + e.itemWidth / 20 < a && n + e.itemWidth / 20 > (i[s + 1] || i[s] - e.itemWidth) && "right" === e.moveDirection() && (!0 === e.options.scrollPerPage ? (o = i[s + 1] || i[i.length - 1], 
        e.currentItem = t.inArray(o, e.positionsInArray)) : (o = i[s + 1], e.currentItem = s + 1));
      }), e.currentItem;
    },
    moveDirection: function() {
      var t, e = this;
      return e.newRelativeX < 0 ? (t = "right", e.playDirection = "next") : (t = "left", 
      e.playDirection = "prev"), t;
    },
    customEvents: function() {
      var t = this;
      t.$elem.on("owl.next", function() {
        t.next();
      }), t.$elem.on("owl.prev", function() {
        t.prev();
      }), t.$elem.on("owl.play", function(e, i) {
        t.options.autoPlay = i, t.play(), t.hoverStatus = "play";
      }), t.$elem.on("owl.stop", function() {
        t.stop(), t.hoverStatus = "stop";
      }), t.$elem.on("owl.goTo", function(e, i) {
        t.goTo(i);
      }), t.$elem.on("owl.jumpTo", function(e, i) {
        t.jumpTo(i);
      }), t.$elem.on("owl.dragging", function(e, i) {
        t.dragging(i);
      });
    },
    stopOnHover: function() {
      var t = this;
      !0 === t.options.stopOnHover && !0 !== t.browser.isTouch && !1 !== t.options.autoPlay && (t.$elem.on("mouseover", function() {
        t.stop();
      }), t.$elem.on("mouseout", function() {
        "stop" !== t.hoverStatus && t.play();
      }));
    },
    lazyLoad: function() {
      var e, i, n, o, s = this;
      if (!1 === s.options.lazyLoad) return !1;
      for (e = 0; e < s.itemsAmount; e += 1) "loaded" !== (i = t(s.$owlItems[e])).data("owl-loaded") && (n = i.data("owl-item"), 
      "string" == typeof (o = i.find(".lazyOwl")).data("src") ? (void 0 === i.data("owl-loaded") && (o.hide(), 
      i.addClass("loading").data("owl-loaded", "checked")), (!0 !== s.options.lazyFollow || n >= s.currentItem) && n < s.currentItem + s.options.items && o.length && o.each(function() {
        s.lazyPreload(i, t(this));
      })) : i.data("owl-loaded", "loaded"));
    },
    lazyPreload: function(t, i) {
      function n() {
        t.data("owl-loaded", "loaded").removeClass("loading"), i.removeAttr("data-src"), 
        "fade" === a.options.lazyEffect ? i.fadeIn(400) : i.show(), "function" == typeof a.options.afterLazyLoad && a.options.afterLazyLoad.apply(this, [ a.$elem ]);
      }
      function o() {
        r += 1, a.completeImg(i.get(0)) || !0 === s ? n() : r <= 100 ? e.setTimeout(o, 100) : n();
      }
      var s, a = this, r = 0;
      "DIV" === i.prop("tagName") ? (i.css("background-image", "url(" + i.data("src") + ")"), 
      s = !0) : i[0].src = i.data("src"), o();
    },
    autoHeight: function() {
      function i() {
        var i = t(s.$owlItems[s.currentItem]).height();
        s.wrapperOuter.css("height", i + "px"), s.wrapperOuter.hasClass("autoHeight") || e.setTimeout(function() {
          s.wrapperOuter.addClass("autoHeight");
        }, 0);
      }
      function n() {
        o += 1, s.completeImg(a.get(0)) ? i() : o <= 100 ? e.setTimeout(n, 100) : s.wrapperOuter.css("height", "");
      }
      var o, s = this, a = t(s.$owlItems[s.currentItem]).find("img");
      void 0 !== a.get(0) ? (o = 0, n()) : i();
    },
    completeImg: function(t) {
      return !!t.complete && ("undefined" == typeof t.naturalWidth || 0 !== t.naturalWidth);
    },
    onVisibleItems: function() {
      var e, i = this;
      for (!0 === i.options.addClassActive && i.$owlItems.removeClass("active"), i.visibleItems = [], 
      e = i.currentItem; e < i.currentItem + i.options.items; e += 1) i.visibleItems.push(e), 
      !0 === i.options.addClassActive && t(i.$owlItems[e]).addClass("active");
      i.owl.visibleItems = i.visibleItems;
    },
    transitionTypes: function(t) {
      var e = this;
      e.outClass = "owl-" + t + "-out", e.inClass = "owl-" + t + "-in";
    },
    singleItemTransition: function() {
      var t = this, e = t.outClass, i = t.inClass, n = t.$owlItems.eq(t.currentItem), o = t.$owlItems.eq(t.prevItem), s = Math.abs(t.positionsInArray[t.currentItem]) + t.positionsInArray[t.prevItem], a = Math.abs(t.positionsInArray[t.currentItem]) + t.itemWidth / 2, r = "webkitAnimationEnd oAnimationEnd MSAnimationEnd animationend";
      t.isTransition = !0, t.$owlWrapper.addClass("owl-origin").css({
        "-webkit-transform-origin": a + "px",
        "-moz-perspective-origin": a + "px",
        "perspective-origin": a + "px"
      }), o.css(function(t) {
        return {
          position: "relative",
          left: t + "px"
        };
      }(s)).addClass(e).on(r, function() {
        t.endPrev = !0, o.off(r), t.clearTransStyle(o, e);
      }), n.addClass(i).on(r, function() {
        t.endCurrent = !0, n.off(r), t.clearTransStyle(n, i);
      });
    },
    clearTransStyle: function(t, e) {
      var i = this;
      t.css({
        position: "",
        left: ""
      }).removeClass(e), i.endPrev && i.endCurrent && (i.$owlWrapper.removeClass("owl-origin"), 
      i.endPrev = !1, i.endCurrent = !1, i.isTransition = !1);
    },
    owlStatus: function() {
      var t = this;
      t.owl = {
        userOptions: t.userOptions,
        baseElement: t.$elem,
        userItems: t.$userItems,
        owlItems: t.$owlItems,
        currentItem: t.currentItem,
        prevItem: t.prevItem,
        visibleItems: t.visibleItems,
        isTouch: t.browser.isTouch,
        browser: t.browser,
        dragDirection: t.dragDirection
      };
    },
    clearEvents: function() {
      var n = this;
      n.$elem.off(".owl owl mousedown.disableTextSelect"), t(i).off(".owl owl"), t(e).off("resize", n.resizer);
    },
    unWrap: function() {
      var t = this;
      0 !== t.$elem.children().length && (t.$owlWrapper.unwrap(), t.$userItems.unwrap().unwrap(), 
      t.owlControls && t.owlControls.remove()), t.clearEvents(), t.$elem.attr({
        style: t.$elem.data("owl-originalStyles") || "",
        class: t.$elem.data("owl-originalClasses")
      });
    },
    destroy: function() {
      var t = this;
      t.stop(), e.clearInterval(t.checkVisible), t.unWrap(), t.$elem.removeData();
    },
    reinit: function(e) {
      var i = this, n = t.extend({}, i.userOptions, e);
      i.unWrap(), i.init(n, i.$elem);
    },
    addItem: function(t, e) {
      var i, n = this;
      return !!t && (0 === n.$elem.children().length ? (n.$elem.append(t), n.setVars(), 
      !1) : (n.unWrap(), (i = void 0 === e || -1 === e ? -1 : e) >= n.$userItems.length || -1 === i ? n.$userItems.eq(-1).after(t) : n.$userItems.eq(i).before(t), 
      void n.setVars()));
    },
    removeItem: function(t) {
      var e, i = this;
      if (0 === i.$elem.children().length) return !1;
      e = void 0 === t || -1 === t ? -1 : t, i.unWrap(), i.$userItems.eq(e).remove(), 
      i.setVars();
    }
  };
  t.fn.owlCarousel = function(e) {
    return this.each(function() {
      if (!0 === t(this).data("owl-init")) return !1;
      t(this).data("owl-init", !0);
      var i = Object.create(n);
      i.init(e, this), t.data(this, "owlCarousel", i);
    });
  }, t.fn.owlCarousel.options = {
    items: 5,
    itemsCustom: !1,
    itemsDesktop: [ 1199, 4 ],
    itemsDesktopSmall: [ 979, 3 ],
    itemsTablet: [ 768, 2 ],
    itemsTabletSmall: !1,
    itemsMobile: [ 479, 1 ],
    singleItem: !1,
    itemsScaleUp: !1,
    slideSpeed: 200,
    paginationSpeed: 800,
    rewindSpeed: 1e3,
    autoPlay: !1,
    stopOnHover: !1,
    navigation: !1,
    navigationText: [ "prev", "next" ],
    rewindNav: !0,
    scrollPerPage: !1,
    pagination: !0,
    paginationNumbers: !1,
    responsive: !0,
    responsiveRefreshRate: 200,
    responsiveBaseWidth: e,
    baseClass: "owl-carousel",
    theme: "owl-theme",
    lazyLoad: !1,
    lazyFollow: !0,
    lazyEffect: "fade",
    autoHeight: !1,
    jsonPath: !1,
    jsonSuccess: !1,
    dragBeforeAnimFinish: !0,
    mouseDrag: !0,
    touchDrag: !0,
    dragging: !0,
    addClassActive: !1,
    transitionStyle: !1,
    beforeUpdate: !1,
    afterUpdate: !1,
    beforeInit: !1,
    afterInit: !1,
    beforeMove: !1,
    afterMove: !1,
    afterAction: !1,
    startDragging: !1,
    afterLazyLoad: !1
  };
}(jQuery, window, document), ACC.address = {
  _autoload: [ "bindToChangeAddressButton", "bindCreateUpdateAddressForm", "bindSuggestedDeliveryAddresses", "bindCountrySpecificAddressForms", "showAddressFormButtonPanel", "bindViewAddressBook", "bindToColorboxClose", "showRemoveAddressFromBookConfirmation", "backToListAddresses" ],
  spinner: $("<img src='" + ACC.config.commonResourcePath + "/images/spinner.gif' />"),
  addressID: "",
  handleChangeAddressButtonClick: function() {
    return ACC.address.addressID = $(this).data("address") ? $(this).data("address") : "", 
    $("#summaryDeliveryAddressFormContainer").show(), $("#summaryOverlayViewAddressBook").show(), 
    $("#summaryDeliveryAddressBook").hide(), $.getJSON(getDeliveryAddressesUrl, ACC.address.handleAddressDataLoad), 
    !1;
  },
  handleAddressDataLoad: function(t) {
    ACC.address.setupDeliveryAddressPopupForm(t), ACC.colorbox.open("", {
      inline: !0,
      href: "#summaryDeliveryAddressOverlay",
      overlayClose: !1,
      onOpen: function() {
        ACC.address.emptyAddressForm(), $(document).on("change", "#saveAddress", function() {
          var t = $(this).prop("checked");
          $("#defaultAddress").prop("disabled", !t), t || $("#defaultAddress").prop("checked", !1);
        });
      }
    });
  },
  setupDeliveryAddressPopupForm: function(t) {
    $("#summaryDeliveryAddressBook").html($("#deliveryAddressesTemplate").tmpl({
      addresses: t
    })), $("#summaryDeliveryAddressBook button.use_address").click(ACC.address.handleSelectExistingAddressClick), 
    $("#summaryDeliveryAddressBook button.edit").click(ACC.address.handleEditAddressClick), 
    $("#summaryDeliveryAddressBook button.default").click(ACC.address.handleDefaultAddressClick);
  },
  emptyAddressForm: function() {
    var t = {
      url: getDeliveryAddressFormUrl,
      data: {
        addressId: ACC.address.addressID,
        createUpdateStatus: ""
      },
      type: "GET",
      success: function(t) {
        $("#summaryDeliveryAddressFormContainer").html(t), ACC.address.bindCreateUpdateAddressForm();
      }
    };
    $.ajax(t);
  },
  handleSelectExistingAddressClick: function() {
    var t = $(this).attr("data-address");
    return $.postJSON(setDeliveryAddressUrl, {
      addressId: t
    }, ACC.address.handleSelectExitingAddressSuccess), !1;
  },
  handleEditAddressClick: function() {
    $("#summaryDeliveryAddressFormContainer").show(), $("#summaryOverlayViewAddressBook").show(), 
    $("#summaryDeliveryAddressBook").hide();
    var t = $(this).attr("data-address"), e = {
      url: getDeliveryAddressFormUrl,
      data: {
        addressId: t,
        createUpdateStatus: ""
      },
      target: "#summaryDeliveryAddressFormContainer",
      type: "GET",
      success: function(t) {
        ACC.address.bindCreateUpdateAddressForm(), ACC.colorbox.resize();
      },
      error: function(t, e, i) {
        alert("Failed to update cart. Error details [" + t + ", " + e + ", " + i + "]");
      }
    };
    return $(this).ajaxSubmit(e), !1;
  },
  handleDefaultAddressClick: function() {
    var t = $(this).attr("data-address"), e = {
      url: setDefaultAddressUrl,
      data: {
        addressId: t
      },
      type: "GET",
      success: function(t) {
        ACC.address.setupDeliveryAddressPopupForm(t);
      },
      error: function(t, e, i) {
        alert("Failed to update address book. Error details [" + t + ", " + e + ", " + i + "]");
      }
    };
    return $(this).ajaxSubmit(e), !1;
  },
  handleSelectExitingAddressSuccess: function(t) {
    null != t ? (ACC.refresh.refreshPage(t), ACC.colorbox.close()) : alert("Failed to set delivery address");
  },
  bindCreateUpdateAddressForm: function() {
    $(".create_update_address_form").each(function() {
      var t = {
        type: "POST",
        beforeSubmit: function() {
          $("#checkout_delivery_address").block({
            message: ACC.address.spinner
          });
        },
        success: function(t) {
          $("#summaryDeliveryAddressFormContainer").html(t);
          var e = $(".create_update_address_id").attr("status");
          null != e && "success" === e.toLowerCase() ? (ACC.refresh.getCheckoutCartDataAndRefreshPage(), 
          ACC.colorbox.close()) : (ACC.address.bindCreateUpdateAddressForm(), ACC.colorbox.resize());
        },
        error: function(t, e, i) {
          alert("Failed to update cart. Error details [" + t + ", " + e + ", " + i + "]");
        },
        complete: function() {
          $("#checkout_delivery_address").unblock();
        }
      };
      $(this).ajaxForm(t);
    });
  },
  refreshDeliveryAddressSection: function(t) {
    $(".summaryDeliveryAddress").replaceWith($("#deliveryAddressSummaryTemplate").tmpl(t));
  },
  bindSuggestedDeliveryAddresses: function() {
    var t = $(".add_edit_delivery_address_id").attr("status");
    null != t && "hasSuggestedAddresses" == t && ACC.address.showSuggestedAddressesPopup();
  },
  showSuggestedAddressesPopup: function() {
    ACC.colorbox.open("", {
      href: "#popup_suggested_delivery_addresses",
      inline: !0,
      overlayClose: !1,
      width: 525
    });
  },
  bindCountrySpecificAddressForms: function() {
    $(document).on("change", "#countrySelector select", function() {
      var t = {
        addressCode: "",
        countryIsoCode: $(this).val()
      };
      ACC.address.displayCountrySpecificAddressForm(t, ACC.address.showAddressFormButtonPanel);
    });
  },
  showAddressFormButtonPanel: function() {
    "" !== $("#countrySelector :input").val() && $("#addressform_button_panel").show();
  },
  bindToColorboxClose: function() {
    $(document).on("click", ".closeColorBox", function() {
      ACC.colorbox.close();
    });
  },
  displayCountrySpecificAddressForm: function(t, e) {
    $.ajax({
      url: ACC.config.encodedContextPath + "/my-account/addressform",
      async: !0,
      data: t,
      dataType: "html",
      beforeSend: function() {
        $("#i18nAddressForm").html(ACC.address.spinner);
      }
    }).done(function(t) {
      $("#i18nAddressForm").html($(t).html()), "function" == typeof e && e.call();
    });
  },
  bindToChangeAddressButton: function() {
    $(document).on("click", ".summaryDeliveryAddress .editButton", ACC.address.handleChangeAddressButtonClick);
  },
  bindViewAddressBook: function() {
    $(document).on("click", ".js-address-book", function(t) {
      t.preventDefault(), ACC.colorbox.open("", {
        href: "#addressbook",
        inline: !0,
        width: "100%"
      });
    }), $(document).on("click", "#summaryOverlayViewAddressBook", function() {
      $("#summaryDeliveryAddressFormContainer").hide(), $("#summaryOverlayViewAddressBook").hide(), 
      $("#summaryDeliveryAddressBook").show(), ACC.colorbox.resize();
    });
  },
  showRemoveAddressFromBookConfirmation: function() {
    $(document).on("click", ".removeAddressFromBookButton", function() {
      var t = $(this).data("addressId"), e = $(this).data("popupTitle");
      ACC.colorbox.open(e, {
        inline: !0,
        height: !1,
        href: "#popup_confirm_address_removal_" + t,
        onComplete: function() {
          $(this).colorbox.resize();
        }
      });
    });
  },
  backToListAddresses: function() {
    $(".addressBackBtn").on("click", function() {
      var t = $(this).data("backToAddresses");
      window.location = t;
    });
  }
}, ACC.autocomplete = {
  _autoload: [ "bindSearchAutocomplete", "bindDisableSearch" ],
  bindSearchAutocomplete: function() {
    $.widget("custom.yautocomplete", $.ui.autocomplete, {
      _create: function() {
        var t = this.element.data("options");
        this._setOptions({
          minLength: t.minCharactersBeforeRequest,
          displayProductImages: t.displayProductImages,
          delay: t.waitTimeBeforeRequest,
          autocompleteUrl: t.autocompleteUrl,
          source: this.source
        }), $.ui.autocomplete.prototype._create.call(this);
      },
      options: {
        cache: {},
        focus: function() {
          return !1;
        },
        select: function(t, e) {
          window.location.href = e.item.url;
        }
      },
      _renderItem: function(t, e) {
        if ("autoSuggestion" == e.type) {
          i = "<a href='" + e.url + "' ><div class='name'>" + e.value + "</div></a>";
          return $("<li>").data("item.autocomplete", e).append(i).appendTo(t);
        }
        if ("productResult" == e.type) {
          var i = "<a href='" + e.url + "' >";
          return null != e.image && (i += "<div class='thumb'><img src='" + e.image + "'  /></div>"), 
          i += "<div class='name'>" + e.value + "</div>", null !== e.discountPrice ? i += "<div class='price'>" + e.discountPrice.formattedValue + "</div>" : i += "<div class='price'>" + e.price + "</div>", 
          i += "</a>", $("<li>").data("item.autocomplete", e).append(i).appendTo(t);
        }
      },
      source: function(t, e) {
        var i = this, n = t.term.toLowerCase();
        if (n in i.options.cache) return e(i.options.cache[n]);
        $.getJSON(i.options.autocompleteUrl, {
          term: t.term
        }, function(t) {
          var o = [];
          return null != t.suggestions && $.each(t.suggestions, function(t, e) {
            o.push({
              value: e.term,
              url: ACC.config.encodedContextPath + "/search?text=" + e.term,
              type: "autoSuggestion"
            });
          }), null != t.products && $.each(t.products, function(t, e) {
            o.push({
              value: ACC.autocomplete.escapeHTML(e.name),
              code: e.code,
              desc: e.description,
              manufacturer: e.manufacturer,
              url: ACC.config.encodedContextPath + e.url,
              price: e.price.formattedValue,
              discountPrice: e.discountPrice,
              type: "productResult",
              image: null != e.images && i.options.displayProductImages ? e.images[0].url : null
            });
          }), i.options.cache[n] = o, e(o);
        });
      }
    }), $search = $(".js-site-search-input"), $search.length > 0 && $search.yautocomplete();
  },
  bindDisableSearch: function() {
    $("#js-site-search-input").keyup(function() {
      $("#js-site-search-input").val($("#js-site-search-input").val().replace(/^\s+/gm, "")), 
      $(".js_search_button").prop("disabled", "" == this.value);
    });
  },
  escapeHTML: function(t) {
    return t.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
  }
}, ACC.carousel = {
  _autoload: [ [ "bindCarousel", $(".js-owl-carousel").length > 0 ], "bindJCarousel" ],
  carouselConfig: {
    default: {
      navigation: !0,
      navigationText: [ "<span class='glyphicon glyphicon-chevron-left'></span>", "<span class='glyphicon glyphicon-chevron-right'></span>" ],
      pagination: !1,
      itemsCustom: [ [ 0, 2 ], [ 640, 4 ], [ 1024, 5 ], [ 1400, 7 ] ]
    },
    "rotating-image": {
      navigation: !1,
      pagination: !0,
      singleItem: !0
    },
    "lazy-reference": {
      navigation: !0,
      navigationText: [ "<span class='glyphicon glyphicon-chevron-left'></span>", "<span class='glyphicon glyphicon-chevron-right'></span>" ],
      pagination: !1,
      itemsDesktop: [ 5e3, 7 ],
      itemsDesktopSmall: [ 1200, 5 ],
      itemsTablet: [ 768, 4 ],
      itemsMobile: [ 480, 3 ],
      lazyLoad: !0
    }
  },
  bindCarousel: function() {
    $(".js-owl-carousel").each(function() {
      var t = $(this);
      $.each(ACC.carousel.carouselConfig, function(e, i) {
        t.hasClass("js-owl-" + e) && $(".js-owl-" + e).owlCarousel(i);
      });
    });
  },
  bindJCarousel: function() {
    $(".modal").colorbox({
      onComplete: function() {
        ACC.common.refreshScreenReaderBuffer();
      },
      onClosed: function() {
        ACC.common.refreshScreenReaderBuffer();
      }
    }), $(".svw").each(function() {
      $(this).waitForImages(function() {
        $(this).slideView({
          toolTip: !0,
          ttOpacity: .6,
          autoPlay: !0,
          autoPlayTime: 8e3
        });
      });
    });
  }
}, ACC.cart = {
  _autoload: [ "bindHelp", "cartRestoration", "bindCartPage", "bindMultiDEntryRemoval", "bindMultidCartProduct", "bindApplyVoucher", "bindToReleaseVoucher" ],
  bindHelp: function() {
    $(document).on("click", ".js-cart-help", function(t) {
      t.preventDefault();
      var e = $(this).data("help");
      ACC.colorbox.open(e, {
        html: $(".js-help-popup-content").html(),
        width: "300px"
      });
    });
  },
  cartRestoration: function() {
    $(".cartRestoration").click(function() {
      var t = $(this).data("cartUrl");
      window.location = t;
    });
  },
  bindCartPage: function() {
    $(document).on("click", ".js-show-editable-grid", function(t) {
      ACC.cart.populateAndShowEditableGrid(this, t);
    });
  },
  bindMultiDEntryRemoval: function() {
    $(document).on("click", ".js-submit-remove-product-multi-d", function() {
      var t = $(this).data("index"), e = $("#updateCartForm" + t), i = e.find("input[name=initialQuantity]"), n = e.find("input[name=quantity]"), o = (e.find("input[name=entryNumber]").val(), 
      e.find("input[name=productCode]").val());
      n.val(0), i.val(0), ACC.track.trackRemoveFromCart(o, i, n.val());
      var s = e.attr("method") ? e.attr("method").toUpperCase() : "GET";
      $.ajax({
        url: e.attr("action"),
        data: e.serialize(),
        type: s,
        success: function(t) {
          location.reload();
        },
        error: function() {
          alert("Failed to remove quantity. Error details [" + xht + ", " + textStatus + ", " + ex + "]");
        }
      });
    });
  },
  populateAndShowEditableGrid: function(t, e) {
    var i = $(t).data("readOnlyMultidGrid"), n = $(t).data("index");
    grid = $("#ajaxGrid" + n);
    var o = $("#grid" + n), s = o.data("sub-entries").split(","), a = s[0].split(":")[0];
    $(t).toggleClass("open");
    for (var r = o.data("target-url"), l = new Object(), c = 0; c < s.length; c++) {
      var d = s[c].split(":");
      l[d[0]] = d[1];
    }
    if (grid.children("#cartOrderGridForm").length > 0) grid.slideToggle("slow"); else {
      $.ajax({
        url: r,
        data: {
          productCode: a,
          readOnly: i
        },
        type: "GET",
        success: function(e) {
          grid.html(e), $("#ajaxGrid").removeAttr("id");
          for (var i = grid.find(".product-grid-container"), n = i.length, o = 0; o < n; o++) ACC.cart.getProductQuantity(i.eq(o), l, o);
          grid.slideDown("slow"), ACC.cart.coreCartGridTableActions(t, l), ACC.productorderform.coreTableScrollActions(grid.children("#cartOrderGridForm"));
        },
        error: function(t, e, i) {
          alert("Failed to get variant matrix. Error details [" + t + ", " + e + ", " + i + "]");
        }
      });
    }
  },
  coreCartGridTableActions: function(t, e) {
    ACC.productorderform.bindUpdateFutureStockButton(".update_future_stock_button"), 
    ACC.productorderform.bindVariantSelect($(".variant-select-btn"), "cartOrderGridForm");
    var i = $(t).data("index"), n = 0, o = $("#ajaxGrid" + i + " .product-grid-container");
    o.on("focusin", ".sku-quantity", function(t) {
      n = jQuery.trim(this.value), $(this).parents("tr").next(".variant-summary").remove(), 
      $(this).parents("table").data(ACC.productorderform.selectedVariantData) ? ACC.productorderform.selectedVariants = $(this).parents("table").data(ACC.productorderform.selectedVariantData) : ACC.productorderform.selectedVariants = [], 
      "" == n && (n = 0, this.value = 0);
    }), o.on("focusout keypress", ".sku-quantity", function(t) {
      var s = t.keyCode || t.which || t.charCode;
      if (13 == s || void 0 == s) {
        var a = 0, r = "", l = parseInt($(this).attr("id").match("[0-9]+"));
        this.value = ACC.productorderform.filterSkuEntry(this.value), a = jQuery.trim(this.value);
        var c = $("input[id='cartEntries[" + l + "].sku']").val();
        isNaN(jQuery.trim(this.value)) && (this.value = 0), "" == a && (a = 0, this.value = 0);
        var d = o.find("[data-grid-total-id=total_value_" + l + "]"), u = $("input[id='productPrice[" + l + "]']").val();
        a > 0 && (r = ACC.productorderform.formatTotalsCurrency(parseFloat(u) * parseInt(a))), 
        d.html(r);
        var h = this, p = $(this).siblings(".price"), f = $(this).siblings(".variant-prop"), m = $(this).next(".td_stock").data("sku-id"), g = $(this).siblings(".data-grid-total");
        if (this.value != n) {
          var v = !0;
          ACC.productorderform.selectedVariants.forEach(function(t, e) {
            t.id === m && (v = !1, "0" === h.value || 0 === h.value ? ACC.productorderform.selectedVariants.splice(e, 1) : (ACC.productorderform.selectedVariants[e].quantity = h.value, 
            ACC.productorderform.selectedVariants[e].total = ACC.productorderform.updateVariantTotal(p, h.value, g)));
          }), v && this.value > 0 && ACC.productorderform.selectedVariants.push({
            id: m,
            size: f.data("variant-prop"),
            quantity: h.value,
            total: ACC.productorderform.updateVariantTotal(p, h.value, g)
          });
        }
        if (ACC.productorderform.showSelectedVariant($(this).parents("table")), this.value > 0 && this.value != n ? $(this).parents("table").addClass("selected") : 0 === ACC.productorderform.selectedVariants.length && $(this).parents("table").removeClass("selected").find(".variant-summary").remove(), 
        n != a) {
          $.ajax({
            url: ACC.config.encodedContextPath + "/cart/updateMultiD",
            data: {
              productCode: c,
              quantity: a,
              entryNumber: -1
            },
            type: "POST",
            success: function(t, n, o) {
              ACC.cart.refreshCartData(t, -1, a, i), e[c] = a;
            },
            error: function(t, e, i) {
              var n = t.getResponseHeader("redirectUrl"), o = t.getResponseHeader("Connection");
              null !== n ? window.location = n : "close" === o && window.location.reload();
            }
          });
        }
      }
    });
  },
  refreshCartData: function(t, e, i, n) {
    if (0 == t.entries.length) location.reload(); else {
      var o;
      if (-1 == e) {
        for (var s = (o = $(".js-qty-form" + n)).find("input[name=productCode]").val(), i = 0, a = 0, r = 0; r < t.entries.length; r++) {
          var l = t.entries[r];
          if (l.product.code == s) {
            i = l.quantity, a = l.totalPrice;
            break;
          }
        }
        0 == i ? location.reload() : (o.find(".qtyValue").html(i), o.parent().parent().find(".js-item-total").html(a.formattedValue));
      }
      ACC.cart.refreshCartPageWithJSONResponse(t);
    }
  },
  refreshCartPageWithJSONResponse: function(t) {
    ACC.minicart.updateMiniCartDisplay(), $(".js-cart-top-totals").html($("#cartTopTotalSectionTemplate").tmpl(t)), 
    $("div .cartpotproline").remove(), $("div .cartproline").remove(), $(".js-cart-totals").remove(), 
    $("#ajaxCartPotentialPromotionSection").html($("#cartPotentialPromotionSectionTemplate").tmpl(t)), 
    $("#ajaxCartPromotionSection").html($("#cartPromotionSectionTemplate").tmpl(t)), 
    $("#ajaxCart").html($("#cartTotalsTemplate").tmpl(t)), ACC.quote.bindQuoteDiscount();
  },
  getProductQuantity: function(t, e, i) {
    var n = t.find("table");
    $.each(n, function(i, n) {
      var o = jQuery.map($(n).find("input[type='hidden'].sku"), function(t) {
        return t.value;
      }), s = jQuery.map($(n).find("input[type='textbox'].sku-quantity"), function(t) {
        return t;
      }), a = [];
      $.each(o, function(i, n) {
        var o = e[n];
        if (void 0 != o) {
          s[i].value = o;
          var r = parseInt(s[i].id.match("[0-9]+")), l = t.find("[data-grid-total-id=total_value_" + r + "]"), c = "", d = $("input[id='productPrice[" + r + "]']").val();
          o > 0 && (c = ACC.productorderform.formatTotalsCurrency(parseFloat(d) * parseInt(o))), 
          l.html(c), a.push({
            id: n,
            size: $(s[i]).siblings(".variant-prop").data("variant-prop"),
            quantity: o,
            total: c
          });
        }
      }), 0 != a.length && ($.tmpl(ACC.productorderform.$variantSummaryTemplate, {
        variants: a
      }).appendTo($(n).addClass("selected")), $(n).find(".variant-summary .variant-property").html($(n).find(".variant-detail").data("variant-property")), 
      $(n).data(ACC.productorderform.selectedVariantData, a));
    });
  },
  bindMultidCartProduct: function() {
    $(document).on("click", ".showQuantityProduct", function(t) {
      ACC.multidgrid.populateAndShowGrid(this, t, !0);
    }), $(document).on("click", ".showQuantityProductOverlay", function(t) {
      ACC.multidgrid.populateAndShowGridOverlay(this, t);
    });
  },
  bindApplyVoucher: function() {
    $("#js-voucher-apply-btn").on("click", function(t) {
      ACC.cart.handleApplyVoucher(t);
    }), $("#js-voucher-code-text").on("keypress", function(t) {
      13 == (t.keyCode ? t.keyCode : t.which) && ACC.cart.handleApplyVoucher(t);
    });
  },
  handleApplyVoucher: function(t) {
    var e = $.trim($("#js-voucher-code-text").val());
    "" != e && e.length > 0 && $("#applyVoucherForm").submit();
  },
  bindToReleaseVoucher: function() {
    $(".js-release-voucher-remove-btn").on("click", function(t) {
      $(this).closest("form").submit();
    });
  }
}, ACC.checkout = {
  _autoload: [ "bindCheckO", "bindForms", "bindSavedPayments" ],
  bindForms: function() {
    $(document).on("click", "#addressSubmit", function(t) {
      t.preventDefault(), $("#sharjahAddressForm").submit();
    }), $(document).on("click", "#deliveryMethodSubmit", function(t) {
      t.preventDefault(), $("#selectDeliveryMethodForm").submit();
    });
  },
  bindSavedPayments: function() {
    $(document).on("click", ".js-saved-payments", function(t) {
      t.preventDefault();
      $.colorbox({
        href: "#savedpaymentsbody",
        inline: !0,
        maxWidth: "100%",
        opacity: .7,
        title: "",
        close: '<span class="glyphicon glyphicon-remove"></span>',
        onComplete: function() {}
      });
    });
  },
  bindCheckO: function() {
    var t = !1;
    $(".doFlowSelectedChange").change(function() {
      "multistep-pci" == $("#selectAltCheckoutFlow").val() ? $("#selectPciOption").show() : $("#selectPciOption").hide();
    }), $(".js-continue-shopping-button").click(function() {
      var t = $(this).data("continueShoppingUrl");
      window.location = t;
    }), $(".expressCheckoutButton").click(function() {
      document.getElementById("expressCheckoutCheckbox").checked = !0;
    }), $(document).on("input", ".confirmGuestEmail,.guestEmail", function() {
      $(".guestEmail").val() === $(".confirmGuestEmail").val() ? $(".guestCheckoutBtn").removeAttr("disabled") : $(".guestCheckoutBtn").attr("disabled", "disabled");
    }), $(".js-continue-checkout-button").click(function() {
      var e = $(this).data("checkoutUrl");
      if (!(t = ACC.pickupinstore.validatePickupinStoreCartEntires())) {
        var i = $(".express-checkout-checkbox");
        if (i.is(":checked")) window.location = i.data("expressCheckoutUrl"); else {
          var n = $("#selectAltCheckoutFlow").val();
          if (void 0 == n || "" == n || "select-checkout" == n) window.location = e; else {
            "multistep-pci" == n && (n = "multistep");
            var o = e + "/select-flow?flow=" + n + "&pci=" + $("#selectPciOption").val();
            window.location = o;
          }
        }
      }
      return !1;
    });
  }
}, ACC.checkoutaddress = {
  spinner: $("<img src='" + ACC.config.commonResourcePath + "/images/spinner.gif' />"),
  addressID: "",
  showAddressBook: function() {
    $(document).on("click", "#viewAddressBook", function() {
      var t = $("#savedAddressListHolder").html();
      $.colorbox({
        height: !1,
        html: t,
        onComplete: function() {
          $(this).colorbox.resize();
        }
      });
    });
  },
  showRemoveAddressConfirmation: function() {
    $(document).on("click", ".removeAddressButton", function() {
      var t = $(this).data("addressId");
      $.colorbox({
        inline: !0,
        height: !1,
        href: "#popup_confirm_address_removal_" + t,
        onComplete: function() {
          $(this).colorbox.resize();
        }
      });
    });
  }
}, $(document).ready(function() {
  with (ACC.checkoutaddress) showAddressBook(), showRemoveAddressConfirmation();
}), ACC.checkoutsteps = {
  _autoload: [ "permeateLinks" ],
  permeateLinks: function() {
    $(document).on("click", ".js-checkout-step", function(t) {
      t.preventDefault(), window.location = $(this).closest("a").attr("href");
    });
  }
}, ACC.cms = {
  loadComponent: function(t, e, i, n, o) {
    var s = this;
    t && $.ajax({
      url: ACC.config.contextPath + "/cms/component?componentUid=" + t,
      cache: !1,
      type: "GET",
      success: function(o) {
        reprocess = o.indexOf("js-responsive-image") > -1, s.insertHtml(o, i, reprocess), 
        n && n(o, t, e, i);
      },
      error: function(n) {
        o && o(n, t, e, i);
      }
    });
  },
  insertHtml: function(t, e, i) {
    e && ($(e).html(t), i && ACC.global.reprocessImages());
  }
};

var cboxOptions = {
  width: "95%",
  height: "95%",
  maxWidth: "960px",
  maxHeight: "960px"
};

$(".cbox-link").colorbox(cboxOptions), $(window).resize(function() {
  $("#colorbox").hasClass("variantSelectMobile") || $.colorbox.resize({
    width: window.innerWidth > parseInt(cboxOptions.maxWidth) ? cboxOptions.maxWidth : cboxOptions.width,
    height: window.innerHeight > parseInt(cboxOptions.maxHeight) ? cboxOptions.maxHeight : cboxOptions.height
  });
}), ACC.colorbox = {
  config: {
    maxWidth: "100%",
    opacity: .7,
    width: "auto",
    transition: "none",
    close: '<span class="glyphicon glyphicon-remove"></span>',
    title: '<div class="headline"><span class="headline-text">{title}</span></div>',
    onComplete: function() {
      $.colorbox.resize(), ACC.common.refreshScreenReaderBuffer();
    },
    onClosed: function() {
      ACC.common.refreshScreenReaderBuffer();
    }
  },
  open: function(t, e) {
    return (e = $.extend({}, ACC.colorbox.config, e)).title = e.title.replace(/{title}/g, t), 
    $.colorbox(e);
  },
  resize: function() {
    $.colorbox.resize();
  },
  close: function() {
    $.colorbox.close();
  }
}, ACC.common = {
  currentCurrency: $("main").data("currencyIsoCode") || "USD",
  processingMessage: $("<img src='" + ACC.config.commonResourcePath + "/images/spinner.gif'/>"),
  blockFormAndShowProcessingMessage: function(t) {
    t.parents("form:first").block({
      message: ACC.common.processingMessage
    });
  },
  refreshScreenReaderBuffer: function() {
    $("#accesibility_refreshScreenReaderBufferField").attr("value", new Date().getTime());
  },
  checkAuthenticationStatusBeforeAction: function(t) {
    $.ajax({
      url: ACC.config.authenticationStatusUrl,
      statusCode: {
        401: function() {
          location.href = ACC.config.loginUrl;
        }
      },
      success: function(e) {
        "authenticated" == e && t();
      }
    });
  }
}, jQuery.extend({
  postJSON: function(t, e, i) {
    return jQuery.post(t, e, i, "json");
  }
}), $.ajaxPrefilter(function(t, e, i) {
  if ("post" === t.type || "POST" === t.type) if (void 0 === t.data) t.data = "CSRFToken=" + ACC.config.CSRFToken; else {
    var n = /application\/json/i;
    t.data instanceof window.FormData ? t.data.append("CSRFToken", ACC.config.CSRFToken) : n.test(t.contentType) ? i.setRequestHeader("CSRFToken", ACC.config.CSRFToken) : -1 === t.data.indexOf("CSRFToken") && (t.data = t.data + "&CSRFToken=" + ACC.config.CSRFToken);
  }
}), ACC.forgottenpassword = {
  _autoload: [ "bindLink" ],
  bindLink: function() {
    $(document).on("click", ".js-password-forgotten", function(t) {
      t.preventDefault(), ACC.colorbox.open($(this).data("cboxTitle"), {
        href: $(this).data("link"),
        width: "350px",
        fixed: !0,
        top: 150,
        onOpen: function() {
          $("#validEmail").remove();
        },
        onComplete: function() {
          $("form#forgottenPwdForm").ajaxForm({
            success: function(t) {
              $(t).closest("#validEmail").length ? 0 === $("#validEmail").length && ($(".forgotten-password").replaceWith(t), 
              ACC.colorbox.resize()) : ($("#forgottenPwdForm .control-group").replaceWith($(t).find(".control-group")), 
              ACC.colorbox.resize());
            }
          });
        }
      });
    });
  }
}, ACC.global = {
  _autoload: [ [ "passwordStrength", $(".password-strength").length > 0 ], "bindToggleOffcanvas", "bindToggleXsSearch", "bindHoverIntentMainNavigation", "initImager", "backToHome" ],
  passwordStrength: function() {
    $(".password-strength").pstrength({
      verdicts: [ ACC.pwdStrengthTooShortPwd, ACC.pwdStrengthVeryWeak, ACC.pwdStrengthWeak, ACC.pwdStrengthMedium, ACC.pwdStrengthStrong, ACC.pwdStrengthVeryStrong ],
      minCharText: ACC.pwdStrengthMinCharText
    });
  },
  bindToggleOffcanvas: function() {
    $(document).on("click", ".js-toggle-sm-navigation", function() {
      ACC.global.toggleClassState($("main"), "offcanvas"), ACC.global.toggleClassState($("html"), "offcanvas"), 
      ACC.global.toggleClassState($("body"), "offcanvas"), ACC.global.resetXsSearch();
    });
  },
  bindToggleXsSearch: function() {
    $(document).on("click", ".js-toggle-xs-search", function() {
      ACC.global.toggleClassState($(".site-search"), "active"), ACC.global.toggleClassState($(".js-mainHeader .navigation--middle"), "search-open");
    });
  },
  resetXsSearch: function() {
    $(".site-search").removeClass("active"), $(".js-mainHeader .navigation--middle").removeClass("search-open");
  },
  toggleClassState: function(t, e) {
    return t.hasClass(e) ? t.removeClass(e) : t.addClass(e), t.hasClass(e);
  },
  bindHoverIntentMainNavigation: function() {
    enquire.register("screen and (min-width:" + screenMdMin + ")", {
      match: function() {
        $(".js-enquire-has-sub").hoverIntent(function() {
          var t = $(this), e = t.width(), i = t.find(".js_sub__navigation"), n = i.outerWidth(), o = $(".js_navigation--bottom").width();
          console.log(i);
          var s = t.position().left + e / 2 - n / 2, a = t.position().top + t.height();
          s > 0 && s + n < o ? i.css({
            left: s,
            top: a,
            right: "auto"
          }) : s < 0 ? i.css({
            left: 0,
            top: a,
            right: "auto"
          }) : s + n > o && i.css({
            right: 0,
            top: a,
            left: "auto"
          }), t.addClass("show-sub");
        }, function() {
          $(this).removeClass("show-sub");
        });
      },
      unmatch: function() {
        $(".js_sub__navigation").removeAttr("style"), $(".js-enquire-has-sub").hoverIntent(function() {});
      }
    });
  },
  initImager: function(t) {
    t = t || ".js-responsive-image", this.imgr = new Imager(t);
  },
  reprocessImages: function(t) {
    t = t || ".js-responsive-image", void 0 == this.imgr ? this.initImager(t) : this.imgr.checkImagesNeedReplacing($(t));
  },
  addGoogleMapsApi: function(callback) {
    void 0 != callback && 0 == $(".js-googleMapsApi").length ? $("head").append('<script class="js-googleMapsApi" type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=' + ACC.config.googleApiKey + "&sensor=false&callback=" + callback + '"><\/script>') : void 0 != callback && eval(callback + "()");
  },
  backToHome: function() {
    $(".backToHome").on("click", function() {
      var t = ACC.config.contextPath;
      window.location = t;
    });
  }
}, ACC.hopdebug = {
  bindAll: function() {
    this.bindShowDebugMode();
  },
  bindShowDebugMode: function() {
    $("#hopDebugMode").data("hopDebugMode") || $("#showDebugPage").val() || $("#hostedOrderPagePostForm").submit();
  }
}, $(document).ready(function() {
  ACC.hopdebug.bindAll();
}), ACC.imagegallery = {
  _autoload: [ "bindImageGallery" ],
  bindImageGallery: function() {
    $(".js-gallery").each(function() {
      function t(t) {
        $(t).zoom({
          url: $(t).find("img.lazyOwl").data("zoomImage"),
          touch: !0,
          on: "grab",
          touchduration: 300,
          onZoomIn: function() {},
          onZoomOut: function() {
            e.data("owlCarousel").dragging(!0), e.data("zoomEnable", !0);
          },
          zoomEnableCallBack: function() {
            var t = e.data("zoomEnable"), i = e.data("owlCarousel");
            return 0 == t ? i.dragging(!0) : i.dragging(!1), t;
          }
        });
      }
      var e = $(this).find(".js-gallery-image"), i = $(this).find(".js-gallery-carousel");
      e.owlCarousel({
        singleItem: !0,
        pagination: !0,
        navigation: !0,
        lazyLoad: !0,
        navigationText: [ "<span class='glyphicon glyphicon-chevron-left'></span>", "<span class='glyphicon glyphicon-chevron-right'></span>" ],
        afterAction: function() {
          ACC.imagegallery.syncPosition(e, i, this.currentItem), e.data("zoomEnable", !0);
        },
        startDragging: function() {
          e.data("zoomEnable", !1);
        },
        afterLazyLoad: function(i) {
          var n = e.data("owlCarousel") || {};
          n.currentItem || (n.currentItem = 0), t($(e.find("img.lazyOwl")[n.currentItem]).parent());
        }
      }), i.owlCarousel({
        navigation: !0,
        navigationText: [ "<span class='glyphicon glyphicon-chevron-left'></span>", "<span class='glyphicon glyphicon-chevron-right'></span>" ],
        pagination: !1,
        items: 2,
        itemsDesktop: [ 5e3, 7 ],
        itemsDesktopSmall: [ 1200, 5 ],
        itemsTablet: [ 768, 4 ],
        itemsMobile: [ 480, 3 ],
        lazyLoad: !0,
        afterAction: function() {}
      }), i.on("click", "a.item", function(t) {
        t.preventDefault(), e.trigger("owl.goTo", $(this).parent(".owl-item").data("owlItem"));
      });
    });
  },
  syncPosition: function(t, e, i) {
    e.trigger("owl.goTo", i);
  }
}, ACC.langcurrency = {
  _autoload: [ "bindLangCurrencySelector" ],
  bindLangCurrencySelector: function() {
    $("#lang-selector").change(function() {
      $("#lang-form").submit();
    }), $("#currency-selector").change(function() {
      $("#currency-form").submit();
    });
  }
}, ACC.order = {
  _autoload: [ "backToOrderHistory", "bindMultidProduct" ],
  backToOrderHistory: function() {
    $(".orderBackBtn > button").on("click", function() {
      var t = $(this).data("backToOrders");
      window.location = t;
    });
  },
  bindMultidProduct: function() {
    $(document).on("click", ".js-show-multiD-grid-in-order", function(t) {
      return ACC.multidgrid.populateAndShowGrid(this, t, !0), !1;
    }), $(document).on("click", ".showMultiDGridInOrderOverlay", function(t) {
      ACC.multidgrid.populateAndShowGridOverlay(this, t);
    });
  }
}, ACC.payment = {
  activateSavedPaymentButton: function() {
    $(document).on("click", ".js-saved-payments", function(t) {
      t.preventDefault();
      var e = $("#savedpaymentstitle").html();
      $.colorbox({
        href: "#savedpaymentsbody",
        inline: !0,
        maxWidth: "100%",
        opacity: .7,
        width: "320px",
        title: e,
        close: '<span class="glyphicon glyphicon-remove"></span>',
        onComplete: function() {}
      });
    });
  },
  bindPaymentCardTypeSelect: function() {
    ACC.payment.filterCardInformationDisplayed(), $("#card_cardType").change(function() {
      "024" == $(this).val() ? $("#startDate, #issueNum").show() : $("#startDate, #issueNum").hide();
    });
  },
  filterCardInformationDisplayed: function() {
    "024" == $("#card_cardType").val() ? $("#startDate, #issueNum").show() : $("#startDate, #issueNum").hide();
  }
}, $(document).ready(function() {
  with (ACC.payment) activateSavedPaymentButton(), bindPaymentCardTypeSelect();
}), ACC.paymentDetails = {
  _autoload: [ "showRemovePaymentDetailsConfirmation" ],
  showRemovePaymentDetailsConfirmation: function() {
    $(document).on("click", ".removePaymentDetailsButton", function() {
      var t = $(this).data("paymentId"), e = $(this).data("popupTitle");
      ACC.colorbox.open(e, {
        inline: !0,
        href: "#popup_confirm_payment_removal_" + t,
        onComplete: function() {
          $(this).colorbox.resize();
        }
      });
    });
  }
}, ACC.pickupinstore = {
  _autoload: [ "bindClickPickupInStoreButton", "bindPickupButton", "bindPickupClose", "bindPickupInStoreSearch" ],
  storeId: "",
  unbindPickupPaginationResults: function() {
    $(document).off("click", "#colorbox .js-pickup-store-pager-prev"), $(document).off("click", "#colorbox .js-pickup-store-pager-next");
  },
  bindPickupPaginationResults: function() {
    function t() {
      var t = Math.ceil(a / (o * n) * -1) + 1;
      $("#colorbox .js-pickup-store-pager-item-from").html(t * o - 4);
      var e = t * o > s ? s : t * o;
      t * o - 4 == 1 ? $("#colorbox .js-pickup-store-pager-prev").hide() : $("#colorbox .js-pickup-store-pager-prev").show(), 
      t * o >= s ? $("#colorbox .js-pickup-store-pager-next").hide() : $("#colorbox .js-pickup-store-pager-next").show(), 
      $("#colorbox .js-pickup-store-pager-item-to").html(e);
    }
    var e = $("#colorbox .js-pickup-store-list").height(), i = $("#colorbox .js-pickup-store-list > li"), n = i.height(), o = 5, s = i.length, a = 0;
    $("#colorbox .js-pickup-store-pager-item-all").html(s), $("#colorbox .store-navigation-pager").show(), 
    t(), $(document).on("click", "#colorbox .js-pickup-store-pager-prev", function(n) {
      n.preventDefault(), i.css("transform", "translateY(" + (a + e) + "px)"), a += e, 
      t();
    }), $(document).on("click", "#colorbox .js-pickup-store-pager-next", function(n) {
      n.preventDefault(), i.css("transform", "translateY(" + (a - e) + "px)"), a -= e, 
      t();
    });
  },
  bindPickupInStoreQuantity: function() {
    $(".pdpPickupQtyPlus").click(function(t) {
      t.preventDefault();
      var e = $(".js-add-pickup-cart #pdpPickupAddtoCartInput"), i = parseInt(e.val()), n = e.data("max");
      !isNaN(i) && i < n && (e.val(i + 1), e.change());
    }), $(".pdpPickupQtyMinus").click(function(t) {
      t.preventDefault();
      var e = $(".js-add-pickup-cart #pdpPickupAddtoCartInput"), i = parseInt(e.val()), n = e.data("min");
      !isNaN(i) && i > n && (e.val(i - 1), e.change());
    }), $("body").on("keyup", ".js-add-pickup-cart #pdpPickupAddtoCartInput", function(t) {
      var e = $(t.target);
      e.val(this.value.match(/[0-9]*/));
      e.val();
    });
  },
  bindPickupInStoreSearch: function() {
    $(document).on("click", "#pickupstore_location_search_button", function(t) {
      return ACC.pickupinstore.locationSearchSubmit($("#locationForSearch").val(), $("#atCartPage").val(), $("#entryNumber").val(), $(this).parents("form").attr("action")), 
      !1;
    }), $(document).on("keypress", "#locationForSearch", function(t) {
      if (13 === t.keyCode) return t.preventDefault(), ACC.pickupinstore.locationSearchSubmit($("#locationForSearch").val(), $("#atCartPage").val(), $("input.entryNumber").val(), $(this).parents("form").attr("action")), 
      !1;
    });
  },
  bindPickupHereInStoreButtonClick: function() {
    $(document).on("click", ".pickup_add_to_bag_instore_button", function(t) {
      $(this).prev(".hiddenPickupQty").val($("#pickupQty").val());
    }), $(document).on("click", ".pickup_here_instore_button", function(t) {
      $(this).prev(".hiddenPickupQty").val($("#pickupQty").val()), ACC.colorbox.close();
    });
  },
  locationSearchSubmit: function(t, e, i, n, o, s) {
    $("#colorbox .js-add-to-cart-for-pickup-popup, #colorbox .js-qty-selector-minus, #colorbox .js-qty-selector-input, #colorbox .js-qty-selector-plus").attr("disabled", "disabled"), 
    $.ajax({
      url: n,
      data: {
        locationQuery: t,
        cartPage: e,
        entryNumber: i,
        latitude: o,
        longitude: s
      },
      type: "post",
      success: function(t) {
        ACC.pickupinstore.refreshPickupInStoreColumn(t);
      }
    });
  },
  createListItemHtml: function(t, e) {
    var i = "";
    return i += '<li class="pickup-store-list-entry">', i += '<input type="radio" name="storeNamePost" value="' + t.displayName + '" id="pickup-entry-' + e + '" class="js-pickup-store-input" data-id="' + e + '">', 
    i += '<label for="pickup-entry-' + e + '" class="js-select-store-label">', i += '<span class="pickup-store-info">', 
    i += '<span class="pickup-store-list-entry-name">' + t.displayName + "</span>", 
    i += '<span class="pickup-store-list-entry-address">' + t.line1 + " " + t.line2 + "</span>", 
    i += '<span class="pickup-store-list-entry-city">' + t.town + "</span>", i += "</span>", 
    i += '<span class="store-availability">', i += '<span class="available">' + t.formattedDistance + "<br>" + t.stockPickup + "</span>", 
    i += "</span>", i += "</label>", i += "</li>";
  },
  refreshPickupInStoreColumn: function(t) {
    t = $.parseJSON(t);
    var e = "";
    for ($("#colorbox .js-pickup-component").data("data", t), i = 0; i < t.data.length; i++) e += ACC.pickupinstore.createListItemHtml(t.data[i], i);
    $("#colorbox .js-pickup-store-list").html(e), ACC.pickupinstore.unbindPickupPaginationResults(), 
    ACC.pickupinstore.bindPickupPaginationResults();
    var n = $("#colorbox .js-pickup-store-input")[0];
    $(n).click(), $("#colorbox .js-add-to-cart-for-pickup-popup, #colorbox .js-qty-selector-minus, #colorbox .js-qty-selector-input, #colorbox .js-qty-selector-plus").removeAttr("disabled");
  },
  bindClickPickupInStoreButton: function() {
    $(document).on("click", ".js-pickup-in-store-button", function(t) {
      t.preventDefault();
      var e = $(this), i = "pickupModal_" + $(this).attr("id"), n = $(this).attr("id");
      n = n.split("_"), n = n[1];
      var o = $("#popup_store_pickup_form > #pickupModal").clone(), s = $("#pickupTitle > .pickup-header").html();
      ACC.colorbox.open(s, {
        html: o,
        width: "960px",
        onComplete: function() {
          $("#colorbox .js-add-to-cart-for-pickup-popup, #colorbox .js-qty-selector-minus, #colorbox .js-qty-selector-input, #colorbox .js-qty-selector-plus").attr("disabled", "disabled"), 
          o.show(), ACC.pickupinstore.pickupStorePager();
          $("#colorbox .js-pickup-tabs").accessibleTabs({
            tabhead: ".tabhead",
            tabbody: ".tabbody",
            fx: "show",
            fxspeed: 0,
            currentClass: "active",
            autoAnchor: !0,
            cssClassAvailable: !0
          });
          $("#colorbox #pickupModal *").each(function() {
            void 0 != $(this).attr("data-id") && ($(this).attr("id", $(this).attr("data-id")), 
            $(this).removeAttr("data-id"));
          }), $("#colorbox input#locationForSearch").focus(), $("#colorbox #pickupModal").attr("id", i), 
          $("#colorbox #" + i + " .thumb").html(e.data("img")), $("#colorbox #" + i + " .js-pickup-product-price").html(e.data("productcart"));
          var t = e.data("productcartVariants"), s = "";
          $.each(t, function(t, e) {
            s += "<span>" + e + "</span>";
          }), $("#colorbox #" + i + " .js-pickup-product-variants").html(s), $("#colorbox  #" + i + " .js-pickup-product-info").html(e.data("productname")), 
          $("#colorbox #" + i + " form.searchPOSForm").attr("action", e.data("actionurl")), 
          $("#colorbox #" + i + " form.searchPOSForm").attr("id", "pickup_in_store_search_form_product_" + n), 
          $("#colorbox #" + i + " #pdpPickupAddtoCartInput").attr("value", void 0 !== $("#pdpPickupAddtoCartInput").val() ? $("#pdpPickupAddtoCartInput").val() : e.data("value")), 
          $("#colorbox #" + i + " input#entryNumber").attr("value", e.data("entrynumber")), 
          $("#colorbox #" + i + " input#atCartPage").attr("value", e.data("cartpage")), navigator.geolocation && navigator.geolocation.getCurrentPosition(function(t) {
            ACC.pickupinstore.locationSearchSubmit("", $("#atCartPage").val(), e.data("entrynumber"), e.data("actionurl"), t.coords.latitude, t.coords.longitude);
          }, function(t) {
            console.log("An error occurred... The error code and message are: " + t.code + "/" + t.message);
          }), ACC.product.bindToAddToCartStorePickUpForm();
        }
      });
    });
  },
  pickupStorePager: function() {
    $(document).on("change", "#colorbox .js-pickup-store-input", function(t) {
      t.preventDefault(), $("#colorbox .js-pickup-tabs li.first a").click();
      var e = $("#colorbox .js-pickup-component").data("data");
      e = e.data;
      var i = $(this).data("id"), n = $("#colorbox .display-details");
      $.each(e[i], function(t, e) {
        if ("url" == t) "" != e ? n.find(".js-store-image").html('<img src="' + e + '" alt="" />') : n.find(".js-store-image").html(""); else if ("productcode" == t) n.find(".js-store-productcode").val(e); else if ("openings" == t) if ("" != e) {
          var i = n.find(".js-store-" + t), o = "";
          $.each(e, function(t, e) {
            o += "<dt>" + t + "</dt>", o += "<dd>" + e + "</dd>";
          }), i.html(o);
        } else n.find(".js-store-" + t).html(""); else "specialOpenings" == t || ("" != e ? n.find(".js-store-" + t).html(e) : n.find(".js-store-" + t).html(""));
      }), $(document).one("click", "#colorbox .js-pickup-map-tab", function() {
        ACC.pickupinstore.storeId = e[i], ACC.global.addGoogleMapsApi("ACC.pickupinstore.drawMap");
      });
      var t = $("#colorbox .pickup-store-list-entry input:checked");
      if ($("#add_to_cart_storepickup_form .js-store-id").attr("id", t.attr("id")), $("#add_to_cart_storepickup_form .js-store-id").attr("name", t.attr("name")), 
      $("#add_to_cart_storepickup_form .js-store-id").val(t.val()), e[i].stockLevel > 0 || "" == e[i].stockLevel) {
        var o = $("#add_to_cart_storepickup_form .js-qty-selector-input");
        o.data("max", e[i].stockLevel), ACC.productDetail.checkQtySelector(o, "reset"), 
        $("#add_to_cart_storepickup_form").show();
      } else $("#add_to_cart_storepickup_form").hide();
    }), $(document).on("click", ".js-select-store-label", function(t) {
      $("#colorbox .js-pickup-component").addClass("show-store"), $("#colorbox #cboxTitle .headline-inner").addClass("hidden-xs hidden-sm"), 
      $("#colorbox #cboxTitle .back-to-storelist").removeClass("hidden-xs hidden-sm");
    }), $(document).on("click", ".js-back-to-storelist", function(t) {
      $("#colorbox .js-pickup-component").removeClass("show-store"), $("#colorbox #cboxTitle .headline-inner").removeClass("hidden-xs hidden-sm"), 
      $("#colorbox #cboxTitle .back-to-storelist").addClass("hidden-xs hidden-sm");
    });
  },
  bindPickupButton: function() {
    $(document).on("click", ".js-pickup-button", function(t) {
      t.preventDefault(), $e = $(this).parent().nextAll(".js-inline-layer"), $e.addClass("open");
      var e = $e.height();
      $e.removeClass("open"), $e.animate({
        height: e
      });
    });
  },
  bindPickupClose: function() {
    $(document).on("click", ".js-close-inline-layer", function(t) {
      t.preventDefault(), $e = $(this).parents(".js-inline-layer"), $e.animate({
        height: 0
      });
    });
  },
  checkIfPointOfServiceIsEmpty: function(t) {
    return !t.find(".pointOfServiceName").text().trim().length;
  },
  validatePickupinStoreCartEntires: function() {
    var t = !1;
    return $("form.cartEntryShippingModeForm").each(function() {
      var e = "#" + $(this).attr("id");
      $(e + " input[value=pickUp][checked]").length && ACC.pickupinstore.checkIfPointOfServiceIsEmpty($(this)) && ($(this).addClass("shipError"), 
      t = !0);
    }), t && ($("div#noStoreSelected").show().focus(), $(window).scrollTop(0)), t;
  },
  drawMap: function() {
    if (storeInformation = ACC.pickupinstore.storeId, $("#colorbox .js-map-canvas").length > 0) {
      $("#colorbox .js-map-canvas").attr("id", "pickup-map");
      var t = new google.maps.LatLng(storeInformation.storeLatitude, storeInformation.storeLongitude), e = {
        zoom: 13,
        zoomControl: !0,
        panControl: !0,
        streetViewControl: !1,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: t
      }, i = new google.maps.Map(document.getElementById("pickup-map"), e), n = new google.maps.Marker({
        position: new google.maps.LatLng(storeInformation.storeLatitude, storeInformation.storeLongitude),
        map: i,
        title: storeInformation.name,
        icon: "https://maps.google.com/mapfiles/markerA.png"
      }), o = new google.maps.InfoWindow({
        content: storeInformation.name,
        disableAutoPan: !0
      });
      google.maps.event.addListener(n, "click", function() {
        o.open(i, n);
      });
    }
  }
}, ACC.quickview = {
  _autoload: [ "bindToUiCarouselLink" ],
  initQuickviewLightbox: function() {
    ACC.product.enableAddToCartButton(), ACC.product.bindToAddToCartForm(), ACC.product.enableStorePickupButton();
  },
  refreshScreenReaderBuffer: function() {
    $("#accesibility_refreshScreenReaderBufferField").attr("value", new Date().getTime());
  },
  bindToUiCarouselLink: function() {
    var t = $("#quickViewTitle").html();
    $(".js-owl-carousel-reference .js-reference-item").colorbox({
      close: '<span class="glyphicon glyphicon-remove"></span>',
      title: t,
      maxWidth: "100%",
      onComplete: function() {
        ACC.quickview.refreshScreenReaderBuffer(), ACC.quickview.initQuickviewLightbox(), 
        ACC.ratingstars.bindRatingStars($(".quick-view-stars"));
      },
      onClosed: function() {
        ACC.quickview.refreshScreenReaderBuffer();
      }
    });
  }
}, ACC.ratingstars = {
  _autoload: [ [ "bindRatingStars", $(".js-ratingCalc").length > 0 ], [ "bindRatingStarsSet", $(".js-ratingCalcSet").length > 0 ] ],
  bindRatingStars: function() {
    $(".js-ratingCalc").each(function() {
      var t = $(this).data("rating");
      $(this).find(".js-greenStars").width($(this).width() * (parseFloat(t.rating, 10) / t.total));
    });
  },
  bindRatingStarsSet: function() {
    $(".js-writeReviewStars").on({
      mouseleave: function() {
        i();
        var t = 2 * parseFloat($(".js-ratingSetInput").val(), 10);
        "number" != typeof t || isNaN(t) ? i() : e(t);
      }
    });
    var t = $(".js-writeReviewStars .js-ratingIcon"), e = function(e) {
      t.slice(0, parseFloat(e, 10)).addClass("active");
    }, i = function() {
      t.removeClass("active");
    };
    t.on({
      mouseenter: function() {
        i(), e($(this).index() + 1);
      },
      mouseleave: function() {
        $(this).removeClass("active");
      },
      click: function() {
        $(".js-ratingSetInput").val(($(this).index() + 1) / 2);
      }
    });
  }
}, ACC.tabs = {
  _autoload: [ [ "bindTabs", $(".js-tabs").length > 0 ], "hideReviewBtn", "determineToDisplayReviews" ],
  bindTabs: function() {
    $e = $(".js-tabs");
    var t = $e.accessibleTabs({
      tabhead: ".tabhead",
      tabbody: ".tabbody",
      fx: "show",
      fxspeed: 0,
      currentClass: "active",
      autoAnchor: !0
    });
    $e.on("click", ".tabhead", function(t) {
      if (t.preventDefault(), $(this).hasClass("active")) $(this).removeClass("active"); else {
        $(this).parents(".js-tabs").children(".tabs-list").find("a[href=#" + $(this).attr("id") + "]").click();
        var e = $(this).position().top;
        $("body,html").scrollTop(e);
      }
    }), $e.on("click", "#tabreview", function(t) {
      t.preventDefault(), ACC.tabs.showReviewsAction("reviews");
    }), $e.on("click", ".all-reviews-btn", function(t) {
      t.preventDefault(), ACC.tabs.showReviewsAction("allreviews"), ACC.tabs.hideReviewBtn(".all-reviews-btn"), 
      ACC.tabs.showReviewBtn(".less-reviews-btn");
    }), $e.on("click", ".less-reviews-btn", function(t) {
      t.preventDefault(), ACC.tabs.showReviewsAction("reviews"), ACC.tabs.hideReviewBtn(".less-reviews-btn"), 
      ACC.tabs.showReviewBtn(".all-reviews-btn");
    }), $(document).on("click", ".js-writeReviewTab", function(e) {
      e.preventDefault(), t.showAccessibleTabSelector($(this).attr("href")), $(".js-review-write").show(), 
      $("#reviewForm input[name=headline]").focus();
    }), $(document).on("click", ".js-review-write-toggle", function(t) {
      t.preventDefault(), $(".js-review-write:visible").length < 1 ? $(".js-review-write").show() : $(".js-review-write").hide();
    }), $(document).on("click", ".js-openTab", function() {
      t.showAccessibleTabSelector($(this).attr("href"));
    });
  },
  showReviewsAction: function(t) {
    $.get($("#reviews").data(t), function(t) {
      $("#reviews").html(t), $(".js-ratingCalc").length > 0 && (ACC.ratingstars.bindRatingStars(), 
      ACC.tabs.showingAllReviews());
    });
  },
  hideReviewBtn: function(t) {
    t = void 0 == t ? ".less-reviews-btn" : t, $(t).hide();
  },
  showReviewBtn: function(t) {
    $(t).show();
  },
  showingAllReviews: function() {
    $("#showingAllReviews").data("showingallreviews") && ACC.tabs.hideReviewBtn(".all-reviews-btn");
  },
  determineToDisplayReviews: function() {
    "#tabreview" == location.hash && ACC.tabs.showReviewsAction("reviews");
  }
}, ACC.termsandconditions = {
  bindTermsAndConditionsLink: function() {
    $(document).on("click", ".termsAndConditionsLink", function(t) {
      t.preventDefault(), $.colorbox({
        maxWidth: "100%",
        maxHeight: "80%",
        width: "870px",
        scrolling: !0,
        href: $(this).attr("href"),
        close: '<span class="glyphicon glyphicon-remove"></span>',
        title: '<div class="headline"><span class="headline-text">Terms and Conditions</span></div>',
        onComplete: function() {
          ACC.common.refreshScreenReaderBuffer();
        },
        onClosed: function() {
          ACC.common.refreshScreenReaderBuffer();
        }
      });
    });
  }
}, $(function() {
  with (ACC.termsandconditions) bindTermsAndConditionsLink();
}), ACC.track = {
  trackAddToCart: function(t, e, i) {
    window.mediator.publish("trackAddToCart", {
      productCode: t,
      quantity: e,
      cartData: i
    });
  },
  trackRemoveFromCart: function(t, e) {
    window.mediator.publish("trackRemoveFromCart", {
      productCode: t,
      initialCartQuantity: e
    });
  },
  trackUpdateCart: function(t, e, i) {
    window.mediator.publish("trackUpdateCart", {
      productCode: t,
      initialCartQuantity: e,
      newCartQuantity: i
    });
  }
}, ACC.storefinder = {
  _autoload: [ "init", "bindStoreChange", "bindSearch", "bindPagination" ],
  storeData: "",
  storeId: "",
  coords: {},
  storeSearchData: {},
  createListItemHtml: function(t, e) {
    var i = "";
    return i += '<li class="list__entry">', i += '<input type="radio" name="storeNamePost" value="' + t.displayName + '" id="store-filder-entry-' + e + '" class="js-store-finder-input" data-id="' + e + '">', 
    i += '<label for="store-filder-entry-' + e + '" class="js-select-store-label">', 
    i += '<span class="entry__info">', i += '<span class="entry__name">' + t.displayName + "</span>", 
    i += '<span class="entry__address">' + t.line1 + " " + t.line2 + "</span>", i += "</span>", 
    i += "</span>", i += "</label>", i += "</li>";
  },
  refreshNavigation: function() {
    var t = "";
    if (data = ACC.storefinder.storeData, data) {
      for (i = 0; i < data.data.length; i++) t += ACC.storefinder.createListItemHtml(data.data[i], i);
      $(".js-store-finder-navigation-list").html(t);
      var e = $(".js-store-finder-input")[0];
      $(e).click();
    }
    var n = ACC.storefinder.storeSearchData.page;
    $(".js-store-finder-pager-item-from").html(10 * n + 1);
    var o = 10 * n + 10 > ACC.storefinder.storeData.total ? ACC.storefinder.storeData.total : 10 * n + 10;
    $(".js-store-finder-pager-item-to").html(o), $(".js-store-finder-pager-item-all").html(ACC.storefinder.storeData.total), 
    $(".js-store-finder").removeClass("show-store");
  },
  bindPagination: function() {
    function t(t) {
      0 == t ? $(".js-store-finder-pager-prev").attr("disabled", "disabled") : $(".js-store-finder-pager-prev").removeAttr("disabled"), 
      t == Math.floor(ACC.storefinder.storeData.total / 10) ? $(".js-store-finder-pager-next").attr("disabled", "disabled") : $(".js-store-finder-pager-next").removeAttr("disabled");
    }
    $(document).on("click", ".js-store-finder-details-back", function(t) {
      t.preventDefault(), $(".js-store-finder").removeClass("show-store");
    }), $(document).on("click", ".js-store-finder-pager-prev", function(e) {
      e.preventDefault();
      var i = ACC.storefinder.storeSearchData.page;
      ACC.storefinder.getStoreData(i - 1), t(i - 1);
    }), $(document).on("click", ".js-store-finder-pager-next", function(e) {
      e.preventDefault();
      var i = ACC.storefinder.storeSearchData.page;
      ACC.storefinder.getStoreData(i + 1), t(i + 1);
    });
  },
  bindStoreChange: function() {
    $(document).on("change", ".js-store-finder-input", function(t) {
      t.preventDefault(), storeData = ACC.storefinder.storeData.data;
      var e = $(this).data("id"), i = $(".js-store-finder-details");
      $.each(storeData[e], function(t, e) {
        if ("image" == t) "" != e ? i.find(".js-store-image").html('<img src="' + e + '" alt="" />') : i.find(".js-store-image").html(""); else if ("productcode" == t) i.find(".js-store-productcode").val(e); else if ("openings" == t) if ("" != e) {
          var n = i.find(".js-store-" + t), o = "";
          $.each(e, function(t, e) {
            o += "<dt>" + t + "</dt>", o += "<dd>" + e + "</dd>";
          }), n.html(o);
        } else i.find(".js-store-" + t).html(""); else if ("specialOpenings" == t) ; else if ("features" == t) {
          var s = "";
          $.each(e, function(t, e) {
            s += "<li>" + e + "</li>";
          }), i.find(".js-store-" + t).html(s);
        } else if ("phone" == t) {
          if ("" != e) {
            var a = e;
            i.find(".js-store-phone").html(a);
          }
        } else "" != e ? i.find(".js-store-" + t).html(e) : i.find(".js-store-" + t).html("");
      }), ACC.storefinder.storeId = storeData[e], ACC.storefinder.initGoogleMap();
    }), $(document).on("click", ".js-select-store-label", function(t) {
      $(".js-store-finder").addClass("show-store");
    }), $(document).on("click", ".js-back-to-storelist", function(t) {
      $(".js-store-finder").removeClass("show-store");
    });
  },
  initGoogleMap: function() {
    $(".js-store-finder-map").length > 0 && ACC.global.addGoogleMapsApi("ACC.storefinder.loadGoogleMap");
  },
  loadGoogleMap: function() {
    if (storeInformation = ACC.storefinder.storeId, $(".js-store-finder-map").length > 0) {
      $(".js-store-finder-map").attr("id", "store-finder-map");
      var t = new google.maps.LatLng(storeInformation.latitude, storeInformation.longitude), e = {
        zoom: 13,
        zoomControl: !0,
        panControl: !0,
        streetViewControl: !1,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: t
      }, i = new google.maps.Map(document.getElementById("store-finder-map"), e), n = new google.maps.Marker({
        position: new google.maps.LatLng(storeInformation.latitude, storeInformation.longitude),
        map: i,
        title: storeInformation.name,
        icon: "https://maps.google.com/mapfiles/markerA.png"
      }), o = new google.maps.InfoWindow({
        content: storeInformation.name,
        disableAutoPan: !0
      });
      google.maps.event.addListener(n, "click", function() {
        o.open(i, n);
      });
    }
  },
  bindSearch: function() {
    $(document).on("submit", "#storeFinderForm", function(t) {
      t.preventDefault();
      var e = $(".js-store-finder-search-input").val();
      if (e.length > 0) ACC.storefinder.getInitStoreData(e); else if ($(".js-storefinder-alert").length < 1) {
        var i = $(".searchBtn").data("searchEmpty");
        $(".js-store-finder").hide(), $("#storeFinder").before('<div class="js-storefinder-alert alert alert-danger alert-dismissable" ><button class="close" type="button" data-dismiss="alert" aria-hidden="true">×</button>' + i + "</div>");
      }
    }), $(".js-store-finder").hide(), $(document).on("click", "#findStoresNearMe", function(t) {
      t.preventDefault(), ACC.storefinder.getInitStoreData(null, ACC.storefinder.coords.latitude, ACC.storefinder.coords.longitude);
    });
  },
  getStoreData: function(t) {
    ACC.storefinder.storeSearchData.page = t, url = $(".js-store-finder").data("url"), 
    $.ajax({
      url: url,
      data: ACC.storefinder.storeSearchData,
      type: "get",
      success: function(t) {
        0 == t.startsWith("<") ? ($("#noPosFound").addClass("hide"), ACC.storefinder.storeData = $.parseJSON(t), 
        ACC.storefinder.refreshNavigation(), ACC.storefinder.storeData.total < 10 && $(".js-store-finder-pager-next").attr("disabled", "disabled")) : ($(".js-store-finder").hide(), 
        $("#noPosFound").removeClass("hide"));
      }
    });
  },
  getInitStoreData: function(t, e, i) {
    $(".alert").remove(), data = {
      q: "",
      page: 0
    }, null != t && (data.q = t), null != e && (data.latitude = e), null != i && (data.longitude = i), 
    ACC.storefinder.storeSearchData = data, ACC.storefinder.getStoreData(data.page), 
    $(".js-store-finder").show(), $(".js-store-finder-pager-prev").attr("disabled", "disabled"), 
    $(".js-store-finder-pager-next").removeAttr("disabled");
  },
  init: function() {
    $("#findStoresNearMe").attr("disabled", "disabled"), navigator.geolocation && navigator.geolocation.getCurrentPosition(function(t) {
      ACC.storefinder.coords = t.coords, $("#findStoresNearMe").removeAttr("disabled");
    }, function(t) {
      console.log("An error occurred... The error code and message are: " + t.code + "/" + t.message);
    }), setTimeout(function() {
      ACC.storefinder.getInitStoreData(null, 0, 0);
    }, 100);
  }
}, ACC.futurelink = {
  _autoload: [ "bindFutureStockLink" ],
  bindFutureStockLink: function() {
    $(document).on("click", ".futureStockLink", function(t) {
      t.preventDefault();
      var e = $(this).attr("href"), i = $(this).attr("title");
      ACC.colorbox.open(i, {
        href: e,
        maxWidth: "100%",
        width: "320px",
        height: "320px",
        initialWidth: "320px"
      });
    });
  }
}, ACC.productorderform = {
  _autoload: [ "headerActions", "coreTableActions", "addToCartOrderGridForm" ],
  $addToCartOrderForm: $("#AddToCartOrderForm"),
  $addToCartBtn: $("#addToCartBtn"),
  $omsErrorMessageContainer: $("#globalMessages"),
  $emptySkuQuantityInputs: $(".sku-quantity[value]"),
  $nonEmptySkuQuantityInputs: $(".sku-quantity[value]"),
  $totalGridValues: $("[data-grid-total-id]"),
  $futureTooltipTemplate: $("#future-stock-template"),
  $futureTooltipErrorTemplate: $("#future-tooltip-error-template"),
  $omsErrorMessageTemplate: $("#oms-error-message-template"),
  $variantSummaryTemplate: $("#variant-summary"),
  selectedVariantData: "selected-variant",
  selectedVariants: [],
  quantityTotal: 0,
  scrollTopPos: 0,
  headerActions: function() {
    ACC.productorderform.bindProductDetailToggle($(".product-details-toggle"));
  },
  coreTableActions: function() {
    ACC.productorderform.coreTableScrollActions(ACC.productorderform.$addToCartOrderForm), 
    ACC.productorderform.bindUpdateFutureStockButton(".update_future_stock_button"), 
    ACC.productorderform.bindHideFutureStockInfo(".hide_future_stock_info"), ACC.productorderform.bindVariantSelect($(".variant-select-btn"), "AddToCartOrderForm"), 
    ACC.productorderform.cancelVariantModal(".closeVariantModal"), ACC.productorderform.checkLimitExceed(".sku-quantity");
    var t = 0, e = 0;
    ACC.productorderform.$addToCartOrderForm.on("click", ".sku-quantity", function(t) {
      $(this).select();
    }), ACC.productorderform.$addToCartOrderForm.on("focusin", ".sku-quantity", function(e) {
      t = jQuery.trim(this.value), $(this).parents("tr").next(".variant-summary").remove(), 
      $(this).parents("table").data(ACC.productorderform.selectedVariantData) ? ACC.productorderform.selectedVariants = $(this).parents("table").data(ACC.productorderform.selectedVariantData) : ACC.productorderform.selectedVariants = [], 
      "" == t && (t = 0, this.value = 0);
    }), $(".sku-quantity").on("blur keypress", function(i) {
      var n = i.keyCode || i.which || i.charCode;
      if (13 == n || void 0 == n) {
        var o = parseInt($(this).attr("id").match("[0-9]+")), s = 0, a = this, r = $("input[id='productPrice[" + o + "]']").val();
        this.value = ACC.productorderform.filterSkuEntry(this.value);
        var l = $(".js-total-items-count"), c = l.html(), d = $(".js-total-price-value").val(), u = $(this).parents(".orderForm_grid_group");
        if (isNaN(jQuery.trim(this.value)) && (this.value = 0), "" == (e = jQuery.trim(this.value)) && (e = 0, 
        this.value = 0), ACC.orderform) void 0 !== sessionStorage.totalItems && void 0 !== sessionStorage.totalPriceVal && (c = sessionStorage.totalItems, 
        d = sessionStorage.totalPriceVal), 0 == t ? (l.html(parseInt(c) + parseInt(e)), 
        s = parseFloat(d) + parseFloat(r) * parseInt(e)) : (l.html(parseInt(c) + (parseInt(e) - parseInt(t))), 
        s = parseFloat(d) + parseFloat(r) * (parseInt(e) - parseInt(t))), sessionStorage.totalPrice = ACC.productorderform.formatTotalsCurrency(s), 
        sessionStorage.totalItems = l.html(), sessionStorage.totalPriceVal = s, ACC.orderform.addToSkuQtyInput(a); else if (u && u.length > 0) {
          var h = u.find("#quantityValue"), p = u.find("#avgPriceValue"), f = u.find("#subtotalValue"), m = h.val(), g = f.val();
          0 == t ? (h.val(parseInt(m) + parseInt(e)), f.val(parseFloat(g) + parseFloat(r) * parseInt(e)), 
          l.html(parseInt(c) + parseInt(e)), s = parseFloat(d) + parseFloat(r) * parseInt(e)) : (h.val(parseInt(m) + (parseInt(e) - parseInt(t))), 
          f.val(parseFloat(g) + parseFloat(r) * (parseInt(e) - parseInt(t))), l.html(parseInt(c) + (parseInt(e) - parseInt(t))), 
          s = parseFloat(d) + parseFloat(r) * (parseInt(e) - parseInt(t))), ACC.productorderform.enableBeforeUnloadEvent(e, l.text()), 
          0 != l.length && 0 == l.text() ? (ACC.productorderform.$addToCartBtn.attr("disabled", "disabled"), 
          $(window).off("beforeunload", ACC.productorderform.beforeUnloadHandler)) : ACC.productorderform.$addToCartBtn.removeAttr("disabled"), 
          parseInt(h.val()) > 0 ? p.val(parseFloat(f.val()) / parseInt(h.val())) : p.val(0);
        }
        if (u && u.length > 0) {
          var v = "", b = u.find("[data-grid-total-id=total_value_" + o + "]");
          e > 0 && (v = ACC.productorderform.formatTotalsCurrency(parseFloat(r) * parseInt(e))), 
          b.html(v), ACC.productorderform.updateSelectedVariantGridTotal(this, t, !1, !1);
        }
        $(".js-total-price").html(ACC.productorderform.formatTotalsCurrency(s)), $(".js-total-price-value").val(s);
      }
    }), $("body").on("focusin", "#cboxContent .sku-quantity", function() {
      t = jQuery.trim(this.value);
      var e = $(this).data("variant-id"), i = $("#AddToCartOrderForm, #cartOrderGridForm").find("[data-variant-id='" + e + "']");
      i.trigger("focusin"), i.parents("table").find(".variant-summary").remove(), i.parents("table").data(ACC.productorderform.selectedVariantData) ? ACC.productorderform.selectedVariants = i.parents("table").data(ACC.productorderform.selectedVariantData) : ACC.productorderform.selectedVariants = [], 
      "" == t && (t = 0, this.value = 0);
    }), $("body").on("blur", "#cboxContent .sku-quantity", function() {
      var e = $(this).siblings(".price"), i = $(this).siblings(".data-grid-total"), n = $(this).data("variant-id"), o = $("#AddToCartOrderForm, #cartOrderGridForm").find("[data-variant-id='" + n + "']");
      this.value = ACC.productorderform.filterSkuEntry(this.value), (isNaN(jQuery.trim(this.value)) || this.value < 0 || "" == this.value) && (this.value = 0), 
      o.val(this.value), o.trigger("blur"), ACC.productorderform.updateVariantTotal(e, this.value, i), 
      this.value > 0 && this.value != t ? (o.parents("table").addClass("selected"), o.trigger("change")) : 0 === ACC.productorderform.selectedVariants.length && o.parents("table").removeClass("selected");
    });
  },
  updateSelectedVariantGridTotal: function(t, e, i, n) {
    var o = $(t).siblings(".price"), s = $(t).siblings(".variant-prop"), a = $(t).next(".td_stock").data("sku-id"), r = $(t).siblings(".data-grid-total");
    if (i && (ACC.productorderform.selectedVariants = []), t.value != e) {
      var l = !0;
      ACC.productorderform.selectedVariants.forEach(function(e, i) {
        e.id === a && (l = !1, "0" === t.value || 0 === t.value ? ACC.productorderform.selectedVariants.splice(i, 1) : (ACC.productorderform.selectedVariants[i].quantity = t.value, 
        ACC.productorderform.selectedVariants[i].total = ACC.productorderform.updateVariantTotal(o, t.value, r)));
      }), l && t.value > 0 && ACC.productorderform.selectedVariants.push({
        id: a,
        size: s.data("variant-prop"),
        quantity: t.value,
        total: ACC.productorderform.updateVariantTotal(o, t.value, r)
      });
    }
    n && $(t).parents("table").find(".variant-summary").remove(), ACC.productorderform.showSelectedVariant($(t).parents("table")), 
    t.value > 0 && t.value != e ? $(t).parents("table").addClass("selected") : 0 === ACC.productorderform.selectedVariants.length && $(t).parents("table").removeClass("selected").find(".variant-summary").remove();
  },
  updateVariantTotal: function(t, e, i) {
    var n = parseFloat(t.data("variant-price")) * parseInt(e);
    return i.html(ACC.productorderform.formatTotalsCurrency(n)), ACC.productorderform.formatTotalsCurrency(n);
  },
  bindUpdateFutureStockButton: function(t) {
    $("body").on("click", t, function(t) {
      t.preventDefault();
      var e = $(this).parents(".orderForm_grid_group").find(".product-grid-container"), i = jQuery.map(e.find("input[type='hidden'].sku"), function(t) {
        return t.value;
      }), n = $(this).data("skusId"), o = $(this).data("skusFutureStockUrl"), s = {
        skus: i,
        productCode: n
      }, a = $(this).parent().find(".hide_future_stock_info"), r = $(this);
      $.ajax({
        url: o,
        type: "POST",
        data: s,
        traditional: !0,
        dataType: "json",
        success: function(t) {
          ACC.productorderform.updateFuture(e, i, t, n, r, a);
        },
        error: function(t, e, i) {
          alert("Failed to get delivery modes. Error details [" + t + ", " + e + ", " + i + "]");
        }
      });
    });
  },
  bindHideFutureStockInfo: function(t) {
    $("body").on("click", t, function(t) {
      t.preventDefault();
      var e = $(this).parent().parent().find(".product-grid-container");
      $(this).parent().find(".update_future_stock_button").show(), $(this).hide(), e.find("[data-sku-id]").children(".future_stock, .out-of-stock").remove();
    });
  },
  updateFuture: function(t, e, i, n, o, s) {
    ACC.productorderform.$omsErrorMessageContainer.find("div").remove(), null !== i && void 0 !== i["basket.page.viewFuture.unavailable"] ? $.tmpl(ACC.productorderform.$omsErrorMessageTemplate, {
      errorMessage: i["basket.page.viewFuture.unavailable"]
    }).appendTo(ACC.productorderform.$omsErrorMessageContainer) : function(t) {
      return Object.keys(t).length <= 0;
    }(i) || (o.hide(), s.css("display", "block"), $.each(e, function(e, n) {
      var o = i[n], s = t.find("[data-sku-id='" + n + "']"), a = -1 != s[0].attributes.class.nodeValue.indexOf("in-stock"), r = void 0 !== o && null !== o && null !== o[0] && void 0 !== o[0];
      s.children(".future_stock, .out-of-stock").remove(), r ? (a || s.addClass("future-stock"), 
      $.tmpl(ACC.productorderform.$futureTooltipTemplate, {
        formattedDate: o[0].formattedDate,
        availabilities: o
      }).appendTo(s)) : a || (s[0].attributes.class.nodeValue = "td_stock out-of-stock");
    }));
  },
  toJSON: function(t, e) {
    for (var i = t.find("input.sku").map(function(t, e) {
      return e.value;
    }), n = t.find("input.sku-quantity").map(function(t, e) {
      return parseInt(e.value);
    }), o = [], s = 0; s < i.length; s++) e && 0 === n[s] || o.push({
      product: {
        code: i[s]
      },
      quantity: n[s]
    });
    return JSON.stringify({
      cartEntries: o
    });
  },
  formatTotalsCurrency: function(t) {
    return Currency.formatMoney(Number(t).toFixed(2), Currency.money_format[ACC.common.currentCurrency]);
  },
  cleanValues: function() {
    if (0 !== $(".orderForm_grid_group").length) {
      var t = ACC.productorderform.formatTotalsCurrency("0.00");
      $(".js-total-price").html(t), $("#quantity, .js-total-items-count").html(0), $("#quantityValue, #avgPriceValue, #subtotalValue, .js-total-price-value").val(0), 
      ACC.productorderform.$emptySkuQuantityInputs.val(0), ACC.productorderform.$totalGridValues.html("");
    }
  },
  calculateGrid: function() {
    ACC.productorderform.$nonEmptySkuQuantityInputs.trigger("focusout");
  },
  bindProductDetailToggle: function(t) {
    t.on("click", function(t) {
      t.preventDefault(), $(this).parents(".product-details").toggleClass("open");
    });
  },
  showSelectedVariant: function(t) {
    $.tmpl(ACC.productorderform.$variantSummaryTemplate, {
      variants: ACC.productorderform.selectedVariants
    }).appendTo(t), $(".variant-summary .variant-property").html($(".variant-detail").data("variant-property")), 
    t.data(ACC.productorderform.selectedVariantData, ACC.productorderform.selectedVariants), 
    t.removeClass("currentVariant");
  },
  bindVariantSelect: function(t, e) {
    t.on("click", function(i) {
      i.preventDefault();
      var n = $(this).parents("table");
      n.data(ACC.productorderform.selectedVariantData) ? ACC.productorderform.selectedVariants = n.data(ACC.productorderform.selectedVariantData) : ACC.productorderform.selectedVariants = [];
      var o = t.html(), s = $("#" + e).clone().empty().attr("id", e + "Variant");
      n.addClass("currentVariant");
      var a = $(this).parents(".orderForm_grid_group").clone();
      n.removeClass("currentVariant"), $(a).find(".currentVariant").siblings().remove(), 
      s.html(a), s.find(".hidden-xs").removeClass("hidden-xs"), s.find(".hide").removeClass("hide"), 
      scrollTopPos = $("body").scrollTop(), $("body").scrollTop(0), ACC.colorbox.open(o, {
        html: s,
        width: "100%",
        reposition: !1,
        className: "variantSelectMobile",
        onCleanup: function() {
          ACC.productorderform.$addToCartOrderForm.find(".selected").length > 0 ? ACC.productorderform.$addToCartBtn.removeAttr("disabled") : ACC.productorderform.$addToCartBtn.attr("disabled", "disabled"), 
          $("body").scrollTop(scrollTopPos);
        }
      });
    });
  },
  cancelVariantModal: function(t) {
    $("body").on("click", t, function(t) {
      t.preventDefault(), ACC.colorbox.close();
    });
  },
  checkLimitExceed: function(t) {
    $("body").on("keyup blur", t, function(t) {
      var e = Number($(this).val()), i = Number($(this).attr("data-instock"));
      e > i && $(this).val(i);
    });
  },
  resetSelectedVariant: function() {
    ACC.productorderform.selectedVariants = [], $(".product-grid-container table").removeData(ACC.productorderform.selectedVariantData).removeClass("selected").removeClass("currentVariant");
  },
  addToCartOrderGridForm: function() {
    $("#AddToCartOrderForm").keypress(function(t) {
      "13" == t.which && t.preventDefault();
    }), ACC.productorderform.$addToCartBtn.click(function() {
      ACC.productorderform.$addToCartBtn.attr("disabled", "disabled"), $.ajax({
        url: ACC.productorderform.$addToCartOrderForm.attr("action"),
        type: "POST",
        dataType: "json",
        contentType: "application/json",
        data: ACC.productorderform.toJSON(ACC.productorderform.$addToCartOrderForm, !0),
        async: !1,
        success: function(t) {
          $(window).off("beforeunload", ACC.productorderform.beforeUnloadHandler), ACC.product.displayAddToCartPopup(t), 
          ACC.productorderform.cleanValues(), ACC.productorderform.resetSelectedVariant();
        },
        error: function(t, e, i) {
          console.log("The following error occured: " + e, i);
        }
      });
    });
  },
  beforeUnloadHandler: function() {
    return ACC.productorderform.$addToCartOrderForm.attr("data-grid-confirm-message");
  },
  enableBeforeUnloadEvent: function(t, e) {
    ACC.orderform || t > 0 && e > 0 && $(window).off("beforeunload", ACC.productorderform.beforeUnloadHandler).on("beforeunload", ACC.productorderform.beforeUnloadHandler);
  },
  filterSkuEntry: function(t) {
    return /\D/g.test(t) ? t.replace(/\D/g, "") : t;
  },
  coreTableScrollActions: function(t) {
    if (t.hasClass("visible")) {
      ACC.productorderform.orderGridScroll(t);
      var e = t.parent().find(".order-form-scroll.right"), i = t.parent().find(".order-form-scroll.left"), n = t.parent().find(".order-form-scroll.up"), o = t.parent().find(".order-form-scroll.down"), s = t.find(".widthReference").outerWidth(), a = t.find(".product-grid-container table").eq(0).height() / 2, r = 0, l = 0, c = 0, d = 0;
      t.find(".product-grid-container table").each(function() {
        $(this).outerWidth() > r && (r = $(this).outerWidth());
      }), t.find(".orderForm_grid_group").each(function() {
        l += $(this).height();
      }), c = r - t.outerWidth(), d = l - t.height() + 14, t.scroll(function() {
        $(this).scrollLeft() > 0 ? i.show() : i.hide(), $(this).scrollLeft() >= c ? e.hide() : e.show(), 
        $(this).scrollTop() > 0 ? n.show() : n.hide(), $(this).scrollTop() >= d ? o.hide() : o.show(), 
        t.find(".update-future-stock").css("margin-right", -$(this).scrollLeft());
      }), t.parent().find(".order-form-scroll").click(function() {
        var e = {
          left: t.scrollLeft(),
          top: t.scrollTop()
        };
        $(this).hasClass("right") ? t.scrollLeft(e.left + s) : $(this).hasClass("left") ? t.scrollLeft(e.left - s) : $(this).hasClass("up") ? t.scrollTop(e.top - a) : t.scrollTop(e.top + a);
      });
    }
  },
  orderGridScroll: function(t) {
    var e = !1, i = 0, n = $(t).find(".orderForm_grid_group").innerWidth(), o = $(t).innerHeight() - 18;
    $(t).find(".product-grid-container table").each(function() {
      $(this).width() > n && (e = !0), i += $(this).height();
    }), e && $(t).parent().find(".order-form-scroll.right").show(), i > o && $(t).parent().find(".order-form-scroll.down").show();
  },
  calculateVariantTotal: function(t, e) {
    var i = t.parents(".orderForm_grid_group"), n = parseInt(t.attr("id").match("[0-9]+")), o = $("input[id='productPrice[" + n + "]']").val(), s = i.find("[data-grid-total-id=total_value_" + n + "]");
    e > 0 && s.html(ACC.productorderform.formatTotalsCurrency(parseFloat(o) * parseInt(e)));
  }
}, ACC.savedcarts = {
  _autoload: [ [ "bindRestoreSavedCartClick", 0 != $(".js-restore-saved-cart").length ], [ "bindDeleteSavedCartLink", 0 != $(".js-delete-saved-cart").length ], [ "bindDeleteConfirmLink", 0 != $(".js-savedcart_delete_confirm").length ], [ "bindSaveCartForm", 0 != $(".js-save-cart-link").length || 0 != $(".js-update-saved-cart").length ], [ "bindUpdateUploadingSavedCarts", 0 != $(".js-uploading-saved-carts-update").length ] ],
  $savedCartRestoreBtn: {},
  $currentCartName: {},
  bindRestoreSavedCartClick: function() {
    $(".js-restore-saved-cart").click(function(t) {
      t.preventDefault();
      var e = $(this).data("restore-popup-title"), i = $(this).data("savedcart-id"), n = ACC.config.encodedContextPath + "/my-account/saved-carts/" + i + "/restore";
      $.get(n).done(function(t) {
        ACC.colorbox.open(e, {
          html: t,
          width: 500,
          onComplete: function() {
            ACC.common.refreshScreenReaderBuffer(), ACC.savedcarts.bindRestoreModalHandlers(), 
            ACC.savedcarts.bindPostRestoreSavedCartLink();
          },
          onClosed: function() {
            ACC.common.refreshScreenReaderBuffer();
          }
        });
      });
    });
  },
  bindRestoreModalHandlers: function() {
    ACC.savedcarts.$savedCartRestoreBtn = $(".js-save-cart-restore-btn"), ACC.savedcarts.$currentCartName = $(".js-current-cart-name"), 
    $(".js-prevent-save-active-cart").on("change", function(t) {
      if (!0 === $(this).prop("checked")) ACC.savedcarts.$currentCartName.attr("disabled", "disabled"), 
      ACC.savedcarts.$savedCartRestoreBtn.removeAttr("disabled"); else {
        ACC.savedcarts.$currentCartName.removeAttr("disabled");
        var e = ACC.savedcarts.$currentCartName.val();
        "" == e && 0 === e.length && ACC.savedcarts.$savedCartRestoreBtn.attr("disabled", "disabled");
      }
    }), ACC.savedcarts.$currentCartName.on("focus", function(t) {
      $(".js-restore-current-cart-form").removeClass("has-error"), $(".js-restore-error-container").html("");
    }), ACC.savedcarts.$currentCartName.on("blur", function(t) {
      "" == this.value && 0 === this.value.length ? ACC.savedcarts.$savedCartRestoreBtn.attr("disabled", "disabled") : ACC.savedcarts.$savedCartRestoreBtn.removeAttr("disabled");
    });
  },
  bindPostRestoreSavedCartLink: function() {
    var t = !0, e = !1;
    $(document).on("click", ".js-keep-restored-cart", function(e) {
      t = $(this).prop("checked");
    }), $(document).on("click", ".js-prevent-save-active-cart", function(t) {
      e = $(this).prop("checked");
    }), $(document).on("click", ".js-save-cart-restore-btn", function(i) {
      i.preventDefault();
      var n = $("#activeCartName").val(), o = $(this).data("restore-url"), s = {
        preventSaveActiveCart: e,
        keepRestoredCart: t,
        cartName: n
      };
      $.post(o, s).done(function(t, e, i) {
        if ("200" == t) {
          var n = ACC.config.encodedContextPath + "/cart";
          window.location.replace(n);
        } else {
          var o = i.responseText.slice(1, -1);
          $(".js-restore-current-cart-form").addClass("has-error"), $(".js-restore-error-container").html(o), 
          $(".js-savedcart_restore_confirm_modal").colorbox.resize();
        }
      });
    }), $(document).on("click", ".js-cancel-restore-btn", function(t) {
      ACC.colorbox.close();
    });
  },
  bindDeleteSavedCartLink: function() {
    $(document).on("click", ".js-delete-saved-cart", function(t) {
      t.preventDefault();
      var e = $(this).data("savedcart-id"), i = $(this).data("delete-popup-title");
      ACC.colorbox.open(i, {
        inline: !0,
        className: "js-savedcart_delete_confirm_modal",
        href: "#popup_confirm_savedcart_delete_" + e,
        width: "500px",
        onComplete: function() {
          $(this).colorbox.resize();
        }
      });
    });
  },
  bindDeleteConfirmLink: function() {
    $(document).on("click", ".js-savedcart_delete_confirm", function(t) {
      t.preventDefault();
      var e = $(this).data("savedcart-id"), i = ACC.config.encodedContextPath + "/my-account/saved-carts/" + e + "/delete";
      $.ajax({
        url: i,
        type: "DELETE",
        success: function(t) {
          ACC.colorbox.close();
          var e = ACC.config.encodedContextPath + "/my-account/saved-carts";
          window.location.replace(e);
        }
      });
    }), $(document).on("click", ".js-savedcart_delete_confirm_cancel", function(t) {
      ACC.colorbox.close();
    });
  },
  bindSaveCartForm: function() {
    ACC.savedcarts.charactersLeftInit();
    var t = $("#saveCartForm"), e = !1, i = function() {
      var i = $("#saveCart").data("saveCartTitle");
      ACC.colorbox.open(i, {
        href: "#saveCart",
        inline: !0,
        width: "620px",
        onOpen: function() {
          $("#saveCartName").val() && ACC.savedcarts.disableSaveCartButton(!1);
        },
        onComplete: function() {
          $(this).colorbox.resize(), e = !1;
        },
        onClosed: function() {
          e && t.submit(), document.getElementById("saveCartForm").reset(), ACC.savedcarts.disableSaveCartButton(!0), 
          ACC.savedcarts.charactersLeftInit();
        }
      });
    };
    $(document).on("click", ".js-save-cart-link, .js-update-saved-cart", function(t) {
      t.preventDefault(), ACC.common.checkAuthenticationStatusBeforeAction(i);
    }), $(document).on("click", "#saveCart #cancelSaveCartButton", function(t) {
      t.preventDefault(), $.colorbox.close();
    }), $("#saveCartName").keyup(function() {
      $("#saveCart #saveCartButton").prop("disabled", "" == this.value.trim());
      var t = $("#localized_val").attr("value"), e = $(this).val().length;
      remain = 255 - parseInt(e), $("#remain").text(t + " : " + remain);
    }), $("#saveCartDescription").keyup(function() {
      var t = $("#localized_val").attr("value"), e = $(this).val().length;
      remain = 255 - parseInt(e), $("#remainTextArea").text(t + " : " + remain);
    }), $(document).on("click", "#saveCart #saveCartButton", function(t) {
      t.preventDefault(), e = !0, $.colorbox.close();
    });
  },
  disableSaveCartButton: function(t) {
    $("#saveCart #saveCartButton").prop("disabled", t);
  },
  charactersLeftInit: function() {
    $("#remain").text($("#localized_val").attr("value") + " : 255"), $("#remainTextArea").text($("#localized_val").attr("value") + " : 255");
  },
  bindUpdateUploadingSavedCarts: function() {
    var t = $(".js-uploading-saved-carts-update").data("idRowMapping"), e = $(".js-uploading-saved-carts-update").data("refreshCart");
    if (t && e) {
      var n = $(".js-uploading-saved-carts-update").data("refreshInterval"), o = t.split(","), s = new Object(), a = [];
      for (i = 0; i < o.length; i++) {
        var r = o[i].split(":");
        "" != r && (s[r[0]] = r[1], a.push(r[0]));
      }
      a.length > 0 && setTimeout(function() {
        ACC.savedcarts.refreshWorker(a, s, n);
      }, n);
    }
  },
  refreshWorker: function(t, e, n) {
    $.ajax({
      dataType: "json",
      url: ACC.config.encodedContextPath + "/my-account/saved-carts/uploadingCarts",
      data: {
        cartCodes: t
      },
      type: "GET",
      traditional: !0,
      success: function(o) {
        if (void 0 != o) {
          for (i = 0; i < o.length; i++) {
            var s = o[i], a = $.inArray(s.code, t);
            a > -1 && t.splice(a, 1);
            var r = e[s.code];
            if (void 0 != r) {
              var l = "#row-" + r;
              $(l + " .js-saved-cart-name").removeClass("not-active"), $(l + " .js-saved-cart-date").removeClass("hidden"), 
              $(l + " .js-file-importing").remove(), $(l + " .js-saved-cart-description").text(s.description);
              var c = s.entries.length;
              $(l + " .js-saved-cart-number-of-items").text(c), $(l + " .js-saved-cart-total").text(s.totalPrice.formattedValue), 
              c > 0 && $(l + " .js-restore-saved-cart").removeClass("hidden"), $(l + " .js-delete-saved-cart").removeClass("hidden");
            }
          }
        }
        t.length > 0 && setTimeout(function() {
          ACC.savedcarts.refreshWorker(t, e, n);
        }, n);
      }
    });
  }
}, ACC.multidgrid = {
  populateAndShowGridOverlay: function(t, e) {
    e.preventDefault();
    var i = $(t).data("index"), n = $("#grid" + i), o = n.data("sub-entries"), s = n.data("product-name"), a = o.split(",")[0].split(":")[0], r = n.data("target-url") + "?productCode=" + a;
    ACC.colorbox.open(s, {
      href: r,
      className: "read-only-grid",
      close: '<span class="glyphicon glyphicon-remove"></span>',
      width: window.innerWidth > parseInt(cboxOptions.maxWidth) ? cboxOptions.maxWidth : cboxOptions.width,
      height: window.innerHeight > parseInt(cboxOptions.maxHeight) ? cboxOptions.maxHeight : cboxOptions.height,
      onComplete: function() {
        $("body").addClass("offcanvas");
        var t = $("#cboxLoadedContent").height();
        $("#cboxLoadedContent").height(t - $("#cboxTitle").height() + "px");
      },
      onClosed: function() {
        $("body").removeClass("offcanvas");
      }
    });
  },
  populateAndShowGrid: function(t, e, i) {
    var n = $(t).data("index");
    grid = $("#ajaxGrid" + n);
    var o = $("#grid" + n);
    if ($(t).toggleClass("open"), grid.is(":hidden")) if ("" == grid.html()) {
      var s = o.data("sub-entries").split(",")[0].split(":")[0], a = o.data("target-url");
      $.ajax({
        url: a,
        data: {
          productCode: s
        },
        type: "GET",
        success: function(t) {
          grid.html(t), grid.slideDown("slow");
        },
        error: function(t, e, i) {
          alert("Failed to get variant matrix. Error details [" + t + ", " + e + ", " + i + "]");
        }
      });
    } else grid.slideToggle("slow"); else grid.slideUp();
  }
};

var ACC = ACC || {};

$("#quickOrder").length > 0 && (ACC.quickorder = {
  _autoload: [ "bindClearQuickOrderRow", "bindAddSkuInputRow", "bindResetFormBtn", "bindAddToCartClick" ],
  $quickOrderContainer: $(".js-quick-order-container"),
  $quickOrderMinRows: Number($(".js-quick-order-container").data("quickOrderMinRows")),
  $quickOrderMaxRows: Number($(".js-quick-order-container").data("quickOrderMaxRows")),
  $productExistsInFormMsg: $(".js-quick-order-container").data("productExistsInFormMsg"),
  $quickOrderLeavePageMsg: $("#quickOrder").data("gridConfirmMessage"),
  $hiddenSkuInput: "input.js-hidden-sku-field",
  $addToCartBtn: $("#js-add-to-cart-quick-order-btn-top, #js-add-to-cart-quick-order-btn-bottom"),
  $resetFormBtn: $("#js-reset-quick-order-form-btn-top, #js-reset-quick-order-form-btn-bottom"),
  $productInfoContainer: ".js-product-info",
  $skuInputField: ".js-sku-input-field",
  $qtyInputField: ".js-quick-order-qty",
  $jsLiContainer: "li.js-li-container",
  $removeQuickOrderRowBtn: ".js-remove-quick-order-row",
  $skuValidationContainer: ".js-sku-validation-container",
  $qtyValidationContainer: ".js-qty-validation-container",
  $productItemTotal: ".js-quick-order-item-total",
  $classHasError: "has-error",
  bindResetFormBtn: function() {
    ACC.quickorder.$resetFormBtn.on("click", ACC.quickorder.clearForm);
  },
  bindAddToCartClick: function() {
    ACC.quickorder.$addToCartBtn.on("click", ACC.quickorder.addToCart);
  },
  bindAddSkuInputRow: function() {
    $(ACC.quickorder.$skuInputField).on("focusin", ACC.quickorder.addInputRow).on("focusout keydown", ACC.quickorder.handleFocusOutOnSkuInput);
  },
  bindClearQuickOrderRow: function() {
    $(ACC.quickorder.$removeQuickOrderRowBtn).on("mousedown", ACC.quickorder.clearQuickOrderRow);
  },
  addToCart: function() {
    $.ajax({
      url: ACC.quickorder.$quickOrderContainer.data("quickOrderAddToCartUrl"),
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: ACC.quickorder.getJSONDataForAddToCart(),
      async: !1,
      success: function(t) {
        ACC.quickorder.handleAddToCartSuccess(t);
      },
      error: function(t, e, i) {
        console.log("The following error occurred: " + e, i);
      }
    });
  },
  handleAddToCartSuccess: function(t) {
    $(t.quickOrderErrorData).size() > 0 && ACC.quickorder.disableBeforeUnloadEvent();
    var e = {};
    t.quickOrderErrorData.forEach(function(t) {
      e[t.sku] = t.errorMsg;
    }), $(ACC.quickorder.$qtyInputField).each(function() {
      var t = ACC.quickorder.getCurrentParentLi(this), i = ACC.quickorder.findElement(t, ACC.quickorder.$skuInputField).val(), n = e[i];
      n ? ACC.quickorder.findElement(t, ACC.quickorder.$skuValidationContainer).text(n) : ACC.quickorder.findElement(t, ACC.quickorder.$removeQuickOrderRowBtn).trigger("mousedown");
    }), ACC.quickorder.handleBeforeUnloadEvent(), ACC.product.displayAddToCartPopup(t);
  },
  getJSONDataForAddToCart: function() {
    var t = [];
    return $(ACC.quickorder.$qtyInputField).each(function() {
      var e = Number($(this).val());
      if (e > 0) {
        var i = jQuery.trim(ACC.quickorder.findElementInCurrentParentLi(this, ACC.quickorder.$skuInputField).val());
        t.push({
          product: {
            code: i
          },
          quantity: e
        });
      }
    }), JSON.stringify({
      cartEntries: t
    });
  },
  handleFocusOutOnSkuInput: function(t) {
    13 == (t.charCode ? t.charCode : t.keyCode ? t.keyCode : 0) && $(t.target).focusout(), 
    "focusout" == t.type && (ACC.quickorder.handleGetProduct(t), ACC.quickorder.handleBeforeUnloadEvent());
  },
  handleFocusOutOnQtyInput: function(t) {
    13 == (t.charCode ? t.charCode : t.keyCode ? t.keyCode : 0) && (t.preventDefault(), 
    ACC.quickorder.getCurrentParentLi(t.target).next().find(ACC.quickorder.$skuInputField).focus(), 
    $(t.target).focusout()), "focusout" == t.type && (ACC.quickorder.validateAndUpdateItemTotal(t), 
    ACC.quickorder.enableDisableAddToCartBtn());
  },
  clearForm: function() {
    window.location.reload();
  },
  validateAndUpdateItemTotal: function(t) {
    var e = ACC.quickorder.getCurrentParentLi(t.target), i = jQuery.trim(ACC.productorderform.filterSkuEntry($(t.target).val()));
    if (isNaN(i) || "" == i) i = 0, $(t.target).removeClass(ACC.quickorder.$classHasError), 
    ACC.quickorder.findElement(e, ACC.quickorder.$qtyValidationContainer).text(""), 
    $(t.target).val(0); else {
      i = Number(i), $(t.target).val(i);
      var n = jQuery.trim(ACC.quickorder.findElement(e, ACC.quickorder.$qtyInputField).data("maxProductQty")), o = jQuery.trim(ACC.quickorder.findElement(e, ACC.quickorder.$qtyInputField).data("stockLevelStatus"));
      if (n = $.isEmptyObject(n) && "inStock" == o ? "FORCE_IN_STOCK" : Number(n), !isNaN(n) && i > n) {
        $(t.target).addClass(ACC.quickorder.$classHasError);
        var s = ACC.quickorder.findElement(e, ACC.quickorder.$qtyValidationContainer);
        s.text(s.data("maxProductQtyMsg")), i = n, $(t.target).val(n);
      } else $(t.target).removeClass(ACC.quickorder.$classHasError), ACC.quickorder.findElement(e, ACC.quickorder.$qtyValidationContainer).text("");
    }
    if (i > 0) {
      var a = parseFloat(ACC.quickorder.findElement(e, ".js-product-price").data("productPrice"));
      ACC.quickorder.findElement(e, ACC.quickorder.$productItemTotal).html(ACC.productorderform.formatTotalsCurrency(a * i));
    } else ACC.quickorder.findElement(e, ACC.quickorder.$productItemTotal).text("");
  },
  clearQuickOrderRow: function() {
    var t = ACC.quickorder.$quickOrderMinRows, e = ACC.quickorder.getCurrentParentLi(this);
    $(".js-ul-container li.js-li-container").length > t ? (e.remove(), ACC.quickorder.bindClearQuickOrderRow()) : (ACC.quickorder.findElement(e, ACC.quickorder.$productInfoContainer).remove(), 
    ACC.quickorder.findElement(e, ACC.quickorder.$skuValidationContainer).text(""), 
    ACC.quickorder.findElement(e, ACC.quickorder.$skuInputField).val(""), ACC.quickorder.findElement(e, ACC.quickorder.$hiddenSkuInput).val("")), 
    ACC.quickorder.enableDisableAddToCartBtn(), ACC.quickorder.handleBeforeUnloadEvent();
  },
  addInputRow: function(t) {
    if ($(".js-quick-order-container li.js-li-container:last-child").find(ACC.quickorder.$skuInputField).is($(t.target)) && $(ACC.quickorder.$jsLiContainer).length < ACC.quickorder.$quickOrderMaxRows) {
      var e = $(".js-quick-order-container li.js-li-container:first").clone();
      ACC.quickorder.findElement(e, ACC.quickorder.$productInfoContainer).remove(), ACC.quickorder.findElement(e, ACC.quickorder.$skuValidationContainer).text(""), 
      ACC.quickorder.findElement(e, ACC.quickorder.$hiddenSkuInput).val("");
      var i = ACC.quickorder.findElement(e, ACC.quickorder.$skuInputField);
      i.val(""), i.focusin(ACC.quickorder.addInputRow).focusout(ACC.quickorder.handleFocusOutOnSkuInput).keydown(ACC.quickorder.handleFocusOutOnSkuInput), 
      ACC.quickorder.findElement(e, ACC.quickorder.$removeQuickOrderRowBtn).click(ACC.quickorder.clearQuickOrderRow), 
      $(".js-ul-container").append(e);
    }
  },
  handleGetProduct: function(t) {
    var e = ACC.quickorder.getCurrentParentLi(t.target), i = $.trim(t.target.value);
    $(t.target).val(i), ACC.quickorder.isCurrentSkuSameAsPrevious(e, i) || (i.length > 0 ? (ACC.quickorder.findElement(e, ACC.quickorder.$productInfoContainer).remove(), 
    ACC.quickorder.isDuplicateSku(t.target, i) ? ACC.quickorder.findElement(e, ACC.quickorder.$skuValidationContainer).text(ACC.quickorder.$productExistsInFormMsg) : ACC.quickorder.getAndDisplayProductInfo(t, e, i), 
    ACC.quickorder.findElement(e, ACC.quickorder.$hiddenSkuInput).val(i)) : ($(t.target).removeClass(ACC.quickorder.$classHasError), 
    ACC.quickorder.findElement(e, ACC.quickorder.$skuValidationContainer).text(""), 
    ACC.quickorder.findElement(e, ACC.quickorder.$productInfoContainer).remove()));
  },
  isCurrentSkuSameAsPrevious: function(t, e) {
    return ACC.quickorder.findElement(t, ACC.quickorder.$hiddenSkuInput).val() == e;
  },
  isDuplicateSku: function(t, e) {
    var i = !1;
    return $(ACC.quickorder.$skuInputField).each(function() {
      if ($(this).val() == e && !$(this).is($(t))) return i = !0, !1;
    }), i;
  },
  getAndDisplayProductInfo: function(t, e, i) {
    var n = ACC.config.encodedContextPath + "/quickOrder/productInfo?code=" + i;
    $.getJSON(n, function(i) {
      if (null != i.errorMsg && i.errorMsg.length > 0) $(t.target).addClass(ACC.quickorder.$classHasError), 
      ACC.quickorder.findElement(e, ACC.quickorder.$skuValidationContainer).text(i.errorMsg); else {
        $(t.target).removeClass(ACC.quickorder.$classHasError), ACC.quickorder.findElement(e, ACC.quickorder.$skuValidationContainer).text(""), 
        $("#quickOrderRowTemplate").tmpl(i.productData).insertAfter(ACC.quickorder.findElement(e, ".js-sku-container"));
        var n = ACC.quickorder.findElement(e, ACC.quickorder.$qtyInputField);
        n.focusout(ACC.quickorder.handleFocusOutOnQtyInput).keydown(ACC.quickorder.handleFocusOutOnQtyInput), 
        "outOfStock" == i.productData.stock.stockLevelStatus.code ? (n.val(0), n.prop("disabled", !0)) : n.focus().select(), 
        ACC.quickorder.enableDisableAddToCartBtn();
      }
    });
  },
  handleBeforeUnloadEvent: function() {
    ACC.quickorder.isAnySkuPresent() ? (ACC.quickorder.disableBeforeUnloadEvent(), ACC.quickorder.enableBeforeUnloadEvent()) : ACC.quickorder.disableBeforeUnloadEvent();
  },
  disableBeforeUnloadEvent: function() {
    $(window).off("beforeunload", ACC.quickorder.beforeUnloadHandler);
  },
  enableBeforeUnloadEvent: function() {
    $(window).on("beforeunload", ACC.quickorder.beforeUnloadHandler);
  },
  beforeUnloadHandler: function() {
    return ACC.quickorder.$quickOrderLeavePageMsg;
  },
  enableDisableAddToCartBtn: function() {
    ACC.quickorder.shouldAddToCartBeEnabled() ? ACC.quickorder.$addToCartBtn.removeAttr("disabled") : ACC.quickorder.$addToCartBtn.attr("disabled", "disabled");
  },
  shouldAddToCartBeEnabled: function() {
    var t = 0, e = !1;
    return $(ACC.quickorder.$qtyInputField).each(function() {
      var i = this.value.trim();
      if (i && (t += parseInt(i, 10)), t >= 1) return e = !0, !1;
    }), e;
  },
  isAnySkuPresent: function() {
    var t = !1;
    return $(ACC.quickorder.$skuInputField).each(function() {
      if (jQuery.trim(this.value)) return t = !0, !1;
    }), t;
  },
  getCurrentParentLi: function(t) {
    return $(t).closest(ACC.quickorder.$jsLiContainer);
  },
  findElement: function(t, e) {
    return $(t).find(e);
  },
  findElementInCurrentParentLi: function(t, e) {
    return $(t).closest(ACC.quickorder.$jsLiContainer).find(e);
  }
}), ACC.quote = {
  _autoload: [ [ "bindAddComment", 0 != $("#js-quote-comments").length ], [ "bindAddEntryComment", 0 != $(".js-quote-entry-comments").length ], [ "toggleMoreComments", 0 != $("#js-quote-comments").length ], [ "toggleLessComments", 0 != $("#js-quote-comments").length ], [ "displayLessComments", 0 != $("#js-quote-comments").length ], [ "quoteDetailsNavigation", 0 != $(".js-quote-actions").length ], [ "bindQuoteButtons", 0 != $(".js-btn-quote").length ], [ "bindEditQuoteButton", 0 != $(".js-quote-edit-btn").length ], [ "bindSubmitConfirmation", 0 != $(".js-quote-submit-btn").length ], [ "bindCancelConfirmation", 0 != $(".js-quote-cancel-btn").length ], [ "bindName", 0 != $("#js-quote-name").length ], [ "bindDescription", 0 != $("#js-quote-description").length ], [ "bindExpirationTime", 0 != $("#js-quote-expiration-time").length ], [ "bindCheckoutConfirmation", 0 != $(".js-quote-checkout-btn").length ], [ "bindEditConfirmation", 0 != $(".js-quote-warning-btn").length ], [ "bindQuoteDiscount", 0 != $(".js-quote-discount-link").length ], [ "bindNewCartClick", 0 != $(".new__cart--link").length ] ],
  bindEditQuoteButton: function() {
    $(".js-quote-edit-btn").on("click", function() {
      var t = $(this).data("quoteEditUrl");
      window.location = t;
    });
  },
  bindNewCartClick: function() {
    $(".new__cart--link").bind("click", function(t) {
      $(this).unbind(t);
    });
  },
  bindAddComment: function() {
    $(document).on("keypress", "#js-quote-comments #comment", function(t) {
      return 13 != t.keyCode || "" != $("#comment").val().trim() && (t.preventDefault(), 
      ACC.quote.quoteCommentSubmit($("#comment").val()), $("#comment").val(""), !1);
    });
  },
  bindAddEntryComment: function() {
    $(document).on("keypress", ".js-quote-entry-comments", function(t) {
      return 13 != t.keyCode || (t.preventDefault(), ACC.quote.quoteEntryCommentSubmit($(this).val(), $(this).data("entry-number")), 
      !1);
    });
  },
  bindQuoteButtons: function() {
    $(".js-save-quote-btn").click(function() {
      var t = $(this).data("saveQuoteUrl");
      $("#quoteForm").attr("action", t).submit();
    }), $(".js-submit-quote-btn").click(function() {
      var t = $(this).data("submitQuoteUrl");
      $("#quoteForm").attr("action", t).submit();
    }), $(".js-accept-quote-btn").click(function() {
      var t = $(this).data("acceptQuoteUrl");
      $("#quoteForm").attr("action", t).submit();
    });
  },
  quoteCommentSubmit: function(t) {
    var e = $("#js-quote-comments"), i = e.data("quote-base-link") + "comment", n = e.data("show-all-comments");
    $.ajax({
      url: i,
      data: {
        comment: t
      },
      type: "post",
      success: function(t) {
        ACC.quote.onCommentSuccess(n);
      }
    });
  },
  quoteEntryCommentSubmit: function(t, e) {
    if (t && t.length) {
      var i = $("#js-quote-comments").data("quote-base-link") + "entry/comment";
      $.ajax({
        url: i,
        data: {
          comment: t,
          entryNumber: e
        },
        type: "post",
        success: function() {
          ACC.quote.onEntryCommentSuccess(e);
        }
      });
    }
  },
  onCommentSuccess: function(t) {
    $("#commentListDiv").load(location.href + " #commentListDiv", function() {
      ACC.quote.displayComments("" + t);
    });
  },
  onEntryCommentSuccess: function(t) {
    $("#entryCommentListDiv_" + t).load(location.href + " #entryCommentListDiv_" + t, function() {
      ACC.quote.displayEntryComments(t);
    }), $("#entryComment_" + t).val("");
  },
  toggleMoreComments: function() {
    $(document).on("click", "#moreCommentsAnchor", ACC.quote.displayMoreComments), $(document).on("click", ".js-more-entry-comments-anchor", ACC.quote.displayMoreEntryComments);
  },
  toggleLessComments: function() {
    $(document).on("click", "#lessCommentsAnchor", ACC.quote.displayLessComments), $(document).on("click", ".js-less-entry-comments-anchor", ACC.quote.displayLessEntryComments);
  },
  displayMoreComments: function(t) {
    t.preventDefault(), ACC.quote.displayComments("true");
  },
  displayMoreEntryComments: function(t) {
    t.preventDefault(), ACC.quote.displayEntryComments($(this).data("entry-number"), "true");
  },
  displayLessComments: function(t) {
    void 0 != t && t.preventDefault(), ACC.quote.displayComments("false");
  },
  displayLessEntryComments: function(t) {
    t.preventDefault(), ACC.quote.displayEntryComments($(this).data("entry-number"), "false");
  },
  displayComments: function(t) {
    for (var e = $("#js-quote-comments"), i = e.data("current-comments-shown"), n = $('[id^="comment_"]'), o = 0; o < n.length; o++) "true" === t ? $(n[o]).show() : o < i ? $(n[o]).show() : $(n[o]).hide();
    return "false" === t ? ($("#moreCommentsAnchor").show(), $("#lessCommentsAnchor").hide(), 
    e.data("show-all-comments", !1)) : ($("#moreCommentsAnchor").hide(), $("#lessCommentsAnchor").show(), 
    e.data("show-all-comments", !0)), !1;
  },
  displayEntryComments: function(t, e) {
    var i = $("#entryCommentListDiv_" + t), n = i.find('[id^="entryComment_' + t + '"]');
    e = e || "" + i.data("show-all-entry-comments");
    for (var o = 0; o < n.length; o++) "true" === e ? $(n[o]).show() : o < 4 ? $(n[o]).show() : $(n[o]).hide();
    return "false" === e ? (i.find(".js-more-entry-comments-anchor").show(), i.find(".js-less-entry-comments-anchor").hide(), 
    i.data("show-all-entry-comments", !1)) : (i.find(".js-more-entry-comments-anchor").hide(), 
    i.find(".js-less-entry-comments-anchor").show(), i.data("show-all-entry-comments", !0)), 
    !1;
  },
  quoteDetailsNavigation: function() {
    $(".js-quote-actions").on("click", function(t) {
      $(this).parent().find("nav").toggleClass("display-none");
    });
  },
  bindSubmitConfirmation: function(t) {
    ACC.quote.handleConfirmationModal({
      actionButtonSelector: ".js-quote-submit-btn",
      modalWindowSelector: "#js-quote-submit-modal",
      modalTitleDataAttributeName: "submit-confirmation-modal-title",
      cancelButtonSelector: "#js-quote-submit-modal #submitNoButton"
    }), $("#quoteSubmitForm").submit(function(t) {
      var e = $("#quoteForm");
      if (e.is("form")) {
        t.preventDefault();
        var i = $(this).prop("action");
        e.prop("action", i), e.submit();
      }
    });
  },
  bindCheckoutConfirmation: function(t) {
    ACC.quote.handleConfirmationModal({
      actionButtonSelector: ".js-quote-checkout-btn",
      modalWindowSelector: "#js-quote-checkout-modal",
      modalTitleDataAttributeName: "submit-confirmation-modal-title",
      cancelButtonSelector: "#js-quote-checkout-modal #submitNoButton"
    });
  },
  bindCancelConfirmation: function(t) {
    ACC.quote.handleConfirmationModal({
      actionButtonSelector: ".js-quote-cancel-btn",
      modalWindowSelector: "#js-quote-cancel-modal",
      modalTitleDataAttributeName: "cancel-confirmation-modal-title",
      cancelButtonSelector: "#js-quote-cancel-modal #cancelNoButton"
    });
  },
  bindEditConfirmation: function(t) {
    ACC.quote.handleConfirmationModal({
      actionButtonSelector: ".js-quote-warning-btn",
      modalWindowSelector: "#js-quote-edit-modal",
      modalTitleDataAttributeName: "edit-confirmation-modal-title",
      cancelButtonSelector: "#js-quote-edit-modal #cancelEditNoButton",
      confirmButtonSelector: "#js-quote-edit-modal #cancelEditYesButton"
    });
  },
  handleConfirmationModal: function(t) {
    $(t.actionButtonSelector).click(function(e) {
      e.preventDefault();
      var i = $(t.modalWindowSelector), n = i.data(t.modalTitleDataAttributeName);
      t.initializeCallback && t.initializeCallback(), ACC.colorbox.open(n, {
        inline: !0,
        href: i,
        width: "480px",
        escKey: !1,
        overlayClose: !1,
        onComplete: function() {
          ACC.colorbox.resize();
        }
      });
    }), $(t.cancelButtonSelector).click(function(t) {
      t.preventDefault(), ACC.colorbox.close();
    }), $(t.confirmButtonSelector).click(function(e) {
      e.preventDefault(), ACC.colorbox.close();
      var i = $(t.actionButtonSelector).data("quoteEditUrl");
      window.location = i;
    });
  },
  bindQuoteDiscount: function(t) {
    ACC.quote.handleDiscountModal({
      actionButtonSelector: ".js-quote-discount-link",
      modalWindowSelector: "#js-quote-discount-modal",
      modalTitleDataAttributeName: "quote-modal-title",
      modalTotalDataAttributeName: "quote-modal-total",
      modalQuoteDiscountDataAttributeName: "quote-modal-quote-discount",
      modalCurrencyDataAttributeName: "quote-modal-currency",
      cancelButtonSelector: "#js-quote-discount-modal #cancelButton"
    });
  },
  handleDiscountModal: function(t) {
    function e() {
      $("#js-quote-discount-by-percentage").css("border-color", "#cccccc"), $("#js-quote-discount-by-amount").css("border-color", "#cccccc"), 
      $("#js-quote-discount-adjust-total").css("border-color", "#cccccc"), $("#submitButton").prop("disabled", !1);
    }
    function i(t) {
      return isNaN(parseFloat(t)) && (t = (t = 0).toFixed(2)), t;
    }
    function n() {
      var t = parseFloat($("#js-quote-discount-by-percentage").val());
      t > 100 || t < 0 ? ($("#js-quote-discount-by-percentage").css("border-color", "red"), 
      $("#submitButton").prop("disabled", !0)) : e();
      var n = (n = d * t / 100).toFixed(2);
      $("#js-quote-discount-by-amount").val(i(n));
      var o = (o = d - n).toFixed(2);
      $("#js-quote-discount-adjust-total").val(i(o)), $("#js-quote-discount-rate").val(i(t)), 
      $("#js-quote-discount-type").val("PERCENT"), l(o);
    }
    function o() {
      var t = $("#js-quote-discount-by-percentage").val(), e = $("#js-quote-discount-by-amount").val(), i = $("#js-quote-discount-adjust-total").val();
      "" == t && $("#js-quote-discount-by-percentage").val("0.00"), "" == e && $("#js-quote-discount-by-amount").val("0.00"), 
      "" != i && 0 != i || $("#js-quote-discount-adjust-total").val(d);
    }
    function s(t) {
      var e = $(this);
      46 == t.which && -1 == e.val().indexOf(".") || !(t.which < 48 || t.which > 57) || 0 == t.which || 8 == t.which || t.preventDefault();
      var i = $(this).val();
      46 == t.which && -1 == i.indexOf(".") && setTimeout(function() {
        e.val().substring(e.val().indexOf(".")).length > 3 && e.val(e.val().substring(0, e.val().indexOf(".") + 3));
      }, 1), -1 != i.indexOf(".") && i.substring(i.indexOf(".")).length > 2 && 0 != t.which && 8 != t.which && $(this)[0].selectionStart >= i.length - 2 && t.preventDefault();
    }
    function a() {
      var t = parseFloat($("#js-quote-discount-by-amount").val());
      t > d || t < 0 ? ($("#js-quote-discount-by-amount").css("border-color", "red"), 
      $("#submitButton").prop("disabled", !0)) : e();
      var n = (n = t / d * 100).toFixed(2);
      $("#js-quote-discount-by-percentage").val(i(n));
      var o = (o = d - t).toFixed(2);
      $("#js-quote-discount-adjust-total").val(i(o)), $("#js-quote-discount-rate").val(i(t)), 
      $("#js-quote-discount-type").val("ABSOLUTE"), l(o);
    }
    function r() {
      var t = parseFloat($("#js-quote-discount-adjust-total").val());
      t > d || t < 0 ? ($("#js-quote-discount-adjust-total").css("border-color", "red"), 
      $("#submitButton").prop("disabled", !0)) : e();
      var n = (n = d - t).toFixed(2);
      $("#js-quote-discount-by-amount").val(i(n));
      var o = (o = n / d * 100).toFixed(2);
      $("#js-quote-discount-by-percentage").val(i(o)), $("#js-quote-discount-rate").val(i(t)), 
      $("#js-quote-discount-type").val("TARGET"), l(t);
    }
    function l(t) {
      isNaN(parseFloat(t)) && (t = d);
      var t = parseFloat(t).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
      $("#js-quote-discount-new-total").text(h.concat(t));
    }
    var c = $(t.modalWindowSelector), d = parseFloat(c.data(t.modalTotalDataAttributeName)), u = parseFloat(c.data(t.modalQuoteDiscountDataAttributeName)), h = c.data(t.modalCurrencyDataAttributeName);
    $(t.actionButtonSelector).click(function(e) {
      e.preventDefault();
      var i = c.data(t.modalTitleDataAttributeName);
      t.initializeCallback && t.initializeCallback(), ACC.colorbox.open(i, {
        inline: !0,
        href: c,
        width: "480px",
        onComplete: function() {
          ACC.colorbox.resize();
          var t = u / d * 100, e = (d - u).toFixed(2);
          $("#js-quote-discount-by-percentage").val(t.toFixed(2)), $("#js-quote-discount-by-amount").val(u.toFixed(2)), 
          $("#js-quote-discount-adjust-total").val(e);
        }
      });
    }), $(t.cancelButtonSelector).click(function(t) {
      t.preventDefault(), ACC.colorbox.close();
    }), $("#js-quote-discount-by-percentage").keyup(n), $("#js-quote-discount-by-percentage").change(n), 
    $("#js-quote-discount-by-percentage").blur(o), $("#js-quote-discount-by-percentage").keypress(s), 
    $("#js-quote-discount-by-amount").keyup(a), $("#js-quote-discount-by-amount").focusout(a), 
    $("#js-quote-discount-by-amount").keypress(s), $("#js-quote-discount-by-amount").blur(o), 
    $("#js-quote-discount-adjust-total").keyup(r), $("#js-quote-discount-adjust-total").focusout(r), 
    $("#js-quote-discount-adjust-total").keypress(s), $("#js-quote-discount-adjust-total").blur(o);
  },
  bindName: function() {
    $("#js-quote-name").on("focusout", function() {
      ACC.quote.updateMetadata();
    });
  },
  bindDescription: function() {
    $("#js-quote-description").on("focusout", function() {
      ACC.quote.updateMetadata();
    });
  },
  updateMetadata: function() {
    var t = $("#quoteFormDiv").data("metadata-url"), e = $("#js-quote-name").val().trim(), i = $("#js-quote-description").val(), n = $("#js-quote-name-wrapper");
    e && e.length ? (n.removeClass("has-error"), $.ajax({
      url: t,
      data: {
        name: e,
        description: i
      },
      type: "POST",
      success: function() {
        $(".js-modal-quote-description").text(i), $(".js-modal-quote-name").text(e);
      }
    })) : n.hasClass("has-error") || n.addClass("has-error");
  },
  bindExpirationTime: function(t) {
    var e = $("#js-quote-expiration-time"), i = e.data("date-format-for-date-picker"), n = e.data("min-offer-validity-period-days"), o = new Date();
    o.setDate(o.getDate() + n), $("#expirationTime").datepicker({
      dateFormat: i,
      constrainInput: !0,
      minDate: o,
      onSelect: function() {
        ACC.quote.handleExpirationTimeUpdate(e, i, n);
      }
    }), $("#expirationTime").change(function() {
      ACC.quote.handleExpirationTimeUpdate(e, i, n);
    }), $(document).on("click", ".js-open-datepicker-quote-expiration-time", function() {
      $("#expirationTime").datepicker("show");
    });
  },
  handleExpirationTimeUpdate: function(t, e, i) {
    var n = $("#expirationTime").val();
    ACC.quote.validateExpirationTime(e, n, i) ? (ACC.quote.updateExpirationTime(n.trim()), 
    t.removeClass("has-error")) : t.hasClass("has-error") || t.addClass("has-error");
  },
  validateExpirationTime: function(t, e, i) {
    try {
      if (e) {
        var n = $.datepicker.parseDate(t, e), o = new Date();
        return o.setHours(0, 0, 0, 0), o.setDate(o.getDate() + i), n >= o;
      }
      return !0;
    } catch (t) {
      return !1;
    }
  },
  updateExpirationTime: function(t) {
    var e = $("#js-quote-expiration-time").data("expiration-time-url");
    $.ajax({
      url: e,
      type: "POST",
      data: {
        expirationTime: t
      },
      error: function(t) {
        var e = $("#js-quote-expiration-time");
        e.hasClass("has-error") || e.addClass("has-error");
      }
    });
  }
}, ACC.csvimport = {
  TEXT_CSV_CONTENT_TYPE: "text/csv",
  APP_EXCEL_CONTENT_TYPE: "application/vnd.ms-excel",
  _autoload: [ [ "changeFileUploadAppearance", 0 != $(".js-file-upload").length ], [ "bindImportCSVActions", 0 != $(".js-import-csv").length ] ],
  changeFileUploadAppearance: function() {
    $(".js-file-upload__input").on("change", function() {
      $(".js-file-upload__file-name").text(this.value.toLowerCase().replace(/c:\\fakepath\\/g, "")), 
      $(".js-file-upload").parents("#cboxLoadedContent").length > 0 && ACC.colorbox.resize();
    });
  },
  bindImportCSVActions: function() {
    $("#chooseFileButton").on("click", function(t) {
      ACC.csvimport.clearGlobalAlerts();
    }), $("#importButton").on("click", function(t) {
      if (t.preventDefault(), ACC.csvimport.clearGlobalAlerts(), $(".js-file-upload__input").val().trim().length > 0) {
        var e = document.getElementById("csvFile").files[0];
        if (ACC.csvimport.isSelectedFileValid(e)) {
          var i = document.getElementById("importCSVSavedCartForm"), n = new window.FormData(i);
          n.append("csvFile", e), ACC.csvimport.displayGlobalAlert({
            type: "warning",
            messageId: "import-csv-upload-message"
          }), ACC.csvimport.enableDisableActionButtons(!1), $.ajax({
            url: i.action,
            type: "POST",
            data: n,
            contentType: !1,
            processData: !1,
            success: function() {
              ACC.csvimport.displayGlobalAlert({
                type: "info",
                message: ""
              }), $("#import-csv-alerts .alert-info").append($("#import-csv-success-message").html()), 
              ACC.csvimport.clearChosenFile();
            },
            error: function(t) {
              400 == t.status && t.responseJSON ? ACC.csvimport.displayGlobalAlert({
                type: "error",
                message: t.responseJSON
              }) : ACC.csvimport.displayGlobalAlert({
                type: "error",
                messageId: "import-csv-generic-error-message"
              });
            },
            complete: function() {
              ACC.csvimport.enableDisableActionButtons(!0);
            }
          });
        }
      } else ACC.csvimport.displayGlobalAlert({
        type: "error",
        messageId: "import-csv-no-file-chosen-error-message"
      });
    });
  },
  isSelectedFileValid: function(t) {
    if (window.File && window.Blob) {
      if (t) {
        if (t.type != ACC.csvimport.TEXT_CSV_CONTENT_TYPE && t.type != ACC.csvimport.APP_EXCEL_CONTENT_TYPE) return ACC.csvimport.displayGlobalAlert({
          type: "error",
          messageId: "import-csv-file-csv-required"
        }), !1;
        var e = t.name;
        if (!e || !/\.csv$/i.test(e)) return ACC.csvimport.displayGlobalAlert({
          type: "error",
          messageId: "import-csv-file-csv-required"
        }), !1;
      }
      var i = $(".js-file-upload__input").data("file-max-size");
      if ($.isNumeric(i) && t && t.size > parseFloat(i)) return ACC.csvimport.displayGlobalAlert({
        type: "error",
        messageId: "import-csv-file-max-size-exceeded-error-message"
      }), !1;
    }
    return !0;
  },
  displayGlobalAlert: function(t) {
    ACC.csvimport.clearGlobalAlerts();
    var e;
    switch (t.type) {
     case "error":
      e = "#global-alert-danger-template";
      break;

     case "warning":
      e = "#global-alert-warning-template";
      break;

     default:
      e = "#global-alert-info-template";
    }
    void 0 !== t.message && $("#import-csv-alerts").append($(e).tmpl({
      message: t.message
    })), void 0 !== t.messageId && $("#import-csv-alerts").append($(e).tmpl({
      message: $("#" + t.messageId).text()
    }));
  },
  clearGlobalAlerts: function() {
    $("#import-csv-alerts").empty();
  },
  clearChosenFile: function() {
    document.getElementById("csvFile").value = "", $(".js-file-upload__file-name").text("");
  },
  enableDisableActionButtons: function(t) {
    $("#chooseFileButton").attr("disabled", !t), $("#importButton").prop("disabled", !t);
  }
}, function(t) {
  "use strict";
  "function" == typeof define && define.amd ? define([ "jquery" ], t) : "undefined" != typeof exports ? module.exports = t(require("jquery")) : t(jQuery);
}(function(t) {
  "use strict";
  var e = window.Slick || {};
  (e = function() {
    var e = 0;
    return function(i, n) {
      var o, s = this;
      s.defaults = {
        accessibility: !0,
        adaptiveHeight: !1,
        appendArrows: t(i),
        appendDots: t(i),
        arrows: !0,
        asNavFor: null,
        prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">Previous</button>',
        nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button">Next</button>',
        autoplay: !1,
        autoplaySpeed: 3e3,
        centerMode: !1,
        centerPadding: "50px",
        cssEase: "ease",
        customPaging: function(e, i) {
          return t('<button type="button" data-role="none" role="button" tabindex="0" />').text(i + 1);
        },
        dots: !1,
        dotsClass: "slick-dots",
        draggable: !0,
        easing: "linear",
        edgeFriction: .35,
        fade: !1,
        focusOnSelect: !1,
        infinite: !0,
        initialSlide: 0,
        lazyLoad: "ondemand",
        mobileFirst: !1,
        pauseOnHover: !0,
        pauseOnFocus: !0,
        pauseOnDotsHover: !1,
        respondTo: "window",
        responsive: null,
        rows: 1,
        rtl: !1,
        slide: "",
        slidesPerRow: 1,
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 500,
        swipe: !0,
        swipeToSlide: !1,
        touchMove: !0,
        touchThreshold: 5,
        useCSS: !0,
        useTransform: !0,
        variableWidth: !1,
        vertical: !1,
        verticalSwiping: !1,
        waitForAnimate: !0,
        zIndex: 1e3
      }, s.initials = {
        animating: !1,
        dragging: !1,
        autoPlayTimer: null,
        currentDirection: 0,
        currentLeft: null,
        currentSlide: 0,
        direction: 1,
        $dots: null,
        listWidth: null,
        listHeight: null,
        loadIndex: 0,
        $nextArrow: null,
        $prevArrow: null,
        slideCount: null,
        slideWidth: null,
        $slideTrack: null,
        $slides: null,
        sliding: !1,
        slideOffset: 0,
        swipeLeft: null,
        $list: null,
        touchObject: {},
        transformsEnabled: !1,
        unslicked: !1
      }, t.extend(s, s.initials), s.activeBreakpoint = null, s.animType = null, s.animProp = null, 
      s.breakpoints = [], s.breakpointSettings = [], s.cssTransitions = !1, s.focussed = !1, 
      s.interrupted = !1, s.hidden = "hidden", s.paused = !0, s.positionProp = null, s.respondTo = null, 
      s.rowCount = 1, s.shouldClick = !0, s.$slider = t(i), s.$slidesCache = null, s.transformType = null, 
      s.transitionType = null, s.visibilityChange = "visibilitychange", s.windowWidth = 0, 
      s.windowTimer = null, o = t(i).data("slick") || {}, s.options = t.extend({}, s.defaults, n, o), 
      s.currentSlide = s.options.initialSlide, s.originalSettings = s.options, void 0 !== document.mozHidden ? (s.hidden = "mozHidden", 
      s.visibilityChange = "mozvisibilitychange") : void 0 !== document.webkitHidden && (s.hidden = "webkitHidden", 
      s.visibilityChange = "webkitvisibilitychange"), s.autoPlay = t.proxy(s.autoPlay, s), 
      s.autoPlayClear = t.proxy(s.autoPlayClear, s), s.autoPlayIterator = t.proxy(s.autoPlayIterator, s), 
      s.changeSlide = t.proxy(s.changeSlide, s), s.clickHandler = t.proxy(s.clickHandler, s), 
      s.selectHandler = t.proxy(s.selectHandler, s), s.setPosition = t.proxy(s.setPosition, s), 
      s.swipeHandler = t.proxy(s.swipeHandler, s), s.dragHandler = t.proxy(s.dragHandler, s), 
      s.keyHandler = t.proxy(s.keyHandler, s), s.instanceUid = e++, s.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/, 
      s.registerBreakpoints(), s.init(!0);
    };
  }()).prototype.activateADA = function() {
    this.$slideTrack.find(".slick-active").attr({
      "aria-hidden": "false"
    }).find("a, input, button, select").attr({
      tabindex: "0"
    });
  }, e.prototype.addSlide = e.prototype.slickAdd = function(e, i, n) {
    var o = this;
    if ("boolean" == typeof i) n = i, i = null; else if (i < 0 || i >= o.slideCount) return !1;
    o.unload(), "number" == typeof i ? 0 === i && 0 === o.$slides.length ? t(e).appendTo(o.$slideTrack) : n ? t(e).insertBefore(o.$slides.eq(i)) : t(e).insertAfter(o.$slides.eq(i)) : !0 === n ? t(e).prependTo(o.$slideTrack) : t(e).appendTo(o.$slideTrack), 
    o.$slides = o.$slideTrack.children(this.options.slide), o.$slideTrack.children(this.options.slide).detach(), 
    o.$slideTrack.append(o.$slides), o.$slides.each(function(e, i) {
      t(i).attr("data-slick-index", e);
    }), o.$slidesCache = o.$slides, o.reinit();
  }, e.prototype.animateHeight = function() {
    var t = this;
    if (1 === t.options.slidesToShow && !0 === t.options.adaptiveHeight && !1 === t.options.vertical) {
      var e = t.$slides.eq(t.currentSlide).outerHeight(!0);
      t.$list.animate({
        height: e
      }, t.options.speed);
    }
  }, e.prototype.animateSlide = function(e, i) {
    var n = {}, o = this;
    o.animateHeight(), !0 === o.options.rtl && !1 === o.options.vertical && (e = -e), 
    !1 === o.transformsEnabled ? !1 === o.options.vertical ? o.$slideTrack.animate({
      left: e
    }, o.options.speed, o.options.easing, i) : o.$slideTrack.animate({
      top: e
    }, o.options.speed, o.options.easing, i) : !1 === o.cssTransitions ? (!0 === o.options.rtl && (o.currentLeft = -o.currentLeft), 
    t({
      animStart: o.currentLeft
    }).animate({
      animStart: e
    }, {
      duration: o.options.speed,
      easing: o.options.easing,
      step: function(t) {
        t = Math.ceil(t), !1 === o.options.vertical ? (n[o.animType] = "translate(" + t + "px, 0px)", 
        o.$slideTrack.css(n)) : (n[o.animType] = "translate(0px," + t + "px)", o.$slideTrack.css(n));
      },
      complete: function() {
        i && i.call();
      }
    })) : (o.applyTransition(), e = Math.ceil(e), !1 === o.options.vertical ? n[o.animType] = "translate3d(" + e + "px, 0px, 0px)" : n[o.animType] = "translate3d(0px," + e + "px, 0px)", 
    o.$slideTrack.css(n), i && setTimeout(function() {
      o.disableTransition(), i.call();
    }, o.options.speed));
  }, e.prototype.getNavTarget = function() {
    var e = this, i = e.options.asNavFor;
    return i && null !== i && (i = t(i).not(e.$slider)), i;
  }, e.prototype.asNavFor = function(e) {
    var i = this.getNavTarget();
    null !== i && "object" == typeof i && i.each(function() {
      var i = t(this).slick("getSlick");
      i.unslicked || i.slideHandler(e, !0);
    });
  }, e.prototype.applyTransition = function(t) {
    var e = this, i = {};
    !1 === e.options.fade ? i[e.transitionType] = e.transformType + " " + e.options.speed + "ms " + e.options.cssEase : i[e.transitionType] = "opacity " + e.options.speed + "ms " + e.options.cssEase, 
    !1 === e.options.fade ? e.$slideTrack.css(i) : e.$slides.eq(t).css(i);
  }, e.prototype.autoPlay = function() {
    var t = this;
    t.autoPlayClear(), t.slideCount > t.options.slidesToShow && (t.autoPlayTimer = setInterval(t.autoPlayIterator, t.options.autoplaySpeed));
  }, e.prototype.autoPlayClear = function() {
    var t = this;
    t.autoPlayTimer && clearInterval(t.autoPlayTimer);
  }, e.prototype.autoPlayIterator = function() {
    var t = this, e = t.currentSlide + t.options.slidesToScroll;
    t.paused || t.interrupted || t.focussed || (!1 === t.options.infinite && (1 === t.direction && t.currentSlide + 1 === t.slideCount - 1 ? t.direction = 0 : 0 === t.direction && (e = t.currentSlide - t.options.slidesToScroll, 
    t.currentSlide - 1 == 0 && (t.direction = 1))), t.slideHandler(e));
  }, e.prototype.buildArrows = function() {
    var e = this;
    !0 === e.options.arrows && (e.$prevArrow = t(e.options.prevArrow).addClass("slick-arrow"), 
    e.$nextArrow = t(e.options.nextArrow).addClass("slick-arrow"), e.slideCount > e.options.slidesToShow ? (e.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), 
    e.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), e.htmlExpr.test(e.options.prevArrow) && e.$prevArrow.prependTo(e.options.appendArrows), 
    e.htmlExpr.test(e.options.nextArrow) && e.$nextArrow.appendTo(e.options.appendArrows), 
    !0 !== e.options.infinite && e.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true")) : e.$prevArrow.add(e.$nextArrow).addClass("slick-hidden").attr({
      "aria-disabled": "true",
      tabindex: "-1"
    }));
  }, e.prototype.buildDots = function() {
    var e, i, n = this;
    if (!0 === n.options.dots && n.slideCount > n.options.slidesToShow) {
      for (n.$slider.addClass("slick-dotted"), i = t("<ul />").addClass(n.options.dotsClass), 
      e = 0; e <= n.getDotCount(); e += 1) i.append(t("<li />").append(n.options.customPaging.call(this, n, e)));
      n.$dots = i.appendTo(n.options.appendDots), n.$dots.find("li").first().addClass("slick-active").attr("aria-hidden", "false");
    }
  }, e.prototype.buildOut = function() {
    var e = this;
    e.$slides = e.$slider.children(e.options.slide + ":not(.slick-cloned)").addClass("slick-slide"), 
    e.slideCount = e.$slides.length, e.$slides.each(function(e, i) {
      t(i).attr("data-slick-index", e).data("originalStyling", t(i).attr("style") || "");
    }), e.$slider.addClass("slick-slider"), e.$slideTrack = 0 === e.slideCount ? t('<div class="slick-track"/>').appendTo(e.$slider) : e.$slides.wrapAll('<div class="slick-track"/>').parent(), 
    e.$list = e.$slideTrack.wrap('<div aria-live="polite" class="slick-list"/>').parent(), 
    e.$slideTrack.css("opacity", 0), !0 !== e.options.centerMode && !0 !== e.options.swipeToSlide || (e.options.slidesToScroll = 1), 
    t("img[data-lazy]", e.$slider).not("[src]").addClass("slick-loading"), e.setupInfinite(), 
    e.buildArrows(), e.buildDots(), e.updateDots(), e.setSlideClasses("number" == typeof e.currentSlide ? e.currentSlide : 0), 
    !0 === e.options.draggable && e.$list.addClass("draggable");
  }, e.prototype.buildRows = function() {
    var t, e, i, n, o, s, a, r = this;
    if (n = document.createDocumentFragment(), s = r.$slider.children(), r.options.rows > 1) {
      for (a = r.options.slidesPerRow * r.options.rows, o = Math.ceil(s.length / a), t = 0; t < o; t++) {
        var l = document.createElement("div");
        for (e = 0; e < r.options.rows; e++) {
          var c = document.createElement("div");
          for (i = 0; i < r.options.slidesPerRow; i++) {
            var d = t * a + (e * r.options.slidesPerRow + i);
            s.get(d) && c.appendChild(s.get(d));
          }
          l.appendChild(c);
        }
        n.appendChild(l);
      }
      r.$slider.empty().append(n), r.$slider.children().children().children().css({
        width: 100 / r.options.slidesPerRow + "%",
        display: "inline-block"
      });
    }
  }, e.prototype.checkResponsive = function(e, i) {
    var n, o, s, a = this, r = !1, l = a.$slider.width(), c = window.innerWidth || t(window).width();
    if ("window" === a.respondTo ? s = c : "slider" === a.respondTo ? s = l : "min" === a.respondTo && (s = Math.min(c, l)), 
    a.options.responsive && a.options.responsive.length && null !== a.options.responsive) {
      o = null;
      for (n in a.breakpoints) a.breakpoints.hasOwnProperty(n) && (!1 === a.originalSettings.mobileFirst ? s < a.breakpoints[n] && (o = a.breakpoints[n]) : s > a.breakpoints[n] && (o = a.breakpoints[n]));
      null !== o ? null !== a.activeBreakpoint ? (o !== a.activeBreakpoint || i) && (a.activeBreakpoint = o, 
      "unslick" === a.breakpointSettings[o] ? a.unslick(o) : (a.options = t.extend({}, a.originalSettings, a.breakpointSettings[o]), 
      !0 === e && (a.currentSlide = a.options.initialSlide), a.refresh(e)), r = o) : (a.activeBreakpoint = o, 
      "unslick" === a.breakpointSettings[o] ? a.unslick(o) : (a.options = t.extend({}, a.originalSettings, a.breakpointSettings[o]), 
      !0 === e && (a.currentSlide = a.options.initialSlide), a.refresh(e)), r = o) : null !== a.activeBreakpoint && (a.activeBreakpoint = null, 
      a.options = a.originalSettings, !0 === e && (a.currentSlide = a.options.initialSlide), 
      a.refresh(e), r = o), e || !1 === r || a.$slider.trigger("breakpoint", [ a, r ]);
    }
  }, e.prototype.changeSlide = function(e, i) {
    var n, o, s, a = this, r = t(e.currentTarget);
    switch (r.is("a") && e.preventDefault(), r.is("li") || (r = r.closest("li")), s = a.slideCount % a.options.slidesToScroll != 0, 
    n = s ? 0 : (a.slideCount - a.currentSlide) % a.options.slidesToScroll, e.data.message) {
     case "previous":
      o = 0 === n ? a.options.slidesToScroll : a.options.slidesToShow - n, a.slideCount > a.options.slidesToShow && a.slideHandler(a.currentSlide - o, !1, i);
      break;

     case "next":
      o = 0 === n ? a.options.slidesToScroll : n, a.slideCount > a.options.slidesToShow && a.slideHandler(a.currentSlide + o, !1, i);
      break;

     case "index":
      var l = 0 === e.data.index ? 0 : e.data.index || r.index() * a.options.slidesToScroll;
      a.slideHandler(a.checkNavigable(l), !1, i), r.children().trigger("focus");
      break;

     default:
      return;
    }
  }, e.prototype.checkNavigable = function(t) {
    var e, i;
    if (e = this.getNavigableIndexes(), i = 0, t > e[e.length - 1]) t = e[e.length - 1]; else for (var n in e) {
      if (t < e[n]) {
        t = i;
        break;
      }
      i = e[n];
    }
    return t;
  }, e.prototype.cleanUpEvents = function() {
    var e = this;
    e.options.dots && null !== e.$dots && t("li", e.$dots).off("click.slick", e.changeSlide).off("mouseenter.slick", t.proxy(e.interrupt, e, !0)).off("mouseleave.slick", t.proxy(e.interrupt, e, !1)), 
    e.$slider.off("focus.slick blur.slick"), !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && (e.$prevArrow && e.$prevArrow.off("click.slick", e.changeSlide), 
    e.$nextArrow && e.$nextArrow.off("click.slick", e.changeSlide)), e.$list.off("touchstart.slick mousedown.slick", e.swipeHandler), 
    e.$list.off("touchmove.slick mousemove.slick", e.swipeHandler), e.$list.off("touchend.slick mouseup.slick", e.swipeHandler), 
    e.$list.off("touchcancel.slick mouseleave.slick", e.swipeHandler), e.$list.off("click.slick", e.clickHandler), 
    t(document).off(e.visibilityChange, e.visibility), e.cleanUpSlideEvents(), !0 === e.options.accessibility && e.$list.off("keydown.slick", e.keyHandler), 
    !0 === e.options.focusOnSelect && t(e.$slideTrack).children().off("click.slick", e.selectHandler), 
    t(window).off("orientationchange.slick.slick-" + e.instanceUid, e.orientationChange), 
    t(window).off("resize.slick.slick-" + e.instanceUid, e.resize), t("[draggable!=true]", e.$slideTrack).off("dragstart", e.preventDefault), 
    t(window).off("load.slick.slick-" + e.instanceUid, e.setPosition), t(document).off("ready.slick.slick-" + e.instanceUid, e.setPosition);
  }, e.prototype.cleanUpSlideEvents = function() {
    var e = this;
    e.$list.off("mouseenter.slick", t.proxy(e.interrupt, e, !0)), e.$list.off("mouseleave.slick", t.proxy(e.interrupt, e, !1));
  }, e.prototype.cleanUpRows = function() {
    var t, e = this;
    e.options.rows > 1 && ((t = e.$slides.children().children()).removeAttr("style"), 
    e.$slider.empty().append(t));
  }, e.prototype.clickHandler = function(t) {
    !1 === this.shouldClick && (t.stopImmediatePropagation(), t.stopPropagation(), t.preventDefault());
  }, e.prototype.destroy = function(e) {
    var i = this;
    i.autoPlayClear(), i.touchObject = {}, i.cleanUpEvents(), t(".slick-cloned", i.$slider).detach(), 
    i.$dots && i.$dots.remove(), i.$prevArrow && i.$prevArrow.length && (i.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), 
    i.htmlExpr.test(i.options.prevArrow) && i.$prevArrow.remove()), i.$nextArrow && i.$nextArrow.length && (i.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), 
    i.htmlExpr.test(i.options.nextArrow) && i.$nextArrow.remove()), i.$slides && (i.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function() {
      t(this).attr("style", t(this).data("originalStyling"));
    }), i.$slideTrack.children(this.options.slide).detach(), i.$slideTrack.detach(), 
    i.$list.detach(), i.$slider.append(i.$slides)), i.cleanUpRows(), i.$slider.removeClass("slick-slider"), 
    i.$slider.removeClass("slick-initialized"), i.$slider.removeClass("slick-dotted"), 
    i.unslicked = !0, e || i.$slider.trigger("destroy", [ i ]);
  }, e.prototype.disableTransition = function(t) {
    var e = this, i = {};
    i[e.transitionType] = "", !1 === e.options.fade ? e.$slideTrack.css(i) : e.$slides.eq(t).css(i);
  }, e.prototype.fadeSlide = function(t, e) {
    var i = this;
    !1 === i.cssTransitions ? (i.$slides.eq(t).css({
      zIndex: i.options.zIndex
    }), i.$slides.eq(t).animate({
      opacity: 1
    }, i.options.speed, i.options.easing, e)) : (i.applyTransition(t), i.$slides.eq(t).css({
      opacity: 1,
      zIndex: i.options.zIndex
    }), e && setTimeout(function() {
      i.disableTransition(t), e.call();
    }, i.options.speed));
  }, e.prototype.fadeSlideOut = function(t) {
    var e = this;
    !1 === e.cssTransitions ? e.$slides.eq(t).animate({
      opacity: 0,
      zIndex: e.options.zIndex - 2
    }, e.options.speed, e.options.easing) : (e.applyTransition(t), e.$slides.eq(t).css({
      opacity: 0,
      zIndex: e.options.zIndex - 2
    }));
  }, e.prototype.filterSlides = e.prototype.slickFilter = function(t) {
    var e = this;
    null !== t && (e.$slidesCache = e.$slides, e.unload(), e.$slideTrack.children(this.options.slide).detach(), 
    e.$slidesCache.filter(t).appendTo(e.$slideTrack), e.reinit());
  }, e.prototype.focusHandler = function() {
    var e = this;
    e.$slider.off("focus.slick blur.slick").on("focus.slick blur.slick", "*:not(.slick-arrow)", function(i) {
      i.stopImmediatePropagation();
      var n = t(this);
      setTimeout(function() {
        e.options.pauseOnFocus && (e.focussed = n.is(":focus"), e.autoPlay());
      }, 0);
    });
  }, e.prototype.getCurrent = e.prototype.slickCurrentSlide = function() {
    return this.currentSlide;
  }, e.prototype.getDotCount = function() {
    var t = this, e = 0, i = 0, n = 0;
    if (!0 === t.options.infinite) for (;e < t.slideCount; ) ++n, e = i + t.options.slidesToScroll, 
    i += t.options.slidesToScroll <= t.options.slidesToShow ? t.options.slidesToScroll : t.options.slidesToShow; else if (!0 === t.options.centerMode) n = t.slideCount; else if (t.options.asNavFor) for (;e < t.slideCount; ) ++n, 
    e = i + t.options.slidesToScroll, i += t.options.slidesToScroll <= t.options.slidesToShow ? t.options.slidesToScroll : t.options.slidesToShow; else n = 1 + Math.ceil((t.slideCount - t.options.slidesToShow) / t.options.slidesToScroll);
    return n - 1;
  }, e.prototype.getLeft = function(t) {
    var e, i, n, o = this, s = 0;
    return o.slideOffset = 0, i = o.$slides.first().outerHeight(!0), !0 === o.options.infinite ? (o.slideCount > o.options.slidesToShow && (o.slideOffset = o.slideWidth * o.options.slidesToShow * -1, 
    s = i * o.options.slidesToShow * -1), o.slideCount % o.options.slidesToScroll != 0 && t + o.options.slidesToScroll > o.slideCount && o.slideCount > o.options.slidesToShow && (t > o.slideCount ? (o.slideOffset = (o.options.slidesToShow - (t - o.slideCount)) * o.slideWidth * -1, 
    s = (o.options.slidesToShow - (t - o.slideCount)) * i * -1) : (o.slideOffset = o.slideCount % o.options.slidesToScroll * o.slideWidth * -1, 
    s = o.slideCount % o.options.slidesToScroll * i * -1))) : t + o.options.slidesToShow > o.slideCount && (o.slideOffset = (t + o.options.slidesToShow - o.slideCount) * o.slideWidth, 
    s = (t + o.options.slidesToShow - o.slideCount) * i), o.slideCount <= o.options.slidesToShow && (o.slideOffset = 0, 
    s = 0), !0 === o.options.centerMode && !0 === o.options.infinite ? o.slideOffset += o.slideWidth * Math.floor(o.options.slidesToShow / 2) - o.slideWidth : !0 === o.options.centerMode && (o.slideOffset = 0, 
    o.slideOffset += o.slideWidth * Math.floor(o.options.slidesToShow / 2)), e = !1 === o.options.vertical ? t * o.slideWidth * -1 + o.slideOffset : t * i * -1 + s, 
    !0 === o.options.variableWidth && (n = o.slideCount <= o.options.slidesToShow || !1 === o.options.infinite ? o.$slideTrack.children(".slick-slide").eq(t) : o.$slideTrack.children(".slick-slide").eq(t + o.options.slidesToShow), 
    e = !0 === o.options.rtl ? n[0] ? -1 * (o.$slideTrack.width() - n[0].offsetLeft - n.width()) : 0 : n[0] ? -1 * n[0].offsetLeft : 0, 
    !0 === o.options.centerMode && (n = o.slideCount <= o.options.slidesToShow || !1 === o.options.infinite ? o.$slideTrack.children(".slick-slide").eq(t) : o.$slideTrack.children(".slick-slide").eq(t + o.options.slidesToShow + 1), 
    e = !0 === o.options.rtl ? n[0] ? -1 * (o.$slideTrack.width() - n[0].offsetLeft - n.width()) : 0 : n[0] ? -1 * n[0].offsetLeft : 0, 
    e += (o.$list.width() - n.outerWidth()) / 2)), e;
  }, e.prototype.getOption = e.prototype.slickGetOption = function(t) {
    return this.options[t];
  }, e.prototype.getNavigableIndexes = function() {
    var t, e = this, i = 0, n = 0, o = [];
    for (!1 === e.options.infinite ? t = e.slideCount : (i = -1 * e.options.slidesToScroll, 
    n = -1 * e.options.slidesToScroll, t = 2 * e.slideCount); i < t; ) o.push(i), i = n + e.options.slidesToScroll, 
    n += e.options.slidesToScroll <= e.options.slidesToShow ? e.options.slidesToScroll : e.options.slidesToShow;
    return o;
  }, e.prototype.getSlick = function() {
    return this;
  }, e.prototype.getSlideCount = function() {
    var e, i, n = this;
    return i = !0 === n.options.centerMode ? n.slideWidth * Math.floor(n.options.slidesToShow / 2) : 0, 
    !0 === n.options.swipeToSlide ? (n.$slideTrack.find(".slick-slide").each(function(o, s) {
      if (s.offsetLeft - i + t(s).outerWidth() / 2 > -1 * n.swipeLeft) return e = s, !1;
    }), Math.abs(t(e).attr("data-slick-index") - n.currentSlide) || 1) : n.options.slidesToScroll;
  }, e.prototype.goTo = e.prototype.slickGoTo = function(t, e) {
    this.changeSlide({
      data: {
        message: "index",
        index: parseInt(t)
      }
    }, e);
  }, e.prototype.init = function(e) {
    var i = this;
    t(i.$slider).hasClass("slick-initialized") || (t(i.$slider).addClass("slick-initialized"), 
    i.buildRows(), i.buildOut(), i.setProps(), i.startLoad(), i.loadSlider(), i.initializeEvents(), 
    i.updateArrows(), i.updateDots(), i.checkResponsive(!0), i.focusHandler()), e && i.$slider.trigger("init", [ i ]), 
    !0 === i.options.accessibility && i.initADA(), i.options.autoplay && (i.paused = !1, 
    i.autoPlay());
  }, e.prototype.initADA = function() {
    var e = this;
    e.$slides.add(e.$slideTrack.find(".slick-cloned")).attr({
      "aria-hidden": "true",
      tabindex: "-1"
    }).find("a, input, button, select").attr({
      tabindex: "-1"
    }), e.$slideTrack.attr("role", "listbox"), e.$slides.not(e.$slideTrack.find(".slick-cloned")).each(function(i) {
      t(this).attr({
        role: "option",
        "aria-describedby": "slick-slide" + e.instanceUid + i
      });
    }), null !== e.$dots && e.$dots.attr("role", "tablist").find("li").each(function(i) {
      t(this).attr({
        role: "presentation",
        "aria-selected": "false",
        "aria-controls": "navigation" + e.instanceUid + i,
        id: "slick-slide" + e.instanceUid + i
      });
    }).first().attr("aria-selected", "true").end().find("button").attr("role", "button").end().closest("div").attr("role", "toolbar"), 
    e.activateADA();
  }, e.prototype.initArrowEvents = function() {
    var t = this;
    !0 === t.options.arrows && t.slideCount > t.options.slidesToShow && (t.$prevArrow.off("click.slick").on("click.slick", {
      message: "previous"
    }, t.changeSlide), t.$nextArrow.off("click.slick").on("click.slick", {
      message: "next"
    }, t.changeSlide));
  }, e.prototype.initDotEvents = function() {
    var e = this;
    !0 === e.options.dots && e.slideCount > e.options.slidesToShow && t("li", e.$dots).on("click.slick", {
      message: "index"
    }, e.changeSlide), !0 === e.options.dots && !0 === e.options.pauseOnDotsHover && t("li", e.$dots).on("mouseenter.slick", t.proxy(e.interrupt, e, !0)).on("mouseleave.slick", t.proxy(e.interrupt, e, !1));
  }, e.prototype.initSlideEvents = function() {
    var e = this;
    e.options.pauseOnHover && (e.$list.on("mouseenter.slick", t.proxy(e.interrupt, e, !0)), 
    e.$list.on("mouseleave.slick", t.proxy(e.interrupt, e, !1)));
  }, e.prototype.initializeEvents = function() {
    var e = this;
    e.initArrowEvents(), e.initDotEvents(), e.initSlideEvents(), e.$list.on("touchstart.slick mousedown.slick", {
      action: "start"
    }, e.swipeHandler), e.$list.on("touchmove.slick mousemove.slick", {
      action: "move"
    }, e.swipeHandler), e.$list.on("touchend.slick mouseup.slick", {
      action: "end"
    }, e.swipeHandler), e.$list.on("touchcancel.slick mouseleave.slick", {
      action: "end"
    }, e.swipeHandler), e.$list.on("click.slick", e.clickHandler), t(document).on(e.visibilityChange, t.proxy(e.visibility, e)), 
    !0 === e.options.accessibility && e.$list.on("keydown.slick", e.keyHandler), !0 === e.options.focusOnSelect && t(e.$slideTrack).children().on("click.slick", e.selectHandler), 
    t(window).on("orientationchange.slick.slick-" + e.instanceUid, t.proxy(e.orientationChange, e)), 
    t(window).on("resize.slick.slick-" + e.instanceUid, t.proxy(e.resize, e)), t("[draggable!=true]", e.$slideTrack).on("dragstart", e.preventDefault), 
    t(window).on("load.slick.slick-" + e.instanceUid, e.setPosition), t(document).on("ready.slick.slick-" + e.instanceUid, e.setPosition);
  }, e.prototype.initUI = function() {
    var t = this;
    !0 === t.options.arrows && t.slideCount > t.options.slidesToShow && (t.$prevArrow.show(), 
    t.$nextArrow.show()), !0 === t.options.dots && t.slideCount > t.options.slidesToShow && t.$dots.show();
  }, e.prototype.keyHandler = function(t) {
    var e = this;
    t.target.tagName.match("TEXTAREA|INPUT|SELECT") || (37 === t.keyCode && !0 === e.options.accessibility ? e.changeSlide({
      data: {
        message: !0 === e.options.rtl ? "next" : "previous"
      }
    }) : 39 === t.keyCode && !0 === e.options.accessibility && e.changeSlide({
      data: {
        message: !0 === e.options.rtl ? "previous" : "next"
      }
    }));
  }, e.prototype.lazyLoad = function() {
    function e(e) {
      t("img[data-lazy]", e).each(function() {
        var e = t(this), i = t(this).attr("data-lazy"), n = document.createElement("img");
        n.onload = function() {
          e.animate({
            opacity: 0
          }, 100, function() {
            e.attr("src", i).animate({
              opacity: 1
            }, 200, function() {
              e.removeAttr("data-lazy").removeClass("slick-loading");
            }), o.$slider.trigger("lazyLoaded", [ o, e, i ]);
          });
        }, n.onerror = function() {
          e.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), 
          o.$slider.trigger("lazyLoadError", [ o, e, i ]);
        }, n.src = i;
      });
    }
    var i, n, o = this;
    !0 === o.options.centerMode ? !0 === o.options.infinite ? n = (i = o.currentSlide + (o.options.slidesToShow / 2 + 1)) + o.options.slidesToShow + 2 : (i = Math.max(0, o.currentSlide - (o.options.slidesToShow / 2 + 1)), 
    n = o.options.slidesToShow / 2 + 1 + 2 + o.currentSlide) : (i = o.options.infinite ? o.options.slidesToShow + o.currentSlide : o.currentSlide, 
    n = Math.ceil(i + o.options.slidesToShow), !0 === o.options.fade && (i > 0 && i--, 
    n <= o.slideCount && n++)), e(o.$slider.find(".slick-slide").slice(i, n)), o.slideCount <= o.options.slidesToShow ? e(o.$slider.find(".slick-slide")) : o.currentSlide >= o.slideCount - o.options.slidesToShow ? e(o.$slider.find(".slick-cloned").slice(0, o.options.slidesToShow)) : 0 === o.currentSlide && e(o.$slider.find(".slick-cloned").slice(-1 * o.options.slidesToShow));
  }, e.prototype.loadSlider = function() {
    var t = this;
    t.setPosition(), t.$slideTrack.css({
      opacity: 1
    }), t.$slider.removeClass("slick-loading"), t.initUI(), "progressive" === t.options.lazyLoad && t.progressiveLazyLoad();
  }, e.prototype.next = e.prototype.slickNext = function() {
    this.changeSlide({
      data: {
        message: "next"
      }
    });
  }, e.prototype.orientationChange = function() {
    var t = this;
    t.checkResponsive(), t.setPosition();
  }, e.prototype.pause = e.prototype.slickPause = function() {
    var t = this;
    t.autoPlayClear(), t.paused = !0;
  }, e.prototype.play = e.prototype.slickPlay = function() {
    var t = this;
    t.autoPlay(), t.options.autoplay = !0, t.paused = !1, t.focussed = !1, t.interrupted = !1;
  }, e.prototype.postSlide = function(t) {
    var e = this;
    e.unslicked || (e.$slider.trigger("afterChange", [ e, t ]), e.animating = !1, e.setPosition(), 
    e.swipeLeft = null, e.options.autoplay && e.autoPlay(), !0 === e.options.accessibility && e.initADA());
  }, e.prototype.prev = e.prototype.slickPrev = function() {
    this.changeSlide({
      data: {
        message: "previous"
      }
    });
  }, e.prototype.preventDefault = function(t) {
    t.preventDefault();
  }, e.prototype.progressiveLazyLoad = function(e) {
    e = e || 1;
    var i, n, o, s = this, a = t("img[data-lazy]", s.$slider);
    a.length ? (i = a.first(), n = i.attr("data-lazy"), (o = document.createElement("img")).onload = function() {
      i.attr("src", n).removeAttr("data-lazy").removeClass("slick-loading"), !0 === s.options.adaptiveHeight && s.setPosition(), 
      s.$slider.trigger("lazyLoaded", [ s, i, n ]), s.progressiveLazyLoad();
    }, o.onerror = function() {
      e < 3 ? setTimeout(function() {
        s.progressiveLazyLoad(e + 1);
      }, 500) : (i.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), 
      s.$slider.trigger("lazyLoadError", [ s, i, n ]), s.progressiveLazyLoad());
    }, o.src = n) : s.$slider.trigger("allImagesLoaded", [ s ]);
  }, e.prototype.refresh = function(e) {
    var i, n, o = this;
    n = o.slideCount - o.options.slidesToShow, !o.options.infinite && o.currentSlide > n && (o.currentSlide = n), 
    o.slideCount <= o.options.slidesToShow && (o.currentSlide = 0), i = o.currentSlide, 
    o.destroy(!0), t.extend(o, o.initials, {
      currentSlide: i
    }), o.init(), e || o.changeSlide({
      data: {
        message: "index",
        index: i
      }
    }, !1);
  }, e.prototype.registerBreakpoints = function() {
    var e, i, n, o = this, s = o.options.responsive || null;
    if ("array" === t.type(s) && s.length) {
      o.respondTo = o.options.respondTo || "window";
      for (e in s) if (n = o.breakpoints.length - 1, i = s[e].breakpoint, s.hasOwnProperty(e)) {
        for (;n >= 0; ) o.breakpoints[n] && o.breakpoints[n] === i && o.breakpoints.splice(n, 1), 
        n--;
        o.breakpoints.push(i), o.breakpointSettings[i] = s[e].settings;
      }
      o.breakpoints.sort(function(t, e) {
        return o.options.mobileFirst ? t - e : e - t;
      });
    }
  }, e.prototype.reinit = function() {
    var e = this;
    e.$slides = e.$slideTrack.children(e.options.slide).addClass("slick-slide"), e.slideCount = e.$slides.length, 
    e.currentSlide >= e.slideCount && 0 !== e.currentSlide && (e.currentSlide = e.currentSlide - e.options.slidesToScroll), 
    e.slideCount <= e.options.slidesToShow && (e.currentSlide = 0), e.registerBreakpoints(), 
    e.setProps(), e.setupInfinite(), e.buildArrows(), e.updateArrows(), e.initArrowEvents(), 
    e.buildDots(), e.updateDots(), e.initDotEvents(), e.cleanUpSlideEvents(), e.initSlideEvents(), 
    e.checkResponsive(!1, !0), !0 === e.options.focusOnSelect && t(e.$slideTrack).children().on("click.slick", e.selectHandler), 
    e.setSlideClasses("number" == typeof e.currentSlide ? e.currentSlide : 0), e.setPosition(), 
    e.focusHandler(), e.paused = !e.options.autoplay, e.autoPlay(), e.$slider.trigger("reInit", [ e ]);
  }, e.prototype.resize = function() {
    var e = this;
    t(window).width() !== e.windowWidth && (clearTimeout(e.windowDelay), e.windowDelay = window.setTimeout(function() {
      e.windowWidth = t(window).width(), e.checkResponsive(), e.unslicked || e.setPosition();
    }, 50));
  }, e.prototype.removeSlide = e.prototype.slickRemove = function(t, e, i) {
    var n = this;
    if (t = "boolean" == typeof t ? !0 === (e = t) ? 0 : n.slideCount - 1 : !0 === e ? --t : t, 
    n.slideCount < 1 || t < 0 || t > n.slideCount - 1) return !1;
    n.unload(), !0 === i ? n.$slideTrack.children().remove() : n.$slideTrack.children(this.options.slide).eq(t).remove(), 
    n.$slides = n.$slideTrack.children(this.options.slide), n.$slideTrack.children(this.options.slide).detach(), 
    n.$slideTrack.append(n.$slides), n.$slidesCache = n.$slides, n.reinit();
  }, e.prototype.setCSS = function(t) {
    var e, i, n = this, o = {};
    !0 === n.options.rtl && (t = -t), e = "left" == n.positionProp ? Math.ceil(t) + "px" : "0px", 
    i = "top" == n.positionProp ? Math.ceil(t) + "px" : "0px", o[n.positionProp] = t, 
    !1 === n.transformsEnabled ? n.$slideTrack.css(o) : (o = {}, !1 === n.cssTransitions ? (o[n.animType] = "translate(" + e + ", " + i + ")", 
    n.$slideTrack.css(o)) : (o[n.animType] = "translate3d(" + e + ", " + i + ", 0px)", 
    n.$slideTrack.css(o)));
  }, e.prototype.setDimensions = function() {
    var t = this;
    !1 === t.options.vertical ? !0 === t.options.centerMode && t.$list.css({
      padding: "0px " + t.options.centerPadding
    }) : (t.$list.height(t.$slides.first().outerHeight(!0) * t.options.slidesToShow), 
    !0 === t.options.centerMode && t.$list.css({
      padding: t.options.centerPadding + " 0px"
    })), t.listWidth = t.$list.width(), t.listHeight = t.$list.height(), !1 === t.options.vertical && !1 === t.options.variableWidth ? (t.slideWidth = Math.ceil(t.listWidth / t.options.slidesToShow), 
    t.$slideTrack.width(Math.ceil(t.slideWidth * t.$slideTrack.children(".slick-slide").length))) : !0 === t.options.variableWidth ? t.$slideTrack.width(5e3 * t.slideCount) : (t.slideWidth = Math.ceil(t.listWidth), 
    t.$slideTrack.height(Math.ceil(t.$slides.first().outerHeight(!0) * t.$slideTrack.children(".slick-slide").length)));
    var e = t.$slides.first().outerWidth(!0) - t.$slides.first().width();
    !1 === t.options.variableWidth && t.$slideTrack.children(".slick-slide").width(t.slideWidth - e);
  }, e.prototype.setFade = function() {
    var e, i = this;
    i.$slides.each(function(n, o) {
      e = i.slideWidth * n * -1, !0 === i.options.rtl ? t(o).css({
        position: "relative",
        right: e,
        top: 0,
        zIndex: i.options.zIndex - 2,
        opacity: 0
      }) : t(o).css({
        position: "relative",
        left: e,
        top: 0,
        zIndex: i.options.zIndex - 2,
        opacity: 0
      });
    }), i.$slides.eq(i.currentSlide).css({
      zIndex: i.options.zIndex - 1,
      opacity: 1
    });
  }, e.prototype.setHeight = function() {
    var t = this;
    if (1 === t.options.slidesToShow && !0 === t.options.adaptiveHeight && !1 === t.options.vertical) {
      var e = t.$slides.eq(t.currentSlide).outerHeight(!0);
      t.$list.css("height", e);
    }
  }, e.prototype.setOption = e.prototype.slickSetOption = function() {
    var e, i, n, o, s, a = this, r = !1;
    if ("object" === t.type(arguments[0]) ? (n = arguments[0], r = arguments[1], s = "multiple") : "string" === t.type(arguments[0]) && (n = arguments[0], 
    o = arguments[1], r = arguments[2], "responsive" === arguments[0] && "array" === t.type(arguments[1]) ? s = "responsive" : void 0 !== arguments[1] && (s = "single")), 
    "single" === s) a.options[n] = o; else if ("multiple" === s) t.each(n, function(t, e) {
      a.options[t] = e;
    }); else if ("responsive" === s) for (i in o) if ("array" !== t.type(a.options.responsive)) a.options.responsive = [ o[i] ]; else {
      for (e = a.options.responsive.length - 1; e >= 0; ) a.options.responsive[e].breakpoint === o[i].breakpoint && a.options.responsive.splice(e, 1), 
      e--;
      a.options.responsive.push(o[i]);
    }
    r && (a.unload(), a.reinit());
  }, e.prototype.setPosition = function() {
    var t = this;
    t.setDimensions(), t.setHeight(), !1 === t.options.fade ? t.setCSS(t.getLeft(t.currentSlide)) : t.setFade(), 
    t.$slider.trigger("setPosition", [ t ]);
  }, e.prototype.setProps = function() {
    var t = this, e = document.body.style;
    t.positionProp = !0 === t.options.vertical ? "top" : "left", "top" === t.positionProp ? t.$slider.addClass("slick-vertical") : t.$slider.removeClass("slick-vertical"), 
    void 0 === e.WebkitTransition && void 0 === e.MozTransition && void 0 === e.msTransition || !0 === t.options.useCSS && (t.cssTransitions = !0), 
    t.options.fade && ("number" == typeof t.options.zIndex ? t.options.zIndex < 3 && (t.options.zIndex = 3) : t.options.zIndex = t.defaults.zIndex), 
    void 0 !== e.OTransform && (t.animType = "OTransform", t.transformType = "-o-transform", 
    t.transitionType = "OTransition", void 0 === e.perspectiveProperty && void 0 === e.webkitPerspective && (t.animType = !1)), 
    void 0 !== e.MozTransform && (t.animType = "MozTransform", t.transformType = "-moz-transform", 
    t.transitionType = "MozTransition", void 0 === e.perspectiveProperty && void 0 === e.MozPerspective && (t.animType = !1)), 
    void 0 !== e.webkitTransform && (t.animType = "webkitTransform", t.transformType = "-webkit-transform", 
    t.transitionType = "webkitTransition", void 0 === e.perspectiveProperty && void 0 === e.webkitPerspective && (t.animType = !1)), 
    void 0 !== e.msTransform && (t.animType = "msTransform", t.transformType = "-ms-transform", 
    t.transitionType = "msTransition", void 0 === e.msTransform && (t.animType = !1)), 
    void 0 !== e.transform && !1 !== t.animType && (t.animType = "transform", t.transformType = "transform", 
    t.transitionType = "transition"), t.transformsEnabled = t.options.useTransform && null !== t.animType && !1 !== t.animType;
  }, e.prototype.setSlideClasses = function(t) {
    var e, i, n, o, s = this;
    i = s.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden", "true"), 
    s.$slides.eq(t).addClass("slick-current"), !0 === s.options.centerMode ? (e = Math.floor(s.options.slidesToShow / 2), 
    !0 === s.options.infinite && (t >= e && t <= s.slideCount - 1 - e ? s.$slides.slice(t - e, t + e + 1).addClass("slick-active").attr("aria-hidden", "false") : (n = s.options.slidesToShow + t, 
    i.slice(n - e + 1, n + e + 2).addClass("slick-active").attr("aria-hidden", "false")), 
    0 === t ? i.eq(i.length - 1 - s.options.slidesToShow).addClass("slick-center") : t === s.slideCount - 1 && i.eq(s.options.slidesToShow).addClass("slick-center")), 
    s.$slides.eq(t).addClass("slick-center")) : t >= 0 && t <= s.slideCount - s.options.slidesToShow ? s.$slides.slice(t, t + s.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false") : i.length <= s.options.slidesToShow ? i.addClass("slick-active").attr("aria-hidden", "false") : (o = s.slideCount % s.options.slidesToShow, 
    n = !0 === s.options.infinite ? s.options.slidesToShow + t : t, s.options.slidesToShow == s.options.slidesToScroll && s.slideCount - t < s.options.slidesToShow ? i.slice(n - (s.options.slidesToShow - o), n + o).addClass("slick-active").attr("aria-hidden", "false") : i.slice(n, n + s.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false")), 
    "ondemand" === s.options.lazyLoad && s.lazyLoad();
  }, e.prototype.setupInfinite = function() {
    var e, i, n, o = this;
    if (!0 === o.options.fade && (o.options.centerMode = !1), !0 === o.options.infinite && !1 === o.options.fade && (i = null, 
    o.slideCount > o.options.slidesToShow)) {
      for (n = !0 === o.options.centerMode ? o.options.slidesToShow + 1 : o.options.slidesToShow, 
      e = o.slideCount; e > o.slideCount - n; e -= 1) i = e - 1, t(o.$slides[i]).clone(!0).attr("id", "").attr("data-slick-index", i - o.slideCount).prependTo(o.$slideTrack).addClass("slick-cloned");
      for (e = 0; e < n; e += 1) i = e, t(o.$slides[i]).clone(!0).attr("id", "").attr("data-slick-index", i + o.slideCount).appendTo(o.$slideTrack).addClass("slick-cloned");
      o.$slideTrack.find(".slick-cloned").find("[id]").each(function() {
        t(this).attr("id", "");
      });
    }
  }, e.prototype.interrupt = function(t) {
    var e = this;
    t || e.autoPlay(), e.interrupted = t;
  }, e.prototype.selectHandler = function(e) {
    var i = this, n = t(e.target).is(".slick-slide") ? t(e.target) : t(e.target).parents(".slick-slide"), o = parseInt(n.attr("data-slick-index"));
    if (o || (o = 0), i.slideCount <= i.options.slidesToShow) return i.setSlideClasses(o), 
    void i.asNavFor(o);
    i.slideHandler(o);
  }, e.prototype.slideHandler = function(t, e, i) {
    var n, o, s, a, r, l = null, c = this;
    if (e = e || !1, (!0 !== c.animating || !0 !== c.options.waitForAnimate) && !(!0 === c.options.fade && c.currentSlide === t || c.slideCount <= c.options.slidesToShow)) if (!1 === e && c.asNavFor(t), 
    n = t, l = c.getLeft(n), a = c.getLeft(c.currentSlide), c.currentLeft = null === c.swipeLeft ? a : c.swipeLeft, 
    !1 === c.options.infinite && !1 === c.options.centerMode && (t < 0 || t > c.getDotCount() * c.options.slidesToScroll)) !1 === c.options.fade && (n = c.currentSlide, 
    !0 !== i ? c.animateSlide(a, function() {
      c.postSlide(n);
    }) : c.postSlide(n)); else if (!1 === c.options.infinite && !0 === c.options.centerMode && (t < 0 || t > c.slideCount - c.options.slidesToScroll)) !1 === c.options.fade && (n = c.currentSlide, 
    !0 !== i ? c.animateSlide(a, function() {
      c.postSlide(n);
    }) : c.postSlide(n)); else {
      if (c.options.autoplay && clearInterval(c.autoPlayTimer), o = n < 0 ? c.slideCount % c.options.slidesToScroll != 0 ? c.slideCount - c.slideCount % c.options.slidesToScroll : c.slideCount + n : n >= c.slideCount ? c.slideCount % c.options.slidesToScroll != 0 ? 0 : n - c.slideCount : n, 
      c.animating = !0, c.$slider.trigger("beforeChange", [ c, c.currentSlide, o ]), s = c.currentSlide, 
      c.currentSlide = o, c.setSlideClasses(c.currentSlide), c.options.asNavFor && (r = (r = c.getNavTarget()).slick("getSlick")).slideCount <= r.options.slidesToShow && r.setSlideClasses(c.currentSlide), 
      c.updateDots(), c.updateArrows(), !0 === c.options.fade) return !0 !== i ? (c.fadeSlideOut(s), 
      c.fadeSlide(o, function() {
        c.postSlide(o);
      })) : c.postSlide(o), void c.animateHeight();
      !0 !== i ? c.animateSlide(l, function() {
        c.postSlide(o);
      }) : c.postSlide(o);
    }
  }, e.prototype.startLoad = function() {
    var t = this;
    !0 === t.options.arrows && t.slideCount > t.options.slidesToShow && (t.$prevArrow.hide(), 
    t.$nextArrow.hide()), !0 === t.options.dots && t.slideCount > t.options.slidesToShow && t.$dots.hide(), 
    t.$slider.addClass("slick-loading");
  }, e.prototype.swipeDirection = function() {
    var t, e, i, n, o = this;
    return t = o.touchObject.startX - o.touchObject.curX, e = o.touchObject.startY - o.touchObject.curY, 
    i = Math.atan2(e, t), (n = Math.round(180 * i / Math.PI)) < 0 && (n = 360 - Math.abs(n)), 
    n <= 45 && n >= 0 ? !1 === o.options.rtl ? "left" : "right" : n <= 360 && n >= 315 ? !1 === o.options.rtl ? "left" : "right" : n >= 135 && n <= 225 ? !1 === o.options.rtl ? "right" : "left" : !0 === o.options.verticalSwiping ? n >= 35 && n <= 135 ? "down" : "up" : "vertical";
  }, e.prototype.swipeEnd = function(t) {
    var e, i, n = this;
    if (n.dragging = !1, n.interrupted = !1, n.shouldClick = !(n.touchObject.swipeLength > 10), 
    void 0 === n.touchObject.curX) return !1;
    if (!0 === n.touchObject.edgeHit && n.$slider.trigger("edge", [ n, n.swipeDirection() ]), 
    n.touchObject.swipeLength >= n.touchObject.minSwipe) {
      switch (i = n.swipeDirection()) {
       case "left":
       case "down":
        e = n.options.swipeToSlide ? n.checkNavigable(n.currentSlide + n.getSlideCount()) : n.currentSlide + n.getSlideCount(), 
        n.currentDirection = 0;
        break;

       case "right":
       case "up":
        e = n.options.swipeToSlide ? n.checkNavigable(n.currentSlide - n.getSlideCount()) : n.currentSlide - n.getSlideCount(), 
        n.currentDirection = 1;
      }
      "vertical" != i && (n.slideHandler(e), n.touchObject = {}, n.$slider.trigger("swipe", [ n, i ]));
    } else n.touchObject.startX !== n.touchObject.curX && (n.slideHandler(n.currentSlide), 
    n.touchObject = {});
  }, e.prototype.swipeHandler = function(t) {
    var e = this;
    if (!(!1 === e.options.swipe || "ontouchend" in document && !1 === e.options.swipe || !1 === e.options.draggable && -1 !== t.type.indexOf("mouse"))) switch (e.touchObject.fingerCount = t.originalEvent && void 0 !== t.originalEvent.touches ? t.originalEvent.touches.length : 1, 
    e.touchObject.minSwipe = e.listWidth / e.options.touchThreshold, !0 === e.options.verticalSwiping && (e.touchObject.minSwipe = e.listHeight / e.options.touchThreshold), 
    t.data.action) {
     case "start":
      e.swipeStart(t);
      break;

     case "move":
      e.swipeMove(t);
      break;

     case "end":
      e.swipeEnd(t);
    }
  }, e.prototype.swipeMove = function(t) {
    var e, i, n, o, s, a = this;
    return s = void 0 !== t.originalEvent ? t.originalEvent.touches : null, !(!a.dragging || s && 1 !== s.length) && (e = a.getLeft(a.currentSlide), 
    a.touchObject.curX = void 0 !== s ? s[0].pageX : t.clientX, a.touchObject.curY = void 0 !== s ? s[0].pageY : t.clientY, 
    a.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(a.touchObject.curX - a.touchObject.startX, 2))), 
    !0 === a.options.verticalSwiping && (a.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(a.touchObject.curY - a.touchObject.startY, 2)))), 
    "vertical" !== (i = a.swipeDirection()) ? (void 0 !== t.originalEvent && a.touchObject.swipeLength > 4 && t.preventDefault(), 
    o = (!1 === a.options.rtl ? 1 : -1) * (a.touchObject.curX > a.touchObject.startX ? 1 : -1), 
    !0 === a.options.verticalSwiping && (o = a.touchObject.curY > a.touchObject.startY ? 1 : -1), 
    n = a.touchObject.swipeLength, a.touchObject.edgeHit = !1, !1 === a.options.infinite && (0 === a.currentSlide && "right" === i || a.currentSlide >= a.getDotCount() && "left" === i) && (n = a.touchObject.swipeLength * a.options.edgeFriction, 
    a.touchObject.edgeHit = !0), !1 === a.options.vertical ? a.swipeLeft = e + n * o : a.swipeLeft = e + n * (a.$list.height() / a.listWidth) * o, 
    !0 === a.options.verticalSwiping && (a.swipeLeft = e + n * o), !0 !== a.options.fade && !1 !== a.options.touchMove && (!0 === a.animating ? (a.swipeLeft = null, 
    !1) : void a.setCSS(a.swipeLeft))) : void 0);
  }, e.prototype.swipeStart = function(t) {
    var e, i = this;
    if (i.interrupted = !0, 1 !== i.touchObject.fingerCount || i.slideCount <= i.options.slidesToShow) return i.touchObject = {}, 
    !1;
    void 0 !== t.originalEvent && void 0 !== t.originalEvent.touches && (e = t.originalEvent.touches[0]), 
    i.touchObject.startX = i.touchObject.curX = void 0 !== e ? e.pageX : t.clientX, 
    i.touchObject.startY = i.touchObject.curY = void 0 !== e ? e.pageY : t.clientY, 
    i.dragging = !0;
  }, e.prototype.unfilterSlides = e.prototype.slickUnfilter = function() {
    var t = this;
    null !== t.$slidesCache && (t.unload(), t.$slideTrack.children(this.options.slide).detach(), 
    t.$slidesCache.appendTo(t.$slideTrack), t.reinit());
  }, e.prototype.unload = function() {
    var e = this;
    t(".slick-cloned", e.$slider).remove(), e.$dots && e.$dots.remove(), e.$prevArrow && e.htmlExpr.test(e.options.prevArrow) && e.$prevArrow.remove(), 
    e.$nextArrow && e.htmlExpr.test(e.options.nextArrow) && e.$nextArrow.remove(), e.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden", "true").css("width", "");
  }, e.prototype.unslick = function(t) {
    var e = this;
    e.$slider.trigger("unslick", [ e, t ]), e.destroy();
  }, e.prototype.updateArrows = function() {
    var t = this;
    Math.floor(t.options.slidesToShow / 2), !0 === t.options.arrows && t.slideCount > t.options.slidesToShow && !t.options.infinite && (t.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), 
    t.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), 0 === t.currentSlide ? (t.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true"), 
    t.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : t.currentSlide >= t.slideCount - t.options.slidesToShow && !1 === t.options.centerMode ? (t.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), 
    t.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : t.currentSlide >= t.slideCount - 1 && !0 === t.options.centerMode && (t.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), 
    t.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")));
  }, e.prototype.updateDots = function() {
    var t = this;
    null !== t.$dots && (t.$dots.find("li").removeClass("slick-active").attr("aria-hidden", "true"), 
    t.$dots.find("li").eq(Math.floor(t.currentSlide / t.options.slidesToScroll)).addClass("slick-active").attr("aria-hidden", "false"));
  }, e.prototype.visibility = function() {
    var t = this;
    t.options.autoplay && (document[t.hidden] ? t.interrupted = !0 : t.interrupted = !1);
  }, t.fn.slick = function() {
    var t, i, n = this, o = arguments[0], s = Array.prototype.slice.call(arguments, 1), a = n.length;
    for (t = 0; t < a; t++) if ("object" == typeof o || void 0 === o ? n[t].slick = new e(n[t], o) : i = n[t].slick[o].apply(n[t].slick, s), 
    void 0 !== i) return i;
    return n;
  };
}), function(t) {
  "function" == typeof define && define.amd ? define([ "jquery" ], function(e) {
    return t(e, window, document);
  }) : "object" == typeof exports ? module.exports = function(e, i) {
    return e || (e = window), i || (i = "undefined" != typeof window ? require("jquery") : require("jquery")(e)), 
    t(i, e, e.document);
  } : t(jQuery, window, document);
}(function(t, e, i, n) {
  function o(e) {
    var i, n, s = {};
    t.each(e, function(t) {
      (i = t.match(/^([^A-Z]+?)([A-Z])/)) && -1 !== "a aa ai ao as b fn i m o s ".indexOf(i[1] + " ") && (n = t.replace(i[0], i[2].toLowerCase()), 
      s[n] = t, "o" === i[1] && o(e[t]));
    }), e._hungarianMap = s;
  }
  function s(e, i, a) {
    e._hungarianMap || o(e);
    var r;
    t.each(i, function(o) {
      (r = e._hungarianMap[o]) === n || !a && i[r] !== n || ("o" === r.charAt(0) ? (i[r] || (i[r] = {}), 
      t.extend(!0, i[r], i[o]), s(e[r], i[r], a)) : i[r] = i[o]);
    });
  }
  function a(t) {
    var e = Vt.defaults.oLanguage, i = t.sZeroRecords;
    !t.sEmptyTable && i && "No data available in table" === e.sEmptyTable && jt(t, t, "sZeroRecords", "sEmptyTable"), 
    !t.sLoadingRecords && i && "Loading..." === e.sLoadingRecords && jt(t, t, "sZeroRecords", "sLoadingRecords"), 
    t.sInfoThousands && (t.sThousands = t.sInfoThousands), (t = t.sDecimal) && Lt(t);
  }
  function r(t) {
    if (ce(t, "ordering", "bSort"), ce(t, "orderMulti", "bSortMulti"), ce(t, "orderClasses", "bSortClasses"), 
    ce(t, "orderCellsTop", "bSortCellsTop"), ce(t, "order", "aaSorting"), ce(t, "orderFixed", "aaSortingFixed"), 
    ce(t, "paging", "bPaginate"), ce(t, "pagingType", "sPaginationType"), ce(t, "pageLength", "iDisplayLength"), 
    ce(t, "searching", "bFilter"), "boolean" == typeof t.sScrollX && (t.sScrollX = t.sScrollX ? "100%" : ""), 
    "boolean" == typeof t.scrollX && (t.scrollX = t.scrollX ? "100%" : ""), t = t.aoSearchCols) for (var e = 0, i = t.length; e < i; e++) t[e] && s(Vt.models.oSearch, t[e]);
  }
  function l(e) {
    ce(e, "orderable", "bSortable"), ce(e, "orderData", "aDataSort"), ce(e, "orderSequence", "asSorting"), 
    ce(e, "orderDataType", "sortDataType");
    var i = e.aDataSort;
    "number" == typeof i && !t.isArray(i) && (e.aDataSort = [ i ]);
  }
  function c(i) {
    if (!Vt.__browser) {
      var n = {};
      Vt.__browser = n;
      var o = t("<div/>").css({
        position: "fixed",
        top: 0,
        left: -1 * t(e).scrollLeft(),
        height: 1,
        width: 1,
        overflow: "hidden"
      }).append(t("<div/>").css({
        position: "absolute",
        top: 1,
        left: 1,
        width: 100,
        overflow: "scroll"
      }).append(t("<div/>").css({
        width: "100%",
        height: 10
      }))).appendTo("body"), s = o.children(), a = s.children();
      n.barWidth = s[0].offsetWidth - s[0].clientWidth, n.bScrollOversize = 100 === a[0].offsetWidth && 100 !== s[0].clientWidth, 
      n.bScrollbarLeft = 1 !== Math.round(a.offset().left), n.bBounding = !!o[0].getBoundingClientRect().width, 
      o.remove();
    }
    t.extend(i.oBrowser, Vt.__browser), i.oScroll.iBarWidth = Vt.__browser.barWidth;
  }
  function d(t, e, i, o, s, a) {
    var r, l = !1;
    for (i !== n && (r = i, l = !0); o !== s; ) t.hasOwnProperty(o) && (r = l ? e(r, t[o], o, t) : t[o], 
    l = !0, o += a);
    return r;
  }
  function u(e, n) {
    var o = Vt.defaults.column, s = e.aoColumns.length, o = t.extend({}, Vt.models.oColumn, o, {
      nTh: n || i.createElement("th"),
      sTitle: o.sTitle ? o.sTitle : n ? n.innerHTML : "",
      aDataSort: o.aDataSort ? o.aDataSort : [ s ],
      mData: o.mData ? o.mData : s,
      idx: s
    });
    e.aoColumns.push(o), (o = e.aoPreSearchCols)[s] = t.extend({}, Vt.models.oSearch, o[s]), 
    h(e, s, t(n).data());
  }
  function h(e, i, o) {
    var i = e.aoColumns[i], a = e.oClasses, r = t(i.nTh);
    if (!i.sWidthOrig) {
      i.sWidthOrig = r.attr("width") || null;
      var c = (r.attr("style") || "").match(/width:\s*(\d+[pxem%]+)/);
      c && (i.sWidthOrig = c[1]);
    }
    o !== n && null !== o && (l(o), s(Vt.defaults.column, o), o.mDataProp !== n && !o.mData && (o.mData = o.mDataProp), 
    o.sType && (i._sManualType = o.sType), o.className && !o.sClass && (o.sClass = o.className), 
    t.extend(i, o), jt(i, o, "sWidth", "sWidthOrig"), o.iDataSort !== n && (i.aDataSort = [ o.iDataSort ]), 
    jt(i, o, "aDataSort"));
    var d = i.mData, u = $(d), h = i.mRender ? $(i.mRender) : null, o = function(t) {
      return "string" == typeof t && -1 !== t.indexOf("@");
    };
    i._bAttrSrc = t.isPlainObject(d) && (o(d.sort) || o(d.type) || o(d.filter)), i._setter = null, 
    i.fnGetData = function(t, e, i) {
      var o = u(t, e, n, i);
      return h && e ? h(o, e, t, i) : o;
    }, i.fnSetData = function(t, e, i) {
      return A(d)(t, e, i);
    }, "number" != typeof d && (e._rowReadObject = !0), e.oFeatures.bSort || (i.bSortable = !1, 
    r.addClass(a.sSortableNone)), e = -1 !== t.inArray("asc", i.asSorting), o = -1 !== t.inArray("desc", i.asSorting), 
    i.bSortable && (e || o) ? e && !o ? (i.sSortingClass = a.sSortableAsc, i.sSortingClassJUI = a.sSortJUIAscAllowed) : !e && o ? (i.sSortingClass = a.sSortableDesc, 
    i.sSortingClassJUI = a.sSortJUIDescAllowed) : (i.sSortingClass = a.sSortable, i.sSortingClassJUI = a.sSortJUI) : (i.sSortingClass = a.sSortableNone, 
    i.sSortingClassJUI = "");
  }
  function p(t) {
    if (!1 !== t.oFeatures.bAutoWidth) {
      var e = t.aoColumns;
      mt(t);
      for (var i = 0, n = e.length; i < n; i++) e[i].nTh.style.width = e[i].sWidth;
    }
    ("" !== (e = t.oScroll).sY || "" !== e.sX) && pt(t), Ot(t, null, "column-sizing", [ t ]);
  }
  function f(t, e) {
    var i = v(t, "bVisible");
    return "number" == typeof i[e] ? i[e] : null;
  }
  function m(e, i) {
    var n = v(e, "bVisible");
    return -1 !== (n = t.inArray(i, n)) ? n : null;
  }
  function g(e) {
    var i = 0;
    return t.each(e.aoColumns, function(e, n) {
      n.bVisible && "none" !== t(n.nTh).css("display") && i++;
    }), i;
  }
  function v(e, i) {
    var n = [];
    return t.map(e.aoColumns, function(t, e) {
      t[i] && n.push(e);
    }), n;
  }
  function b(t) {
    var e, i, o, s, a, r, l, c, d, u = t.aoColumns, h = t.aoData, p = Vt.ext.type.detect;
    for (e = 0, i = u.length; e < i; e++) if (l = u[e], d = [], !l.sType && l._sManualType) l.sType = l._sManualType; else if (!l.sType) {
      for (o = 0, s = p.length; o < s; o++) {
        for (a = 0, r = h.length; a < r && (d[a] === n && (d[a] = x(t, a, e, "type")), (c = p[o](d[a], t)) || o === p.length - 1) && "html" !== c; a++) ;
        if (c) {
          l.sType = c;
          break;
        }
      }
      l.sType || (l.sType = "string");
    }
  }
  function C(e, i, o, s) {
    var a, r, l, c, d, h, p = e.aoColumns;
    if (i) for (a = i.length - 1; 0 <= a; a--) {
      var f = (h = i[a]).targets !== n ? h.targets : h.aTargets;
      for (t.isArray(f) || (f = [ f ]), r = 0, l = f.length; r < l; r++) if ("number" == typeof f[r] && 0 <= f[r]) {
        for (;p.length <= f[r]; ) u(e);
        s(f[r], h);
      } else if ("number" == typeof f[r] && 0 > f[r]) s(p.length + f[r], h); else if ("string" == typeof f[r]) for (c = 0, 
      d = p.length; c < d; c++) ("_all" == f[r] || t(p[c].nTh).hasClass(f[r])) && s(c, h);
    }
    if (o) for (a = 0, e = o.length; a < e; a++) s(a, o[a]);
  }
  function y(e, i, o, s) {
    var a = e.aoData.length, r = t.extend(!0, {}, Vt.models.oRow, {
      src: o ? "dom" : "data",
      idx: a
    });
    r._aData = i, e.aoData.push(r);
    for (var l = e.aoColumns, c = 0, d = l.length; c < d; c++) l[c].sType = null;
    return e.aiDisplayMaster.push(a), (i = e.rowIdFn(i)) !== n && (e.aIds[i] = r), (o || !e.oFeatures.bDeferRender) && P(e, a, o, s), 
    a;
  }
  function w(e, i) {
    var n;
    return i instanceof t || (i = t(i)), i.map(function(t, i) {
      return n = j(e, i), y(e, n.data, i, n.cells);
    });
  }
  function x(t, e, i, o) {
    var s = t.iDraw, a = t.aoColumns[i], r = t.aoData[e]._aData, l = a.sDefaultContent, c = a.fnGetData(r, o, {
      settings: t,
      row: e,
      col: i
    });
    if (c === n) return t.iDrawError != s && null === l && (It(t, 0, "Requested unknown parameter " + ("function" == typeof a.mData ? "{function}" : "'" + a.mData + "'") + " for row " + e + ", column " + i, 4), 
    t.iDrawError = s), l;
    if (c !== r && null !== c || null === l || o === n) {
      if ("function" == typeof c) return c.call(r);
    } else c = l;
    return null === c && "display" == o ? "" : c;
  }
  function k(t, e, i, n) {
    t.aoColumns[i].fnSetData(t.aoData[e]._aData, n, {
      settings: t,
      row: e,
      col: i
    });
  }
  function _(e) {
    return t.map(e.match(/(\\.|[^\.])+/g) || [ "" ], function(t) {
      return t.replace(/\\\./g, ".");
    });
  }
  function $(e) {
    if (t.isPlainObject(e)) {
      var i = {};
      return t.each(e, function(t, e) {
        e && (i[t] = $(e));
      }), function(t, e, o, s) {
        var a = i[e] || i._;
        return a !== n ? a(t, e, o, s) : t;
      };
    }
    if (null === e) return function(t) {
      return t;
    };
    if ("function" == typeof e) return function(t, i, n, o) {
      return e(t, i, n, o);
    };
    if ("string" == typeof e && (-1 !== e.indexOf(".") || -1 !== e.indexOf("[") || -1 !== e.indexOf("("))) {
      var o = function(e, i, s) {
        var a, r;
        if ("" !== s) for (var l = 0, c = (r = _(s)).length; l < c; l++) {
          if (s = r[l].match(de), a = r[l].match(ue), s) {
            if (r[l] = r[l].replace(de, ""), "" !== r[l] && (e = e[r[l]]), a = [], r.splice(0, l + 1), 
            r = r.join("."), t.isArray(e)) for (l = 0, c = e.length; l < c; l++) a.push(o(e[l], i, r));
            e = "" === (e = s[0].substring(1, s[0].length - 1)) ? a : a.join(e);
            break;
          }
          if (a) r[l] = r[l].replace(ue, ""), e = e[r[l]](); else {
            if (null === e || e[r[l]] === n) return n;
            e = e[r[l]];
          }
        }
        return e;
      };
      return function(t, i) {
        return o(t, i, e);
      };
    }
    return function(t) {
      return t[e];
    };
  }
  function A(e) {
    if (t.isPlainObject(e)) return A(e._);
    if (null === e) return function() {};
    if ("function" == typeof e) return function(t, i, n) {
      e(t, "set", i, n);
    };
    if ("string" == typeof e && (-1 !== e.indexOf(".") || -1 !== e.indexOf("[") || -1 !== e.indexOf("("))) {
      var i = function(e, o, s) {
        var a;
        a = (s = _(s))[s.length - 1];
        for (var r, l, c = 0, d = s.length - 1; c < d; c++) {
          if (r = s[c].match(de), l = s[c].match(ue), r) {
            if (s[c] = s[c].replace(de, ""), e[s[c]] = [], (a = s.slice()).splice(0, c + 1), 
            r = a.join("."), t.isArray(o)) for (l = 0, d = o.length; l < d; l++) a = {}, i(a, o[l], r), 
            e[s[c]].push(a); else e[s[c]] = o;
            return;
          }
          l && (s[c] = s[c].replace(ue, ""), e = e[s[c]](o)), null !== e[s[c]] && e[s[c]] !== n || (e[s[c]] = {}), 
          e = e[s[c]];
        }
        a.match(ue) ? e[a.replace(ue, "")](o) : e[a.replace(de, "")] = o;
      };
      return function(t, n) {
        return i(t, n, e);
      };
    }
    return function(t, i) {
      t[e] = i;
    };
  }
  function S(t) {
    return oe(t.aoData, "_aData");
  }
  function T(t) {
    t.aoData.length = 0, t.aiDisplayMaster.length = 0, t.aiDisplay.length = 0, t.aIds = {};
  }
  function D(t, e, i) {
    for (var o = -1, s = 0, a = t.length; s < a; s++) t[s] == e ? o = s : t[s] > e && t[s]--;
    -1 != o && i === n && t.splice(o, 1);
  }
  function I(t, e, i, o) {
    var s, a = t.aoData[e], r = function(i, n) {
      for (;i.childNodes.length; ) i.removeChild(i.firstChild);
      i.innerHTML = x(t, e, n, "display");
    };
    if ("dom" !== i && (i && "auto" !== i || "dom" !== a.src)) {
      var l = a.anCells;
      if (l) if (o !== n) r(l[o], o); else for (i = 0, s = l.length; i < s; i++) r(l[i], i);
    } else a._aData = j(t, a, o, o === n ? n : a._aData).data;
    if (a._aSortData = null, a._aFilterData = null, r = t.aoColumns, o !== n) r[o].sType = null; else {
      for (i = 0, s = r.length; i < s; i++) r[i].sType = null;
      E(t, a);
    }
  }
  function j(e, i, o, s) {
    var a, r, l, c = [], d = i.firstChild, u = 0, h = e.aoColumns, p = e._rowReadObject, s = s !== n ? s : p ? {} : [], f = function(t, e) {
      if ("string" == typeof t) {
        var i = t.indexOf("@");
        -1 !== i && (i = t.substring(i + 1), A(t)(s, e.getAttribute(i)));
      }
    }, m = function(e) {
      o !== n && o !== u || (r = h[u], l = t.trim(e.innerHTML), r && r._bAttrSrc ? (A(r.mData._)(s, l), 
      f(r.mData.sort, e), f(r.mData.type, e), f(r.mData.filter, e)) : p ? (r._setter || (r._setter = A(r.mData)), 
      r._setter(s, l)) : s[u] = l), u++;
    };
    if (d) for (;d; ) "TD" != (a = d.nodeName.toUpperCase()) && "TH" != a || (m(d), 
    c.push(d)), d = d.nextSibling; else for (d = 0, a = (c = i.anCells).length; d < a; d++) m(c[d]);
    return (i = i.firstChild ? i : i.nTr) && (i = i.getAttribute("id")) && A(e.rowId)(s, i), 
    {
      data: s,
      cells: c
    };
  }
  function P(e, n, o, s) {
    var a, r, l, c, d, u = e.aoData[n], h = u._aData, p = [];
    if (null === u.nTr) {
      for (a = o || i.createElement("tr"), u.nTr = a, u.anCells = p, a._DT_RowIndex = n, 
      E(e, u), c = 0, d = e.aoColumns.length; c < d; c++) l = e.aoColumns[c], (r = o ? s[c] : i.createElement(l.sCellType))._DT_CellIndex = {
        row: n,
        column: c
      }, p.push(r), o && !l.mRender && l.mData === c || t.isPlainObject(l.mData) && l.mData._ === c + ".display" || (r.innerHTML = x(e, n, c, "display")), 
      l.sClass && (r.className += " " + l.sClass), l.bVisible && !o ? a.appendChild(r) : !l.bVisible && o && r.parentNode.removeChild(r), 
      l.fnCreatedCell && l.fnCreatedCell.call(e.oInstance, r, x(e, n, c), h, n, c);
      Ot(e, "aoRowCreatedCallback", null, [ a, h, n ]);
    }
    u.nTr.setAttribute("role", "row");
  }
  function E(e, i) {
    var n = i.nTr, o = i._aData;
    if (n) {
      var s = e.rowIdFn(o);
      s && (n.id = s), o.DT_RowClass && (s = o.DT_RowClass.split(" "), i.__rowc = i.__rowc ? le(i.__rowc.concat(s)) : s, 
      t(n).removeClass(i.__rowc.join(" ")).addClass(o.DT_RowClass)), o.DT_RowAttr && t(n).attr(o.DT_RowAttr), 
      o.DT_RowData && t(n).data(o.DT_RowData);
    }
  }
  function N(e) {
    var i, n, o, s, a, r = e.nTHead, l = e.nTFoot, c = 0 === t("th, td", r).length, d = e.oClasses, u = e.aoColumns;
    for (c && (s = t("<tr/>").appendTo(r)), i = 0, n = u.length; i < n; i++) a = u[i], 
    o = t(a.nTh).addClass(a.sClass), c && o.appendTo(s), e.oFeatures.bSort && (o.addClass(a.sSortingClass), 
    !1 !== a.bSortable && (o.attr("tabindex", e.iTabIndex).attr("aria-controls", e.sTableId), 
    _t(e, a.nTh, i))), a.sTitle != o[0].innerHTML && o.html(a.sTitle), Ft(e, "header")(e, o, a, d);
    if (c && H(e.aoHeader, r), t(r).find(">tr").attr("role", "row"), t(r).find(">tr>th, >tr>td").addClass(d.sHeaderTH), 
    t(l).find(">tr>th, >tr>td").addClass(d.sFooterTH), null !== l) for (i = 0, n = (e = e.aoFooter[0]).length; i < n; i++) a = u[i], 
    a.nTf = e[i].cell, a.sClass && t(a.nTf).addClass(a.sClass);
  }
  function O(e, i, o) {
    var s, a, r, l, c = [], d = [], u = e.aoColumns.length;
    if (i) {
      for (o === n && (o = !1), s = 0, a = i.length; s < a; s++) {
        for (c[s] = i[s].slice(), c[s].nTr = i[s].nTr, r = u - 1; 0 <= r; r--) !e.aoColumns[r].bVisible && !o && c[s].splice(r, 1);
        d.push([]);
      }
      for (s = 0, a = c.length; s < a; s++) {
        if (e = c[s].nTr) for (;r = e.firstChild; ) e.removeChild(r);
        for (r = 0, i = c[s].length; r < i; r++) if (l = u = 1, d[s][r] === n) {
          for (e.appendChild(c[s][r].cell), d[s][r] = 1; c[s + u] !== n && c[s][r].cell == c[s + u][r].cell; ) d[s + u][r] = 1, 
          u++;
          for (;c[s][r + l] !== n && c[s][r].cell == c[s][r + l].cell; ) {
            for (o = 0; o < u; o++) d[s + o][r + l] = 1;
            l++;
          }
          t(c[s][r].cell).attr("rowspan", u).attr("colspan", l);
        }
      }
    }
  }
  function M(e) {
    i = Ot(e, "aoPreDrawCallback", "preDraw", [ e ]);
    if (-1 !== t.inArray(!1, i)) ut(e, !1); else {
      var i = [], o = 0, s = e.asStripeClasses, a = s.length, r = e.oLanguage, l = e.iInitDisplayStart, c = "ssp" == qt(e), d = e.aiDisplay;
      e.bDrawing = !0, l !== n && -1 !== l && (e._iDisplayStart = c ? l : l >= e.fnRecordsDisplay() ? 0 : l, 
      e.iInitDisplayStart = -1);
      var l = e._iDisplayStart, u = e.fnDisplayEnd();
      if (e.bDeferLoading) e.bDeferLoading = !1, e.iDraw++, ut(e, !1); else if (c) {
        if (!e.bDestroying && !R(e)) return;
      } else e.iDraw++;
      if (0 !== d.length) for (r = c ? e.aoData.length : u, c = c ? 0 : l; c < r; c++) {
        var h = d[c], p = e.aoData[h];
        if (null === p.nTr && P(e, h), h = p.nTr, 0 !== a) {
          var f = s[o % a];
          p._sRowStripe != f && (t(h).removeClass(p._sRowStripe).addClass(f), p._sRowStripe = f);
        }
        Ot(e, "aoRowCallback", null, [ h, p._aData, o, c ]), i.push(h), o++;
      } else o = r.sZeroRecords, 1 == e.iDraw && "ajax" == qt(e) ? o = r.sLoadingRecords : r.sEmptyTable && 0 === e.fnRecordsTotal() && (o = r.sEmptyTable), 
      i[0] = t("<tr/>", {
        class: a ? s[0] : ""
      }).append(t("<td />", {
        valign: "top",
        colSpan: g(e),
        class: e.oClasses.sRowEmpty
      }).html(o))[0];
      Ot(e, "aoHeaderCallback", "header", [ t(e.nTHead).children("tr")[0], S(e), l, u, d ]), 
      Ot(e, "aoFooterCallback", "footer", [ t(e.nTFoot).children("tr")[0], S(e), l, u, d ]), 
      (s = t(e.nTBody)).children().detach(), s.append(t(i)), Ot(e, "aoDrawCallback", "draw", [ e ]), 
      e.bSorted = !1, e.bFiltered = !1, e.bDrawing = !1;
    }
  }
  function F(t, e) {
    var i = t.oFeatures, n = i.bFilter;
    i.bSort && wt(t), n ? Q(t, t.oPreviousSearch) : t.aiDisplay = t.aiDisplayMaster.slice(), 
    !0 !== e && (t._iDisplayStart = 0), t._drawHold = e, M(t), t._drawHold = !1;
  }
  function q(e) {
    var i = e.oClasses, n = t(e.nTable), n = t("<div/>").insertBefore(n), o = e.oFeatures, s = t("<div/>", {
      id: e.sTableId + "_wrapper",
      class: i.sWrapper + (e.nTFoot ? "" : " " + i.sNoFooter)
    });
    e.nHolding = n[0], e.nTableWrapper = s[0], e.nTableReinsertBefore = e.nTable.nextSibling;
    for (var a, r, l, c, d, u, h = e.sDom.split(""), p = 0; p < h.length; p++) {
      if (a = null, "<" == (r = h[p])) {
        if (l = t("<div/>")[0], "'" == (c = h[p + 1]) || '"' == c) {
          for (d = "", u = 2; h[p + u] != c; ) d += h[p + u], u++;
          "H" == d ? d = i.sJUIHeader : "F" == d && (d = i.sJUIFooter), -1 != d.indexOf(".") ? (c = d.split("."), 
          l.id = c[0].substr(1, c[0].length - 1), l.className = c[1]) : "#" == d.charAt(0) ? l.id = d.substr(1, d.length - 1) : l.className = d, 
          p += u;
        }
        s.append(l), s = t(l);
      } else if (">" == r) s = s.parent(); else if ("l" == r && o.bPaginate && o.bLengthChange) a = rt(e); else if ("f" == r && o.bFilter) a = V(e); else if ("r" == r && o.bProcessing) a = dt(e); else if ("t" == r) a = ht(e); else if ("i" == r && o.bInfo) a = et(e); else if ("p" == r && o.bPaginate) a = lt(e); else if (0 !== Vt.ext.feature.length) for (u = 0, 
      c = (l = Vt.ext.feature).length; u < c; u++) if (r == l[u].cFeature) {
        a = l[u].fnInit(e);
        break;
      }
      a && ((l = e.aanFeatures)[r] || (l[r] = []), l[r].push(a), s.append(a));
    }
    n.replaceWith(s), e.nHolding = null;
  }
  function H(e, i) {
    var n, o, s, a, r, l, c, d, u, h, p = t(i).children("tr");
    for (e.splice(0, e.length), s = 0, l = p.length; s < l; s++) e.push([]);
    for (s = 0, l = p.length; s < l; s++) for (o = (n = p[s]).firstChild; o; ) {
      if ("TD" == o.nodeName.toUpperCase() || "TH" == o.nodeName.toUpperCase()) {
        for (d = 1 * o.getAttribute("colspan"), u = 1 * o.getAttribute("rowspan"), d = d && 0 !== d && 1 !== d ? d : 1, 
        u = u && 0 !== u && 1 !== u ? u : 1, a = 0, r = e[s]; r[a]; ) a++;
        for (c = a, h = 1 === d, r = 0; r < d; r++) for (a = 0; a < u; a++) e[s + a][c + r] = {
          cell: o,
          unique: h
        }, e[s + a].nTr = n;
      }
      o = o.nextSibling;
    }
  }
  function L(t, e, i) {
    var n = [];
    i || (i = t.aoHeader, e && (i = [], H(i, e)));
    for (var e = 0, o = i.length; e < o; e++) for (var s = 0, a = i[e].length; s < a; s++) !i[e][s].unique || n[s] && t.bSortCellsTop || (n[s] = i[e][s].cell);
    return n;
  }
  function W(e, i, n) {
    if (Ot(e, "aoServerParams", "serverParams", [ i ]), i && t.isArray(i)) {
      var o = {}, s = /(.*?)\[\]$/;
      t.each(i, function(t, e) {
        var i = e.name.match(s);
        i ? (i = i[0], o[i] || (o[i] = []), o[i].push(e.value)) : o[e.name] = e.value;
      }), i = o;
    }
    var a, r = e.ajax, l = e.oInstance, c = function(t) {
      Ot(e, null, "xhr", [ e, t, e.jqXHR ]), n(t);
    };
    if (t.isPlainObject(r) && r.data) {
      a = r.data;
      var d = t.isFunction(a) ? a(i, e) : a, i = t.isFunction(a) && d ? d : t.extend(!0, i, d);
      delete r.data;
    }
    d = {
      data: i,
      success: function(t) {
        var i = t.error || t.sError;
        i && It(e, 0, i), e.json = t, c(t);
      },
      dataType: "json",
      cache: !1,
      type: e.sServerMethod,
      error: function(i, n) {
        var o = Ot(e, null, "xhr", [ e, null, e.jqXHR ]);
        -1 === t.inArray(!0, o) && ("parsererror" == n ? It(e, 0, "Invalid JSON response", 1) : 4 === i.readyState && It(e, 0, "Ajax error", 7)), 
        ut(e, !1);
      }
    }, e.oAjaxData = i, Ot(e, null, "preXhr", [ e, i ]), e.fnServerData ? e.fnServerData.call(l, e.sAjaxSource, t.map(i, function(t, e) {
      return {
        name: e,
        value: t
      };
    }), c, e) : e.sAjaxSource || "string" == typeof r ? e.jqXHR = t.ajax(t.extend(d, {
      url: r || e.sAjaxSource
    })) : t.isFunction(r) ? e.jqXHR = r.call(l, i, c, e) : (e.jqXHR = t.ajax(t.extend(d, r)), 
    r.data = a);
  }
  function R(t) {
    return !t.bAjaxDataGet || (t.iDraw++, ut(t, !0), W(t, z(t), function(e) {
      B(t, e);
    }), !1);
  }
  function z(e) {
    var i, n, o, s, a = e.aoColumns, r = a.length, l = e.oFeatures, c = e.oPreviousSearch, d = e.aoPreSearchCols, u = [], h = yt(e);
    i = e._iDisplayStart, n = !1 !== l.bPaginate ? e._iDisplayLength : -1;
    var p = function(t, e) {
      u.push({
        name: t,
        value: e
      });
    };
    p("sEcho", e.iDraw), p("iColumns", r), p("sColumns", oe(a, "sName").join(",")), 
    p("iDisplayStart", i), p("iDisplayLength", n);
    var f = {
      draw: e.iDraw,
      columns: [],
      order: [],
      start: i,
      length: n,
      search: {
        value: c.sSearch,
        regex: c.bRegex
      }
    };
    for (i = 0; i < r; i++) o = a[i], s = d[i], n = "function" == typeof o.mData ? "function" : o.mData, 
    f.columns.push({
      data: n,
      name: o.sName,
      searchable: o.bSearchable,
      orderable: o.bSortable,
      search: {
        value: s.sSearch,
        regex: s.bRegex
      }
    }), p("mDataProp_" + i, n), l.bFilter && (p("sSearch_" + i, s.sSearch), p("bRegex_" + i, s.bRegex), 
    p("bSearchable_" + i, o.bSearchable)), l.bSort && p("bSortable_" + i, o.bSortable);
    return l.bFilter && (p("sSearch", c.sSearch), p("bRegex", c.bRegex)), l.bSort && (t.each(h, function(t, e) {
      f.order.push({
        column: e.col,
        dir: e.dir
      }), p("iSortCol_" + t, e.col), p("sSortDir_" + t, e.dir);
    }), p("iSortingCols", h.length)), null === (a = Vt.ext.legacy.ajax) ? e.sAjaxSource ? u : f : a ? u : f;
  }
  function B(t, e) {
    var i = U(t, e), o = e.sEcho !== n ? e.sEcho : e.draw, s = e.iTotalRecords !== n ? e.iTotalRecords : e.recordsTotal, a = e.iTotalDisplayRecords !== n ? e.iTotalDisplayRecords : e.recordsFiltered;
    if (o) {
      if (1 * o < t.iDraw) return;
      t.iDraw = 1 * o;
    }
    for (T(t), t._iRecordsTotal = parseInt(s, 10), t._iRecordsDisplay = parseInt(a, 10), 
    o = 0, s = i.length; o < s; o++) y(t, i[o]);
    t.aiDisplay = t.aiDisplayMaster.slice(), t.bAjaxDataGet = !1, M(t), t._bInitComplete || st(t, e), 
    t.bAjaxDataGet = !0, ut(t, !1);
  }
  function U(e, i) {
    var o = t.isPlainObject(e.ajax) && e.ajax.dataSrc !== n ? e.ajax.dataSrc : e.sAjaxDataProp;
    return "data" === o ? i.aaData || i[o] : "" !== o ? $(o)(i) : i;
  }
  function V(e) {
    var n = e.oClasses, o = e.sTableId, s = e.oLanguage, a = e.oPreviousSearch, r = e.aanFeatures, l = '<input type="search" class="' + n.sFilterInput + '"/>', c = (c = s.sSearch).match(/_INPUT_/) ? c.replace("_INPUT_", l) : c + l, n = t("<div/>", {
      id: r.f ? null : o + "_filter",
      class: n.sFilter
    }).append(t("<label/>").append(c)), r = function() {
      var t = this.value ? this.value : "";
      t != a.sSearch && (Q(e, {
        sSearch: t,
        bRegex: a.bRegex,
        bSmart: a.bSmart,
        bCaseInsensitive: a.bCaseInsensitive
      }), e._iDisplayStart = 0, M(e));
    }, l = null !== e.searchDelay ? e.searchDelay : "ssp" === qt(e) ? 400 : 0, d = t("input", n).val(a.sSearch).attr("placeholder", s.sSearchPlaceholder).on("keyup.DT search.DT input.DT paste.DT cut.DT", l ? ge(r, l) : r).on("keypress.DT", function(t) {
      if (13 == t.keyCode) return !1;
    }).attr("aria-controls", o);
    return t(e.nTable).on("search.dt.DT", function(t, n) {
      if (e === n) try {
        d[0] !== i.activeElement && d.val(a.sSearch);
      } catch (t) {}
    }), n[0];
  }
  function Q(t, e, i) {
    var o = t.oPreviousSearch, s = t.aoPreSearchCols, a = function(t) {
      o.sSearch = t.sSearch, o.bRegex = t.bRegex, o.bSmart = t.bSmart, o.bCaseInsensitive = t.bCaseInsensitive;
    };
    if (b(t), "ssp" != qt(t)) {
      for (G(t, e.sSearch, i, e.bEscapeRegex !== n ? !e.bEscapeRegex : e.bRegex, e.bSmart, e.bCaseInsensitive), 
      a(e), e = 0; e < s.length; e++) Y(t, s[e].sSearch, e, s[e].bEscapeRegex !== n ? !s[e].bEscapeRegex : s[e].bRegex, s[e].bSmart, s[e].bCaseInsensitive);
      X(t);
    } else a(e);
    t.bFiltered = !0, Ot(t, null, "search", [ t ]);
  }
  function X(e) {
    for (var i, n, o = Vt.ext.search, s = e.aiDisplay, a = 0, r = o.length; a < r; a++) {
      for (var l = [], c = 0, d = s.length; c < d; c++) n = s[c], i = e.aoData[n], o[a](e, i._aFilterData, n, i._aData, c) && l.push(n);
      s.length = 0, t.merge(s, l);
    }
  }
  function Y(t, e, i, n, o, s) {
    if ("" !== e) {
      for (var a = [], r = t.aiDisplay, n = K(e, n, o, s), o = 0; o < r.length; o++) e = t.aoData[r[o]]._aFilterData[i], 
      n.test(e) && a.push(r[o]);
      t.aiDisplay = a;
    }
  }
  function G(t, e, i, n, o, s) {
    var a, n = K(e, n, o, s), s = t.oPreviousSearch.sSearch, r = t.aiDisplayMaster, o = [];
    if (0 !== Vt.ext.search.length && (i = !0), a = J(t), 0 >= e.length) t.aiDisplay = r.slice(); else {
      for ((a || i || s.length > e.length || 0 !== e.indexOf(s) || t.bSorted) && (t.aiDisplay = r.slice()), 
      e = t.aiDisplay, i = 0; i < e.length; i++) n.test(t.aoData[e[i]]._sFilterRow) && o.push(e[i]);
      t.aiDisplay = o;
    }
  }
  function K(e, i, n, o) {
    return e = i ? e : he(e), n && (e = "^(?=.*?" + t.map(e.match(/"[^"]+"|[^ ]+/g) || [ "" ], function(t) {
      if ('"' === t.charAt(0)) var e = t.match(/^"(.*)"$/), t = e ? e[1] : t;
      return t.replace('"', "");
    }).join(")(?=.*?") + ").*$"), RegExp(e, o ? "i" : "");
  }
  function J(t) {
    var e, i, n, o, s, a, r, l, c = t.aoColumns, d = Vt.ext.type.search;
    for (e = !1, i = 0, o = t.aoData.length; i < o; i++) if (!(l = t.aoData[i])._aFilterData) {
      for (a = [], n = 0, s = c.length; n < s; n++) (e = c[n]).bSearchable ? (r = x(t, i, n, "filter"), 
      d[e.sType] && (r = d[e.sType](r)), null === r && (r = ""), "string" != typeof r && r.toString && (r = r.toString())) : r = "", 
      r.indexOf && -1 !== r.indexOf("&") && (pe.innerHTML = r, r = fe ? pe.textContent : pe.innerText), 
      r.replace && (r = r.replace(/[\r\n]/g, "")), a.push(r);
      l._aFilterData = a, l._sFilterRow = a.join("  "), e = !0;
    }
    return e;
  }
  function Z(t) {
    return {
      search: t.sSearch,
      smart: t.bSmart,
      regex: t.bRegex,
      caseInsensitive: t.bCaseInsensitive
    };
  }
  function tt(t) {
    return {
      sSearch: t.search,
      bSmart: t.smart,
      bRegex: t.regex,
      bCaseInsensitive: t.caseInsensitive
    };
  }
  function et(e) {
    var i = e.sTableId, n = e.aanFeatures.i, o = t("<div/>", {
      class: e.oClasses.sInfo,
      id: n ? null : i + "_info"
    });
    return n || (e.aoDrawCallback.push({
      fn: it,
      sName: "information"
    }), o.attr("role", "status").attr("aria-live", "polite"), t(e.nTable).attr("aria-describedby", i + "_info")), 
    o[0];
  }
  function it(e) {
    var i = e.aanFeatures.i;
    if (0 !== i.length) {
      var n = e.oLanguage, o = e._iDisplayStart + 1, s = e.fnDisplayEnd(), a = e.fnRecordsTotal(), r = e.fnRecordsDisplay(), l = r ? n.sInfo : n.sInfoEmpty;
      r !== a && (l += " " + n.sInfoFiltered), l = nt(e, l += n.sInfoPostFix), null !== (n = n.fnInfoCallback) && (l = n.call(e.oInstance, e, o, s, a, r, l)), 
      t(i).html(l);
    }
  }
  function nt(t, e) {
    var i = t.fnFormatNumber, n = t._iDisplayStart + 1, o = t._iDisplayLength, s = t.fnRecordsDisplay(), a = -1 === o;
    return e.replace(/_START_/g, i.call(t, n)).replace(/_END_/g, i.call(t, t.fnDisplayEnd())).replace(/_MAX_/g, i.call(t, t.fnRecordsTotal())).replace(/_TOTAL_/g, i.call(t, s)).replace(/_PAGE_/g, i.call(t, a ? 1 : Math.ceil(n / o))).replace(/_PAGES_/g, i.call(t, a ? 1 : Math.ceil(s / o)));
  }
  function ot(t) {
    var e, i, n, o = t.iInitDisplayStart, s = t.aoColumns;
    i = t.oFeatures;
    var a = t.bDeferLoading;
    if (t.bInitialised) {
      for (q(t), N(t), O(t, t.aoHeader), O(t, t.aoFooter), ut(t, !0), i.bAutoWidth && mt(t), 
      e = 0, i = s.length; e < i; e++) (n = s[e]).sWidth && (n.nTh.style.width = Ct(n.sWidth));
      Ot(t, null, "preInit", [ t ]), F(t), ("ssp" != (s = qt(t)) || a) && ("ajax" == s ? W(t, [], function(i) {
        var n = U(t, i);
        for (e = 0; e < n.length; e++) y(t, n[e]);
        t.iInitDisplayStart = o, F(t), ut(t, !1), st(t, i);
      }, t) : (ut(t, !1), st(t)));
    } else setTimeout(function() {
      ot(t);
    }, 200);
  }
  function st(t, e) {
    t._bInitComplete = !0, (e || t.oInit.aaData) && p(t), Ot(t, null, "plugin-init", [ t, e ]), 
    Ot(t, "aoInitComplete", "init", [ t, e ]);
  }
  function at(t, e) {
    var i = parseInt(e, 10);
    t._iDisplayLength = i, Mt(t), Ot(t, null, "length", [ t, i ]);
  }
  function rt(e) {
    for (var i = e.oClasses, n = e.sTableId, o = e.aLengthMenu, s = (a = t.isArray(o[0])) ? o[0] : o, o = a ? o[1] : o, a = t("<select/>", {
      name: n + "_length",
      "aria-controls": n,
      class: i.sLengthSelect
    }), r = 0, l = s.length; r < l; r++) a[0][r] = new Option(o[r], s[r]);
    var c = t("<div><label/></div>").addClass(i.sLength);
    return e.aanFeatures.l || (c[0].id = n + "_length"), c.children().append(e.oLanguage.sLengthMenu.replace("_MENU_", a[0].outerHTML)), 
    t("select", c).val(e._iDisplayLength).on("change.DT", function() {
      at(e, t(this).val()), M(e);
    }), t(e.nTable).on("length.dt.DT", function(i, n, o) {
      e === n && t("select", c).val(o);
    }), c[0];
  }
  function lt(e) {
    var i = e.sPaginationType, n = Vt.ext.pager[i], o = "function" == typeof n, s = function(t) {
      M(t);
    }, i = t("<div/>").addClass(e.oClasses.sPaging + i)[0], a = e.aanFeatures;
    return o || n.fnInit(e, i, s), a.p || (i.id = e.sTableId + "_paginate", e.aoDrawCallback.push({
      fn: function(t) {
        if (o) {
          var e, i = t._iDisplayStart, r = t._iDisplayLength, l = t.fnRecordsDisplay(), i = (c = -1 === r) ? 0 : Math.ceil(i / r), r = c ? 1 : Math.ceil(l / r), l = n(i, r), c = 0;
          for (e = a.p.length; c < e; c++) Ft(t, "pageButton")(t, a.p[c], c, l, i, r);
        } else n.fnUpdate(t, s);
      },
      sName: "pagination"
    })), i;
  }
  function ct(t, e, i) {
    var n = t._iDisplayStart, o = t._iDisplayLength, s = t.fnRecordsDisplay();
    return 0 === s || -1 === o ? n = 0 : "number" == typeof e ? (n = e * o) > s && (n = 0) : "first" == e ? n = 0 : "previous" == e ? 0 > (n = 0 <= o ? n - o : 0) && (n = 0) : "next" == e ? n + o < s && (n += o) : "last" == e ? n = Math.floor((s - 1) / o) * o : It(t, 0, "Unknown paging action: " + e, 5), 
    e = t._iDisplayStart !== n, t._iDisplayStart = n, e && (Ot(t, null, "page", [ t ]), 
    i && M(t)), e;
  }
  function dt(e) {
    return t("<div/>", {
      id: e.aanFeatures.r ? null : e.sTableId + "_processing",
      class: e.oClasses.sProcessing
    }).html(e.oLanguage.sProcessing).insertBefore(e.nTable)[0];
  }
  function ut(e, i) {
    e.oFeatures.bProcessing && t(e.aanFeatures.r).css("display", i ? "block" : "none"), 
    Ot(e, null, "processing", [ e, i ]);
  }
  function ht(e) {
    var i = t(e.nTable);
    i.attr("role", "grid");
    var n = e.oScroll;
    if ("" === n.sX && "" === n.sY) return e.nTable;
    var o = n.sX, s = n.sY, a = e.oClasses, r = i.children("caption"), l = r.length ? r[0]._captionSide : null, c = t(i[0].cloneNode(!1)), d = t(i[0].cloneNode(!1)), u = i.children("tfoot");
    u.length || (u = null), c = t("<div/>", {
      class: a.sScrollWrapper
    }).append(t("<div/>", {
      class: a.sScrollHead
    }).css({
      overflow: "hidden",
      position: "relative",
      border: 0,
      width: o ? o ? Ct(o) : null : "100%"
    }).append(t("<div/>", {
      class: a.sScrollHeadInner
    }).css({
      "box-sizing": "content-box",
      width: n.sXInner || "100%"
    }).append(c.removeAttr("id").css("margin-left", 0).append("top" === l ? r : null).append(i.children("thead"))))).append(t("<div/>", {
      class: a.sScrollBody
    }).css({
      position: "relative",
      overflow: "auto",
      width: o ? Ct(o) : null
    }).append(i)), u && c.append(t("<div/>", {
      class: a.sScrollFoot
    }).css({
      overflow: "hidden",
      border: 0,
      width: o ? o ? Ct(o) : null : "100%"
    }).append(t("<div/>", {
      class: a.sScrollFootInner
    }).append(d.removeAttr("id").css("margin-left", 0).append("bottom" === l ? r : null).append(i.children("tfoot")))));
    var h = (i = c.children())[0], a = i[1], p = u ? i[2] : null;
    return o && t(a).on("scroll.DT", function() {
      var t = this.scrollLeft;
      h.scrollLeft = t, u && (p.scrollLeft = t);
    }), t(a).css(s && n.bCollapse ? "max-height" : "height", s), e.nScrollHead = h, 
    e.nScrollBody = a, e.nScrollFoot = p, e.aoDrawCallback.push({
      fn: pt,
      sName: "scrolling"
    }), c[0];
  }
  function pt(e) {
    var i, o, s, a, r, l = (u = e.oScroll).sX, c = u.sXInner, d = u.sY, u = u.iBarWidth, h = t(e.nScrollHead), m = h[0].style, g = (b = h.children("div"))[0].style, v = b.children("table"), b = e.nScrollBody, C = t(b), y = b.style, w = t(e.nScrollFoot).children("div"), x = w.children("table"), k = t(e.nTHead), _ = t(e.nTable), $ = _[0], A = $.style, S = e.nTFoot ? t(e.nTFoot) : null, T = e.oBrowser, D = T.bScrollOversize, I = oe(e.aoColumns, "nTh"), j = [], P = [], E = [], N = [], O = function(t) {
      (t = t.style).paddingTop = "0", t.paddingBottom = "0", t.borderTopWidth = "0", t.borderBottomWidth = "0", 
      t.height = 0;
    };
    o = b.scrollHeight > b.clientHeight, e.scrollBarVis !== o && e.scrollBarVis !== n ? (e.scrollBarVis = o, 
    p(e)) : (e.scrollBarVis = o, _.children("thead, tfoot").remove(), S && (s = S.clone().prependTo(_), 
    i = S.find("tr"), s = s.find("tr")), a = k.clone().prependTo(_), k = k.find("tr"), 
    o = a.find("tr"), a.find("th, td").removeAttr("tabindex"), l || (y.width = "100%", 
    h[0].style.width = "100%"), t.each(L(e, a), function(t, i) {
      r = f(e, t), i.style.width = e.aoColumns[r].sWidth;
    }), S && ft(function(t) {
      t.style.width = "";
    }, s), h = _.outerWidth(), "" === l ? (A.width = "100%", D && (_.find("tbody").height() > b.offsetHeight || "scroll" == C.css("overflow-y")) && (A.width = Ct(_.outerWidth() - u)), 
    h = _.outerWidth()) : "" !== c && (A.width = Ct(c), h = _.outerWidth()), ft(O, o), 
    ft(function(e) {
      E.push(e.innerHTML), j.push(Ct(t(e).css("width")));
    }, o), ft(function(e, i) {
      -1 !== t.inArray(e, I) && (e.style.width = j[i]);
    }, k), t(o).height(0), S && (ft(O, s), ft(function(e) {
      N.push(e.innerHTML), P.push(Ct(t(e).css("width")));
    }, s), ft(function(t, e) {
      t.style.width = P[e];
    }, i), t(s).height(0)), ft(function(t, e) {
      t.innerHTML = '<div class="dataTables_sizing" style="height:0;overflow:hidden;">' + E[e] + "</div>", 
      t.style.width = j[e];
    }, o), S && ft(function(t, e) {
      t.innerHTML = '<div class="dataTables_sizing" style="height:0;overflow:hidden;">' + N[e] + "</div>", 
      t.style.width = P[e];
    }, s), _.outerWidth() < h ? (i = b.scrollHeight > b.offsetHeight || "scroll" == C.css("overflow-y") ? h + u : h, 
    D && (b.scrollHeight > b.offsetHeight || "scroll" == C.css("overflow-y")) && (A.width = Ct(i - u)), 
    ("" === l || "" !== c) && It(e, 1, "Possible column misalignment", 6)) : i = "100%", 
    y.width = Ct(i), m.width = Ct(i), S && (e.nScrollFoot.style.width = Ct(i)), !d && D && (y.height = Ct($.offsetHeight + u)), 
    l = _.outerWidth(), v[0].style.width = Ct(l), g.width = Ct(l), c = _.height() > b.clientHeight || "scroll" == C.css("overflow-y"), 
    g[d = "padding" + (T.bScrollbarLeft ? "Left" : "Right")] = c ? u + "px" : "0px", 
    S && (x[0].style.width = Ct(l), w[0].style.width = Ct(l), w[0].style[d] = c ? u + "px" : "0px"), 
    _.children("colgroup").insertBefore(_.children("thead")), C.scroll(), !e.bSorted && !e.bFiltered || e._drawHold || (b.scrollTop = 0));
  }
  function ft(t, e, i) {
    for (var n, o, s = 0, a = 0, r = e.length; a < r; ) {
      for (n = e[a].firstChild, o = i ? i[a].firstChild : null; n; ) 1 === n.nodeType && (i ? t(n, o, s) : t(n, s), 
      s++), n = n.nextSibling, o = i ? o.nextSibling : null;
      a++;
    }
  }
  function mt(i) {
    var n, o, s = i.nTable, a = i.aoColumns, r = (w = i.oScroll).sY, l = w.sX, c = w.sXInner, d = a.length, u = v(i, "bVisible"), h = t("th", i.nTHead), m = s.getAttribute("width"), b = s.parentNode, C = !1, y = i.oBrowser, w = y.bScrollOversize;
    for ((n = s.style.width) && -1 !== n.indexOf("%") && (m = n), n = 0; n < u.length; n++) null !== (o = a[u[n]]).sWidth && (o.sWidth = gt(o.sWidthOrig, b), 
    C = !0);
    if (w || !C && !l && !r && d == g(i) && d == h.length) for (n = 0; n < d; n++) null !== (u = f(i, n)) && (a[u].sWidth = Ct(h.eq(n).width())); else {
      (d = t(s).clone().css("visibility", "hidden").removeAttr("id")).find("tbody tr").remove();
      var x = t("<tr/>").appendTo(d.find("tbody"));
      for (d.find("thead, tfoot").remove(), d.append(t(i.nTHead).clone()).append(t(i.nTFoot).clone()), 
      d.find("tfoot th, tfoot td").css("width", ""), h = L(i, d.find("thead")[0]), n = 0; n < u.length; n++) o = a[u[n]], 
      h[n].style.width = null !== o.sWidthOrig && "" !== o.sWidthOrig ? Ct(o.sWidthOrig) : "", 
      o.sWidthOrig && l && t(h[n]).append(t("<div/>").css({
        width: o.sWidthOrig,
        margin: 0,
        padding: 0,
        border: 0,
        height: 1
      }));
      if (i.aoData.length) for (n = 0; n < u.length; n++) C = u[n], o = a[C], t(vt(i, C)).clone(!1).append(o.sContentPadding).appendTo(x);
      for (t("[name]", d).removeAttr("name"), o = t("<div/>").css(l || r ? {
        position: "absolute",
        top: 0,
        left: 0,
        height: 1,
        right: 0,
        overflow: "hidden"
      } : {}).append(d).appendTo(b), l && c ? d.width(c) : l ? (d.css("width", "auto"), 
      d.removeAttr("width"), d.width() < b.clientWidth && m && d.width(b.clientWidth)) : r ? d.width(b.clientWidth) : m && d.width(m), 
      n = r = 0; n < u.length; n++) b = t(h[n]), c = b.outerWidth() - b.width(), b = y.bBounding ? Math.ceil(h[n].getBoundingClientRect().width) : b.outerWidth(), 
      r += b, a[u[n]].sWidth = Ct(b - c);
      s.style.width = Ct(r), o.remove();
    }
    m && (s.style.width = Ct(m)), !m && !l || i._reszEvt || (s = function() {
      t(e).on("resize.DT-" + i.sInstance, ge(function() {
        p(i);
      }));
    }, w ? setTimeout(s, 1e3) : s(), i._reszEvt = !0);
  }
  function gt(e, n) {
    if (!e) return 0;
    var o = t("<div/>").css("width", Ct(e)).appendTo(n || i.body), s = o[0].offsetWidth;
    return o.remove(), s;
  }
  function vt(e, i) {
    var n = bt(e, i);
    if (0 > n) return null;
    var o = e.aoData[n];
    return o.nTr ? o.anCells[i] : t("<td/>").html(x(e, n, i, "display"))[0];
  }
  function bt(t, e) {
    for (var i, n = -1, o = -1, s = 0, a = t.aoData.length; s < a; s++) i = x(t, s, e, "display") + "", 
    i = i.replace(me, ""), (i = i.replace(/&nbsp;/g, " ")).length > n && (n = i.length, 
    o = s);
    return o;
  }
  function Ct(t) {
    return null === t ? "0px" : "number" == typeof t ? 0 > t ? "0px" : t + "px" : t.match(/\d$/) ? t + "px" : t;
  }
  function yt(e) {
    var i, o, s, a, r, l, c = [], d = e.aoColumns;
    i = e.aaSortingFixed, o = t.isPlainObject(i);
    var u = [];
    for (s = function(e) {
      e.length && !t.isArray(e[0]) ? u.push(e) : t.merge(u, e);
    }, t.isArray(i) && s(i), o && i.pre && s(i.pre), s(e.aaSorting), o && i.post && s(i.post), 
    e = 0; e < u.length; e++) for (i = 0, o = (s = d[l = u[e][0]].aDataSort).length; i < o; i++) a = s[i], 
    r = d[a].sType || "string", u[e]._idx === n && (u[e]._idx = t.inArray(u[e][1], d[a].asSorting)), 
    c.push({
      src: l,
      col: a,
      dir: u[e][1],
      index: u[e]._idx,
      type: r,
      formatter: Vt.ext.type.order[r + "-pre"]
    });
    return c;
  }
  function wt(t) {
    var e, i, n, o, s = [], a = Vt.ext.type.order, r = t.aoData, l = 0, c = t.aiDisplayMaster;
    for (b(t), e = 0, i = (o = yt(t)).length; e < i; e++) (n = o[e]).formatter && l++, 
    At(t, n.col);
    if ("ssp" != qt(t) && 0 !== o.length) {
      for (e = 0, i = c.length; e < i; e++) s[c[e]] = e;
      l === o.length ? c.sort(function(t, e) {
        var i, n, a, l, c = o.length, d = r[t]._aSortData, u = r[e]._aSortData;
        for (a = 0; a < c; a++) if (l = o[a], i = d[l.col], n = u[l.col], 0 !== (i = i < n ? -1 : i > n ? 1 : 0)) return "asc" === l.dir ? i : -i;
        return i = s[t], n = s[e], i < n ? -1 : i > n ? 1 : 0;
      }) : c.sort(function(t, e) {
        var i, n, l, c, d = o.length, u = r[t]._aSortData, h = r[e]._aSortData;
        for (l = 0; l < d; l++) if (c = o[l], i = u[c.col], n = h[c.col], c = a[c.type + "-" + c.dir] || a["string-" + c.dir], 
        0 !== (i = c(i, n))) return i;
        return i = s[t], n = s[e], i < n ? -1 : i > n ? 1 : 0;
      });
    }
    t.bSorted = !0;
  }
  function xt(t) {
    for (var e, i, n = t.aoColumns, o = yt(t), t = t.oLanguage.oAria, s = 0, a = n.length; s < a; s++) {
      var r = (i = n[s]).asSorting;
      e = i.sTitle.replace(/<.*?>/g, "");
      var l = i.nTh;
      l.removeAttribute("aria-sort"), i.bSortable && (0 < o.length && o[0].col == s ? (l.setAttribute("aria-sort", "asc" == o[0].dir ? "ascending" : "descending"), 
      i = r[o[0].index + 1] || r[0]) : i = r[0], e += "asc" === i ? t.sSortAscending : t.sSortDescending), 
      l.setAttribute("aria-label", e);
    }
  }
  function kt(e, i, o, s) {
    var a = e.aaSorting, r = e.aoColumns[i].asSorting, l = function(e, i) {
      var o = e._idx;
      return o === n && (o = t.inArray(e[1], r)), o + 1 < r.length ? o + 1 : i ? null : 0;
    };
    "number" == typeof a[0] && (a = e.aaSorting = [ a ]), o && e.oFeatures.bSortMulti ? -1 !== (o = t.inArray(i, oe(a, "0"))) ? (null === (i = l(a[o], !0)) && 1 === a.length && (i = 0), 
    null === i ? a.splice(o, 1) : (a[o][1] = r[i], a[o]._idx = i)) : (a.push([ i, r[0], 0 ]), 
    a[a.length - 1]._idx = 0) : a.length && a[0][0] == i ? (i = l(a[0]), a.length = 1, 
    a[0][1] = r[i], a[0]._idx = i) : (a.length = 0, a.push([ i, r[0] ]), a[0]._idx = 0), 
    F(e), "function" == typeof s && s(e);
  }
  function _t(t, e, i, n) {
    var o = t.aoColumns[i];
    Et(e, {}, function(e) {
      !1 !== o.bSortable && (t.oFeatures.bProcessing ? (ut(t, !0), setTimeout(function() {
        kt(t, i, e.shiftKey, n), "ssp" !== qt(t) && ut(t, !1);
      }, 0)) : kt(t, i, e.shiftKey, n));
    });
  }
  function $t(e) {
    var i, n, o = e.aLastSort, s = e.oClasses.sSortColumn, a = yt(e), r = e.oFeatures;
    if (r.bSort && r.bSortClasses) {
      for (r = 0, i = o.length; r < i; r++) n = o[r].src, t(oe(e.aoData, "anCells", n)).removeClass(s + (2 > r ? r + 1 : 3));
      for (r = 0, i = a.length; r < i; r++) n = a[r].src, t(oe(e.aoData, "anCells", n)).addClass(s + (2 > r ? r + 1 : 3));
    }
    e.aLastSort = a;
  }
  function At(t, e) {
    var i, n = t.aoColumns[e], o = Vt.ext.order[n.sSortDataType];
    o && (i = o.call(t.oInstance, t, e, m(t, e)));
    for (var s, a = Vt.ext.type.order[n.sType + "-pre"], r = 0, l = t.aoData.length; r < l; r++) (n = t.aoData[r])._aSortData || (n._aSortData = []), 
    (!n._aSortData[e] || o) && (s = o ? i[r] : x(t, r, e, "sort"), n._aSortData[e] = a ? a(s) : s);
  }
  function St(e) {
    if (e.oFeatures.bStateSave && !e.bDestroying) {
      var i = {
        time: +new Date(),
        start: e._iDisplayStart,
        length: e._iDisplayLength,
        order: t.extend(!0, [], e.aaSorting),
        search: Z(e.oPreviousSearch),
        columns: t.map(e.aoColumns, function(t, i) {
          return {
            visible: t.bVisible,
            search: Z(e.aoPreSearchCols[i])
          };
        })
      };
      Ot(e, "aoStateSaveParams", "stateSaveParams", [ e, i ]), e.oSavedState = i, e.fnStateSaveCallback.call(e.oInstance, e, i);
    }
  }
  function Tt(e, i, o) {
    var s, a, r = e.aoColumns, i = function(i) {
      if (i && i.time) {
        var l = Ot(e, "aoStateLoadParams", "stateLoadParams", [ e, i ]);
        if (-1 === t.inArray(!1, l) && !(0 < (l = e.iStateDuration) && i.time < +new Date() - 1e3 * l || i.columns && r.length !== i.columns.length)) {
          if (e.oLoadedState = t.extend(!0, {}, i), i.start !== n && (e._iDisplayStart = i.start, 
          e.iInitDisplayStart = i.start), i.length !== n && (e._iDisplayLength = i.length), 
          i.order !== n && (e.aaSorting = [], t.each(i.order, function(t, i) {
            e.aaSorting.push(i[0] >= r.length ? [ 0, i[1] ] : i);
          })), i.search !== n && t.extend(e.oPreviousSearch, tt(i.search)), i.columns) for (s = 0, 
          a = i.columns.length; s < a; s++) (l = i.columns[s]).visible !== n && (r[s].bVisible = l.visible), 
          l.search !== n && t.extend(e.aoPreSearchCols[s], tt(l.search));
          Ot(e, "aoStateLoaded", "stateLoaded", [ e, i ]);
        }
      }
      o();
    };
    if (e.oFeatures.bStateSave) {
      var l = e.fnStateLoadCallback.call(e.oInstance, e, i);
      l !== n && i(l);
    } else o();
  }
  function Dt(e) {
    var i = Vt.settings;
    return -1 !== (e = t.inArray(e, oe(i, "nTable"))) ? i[e] : null;
  }
  function It(t, i, n, o) {
    if (n = "DataTables warning: " + (t ? "table id=" + t.sTableId + " - " : "") + n, 
    o && (n += ". For more information about this error, please see http://datatables.net/tn/" + o), 
    i) e.console && console.log && console.log(n); else if (i = Vt.ext, i = i.sErrMode || i.errMode, 
    t && Ot(t, null, "error", [ t, o, n ]), "alert" == i) alert(n); else {
      if ("throw" == i) throw Error(n);
      "function" == typeof i && i(t, o, n);
    }
  }
  function jt(e, i, o, s) {
    t.isArray(o) ? t.each(o, function(n, o) {
      t.isArray(o) ? jt(e, i, o[0], o[1]) : jt(e, i, o);
    }) : (s === n && (s = o), i[o] !== n && (e[s] = i[o]));
  }
  function Pt(e, i, n) {
    var o, s;
    for (s in i) i.hasOwnProperty(s) && (o = i[s], t.isPlainObject(o) ? (t.isPlainObject(e[s]) || (e[s] = {}), 
    t.extend(!0, e[s], o)) : e[s] = n && "data" !== s && "aaData" !== s && t.isArray(o) ? o.slice() : o);
    return e;
  }
  function Et(e, i, n) {
    t(e).on("click.DT", i, function(t) {
      e.blur(), n(t);
    }).on("keypress.DT", i, function(t) {
      13 === t.which && (t.preventDefault(), n(t));
    }).on("selectstart.DT", function() {
      return !1;
    });
  }
  function Nt(t, e, i, n) {
    i && t[e].push({
      fn: i,
      sName: n
    });
  }
  function Ot(e, i, n, o) {
    var s = [];
    return i && (s = t.map(e[i].slice().reverse(), function(t) {
      return t.fn.apply(e.oInstance, o);
    })), null !== n && (i = t.Event(n + ".dt"), t(e.nTable).trigger(i, o), s.push(i.result)), 
    s;
  }
  function Mt(t) {
    var e = t._iDisplayStart, i = t.fnDisplayEnd(), n = t._iDisplayLength;
    e >= i && (e = i - n), e -= e % n, (-1 === n || 0 > e) && (e = 0), t._iDisplayStart = e;
  }
  function Ft(e, i) {
    var n = e.renderer, o = Vt.ext.renderer[i];
    return t.isPlainObject(n) && n[i] ? o[n[i]] || o._ : "string" == typeof n ? o[n] || o._ : o._;
  }
  function qt(t) {
    return t.oFeatures.bServerSide ? "ssp" : t.ajax || t.sAjaxSource ? "ajax" : "dom";
  }
  function Ht(t, e) {
    var i = [], i = Pe.numbers_length, n = Math.floor(i / 2);
    return e <= i ? i = ae(0, e) : t <= n ? ((i = ae(0, i - 2)).push("ellipsis"), i.push(e - 1)) : (t >= e - 1 - n ? i = ae(e - (i - 2), e) : ((i = ae(t - n + 2, t + n - 1)).push("ellipsis"), 
    i.push(e - 1)), i.splice(0, 0, "ellipsis"), i.splice(0, 0, 0)), i.DT_el = "span", 
    i;
  }
  function Lt(e) {
    t.each({
      num: function(t) {
        return Ee(t, e);
      },
      "num-fmt": function(t) {
        return Ee(t, e, Jt);
      },
      "html-num": function(t) {
        return Ee(t, e, Yt);
      },
      "html-num-fmt": function(t) {
        return Ee(t, e, Yt, Jt);
      }
    }, function(t, i) {
      Rt.type.order[t + e + "-pre"] = i, t.match(/^html\-/) && (Rt.type.search[t + e] = Rt.type.search.html);
    });
  }
  function Wt(t) {
    return function() {
      var e = [ Dt(this[Vt.ext.iApiIndex]) ].concat(Array.prototype.slice.call(arguments));
      return Vt.ext.internal[t].apply(this, e);
    };
  }
  var Rt, zt, Bt, Ut, Vt = function(e) {
    this.$ = function(t, e) {
      return this.api(!0).$(t, e);
    }, this._ = function(t, e) {
      return this.api(!0).rows(t, e).data();
    }, this.api = function(t) {
      return new zt(t ? Dt(this[Rt.iApiIndex]) : this);
    }, this.fnAddData = function(e, i) {
      var o = this.api(!0), s = t.isArray(e) && (t.isArray(e[0]) || t.isPlainObject(e[0])) ? o.rows.add(e) : o.row.add(e);
      return (i === n || i) && o.draw(), s.flatten().toArray();
    }, this.fnAdjustColumnSizing = function(t) {
      var e = this.api(!0).columns.adjust(), i = e.settings()[0], o = i.oScroll;
      t === n || t ? e.draw(!1) : ("" !== o.sX || "" !== o.sY) && pt(i);
    }, this.fnClearTable = function(t) {
      var e = this.api(!0).clear();
      (t === n || t) && e.draw();
    }, this.fnClose = function(t) {
      this.api(!0).row(t).child.hide();
    }, this.fnDeleteRow = function(t, e, i) {
      var o = this.api(!0), s = (t = o.rows(t)).settings()[0], a = s.aoData[t[0][0]];
      return t.remove(), e && e.call(this, s, a), (i === n || i) && o.draw(), a;
    }, this.fnDestroy = function(t) {
      this.api(!0).destroy(t);
    }, this.fnDraw = function(t) {
      this.api(!0).draw(t);
    }, this.fnFilter = function(t, e, i, o, s, a) {
      s = this.api(!0), null === e || e === n ? s.search(t, i, o, a) : s.column(e).search(t, i, o, a), 
      s.draw();
    }, this.fnGetData = function(t, e) {
      var i = this.api(!0);
      if (t !== n) {
        var o = t.nodeName ? t.nodeName.toLowerCase() : "";
        return e !== n || "td" == o || "th" == o ? i.cell(t, e).data() : i.row(t).data() || null;
      }
      return i.data().toArray();
    }, this.fnGetNodes = function(t) {
      var e = this.api(!0);
      return t !== n ? e.row(t).node() : e.rows().nodes().flatten().toArray();
    }, this.fnGetPosition = function(t) {
      var e = this.api(!0), i = t.nodeName.toUpperCase();
      return "TR" == i ? e.row(t).index() : "TD" == i || "TH" == i ? (t = e.cell(t).index(), 
      [ t.row, t.columnVisible, t.column ]) : null;
    }, this.fnIsOpen = function(t) {
      return this.api(!0).row(t).child.isShown();
    }, this.fnOpen = function(t, e, i) {
      return this.api(!0).row(t).child(e, i).show().child()[0];
    }, this.fnPageChange = function(t, e) {
      var i = this.api(!0).page(t);
      (e === n || e) && i.draw(!1);
    }, this.fnSetColumnVis = function(t, e, i) {
      t = this.api(!0).column(t).visible(e), (i === n || i) && t.columns.adjust().draw();
    }, this.fnSettings = function() {
      return Dt(this[Rt.iApiIndex]);
    }, this.fnSort = function(t) {
      this.api(!0).order(t).draw();
    }, this.fnSortListener = function(t, e, i) {
      this.api(!0).order.listener(t, e, i);
    }, this.fnUpdate = function(t, e, i, o, s) {
      var a = this.api(!0);
      return i === n || null === i ? a.row(e).data(t) : a.cell(e, i).data(t), (s === n || s) && a.columns.adjust(), 
      (o === n || o) && a.draw(), 0;
    }, this.fnVersionCheck = Rt.fnVersionCheck;
    var i = this, o = e === n, d = this.length;
    o && (e = {}), this.oApi = this.internal = Rt.internal;
    for (var p in Vt.ext.internal) p && (this[p] = Wt(p));
    return this.each(function() {
      var p, f = {}, m = 1 < d ? Pt(f, e, !0) : e, g = 0, f = this.getAttribute("id"), v = !1, b = Vt.defaults, x = t(this);
      if ("table" != this.nodeName.toLowerCase()) It(null, 0, "Non-table node initialisation (" + this.nodeName + ")", 2); else {
        r(b), l(b.column), s(b, b, !0), s(b.column, b.column, !0), s(b, t.extend(m, x.data()));
        var k = Vt.settings, g = 0;
        for (p = k.length; g < p; g++) {
          var _ = k[g];
          if (_.nTable == this || _.nTHead.parentNode == this || _.nTFoot && _.nTFoot.parentNode == this) {
            var A = m.bRetrieve !== n ? m.bRetrieve : b.bRetrieve;
            if (o || A) return _.oInstance;
            if (m.bDestroy !== n ? m.bDestroy : b.bDestroy) {
              _.oInstance.fnDestroy();
              break;
            }
            return void It(_, 0, "Cannot reinitialise DataTable", 3);
          }
          if (_.sTableId == this.id) {
            k.splice(g, 1);
            break;
          }
        }
        null !== f && "" !== f || (this.id = f = "DataTables_Table_" + Vt.ext._unique++);
        var S = t.extend(!0, {}, Vt.models.oSettings, {
          sDestroyWidth: x[0].style.width,
          sInstance: f,
          sTableId: f
        });
        S.nTable = this, S.oApi = i.internal, S.oInit = m, k.push(S), S.oInstance = 1 === i.length ? i : x.dataTable(), 
        r(m), m.oLanguage && a(m.oLanguage), m.aLengthMenu && !m.iDisplayLength && (m.iDisplayLength = t.isArray(m.aLengthMenu[0]) ? m.aLengthMenu[0][0] : m.aLengthMenu[0]), 
        m = Pt(t.extend(!0, {}, b), m), jt(S.oFeatures, m, "bPaginate bLengthChange bFilter bSort bSortMulti bInfo bProcessing bAutoWidth bSortClasses bServerSide bDeferRender".split(" ")), 
        jt(S, m, [ "asStripeClasses", "ajax", "fnServerData", "fnFormatNumber", "sServerMethod", "aaSorting", "aaSortingFixed", "aLengthMenu", "sPaginationType", "sAjaxSource", "sAjaxDataProp", "iStateDuration", "sDom", "bSortCellsTop", "iTabIndex", "fnStateLoadCallback", "fnStateSaveCallback", "renderer", "searchDelay", "rowId", [ "iCookieDuration", "iStateDuration" ], [ "oSearch", "oPreviousSearch" ], [ "aoSearchCols", "aoPreSearchCols" ], [ "iDisplayLength", "_iDisplayLength" ], [ "bJQueryUI", "bJUI" ] ]), 
        jt(S.oScroll, m, [ [ "sScrollX", "sX" ], [ "sScrollXInner", "sXInner" ], [ "sScrollY", "sY" ], [ "bScrollCollapse", "bCollapse" ] ]), 
        jt(S.oLanguage, m, "fnInfoCallback"), Nt(S, "aoDrawCallback", m.fnDrawCallback, "user"), 
        Nt(S, "aoServerParams", m.fnServerParams, "user"), Nt(S, "aoStateSaveParams", m.fnStateSaveParams, "user"), 
        Nt(S, "aoStateLoadParams", m.fnStateLoadParams, "user"), Nt(S, "aoStateLoaded", m.fnStateLoaded, "user"), 
        Nt(S, "aoRowCallback", m.fnRowCallback, "user"), Nt(S, "aoRowCreatedCallback", m.fnCreatedRow, "user"), 
        Nt(S, "aoHeaderCallback", m.fnHeaderCallback, "user"), Nt(S, "aoFooterCallback", m.fnFooterCallback, "user"), 
        Nt(S, "aoInitComplete", m.fnInitComplete, "user"), Nt(S, "aoPreDrawCallback", m.fnPreDrawCallback, "user"), 
        S.rowIdFn = $(m.rowId), c(S);
        var T = S.oClasses;
        m.bJQueryUI ? (t.extend(T, Vt.ext.oJUIClasses, m.oClasses), m.sDom === b.sDom && "lfrtip" === b.sDom && (S.sDom = '<"H"lfr>t<"F"ip>'), 
        S.renderer ? t.isPlainObject(S.renderer) && !S.renderer.header && (S.renderer.header = "jqueryui") : S.renderer = "jqueryui") : t.extend(T, Vt.ext.classes, m.oClasses), 
        x.addClass(T.sTable), S.iInitDisplayStart === n && (S.iInitDisplayStart = m.iDisplayStart, 
        S._iDisplayStart = m.iDisplayStart), null !== m.iDeferLoading && (S.bDeferLoading = !0, 
        f = t.isArray(m.iDeferLoading), S._iRecordsDisplay = f ? m.iDeferLoading[0] : m.iDeferLoading, 
        S._iRecordsTotal = f ? m.iDeferLoading[1] : m.iDeferLoading);
        var D = S.oLanguage;
        t.extend(!0, D, m.oLanguage), D.sUrl && (t.ajax({
          dataType: "json",
          url: D.sUrl,
          success: function(e) {
            a(e), s(b.oLanguage, e), t.extend(!0, D, e), ot(S);
          },
          error: function() {
            ot(S);
          }
        }), v = !0), null === m.asStripeClasses && (S.asStripeClasses = [ T.sStripeOdd, T.sStripeEven ]);
        var f = S.asStripeClasses, I = x.children("tbody").find("tr").eq(0);
        if (-1 !== t.inArray(!0, t.map(f, function(t) {
          return I.hasClass(t);
        })) && (t("tbody tr", this).removeClass(f.join(" ")), S.asDestroyStripes = f.slice()), 
        f = [], 0 !== (k = this.getElementsByTagName("thead")).length && (H(S.aoHeader, k[0]), 
        f = L(S)), null === m.aoColumns) for (k = [], g = 0, p = f.length; g < p; g++) k.push(null); else k = m.aoColumns;
        for (g = 0, p = k.length; g < p; g++) u(S, f ? f[g] : null);
        if (C(S, m.aoColumnDefs, k, function(t, e) {
          h(S, t, e);
        }), I.length) {
          var j = function(t, e) {
            return null !== t.getAttribute("data-" + e) ? e : null;
          };
          t(I[0]).children("th, td").each(function(t, e) {
            var i = S.aoColumns[t];
            if (i.mData === t) {
              var o = j(e, "sort") || j(e, "order"), s = j(e, "filter") || j(e, "search");
              null === o && null === s || (i.mData = {
                _: t + ".display",
                sort: null !== o ? t + ".@data-" + o : n,
                type: null !== o ? t + ".@data-" + o : n,
                filter: null !== s ? t + ".@data-" + s : n
              }, h(S, t));
            }
          });
        }
        var P = S.oFeatures, f = function() {
          if (m.aaSorting === n) {
            e = S.aaSorting;
            for (g = 0, p = e.length; g < p; g++) e[g][1] = S.aoColumns[g].asSorting[0];
          }
          $t(S), P.bSort && Nt(S, "aoDrawCallback", function() {
            if (S.bSorted) {
              var e = yt(S), i = {};
              t.each(e, function(t, e) {
                i[e.src] = e.dir;
              }), Ot(S, null, "order", [ S, e, i ]), xt(S);
            }
          }), Nt(S, "aoDrawCallback", function() {
            (S.bSorted || "ssp" === qt(S) || P.bDeferRender) && $t(S);
          }, "sc");
          var e = x.children("caption").each(function() {
            this._captionSide = t(this).css("caption-side");
          }), i = x.children("thead");
          if (0 === i.length && (i = t("<thead/>").appendTo(x)), S.nTHead = i[0], 0 === (i = x.children("tbody")).length && (i = t("<tbody/>").appendTo(x)), 
          S.nTBody = i[0], 0 === (i = x.children("tfoot")).length && e.length > 0 && ("" !== S.oScroll.sX || "" !== S.oScroll.sY) && (i = t("<tfoot/>").appendTo(x)), 
          0 === i.length || 0 === i.children().length ? x.addClass(T.sNoFooter) : i.length > 0 && (S.nTFoot = i[0], 
          H(S.aoFooter, S.nTFoot)), m.aaData) for (g = 0; g < m.aaData.length; g++) y(S, m.aaData[g]); else (S.bDeferLoading || "dom" == qt(S)) && w(S, t(S.nTBody).children("tr"));
          S.aiDisplay = S.aiDisplayMaster.slice(), S.bInitialised = !0, !1 === v && ot(S);
        };
        m.bStateSave ? (P.bStateSave = !0, Nt(S, "aoDrawCallback", St, "state_save"), Tt(S, m, f)) : f();
      }
    }), i = null, this;
  }, Qt = {}, Xt = /[\r\n]/g, Yt = /<.*?>/g, Gt = /^\d{2,4}[\.\/\-]\d{1,2}[\.\/\-]\d{1,2}([T ]{1}\d{1,2}[:\.]\d{2}([\.:]\d{2})?)?$/, Kt = RegExp("(\\/|\\.|\\*|\\+|\\?|\\||\\(|\\)|\\[|\\]|\\{|\\}|\\\\|\\$|\\^|\\-)", "g"), Jt = /[',$£€¥%\u2009\u202F\u20BD\u20a9\u20BArfk]/gi, Zt = function(t) {
    return !t || !0 === t || "-" === t;
  }, te = function(t) {
    var e = parseInt(t, 10);
    return !isNaN(e) && isFinite(t) ? e : null;
  }, ee = function(t, e) {
    return Qt[e] || (Qt[e] = RegExp(he(e), "g")), "string" == typeof t && "." !== e ? t.replace(/\./g, "").replace(Qt[e], ".") : t;
  }, ie = function(t, e, i) {
    var n = "string" == typeof t;
    return !!Zt(t) || (e && n && (t = ee(t, e)), i && n && (t = t.replace(Jt, "")), 
    !isNaN(parseFloat(t)) && isFinite(t));
  }, ne = function(t, e, i) {
    return !!Zt(t) || (Zt(t) || "string" == typeof t ? !!ie(t.replace(Yt, ""), e, i) || null : null);
  }, oe = function(t, e, i) {
    var o = [], s = 0, a = t.length;
    if (i !== n) for (;s < a; s++) t[s] && t[s][e] && o.push(t[s][e][i]); else for (;s < a; s++) t[s] && o.push(t[s][e]);
    return o;
  }, se = function(t, e, i, o) {
    var s = [], a = 0, r = e.length;
    if (o !== n) for (;a < r; a++) t[e[a]][i] && s.push(t[e[a]][i][o]); else for (;a < r; a++) s.push(t[e[a]][i]);
    return s;
  }, ae = function(t, e) {
    var i, o = [];
    e === n ? (e = 0, i = t) : (i = e, e = t);
    for (var s = e; s < i; s++) o.push(s);
    return o;
  }, re = function(t) {
    for (var e = [], i = 0, n = t.length; i < n; i++) t[i] && e.push(t[i]);
    return e;
  }, le = function(t) {
    var e;
    t: {
      if (!(2 > t.length)) for (var i = (e = t.slice().sort())[0], n = 1, o = e.length; n < o; n++) {
        if (e[n] === i) {
          e = !1;
          break t;
        }
        i = e[n];
      }
      e = !0;
    }
    if (e) return t.slice();
    e = [];
    var s, o = t.length, a = 0, n = 0;
    t: for (;n < o; n++) {
      for (i = t[n], s = 0; s < a; s++) if (e[s] === i) continue t;
      e.push(i), a++;
    }
    return e;
  };
  Vt.util = {
    throttle: function(t, e) {
      var i, o, s = e !== n ? e : 200;
      return function() {
        var e = this, a = +new Date(), r = arguments;
        i && a < i + s ? (clearTimeout(o), o = setTimeout(function() {
          i = n, t.apply(e, r);
        }, s)) : (i = a, t.apply(e, r));
      };
    },
    escapeRegex: function(t) {
      return t.replace(Kt, "\\$1");
    }
  };
  var ce = function(t, e, i) {
    t[e] !== n && (t[i] = t[e]);
  }, de = /\[.*?\]$/, ue = /\(\)$/, he = Vt.util.escapeRegex, pe = t("<div>")[0], fe = pe.textContent !== n, me = /<.*?>/g, ge = Vt.util.throttle, ve = [], be = Array.prototype, Ce = function(e) {
    var i, n, o = Vt.settings, s = t.map(o, function(t) {
      return t.nTable;
    });
    return e ? e.nTable && e.oApi ? [ e ] : e.nodeName && "table" === e.nodeName.toLowerCase() ? -1 !== (i = t.inArray(e, s)) ? [ o[i] ] : null : e && "function" == typeof e.settings ? e.settings().toArray() : ("string" == typeof e ? n = t(e) : e instanceof t && (n = e), 
    n ? n.map(function() {
      return -1 !== (i = t.inArray(this, s)) ? o[i] : null;
    }).toArray() : void 0) : [];
  };
  zt = function(e, i) {
    if (!(this instanceof zt)) return new zt(e, i);
    var n = [], o = function(t) {
      (t = Ce(t)) && (n = n.concat(t));
    };
    if (t.isArray(e)) for (var s = 0, a = e.length; s < a; s++) o(e[s]); else o(e);
    this.context = le(n), i && t.merge(this, i), this.selector = {
      rows: null,
      cols: null,
      opts: null
    }, zt.extend(this, this, ve);
  }, Vt.Api = zt, t.extend(zt.prototype, {
    any: function() {
      return 0 !== this.count();
    },
    concat: be.concat,
    context: [],
    count: function() {
      return this.flatten().length;
    },
    each: function(t) {
      for (var e = 0, i = this.length; e < i; e++) t.call(this, this[e], e, this);
      return this;
    },
    eq: function(t) {
      var e = this.context;
      return e.length > t ? new zt(e[t], this[t]) : null;
    },
    filter: function(t) {
      var e = [];
      if (be.filter) e = be.filter.call(this, t, this); else for (var i = 0, n = this.length; i < n; i++) t.call(this, this[i], i, this) && e.push(this[i]);
      return new zt(this.context, e);
    },
    flatten: function() {
      var t = [];
      return new zt(this.context, t.concat.apply(t, this.toArray()));
    },
    join: be.join,
    indexOf: be.indexOf || function(t, e) {
      for (var i = e || 0, n = this.length; i < n; i++) if (this[i] === t) return i;
      return -1;
    },
    iterator: function(t, e, i, o) {
      var s, a, r, l, c, d, u, h = [], p = this.context, f = this.selector;
      for ("string" == typeof t && (o = i, i = e, e = t, t = !1), a = 0, r = p.length; a < r; a++) {
        var m = new zt(p[a]);
        if ("table" === e) (s = i.call(m, p[a], a)) !== n && h.push(s); else if ("columns" === e || "rows" === e) (s = i.call(m, p[a], this[a], a)) !== n && h.push(s); else if ("column" === e || "column-rows" === e || "row" === e || "cell" === e) for (u = this[a], 
        "column-rows" === e && (d = _e(p[a], f.opts)), l = 0, c = u.length; l < c; l++) s = u[l], 
        (s = "cell" === e ? i.call(m, p[a], s.row, s.column, a, l) : i.call(m, p[a], s, a, l, d)) !== n && h.push(s);
      }
      return h.length || o ? (t = new zt(p, t ? h.concat.apply([], h) : h), e = t.selector, 
      e.rows = f.rows, e.cols = f.cols, e.opts = f.opts, t) : this;
    },
    lastIndexOf: be.lastIndexOf || function(t, e) {
      return this.indexOf.apply(this.toArray.reverse(), arguments);
    },
    length: 0,
    map: function(t) {
      var e = [];
      if (be.map) e = be.map.call(this, t, this); else for (var i = 0, n = this.length; i < n; i++) e.push(t.call(this, this[i], i));
      return new zt(this.context, e);
    },
    pluck: function(t) {
      return this.map(function(e) {
        return e[t];
      });
    },
    pop: be.pop,
    push: be.push,
    reduce: be.reduce || function(t, e) {
      return d(this, t, e, 0, this.length, 1);
    },
    reduceRight: be.reduceRight || function(t, e) {
      return d(this, t, e, this.length - 1, -1, -1);
    },
    reverse: be.reverse,
    selector: null,
    shift: be.shift,
    slice: function() {
      return new zt(this.context, this);
    },
    sort: be.sort,
    splice: be.splice,
    toArray: function() {
      return be.slice.call(this);
    },
    to$: function() {
      return t(this);
    },
    toJQuery: function() {
      return t(this);
    },
    unique: function() {
      return new zt(this.context, le(this));
    },
    unshift: be.unshift
  }), zt.extend = function(e, i, n) {
    if (n.length && i && (i instanceof zt || i.__dt_wrapper)) {
      var o, s, a;
      for (o = 0, s = n.length; o < s; o++) a = n[o], i[a.name] = "function" == typeof a.val ? function(t, e, i) {
        return function() {
          var n = e.apply(t, arguments);
          return zt.extend(n, n, i.methodExt), n;
        };
      }(e, a.val, a) : t.isPlainObject(a.val) ? {} : a.val, i[a.name].__dt_wrapper = !0, 
      zt.extend(e, i[a.name], a.propExt);
    }
  }, zt.register = Bt = function(e, i) {
    if (t.isArray(e)) for (var n = 0, o = e.length; n < o; n++) zt.register(e[n], i); else for (var s, a, r = e.split("."), l = ve, n = 0, o = r.length; n < o; n++) {
      s = (a = -1 !== r[n].indexOf("()")) ? r[n].replace("()", "") : r[n];
      var c;
      t: {
        c = 0;
        for (var d = l.length; c < d; c++) if (l[c].name === s) {
          c = l[c];
          break t;
        }
        c = null;
      }
      c || (c = {
        name: s,
        val: {},
        methodExt: [],
        propExt: []
      }, l.push(c)), n === o - 1 ? c.val = i : l = a ? c.methodExt : c.propExt;
    }
  }, zt.registerPlural = Ut = function(e, i, o) {
    zt.register(e, o), zt.register(i, function() {
      var e = o.apply(this, arguments);
      return e === this ? this : e instanceof zt ? e.length ? t.isArray(e[0]) ? new zt(e.context, e[0]) : e[0] : n : e;
    });
  }, Bt("tables()", function(e) {
    var i;
    if (e) {
      i = zt;
      var n = this.context;
      if ("number" == typeof e) e = [ n[e] ]; else var o = t.map(n, function(t) {
        return t.nTable;
      }), e = t(o).filter(e).map(function() {
        var e = t.inArray(this, o);
        return n[e];
      }).toArray();
      i = new i(e);
    } else i = this;
    return i;
  }), Bt("table()", function(t) {
    var e = (t = this.tables(t)).context;
    return e.length ? new zt(e[0]) : t;
  }), Ut("tables().nodes()", "table().node()", function() {
    return this.iterator("table", function(t) {
      return t.nTable;
    }, 1);
  }), Ut("tables().body()", "table().body()", function() {
    return this.iterator("table", function(t) {
      return t.nTBody;
    }, 1);
  }), Ut("tables().header()", "table().header()", function() {
    return this.iterator("table", function(t) {
      return t.nTHead;
    }, 1);
  }), Ut("tables().footer()", "table().footer()", function() {
    return this.iterator("table", function(t) {
      return t.nTFoot;
    }, 1);
  }), Ut("tables().containers()", "table().container()", function() {
    return this.iterator("table", function(t) {
      return t.nTableWrapper;
    }, 1);
  }), Bt("draw()", function(t) {
    return this.iterator("table", function(e) {
      "page" === t ? M(e) : ("string" == typeof t && (t = "full-hold" !== t), F(e, !1 === t));
    });
  }), Bt("page()", function(t) {
    return t === n ? this.page.info().page : this.iterator("table", function(e) {
      ct(e, t);
    });
  }), Bt("page.info()", function() {
    if (0 === this.context.length) return n;
    var t = this.context[0], e = t._iDisplayStart, i = t.oFeatures.bPaginate ? t._iDisplayLength : -1, o = t.fnRecordsDisplay(), s = -1 === i;
    return {
      page: s ? 0 : Math.floor(e / i),
      pages: s ? 1 : Math.ceil(o / i),
      start: e,
      end: t.fnDisplayEnd(),
      length: i,
      recordsTotal: t.fnRecordsTotal(),
      recordsDisplay: o,
      serverSide: "ssp" === qt(t)
    };
  }), Bt("page.len()", function(t) {
    return t === n ? 0 !== this.context.length ? this.context[0]._iDisplayLength : n : this.iterator("table", function(e) {
      at(e, t);
    });
  });
  var ye = function(t, e, i) {
    if (i) {
      var n = new zt(t);
      n.one("draw", function() {
        i(n.ajax.json());
      });
    }
    if ("ssp" == qt(t)) F(t, e); else {
      ut(t, !0);
      var o = t.jqXHR;
      o && 4 !== o.readyState && o.abort(), W(t, [], function(i) {
        T(t);
        for (var n = 0, o = (i = U(t, i)).length; n < o; n++) y(t, i[n]);
        F(t, e), ut(t, !1);
      });
    }
  };
  Bt("ajax.json()", function() {
    var t = this.context;
    if (0 < t.length) return t[0].json;
  }), Bt("ajax.params()", function() {
    var t = this.context;
    if (0 < t.length) return t[0].oAjaxData;
  }), Bt("ajax.reload()", function(t, e) {
    return this.iterator("table", function(i) {
      ye(i, !1 === e, t);
    });
  }), Bt("ajax.url()", function(e) {
    var i = this.context;
    return e === n ? 0 === i.length ? n : (i = i[0]).ajax ? t.isPlainObject(i.ajax) ? i.ajax.url : i.ajax : i.sAjaxSource : this.iterator("table", function(i) {
      t.isPlainObject(i.ajax) ? i.ajax.url = e : i.ajax = e;
    });
  }), Bt("ajax.url().load()", function(t, e) {
    return this.iterator("table", function(i) {
      ye(i, !1 === e, t);
    });
  });
  var we = function(e, i, o, s, a) {
    var r, l, c, d, u, h, p = [];
    for (c = typeof i, i && "string" !== c && "function" !== c && i.length !== n || (i = [ i ]), 
    c = 0, d = i.length; c < d; c++) for (u = 0, h = (l = i[c] && i[c].split && !i[c].match(/[\[\(:]/) ? i[c].split(",") : [ i[c] ]).length; u < h; u++) (r = o("string" == typeof l[u] ? t.trim(l[u]) : l[u])) && r.length && (p = p.concat(r));
    if ((e = Rt.selector[e]).length) for (c = 0, d = e.length; c < d; c++) p = e[c](s, a, p);
    return le(p);
  }, xe = function(e) {
    return e || (e = {}), e.filter && e.search === n && (e.search = e.filter), t.extend({
      search: "none",
      order: "current",
      page: "all"
    }, e);
  }, ke = function(t) {
    for (var e = 0, i = t.length; e < i; e++) if (0 < t[e].length) return t[0] = t[e], 
    t[0].length = 1, t.length = 1, t.context = [ t.context[e] ], t;
    return t.length = 0, t;
  }, _e = function(e, i) {
    var n, o, s, a = [], r = e.aiDisplay;
    n = e.aiDisplayMaster;
    var l = i.search;
    if (o = i.order, s = i.page, "ssp" == qt(e)) return "removed" === l ? [] : ae(0, n.length);
    if ("current" == s) for (n = e._iDisplayStart, o = e.fnDisplayEnd(); n < o; n++) a.push(r[n]); else if ("current" == o || "applied" == o) a = "none" == l ? n.slice() : "applied" == l ? r.slice() : t.map(n, function(e) {
      return -1 === t.inArray(e, r) ? e : null;
    }); else if ("index" == o || "original" == o) for (n = 0, o = e.aoData.length; n < o; n++) "none" == l ? a.push(n) : (-1 === (s = t.inArray(n, r)) && "removed" == l || 0 <= s && "applied" == l) && a.push(n);
    return a;
  };
  Bt("rows()", function(e, i) {
    e === n ? e = "" : t.isPlainObject(e) && (i = e, e = "");
    var i = xe(i), o = this.iterator("table", function(o) {
      var s, a = i;
      return we("row", e, function(e) {
        var i = te(e);
        if (null !== i && !a) return [ i ];
        if (s || (s = _e(o, a)), null !== i && -1 !== t.inArray(i, s)) return [ i ];
        if (null === e || e === n || "" === e) return s;
        if ("function" == typeof e) return t.map(s, function(t) {
          var i = o.aoData[t];
          return e(t, i._aData, i.nTr) ? t : null;
        });
        if (i = re(se(o.aoData, s, "nTr")), e.nodeName) return e._DT_RowIndex !== n ? [ e._DT_RowIndex ] : e._DT_CellIndex ? [ e._DT_CellIndex.row ] : (i = t(e).closest("*[data-dt-row]")).length ? [ i.data("dt-row") ] : [];
        if ("string" == typeof e && "#" === e.charAt(0)) {
          var r = o.aIds[e.replace(/^#/, "")];
          if (r !== n) return [ r.idx ];
        }
        return t(i).filter(e).map(function() {
          return this._DT_RowIndex;
        }).toArray();
      }, o, a);
    }, 1);
    return o.selector.rows = e, o.selector.opts = i, o;
  }), Bt("rows().nodes()", function() {
    return this.iterator("row", function(t, e) {
      return t.aoData[e].nTr || n;
    }, 1);
  }), Bt("rows().data()", function() {
    return this.iterator(!0, "rows", function(t, e) {
      return se(t.aoData, e, "_aData");
    }, 1);
  }), Ut("rows().cache()", "row().cache()", function(t) {
    return this.iterator("row", function(e, i) {
      var n = e.aoData[i];
      return "search" === t ? n._aFilterData : n._aSortData;
    }, 1);
  }), Ut("rows().invalidate()", "row().invalidate()", function(t) {
    return this.iterator("row", function(e, i) {
      I(e, i, t);
    });
  }), Ut("rows().indexes()", "row().index()", function() {
    return this.iterator("row", function(t, e) {
      return e;
    }, 1);
  }), Ut("rows().ids()", "row().id()", function(t) {
    for (var e = [], i = this.context, n = 0, o = i.length; n < o; n++) for (var s = 0, a = this[n].length; s < a; s++) {
      var r = i[n].rowIdFn(i[n].aoData[this[n][s]]._aData);
      e.push((!0 === t ? "#" : "") + r);
    }
    return new zt(i, e);
  }), Ut("rows().remove()", "row().remove()", function() {
    var t = this;
    return this.iterator("row", function(e, i, o) {
      var s, a, r, l, c, d = e.aoData, u = d[i];
      for (d.splice(i, 1), s = 0, a = d.length; s < a; s++) if (r = d[s], c = r.anCells, 
      null !== r.nTr && (r.nTr._DT_RowIndex = s), null !== c) for (r = 0, l = c.length; r < l; r++) c[r]._DT_CellIndex.row = s;
      D(e.aiDisplayMaster, i), D(e.aiDisplay, i), D(t[o], i, !1), Mt(e), (i = e.rowIdFn(u._aData)) !== n && delete e.aIds[i];
    }), this.iterator("table", function(t) {
      for (var e = 0, i = t.aoData.length; e < i; e++) t.aoData[e].idx = e;
    }), this;
  }), Bt("rows.add()", function(e) {
    var i = this.iterator("table", function(t) {
      var i, n, o, s = [];
      for (n = 0, o = e.length; n < o; n++) (i = e[n]).nodeName && "TR" === i.nodeName.toUpperCase() ? s.push(w(t, i)[0]) : s.push(y(t, i));
      return s;
    }, 1), n = this.rows(-1);
    return n.pop(), t.merge(n, i), n;
  }), Bt("row()", function(t, e) {
    return ke(this.rows(t, e));
  }), Bt("row().data()", function(t) {
    var e = this.context;
    return t === n ? e.length && this.length ? e[0].aoData[this[0]]._aData : n : (e[0].aoData[this[0]]._aData = t, 
    I(e[0], this[0], "data"), this);
  }), Bt("row().node()", function() {
    var t = this.context;
    return t.length && this.length ? t[0].aoData[this[0]].nTr || null : null;
  }), Bt("row.add()", function(e) {
    e instanceof t && e.length && (e = e[0]);
    var i = this.iterator("table", function(t) {
      return e.nodeName && "TR" === e.nodeName.toUpperCase() ? w(t, e)[0] : y(t, e);
    });
    return this.row(i[0]);
  });
  var $e = function(t, e) {
    var i = t.context;
    i.length && (i = i[0].aoData[e !== n ? e : t[0]]) && i._details && (i._details.remove(), 
    i._detailsShow = n, i._details = n);
  }, Ae = function(t, e) {
    var i = t.context;
    if (i.length && t.length) {
      var n = i[0].aoData[t[0]];
      if (n._details) {
        (n._detailsShow = e) ? n._details.insertAfter(n.nTr) : n._details.detach();
        var o = i[0], s = new zt(o), a = o.aoData;
        s.off("draw.dt.DT_details column-visibility.dt.DT_details destroy.dt.DT_details"), 
        0 < oe(a, "_details").length && (s.on("draw.dt.DT_details", function(t, e) {
          o === e && s.rows({
            page: "current"
          }).eq(0).each(function(t) {
            (t = a[t])._detailsShow && t._details.insertAfter(t.nTr);
          });
        }), s.on("column-visibility.dt.DT_details", function(t, e) {
          if (o === e) for (var i, n = g(e), s = 0, r = a.length; s < r; s++) (i = a[s])._details && i._details.children("td[colspan]").attr("colspan", n);
        }), s.on("destroy.dt.DT_details", function(t, e) {
          if (o === e) for (var i = 0, n = a.length; i < n; i++) a[i]._details && $e(s, i);
        }));
      }
    }
  };
  Bt("row().child()", function(e, i) {
    s = this.context;
    if (e === n) return s.length && this.length ? s[0].aoData[this[0]]._details : n;
    if (!0 === e) this.child.show(); else if (!1 === e) $e(this); else if (s.length && this.length) {
      var o = s[0], s = s[0].aoData[this[0]], a = [], r = function(e, i) {
        if (t.isArray(e) || e instanceof t) for (var n = 0, s = e.length; n < s; n++) r(e[n], i); else e.nodeName && "tr" === e.nodeName.toLowerCase() ? a.push(e) : (n = t("<tr><td/></tr>").addClass(i), 
        t("td", n).addClass(i).html(e)[0].colSpan = g(o), a.push(n[0]));
      };
      r(e, i), s._details && s._details.detach(), s._details = t(a), s._detailsShow && s._details.insertAfter(s.nTr);
    }
    return this;
  }), Bt([ "row().child.show()", "row().child().show()" ], function() {
    return Ae(this, !0), this;
  }), Bt([ "row().child.hide()", "row().child().hide()" ], function() {
    return Ae(this, !1), this;
  }), Bt([ "row().child.remove()", "row().child().remove()" ], function() {
    return $e(this), this;
  }), Bt("row().child.isShown()", function() {
    var t = this.context;
    return !(!t.length || !this.length) && (t[0].aoData[this[0]]._detailsShow || !1);
  });
  var Se = /^([^:]+):(name|visIdx|visible)$/, Te = function(t, e, i, n, o) {
    for (var i = [], n = 0, s = o.length; n < s; n++) i.push(x(t, o[n], e));
    return i;
  };
  Bt("columns()", function(e, i) {
    e === n ? e = "" : t.isPlainObject(e) && (i = e, e = "");
    var i = xe(i), o = this.iterator("table", function(n) {
      var o = e, s = i, a = n.aoColumns, r = oe(a, "sName"), l = oe(a, "nTh");
      return we("column", o, function(e) {
        var i = te(e);
        if ("" === e) return ae(a.length);
        if (null !== i) return [ i >= 0 ? i : a.length + i ];
        if ("function" == typeof e) {
          var o = _e(n, s);
          return t.map(a, function(t, i) {
            return e(i, Te(n, i, 0, 0, o), l[i]) ? i : null;
          });
        }
        var c = "string" == typeof e ? e.match(Se) : "";
        if (c) switch (c[2]) {
         case "visIdx":
         case "visible":
          if ((i = parseInt(c[1], 10)) < 0) {
            var d = t.map(a, function(t, e) {
              return t.bVisible ? e : null;
            });
            return [ d[d.length + i] ];
          }
          return [ f(n, i) ];

         case "name":
          return t.map(r, function(t, e) {
            return t === c[1] ? e : null;
          });

         default:
          return [];
        }
        return e.nodeName && e._DT_CellIndex ? [ e._DT_CellIndex.column ] : (i = t(l).filter(e).map(function() {
          return t.inArray(this, l);
        }).toArray()).length || !e.nodeName ? i : (i = t(e).closest("*[data-dt-column]")).length ? [ i.data("dt-column") ] : [];
      }, n, s);
    }, 1);
    return o.selector.cols = e, o.selector.opts = i, o;
  }), Ut("columns().header()", "column().header()", function() {
    return this.iterator("column", function(t, e) {
      return t.aoColumns[e].nTh;
    }, 1);
  }), Ut("columns().footer()", "column().footer()", function() {
    return this.iterator("column", function(t, e) {
      return t.aoColumns[e].nTf;
    }, 1);
  }), Ut("columns().data()", "column().data()", function() {
    return this.iterator("column-rows", Te, 1);
  }), Ut("columns().dataSrc()", "column().dataSrc()", function() {
    return this.iterator("column", function(t, e) {
      return t.aoColumns[e].mData;
    }, 1);
  }), Ut("columns().cache()", "column().cache()", function(t) {
    return this.iterator("column-rows", function(e, i, n, o, s) {
      return se(e.aoData, s, "search" === t ? "_aFilterData" : "_aSortData", i);
    }, 1);
  }), Ut("columns().nodes()", "column().nodes()", function() {
    return this.iterator("column-rows", function(t, e, i, n, o) {
      return se(t.aoData, o, "anCells", e);
    }, 1);
  }), Ut("columns().visible()", "column().visible()", function(e, i) {
    var o = this.iterator("column", function(i, o) {
      if (e === n) return i.aoColumns[o].bVisible;
      var s, a, r, l = i.aoColumns, c = l[o], d = i.aoData;
      if (e !== n && c.bVisible !== e) {
        if (e) {
          var u = t.inArray(!0, oe(l, "bVisible"), o + 1);
          for (s = 0, a = d.length; s < a; s++) r = d[s].nTr, l = d[s].anCells, r && r.insertBefore(l[o], l[u] || null);
        } else t(oe(i.aoData, "anCells", o)).detach();
        c.bVisible = e, O(i, i.aoHeader), O(i, i.aoFooter), St(i);
      }
    });
    return e !== n && (this.iterator("column", function(t, n) {
      Ot(t, null, "column-visibility", [ t, n, e, i ]);
    }), (i === n || i) && this.columns.adjust()), o;
  }), Ut("columns().indexes()", "column().index()", function(t) {
    return this.iterator("column", function(e, i) {
      return "visible" === t ? m(e, i) : i;
    }, 1);
  }), Bt("columns.adjust()", function() {
    return this.iterator("table", function(t) {
      p(t);
    }, 1);
  }), Bt("column.index()", function(t, e) {
    if (0 !== this.context.length) {
      var i = this.context[0];
      if ("fromVisible" === t || "toData" === t) return f(i, e);
      if ("fromData" === t || "toVisible" === t) return m(i, e);
    }
  }), Bt("column()", function(t, e) {
    return ke(this.columns(t, e));
  }), Bt("cells()", function(e, i, o) {
    if (t.isPlainObject(e) && (e.row === n ? (o = e, e = null) : (o = i, i = null)), 
    t.isPlainObject(i) && (o = i, i = null), null === i || i === n) return this.iterator("table", function(i) {
      var s, a, r, l, c, d, u, h = e, p = xe(o), f = i.aoData, m = _e(i, p), g = re(se(f, m, "anCells")), v = t([].concat.apply([], g)), b = i.aoColumns.length;
      return we("cell", h, function(e) {
        var o = "function" == typeof e;
        if (null === e || e === n || o) {
          for (a = [], r = 0, l = m.length; r < l; r++) for (s = m[r], c = 0; c < b; c++) d = {
            row: s,
            column: c
          }, o ? (u = f[s], e(d, x(i, s, c), u.anCells ? u.anCells[c] : null) && a.push(d)) : a.push(d);
          return a;
        }
        return t.isPlainObject(e) ? [ e ] : (o = v.filter(e).map(function(t, e) {
          return {
            row: e._DT_CellIndex.row,
            column: e._DT_CellIndex.column
          };
        }).toArray()).length || !e.nodeName ? o : (u = t(e).closest("*[data-dt-row]")).length ? [ {
          row: u.data("dt-row"),
          column: u.data("dt-column")
        } ] : [];
      }, i, p);
    });
    var s, a, r, l, c, d = this.columns(i, o), u = this.rows(e, o), h = this.iterator("table", function(t, e) {
      for (s = [], a = 0, r = u[e].length; a < r; a++) for (l = 0, c = d[e].length; l < c; l++) s.push({
        row: u[e][a],
        column: d[e][l]
      });
      return s;
    }, 1);
    return t.extend(h.selector, {
      cols: i,
      rows: e,
      opts: o
    }), h;
  }), Ut("cells().nodes()", "cell().node()", function() {
    return this.iterator("cell", function(t, e, i) {
      return (t = t.aoData[e]) && t.anCells ? t.anCells[i] : n;
    }, 1);
  }), Bt("cells().data()", function() {
    return this.iterator("cell", function(t, e, i) {
      return x(t, e, i);
    }, 1);
  }), Ut("cells().cache()", "cell().cache()", function(t) {
    return t = "search" === t ? "_aFilterData" : "_aSortData", this.iterator("cell", function(e, i, n) {
      return e.aoData[i][t][n];
    }, 1);
  }), Ut("cells().render()", "cell().render()", function(t) {
    return this.iterator("cell", function(e, i, n) {
      return x(e, i, n, t);
    }, 1);
  }), Ut("cells().indexes()", "cell().index()", function() {
    return this.iterator("cell", function(t, e, i) {
      return {
        row: e,
        column: i,
        columnVisible: m(t, i)
      };
    }, 1);
  }), Ut("cells().invalidate()", "cell().invalidate()", function(t) {
    return this.iterator("cell", function(e, i, n) {
      I(e, i, t, n);
    });
  }), Bt("cell()", function(t, e, i) {
    return ke(this.cells(t, e, i));
  }), Bt("cell().data()", function(t) {
    var e = this.context, i = this[0];
    return t === n ? e.length && i.length ? x(e[0], i[0].row, i[0].column) : n : (k(e[0], i[0].row, i[0].column, t), 
    I(e[0], i[0].row, "data", i[0].column), this);
  }), Bt("order()", function(e, i) {
    var o = this.context;
    return e === n ? 0 !== o.length ? o[0].aaSorting : n : ("number" == typeof e ? e = [ [ e, i ] ] : e.length && !t.isArray(e[0]) && (e = Array.prototype.slice.call(arguments)), 
    this.iterator("table", function(t) {
      t.aaSorting = e.slice();
    }));
  }), Bt("order.listener()", function(t, e, i) {
    return this.iterator("table", function(n) {
      _t(n, t, e, i);
    });
  }), Bt("order.fixed()", function(e) {
    if (!e) {
      var i = (i = this.context).length ? i[0].aaSortingFixed : n;
      return t.isArray(i) ? {
        pre: i
      } : i;
    }
    return this.iterator("table", function(i) {
      i.aaSortingFixed = t.extend(!0, {}, e);
    });
  }), Bt([ "columns().order()", "column().order()" ], function(e) {
    var i = this;
    return this.iterator("table", function(n, o) {
      var s = [];
      t.each(i[o], function(t, i) {
        s.push([ i, e ]);
      }), n.aaSorting = s;
    });
  }), Bt("search()", function(e, i, o, s) {
    var a = this.context;
    return e === n ? 0 !== a.length ? a[0].oPreviousSearch.sSearch : n : this.iterator("table", function(n) {
      n.oFeatures.bFilter && Q(n, t.extend({}, n.oPreviousSearch, {
        sSearch: e + "",
        bRegex: null !== i && i,
        bSmart: null === o || o,
        bCaseInsensitive: null === s || s
      }), 1);
    });
  }), Ut("columns().search()", "column().search()", function(e, i, o, s) {
    return this.iterator("column", function(a, r) {
      var l = a.aoPreSearchCols;
      if (e === n) return l[r].sSearch;
      a.oFeatures.bFilter && (t.extend(l[r], {
        sSearch: e + "",
        bRegex: null !== i && i,
        bSmart: null === o || o,
        bCaseInsensitive: null === s || s
      }), Q(a, a.oPreviousSearch, 1));
    });
  }), Bt("state()", function() {
    return this.context.length ? this.context[0].oSavedState : null;
  }), Bt("state.clear()", function() {
    return this.iterator("table", function(t) {
      t.fnStateSaveCallback.call(t.oInstance, t, {});
    });
  }), Bt("state.loaded()", function() {
    return this.context.length ? this.context[0].oLoadedState : null;
  }), Bt("state.save()", function() {
    return this.iterator("table", function(t) {
      St(t);
    });
  }), Vt.versionCheck = Vt.fnVersionCheck = function(t) {
    for (var e, i, n = Vt.version.split("."), o = 0, s = (t = t.split(".")).length; o < s; o++) if (e = parseInt(n[o], 10) || 0, 
    i = parseInt(t[o], 10) || 0, e !== i) return e > i;
    return !0;
  }, Vt.isDataTable = Vt.fnIsDataTable = function(e) {
    var i = t(e).get(0), n = !1;
    return e instanceof Vt.Api || (t.each(Vt.settings, function(e, o) {
      var s = o.nScrollHead ? t("table", o.nScrollHead)[0] : null, a = o.nScrollFoot ? t("table", o.nScrollFoot)[0] : null;
      o.nTable !== i && s !== i && a !== i || (n = !0);
    }), n);
  }, Vt.tables = Vt.fnTables = function(e) {
    var i = !1;
    t.isPlainObject(e) && (i = e.api, e = e.visible);
    var n = t.map(Vt.settings, function(i) {
      if (!e || e && t(i.nTable).is(":visible")) return i.nTable;
    });
    return i ? new zt(n) : n;
  }, Vt.camelToHungarian = s, Bt("$()", function(e, i) {
    var n = this.rows(i).nodes(), n = t(n);
    return t([].concat(n.filter(e).toArray(), n.find(e).toArray()));
  }), t.each([ "on", "one", "off" ], function(e, i) {
    Bt(i + "()", function() {
      var e = Array.prototype.slice.call(arguments);
      e[0] = t.map(e[0].split(/\s/), function(t) {
        return t.match(/\.dt\b/) ? t : t + ".dt";
      }).join(" ");
      var n = t(this.tables().nodes());
      return n[i].apply(n, e), this;
    });
  }), Bt("clear()", function() {
    return this.iterator("table", function(t) {
      T(t);
    });
  }), Bt("settings()", function() {
    return new zt(this.context, this.context);
  }), Bt("init()", function() {
    var t = this.context;
    return t.length ? t[0].oInit : null;
  }), Bt("data()", function() {
    return this.iterator("table", function(t) {
      return oe(t.aoData, "_aData");
    }).flatten();
  }), Bt("destroy()", function(i) {
    return i = i || !1, this.iterator("table", function(n) {
      var o, s = n.nTableWrapper.parentNode, a = n.oClasses, r = n.nTable, l = n.nTBody, c = n.nTHead, d = n.nTFoot, u = t(r), l = t(l), h = t(n.nTableWrapper), p = t.map(n.aoData, function(t) {
        return t.nTr;
      });
      n.bDestroying = !0, Ot(n, "aoDestroyCallback", "destroy", [ n ]), i || new zt(n).columns().visible(!0), 
      h.off(".DT").find(":not(tbody *)").off(".DT"), t(e).off(".DT-" + n.sInstance), r != c.parentNode && (u.children("thead").detach(), 
      u.append(c)), d && r != d.parentNode && (u.children("tfoot").detach(), u.append(d)), 
      n.aaSorting = [], n.aaSortingFixed = [], $t(n), t(p).removeClass(n.asStripeClasses.join(" ")), 
      t("th, td", c).removeClass(a.sSortable + " " + a.sSortableAsc + " " + a.sSortableDesc + " " + a.sSortableNone), 
      n.bJUI && (t("th span." + a.sSortIcon + ", td span." + a.sSortIcon, c).detach(), 
      t("th, td", c).each(function() {
        var e = t("div." + a.sSortJUIWrapper, this);
        t(this).append(e.contents()), e.detach();
      })), l.children().detach(), l.append(p), u[c = i ? "remove" : "detach"](), h[c](), 
      !i && s && (s.insertBefore(r, n.nTableReinsertBefore), u.css("width", n.sDestroyWidth).removeClass(a.sTable), 
      (o = n.asDestroyStripes.length) && l.children().each(function(e) {
        t(this).addClass(n.asDestroyStripes[e % o]);
      })), -1 !== (s = t.inArray(n, Vt.settings)) && Vt.settings.splice(s, 1);
    });
  }), t.each([ "column", "row", "cell" ], function(t, e) {
    Bt(e + "s().every()", function(t) {
      var i = this.selector.opts, o = this;
      return this.iterator(e, function(s, a, r, l, c) {
        t.call(o[e](a, "cell" === e ? r : i, "cell" === e ? i : n), a, r, l, c);
      });
    });
  }), Bt("i18n()", function(e, i, o) {
    var s = this.context[0];
    return (e = $(e)(s.oLanguage)) === n && (e = i), o !== n && t.isPlainObject(e) && (e = e[o] !== n ? e[o] : e._), 
    e.replace("%d", o);
  }), Vt.version = "1.10.15", Vt.settings = [], Vt.models = {}, Vt.models.oSearch = {
    bCaseInsensitive: !0,
    sSearch: "",
    bRegex: !1,
    bSmart: !0
  }, Vt.models.oRow = {
    nTr: null,
    anCells: null,
    _aData: [],
    _aSortData: null,
    _aFilterData: null,
    _sFilterRow: null,
    _sRowStripe: "",
    src: null,
    idx: -1
  }, Vt.models.oColumn = {
    idx: null,
    aDataSort: null,
    asSorting: null,
    bSearchable: null,
    bSortable: null,
    bVisible: null,
    _sManualType: null,
    _bAttrSrc: !1,
    fnCreatedCell: null,
    fnGetData: null,
    fnSetData: null,
    mData: null,
    mRender: null,
    nTh: null,
    nTf: null,
    sClass: null,
    sContentPadding: null,
    sDefaultContent: null,
    sName: null,
    sSortDataType: "std",
    sSortingClass: null,
    sSortingClassJUI: null,
    sTitle: null,
    sType: null,
    sWidth: null,
    sWidthOrig: null
  }, Vt.defaults = {
    aaData: null,
    aaSorting: [ [ 0, "asc" ] ],
    aaSortingFixed: [],
    ajax: null,
    aLengthMenu: [ 10, 25, 50, 100 ],
    aoColumns: null,
    aoColumnDefs: null,
    aoSearchCols: [],
    asStripeClasses: null,
    bAutoWidth: !0,
    bDeferRender: !1,
    bDestroy: !1,
    bFilter: !0,
    bInfo: !0,
    bJQueryUI: !1,
    bLengthChange: !0,
    bPaginate: !0,
    bProcessing: !1,
    bRetrieve: !1,
    bScrollCollapse: !1,
    bServerSide: !1,
    bSort: !0,
    bSortMulti: !0,
    bSortCellsTop: !1,
    bSortClasses: !0,
    bStateSave: !1,
    fnCreatedRow: null,
    fnDrawCallback: null,
    fnFooterCallback: null,
    fnFormatNumber: function(t) {
      return t.toString().replace(/\B(?=(\d{3})+(?!\d))/g, this.oLanguage.sThousands);
    },
    fnHeaderCallback: null,
    fnInfoCallback: null,
    fnInitComplete: null,
    fnPreDrawCallback: null,
    fnRowCallback: null,
    fnServerData: null,
    fnServerParams: null,
    fnStateLoadCallback: function(t) {
      try {
        return JSON.parse((-1 === t.iStateDuration ? sessionStorage : localStorage).getItem("DataTables_" + t.sInstance + "_" + location.pathname));
      } catch (t) {}
    },
    fnStateLoadParams: null,
    fnStateLoaded: null,
    fnStateSaveCallback: function(t, e) {
      try {
        (-1 === t.iStateDuration ? sessionStorage : localStorage).setItem("DataTables_" + t.sInstance + "_" + location.pathname, JSON.stringify(e));
      } catch (t) {}
    },
    fnStateSaveParams: null,
    iStateDuration: 7200,
    iDeferLoading: null,
    iDisplayLength: 10,
    iDisplayStart: 0,
    iTabIndex: 0,
    oClasses: {},
    oLanguage: {
      oAria: {
        sSortAscending: ": activate to sort column ascending",
        sSortDescending: ": activate to sort column descending"
      },
      oPaginate: {
        sFirst: "First",
        sLast: "Last",
        sNext: "Next",
        sPrevious: "Previous"
      },
      sEmptyTable: "No data available in table",
      sInfo: "Showing _START_ to _END_ of _TOTAL_ entries",
      sInfoEmpty: "Showing 0 to 0 of 0 entries",
      sInfoFiltered: "(filtered from _MAX_ total entries)",
      sInfoPostFix: "",
      sDecimal: "",
      sThousands: ",",
      sLengthMenu: "Show _MENU_ entries",
      sLoadingRecords: "Loading...",
      sProcessing: "Processing...",
      sSearch: "Search:",
      sSearchPlaceholder: "",
      sUrl: "",
      sZeroRecords: "No matching records found"
    },
    oSearch: t.extend({}, Vt.models.oSearch),
    sAjaxDataProp: "data",
    sAjaxSource: null,
    sDom: "lfrtip",
    searchDelay: null,
    sPaginationType: "simple_numbers",
    sScrollX: "",
    sScrollXInner: "",
    sScrollY: "",
    sServerMethod: "GET",
    renderer: null,
    rowId: "DT_RowId"
  }, o(Vt.defaults), Vt.defaults.column = {
    aDataSort: null,
    iDataSort: -1,
    asSorting: [ "asc", "desc" ],
    bSearchable: !0,
    bSortable: !0,
    bVisible: !0,
    fnCreatedCell: null,
    mData: null,
    mRender: null,
    sCellType: "td",
    sClass: "",
    sContentPadding: "",
    sDefaultContent: null,
    sName: "",
    sSortDataType: "std",
    sTitle: null,
    sType: null,
    sWidth: null
  }, o(Vt.defaults.column), Vt.models.oSettings = {
    oFeatures: {
      bAutoWidth: null,
      bDeferRender: null,
      bFilter: null,
      bInfo: null,
      bLengthChange: null,
      bPaginate: null,
      bProcessing: null,
      bServerSide: null,
      bSort: null,
      bSortMulti: null,
      bSortClasses: null,
      bStateSave: null
    },
    oScroll: {
      bCollapse: null,
      iBarWidth: 0,
      sX: null,
      sXInner: null,
      sY: null
    },
    oLanguage: {
      fnInfoCallback: null
    },
    oBrowser: {
      bScrollOversize: !1,
      bScrollbarLeft: !1,
      bBounding: !1,
      barWidth: 0
    },
    ajax: null,
    aanFeatures: [],
    aoData: [],
    aiDisplay: [],
    aiDisplayMaster: [],
    aIds: {},
    aoColumns: [],
    aoHeader: [],
    aoFooter: [],
    oPreviousSearch: {},
    aoPreSearchCols: [],
    aaSorting: null,
    aaSortingFixed: [],
    asStripeClasses: null,
    asDestroyStripes: [],
    sDestroyWidth: 0,
    aoRowCallback: [],
    aoHeaderCallback: [],
    aoFooterCallback: [],
    aoDrawCallback: [],
    aoRowCreatedCallback: [],
    aoPreDrawCallback: [],
    aoInitComplete: [],
    aoStateSaveParams: [],
    aoStateLoadParams: [],
    aoStateLoaded: [],
    sTableId: "",
    nTable: null,
    nTHead: null,
    nTFoot: null,
    nTBody: null,
    nTableWrapper: null,
    bDeferLoading: !1,
    bInitialised: !1,
    aoOpenRows: [],
    sDom: null,
    searchDelay: null,
    sPaginationType: "two_button",
    iStateDuration: 0,
    aoStateSave: [],
    aoStateLoad: [],
    oSavedState: null,
    oLoadedState: null,
    sAjaxSource: null,
    sAjaxDataProp: null,
    bAjaxDataGet: !0,
    jqXHR: null,
    json: n,
    oAjaxData: n,
    fnServerData: null,
    aoServerParams: [],
    sServerMethod: null,
    fnFormatNumber: null,
    aLengthMenu: null,
    iDraw: 0,
    bDrawing: !1,
    iDrawError: -1,
    _iDisplayLength: 10,
    _iDisplayStart: 0,
    _iRecordsTotal: 0,
    _iRecordsDisplay: 0,
    bJUI: null,
    oClasses: {},
    bFiltered: !1,
    bSorted: !1,
    bSortCellsTop: null,
    oInit: null,
    aoDestroyCallback: [],
    fnRecordsTotal: function() {
      return "ssp" == qt(this) ? 1 * this._iRecordsTotal : this.aiDisplayMaster.length;
    },
    fnRecordsDisplay: function() {
      return "ssp" == qt(this) ? 1 * this._iRecordsDisplay : this.aiDisplay.length;
    },
    fnDisplayEnd: function() {
      var t = this._iDisplayLength, e = this._iDisplayStart, i = e + t, n = this.aiDisplay.length, o = this.oFeatures, s = o.bPaginate;
      return o.bServerSide ? !1 === s || -1 === t ? e + n : Math.min(e + t, this._iRecordsDisplay) : !s || i > n || -1 === t ? n : i;
    },
    oInstance: null,
    sInstance: null,
    iTabIndex: 0,
    nScrollHead: null,
    nScrollFoot: null,
    aLastSort: [],
    oPlugins: {},
    rowIdFn: null,
    rowId: null
  }, Vt.ext = Rt = {
    buttons: {},
    classes: {},
    builder: "-source-",
    errMode: "alert",
    feature: [],
    search: [],
    selector: {
      cell: [],
      column: [],
      row: []
    },
    internal: {},
    legacy: {
      ajax: null
    },
    pager: {},
    renderer: {
      pageButton: {},
      header: {}
    },
    order: {},
    type: {
      detect: [],
      search: {},
      order: {}
    },
    _unique: 0,
    fnVersionCheck: Vt.fnVersionCheck,
    iApiIndex: 0,
    oJUIClasses: {},
    sVersion: Vt.version
  }, t.extend(Rt, {
    afnFiltering: Rt.search,
    aTypes: Rt.type.detect,
    ofnSearch: Rt.type.search,
    oSort: Rt.type.order,
    afnSortData: Rt.order,
    aoFeatures: Rt.feature,
    oApi: Rt.internal,
    oStdClasses: Rt.classes,
    oPagination: Rt.pager
  }), t.extend(Vt.ext.classes, {
    sTable: "dataTable",
    sNoFooter: "no-footer",
    sPageButton: "paginate_button",
    sPageButtonActive: "current",
    sPageButtonDisabled: "disabled",
    sStripeOdd: "odd",
    sStripeEven: "even",
    sRowEmpty: "dataTables_empty",
    sWrapper: "dataTables_wrapper",
    sFilter: "dataTables_filter",
    sInfo: "dataTables_info",
    sPaging: "dataTables_paginate paging_",
    sLength: "dataTables_length",
    sProcessing: "dataTables_processing",
    sSortAsc: "sorting_asc",
    sSortDesc: "sorting_desc",
    sSortable: "sorting",
    sSortableAsc: "sorting_asc_disabled",
    sSortableDesc: "sorting_desc_disabled",
    sSortableNone: "sorting_disabled",
    sSortColumn: "sorting_",
    sFilterInput: "",
    sLengthSelect: "",
    sScrollWrapper: "dataTables_scroll",
    sScrollHead: "dataTables_scrollHead",
    sScrollHeadInner: "dataTables_scrollHeadInner",
    sScrollBody: "dataTables_scrollBody",
    sScrollFoot: "dataTables_scrollFoot",
    sScrollFootInner: "dataTables_scrollFootInner",
    sHeaderTH: "",
    sFooterTH: "",
    sSortJUIAsc: "",
    sSortJUIDesc: "",
    sSortJUI: "",
    sSortJUIAscAllowed: "",
    sSortJUIDescAllowed: "",
    sSortJUIWrapper: "",
    sSortIcon: "",
    sJUIHeader: "",
    sJUIFooter: ""
  });
  var De = "ui-state-default", Ie = "css_right ui-icon ui-icon-", je = "fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix";
  t.extend(Vt.ext.oJUIClasses, Vt.ext.classes, {
    sPageButton: "fg-button ui-button " + De,
    sPageButtonActive: "ui-state-disabled",
    sPageButtonDisabled: "ui-state-disabled",
    sPaging: "dataTables_paginate fg-buttonset ui-buttonset fg-buttonset-multi ui-buttonset-multi paging_",
    sSortAsc: De + " sorting_asc",
    sSortDesc: De + " sorting_desc",
    sSortable: De + " sorting",
    sSortableAsc: De + " sorting_asc_disabled",
    sSortableDesc: De + " sorting_desc_disabled",
    sSortableNone: De + " sorting_disabled",
    sSortJUIAsc: Ie + "triangle-1-n",
    sSortJUIDesc: Ie + "triangle-1-s",
    sSortJUI: Ie + "carat-2-n-s",
    sSortJUIAscAllowed: Ie + "carat-1-n",
    sSortJUIDescAllowed: Ie + "carat-1-s",
    sSortJUIWrapper: "DataTables_sort_wrapper",
    sSortIcon: "DataTables_sort_icon",
    sScrollHead: "dataTables_scrollHead " + De,
    sScrollFoot: "dataTables_scrollFoot " + De,
    sHeaderTH: De,
    sFooterTH: De,
    sJUIHeader: je + " ui-corner-tl ui-corner-tr",
    sJUIFooter: je + " ui-corner-bl ui-corner-br"
  });
  var Pe = Vt.ext.pager;
  t.extend(Pe, {
    simple: function() {
      return [ "previous", "next" ];
    },
    full: function() {
      return [ "first", "previous", "next", "last" ];
    },
    numbers: function(t, e) {
      return [ Ht(t, e) ];
    },
    simple_numbers: function(t, e) {
      return [ "previous", Ht(t, e), "next" ];
    },
    full_numbers: function(t, e) {
      return [ "first", "previous", Ht(t, e), "next", "last" ];
    },
    first_last_numbers: function(t, e) {
      return [ "first", Ht(t, e), "last" ];
    },
    _numbers: Ht,
    numbers_length: 7
  }), t.extend(!0, Vt.ext.renderer, {
    pageButton: {
      _: function(e, o, s, a, r, l) {
        var c, d, u, h = e.oClasses, p = e.oLanguage.oPaginate, f = e.oLanguage.oAria.paginate || {}, m = 0, g = function(i, n) {
          var o, a, u, v;
          for (o = 0, a = n.length; o < a; o++) if (v = n[o], t.isArray(v)) u = t("<" + (v.DT_el || "div") + "/>").appendTo(i), 
          g(u, v); else {
            switch (c = null, d = "", v) {
             case "ellipsis":
              i.append('<span class="ellipsis">&#x2026;</span>');
              break;

             case "first":
              c = p.sFirst, d = v + (r > 0 ? "" : " " + h.sPageButtonDisabled);
              break;

             case "previous":
              c = p.sPrevious, d = v + (r > 0 ? "" : " " + h.sPageButtonDisabled);
              break;

             case "next":
              c = p.sNext, d = v + (r < l - 1 ? "" : " " + h.sPageButtonDisabled);
              break;

             case "last":
              c = p.sLast, d = v + (r < l - 1 ? "" : " " + h.sPageButtonDisabled);
              break;

             default:
              c = v + 1, d = r === v ? h.sPageButtonActive : "";
            }
            null !== c && (Et(u = t("<a>", {
              class: h.sPageButton + " " + d,
              "aria-controls": e.sTableId,
              "aria-label": f[v],
              "data-dt-idx": m,
              tabindex: e.iTabIndex,
              id: 0 === s && "string" == typeof v ? e.sTableId + "_" + v : null
            }).html(c).appendTo(i), {
              action: v
            }, function(t) {
              ct(e, t.data.action, !0);
            }), m++);
          }
        };
        try {
          u = t(o).find(i.activeElement).data("dt-idx");
        } catch (t) {}
        g(t(o).empty(), a), u !== n && t(o).find("[data-dt-idx=" + u + "]").focus();
      }
    }
  }), t.extend(Vt.ext.type.detect, [ function(t, e) {
    var i = e.oLanguage.sDecimal;
    return ie(t, i) ? "num" + i : null;
  }, function(t) {
    if (t && !(t instanceof Date) && !Gt.test(t)) return null;
    var e = Date.parse(t);
    return null !== e && !isNaN(e) || Zt(t) ? "date" : null;
  }, function(t, e) {
    var i = e.oLanguage.sDecimal;
    return ie(t, i, !0) ? "num-fmt" + i : null;
  }, function(t, e) {
    var i = e.oLanguage.sDecimal;
    return ne(t, i) ? "html-num" + i : null;
  }, function(t, e) {
    var i = e.oLanguage.sDecimal;
    return ne(t, i, !0) ? "html-num-fmt" + i : null;
  }, function(t) {
    return Zt(t) || "string" == typeof t && -1 !== t.indexOf("<") ? "html" : null;
  } ]), t.extend(Vt.ext.type.search, {
    html: function(t) {
      return Zt(t) ? t : "string" == typeof t ? t.replace(Xt, " ").replace(Yt, "") : "";
    },
    string: function(t) {
      return Zt(t) ? t : "string" == typeof t ? t.replace(Xt, " ") : t;
    }
  });
  var Ee = function(t, e, i, n) {
    return 0 === t || t && "-" !== t ? (e && (t = ee(t, e)), t.replace && (i && (t = t.replace(i, "")), 
    n && (t = t.replace(n, ""))), 1 * t) : -1 / 0;
  };
  t.extend(Rt.type.order, {
    "date-pre": function(t) {
      return Date.parse(t) || -1 / 0;
    },
    "html-pre": function(t) {
      return Zt(t) ? "" : t.replace ? t.replace(/<.*?>/g, "").toLowerCase() : t + "";
    },
    "string-pre": function(t) {
      return Zt(t) ? "" : "string" == typeof t ? t.toLowerCase() : t.toString ? t.toString() : "";
    },
    "string-asc": function(t, e) {
      return t < e ? -1 : t > e ? 1 : 0;
    },
    "string-desc": function(t, e) {
      return t < e ? 1 : t > e ? -1 : 0;
    }
  }), Lt(""), t.extend(!0, Vt.ext.renderer, {
    header: {
      _: function(e, i, n, o) {
        t(e.nTable).on("order.dt.DT", function(t, s, a, r) {
          e === s && (t = n.idx, i.removeClass(n.sSortingClass + " " + o.sSortAsc + " " + o.sSortDesc).addClass("asc" == r[t] ? o.sSortAsc : "desc" == r[t] ? o.sSortDesc : n.sSortingClass));
        });
      },
      jqueryui: function(e, i, n, o) {
        t("<div/>").addClass(o.sSortJUIWrapper).append(i.contents()).append(t("<span/>").addClass(o.sSortIcon + " " + n.sSortingClassJUI)).appendTo(i), 
        t(e.nTable).on("order.dt.DT", function(t, s, a, r) {
          e === s && (t = n.idx, i.removeClass(o.sSortAsc + " " + o.sSortDesc).addClass("asc" == r[t] ? o.sSortAsc : "desc" == r[t] ? o.sSortDesc : n.sSortingClass), 
          i.find("span." + o.sSortIcon).removeClass(o.sSortJUIAsc + " " + o.sSortJUIDesc + " " + o.sSortJUI + " " + o.sSortJUIAscAllowed + " " + o.sSortJUIDescAllowed).addClass("asc" == r[t] ? o.sSortJUIAsc : "desc" == r[t] ? o.sSortJUIDesc : n.sSortingClassJUI));
        });
      }
    }
  });
  var Ne = function(t) {
    return "string" == typeof t ? t.replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;") : t;
  };
  return Vt.render = {
    number: function(t, e, i, n, o) {
      return {
        display: function(s) {
          if ("number" != typeof s && "string" != typeof s) return s;
          var a = 0 > s ? "-" : "", r = parseFloat(s);
          return isNaN(r) ? Ne(s) : (r = r.toFixed(i), s = Math.abs(r), r = parseInt(s, 10), 
          s = i ? e + (s - r).toFixed(i).substring(2) : "", a + (n || "") + r.toString().replace(/\B(?=(\d{3})+(?!\d))/g, t) + s + (o || ""));
        }
      };
    },
    text: function() {
      return {
        display: Ne
      };
    }
  }, t.extend(Vt.ext.internal, {
    _fnExternApiFunc: Wt,
    _fnBuildAjax: W,
    _fnAjaxUpdate: R,
    _fnAjaxParameters: z,
    _fnAjaxUpdateDraw: B,
    _fnAjaxDataSrc: U,
    _fnAddColumn: u,
    _fnColumnOptions: h,
    _fnAdjustColumnSizing: p,
    _fnVisibleToColumnIndex: f,
    _fnColumnIndexToVisible: m,
    _fnVisbleColumns: g,
    _fnGetColumns: v,
    _fnColumnTypes: b,
    _fnApplyColumnDefs: C,
    _fnHungarianMap: o,
    _fnCamelToHungarian: s,
    _fnLanguageCompat: a,
    _fnBrowserDetect: c,
    _fnAddData: y,
    _fnAddTr: w,
    _fnNodeToDataIndex: function(t, e) {
      return e._DT_RowIndex !== n ? e._DT_RowIndex : null;
    },
    _fnNodeToColumnIndex: function(e, i, n) {
      return t.inArray(n, e.aoData[i].anCells);
    },
    _fnGetCellData: x,
    _fnSetCellData: k,
    _fnSplitObjNotation: _,
    _fnGetObjectDataFn: $,
    _fnSetObjectDataFn: A,
    _fnGetDataMaster: S,
    _fnClearTable: T,
    _fnDeleteIndex: D,
    _fnInvalidate: I,
    _fnGetRowElements: j,
    _fnCreateTr: P,
    _fnBuildHead: N,
    _fnDrawHead: O,
    _fnDraw: M,
    _fnReDraw: F,
    _fnAddOptionsHtml: q,
    _fnDetectHeader: H,
    _fnGetUniqueThs: L,
    _fnFeatureHtmlFilter: V,
    _fnFilterComplete: Q,
    _fnFilterCustom: X,
    _fnFilterColumn: Y,
    _fnFilter: G,
    _fnFilterCreateSearch: K,
    _fnEscapeRegex: he,
    _fnFilterData: J,
    _fnFeatureHtmlInfo: et,
    _fnUpdateInfo: it,
    _fnInfoMacros: nt,
    _fnInitialise: ot,
    _fnInitComplete: st,
    _fnLengthChange: at,
    _fnFeatureHtmlLength: rt,
    _fnFeatureHtmlPaginate: lt,
    _fnPageChange: ct,
    _fnFeatureHtmlProcessing: dt,
    _fnProcessingDisplay: ut,
    _fnFeatureHtmlTable: ht,
    _fnScrollDraw: pt,
    _fnApplyToChildren: ft,
    _fnCalculateColumnWidths: mt,
    _fnThrottle: ge,
    _fnConvertToWidth: gt,
    _fnGetWidestNode: vt,
    _fnGetMaxLenString: bt,
    _fnStringToCss: Ct,
    _fnSortFlatten: yt,
    _fnSort: wt,
    _fnSortAria: xt,
    _fnSortListener: kt,
    _fnSortAttachListener: _t,
    _fnSortingClasses: $t,
    _fnSortData: At,
    _fnSaveState: St,
    _fnLoadState: Tt,
    _fnSettingsFromNode: Dt,
    _fnLog: It,
    _fnMap: jt,
    _fnBindAction: Et,
    _fnCallbackReg: Nt,
    _fnCallbackFire: Ot,
    _fnLengthOverflow: Mt,
    _fnRenderer: Ft,
    _fnDataSource: qt,
    _fnRowAttributes: E,
    _fnCalculateEnd: function() {}
  }), t.fn.dataTable = Vt, Vt.$ = t, t.fn.dataTableSettings = Vt.settings, t.fn.dataTableExt = Vt.ext, 
  t.fn.DataTable = function(e) {
    return t(this).dataTable(e).api();
  }, t.each(Vt, function(e, i) {
    t.fn.DataTable[e] = i;
  }), t.fn.dataTable;
}), function(t) {
  t.fn.angle = function(e) {
    if ("object" == typeof e) return this.each(function() {
      new t.angle(this, e);
    });
    var i = this.data("angle");
    switch (e) {
     case "resize":
      i.init();
    }
  }, t.fn.angle.defaults = {
    speed: 2,
    drag: !1,
    previous: "",
    next: "",
    current: 0,
    get_image: function() {},
    after: function() {}
  }, t.angle = function(e, n) {
    var o = t.extend({}, t.fn.angle.defaults, n), s = t(e);
    t.data(e, "angle", s);
    var a, r, l, c, d, u, h, p = 0, f = 0;
    s.attr("data-current", o.current);
    var m, g = !1, v = !1, b = !1, C = !0, y = function() {
      (a = t(e.parentNode).css("width")) && -1 === a.indexOf("%") || (a = e.parentNode.getBoundingClientRect().width || e.parentNode.offsetWidth), 
      a = parseInt(a), r = s.find("img:first"), l = r.offset().left, (c = s.find("img")).each(function(e) {
        t(this).attr("data-id", e);
      }), d = c.size(), u = parseInt(a / d / o.speed), c.toggle(), h = s.data("current"), 
      c.eq(h).toggle();
    }, w = function() {
      C = !1;
      var i = o.get_image();
      if ("object" == typeof i) {
        var n = "";
        for (var s in i) n += '<li><img src="' + i[s] + '" /></li>';
        t(e).children("ul").html(n), y(), A();
      }
    };
    o.previous && t(o.previous).click(function() {
      C && w(), x();
    }), o.next && t(o.next).click(function() {
      C && w(), k();
    });
    var x = function() {
      c.eq(h).toggle(), h = _(h - 1), c.eq(h).toggle(), s.attr("data-current", h), S(o.after());
    }, k = function() {
      c.eq(h).toggle(), h = _(h + 1), c.eq(h).toggle(), s.attr("data-current", h), S(o.after());
    }, _ = function(t) {
      return (d + t % d) % d;
    }, $ = {
      handleEvent: function(t) {
        switch (t.type) {
         case "touchstart":
          this.start(t);
          break;

         case "touchmove":
          this.move(t);
          break;

         case "touchend":
          this.end();
        }
      },
      start: function(t) {
        if (b = !0) {
          var i = t.touches[0];
          startPos = {
            x: i.pageX,
            y: i.pageY
          }, C && w(), p = f = parseInt((startPos.x - l) / u), m = void 0, delta = {}, e.addEventListener("touchmove", this, !1), 
          e.addEventListener("touchend", this, !1);
        }
      },
      move: function(t) {
        if (b) {
          var e = t.touches[0];
          delta = {
            x: e.pageX - startPos.x,
            y: e.pageY - startPos.y
          }, void 0 === m && (m = !!(m || Math.abs(delta.x) < Math.abs(delta.y))), m || (t.preventDefault(), 
          (f = parseInt((e.pageX - l) / u)) !== p && (f > p ? k() : x(), p = f));
        }
      },
      end: function() {
        b = !1, e.removeEventListener("touchmove", this, !1), e.removeEventListener("touchend", this, !1);
      }
    }, A = function() {
      if (o.drag) {
        var n = null;
        for (i in document.images) document.images[i].ondragstart = function() {
          return !1;
        };
        t(e).mousedown(function(t) {
          (v = !0) && (n = t.screenX, C && w(), p = f = parseInt((n - l) / u));
        }), t(e).mousemove(function(t) {
          v && n && (t.preventDefault(), (f = parseInt((t.screenX - l) / u)) !== p && (f > p ? k() : x(), 
          p = f));
        }), t(e).mouseup(function() {
          g = !1, n = null;
        });
      } else t(e).mouseover(function(t) {
        (g = !0) && (C && w(), p = f = parseInt((t.screenX - l) / u));
      }), t(e).mousemove(function(t) {
        g && (f = parseInt((t.screenX - l) / u)) !== p && (f > p ? k() : x(), p = f);
      }), t(e).mouseout(function() {
        g = !1;
      });
    };
    "ontouchstart" in window && e.addEventListener("touchstart", $, !1), s.setPosition = function(t) {
      this.find("img[data-id='" + t + "']").css("display", "inline"), this.find("img[data-id!='" + t + "']").css("display", "none"), 
      s.attr("data-current", t), h = t;
    };
    var S = function(t) {
      setTimeout(function() {}, 0);
    };
    y(), A();
  };
}(jQuery), function(t) {
  var e = {
    url: !1,
    callback: !1,
    target: !1,
    duration: 120,
    on: "mouseover",
    touch: !0,
    onZoomIn: !1,
    onZoomOut: !1,
    magnify: 1
  };
  t.zoom = function(e, i, n, o) {
    var s, a, r, l, c, d, u, h = t(e), p = h.css("position"), f = t(i);
    return e.style.position = /(absolute|fixed)/.test(p) ? p : "relative", e.style.overflow = "hidden", 
    n.style.width = n.style.height = "", t(n).addClass("zoomImg").css({
      position: "absolute",
      top: 0,
      left: 0,
      opacity: 0,
      width: n.width * o,
      height: n.height * o,
      border: "none",
      maxWidth: "none",
      maxHeight: "none"
    }).appendTo(e), {
      init: function() {
        a = h.outerWidth(), s = h.outerHeight(), i === e ? (l = a, r = s) : (l = f.outerWidth(), 
        r = f.outerHeight()), c = (n.width - a) / l, d = (n.height - s) / r, u = f.offset();
      },
      move: function(t) {
        var e = t.pageX - u.left, i = t.pageY - u.top;
        i = Math.max(Math.min(i, r), 0), e = Math.max(Math.min(e, l), 0), n.style.left = e * -c + "px", 
        n.style.top = i * -d + "px";
      }
    };
  }, t.fn.zoom = function(i) {
    return this.each(function() {
      var n = t.extend({}, e, i || {}), o = n.target && t(n.target)[0] || this, s = this, a = t(s), r = document.createElement("img"), l = t(r), c = "mousemove.zoom", d = !1, u = !1;
      if (!n.url) {
        var h = s.querySelector("img");
        if (h && (n.url = h.getAttribute("data-src") || h.currentSrc || h.src), !n.url) return;
      }
      a.one("zoom.destroy", function(t, e) {
        a.off(".zoom"), o.style.position = t, o.style.overflow = e, r.onload = null, l.remove();
      }.bind(this, o.style.position, o.style.overflow)), r.onload = function() {
        function e(e) {
          h.init(), h.move(e), l.stop().fadeTo(t.support.opacity ? n.duration : 0, 1, !!t.isFunction(n.onZoomIn) && n.onZoomIn.call(r));
        }
        function i() {
          l.stop().fadeTo(n.duration, 0, !!t.isFunction(n.onZoomOut) && n.onZoomOut.call(r));
        }
        var h = t.zoom(o, s, r, n.magnify);
        "grab" === n.on ? a.on("mousedown.zoom", function(n) {
          1 === n.which && (t(document).one("mouseup.zoom", function() {
            i(), t(document).off(c, h.move);
          }), e(n), t(document).on(c, h.move), n.preventDefault());
        }) : "click" === n.on ? a.on("click.zoom", function(n) {
          return d ? void 0 : (d = !0, e(n), t(document).on(c, h.move), t(document).one("click.zoom", function() {
            i(), d = !1, t(document).off(c, h.move);
          }), !1);
        }) : "toggle" === n.on ? a.on("click.zoom", function(t) {
          d ? i() : e(t), d = !d;
        }) : "mouseover" === n.on && (h.init(), a.on("mouseenter.zoom", e).on("mouseleave.zoom", i).on(c, h.move)), 
        n.touch && a.on("touchstart.zoom", function(t) {
          t.preventDefault(), u ? (u = !1, i()) : (u = !0, e(t.originalEvent.touches[0] || t.originalEvent.changedTouches[0]));
        }).on("touchmove.zoom", function(t) {
          t.preventDefault(), h.move(t.originalEvent.touches[0] || t.originalEvent.changedTouches[0]);
        }).on("touchend.zoom", function(t) {
          t.preventDefault(), u && (u = !1, i());
        }), t.isFunction(n.callback) && n.callback.call(r);
      }, r.setAttribute("role", "presentation"), r.src = n.url;
    });
  }, t.fn.zoom.defaults = e;
}(window.jQuery), function(t) {
  t.fn.slickActivator = function() {
    console.log("jquery.slickActivator.js - loaded", t(this).attr("class"));
    var e = void 0 !== t(this).data("carousel-mobile-dots") && !0 === t(this).data("carousel-mobile-dots"), i = (void 0 !== t(this).data("carousel-mobile-autoplay") && t(this).data("carousel-mobile-autoplay"), 
    void 0 !== t(this).data("carousel-mobile-arrows") && !0 === t(this).data("carousel-mobile-arrows")), n = void 0 !== t(this).data("carousel-mobile-items") ? t(this).data("carousel-mobile-items") : 1, o = void 0 !== t(this).data("carousel-mobile-center") && !0 === t(this).data("carousel-mobile-center"), s = void 0 !== t(this).data("carousel-desktop-dots") && !0 === t(this).data("carousel-desktop-dots"), a = void 0 !== t(this).data("carousel-desktop-arrows") && !0 === t(this).data("carousel-desktop-arrows"), r = void 0 !== t(this).data("carousel-desktop-items") ? t(this).data("carousel-desktop-items") : 1, l = void 0 !== t(this).data("carousel-desktop-center") && !0 === t(this).data("carousel-desktop-center"), c = (void 0 !== t(this).data("carousel-width-xs") && t(this).data("carousel-width-xs"), 
    void 0 !== t(this).data("carousel-width-sm") && !0 === t(this).data("carousel-width-sm")), d = void 0 !== t(this).data("carousel-width-lg") && !0 === t(this).data("carousel-width-lg"), u = "undefined" !== t(this).data("carousel-arrow-color") && "string" == typeof t(this).data("carousel-arrow-color") ? t(this).data("carousel-arrow-color") : "black", h = "undefined" !== t(this).data("carousel-dir") && "rtl" === t(this).data("carousel-dir"), p = void 0 !== t(this).data("carousel-autoplay") && !0 === t(this).data("carousel-autoplay"), f = void 0 !== t(this).data("carousel-speed") ? Number(t(this).data("carousel-speed")) : 500, m = {
      autoplay: p,
      centerPadding: "0",
      rtl: h,
      mobileFirst: !0,
      prevArrow: '<div class="a-left control-c prev slick-prev"><img src="' + ACC.config.themeResourcePath + "/images/slick/arrow-left-" + u + '.png" /></div>',
      nextArrow: '<div class="a-right control-c next slick-next"><img src="' + ACC.config.themeResourcePath + "/images/slick/arrow-right-" + u + '.png" /></div>',
      responsive: [ {
        breakpoint: 0,
        settings: {
          arrows: i,
          centerMode: o,
          dots: e,
          slidesToShow: n,
          variableWidth: c
        }
      }, {
        breakpoint: 1023,
        settings: {
          arrows: a,
          centerMode: l,
          dots: s,
          slidesToShow: r,
          variableWidth: d
        }
      } ],
      slidesToScroll: 1,
      speed: f
    };
    console.log(m), t(this).parents(".js-carousel-disabled").length ? t(this).addClass("not-slicked") : t(this).slick(m);
  };
}(jQuery), ACC.cartitem = {
  _autoload: [ "bindCartItem" ],
  submitTriggered: !1,
  bindCartItem: function() {
    $(".js-remove-entry-button").on("click", function() {
      var t = $(this).attr("id").split("_"), e = $("#updateCartForm" + t[1]), i = e.find("input[name=productCode]").val(), n = e.find("input[name=initialQuantity]"), o = e.find("input[name=quantity]");
      ACC.track.trackRemoveFromCart(i, n.val()), o.val(0), n.val(0), e.submit();
    }), $(".js-update-entry-quantity-input").on("blur", function(t) {
      ACC.cartitem.handleUpdateQuantity(this, t);
    }).on("keyup", function(t) {
      return ACC.cartitem.handleKeyEvent(this, t);
    }).on("keydown", function(t) {
      return ACC.cartitem.handleKeyEvent(this, t);
    }), $(document).on("click", ".js-qty-selector .js-cartPage-qty-selector-minus", function(t) {
      ACC.productDetail.checkQtySelector(this, "minus"), $(this).closest(".update_cart_form").length && (ACC.cartitem.changeQuantityForm($(this).closest("form")), 
      ACC.cartitem.handleUpdateQuantity(this, t));
    }), $(document).on("click", ".js-qty-selector .js-cartPage-qty-selector-plus", function(t) {
      ACC.productDetail.checkQtySelector(this, "plus"), $(this).closest(".update_cart_form").length && (ACC.cartitem.changeQuantityForm($(this).closest("form")), 
      ACC.cartitem.handleUpdateQuantity(this, t));
    });
  },
  handleKeyEvent: function(t, e) {
    return 13 !== e.which || ACC.cartitem.submitTriggered ? !ACC.cartitem.submitTriggered : (ACC.cartitem.submitTriggered = ACC.cartitem.handleUpdateQuantity(t, e), 
    !1);
  },
  handleUpdateQuantity: function(t, e) {
    var i = $(t).closest("form"), n = i.find("input[name=productCode]").val(), o = i.find("input[name=initialQuantity]").val(), s = i.find("input[name=quantity]").val();
    return o !== s && (ACC.track.trackUpdateCart(n, o, s), i.submit(), !0);
  },
  changeQuantityForm: function(t) {
    var e = $(t).find("input[name=pdpAddtoCartInput]").val();
    $(t).find("input:hidden[name=quantity]").val(e);
  }
};

var cboxOptions = {
  width: "95%",
  height: "95%",
  maxWidth: "960px",
  maxHeight: "960px"
};

$(".cbox-link").colorbox(cboxOptions), $(window).resize(function() {
  $("#colorbox").hasClass("variantSelectMobile") || $.colorbox.resize({
    width: window.innerWidth > parseInt(cboxOptions.maxWidth) ? cboxOptions.maxWidth : cboxOptions.width,
    height: window.innerHeight > parseInt(cboxOptions.maxHeight) ? cboxOptions.maxHeight : cboxOptions.height
  });
}), ACC.colorbox = {
  _autoload: [ "init" ],
  config: {
    maxWidth: "100%",
    opacity: .7,
    width: "auto",
    transition: "none",
    close: '<span class="glyphicon glyphicon-remove"></span>',
    title: '<div class="headline"><span class="headline-text">{title}</span></div>',
    onComplete: function() {
      $.colorbox.resize(), ACC.common.refreshScreenReaderBuffer();
    },
    onClosed: function() {
      ACC.common.refreshScreenReaderBuffer();
    }
  },
  init: function() {
    $(document).bind("cbox_complete", function() {
      ACC.colorbox.resize();
    });
  },
  open: function(t, e) {
    var i = $.extend({}, ACC.colorbox.config, e);
    return i.title = i.title.replace(/{title}/g, t), $.colorbox(i);
  },
  resize: function() {
    $.colorbox.resize();
  },
  close: function() {
    $.colorbox.close();
  }
}, ACC.content = {
  _autoload: [ "bindContent" ],
  id: "#underHeader",
  bindContent: function() {
    console.log("acc.content.js - ACC.content.bindContent() : executed"), $(window).on("resize", function() {
      ACC.content.resizeSerachDiv(), ACC.content.resizeContent();
    }).resize();
  },
  resizeContent: function() {
    console.log("acc.content.js - ACC.content.resizeContent() : executed"), $(ACC.content.id).css({
      height: window.innerHeight - $(".header").outerHeight() - $("#cookie-bar").outerHeight() + "px"
    });
  },
  resizeSerachDiv: function() {
    var t = $(".navigation--mobile-site-search"), e = $(t).find(".site-search");
    $(e).is(":visible") && $(t).css("height", e.outerHeight());
  }
}, ACC.device = {
  _autoload: [ "setDeviceType", "setOrientationType" ],
  setDeviceType: function() {
    console.log("ACC.device.setDeviceType()");
    var t = $(window).width(), e = screenMdMin.slice(0, -2), i = screenMdMin.slice(0, -2);
    ACC.device.type = t < e ? "mobile" : t < i ? "tablet" : "desktop";
  },
  setOrientationType: function() {
    void 0 !== screen.orientation ? ACC.device.orientation = screen.orientation.type : ACC.device.orientation = "landscape-primary";
  }
}, window.addEventListener("resize", function() {
  ACC.device.setDeviceType();
}), window.addEventListener("orientationchange", function() {
  ACC.device.setOrientationType(), ACC.navigation.mobileNavHeight();
}), ACC.global = {
  _autoload: [ [ "passwordStrength", $(".password-strength").length > 0 ], "bindToggleOffcanvas", "bindToggleXsSearch", "bindHoverIntentMainNavigation", "initImager", "backToHome", "bindSelect" ],
  passwordStrength: function() {
    $(".password-strength").pstrength({
      verdicts: [ ACC.pwdStrengthTooShortPwd, ACC.pwdStrengthVeryWeak, ACC.pwdStrengthWeak, ACC.pwdStrengthMedium, ACC.pwdStrengthStrong, ACC.pwdStrengthVeryStrong ],
      minCharText: ACC.pwdStrengthMinCharText
    });
  },
  bindToggleOffcanvas: function() {
    $(document).on("click", ".js-toggle-sm-navigation", function() {
      ACC.global.toggleClassState($("main"), "offcanvas"), ACC.global.toggleClassState($("html"), "offcanvas"), 
      ACC.global.toggleClassState($("body"), "offcanvas"), ACC.global.resetXsSearch(), 
      ACC.navigation.mobileNavHeight();
    });
  },
  bindToggleXsSearch: function() {
    $(document).on("click", ".js-toggle-xs-search", function() {
      ACC.global.toggleClassState($(".site-search"), "active"), ACC.global.toggleClassState($(".js-mainHeader .navigation--middle"), "search-open");
    });
  },
  resetXsSearch: function() {
    $(".site-search").removeClass("active"), $(".js-mainHeader .navigation--middle").removeClass("search-open");
  },
  toggleClassState: function(t, e) {
    return t.hasClass(e) ? t.removeClass(e) : t.addClass(e), t.hasClass(e);
  },
  bindHoverIntentMainNavigation: function() {
    enquire.register("screen and (min-width:" + screenMdMin + ")", {
      match: function() {
        $(".js-enquire-has-sub").hoverIntent(function() {
          var t = $(this), e = (t.width(), t.find(".js_sub__navigation")), i = (e.outerWidth(), 
          $(".js_navigation--bottom")), n = (i.width(), i.outerHeight());
          e.css({
            right: 0,
            top: n,
            left: "auto"
          }), t.addClass("show-sub");
        }, function() {
          $(this).removeClass("show-sub");
        });
      },
      unmatch: function() {
        $(".js_sub__navigation").removeAttr("style"), $(".js-enquire-has-sub").hoverIntent(function() {});
      }
    });
  },
  initImager: function(t) {
    t = t || ".js-responsive-image", this.imgr = new Imager(t);
  },
  reprocessImages: function(t) {
    t = t || ".js-responsive-image", void 0 === this.imgr ? this.initImager(t) : this.imgr.checkImagesNeedReplacing($(t));
  },
  addGoogleMapsApi: function(t) {
    void 0 !== t && 0 === $(".js-googleMapsApi").length ? $("head").append('<script class="js-googleMapsApi" type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=' + ACC.config.googleApiKey + "&callback=" + t + '"><\/script>') : void 0 !== t && setTimeout(t + "()", 0);
  },
  backToHome: function() {
    $(".backToHome").on("click", function() {
      var t = ACC.config.contextPath;
      window.location = t;
    });
  },
  bindSelect: function() {
    $("select").each(function() {
      $(this).find("option").length > 1 && "" === $(this).find(":selected").val() && $(this).css("color", "gray");
    }), $("select").change(function() {
      "" !== $(this).val() ? $(this).css("color", "black") : $(this).css("color", "gray");
    });
  }
}, ACC.langselector = {
  _autoload: [ "bindLanguageSwitcher", "bindLanguageDisplay", "bindLanguageHide" ],
  bindLanguageSwitcher: function() {
    console.log("acc.language-selector.js - ACC.langselector.bindLanguageSelector() : executed"), 
    $(document).on("click", ".js-lang-switch", function() {
      console.log("acc.language-selector.js - ACC.langselector.bindLanguageSelector() - click on : .js-lang-switch");
      var t = $(this), e = t.parents("form");
      e.find("#langcode").val(t.data("lang-to-switch")), console.log("acc.language-selector.js - ACC.langselector.bindLanguageSelector() - language switched to : " + t.data("lang-to-switch")), 
      e.submit();
    });
  },
  bindLanguageDisplay: function() {
    console.log("acc.language-selector.js - ACC.langselector.bindLanguageDisplay() : executed"), 
    $(document).on("click", ".language-switcher:not(.open)", function(t) {
      console.log("acc.language-selector.js - ACC.langselector.bindLanguageDisplay() - click on : .language-switcher:not(.open)"), 
      t.stopPropagation(), $(this).find(".items-wrapper").removeClass("hide"), $(this).addClass("open");
    });
  },
  bindLanguageHide: function() {
    console.log("acc.language-selector.js - ACC.langselector.bindLanguageDisplay() : executed"), 
    $(".language-switcher.open").on("mouseleave", function() {
      console.log("acc.language-selector.js - ACC.langselector.bindLanguageDisplay() - mouseleave on : .language-switcher.open"), 
      $(this).find(".items-wrapper").addClass("hide");
    }), $(window).on("click", function() {
      $(".language-switcher.open .items-wrapper").addClass("hide"), $(".language-switcher.open").removeClass("open");
    });
  }
}, ACC.minicart = {
  _autoload: [ "bindMiniCart" ],
  id: "#cart",
  class: "cart",
  classActive: "active",
  classLoader: "cart--loader",
  classTriggerToggle: "js-mini-cart-link",
  classWrapperContent: "cart--content",
  classWrapperItemList: "cart--item-list",
  opened: !1,
  active: !1,
  bindMiniCart: function() {
    console.log("acc.minicart.js - ACC.minicart.bindMiniCart() : executed"), $(document).on("click", "." + ACC.minicart.classTriggerToggle, function(t) {
      console.log("acc.minicart.js - ACC.minicart.bindMiniCart() - click on : ." + ACC.minicart.classTriggerToggle), 
      t.preventDefault(), "desktop" === ACC.device.type ? ACC.minicart.toggleMiniCart() : window.location = $(this).data("mini-cart-url-checkout");
    }), $(document).on("click", ".js-qty-selector .js-cart-qty-selector-minus", function(t) {
      console.log("acc.minicart.js - ACC.minicart.bindMiniCart() - click on : " + ACC.minicart.id + " .js-qty-selector .js-cart-qty-selector-minus"), 
      t.preventDefault();
      var e = $(this).parents(".js-qty-selector").find(".js-qty-selector-input"), i = e.val();
      ACC.productDetail.mainCheckQtySelector(this, "minus", ".js-cart-qty-selector");
      var n = e.val();
      ACC.minicart.updateEntry(this, i, n);
    }), $(document).on("click", ".js-cart-qty-selector-plus", function(t) {
      console.log("acc.minicart.js - ACC.minicart.bindMiniCart() - click on : " + ACC.minicart.id + " .js-cart-qty-selector-plus"), 
      t.preventDefault();
      var e = $(this).parents(".js-qty-selector").find(".js-qty-selector-input"), i = e.val();
      console.log("inputQty  : "), console.log(e), console.log("lastQtyVal " + i), ACC.productDetail.mainCheckQtySelector(this, "plus", ".js-cart-qty-selector");
      var n = e.val();
      console.log("newQtyVal " + n), ACC.minicart.updateEntry(this, i, n);
    }), $(document).on("click", ".remove-mini-cart-entry-button", function(t) {
      console.log("acc.minicart.js - ACC.minicart.bindMiniCart() - click on : .remove-mini-cart-entry-button");
      var e = $(this).data("productcode"), i = $(this).data("quantity"), n = $(this).data("productname"), o = $(this).data("productcategories"), s = $(this).data("productbrand"), a = $(this).data("productprice");
      t.preventDefault(), console.log("REMOVE"), ACC.minicart.updateEntry(this, 0, 0), 
      ACC.track.trackRemoveFromCart(e, i, n, o, s, a);
    }), $(document).on("click", ".js-mini-cart-close-button", function(t) {
      console.log("acc.minicart.js - ACC.minicart.bindMiniCart() - click on : .js-mini-cart-close-button"), 
      t.preventDefault(), ACC.colorbox.close(), ACC.minicart.hideCart();
    }), $(document).on("click", ".js-resize", function(t) {
      ACC.minicart.active && ACC.minicart.resizeRuler();
    }), $(document).on("click", function(t) {
      console.log("acc.minicart.js - ACC.minicart.bindMiniCart() - click on : document"), 
      console.log(t.target.id), console.log($(t.target).parents(".cart")), "cart" === t.target.id || $(t.target).parents("#cart").length || ACC.minicart.hideCart();
    }), window.addEventListener("resize", function() {
      ACC.minicart.opened && ACC.minicart.hideCart(), ACC.minicart.resizeRuler();
    });
  },
  getContent: function() {
    console.log("acc.minicart.js - ACC.minicart.getContent() : executed");
    var t = $("." + ACC.minicart.classTriggerToggle).data("miniCartUrl"), e = $(ACC.minicart.id + " ." + ACC.minicart.classWrapperContent);
    $.ajax({
      url: t,
      cache: !1,
      type: "GET",
      success: function(t) {
        console.log("acc.minicart.js - ACC.minicart.getContent() - ajax call : success"), 
        $("." + ACC.minicart.classLoader).fadeOut("slow", function() {
          $(this).removeClass(ACC.minicart.classActive), e.html(t), e.show(), ACC.minicart.showCart(), 
          ACC.minicart.opened = !0, ACC.minicart.active = !0, $("." + ACC.minicart.classWrapperContent).fadeIn("fast");
        });
      }
    });
  },
  hideCart: function() {
    console.log("acc.minicart.js - ACC.minicart.hideCart() : executed"), ACC.minicart.active && $("." + ACC.minicart.classWrapperContent).fadeOut("fast", function() {
      $(ACC.minicart.id).removeClass(ACC.minicart.classActive), ACC.minicart.active = !1, 
      $("#underHeader").removeClass("underHeader_overlay");
    });
  },
  initMiniCart: function(t) {
    console.log("acc.minicart.js - ACC.minicart.initMiniCart() : executed"), !0 === ACC.minicart.opened ? $("." + ACC.minicart.classWrapperContent).fadeOut("fast", function() {
      $("." + ACC.minicart.classLoader).fadeIn("fast"), ACC.minicart.getContent();
    }) : ACC.minicart.getContent();
  },
  resize: function(t, e) {
    console.log("acc.minicart.js - ACC.minicart.resize() : executed");
    var i = $(ACC.minicart.id), n = $(ACC.minicart.id + " ." + ACC.minicart.classWrapperItemList), o = void 0 !== t && !0 === t, s = void 0 !== e && !0 === e;
    i.css({
      opacity: "0",
      visibility: "visible",
      height: "",
      top: ""
    }), n.css({
      opacity: "0",
      visibility: "visible",
      height: ""
    }), o && (console.log("acc.minicart.js - ACC.minicart.resize() : mustResize"), i.animate({
      opacity: "1",
      height: window.innerHeight - $(".header").outerHeight() - $("#cookie-bar").outerHeight() + "px"
    }, 0, function() {
      console.log("acc.minicart.js - ACC.minicart.resize() : " + i.outerHeight() - $(".cart--header").outerHeight() - $(".cart--footer").outerHeight() + "px"), 
      n.css({
        opacity: "1",
        height: i.outerHeight() - $(".cart--header").outerHeight() - $(".cart--footer").outerHeight() + "px"
      });
    })), s && (i.css({
      opacity: "1",
      top: $(".header").outerHeight() + $("#cookie-bar").outerHeight() + "px"
    }), n.css({
      opacity: "1"
    }));
  },
  resizeRuler: function() {
    console.log("acc.minicart.js - ACC.minicart.resizeRuler() : executed"), "desktop" === ACC.device.type && (console.log("acc.minicart.js - ACC.minicart.resizeRuler() - desktop"), 
    ACC.minicart.resize(!0, !0)), "mobile" === ACC.device.type && (console.log("acc.minicart.js - ACC.minicart.resizeRuler() - mobile"), 
    ACC.minicart.resize(!1, !0));
  },
  showCart: function() {
    console.log("acc.minicart.js - ACC.minicart.showCart() : executed");
    var t = $(ACC.minicart.id);
    ACC.minicart.resizeRuler(), t.addClass(ACC.minicart.classActive), $("#underHeader").addClass("underHeader_overlay");
  },
  toggleMiniCart: function(t) {
    console.log("acc.minicart.js - ACC.minicart.toggleMiniCart() : executed"), ACC.minicart.active ? ACC.minicart.hideCart() : ACC.minicart.opened ? ACC.minicart.updateContent(t) : ACC.minicart.initMiniCart(t);
  },
  toggleContentScroll: function() {},
  updateContent: function() {
    $("." + ACC.minicart.classTriggerToggle).data("miniCartUrl");
    $(ACC.minicart.id + " ." + ACC.minicart.classWrapperContent).fadeOut("fast", function() {
      ACC.minicart.showCart(), $("." + ACC.minicart.classLoader).fadeIn("fast", function() {
        ACC.minicart.getContent();
      });
    });
  },
  updateEntry: function(t, e, i) {
    console.log("acc.minicart.js - ACC.minicart.updateEntry() : executed");
    var n, o = $(t).data("miniCartUrl"), s = $(t).data("productcode"), a = $(t).data("entrynumber"), r = o + "?productCode=" + s + "&entryNumber=" + a + "&quantity=" + i, l = $("#cart div.qty[data-productCode=" + s + "]"), c = $(l).data("unit"), d = $(t).data("hasWarranty");
    n && n.abort(), n = $.ajax({
      url: r,
      cache: !1,
      type: "GET"
    }).done(function(t) {
      console.log("//==== SUCCESS updateEntry ===//"), console.log(t), console.log("//============================//"), 
      ACC.minicart.updateMiniCartDisplayResult(t), ACC.minicart.updatePriceForEntry(a, parseInt(i), t), 
      $(".js-number-items-popin").children("span").html(t.miniCartCount), $(".js-total-price-popin").html(t.miniCartPrice), 
      0 === i || t.alertMessage || d ? ACC.minicart.initMiniCart(t.alertMessage) : ($(".alert-message").html(""), 
      void 0 !== c ? l.text(i + c) : l.text(i));
    }).always(function() {
      n = void 0;
    });
  },
  updateMiniCartDisplay: function() {
    console.log("acc.minicart.js - ACC.minicart.updateMiniCartDisplay() : executed");
    var t = $("." + ACC.minicart.classTriggerToggle).data("miniCartRefreshUrl");
    $.ajax({
      url: t,
      cache: !1,
      dataType: "json",
      type: "GET",
      success: function(t) {
        console.log("//==== COMPLETE updateMiniCartDisplay ===//"), console.log(t), console.log("//============================//"), 
        ACC.minicart.updateMiniCartDisplayResult(t);
      },
      error: function(t) {
        console.log("//==== ERROR updateMiniCartDisplay ===//"), console.log(t), console.log("//============================//");
      }
    });
  },
  updateMiniCartDisplayResult: function(t) {
    console.log("acc.minicart.js - ACC.minicart.updateMiniCartDisplayResult() : executed"), 
    ACC.minicart.updateMinicartCounter(t.miniCartCount), ACC.minicart.updateMinicartTotal(t.totalPrice, t.totalDiscounts, t.subTotal), 
    $(document).find(" .js-mini-cart-price .price--value").html(t.totalPrice);
  },
  updateMinicartCounter: function(t) {
    console.log("acc.minicart.js - ACC.minicart.updateMinicartCounter() : executed"), 
    $(document).find(".js-mini-cart-qty").html(t);
  },
  updateMinicartTotal: function(t, e, i) {
    console.log("acc.minicart.js - ACC.minicart.updateMinicartTotal() : executed"), 
    $(document).find('.js-mini-cart-price span[class^="price--total"]').html(t), $(document).find(".cart--totals-savings-amount .price--base").html(e), 
    $(document).find(".cart--totals-subTotal-amount .price--base").html(i);
  },
  updatePriceForEntry: function(t, e, i) {
    var n = $(".entry" + t), o = parseFloat(n.attr("data-productprice")), s = (e * o).toFixed(2);
    n.find(".price--total").html((e * o).toFixed(2)), ACC.minicart.updateMessageReached(s, i);
  },
  updateMessageReached: function(t, e) {
    var i = document.getElementById("threshold").value, n = e.totalPrice;
    if (n >= i) $("#cart--warning").hide(), $(".mini-cart-checkout-button").removeClass("disabled"), 
    $("#cart--warning--hiden").addClass("hidden"); else {
      var o = i - n;
      null !== document.getElementById("cart--warning--hiden") && n < i ? ($("#cart--warning--hiden").removeClass("hidden"), 
      $(document).find(".cart--warning .cart--warning-value").html(o.toFixed(2)), $(".mini-cart-checkout-button").addClass("disabled")) : ($(document).find(".cart--warning .cart--warning-value").html(o.toFixed(2)), 
      $(".mini-cart-checkout-button").addClass("disabled"), $("#cart--warning").show());
    }
  },
  log: function(t) {
    void 0 === t && (t = "unknwon"), console.log("========== start minicart log : " + t + " =========="), 
    console.log("ACC.minicart.active", ACC.minicart.active), console.log("ACC.minicart.opened", ACC.minicart.opened), 
    console.log("========== finish minicart log : " + t + " ==========");
  }
};

var oDoc = document;

ACC.navigation = {
  _autoload: [ "bindShowNavDesktop", "bindDrodownSubcat", "offcanvasNavigation", "myAccountNavigation", "orderToolsNavigation" ],
  bindShowNavDesktop: function() {
    $(document).on("click", ".shopCategory", function(t) {
      $("#topNavigation").show(), $(".js-secondaryNavAccount").hasClass("in") && $(".js-myAccount-toggle").click();
    }), $("#topNavigation").on("mouseleave", function() {
      $(this).hide();
    });
  },
  bindDrodownSubcat: function() {
    console.log("ACC.navigation.js - ACC.navigation.bindDrodownSubcat() : executed"), 
    $(".js-dropdown-subcat").on("click", function(t) {
      t.preventDefault(), console.log("ACC.navigation.js - ACC.navigation.bindDrodownSubcat() - Click on $('.js-dropdown-subcat')");
      var e = $(this);
      e.hasClass("open") ? (e.removeClass("open"), $(e.data("target")).hide().removeClass("active")) : ($(".js-dropdown-subcat").removeClass("open"), 
      e.addClass("open"), $(e.data("target")).removeClass("active"), e.parent().find(e.data("target")).addClass("active"), 
      $(e.data("target")).not(".active").each(function() {
        $(this).hide();
      }).promise().done(function() {
        e.parent().find(e.data("target")).show();
      }));
    });
  },
  offcanvasNavigation: function() {
    console.log("ACC.navigation.js - ACC.navigation.offcanvasNavigation() : executed"), 
    enquire.register("screen and (max-width:" + screenSmMax + ")", {
      match: function() {
        $("body").on("click", ".js-enquire-offcanvas-navigation .js-enquire-has-sub .js_nav__link--drill__down", function(t) {
          console.log("ACC.navigation.js - ACC.navigation.offcanvasNavigation() - mobile mode - click on : .js-enquire-offcanvas-navigation .js-enquire-has-sub .js_nav__link--drill__down", $(this)), 
          t.preventDefault(), $(".js-userAccount-Links").hide(), $(".js-enquire-offcanvas-navigation ul.js-offcanvas-links").addClass("active"), 
          $(".js-enquire-offcanvas-navigation .js-enquire-has-sub").removeClass("active"), 
          $(this).parent(".js-enquire-has-sub").addClass("active"), $(this).parent().find(".sm-back .sm-back--content").html($(this).parent().find(".nav__link a").html());
        }), $("body").on("click", ".js-enquire-offcanvas-navigation .js-enquire-sub-close", function(t) {
          console.log("ACC.navigation.js - ACC.navigation.offcanvasNavigation() - mobile mode - click on : .js-enquire-offcanvas-navigation .js-enquire-sub-close"), 
          t.preventDefault(), $(".js-userAccount-Links").show(), $(".js-enquire-offcanvas-navigation ul.js-offcanvas-links").removeClass("active"), 
          $(".js-enquire-offcanvas-navigation .js-enquire-has-sub").removeClass("active"), 
          $(".js-dropdown-subcat .active").removeClass("active").css("display", "none");
        });
      },
      unmatch: function() {
        $(".js-userAccount-Links").show(), $(".js-enquire-offcanvas-navigation ul.js-offcanvas-links").removeClass("active"), 
        $(".js-enquire-offcanvas-navigation .js-enquire-has-sub").removeClass("active"), 
        $("body").off("click", ".js-enquire-offcanvas-navigation .js-enquire-has-sub > a"), 
        $("body").off("click", ".js-enquire-offcanvas-navigation .js-enquire-sub-close");
      }
    });
  },
  mobileNavHeight: function() {
    if (console.log("ACC.navigation.js - ACC.navigation.mobileNavHeight() : executed"), 
    "mobile" === ACC.device.type) {
      var t = $(".hidden-md.hidden-lg").find(".sticky-nav-top").height(), e = $("#cookie-bar").height(), i = $(window).height() - t - e;
      $(".navigation__overflow").css({
        height: i + "px"
      });
    }
  },
  myAccountNavigation: function() {
    $(".js-mobile-logo").html($(".js-site-logo a").clone()), $(".nav-form").html($('<span class="icon icon--list-alt"></span>'));
    var t = [], e = "", i = $(".accNavComponent"), n = $(".js-secondaryNavAccount > ul");
    $(".navigation--bottom > ul.nav__links.nav__links--products");
    if (i) for (var o = i.find("a"), s = 0; s < o.length; s++) t.push({
      link: o[s].href,
      text: o[s].title,
      className: o[s].className
    }), console.log("text : ", o[s].href), console.log("link : ", o[s].title), console.log("classList : ", o[s].classList), 
    console.log("--------------");
    var a = $(".mobile-lang-switcher")[0].outerHTML, r = $(".nav__right ul li.logged_in"), l = "";
    if (l += '<div class="close-nav">', l += '<button type="button" class="js-toggle-sm-navigation btn"><span class="icon icon--close-white"></span></button>', 
    l += "</div>", $(".liOffcanvas a") && $(".liOffcanvas a").length > 0 && (e += '<li class="auto liUserSign" ><div class="userGroup myAcctUserIcon"><a class="userSign" href="' + $(".liOffcanvas a")[0].href + '"><span class="icon icon--user-white"></span>' + $(".liOffcanvas a")[0].innerHTML + "</a></div>", 
    0 === r.length && (e += a), e += "</li>"), r && 1 === r.length) {
      var c = $(".navigation-menu--bottom .sticky-nav-top").html();
      c += '<li class="auto ">', c += '<div class="userGroup">', c += '<span class="icon icon--user-white myAcctUserIcon"></span>', 
      c += '<div class="userName">' + r[0].innerHTML + "</div>", t.length > 0 && (c += '<a class="collapsed js-nav-collapse" id="signedInUserOptionsToggle" data-toggle="collapse"  data-target=".offcanvasGroup1">', 
      c += '<span class="icon icon--minus-white myAcctExp"></span>', c += "</a>"), c += "</div>", 
      c += a, c += l, $(".js-sticky-user-group").html(c), $(".js-userAccount-Links").append(e), 
      $(".js-userAccount-Links").append($('<li class="auto"><div class="myAccountLinksContainer js-myAccountLinksContainer"></div></li>'));
      var d = $('<a class="myAccountLinksHeader collapsed js-myAccount-toggle" data-toggle="collapse" data-parent=".nav__right" href="#accNavComponentDesktopOne">' + i.data("title") + "</a>");
      d.insertBefore(i), (d = []).push('<div class="sub-nav">'), d.push('<a id="signedInUserAccountToggle" class="myAccountLinksHeader collapsed js-myAccount-toggle" data-toggle="collapse" data-target=".offcanvasGroup2">'), 
      d.push(i.data("title")), d.push('<span class="icon icon--plus myAcctExp"></span>'), 
      d.push("</a>"), d.push("</div>"), $(".js-myAccountLinksContainer").append(d.join("")), 
      $(".js-myAccountLinksContainer").append($('<ul data-trigger="#signedInUserAccountToggle" class="offcanvasGroup2 offcanvasNoBorder collapse js-nav-collapse-body subNavList js-myAccount-root sub-nav"></ul>'));
      for (var u = t.length - 1; u >= 0; u--) {
        var h = oDoc.createElement("a");
        h.title = t[u].text, h.href = t[u].link, h.innerHTML = t[u].text;
        var p = oDoc.createElement("li");
        p.appendChild(h), (p = $(p)).addClass("auto"), $(".js-myAccount-root").append(p);
      }
    } else {
      var f = e.substring(0, e.length - 5) + l + "</li>";
      $(".js-sticky-user-group").html(f);
    }
    for (var m = t.length - 1; m >= 0; m--) {
      var g = oDoc.createElement("a");
      g.title = t[m].text, g.href = t[m].link, g.className = t[m].className, g.innerHTML = t[m].text;
      var v = oDoc.createElement("li");
      v.appendChild(g), (v = $(v)).addClass("auto"), n.get(0).appendChild(v.get(0));
    }
    $(".js-secondaryNavAccount").on("shown.bs.collapse", function() {
      $(".js-secondaryNavCompany").hasClass("in") && $(".js-myCompany-toggle").click();
    }), $(".js-secondaryNavCompany").on("shown.bs.collapse", function() {
      $(".js-secondaryNavAccount").hasClass("in") && $(".js-myAccount-toggle").click();
    }), $(".js-nav-collapse-body").on("hidden.bs.collapse", function(t) {
      var e = $(t.target), i = e.attr("data-trigger") + " > span";
      e.hasClass("in") ? $(i).removeClass("icon--plus").addClass("icon--minus") : $(i).removeClass("icon--minus").addClass("icon--plus");
    }), $(".js-nav-collapse-body").on("show.bs.collapse", function(t) {
      var e = $(t.target), i = e.attr("data-trigger") + " > span";
      e.hasClass("in") ? $(i).removeClass("icon--minus").addClass("icon--plus") : $(i).removeClass("icon--plus").addClass("icon--minus");
    });
  },
  orderToolsNavigation: function() {
    $(".js-nav-order-tools").on("click", function(t) {
      $(this).toggleClass("js-nav-order-tools--active");
    });
  }
}, ACC.paginationsort = {
  downUpKeysPressed: !1,
  bindAll: function() {
    this.bindPaginaSort();
  },
  bindPaginaSort: function() {
    ACC.paginationsort.bindSortForm($("#sort_form1")), ACC.paginationsort.bindSortForm($("#sort_form2")), 
    ACC.paginationsort.bindSortForm($("#sortForm1")), ACC.paginationsort.bindSortForm($("#sortForm2"));
  },
  bindSortForm: function(t) {
    t.change(function() {
      ACC.paginationsort.downUpPressed || this.submit(), ACC.paginationsort.downUpPressed = !1;
    });
  },
  sortFormIEFix: function(t, e) {
    t.keydown(function(t) {
      38 === t.keyCode || 40 === t.keyCode ? ACC.paginationsort.downUpPressed = !0 : 13 === t.keyCode && e !== $(this).val() ? $(this).parent().submit() : ACC.paginationsort.downUpPressed = !1;
    });
  }
}, $(document).ready(function() {
  ACC.paginationsort.bindAll();
}), ACC.product = {
  _autoload: [ "bindToAddToCartForm", "enableStorePickupButton", "enableVariantSelectors", "bindFacets" ],
  addToCart: function(t) {
    console.log("acc.product.js - ACC.product.addToCart() : executed");
    var e = t, i = e.attr("action"), n = e.find("[name=productCodePost]").val(), o = e.find("#qty").val();
    console.log("url : ", i), console.log("productCode : ", n), console.log("quantityField : ", o), 
    void 0 !== n && void 0 !== o && $.ajax({
      type: "POST",
      url: i,
      data: e.serialize(),
      dataType: "json",
      success: function(e, i, n, o) {
        console.log("%c acc.product.js - ACC.product.addToCart() - ajaxForm : SUCCES", "background-color:green; color:white; font-weight:bold;"), 
        console.log("//==== COMPLETE addToCart ===//"), console.log(e), console.log(i), 
        console.log(n), console.log(o), console.log("//============================//"), 
        "function" == typeof ACC.minicart.updateMiniCartDisplay && ACC.minicart.updateMiniCartDisplay(), 
        ACC.product.displayGreenTick(t);
      },
      error: function(t) {
        console.log(t), console.log("%c acc.product.js - ACC.product.addToCart() - ajaxForm : ERROR", "background-color:red; color:white; font-weight:bold;");
      }
    });
  },
  displayGreenTick: function(t) {
    var e = $(t).find(".js-enable-btn");
    $(e).children().hide(), 1 === $(e).children().length ? ($(e).addClass("add-button-product-toCart-hidden"), 
    $(e).css({
      background: "url(/_ui/responsive/theme-sharjah/images/svg/check-white.svg) no-repeat 0 0",
      "background-color": "#32CD32",
      "background-position-x": "center",
      "background-position-y": "center"
    })) : $(e).css({
      background: "url(/_ui/responsive/theme-sharjah/images/svg/check-white.svg) no-repeat 0 0",
      "background-color": "#32CD32",
      "background-position-x": "center",
      "background-position-y": "center",
      height: "100%",
      width: "100%",
      "border-color": "#32CD32"
    }), setTimeout(function() {
      $(e).removeAttr("style"), $(e).children().show(), $(e).removeClass("add-button-product-toCart-hidden");
    }, 2e3);
  },
  bindFacets: function() {
    console.log("acc.product.js - ACC.product.bindFacets() : executed"), "mobile" === ACC.device.type && ($(document).on("click", ".js-show-facets", function(t) {
      t.preventDefault();
      var e = $(this).data("selectRefinementsTitle");
      ACC.colorbox.open(e, {
        href: ".js-product-facet",
        inline: !0,
        height: window.innerHeight - $(".navigation--mobile-navbar").outerHeight() + $("#cookie-bar").outerHeight() + "px",
        scrolling: !0,
        top: $(".navigation--mobile-navbar").outerHeight() + $("#cookie-bar").outerHeight() + "px",
        width: "100%",
        onComplete: function() {
          $("#cboxTitle").hide(), $("#cboxLoadedContent").css({
            "margin-top": "0px",
            padding: "0px",
            overflow: "scroll",
            width: "100%"
          }), $(document).find(".js-product-facet .js-facet").removeClass("active)"), $(document).on("click", ".js-product-facet .js-facet-name", function(t) {
            console.log("acc.product.js - ACC.product.bindFacets() - click on : document .js-product-facet .js-facet-name"), 
            t.preventDefault(), $(this).parents(".js-facet").hasClass("active") ? $(this).parents(".js-facet").removeClass("active") : ($(".js-product-facet .js-facet").removeClass("active"), 
            $(this).parents(".js-facet").addClass("active"));
          });
        },
        onClosed: function() {
          $(document).off("click", ".js-product-facet .js-facet-name"), $("#cboxLoadedContent").attr("style", "");
        }
      });
    }), $("#cboxClose").click());
  },
  bindToAddToCartForm: function() {
    console.log("acc.product.js - ACC.product.bindToAddToCartForm() : executed");
    var t = [ ".add-button-toCart", ".add-button-product-toCart" ];
    $(".add_to_cart_form").on("click", t.join(", "), function(t) {
      console.log("acc.product.js - ACC.product.bindToAddToCartForm() - submit on : .add_to_cart_form"), 
      t.preventDefault(), ACC.product.addToCart($(this).parents(".add_to_cart_form"));
    });
  },
  bindToAddToCartStorePickUpForm: function() {
    console.log("acc.product.js - ACC.product.bindToAddToCartStorePickUpForm() : executed"), 
    $("#colorbox #add_to_cart_storepickup_form").ajaxForm({
      success: ACC.product.displayAddToCartPopup
    });
  },
  displayAddToCartPopup: function(t, e, i, n) {
    console.log("acc.product.js - ACC.product.bindToAddToCartStorePickUpForm() : executed"), 
    console.log("//==== COMPLETE displayAddToCartPopup ===//"), console.log(t), console.log(e), 
    console.log(i), console.log(n), console.log("//============================//"), 
    $ajaxCallEvent = !0, $("#addToCartLayer").remove(), "function" == typeof ACC.minicart.updateMiniCartDisplay && ACC.minicart.updateMiniCartDisplay();
    var o = $("[name=productCodePost]", n).val(), s = $("[name=qty]", n).val(), a = 1;
    void 0 !== s && (a = s);
    var r = t.cartAnalyticsData, l = {
      cartCode: r.cartCode,
      productCode: o,
      quantity: a,
      productPrice: r.productPostPrice,
      productName: r.productName
    };
    ACC.track.trackAddToCart(o, a, l);
  },
  enableAddToCartButton: function() {
    console.log("acc.product.js - ACC.product.enableAddToCartButton() : executed"), 
    $(".js-enable-btn").each(function() {
      $(this).hasClass("outOfStock") || $(this).hasClass("out-of-stock") || $(this).removeAttr("disabled");
    });
  },
  enableStorePickupButton: function() {
    console.log("acc.product.js - ACC.product.bindToAddToCartStorePickUpForm() : executed"), 
    $(".js-pickup-in-store-button").removeAttr("disabled");
  },
  enableVariantSelectors: function() {
    console.log("acc.product.js - ACC.product.enableVariantSelectors() : executed"), 
    $(".variant-select").removeAttr("disabled");
  },
  showRequest: function(t, e, i) {
    return console.log("acc.product.js - ACC.product.showRequest() : executed"), !!$ajaxCallEvent && ($ajaxCallEvent = !1, 
    !0);
  }
}, $(document).ready(function() {
  $ajaxCallEvent = !0, ACC.product.enableAddToCartButton();
}), $(document).on("click", ".showAll", function(t) {
  var e = $(this).data("showcode"), i = window.location.href;
  document.location.href = updateQueryStringParameter(i, e);
}), $(document).on("click", ".showNoAll", function(t) {
  var e = $(this).data("showcode"), i = window.location.href;
  document.location.href = updateQueryStringParameter(i, e);
}), ACC.productDetail = {
  _autoload: [ "initPageEvents", "bindVariantOptions" ],
  checkQtySelector: function(t, e) {
    console.log("acc.productDetail.js - ACC.productDetail.checkQtySelector() : executed"), 
    ACC.productDetail.mainCheckQtySelector(t, e, ".js-qty-selector");
  },
  mainCheckQtySelector: function(t, e, i) {
    console.log("acc.productDetail.js - ACC.productDetail.mainCheckQtySelector() : executed");
    var n = $(t).parents(".js-qty-selector"), o = n.find(".js-qty-selector-input"), s = parseInt(o.val()), a = o.data("max"), r = n.find(i + "-minus"), l = n.find(i + "-plus");
    n.find(".qty-selector-btn").removeAttr("disabled"), "minus" === e ? 1 !== s ? (ACC.productDetail.updateQtyValue(t, s - 1), 
    s - 1 == 1 && r.attr("disabled", "disabled")) : r.attr("disabled", "disabled") : "reset" === e ? ACC.productDetail.updateQtyValue(t, 1) : "plus" === e ? "FORCE_IN_STOCK" === a ? ACC.productDetail.updateQtyValue(t, s + 1) : s < a ? (ACC.productDetail.updateQtyValue(t, s + 1), 
    s + 1 === a && l.attr("disabled", "disabled")) : l.attr("disabled", "disabled") : "input" === e ? 1 === s ? r.attr("disabled", "disabled") : "FORCE_IN_STOCK" === a && s > 0 ? ACC.productDetail.updateQtyValue(t, s) : s === a ? l.attr("disabled", "disabled") : s < 1 ? (ACC.productDetail.updateQtyValue(t, 1), 
    r.attr("disabled", "disabled")) : s > a && (ACC.productDetail.updateQtyValue(t, a), 
    l.attr("disabled", "disabled")) : "focusout" === e && (isNaN(s) ? (ACC.productDetail.updateQtyValue(t, 1), 
    r.attr("disabled", "disabled")) : s >= a && l.attr("disabled", "disabled"));
  },
  updateQtyValue: function(t, e) {
    console.log("acc.productDetail.js - ACC.productDetail.updateQtyValue() : executed");
    var i = $(t).parents(".js-qty-selector").find(".js-qty-selector-input"), n = $(t).parents(".add-to-cart").find("#addToCartForm"), o = $(t).parents(".bottom").find("#addToCartForm" + i.data("product")), s = (void 0 === n.attr("id") ? o : n).find(".js-qty-selector-input"), a = $(t).parents("form").find("#qty");
    i.val(e).triggerHandler("change"), s.val(e), a.val(e);
  },
  initPageEvents: function() {
    console.log("acc.productDetail.js - ACC.productDetail.initPageEvents() : executed"), 
    $(document).on("click", ".js-qty-selector .js-qty-selector-minus", function(t) {
      ACC.productDetail.checkQtySelector(this, "minus"), $(this).closest(".update_cart_form").length && ACC.cartitem.handleUpdateQuantity(this, t);
    }), $(document).on("click", ".js-qty-selector .js-qty-selector-plus", function(t) {
      ACC.productDetail.checkQtySelector(this, "plus"), $(this).closest(".update_cart_form").length && ACC.cartitem.handleUpdateQuantity(this, t);
    }), $(document).on("keydown", ".js-qty-selector .js-qty-selector-input", function(t) {
      " " !== $(this).val() && (t.which >= 48 && t.which <= 57 || t.which >= 96 && t.which <= 105) || 8 === t.which || 46 === t.which || 37 === t.which || 39 === t.which || 9 === t.which || (38 === t.which ? (ACC.cartitem.changeQuantityForm($(this).closest("form")), 
      ACC.productDetail.checkQtySelector(this, "plus")) : 40 === t.which ? (ACC.cartitem.changeQuantityForm($(this).closest("form")), 
      ACC.productDetail.checkQtySelector(this, "minus")) : t.preventDefault());
    }), $(document).on("keyup", ".js-qty-selector .js-qty-selector-input", function(t) {
      ACC.productDetail.checkQtySelector(this, "input"), ACC.cartitem.changeQuantityForm($(this).closest("form")), 
      ACC.productDetail.updateQtyValue(this, $(this).val());
    }), $(document).on("focusout", ".js-qty-selector .js-qty-selector-input", function(t) {
      ACC.productDetail.checkQtySelector(this, "focusout"), ACC.cartitem.changeQuantityForm($(this).closest("form")), 
      ACC.productDetail.updateQtyValue(this, $(this).val());
    }), $("#Size").change(function() {
      ACC.productDetail.changeOnVariantOptionSelection($("#Size option:selected"));
    }), $("#variant").change(function() {
      ACC.productDetail.changeOnVariantOptionSelection($("#variant option:selected"));
    }), $(".selectPriority").change(function() {
      window.location.href = $(this[this.selectedIndex]).val();
    }), $(document).on("click", ".js-add-to-cart-action", function(t) {
      console.log("add to cart form with conf submit");
      var e = $("#productConfOption").val();
      $('input[name="productConfOption "]').val(e), $("#addToCartForm").submit();
    });
  },
  changeOnVariantOptionSelection: function(t) {
    console.log("acc.productDetail.js - ACC.productDetail.changeOnVariantOptionSelection() : executed"), 
    window.location.href = t.attr("value");
  },
  bindVariantOptions: function() {
    console.log("acc.productDetail.js - ACC.productDetail.bindVariantOptions() : executed"), 
    ACC.productDetail.bindCurrentStyle(), ACC.productDetail.bindCurrentSize(), ACC.productDetail.bindCurrentType();
  },
  bindCurrentStyle: function() {
    console.log("acc.productDetail.js - ACC.productDetail.bindCurrentStyle() : executed");
    var t = $("#currentStyleValue").data("styleValue"), e = $(".styleName");
    null !== t && e.text(": " + t);
  },
  bindCurrentSize: function() {
    console.log("acc.productDetail.js - ACC.productDetail.bindCurrentSize() : executed");
    var t = $("#currentSizeValue").data("sizeValue"), e = $(".sizeName");
    null !== t && e.text(": " + t);
  },
  bindCurrentType: function() {
    console.log("acc.productDetail.js - ACC.productDetail.bindCurrentType() : executed");
    var t = $("#currentTypeValue").data("typeValue"), e = $(".typeName");
    null !== t && e.text(": " + t);
  }
}, ACC.refinements = {
  _autoload: [ "bindMoreLessToggles", "bindMoreStoresToggles", "init", "bindSearch" ],
  coords: {},
  storeSearchData: {},
  init: function() {
    navigator.geolocation.getCurrentPosition(function(t) {
      ACC.refinements.coords = t.coords;
    }, function(t) {
      console.log("An error occurred... The error code and message are: " + t.code + "/" + t.message);
    });
  },
  bindSearch: function() {
    $(document).on("submit", "#user_location_form", function(t) {
      t.preventDefault();
      var e = $(".js-shop-stores-facet .js-shop-store-search-input").val();
      e.length > 0 && ACC.refinements.getInitStoreData(e);
    }), $(document).on("click", "#findStoresNearMeAjax", function(t) {
      t.preventDefault(), ACC.refinements.getInitStoreData(null, ACC.refinements.coords.latitude, ACC.refinements.coords.longitude);
    });
  },
  getInitStoreData: function(t, e, i) {
    $(".alert").remove(), data = {
      q: "",
      page: "0"
    }, null !== t && (data.q = t), null !== e && (data.latitude = e), null !== i && (data.longitude = i), 
    ACC.refinements.storeSearchData = data, ACC.refinements.getStoreData();
  },
  getStoreData: function() {
    url = $(".js-facet-form").data("url"), $.ajax({
      url: url,
      data: ACC.refinements.storeSearchData,
      type: "get",
      success: function(t) {
        window.location.reload();
      }
    });
  },
  bindMoreLessToggles: function() {
    console.log("acc.refinements.js - ACC.refinements.bindMoreLessToggles() : executed"), 
    $(document).on("click", ".js-shop-stores-facet .js-facet-change-link", function(t) {
      t.preventDefault(), $(".js-shop-stores-facet .js-facet-container").hide(), $(".js-shop-stores-facet .js-facet-form").show();
    }), $(document).on("change", ".js-product-facet .js-facet-checkbox", function() {
      console.log("acc.refinements.js - ACC.refinements.bindMoreLessToggles() - change on : .js-product-facet .js-facet-checkbox"), 
      $(this).parents("form").submit();
    }), $(document).on("click", ".js-product-facet .js-more-facet-values-link", function(t) {
      t.preventDefault(), $(this).parents(".js-facet").find(".js-facet-top-values").hide(), 
      $(this).parents(".js-facet").find(".js-facet-list-hidden").show(), $(this).parents(".js-facet").find(".js-more-facet-values").hide(), 
      $(this).parents(".js-facet").find(".js-less-facet-values").show();
    }), $(document).on("click", ".js-product-facet .js-less-facet-values-link", function(t) {
      t.preventDefault(), $(this).parents(".js-facet").find(".js-facet-top-values").show(), 
      $(this).parents(".js-facet").find(".js-facet-list-hidden").hide(), $(this).parents(".js-facet").find(".js-more-facet-values").show(), 
      $(this).parents(".js-facet").find(".js-less-facet-values").hide();
    });
  },
  bindMoreStoresToggles: function() {
    $(document).on("click", ".js-shop-stores-facet .js-more-stores-facet-values", function(t) {
      t.preventDefault(), $(".js-shop-stores-facet ul.js-facet-list li.hidden").slice(0, 5).removeClass("hidden").first().find(".js-facet-checkbox").focus(), 
      0 === $(".js-shop-stores-facet ul.js-facet-list li.hidden").length && $(".js-shop-stores-facet .js-more-stores-facet-values").hide();
    });
  }
}, ACC.silentorderpost = {
  spinner: $("<img src='" + ACC.config.commonResourcePath + "/images/spinner.gif' />"),
  bindUseDeliveryAddress: function() {
    if ($("#useDeliveryAddress").on("change", function() {
      if ($("#useDeliveryAddress").is(":checked")) {
        var t = {
          countryIsoCode: $("#useDeliveryAddressData").data("countryisocode"),
          useDeliveryAddress: !0
        };
        ACC.silentorderpost.enableAddressForm(), ACC.silentorderpost.displayCreditCardAddressForm(t, ACC.silentorderpost.useDeliveryAddressSelected), 
        ACC.silentorderpost.disableAddressForm();
      } else ACC.silentorderpost.clearAddressForm(), ACC.silentorderpost.enableAddressForm();
    }), $("#useDeliveryAddress").is(":checked")) {
      var t = {
        countryIsoCode: $("#useDeliveryAddressData").data("countryisocode"),
        useDeliveryAddress: !0
      };
      ACC.silentorderpost.enableAddressForm(), ACC.silentorderpost.displayCreditCardAddressForm(t, ACC.silentorderpost.useDeliveryAddressSelected), 
      ACC.silentorderpost.disableAddressForm();
    }
  },
  bindSubmitSilentOrderPostForm: function() {
    $(".submit_silentOrderPostForm").click(function() {
      ACC.common.blockFormAndShowProcessingMessage($(this)), $(".billingAddressForm").filter(":hidden").remove(), 
      ACC.silentorderpost.enableAddressForm(), $("#silentOrderPostForm").submit();
    });
  },
  bindCycleFocusEvent: function() {
    $("#lastInTheForm").blur(function() {
      $('#silentOrderPostForm [tabindex$="10"]').focus();
    });
  },
  isEmpty: function(t) {
    return void 0 === t || null === t || "" === t;
  },
  disableAddressForm: function() {
    $('input[id^="address\\."]').prop("disabled", !0), $('select[id^="address\\."]').prop("disabled", !0);
  },
  enableAddressForm: function() {
    $(".billingAddressForm").css("display", "block"), $('input[id^="address\\."]').prop("disabled", !1), 
    $('select[id^="address\\."]').prop("disabled", !1);
  },
  clearAddressForm: function() {
    $('input[id^="address\\."]').val(""), $('select[id^="address\\."]').val("");
  },
  useDeliveryAddressSelected: function() {
    $("#useDeliveryAddress").is(":checked") ? ACC.silentorderpost.disableAddressForm() : (ACC.silentorderpost.clearAddressForm(), 
    ACC.silentorderpost.enableAddressForm());
  },
  bindCreditCardAddressForm: function() {
    $("#billingCountrySelector :input").on("change", function() {
      var t = {
        countryIsoCode: $(this).val(),
        useDeliveryAddress: !1
      };
      ACC.silentorderpost.displayCreditCardAddressForm(t);
    });
  },
  displayCreditCardAddressForm: function(t, e) {
    $.ajax({
      url: ACC.config.encodedContextPath + "/checkout/multi/payment-method/paymentAddressForm",
      async: !0,
      data: t,
      dataType: "html",
      beforeSend: function() {
        $("#billingAddressForm").html(ACC.silentorderpost.spinner);
      }
    }).done(function(t) {
      $("#billingAddressForm").html(t), ACC.deliveryarea.fillCitiesList(), "function" == typeof e && e.call();
    });
  }
}, $(document).ready(function() {
  ACC.silentorderpost.bindUseDeliveryAddress(), ACC.silentorderpost.bindSubmitSilentOrderPostForm(), 
  ACC.silentorderpost.bindCreditCardAddressForm(), $("#useDeliveryAddress").click();
  var t, e = function() {
    var t = 0;
    return function(e, i) {
      clearTimeout(t), t = setTimeout(e, i);
    };
  }(), i = function() {
    var e = $(t.closest("form"));
    $(e).hasClass("update_cart_form") && e.submit();
  };
  $(".js-qty-selector-input").keyup(function(n) {
    t = $(n.target), e(i, 1e3);
  });
}), $(function() {
  _autoload();
}), ACC.addtocartaction = {}, $(document).ready(function() {
  with (ACC.addtocartaction) ;
}), ACC.listaddtocartaction = {}, $(document).ready(function() {
  with (ACC.listaddtocartaction) ;
}), ACC.listorderformaction = {}, $(document).ready(function() {
  with (ACC.listorderformaction) ;
}), ACC.listpickupinstoreaction = {}, $(document).ready(function() {
  with (ACC.listpickupinstoreaction) ;
}), ACC.pickupinstoreaction = {}, $(document).ready(function() {
  with (ACC.pickupinstoreaction) ;
}), ACC.shareonsocialnetworkaction = {}, $(document).ready(function() {
  with (ACC.shareonsocialnetworkaction) ;
}), ACC.view360 = {
  open360View: function() {
    console.log("Open360Vew Popin Function");
    var t = $(".listImagesView360").html();
    t = '<div class="angle-view View360InPopin">' + t + "</div>", ACC.colorbox.open("", {
      open: !0,
      current: "",
      title: "",
      html: t,
      close: '<span class="close"></span>',
      className: "View360Popin",
      onComplete: function() {
        console.log("360 Angle View Product Details- loaded"), $(".View360InPopin").angle({
          speed: 1,
          previous: ".prev-image",
          next: ".next-image"
        });
      }
    });
  }
}, $(document).ready(function() {
  console.log("scs.checkout.js : executed"), "cod" === $("#codePaymentModeValue").val() && $("#paymentCashText").removeClass("hidden"), 
  $(document).on("click", ".js-payment-mode", function(t) {
    var e = $(this).val();
    $("#paymentCashText").toggleClass("hidden", "cod" !== e), $("#saveCardBlock").toggle("creditcard" === e), 
    $.get(ACC.config.encodedContextPath + "/checkout/multi/payment-method/" + e), $(".payment-type span").html($("label[for='" + $(this).attr("id") + "']").text());
  }), $(document).on("click", ".js-cancelOrderDetail", function(t) {
    console.log("scs.checkout.js : start executed"), console.log("CancelOrder : start executed");
    var e = document.getElementById("orderCode").value;
    $.ajax({
      url: ACC.config.encodedContextPath + "/my-account/cancelOrder?orderCode=" + e,
      type: "POST",
      data: {},
      success: function(t) {
        window.location = ACC.config.encodedContextPath + "/my-account/orders";
      }
    }), console.log("CancelOrder : end executed");
  }), $(document).on("click", ".js-reOrder", function(t) {
    console.log("scs.checkout.js : start executed"), console.log("ReOrder : start executed");
    var e = document.getElementById("orderCode").value;
    $.ajax({
      url: ACC.config.encodedContextPath + "/my-account/reorder?orderCode=" + e,
      type: "POST",
      data: {},
      success: function(t) {
        window.location = ACC.config.encodedContextPath + "/cart";
      }
    }), console.log("ReOrder : end executed");
  });
});

var SLOTS = {
  classSlot: "deliverySlot",
  classSlotSelected: "selected",
  classSlotAvailable: "available"
};

SLOTS.selected = {
  TRUE: !0,
  FALSE: !1
}, SLOTS.refreshSlots = function() {
  $.each($("#divDeliverySlotsTable .deliverySlot"), function() {
    $(this).data("selected") === SLOTS.selected.TRUE ? $(this).addClass(SLOTS.classSlotSelected) : $(this).removeClass(SLOTS.classSlotSelected);
  });
}, SLOTS.capitalize = function(t) {
  return t.charAt(0).toUpperCase() + t.slice(1);
}, deliveryPerferedSlot = function() {
  var t = document.getElementById("deliveryPerferedSlot").value.split(","), e = t[0], i = t[1], n = t[2], o = t[3];
  $.ajax({
    url: "save-preferred-slot",
    data: {
      pos: e,
      date: i,
      slotCode: n
    },
    type: "post",
    dataType: "json",
    success: function() {
      console.log("scs.delivery-address.js - Save the preferred delivery slot : SUCCESS"), 
      $("#divFavoriteSlot").show();
      var t = SLOTS.capitalize(o);
      $("#divFavoriteSlot .lnkShowSlots").text(" " + t), $("#divShowSlots").hide(), $("#divSelectSlot").hide();
    },
    error: function() {
      console.error("scs.delivery-address.js - Error saving the preferred delivery slot"), 
      $("#divSelectSlot").hide();
    }
  });
}, $(function() {
  SLOTS.refreshSlots(), $(".deliverySlot").css("cursor", "pointer"), $(".lnkShowSlots").click(function() {
    $("#divSelectSlot").toggle();
  }), $("#divDeliverySlotsTable .deliverySlot").click(function() {
    if ("unavailable" !== $(this).data("status")) {
      var t = $(this);
      $.ajax({
        url: "save-selected-slot",
        data: {
          pos: $(this).data("pos"),
          date: $(this).data("date"),
          slotCode: $(this).data("slot-code")
        },
        type: "post",
        dataType: "json",
        success: function() {
          console.log("scs.delivery-address.js - Save the selected delivery slot : SUCCESS"), 
          $("#divDeliverySlotsTable .deliverySlot[data-selected='true']").data("status", "available"), 
          $("#divDeliverySlots .deliverySlot").data("selected", SLOTS.selected.FALSE), t.data("selected", SLOTS.selected.TRUE), 
          SLOTS.refreshSlots();
        },
        error: function() {
          console.error("scs.delivery-address.js - Error saving the selected delivery slot");
        }
      });
    }
  }), $('#divDeliverySlotsTable [data-status="' + SLOTS.classSlotAvailable + '"]:not(".selected")').hover(function() {
    SLOTS.saveHtml = $(this).html(), $(this).html(ACC.config.deliverySlot.statusOk);
  }, function() {
    $(this).html(SLOTS.saveHtml);
  }), $("#divSelectSlot ." + SLOTS.classSlot).hover(function() {
    SLOTS.saveHtml = $(this).html(), $(this).html(ACC.config.deliverySlot.statusOk);
  }, function() {
    $(this).html(SLOTS.saveHtml);
  });
}), ACC.deliveryarea = {
  _autoload: [ "fillCitiesList", "updateAreaDropDown" ],
  citiesList: [],
  fillCitiesList: function() {
    $("#address\\.townCity").length && (console.log("scs.deliveryarea.js - ACC.deliveryarea.fillCitiesList() : executed"), 
    $.ajax({
      url: ACC.config.encodedContextPath + "/my-account/address-area-list",
      async: !0,
      dataType: "json"
    }).done(function(t) {
      console.log("scs.deliveryarea.js - ACC.deliveryarea.fillCitiesList() - Ajax /my-account/address-area-list : success"), 
      t && (ACC.deliveryarea.citiesList = t, 2 === $("#address\\.townCity").children("option").length && $("#address\\.townCity option").is(":selected") && ACC.deliveryarea.getAreaDropDownList("#address\\.state", $("#address\\.townCity option:selected").val()));
    }));
  },
  updateAreaDropDown: function() {
    console.log("scs.deliveryarea.js - ACC.deliveryarea.updateAreaDropDown() : executed"), 
    $(document).on("change", "#address\\.townCity", function() {
      console.log("scs.deliveryarea.js - ACC.deliveryarea.updateAreaDropDown() - Change event triggered"), 
      ACC.deliveryarea.getAreaDropDownList("#address\\.state", $(this).val());
    });
  },
  getAreaDropDownList: function(t, e) {
    console.log("scs.deliveryarea.js - ACC.deliveryarea.getAreaDropDownList() : executed"), 
    $(t).empty();
    var i = [];
    console.log("scs.deliveryarea.js - ACC.deliveryarea.getAreaDropDownList() - fetching areas"), 
    $.each(ACC.deliveryarea.citiesList, function(t, n) {
      e === n.code && (i = n.areas);
    });
    var n;
    $(t).parent().find("#selected-value").length && (n = $(t).parent().find("#selected-value").attr("selected-value")), 
    console.log("scs.deliveryarea.js - ACC.deliveryarea.getAreaDropDownList() - updating html"), 
    $.each(i, function(e, i) {
      null !== n && n === i.code ? $(t).append($("<option selected></option>").attr("value", i.code).text(i.name)) : $(t).append($("<option></option>").attr("value", i.code).text(i.name));
    });
  }
}, $(document).ready(function() {
  console.log("scs.login.js : start executed"), $("#register\\.membershipNumber").blur(function() {
    var t = $(this).val(), e = document.getElementById("register.membershipPrefixNumber").value;
    null !== t && null !== e && "" !== t && "" !== e && $.ajax({
      url: ACC.config.encodedContextPath + "/login/verifyShareholder?number=" + e + t,
      type: "GET",
      data: {},
      success: function(t) {
        document.getElementById("register.firstName").value = t.firstName, document.getElementById("register.lastName").value = t.lastName, 
        document.getElementById("register.email").value = t.email, null !== t.phoneNumber && (document.getElementById("register.mobileNumber").value = t.phoneNumber), 
        null !== t.gender && ("MALE" === t.gender ? document.getElementById("profile.genderCode1").checked = !0 : document.getElementById("profile.genderCode2").checked = !0), 
        null !== t.email & "" !== t.email ? document.getElementById("register.email").readOnly = !0 : document.getElementById("register.email").readOnly = !1;
      }
    });
  }), $("#register\\.membershipPrefixNumber").blur(function() {
    var t = $(this).val(), e = document.getElementById("register.membershipNumber").value;
    null !== e && null !== t && "" !== e && "" !== t && $.ajax({
      url: ACC.config.encodedContextPath + "/login/verifyShareholder?number=" + t + e,
      type: "GET",
      data: {},
      success: function(t) {
        document.getElementById("register.firstName").value = t.firstName, document.getElementById("register.lastName").value = t.lastName, 
        document.getElementById("register.email").value = t.email, null !== t.phoneNumber && (document.getElementById("register.mobileNumber").value = t.phoneNumber), 
        null !== t.gender && ("MALE" === t.gender ? document.getElementById("profile.genderCode1").checked = !0 : document.getElementById("profile.genderCode2").checked = !0), 
        null !== t.email & "" !== t.email ? document.getElementById("register.email").readOnly = !0 : document.getElementById("register.email").readOnly = !1;
      }
    });
  }), console.log("scs.login.js :  end executed");
});

var SCS = {};

SCS.main = {
  removeColorboxTitle: function() {
    $("#cboxTitle").remove();
  }
}, $(document).ready(function() {
  window.innerWidth >= 1024 ? ($('div[id^="child2Desktop"]').each(function() {
    this.classList.remove("displayChildDesktop");
  }), $('div[id^="child2Mobile"]').each(function() {
    this.classList.add("displayChildMobile");
  })) : ($('div[id^="child2Desktop"]').each(function() {
    this.classList.add("displayChildDesktop");
  }), $('div[id^="child2Mobile"]').each(function() {
    this.classList.remove("displayChildMobile");
  })), 0 !== $("#preferredSlotLabel").length && $("#divSelectSlot").hide(), $("#paymentMode-creditcardod").is(":checked") && $("#saveCardBlock").hide(), 
  $("#paymentMode-cod").is(":checked") && $("#saveCardBlock").hide(), 0 === $(".carousel-slide1").length && $(".category--carousel1").hide(), 
  0 === $(".carousel-slide2").length && $(".brand--carousel2").hide(), "MALE" === $("#myAccountGenderCode").val() ? ($(".personaldetails").removeClass("icon--user-circle"), 
  $(".personaldetails").removeClass("icon--woman-circle"), $(".personaldetails").addClass("icon--user-circle")) : ($(".personaldetails").removeClass("icon--user-circle"), 
  $(".personaldetails").removeClass("icon--woman-circle"), $(".personaldetails").addClass("icon--woman-circle")), 
  $("#orderHistoryDataTable").DataTable({
    language: {
      paginate: {
        next: "&rsaquo;",
        previous: "&lsaquo;"
      }
    },
    columnDefs: [ {
      targets: [ 0 ],
      orderData: [ 0, 1 ]
    }, {
      targets: [ 1 ],
      orderData: [ 1, 0 ]
    }, {
      targets: [ 2 ],
      orderData: [ 2, 0 ]
    }, {
      targets: [ 3 ],
      orderData: [ 3, 0 ]
    } ]
  }), $("#orderSortBySelectFilter").change(function() {
    if ("1" === $("#orderSortBySelectFilter").val()) $("#sortFilter_1").trigger("click"); else if ("2" === $("#orderSortBySelectFilter").val()) $("#sortFilter_2").trigger("click"); else if ("3" === $("#orderSortBySelectFilter").val()) $("#sortFilter_3").trigger("click"); else {
      if ("4" !== $("#orderSortBySelectFilter").val()) return;
      $("#sortFilter_4").trigger("click");
    }
    $("#orderSortBySelectFilter").val("0");
  }), console.log("scs.main.js : executed");
}), $(document).ready(function() {
  var t = $(".js-carousel");
  console.log("scs.slickconf.js - loaded"), t.on("init", function(t) {
    console.log("test"), $(this).css({
      opacity: 0
    }).animate({
      opacity: 1
    }, 1e3);
  }), t.each(function(t) {
    $(this).slickActivator();
  }), $('.js-gallery-carousel[data-slick-to-click="true"]').on("click", "a.gallery-carousel--item", function(e) {
    e.preventDefault(), "number" == typeof $(this).data("index") && t.slick("slickGoTo", $(this).data("index"));
  }), t.each(function() {
    $(this).on("afterChange", function() {
      "mobile" === ACC.device.type && ($(this).find(".js-qty-selector").removeClass("hiddenClass"), 
      $(this).find(".slick-slide").not(".slick-current").each(function() {
        $(this).find(".js-qty-selector").addClass("hiddenClass");
      }));
    }), $(this).find(".product-tile").length && $(this).on("setPosition", function() {
      $(this).find(".slick-slide").height("auto");
      var t = $(this).find(".slick-track"), e = $(t).outerHeight();
      $(this).find(".slick-slide").css("height", e + "px");
    });
  });
  var e = function() {
    "mobile" === ACC.device.type && $(this).find(".slick-current").next().find(".js-qty-selector").addClass("hiddenClass");
  };
  $(window).resize(function() {
    e();
  }).resize();
}), ACC.wishlistorder = {
  _autoload: [ "bindWishlistOrder", "bindWishlistOrderCancel", "bindWishlistOrderSubmit" ],
  bindWishlistOrder: function() {
    $(".js-wishlist-add-button").on("click", function(t) {
      console.log("scs.wishlist-order.js - ACC.wishlist-order.bindWishlistOrder : wishlist add button clicked"), 
      t.preventDefault();
      var e = {
        html: $("#idAddOrderToWishlist").html()
      };
      ACC.colorbox.open("", e);
    });
  },
  bindWishlistOrderSubmit: function() {
    $(document).on("click", "#idSubmitNewWishlist", function() {
      console.log("scs.wishlist-order.js - ACC.wishlist-order.bindWishlistOrderSubmit : submitting wishlist");
      var t = $("#cboxContent #idSelectOption").val();
      return t === ACC.wishlist.NEW ? (console.log("scs.wishlist-order.js - ACC.wishlist-order.bindWishlistOrderSubmit : wishlist name is empty"), 
      0 === $("#cboxContent #newWishlistName").val().length ? ($("#cboxContent #newWishlistName").addClass("errorInput"), 
      $("#cboxContent .errorValidation").show(), ACC.colorbox.resize(), !1) : $("#cboxContent #newWishlistName").val().length > 50 ? (console.log("scs.wishlist-order.js - ACC.wishlist-order.bindWishlistOrderSubmit : wishlist name is too long"), 
      $("#cboxContent #newWishlistName").addClass("errorInput"), $("#cboxContent .errorSize").show(), 
      ACC.colorbox.resize(), !1) : (console.log("scs.wishlist-order.js - ACC.wishlist-order.bindWishlistOrderSubmit : prepare to create new wishlist"), 
      ACC.wishlistorder.addOrderToWishlist("/checkout/orderConfirmation/addToWishlist/?" + ACC.wishlistorder.generateAddProductParams()), 
      $(".js-wishlist-add-button").prop("disabled", !0), !0)) : t !== ACC.wishlist.NONE ? (console.log("scs.wishlist-order.js - ACC.wishlist-order.bindWishlistOrderSubmit : prepare to update wishlist"), 
      ACC.wishlistorder.addOrderToWishlist("/checkout/orderConfirmation/addToWishlist/?" + ACC.wishlistorder.generateAddProductParams()), 
      $(".js-wishlist-add-button").prop("disabled", !0), !0) : void 0;
    });
  },
  generateAddProductParams: function() {
    var t = !1;
    console.log("scs.wishlist-order.js - ACC.wishlist-order.generateAddProductParams : preparing wishlist params");
    var e = $("#cboxContent #idSelectOption").val();
    e === ACC.wishlist.NEW && (t = !0, e = $("#cboxContent #newWishlistName").val());
    var i = {
      wishlistName: e,
      orderCode: $("#cboxContent #orderCode").val(),
      isNewWishlist: t
    };
    return $.param(i);
  },
  addOrderToWishlist: function(t) {
    console.log("scs.wishlist-order.js - ACC.wishlist-order.addOrderToWishlist : sending wishlist params to server"), 
    $.ajax({
      url: ACC.config.encodedContextPath + t,
      dataType: "html",
      type: "GET",
      success: function(t) {
        console.log("scs.wishlist-order.js - ACC.wishlist-order.addOrderToWishlist : sending successfull");
        var e = t, i = "Order successfully added to shopping list ", n = $("#cboxContent #idSelectOption").val();
        n === ACC.wishlist.NEW && (n = $("#cboxContent #newWishlistName").val(), i = "Order successfully added to newly created shopping list "), 
        -1 !== e.indexOf("success") ? $("#cboxContent #idResult").text(i + n) : $("#cboxContent #idResult").text("Error while trying to add order to shopping list " + n), 
        $("#cboxContent #idResult").show(), $("#cboxContent .wishListPopin").hide(), ACC.colorbox.resize();
      }
    });
  },
  bindWishlistOrderCancel: function() {
    $(document).on("click", "#wishlistCancel", function(t) {
      console.log("scs.wishlist-order.js - ACC.wishlist-order.bindWishlistOrderCancel : canceling wishlist popin"), 
      t.preventDefault(), ACC.colorbox.close();
    });
  }
}, ACC.wishlist = {
  NEW: "newWishlist",
  NONE: "noWishlist",
  quantity: 1,
  _autoload: [ "bindWishlist" ],
  bindWishlist: function() {
    console.log("scs.wishlist.js - ACC.wishlist.bindWishlist : executed"), ACC.wishlist.selectedByDefault(), 
    ACC.wishlist.updateTotalPrice(), $(".js-wishlist-click").on("click", function(t) {
      t.preventDefault(), ACC.wishlist.performChooseWishlistValue($(this));
    }), $(".js-wishlist-new").on("click", function(t) {
      t.preventDefault(), ACC.wishlist.openNewWishlistPopin();
    }), $(".js-add-product-to-wishlist").on("click", function(t) {
      t.preventDefault(), ACC.wishlist.openAddProductToWishlistPopup(this);
    }), $(".js-wishlist-rename").on("click", function(t) {
      t.preventDefault(), ACC.wishlist.openRenameWishlistPopin();
    }), $(".js-wishlist-share").on("click", function(t) {
      t.preventDefault(), ACC.wishlist.openShareWishlistPopin();
    }), $("body").on("keyup", "#idWishlistName", function(t) {
      t.preventDefault(), ACC.wishlist.wishlistNameValidator();
    }), $("body").on("click", ".js-wishlist-new-cancel", function(t) {
      t.preventDefault(), ACC.colorbox.close();
    }), $("body").on("click", ".js-wishlist-new-send", function(t) {
      var e = $("#receiverName").val(), i = $("#codewishlist").val(), n = $("#receiverEmail").val(), o = ACC.config.encodedContextPath + "/my-account/wishlist/share/?wishlitCode=" + i + "&receiverName=" + e + "&receiverEmail=" + n;
      $.ajax({
        url: o,
        data: {},
        type: "POST",
        success: function() {
          ACC.colorbox.close();
        },
        error: function(t) {
          console.log(t.message);
        }
      });
    }), $("body").on("click", ".js-wishlist-create", function(t) {
      t.preventDefault(), ACC.wishlist.performSaveWishlistFromAccount("INSERT");
    }), $("body").on("click", ".js-wishlist-rename-confirm", function(t) {
      t.preventDefault(), ACC.wishlist.performSaveWishlistFromAccount("UPDATE");
    }), $(".js-wishlist-delete").on("click", function(t) {
      t.preventDefault(), ACC.wishlist.openDeleteWishlistPopin($(this));
    }), $("body").on("click", ".js-wishlist-delete-confirm", function(t) {
      t.preventDefault(), ACC.wishlist.performDeleteWishlist();
    }), $("body").on("click", ".js-wishlist-delete-cancel", function(t) {
      t.preventDefault(), ACC.colorbox.close();
    }), $(".js-wishlist-back").on("click", function(t) {
      t.preventDefault(), $(location).attr("href", ACC.config.encodedContextPath + "/my-account/wishlist");
    }), $(".js-wishlist-entry-delete").on("click", function(t) {
      t.preventDefault(), ACC.wishlist.openDeleteWishlistEntryPopin($(this));
    }), $("body").on("click", ".js-wishlist-entry-delete-confirm", function(t) {
      t.preventDefault(), ACC.wishlist.performRemoveWishlistEntry();
    }), $("body").on("click", ".js-wishlist-entry-delete-cancel", function(t) {
      t.preventDefault(), ACC.colorbox.close();
    }), $(".js-wishlist-entry-check-all").change(function() {
      $("input:checkbox").prop("checked", $(this).prop("checked")), ACC.wishlist.disableAddToCartButton();
    }), $(".js-entry-checkbox").change(function() {
      ACC.wishlist.disableAddToCartButton();
    }), $(".my-account-wishlist .js-wishlist-add-to-cart").on("click", function(t) {
      t.preventDefault(), ACC.wishlist.performAddEntriesTocart($(this));
    }), $(".my-account-wishlist .js-qty-selector-input").on("change", function(t) {
      ACC.wishlist.updateSubtotalPrice($(this));
    }).triggerHandler("change"), "" === $.trim($(".promo-banniere").text()) && $(".promo-banniere").hide(), 
    $(".fact-liv:not(.n-livrer)").click(function() {
      1 === $(this).parent(".facturation").index() && $("#factAddress").val($(this).attr("id").substring(5)), 
      2 === $(this).parent(".facturation").index() && $("#livAddress").val($(this).attr("id").substring(4)), 
      $(this).addClass("checked");
    });
    var t = $("body").attr("class");
    if (t) {
      var e = t.split(" ");
      ACC.wishlist.getPageName(e) && "true" === ACC.wishlist.readCookie("flagwishlist") && (ACC.colorbox.open("", {
        href: $("#idFav").attr("href"),
        resize: !0
      }), ACC.colorbox.resize(), ACC.wishlist.eraseCookie("flagwishlist"));
    }
  },
  selectedByDefault: function() {
    console.log("scs.wishlist.js - ACC.wishlist.selectedByDefault : executed"), $(".my-account-wishlist input:checkbox").prop("checked", !0);
  },
  performWishlistCookieValidation: function() {
    console.log("scs.wishlist.js - ACC.wishlist.performWishlistCookieValidation : executed"), 
    "false" === ACC.wishlist.readCookie("flagwishlist") && (ACC.wishlist.createCookie("flagwishlist", "true", 1), 
    ACC.wishlist.readCookie("flagwishlist"));
  },
  bindAddProductPopup: function() {
    console.log("scs.wishlist.js - ACC.wishlist.bindAddProductPopup : executed"), $(".wishlistError").hide(), 
    ACC.wishlist.refreshAddProductPopup(), ACC.colorbox.resize(), $("#idSelectOption").val() === ACC.wishlist.NEW && ($("#newWishlistSection").removeClass("hidden"), 
    $("#idNewWish").focus(), ACC.colorbox.resize()), $("#wishlistCancel").click(function() {
      ACC.colorbox.close();
    }), $("#idSelectOption").change(function() {
      $(this).val() === ACC.wishlist.NEW && (console.debug("Selected New shopping list"), 
      $("#newWishlistSection").removeClass("hidden"), $("#idNewWish").focus(), ACC.colorbox.resize()), 
      ACC.wishlist.refreshAddProductPopup();
    }), $("#idNewWish").on("change, keyup", function() {
      ACC.wishlist.refreshAddProductPopup();
    }), $("#idSubmitNewWish").click(function() {
      var t = $("#idSelectOption").val();
      t === ACC.wishlist.NEW ? ACC.wishlist.performSubmitNewWishlist() : t !== ACC.wishlist.NONE && ACC.wishlist.performSelectWishlistOption();
    }), $("#idLoginhref").click(function() {
      ACC.wishlist.performWishlistCookieValidation();
    }), SCS.main.removeColorboxTitle();
  },
  refreshAddProductPopup: function() {
    console.log("scs.wishlist.js - ACC.wishlist.refreshAddProductPopup : executed");
    var t = $("#idSubmitNewWish");
    switch ($("#idSelectOption").val()) {
     case ACC.wishlist.NONE:
      t.text(t.data("add-label")), t.attr("disabled", !0);
      break;

     case ACC.wishlist.NEW:
      t.text(t.data("create-label")), t.attr("disabled", 0 === $("#idNewWish").val().length);
      break;

     default:
      t.text(t.data("add-label")), t.attr("disabled", !1);
    }
  },
  generateAddProductParams: function() {
    console.log("scs.wishlist.js - ACC.wishlist.generateAddProductParams : executed");
    var t = $("#idSelectOption").val(), e = "";
    t === ACC.wishlist.NEW && (t = $("#idNewWish").val()), $("#productConfOption").length > 0 && (e = $("#productConfOption").val());
    var i = {
      productCode: $("#productCode").val(),
      quantity: ACC.wishlist.quantity,
      wishlistName: t,
      listFromCart: $("#listFromCart").val(),
      configProduct: e
    };
    return $.param(i);
  },
  performSelectWishlistOption: function() {
    console.log("scs.wishlist.js - ACC.wishlist.performSelectWishlistOption : executed"), 
    ACC.wishlist.addProductToWishList("/wishlist/addProductWishlist/?" + ACC.wishlist.generateAddProductParams());
  },
  performSubmitNewWishlist: function() {
    return console.log("scs.wishlist.js - ACC.wishlist.performSubmitNewWishlist : executed"), 
    0 === $("#idNewWish").val().length ? ($("#idNewWish").addClass("errorInput"), $(".errorValidation").show(), 
    ACC.colorbox.resize(), !1) : $("#idNewWish").val().length > 50 ? ($("#idNewWish").addClass("errorInput"), 
    $(".errorSize").show(), ACC.colorbox.resize(), !1) : (ACC.wishlist.addNewWishList("/wishlist/addNewWishlist/?" + ACC.wishlist.generateAddProductParams()), 
    !0);
  },
  performChooseWishlistValue: function(t) {
    console.log("scs.wishlist.js - ACC.wishlist.performChooseWishlistValue : executed");
    var e = t.data("wishlistItemName"), i = ACC.config.encodedContextPath + "/my-account/wishlist/detail?wishlistParam=" + e;
    $(location).attr("href", i);
  },
  openNewWishlistPopin: function() {
    console.log("scs.wishlist.js - ACC.wishlist.openNewWishlistPopin : executed");
    var t = $(".js-wishlist-new").attr("title"), e = ACC.config.encodedContextPath + "/wishlist/addwishlist";
    ACC.colorbox.open(t, {
      open: !0,
      href: e,
      width: "600px",
      close: '<span class="close"></span>',
      maxWidth: "90%",
      maxHeight: "90%",
      onComplete: function() {
        $("#wishListPopin p:first").html(t), $("#cboxTitle").css({
          height: 0,
          padding: 0
        });
      }
    }), ACC.wishlist.createCookie("flagwishlist", "false", 1);
  },
  openRenameWishlistPopin: function() {
    console.log("scs.wishlist.js - ACC.wishlist.openRenameWishlistPopin : executed");
    var t = $(".js-wishlist-rename").attr("title"), e = $("#wishlistName").val(), i = ACC.config.encodedContextPath + "/wishlist/renamewishlist";
    ACC.colorbox.open(t, {
      open: !0,
      href: i,
      width: "600px",
      close: '<span class="close"></span>',
      maxWidth: "90%",
      maxHeight: "90%",
      onComplete: function() {
        $("#idWishlistName").val(e), SCS.main.removeColorboxTitle();
      }
    });
  },
  openShareWishlistPopin: function() {
    console.log("scs.wishlist.js - ACC.wishlist.openShareWishlistPopin : executed");
    var t = $("#wishlistName").val(), e = ACC.config.encodedContextPath + "/my-account/wishlist/share/?wishlitCode=" + t;
    $.ajax({
      url: e,
      type: "GET",
      success: function(t) {
        ACC.colorbox.open("", {
          open: !0,
          html: t,
          close: '<span class="close"></span>',
          maxWidth: "90%",
          maxHeight: "90%",
          width: "600px",
          height: "400px"
        });
      },
      error: function(t, e, i) {}
    });
  },
  openAddProductToWishlistPopup: function(t) {
    console.log("scs.wishlist.js - ACC.wishlist.openAddProductToWishlistPopup : executed"), 
    document.location.href.indexOf("/p/") > -1 ? ACC.wishlist.quantity = ACC.wishlist.searchQuantity(t, ".addtocart-component") : document.location.href.indexOf("/search/") > -1 ? ACC.wishlist.quantity = ACC.wishlist.searchQuantity(t, ".actions-container-for-SearchResultsGrid") : ACC.wishlist.quantity = ACC.wishlist.searchQuantity(t, ".carousel__item");
    var e = $(t).data("addProductToWishlistUrl"), i = $(t).data("title");
    ACC.colorbox.open("", {
      href: e,
      title: i,
      onComplete: ACC.wishlist.bindAddProductPopup,
      width: "400px",
      height: "200px",
      maxWidth: "90%",
      maxHeight: "90%"
    });
  },
  searchQuantity: function(t, e) {
    var i = $(t).parents(e).find(".js-qty-selector-input").val();
    return i || (console.warn("No quantity found, setting to 1"), i = 1), i;
  },
  wishlistNameValidator: function() {
    console.log("scs.wishlist.js - ACC.wishlist.wishlistNameValidator : executed"), 
    0 === $("#idWishlistName").val().length ? $("body").find("#idWishlistSaveButton").prop("disabled", !0) : $("body").find("#idWishlistSaveButton").prop("disabled", !1);
  },
  performSaveWishlistFromAccount: function(t) {
    return console.log("scs.wishlist.js - ACC.wishlist.performSaveWishlistFromAccount : executed"), 
    0 === $("#idWishlistName").val().length ? ($("#idWishlistName").addClass("errorInput"), 
    $(".errorValidationEmpty").show(), ACC.colorbox.resize(), !1) : $("#idWishlistName").val().length > 50 ? ($("#idWishlistName").addClass("errorInput"), 
    $(".errorMaxCharacter").show(), ACC.colorbox.resize(), !1) : (ACC.wishlist.saveWishlist(t), 
    !0);
  },
  saveWishlist: function(t) {
    console.log("scs.wishlist.js - ACC.wishlist.saveWishlist : executed");
    var e, i = $("#idWishlistName").val();
    e = "INSERT" === t ? "/wishlist/addwishlist/?wishlistName=" + i : "/wishlist/renamewishlist/?wishlistName=" + $("#wishlistName").val() + "&newWishlistName=" + i, 
    $.ajax({
      url: ACC.config.encodedContextPath + e,
      type: "POST",
      success: function(e) {
        ACC.wishlist.showSaveWishlistResult(e, t, i);
      }
    });
  },
  showSaveWishlistResult: function(t, e, i) {
    if (console.log("scs.wishlist.js - ACC.wishlist.showSaveWishlistResult : executed"), 
    t.error) {
      $("#idWishlistName").addClass("errorInput"), $(".errorValidationEmpty").hide(), 
      $(".errorValidationSpace").hide(), ACC.colorbox.resize();
      var n = $("#idWishlistName").val();
      "maxSize" === t.message ? ($(".errorSizeMessage").show(), $(".errorValidationUnique").hide(), 
      ACC.colorbox.resize()) : (" " === n ? ($(".errorValidationUnique").hide(), $(".errorValidationSpace").show()) : ($(".errorValidationUnique").show(), 
      $(".errorSizeMessage").hide()), ACC.colorbox.resize());
    } else {
      var o;
      o = "INSERT" === e ? ACC.config.encodedContextPath + "/my-account/wishlist" : ACC.config.encodedContextPath + "/my-account/wishlist/detail?wishlistParam=" + i, 
      $(location).attr("href", o), ACC.colorbox.resize();
    }
  },
  openDeleteWishlistPopin: function(t) {
    console.log("scs.wishlist.js - ACC.wishlist.openDeleteWishlistPopin : executed");
    var e = t.data("wishlistItemName");
    $("#wishlistName").val(e);
    var i = $("#whishConfirmationMssg").html();
    i = i.replace("{0}", "<b>" + e + "</b>"), $(".js-wishlist-delete-confirm")[0].setAttribute("id", e), 
    ACC.colorbox.open("", {
      open: !0,
      html: i,
      width: "600px",
      close: '<span class="close"></span>',
      maxWidth: "90%",
      maxHeight: "90%",
      onComplete: SCS.main.removeColorboxTitle
    });
  },
  performDeleteWishlist: function() {
    console.log("scs.wishlist.js - ACC.wishlist.performDeleteWishlist : executed");
    var t = $(".js-wishlist-delete-confirm")[0].id;
    $.ajax({
      url: ACC.config.encodedContextPath + "/wishlist/remove/",
      type: "POST",
      data: "wishlistName=" + t,
      success: function(t) {
        ACC.colorbox.close(), window.location = ACC.config.encodedContextPath + "/my-account/wishlist/";
      },
      error: function(t, e, i) {}
    });
  },
  openDeleteWishlistEntryPopin: function(t) {
    console.log("scs.wishlist.js - ACC.wishlist.openDeleteWishlistEntryPopin : executed");
    var e = t.data("productCode"), i = t.data("wishlistName").replace(" ", "%20"), n = ACC.config.encodedContextPath + "/wishlist/askremove/?productCode=" + e + "&wishlistName=" + i;
    ACC.colorbox.open("", {
      open: !0,
      href: n,
      width: "600px",
      close: '<span class="close"></span>',
      maxWidth: "90%",
      maxHeight: "90%",
      onComplete: SCS.main.removeColorboxTitle
    });
  },
  performRemoveWishlistEntry: function() {
    console.log("scs.wishlist.js - ACC.wishlist.performRemoveWishlistEntry : executed");
    var t = $("#idproductCode").attr("value"), e = $("#idwishlistName").attr("value"), i = ACC.config.encodedContextPath + "/wishlist/removeEntry/?productCode=" + t + "&wishlistName=" + e;
    $(location).attr("href", i);
  },
  performAddEntriesTocart: function(t) {
    console.log("scs.wishlist.js - ACC.wishlist.performAddEntriesTocart : executed");
    var e = [], i = [];
    $("input.js-entry-checkbox:checkbox").each(function() {
      this.checked && i.push("" + $(this).attr("id"));
    }), $("input.js-qty-selector-input:text").each(function() {
      var t = $(this).data("product").toString();
      for (var n in i) if (i[n] === t) {
        var o = "#productConfOption" + t, s = "";
        $(o).length && (s = $(o).val());
        var a = {
          productCode: t.toString(),
          quantity: parseFloat($(this).val()),
          configuration: s.toString()
        };
        e.push(a);
      }
    });
    var n = ACC.config.encodedContextPath + "/add-wish-to-cart/add";
    $.ajax({
      url: n,
      type: "POST",
      data: JSON.stringify(e),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(t) {
        t.productsNotAdded.forEach(function(t) {
          $("." + t.productCode + "-wl-entry").show();
        }), t.productsAdded.forEach(function(t) {
          $("." + t.productCode + "-wl-entry").hide();
        }), $.isArray(t.productsAdded) && t.productsAdded.length ? ($(".add-message").removeClass("alert"), 
        $(".add-message").removeClass("alert-danger"), $(".add-message").addClass("alert alert-ok global-alerts alert-msg alert-info alert-dismissable")) : ($(".add-message").removeClass("alert alert-ok"), 
        $(".add-message").removeClass("alert-info"), $(".add-message").addClass("alert global-alerts alert-msg alert-danger alert-dismissable")), 
        $(".add-message").html(t.message), $("input:checkbox").prop("checked", !1), ACC.minicart.updateMiniCartDisplay(), 
        ACC.wishlist.disableAddToCartButton();
      }
    });
  },
  disableAddToCartButton: function(t) {
    console.log("scs.wishlist.js - ACC.wishlist.disableAddToCartButton : executed");
    var e = 0;
    $("input.js-entry-checkbox:checkbox").each(function() {
      e += this.checked;
    }), 0 === e && $(".js-wishlist-entry-check-all").prop("checked", !1), ACC.wishlist.updateTotalPrice();
  },
  updateSubtotalPrice: function(t) {
    console.log("scs.wishlist.js - ACC.wishlist.updateSubtotalPrice : executed");
    var e = $(t).data("product"), i = t.val(), n = $("input#productUnitPrice_" + e).val();
    if ("" !== n) {
      var o = parseFloat(i) * parseFloat(n);
      $("span#productSubtotalPrice_" + e).html(o.toFixed(2));
    }
    ACC.wishlist.updateTotalPrice();
  },
  updateTotalPrice: function() {
    console.log("scs.wishlist.js - ACC.wishlist.updateTotalPrice : executed");
    var t = 0, e = $(".currency").html();
    $(".js-subtotal-price").each(function() {
      var e = $(this).data("productCode");
      $("#" + e).is(":checked") && (t += parseFloat($(this).html().replace(/[^\d\.]/g, "")));
    }), $("#grandTotalPrice").html(t.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,") + " " + e), 
    ACC.wishlist.updateTotalQty();
  },
  updateTotalQty: function() {
    console.log("scs.wishlist.js - ACC.wishlist.updateTotalQty : executed");
    var t = 0;
    $(".js-qty-selector-input").each(function() {
      var e = $(this).data("product");
      if ($("#" + e).is(":checked")) {
        var i = parseFloat($(this).val());
        "FOOD" === $("input#productType_" + e).val() && (i = 1), t += i;
      }
    }), 1 !== t ? $("#itemsCount").html(t + $("#items").html()) : $("#itemsCount").html(t + $("#item").html());
  },
  getPageName: function(t) {
    console.log("scs.wishlist.js - ACC.wishlist.getPageName : executed");
    for (var e, i = 0; i < t.length; i++) "page-productDetails" === t[i] && (e = t[i]);
    return e;
  },
  addProductToWishList: function(t) {
    console.log("scs.wishlist.js - ACC.wishlist.addProductToWishList : executed"), $.ajax({
      url: ACC.config.encodedContextPath + t,
      dataType: "html",
      type: "POST",
      success: function(t) {
        $("#idResult").html(t), ACC.colorbox.resize(), "false" === $("#addResult").attr("value") && $(".wishListPopin").hide();
      },
      error: function(t, e, i) {}
    });
  },
  addNewWishList: function(t) {
    console.log("scs.wishlist.js - ACC.wishlist.addNewWishList : executed"), $.ajax({
      url: ACC.config.encodedContextPath + t,
      dataType: "html",
      type: "POST",
      success: function(t) {
        $("#idResult").html(t), ACC.colorbox.resize(), "false" === $("#addResult").attr("value") && $(".wishListPopin").hide();
      }
    });
  },
  createCookie: function(t, e, i) {
    console.log("scs.wishlist.js - ACC.wishlist.createCookie : executed");
  },
  readCookie: function(t) {
    console.log("scs.wishlist.js - ACC.wishlist.readCookie : executed");
    for (var e = t + "=", i = document.cookie.split(";"), n = 0; n < i.length; n++) {
      for (var o = i[n]; " " === o.charAt(0); ) o = o.substring(1, o.length);
      if (0 === o.indexOf(e)) return o.substring(e.length, o.length);
    }
    return null;
  },
  eraseCookie: function(t) {
    console.log("scs.wishlist.js - ACC.wishlist.eraseCookie : executed"), ACC.wishlist.createCookie(t, "", -1);
  },
  formShareList: function(t) {
    console.log("scs.wishlist.js - ACC.wishlist.formShareList : executed"), $("#shareForm").validate({
      submitHandler: function(e) {
        shareWishList(e, t), ACC.colorbox.resize();
      }
    });
  },
  shareWishList: function(t, e) {
    console.log("scs.wishlist.js - ACC.wishlist.shareWishList : executed");
    var i = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
    return 0 === $("#email").val().length ? ($("#email").addClass("errorInputShare"), 
    $(".errorValidation").show(), $(".errorSize").hide(), ACC.colorbox.resize(), !1) : -1 === $("#email").val().search(i) ? ($("#email").addClass("errorInputShare"), 
    $(".errorValidation").hide(), $(".errorSize").show(), ACC.colorbox.resize(), !1) : $("#message").val().length > 500 ? ($("#message").addClass("errorInputShare"), 
    $(".errorValidation").hide(), $(".errorSize").hide(), $(".errorSizeMsg").show(), 
    ACC.colorbox.resize(), !1) : ($.ajax({
      type: "POST",
      url: ACC.config.contextPath + e,
      data: $(t).serialize(),
      success: function(t) {
        $("#partage-form").html($("#div-popin-pwd-ok").html()), ACC.colorbox.resize();
      },
      error: function(t, e, i) {
        console.error(i);
      }
    }), !0);
  }
}, $(document).ready(function() {
  console.log("on.click.conf.js - loaded"), $(".js-close-target").on("click", function(t) {
    console.log("on.click.conf.js - click on : .js-close-target", $(this).data("close-slide")), 
    t.preventDefault();
    var e = "undefined" !== $(this).data("close-target") && "string" == typeof $(this).data("close-target") ? $($(this).data("close-target")) : $(this), i = ("undefined" === $(this).data("close-remember") || $(this).data("close-remember"), 
    "undefined" !== $(this).data("close-slide") && "string" == typeof $(this).data("close-slide") && $(this).data("close-slide"));
    if (i) switch (e.find("> *").css({
      visibility: "hidden"
    }), i) {
     case "down":
      e.hide("slide", {
        direction: "down"
      }, 300);
      break;

     case "left":
      e.hide("slide", {
        direction: "left"
      }, 300);
      break;

     case "right":
      e.hide("slide", {
        direction: "right"
      }, 300);
      break;

     case "up":
      e.slideUp();
      break;

     default:
      e.hide();
    } else e.hide();
  }), $(document).on("click", "#cookie-bar .cb-enable", function() {
    $(".js-resize").trigger("click");
  });
  $(window).on("click", function() {
    $('[data-active-me="true"]').each(function() {
      $(this).removeClass("active"), void 0 !== $(this).attr("data-active-me") && $(this).removeAttr("data-active-me");
    });
  }), $(".js-activeMe").on("click", function(t) {
    t.stopPropagation(), $(this).toggleClass("active"), void 0 !== $(this).attr("data-active-me") ? $(this).removeAttr("data-active-me") : $(this).attr("data-active-me", "true");
  }), $(".js-open-view360").on("click", function(t) {
    console.log("Click on : .js-open-view360 To open View 360 Product", $(this).data("close-slide")), 
    t.stopPropagation(), ACC.view360.open360View();
  }), $(".js-closeNavStoreFinder").on("click", function(t) {
    console.log("js-closeNavStoreFinder : CLICK"), $(".store__finder--navigation").addClass("closedNav"), 
    $(".js-closeNavStoreFinder").hide(), $(".js-openNavStoreFinder").show();
  }), $(".js-openNavStoreFinder").on("click", function(t) {
    console.log("js-openNavStoreFinder : CLICK"), $(".store__finder--navigation").removeClass("closedNav"), 
    $(".js-closeNavStoreFinder").show(), $(".js-openNavStoreFinder").hide();
  });
}), $(window).load(function() {
  console.log("on.load.conf.js - loaded"), $(document).ready(function() {
    $("html").addClass("js"), console.log("Zoom Image Product"), $(".zoom").zoom({});
  }), console.log("on.load.conf.js - executed");
});
//# sourceMappingURL=main.js.map