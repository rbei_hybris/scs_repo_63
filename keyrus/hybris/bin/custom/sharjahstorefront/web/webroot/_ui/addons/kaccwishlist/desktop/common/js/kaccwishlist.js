
/* --  addProductWishlistPopup -- */ 	

function performWishlistCookieValidation() 
{
	var cookie_value = readCookie('flagwishlist');
	if(cookie_value == 'false')
	{
		createCookie('flagwishlist', 'true', 1);
		vall =  readCookie('flagwishlist');
	}
}

function performSelectWishlistOption() 
{
	var url = '/wishlist/addProductWishlist/?productCode='+ $('#productCode').attr('value') + '&wishlistName='+ $('#idSelectOption').val();
	addProductToWishList(url);
}

function performSubmitNewWishlist() 
{
	  if( $('#idNewWish').val().length === 0 )
	    {   
	    	 $("#idNewWish").addClass("errorInput");
	    	 $(".errorValidation").show();
	    	 $.colorbox.resize();
	    	 
	   		  return false
	    }
	    else if($('#idNewWish').val().length > 50)
	    	{
	    		 $("#idNewWish").addClass("errorInput");
	    		 $(".errorSize").show();
	    		 $.colorbox.resize();
	    		 return false;
	    	}
	    else{   
	     
	     	var url = '/wishlist/addNewWishlist/?productCode='+ $('#productCode').attr('value') + '&wishlistName=' +$('#idNewWish').attr('value');     
	    				 addNewWishList(url); 
	    				 $("#idNewWish").addClass("errorInput");
	    				 $(".errorValidation").hide();
	    				
	    }
}
/*--- 		----		--*/

/* -- addWishListPopup -- */

function performCreateWishlistFromAccount() 
{
    
    if( $('#idNewWishName').val().length === 0 )
    {   
    	 $("#idNewWishName").addClass("errorInput");
    	 $(".errorValidationEmpty").show();
   		  return false
    }
    else if($('#idNewWishName').val().length > 50)
	{
    		$("#idNewWishName").addClass("errorInput");
    		$(".errorMaxCharacter").show();
    		return false
	}
    else
    {   
    		var wishlistName= $('#idNewWishName').attr('value');
    		var url = '/wishlist/addwishlist/?wishlistName='+ wishlistName;    	
			createWishlist(url,wishlistName);    
    }
	
}

/* --     ----        -- */ 


/* -- removeWishlistEntry -- */

function performRemoveWishlistEntry() 
{
 	var productCode  =$('#idproductCode').attr('value'); 
	var	wishlistName =$('#idwishlistName').attr('value'); 
	var url = $('#idurl').attr('value')+'/wishlist/removeEntry/?productCode='+productCode+'&wishlistName='+wishlistName;
     location.href=url; 
}

function performCancelRemovingWishlistEntry() 
{
	  $.colorbox.close();
}
/* --     ----        -- */ 



/* --  wishlistpagecomponent    --  */

function performChooseWishlistValue()
{
	var selectedValue =  $('#wishcombo').val();
	var toUrl = ACC.config.encodedContextPath+'/my-account/wishlist/?wishlistParam=' + selectedValue;
	$(location).attr('href',toUrl);	
}

/* --     ----        -- */ 




function confirmRemove() {
	var url = $('#idurl').attr('value');
	$.colorbox({
		href : url,
		width : "400px",
		height : "350px",
	});
}

function getPageName(nclass) {
	var name;
	for (var int = 0; int < nclass.length; int++) {
		if (nclass[int] == "page-productDetails") {
			name = nclass[int];
		}
	}
	return name;
}

function addProductToWishList(url) {
	$.ajax({
		url : ACC.config.encodedContextPath + url,
		dataType : "html",
		type : "POST",
		success : function(data) {
			$('#idResult').html(data);
			var addResult = $('#addResult').attr('value');
			if (addResult == 'false') {
				$('.wishListPopin').hide();
				$.colorbox.resize();
			}
		},
		error : function(xht, textStatus, ex) {

		}
	});
};

function createWishlist(url,wishlistName) {
	$.ajax({
		url : ACC.config.encodedContextPath + url,
		type : "POST",
		success : function(data)
		{
			var result = data.error; 
			if(result == true)
				{
					$("#idNewWishName").addClass("errorInput");
					$('.errorValidationEmpty').hide();
					$.colorbox.resize();
						

					
					if(data.message=='maxSize')
					{
						$(".errorSizeMessage").show();
						$(".errorValidationUnique").hide()
						$.colorbox.resize();
						
					}else
					{
						$(".errorValidationUnique").show();
						$(".errorSizeMessage").hide(); 
						$.colorbox.resize();
					}	
					
				}
			else
				{
					var urlRedi = ACC.config.encodedContextPath+'/my-account/wishlist/?wishlistParam='+wishlistName;
					$(location).attr('href', urlRedi);
					$.colorbox.resize();
				}
		},
		error : function(xht, textStatus, ex) {

		}
	});
};


function addNewWishList(url) {
	$.ajax({
		url : ACC.config.encodedContextPath+ url,
		dataType : "html",
		type : "POST",
		success : function(data) {
			$('#idResult').html(data);
			var addResult = $('#addResult').attr('value');
			if (addResult == 'false') {
				$('.wishListPopin').hide();
				$.colorbox.resize();
			}
		},
		error : function(xht, textStatus, ex) {

		}
	});
};

function createCookie(name, value, days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
		var expires = "; expires=" + date.toGMTString();
	} else
		var expires = "";
	document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ')
			c = c.substring(1, c.length);
		if (c.indexOf(nameEQ) == 0)
			return c.substring(nameEQ.length, c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name, "", -1);
}


$(document).ready(function() {
		if ($.trim($(".promo-banniere").text()) == "") {
			$(".promo-banniere").hide();
				}
		// Select a default address in customer account
			$('.fact-liv:not(.n-livrer)').click(function() {
				if ($(this).parent('.facturation')
						.index() == 1) {
					$('#factAddress').val(
							$(this).attr("id")
									.substring(5));
				}
				if ($(this).parent('.facturation')
						.index() == 2) {
					$('#livAddress').val(
							$(this).attr("id")
									.substring(4));
				}
				$(this).addClass('checked');
			});

			// Pagination
			if (typeof enablePaginate != 'undefined' && enablePaginate) {
				var $paginationItems = $(paginationItemsClass
						+ ' .item-row');

				var totalItems = $paginationItems.length;
				var $pager = $('.pager');

				var pagesShown = 5;
				var nbPages = parseInt(totalItems / itemsPerPage)
						+ (totalItems % itemsPerPage > 0 ? 1 : 0);

				if (nbPages > 1) {
					function showPage(page) {
						$paginationItems.each(function(index) {
							$(this).toggle(	index >= (page - 1) * itemsPerPage	&& index < (page) * itemsPerPage);
						});
						var pagination = '';
						var displayPrev = '';
						if (page == 1) {
							displayPrev = 'style="display:none;"';
						}
						pagination += '<a class="prev" href="#'	+ (page - 1) + '" ' + displayPrev+ '><i class="ico ico-prev"></i></a>';
						for (i = 1; i <= nbPages; i++) {
							if (nbPages > pagesShown) {
								if ((page < pagesShown && i == pagesShown) || (page >= pagesShown && i - page == 1)) {
									pagination += '...';
								}
							}
							if (i < pagesShown || i <= page	|| i == nbPages) {
								var active = '';
								if (i == page) {
									active = 'active';
								}
								pagination += '<a class="' + active	+ '" href="#' + i + '">' + i + '</a>';
							}
						}
						var displayNext = '';
						if (page == nbPages) {
							displayNext = 'style="display:none;"';
						}
						pagination += '<a class="next" href="#'	+ (page + 1) + '" ' + displayNext + '><i class="ico ico-next"></i></a>';

						$pager.html(pagination);
						//equalHeightAll();
					}

					// Initialize to first page
					showPage(1);
					$pager.on('click', 'a', function(e) {
						e.preventDefault();

						var url = $(this).attr('href');
						var idx = url.indexOf("#");
						var page = idx != -1 ? parseInt(url	.substring(idx + 1)) : 1;
						showPage(page);

						// return to the beginning of the list
						$('html,body').animate(
								{
									scrollTop : $('#addAdressPopin')
											.offset().top
								}, 'fast');
					});
				}
			}

					var nclass = $('body').attr('class').split(" ");
					var name = getPageName(nclass);
					if (name != null) {
						var cookie_value = readCookie('flagwishlist');
						if (cookie_value == 'true') {
							$.colorbox({
								href : $("#idFav").attr('href'),
								resize : true
							});

							$.colorbox.resize();

							eraseCookie('flagwishlist');
						}
					}

					$(".linkToPopin").click(function() {
						$.colorbox({

							//width : "auto",
							height : "450px",
							maxWidth: '600px',
							open : true,
							href : $(this).attr('href'),
							onComplete : function() { 								
								$.colorbox.resize();
							  } 
						});
						
						
						createCookie('flagwishlist', 'false', 1);
						return false
						$.colorbox.resize();
					});
					

				});

function cancelDelete() {
	location.reload();
}

function confirmDelete()
{
	var selectBox = document.getElementById("wishcombo");
	var selectedValue = selectBox.options[selectBox.selectedIndex].value;
	var url = ACC.config.encodedContextPath+'/wishlist/remove/?wishlistName='+ selectedValue; 
	$(location).attr('href', url);
}

function showConfirmationMssg()
{
	$('#whishConfirmationMssg').addClass('whishConfirmationMssgVisible');
	$("#whishConfirmationBtn").hide();
	$.colorbox.resize();
}

function sendEmail() {
	var url = $('#iurl').attr('value');
	$.colorbox({
		href : url,
		width : "400px",
		height : "300px",
	});
}


function formShareList(url){
	$("#shareForm").validate({
		submitHandler: function(form) {
            shareWishList(form,url);
            $.colorbox.resize();
        }
	});
	
	}

function shareWishList(form,url) {
	var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
	
	if( $('#email').val().length === 0 )
	{   
		 $("#email").addClass("errorInputShare");
		 $(".errorValidation").show();
		 $(".errorSize").hide();
		 $.colorbox.resize();
		 return false
	}else if ($('#email').val().search(emailRegEx) == -1) {
	   	 $("#email").addClass("errorInputShare");
	   	 $(".errorValidation").hide();
	   	 $(".errorSize").show();
		 $.colorbox.resize();
		 return false
	}else if (
		$('#message').val().length > 400) {
		   	 $("#message").addClass("errorInputShare");
		   	 $(".errorValidation").hide();
		   	 $(".errorSize").hide();
		   	 $(".errorSizeMsg").show();
			 $.colorbox.resize();
			 return false
		}else{   
	 
		$.ajax({
			type: "POST",
			url: ACC.config.contextPath + url,
			data: $(form).serialize(),
			success: function(data) {
				$('#partage-form').html($('#div-popin-pwd-ok').html());
				$.colorbox.resize();
			},
		error : function(jqXHR, textStatus, errorThrown) 
		  {	alert("Error")  }
		});   
	}
	
	
};

